package com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity

import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.base.MockUpData
import com.eazy.daikou.request_data.request.service_property_ws.cleaning_service.ServiceCleaningWs
import com.eazy.daikou.helper.AbsoluteFitLayoutManager
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.model.my_property.service_property_employee.AllService
import com.eazy.daikou.model.my_property.service_provider.ServiceTypeCleaning
import com.eazy.daikou.model.utillity_tracking.model.UnitImgModel
import com.eazy.daikou.model.profile.User
import com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.adapter.*
import com.google.gson.Gson
import kotlin.collections.ArrayList


class SelectServiceNameCleaningActivity : BaseActivity(){

    private lateinit var imageView: ImageView
    private lateinit var recyclerViewListTerm: RecyclerView
    private lateinit var recyclerViewUnit: RecyclerView
    private lateinit var serviceTermAdapter: ServiceTermAdapter
    private lateinit var recyclerViewListService: RecyclerView
    private lateinit var serviceAdapter : ServiceCleaningNameAdapter
    private var sessionManagement: UserSessionManagement? = null
    private var user: User? = null
    private lateinit var loading : ProgressBar

    private var myUnitList: ArrayList<UnitImgModel> = ArrayList()

    private var state = 0
    private var serviceTermText = "time"
    private var unitTypeIds = ""
    private var unitId = ""
    private var userId = ""
    private var propertyId = ""
    private var CODE_REFRESH = 569

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_serive_cleaning)

        initView()

        initData()

    }

    private fun initView(){
        val titleTv = findViewById<TextView>(R.id.titleToolbar)
        titleTv.text = getString(R.string.create_cleaning_service)
        val btnBack = findViewById<ImageView>(R.id.iconBack)
        btnBack.setOnClickListener { finish() }

        imageView = findViewById(R.id.image_cleaning)
        recyclerViewListTerm = findViewById(R.id.list_term)
        recyclerViewListService = findViewById(R.id.list_type)
        recyclerViewUnit = findViewById(R.id.list_my_unit)
        loading = findViewById(R.id.loading_bar);

        loading.visibility = View.VISIBLE

        addTapServiceTerm()

    }

    private fun initData(){
        sessionManagement = UserSessionManagement(this)
        val gson = Gson()
        user = gson.fromJson(sessionManagement!!.userDetail, User::class.java)

        propertyId = user!!.activePropertyIdFk
        if(sessionManagement!!.userId!=null){
            userId = sessionManagement!!.userId
        }

        if(intent.hasExtra("lisUnit")){
            myUnitList = intent.getSerializableExtra("lisUnit") as ArrayList<UnitImgModel>
            unitTypeIds = myUnitList[0].propertyUnitTypeIdFk
            unitId = myUnitList[0].unitId
        }

        getUnitNo()

        addTapServiceTerm()

        getServiceType( unitTypeIds ,serviceTermText)

    }

    @SuppressLint("SetTextI18n")
    private val itemUnitNo = MyUnitsAdapter.ItemClickOnService { _, unitIds, unitTypeId, _, _ ->

            unitTypeIds = unitTypeId
            unitId = unitIds
            getServiceType(unitTypeIds,serviceTermText)

    }

    private fun addTapServiceTerm(){
        loading.visibility = View.GONE
        serviceTermText = "time"
        val layoutManager = AbsoluteFitLayoutManager(
            this,
            1,
            RecyclerView.HORIZONTAL,
            false,
            4
        )
        recyclerViewListTerm.layoutManager = layoutManager
        serviceTermAdapter = ServiceTermAdapter(MockUpData.listServiceTerm(),"service_term", state,this,  itemClickTap)
        recyclerViewListTerm.adapter = serviceTermAdapter
    }

    private val itemClickTap = ServiceTermAdapter.ItemClickOnService { listString, type, _, stat ->
        state = stat
        MockUpData.setStatProperty(state)
        if(type == "service_term"){
                serviceTermText = when (listString) {
                    "Time" -> {
                        "time"
                    }
                    "Weekly" -> {
                        "weekly"
                    }
                    else -> {
                        "monthly"
                    }
                }

            }
            loading.visibility = View.VISIBLE
            getServiceType(unitTypeIds,serviceTermText)
        }

    //getUnit
    private  fun getUnitNo() {
        val layoutManager = AbsoluteFitLayoutManager(
            this@SelectServiceNameCleaningActivity,
            1,
            RecyclerView.HORIZONTAL,
            false,
            3
        )
        recyclerViewUnit.layoutManager = layoutManager
        val adapter = MyUnitsAdapter(myUnitList, state,this@SelectServiceNameCleaningActivity,itemUnitNo)
        recyclerViewUnit.adapter = adapter
    }

    private fun getServiceType( unitTypeId : String , term : String ){
        ServiceCleaningWs().getServiceNameCleaning(this,1,5,unitTypeId,propertyId,term,serviceTypeCall)
    }

    private val serviceTypeCall = object : ServiceCleaningWs.OnLoadRequestCallBack{
        override fun onLoadSuccessFull(listService: List<ServiceTypeCleaning>) {
            loading.visibility = View.GONE
            val layoutManager = LinearLayoutManager(this@SelectServiceNameCleaningActivity)
            recyclerViewListService.layoutManager = layoutManager
            serviceAdapter = ServiceCleaningNameAdapter(this@SelectServiceNameCleaningActivity, listService, itemClickSeeMore, itemClick)
            recyclerViewListService.adapter = serviceAdapter

        }

        override fun onLoadFail(message: String) {
            loading.visibility = View.GONE
            Toast.makeText(this@SelectServiceNameCleaningActivity, message, Toast.LENGTH_SHORT).show()

        }

    }
    private var itemClickSeeMore = object : ServiceCleaningNameAdapter.ItemClickSeeMore{
        override fun onItemClickSeeMore(service: ServiceTypeCleaning) {
            val intent = Intent(this@SelectServiceNameCleaningActivity, ListAllServiceNameActivity::class.java)
            intent.putExtra("service_type",service.serviceType)
            intent.putExtra("service_term",serviceTermText)
            intent.putExtra("unitTypeId",unitTypeIds)
            intent.putExtra("unitId",unitId)
            startActivityForResult(intent,CODE_REFRESH)
        }

    }
    private var itemClick = object : ServiceCleaningAdapter.ItemClickOnServiceName{
        override fun onClick(service: AllService, stat: Int) {
            val intent = Intent(this@SelectServiceNameCleaningActivity, ServicePropertyDateTimeActivity::class.java)
            intent.putExtra("title",service.cleaningServiceName)
            intent.putExtra("service",service)
            intent.putExtra("service_term",serviceTermText)
            intent.putExtra("user_id",userId)
            intent.putExtra("unitId",unitId)
            startActivityForResult(intent,CODE_REFRESH)

        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(data!=null && resultCode == RESULT_OK){
            if(requestCode == CODE_REFRESH){
                if(data.hasExtra("isRefresh")){
                    val  intent = Intent()
                    loading.visibility = View.GONE
                    intent.putExtra("isRefresh","is")
                    setResult(RESULT_OK,intent)
                    finish()
                }

            }
        }
    }

}