package com.eazy.daikou.ui.home.my_property.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.my_property.my_property.PropertyMyUnitModel
import com.eazy.daikou.ui.QRCodeAlertDialog

class PropertyMyUnitAdapter (private val context: Context, private val propertyMyUnitList : ArrayList<PropertyMyUnitModel>, private val callBackClick: CallBackItemClickListener): RecyclerView.Adapter<PropertyMyUnitAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
       val view = LayoutInflater.from(context).inflate(R.layout.custom_layout_view_list_my_unit, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val propertyMyUnit : PropertyMyUnitModel = propertyMyUnitList[position]
        if (propertyMyUnit != null){
            if (propertyMyUnit.type!!.images.size > 0){
                Glide.with(context).load(propertyMyUnit.type!!.images[0].file_path).into(holder.imageMyProperty)
            } else {
                Glide.with(context).load(R.drawable.no_image).into(holder.imageMyProperty)
            }

            Utils.setValueOnText(holder.unitTypeTv, propertyMyUnit.type!!.name)
            Utils.setValueOnText(holder.unitNameTv, propertyMyUnit.name)
            Utils.setValueOnText(holder.floorTv, propertyMyUnit.total_storey)
            Utils.setValueOnText(holder.sizeTv, propertyMyUnit.total_area_sqm)
            Utils.setValueOnText(holder.bedTv, propertyMyUnit.total_bedroom)
            Utils.setValueOnText(holder.addressTv, propertyMyUnit.account_address)
            holder.iconQrCode.setOnClickListener(CustomSetOnClickViewListener{
                if (propertyMyUnit.qr_code != null){
                    val qrCodeImageBitmap = Utils.getQRCodeImage512(propertyMyUnit.qr_code)
                    val qrCodeAlertDialog = QRCodeAlertDialog(
                        context,
                        qrCodeImageBitmap
                    )
                    qrCodeAlertDialog.show()
                }
            })

            holder.mainLayout.setOnClickListener (CustomSetOnClickViewListener { callBackClick.clickItemCallBack(propertyMyUnit) })

        }
    }

    override fun getItemCount(): Int {
        return propertyMyUnitList.size
    }

    class ViewHolder( view: View) : RecyclerView.ViewHolder(view){
        var imageMyProperty: ImageView = view.findViewById(R.id.imageMyProperty)
        var unitTypeTv: TextView = view.findViewById(R.id.unitTypeTv)
        var unitNameTv: TextView = view.findViewById(R.id.unitNameTv)
        var floorTv: TextView = view.findViewById(R.id.floorTv)
        var bedTv: TextView = view.findViewById(R.id.bedTv)
        var sizeTv: TextView = view.findViewById(R.id.sizeTv)
        var addressTv: TextView = view.findViewById(R.id.addressTv)
        var iconQrCode : ImageView = view.findViewById(R.id.iconQrCode)
        var mainLayout : CardView = view.findViewById(R.id.mainLayout)
    }

    interface CallBackItemClickListener{
        fun clickItemCallBack(propertyMyUnit : PropertyMyUnitModel)
    }

    fun clear(){
        val size = propertyMyUnitList.size
        if (size > 0) {
            for (i in 0 until size) {
                propertyMyUnitList.removeAt(0)
            }
            notifyItemRangeRemoved(0, size)
        }
    }
}