package com.eazy.daikou.ui.home.laws;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.eazy.daikou.R;
import com.eazy.daikou.helper.Utils;
import com.eazy.daikou.model.law.LawsDetailModel;
import com.eazy.daikou.ui.home.laws.adapter.LawsDetailAdapter;
import com.eazy.daikou.request_data.request.book_quote_ws.RuleAndNewsWs;
import com.eazy.daikou.base.BaseActivity;

import java.util.ArrayList;
import java.util.List;

public class LawsDetailActivity extends BaseActivity {

    private ProgressBar progressBar;
    private RecyclerView lawsRecyclerView;
    private LinearLayoutManager linearLayoutManager;
    private CardView noItem;
    private LawsDetailAdapter lawsDetailAdapter;
    private SwipeRefreshLayout refreshLayout;

    private final List<LawsDetailModel> lawsDetailList = new ArrayList<>();

    private int currentItem, total, scrollDown, currentPage = 1, size = 20;
    private Boolean isScrolling = true ;
    private String category_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_laws_detail);

        initView();

        // init action
        if (getIntent() != null && getIntent().hasExtra("category_id")){
            category_id = getIntent().getStringExtra("category_id");
        }

        if (getIntent() != null && getIntent().hasExtra("category_name")){
            String titleCategory = getIntent().getStringExtra("category_name");
            Utils.customOnToolbar(this, titleCategory, this::onBackPressed);
        }

        linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        lawsRecyclerView.setLayoutManager(linearLayoutManager);
        initRecyclerView();

        initRecyclerViewScroll();

        refreshLayout = findViewById(R.id.swipe_layouts);
        refreshLayout.setColorSchemeResources(R.color.colorPrimary);
        refreshLayout.setOnRefreshListener(this::refreshList);

    }

    private void initView(){
        progressBar = findViewById(R.id.progressItem);
        lawsRecyclerView = findViewById(R.id.listLawsDetail);
        noItem = findViewById(R.id.noItemListLaws);
    }

    private void refreshList(){
        currentPage = 1;
        size = 20;
        isScrolling = true;
        if (lawsDetailAdapter != null)    lawsDetailAdapter.clear();
        requestService();
    }

    private void initRecyclerView(){
        lawsDetailAdapter = new LawsDetailAdapter(lawsDetailList, clickCallBack);
        lawsRecyclerView.setAdapter(lawsDetailAdapter);

        requestService();
    }

    private void requestService(){
        progressBar.setVisibility(View.VISIBLE);
        new RuleAndNewsWs().getLawsDetail(this, currentPage, 20, category_id, callBackListener);
    }
    private void initRecyclerViewScroll() {
        lawsRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true;
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                currentItem = linearLayoutManager.getChildCount();
                total = linearLayoutManager.getItemCount();
                scrollDown = linearLayoutManager.findFirstVisibleItemPosition();
                if (dy > 0) {
                    if (isScrolling && (currentItem + scrollDown == total)) {
                        if (total == size) {
                            isScrolling = false;
                            currentPage++;
                            progressBar.setVisibility(View.VISIBLE);
                            recyclerView.scrollToPosition(lawsDetailList.size() - 1);
                            initRecyclerView();
                            size += 20;
                        }
                    }
                }

            }
        });
    }
    private final LawsDetailAdapter.LawsClickCallBackListener clickCallBack = lawsDetailModel ->{
        if (lawsDetailModel.getFilePath() != null && !lawsDetailModel.getFilePath().equals("")) {
            if (lawsDetailModel.getFilePath().contains(".pdf")) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(Uri.parse(lawsDetailModel.getFilePath()), "application/pdf");
                try {
                    startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(LawsDetailActivity.this, getResources().getString(R.string.cannot_read_pdf_file), Toast.LENGTH_SHORT).show();
                }
            }
        } else {
            Toast.makeText(LawsDetailActivity.this, getResources().getString(R.string.no_description), Toast.LENGTH_SHORT).show();
        }
    };


    private final RuleAndNewsWs.LawsCallBackListener callBackListener = new RuleAndNewsWs.LawsCallBackListener() {
        @SuppressLint("NotifyDataSetChanged")
        @Override
        public void getLawsDetail(List<LawsDetailModel> lawsModelDetailList) {
            progressBar.setVisibility(View.GONE);
            refreshLayout.setRefreshing(false);
            lawsDetailList.addAll(lawsModelDetailList);
            lawsDetailAdapter.notifyDataSetChanged();

            noItem.setVisibility(lawsDetailList.size() == 0 ? View.VISIBLE: View.GONE);
            lawsRecyclerView.setVisibility(lawsDetailList.size() > 0 ? View.VISIBLE: View.GONE);
        }

        @Override
        public void onFailed(String error) {
            progressBar.setVisibility(View.GONE);
            refreshLayout.setRefreshing(false);
            Utils.customToastMsgError(LawsDetailActivity.this, error, false);
        }
    };

    @Override
    public void onBackPressed() {
        finish();
    }
}