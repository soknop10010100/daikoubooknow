package com.eazy.daikou.ui.profile.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.profile.UnitNoModel
import java.util.*

class SwitchUnitNoAdapter(private val context : Context, private val list : ArrayList<UnitNoModel>, private val onClick : OnClickCallBackListener) :
    RecyclerView.Adapter<SwitchUnitNoAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.select_unit_no_model_unit, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBindingView(context, list[position], onClick)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private var title: TextView = itemView.findViewById(R.id.name_item_hr)
        private var descriptionTv: TextView = itemView.findViewById(R.id.descriptionTv)
        private var cardLayout : CardView = itemView.findViewById(R.id.card_hr)
        private var iconDropDown : ImageView = itemView.findViewById(R.id.iconDropDown)

        fun onBindingView(context: Context, unitNoModel: UnitNoModel, onClick : OnClickCallBackListener){
            Utils.setValueOnText(title, unitNoModel.name)
            Utils.setValueOnText(descriptionTv, unitNoModel.property)
            iconDropDown.setImageResource(R.drawable.ic_tech_black)

            if (unitNoModel.isClick){
                iconDropDown.setColorFilter(Utils.getColor(context, R.color.appBarColorOld))
                iconDropDown.visibility = View.VISIBLE
            } else {
                iconDropDown.setColorFilter(Utils.getColor(context, R.color.gray))
                iconDropDown.visibility = View.INVISIBLE
            }

            cardLayout.setOnClickListener(
                CustomSetOnClickViewListener{
                    onClick.onClickCallBack(unitNoModel)
                }
            )
        }
    }

    interface OnClickCallBackListener{
        fun onClickCallBack(unitNoModel : UnitNoModel)
    }

}