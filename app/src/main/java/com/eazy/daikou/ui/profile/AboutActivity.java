package com.eazy.daikou.ui.profile;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.eazy.daikou.BuildConfig;
import com.eazy.daikou.R;
import com.eazy.daikou.base.BaseActivity;
import com.eazy.daikou.helper.Utils;

public class AboutActivity extends BaseActivity {

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        TextView titleToolBar = findViewById(R.id.titleToolbar);
        titleToolBar.setAllCaps(true);
        titleToolBar.setText(getResources().getString(R.string.about));
        findViewById(R.id.layoutMap).setVisibility(View.INVISIBLE);
        findViewById(R.id.iconBack).setOnClickListener(view -> finish());

        TextView versionTv = findViewById(R.id.version);
        versionTv.setText(getResources().getString(R.string.version)+ " " + BuildConfig.VERSION_NAME);

        findViewById(R.id.txtTermOfService).setOnClickListener(view -> {
            String urlImage = "https://eazy.daikou.asia/api/term_and_service_file_api.pdf";
            openPdfView(urlImage);
        });

        findViewById(R.id.txtPrivacyPolicy).setOnClickListener(view -> {
            String urlImage = "https://eazy.daikou.asia/api/privacy_api_file.pdf";
            openPdfView(urlImage);
        });
    }
    private void openPdfView(String urlImage){
        Utils.openDefaultPdfView(AboutActivity.this, urlImage);
    }

}