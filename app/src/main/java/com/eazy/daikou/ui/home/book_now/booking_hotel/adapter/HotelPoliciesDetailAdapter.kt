package com.eazy.daikou.ui.home.book_now.booking_hotel.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.booking_hotel.PoliciesModel

class HotelPoliciesDetailAdapter(private val action : String, private val historyList : ArrayList<PoliciesModel>) : RecyclerView.Adapter<HotelPoliciesDetailAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.hotel_policies_detail_adapter, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBindingView(action, historyList[position])
    }

    override fun getItemCount(): Int {
        return historyList.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val subjectTv : TextView = itemView.findViewById(R.id.subjectTv)
        private val descriptionTv : TextView = itemView.findViewById(R.id.descriptionTv)
        private val titleTv : TextView = itemView.findViewById(R.id.titleTv)
        private val descTv : TextView = itemView.findViewById(R.id.descTv)
        private val contentTv : TextView = itemView.findViewById(R.id.contentTv)

        private val policiesLayout : LinearLayout = itemView.findViewById(R.id.policiesLayout)
        private val itineraryLayout : LinearLayout = itemView.findViewById(R.id.itineraryLayout)

        fun onBindingView(action : String, itemHistory : PoliciesModel){
            if (itemHistory != null){
                if (action == "policies"){
                    policiesLayout.visibility = View.VISIBLE
                    itineraryLayout.visibility = View.GONE
                    Utils.setValueOnText(subjectTv, itemHistory.title)
                    Utils.setValueOnText(descriptionTv, itemHistory.content)
                } else {
                    policiesLayout.visibility = View.GONE
                    itineraryLayout.visibility = View.VISIBLE
                    Utils.setValueOnText(titleTv, itemHistory.title)
                    Utils.setValueOnText(descTv, itemHistory.desc)
                    Utils.setValueOnText(contentTv, itemHistory.content)
                }
            }
        }
    }
}