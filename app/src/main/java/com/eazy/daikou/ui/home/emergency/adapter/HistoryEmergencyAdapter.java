package com.eazy.daikou.ui.home.emergency.adapter;

import android.content.Context;
import android.content.res.ColorStateList;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.eazy.daikou.R;
import com.eazy.daikou.helper.DateUtil;
import com.eazy.daikou.helper.Utils;
import com.eazy.daikou.model.home.HisEmergencyModel;

import java.util.List;

public class HistoryEmergencyAdapter extends RecyclerView.Adapter<HistoryEmergencyAdapter.ViewHolder> {

    private final Context context;
    private final List<HisEmergencyModel> emergencyList;
    private final ClickBackListener clickBackListener;

    public HistoryEmergencyAdapter(List<HisEmergencyModel> emergencyList, Context context, ClickBackListener clickBackListener) {
        this.emergencyList = emergencyList;
        this.context = context;
        this.clickBackListener = clickBackListener;
    }

    @NonNull
    @Override
    public HistoryEmergencyAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_notification_info, parent,false);
        return new ViewHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(@NonNull HistoryEmergencyAdapter.ViewHolder holder, int position) {
        HisEmergencyModel hisEmergencyModel = emergencyList.get(position);
        if (hisEmergencyModel != null){

            holder.date.setText(DateUtil.formatDateComplaint(hisEmergencyModel.getCreatedDt()));

            holder.time.setText(DateUtil.formatDateToConvertOnlyTimeZoneNoSecond(hisEmergencyModel.getCreatedDt()));

            holder.status.setText(Utils.convertFirstCapital(hisEmergencyModel.getStatus()));
            String statusVal = hisEmergencyModel.getStatus();
            if(statusVal.equalsIgnoreCase("new")){
                holder.status.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(context, R.color.appBarColor)));
                Utils.setBgTint(holder.cardIcon, R.color.yellow);
            } else if(statusVal.equalsIgnoreCase("accept")){
                holder.status.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(context, R.color.green)));
                Utils.setBgTint(holder.cardIcon, R.color.green);
            }else if(statusVal.equalsIgnoreCase("pending")){
                holder.status.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(context, R.color.yellow)));
                Utils.setBgTint(holder.cardIcon, R.color.yellow);
            } else if(statusVal.equalsIgnoreCase("completed")){
                holder.status.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(context, R.color.green)));
                Utils.setBgTint(holder.cardIcon, R.color.green);
            }else if(statusVal.equalsIgnoreCase("reject")){
                holder.status.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(context, R.color.red)));
                Utils.setBgTint(holder.cardIcon, R.color.yellow);
            }else {
                holder.status.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(context, R.color.appBarColor)));
                Utils.setBgTint(holder.cardIcon, R.color.yellow);
            }

            holder.itemView.setOnClickListener(view -> clickBackListener.clickOnItemAcceptOpenMap(hisEmergencyModel.getId()));
        }

    }

    @Override
    public int getItemCount() {
        return emergencyList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        private final TextView date, time, status;
        private final CardView cardIcon;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            status = itemView.findViewById(R.id.status);
            date = itemView.findViewById(R.id.date);
            time = itemView.findViewById(R.id.time);
            cardIcon = itemView.findViewById(R.id.cardIcon);
        }
    }

    public void clear() {
        int size = emergencyList.size ();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                emergencyList.remove ( 0 );
            }
            notifyItemRangeRemoved ( 0, size );
        }
    }

    public interface ClickBackListener {
        void clickOnItemAcceptOpenMap(String id);
    }
}
