package com.eazy.daikou.ui.profile;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.eazy.daikou.R;
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener;
import com.eazy.daikou.request_data.request.profile_ws.PasswordWs;
import com.eazy.daikou.base.BaseActivity;
import com.eazy.daikou.helper.AppAlertCusDialog;
import com.eazy.daikou.helper.CheckIsAppActive;
import com.eazy.daikou.ui.LoginActivity;
import com.eazy.daikou.helper.Utils;
import com.eazy.daikou.ui.home.book_now.booking_account.LoginBookNowActivity;
import com.eazy.daikou.ui.SampleCanvasClassActivity;

import java.util.HashMap;

public class NewPasswordActivity extends BaseActivity {

    private LinearLayout resetPassBtn;
    private EditText resetNewPass, confNewPass;
    private View progressBar;
    private AppAlertCusDialog alertCusDialog;
    private String eazyhotelUserId = "", emailAndPhone = "";

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(isBookNowApp ? R.layout.hotel_reset_new_password : R.layout.activity_new_password);

        initView();

        initData();

        initAction();
    }

    private void initView(){
        resetPassBtn = findViewById(R.id.btn_reset_password);
        resetNewPass = findViewById(R.id.reset_new_password);
        confNewPass = findViewById(R.id.reset_conf_new_password);
        progressBar = findViewById(R.id.progressItem);
        progressBar.setVisibility(View.GONE);

        if (isBookNowApp){
            RelativeLayout fragmentLayout = findViewById(R.id.fragmentLayout);
            fragmentLayout.addView(new SampleCanvasClassActivity( false,this, "#0b4891"));
        } else {
            resetNewPass.setHint(getResources().getString(R.string.please_input_your_new_password));
            confNewPass.setHint(getResources().getString(R.string.please_confirm_your_new_password));
        }

        Utils.customOnToolbar(this, getResources().getString(R.string.reset_password), this::finish);

    }
    private void initData(){
        if (getIntent() != null && getIntent().hasExtra("eazyhotel_user_id")){
            eazyhotelUserId = getIntent().getStringExtra("eazyhotel_user_id");
        }
        if (getIntent() != null && getIntent().hasExtra("email_and_phone")){
            emailAndPhone = getIntent().getStringExtra("email_and_phone");
        }
    }

    private void initAction(){
        alertCusDialog = new AppAlertCusDialog();
        resetPassBtn.setOnClickListener(new CustomSetOnClickViewListener(view -> {
            Utils.hideSoftKeyboard(resetPassBtn);
            if(resetNewPass.getText().length() >= 6 && confNewPass.getText().length() >= 6) {
                updatePassword();
            } else {
                if (resetNewPass.getText().length() == 0){
                    alertCusDialog.showDialog(NewPasswordActivity.this, getResources().getString(R.string.please_input_your_new_password));
                } else if (confNewPass.getText().length() == 0) {
                    alertCusDialog.showDialog(NewPasswordActivity.this, getResources().getString(R.string.please_confirm_your_new_password));
                } else if(!resetNewPass.getText().toString().equals(confNewPass.getText().toString())){
                    alertCusDialog.showDialog(NewPasswordActivity.this, getResources().getString(R.string.password_and_confirm_password_must_the_same));
                } else {
                    alertCusDialog.showDialog(NewPasswordActivity.this, getResources().getString(R.string.password_must_more_than_six_word));
                }
            }
        }));

        setTypeMode(resetNewPass);

        setTypeMode(confNewPass);
    }

    private void setTypeMode(EditText editText){
        editText.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
        editText.setTransformationMethod(PasswordTransformationMethod.getInstance());
    }

    private void updatePassword(){
        progressBar.setVisibility(View.VISIBLE);
        if (isBookNowApp){
            HashMap<String, Object> hashMapNewPass = new HashMap<>() ;

            hashMapNewPass.put("eazyhotel_user_id", eazyhotelUserId);
            hashMapNewPass.put("new_password", resetNewPass.getText().toString().trim());

            progressBar.setVisibility(View.VISIBLE);
            new PasswordWs().resetNewPassword(NewPasswordActivity.this, hashMapNewPass, isBookNowApp, resetNewPassCallBack);
        } else {
            HashMap<String, String> hashMap = new HashMap<>() ;

            hashMap.put("action", "forget");
            hashMap.put("username", emailAndPhone);
            hashMap.put("new_password", resetNewPass.getText().toString().trim());
            hashMap.put("confirm_password", confNewPass.getText().toString().trim());

            new PasswordWs().changeAndForgetPasswordWS(this, hashMap, callbackForgetPassword);
        }
    }

    private final PasswordWs.ResetNewPassCallBack resetNewPassCallBack = new PasswordWs.ResetNewPassCallBack() {
        @Override
        public void onSuccess() {
            progressBar.setVisibility(View.GONE);
            Toast.makeText(NewPasswordActivity.this, getResources().getString(R.string.reset_password_success_full), Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(NewPasswordActivity.this, isBookNowApp ? LoginBookNowActivity.class : LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK| Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }

        @Override
        public void onWrong() {
            alertCusDialog.showDialog(NewPasswordActivity.this, getResources().getString(R.string.new_password_and_confirm_password_must_be_the_same));
            progressBar.setVisibility(View.GONE);
        }

        @Override
        public void onFailed(String mess) {
            alertCusDialog.showDialog(NewPasswordActivity.this, mess);
            progressBar.setVisibility(View.GONE);
        }
    };

    private final PasswordWs.PasswordCallBackListener callbackForgetPassword = new PasswordWs.PasswordCallBackListener() {
        @Override
        public void onSuccess(String msgSuccess) {
            progressBar.setVisibility(View.GONE);

            Intent intent = new Intent(NewPasswordActivity.this, LoginActivity.class);
            startActivity(intent);
            Utils.customToastMsgError(NewPasswordActivity.this, msgSuccess, true);
            setResult(RESULT_OK);
            finish();
        }

        @Override
        public void onWrong(String msg) {
            progressBar.setVisibility(View.GONE);
            Utils.customToastMsgError(NewPasswordActivity.this, msg, true);
        }

        @Override
        public void onFailed(String mess) {
            progressBar.setVisibility(View.GONE);
            Utils.customToastMsgError(NewPasswordActivity.this, mess, true);

        }
    };

}