package com.eazy.daikou.ui.home.book_now.booking_hotel

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.model.booking_hotel.LocationHotelModel
import com.eazy.daikou.model.booking_hotel.NumRoomBookingModel
import com.eazy.daikou.ui.home.book_now.booking_hotel.adapter.SelectLocationAdapter
import com.eazy.daikou.ui.home.book_now.booking_hotel.adapter.SelectNumRoomBookingAdapter
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class SelectLocationBookingFragment : BottomSheetDialogFragment() {

    //Select location
    private lateinit var recyclerView: RecyclerView
    private lateinit var onClickAllAction : OnClickCallBackListener
    private var mContext: Context? = null
    private var selectedItem = ""
    private lateinit var progressBar: ProgressBar
    private var action : String = ""
    private lateinit var titleItem : TextView
    private var locationHotelModelList: ArrayList<LocationHotelModel> = ArrayList()

    //Select room
    private var listNumBookRoom: ArrayList<NumRoomBookingModel> = ArrayList()
    private lateinit var selectNumRoomBookingAdapter : SelectNumRoomBookingAdapter
    private var room = ""
    private var adult = ""
    private var children = ""
    private lateinit var btnSelectGuest : TextView
    private var maxPeople = 0

    fun newInstance(item: String, action : String, locationHotelModel: ArrayList<LocationHotelModel>): SelectLocationBookingFragment {
        val bottomSheetDialogFragment = SelectLocationBookingFragment()
        val bundle = Bundle()
        bundle.putString("order_id", item)
        bundle.putString("action", action)
        bundle.putSerializable("location_list", locationHotelModel)
        bottomSheetDialogFragment.arguments = bundle
        return bottomSheetDialogFragment
    }

    fun newInstance(room: String, adult: String, children: String, action : String): SelectLocationBookingFragment {
        val bottomSheetDialogFragment = SelectLocationBookingFragment()
        val bundle = Bundle()
        bundle.putString("room", room)
        bundle.putString("adult", adult)
        bundle.putString("children", children)
        bundle.putString("action", action)
        bottomSheetDialogFragment.arguments = bundle
        return bottomSheetDialogFragment
    }

    // For space
    fun newInstance(room: String, adult: String, children: String, maxPeople : Int, action : String): SelectLocationBookingFragment {
        val bottomSheetDialogFragment = SelectLocationBookingFragment()
        val bundle = Bundle()
        bundle.putString("room", room)
        bundle.putString("adult", adult)
        bundle.putString("children", children)
        bundle.putString("action", action)
        bundle.putInt("max_people", maxPeople)
        bottomSheetDialogFragment.arguments = bundle
        return bottomSheetDialogFragment
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // setStyle(STYLE_NO_FRAME, R.style.AlertTheme)
        if (arguments != null && requireArguments().containsKey("order_id")) {
            selectedItem = requireArguments().getString("order_id").toString()
        }
        if (arguments != null && requireArguments().containsKey("action")) {
            action = requireArguments().getString("action").toString()
        }
        if (arguments != null && requireArguments().containsKey("location_list")) {
            locationHotelModelList = requireArguments().getSerializable("location_list") as ArrayList<LocationHotelModel>
        }
        if (arguments != null && requireArguments().containsKey("room")) {
            room = requireArguments().getString("room").toString()
        }
        if (arguments != null && requireArguments().containsKey("adult")) {
            adult = requireArguments().getString("adult").toString()
        }
        if (arguments != null && requireArguments().containsKey("children")) {
            children = requireArguments().getString("children").toString()
        }
        if (arguments != null && requireArguments().containsKey("max_people")) {
            maxPeople = requireArguments().getInt("max_people")
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view =  inflater.inflate(R.layout.fragment_select_location_booking, container, false)
        //init view
        setStyle(STYLE_NO_FRAME, R.style.AppBottomSheetDialogTheme)
        recyclerView = view.findViewById(R.id.recyclerView)
        progressBar = view.findViewById(R.id.progressItem)
        progressBar.visibility = View.GONE
        titleItem = view.findViewById(R.id.titleItem)
        btnSelectGuest = view.findViewById(R.id.btnSelect)

        //init action
        initAction()

        return view
    }

    private fun initAction(){
        recyclerView.layoutManager = LinearLayoutManager(mContext)
        if (action == "select_location") {      //select location from one 1 activity
            titleItem.text = mContext?.resources!!.getString(R.string.location)
            recyclerView.adapter = SelectLocationAdapter(selectedItem, locationHotelModelList,
                object : SelectLocationAdapter.OnClickCallBackLister {
                    override fun onClickCallBack(value: LocationHotelModel) {
                        onClickAllAction.onClickSelectLocation(value)
                        dismiss()
                    }

                })
        } else {                            //select Guests from one 1 activity
            titleItem.text = mContext?.resources!!.getString(R.string.guest)
            btnSelectGuest.visibility = View.VISIBLE
            selectNumRoomBookingAdapter = SelectNumRoomBookingAdapter(mContext!!, addListNumRoomDefault(), onClickCallBack)
            recyclerView.adapter = selectNumRoomBookingAdapter

            btnSelectGuest.setOnClickListener(
                CustomSetOnClickViewListener{
                    setBackItemRoom()
                    dismiss()
                }
            )

            // Space
            if (maxPeople > 0) titleItem.append(String.format("%s %s %s %s", " (", mContext!!.getString(R.string.max_people), maxPeople, ")"))
        }
    }

    private fun setBackItemRoom(){
        var getValueGuest = ""
        var room = ""
        var adults = ""
        var children = ""
        for (item in listNumBookRoom){
            getValueGuest += item.numValues + " " + item.action + if (item.action == "children") "" else " ∙ "
            when (item.action) {
                "room" -> {
                    room = item.numValues!!
                }
                "adults" -> {
                    adults = item.numValues!!
                }
                else -> {
                    children = item.numValues!!
                }
            }
        }
        onClickAllAction.onClickSelect(getValueGuest, room, adults, children)
    }

    fun initListener(onClickAllAction: OnClickCallBackListener) {
        this.onClickAllAction = onClickAllAction
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
    }

    interface OnClickCallBackListener {
        fun onClickSelectLocation(titleHeader : LocationHotelModel)
        fun onClickSelect(titleHeader : String, r : String, ad : String, ch : String)
    }

    // ============== Select Action Num Room ===========================
    private fun addListNumRoomDefault(): ArrayList<NumRoomBookingModel> {
        listNumBookRoom = ArrayList()
        if (room != "no_room_check")    listNumBookRoom.add(NumRoomBookingModel(if(room != "") room else "1", "room"))  //no add when not search room
        listNumBookRoom.add(NumRoomBookingModel(if(adult != "") adult else "1", "adults"))
        listNumBookRoom.add(NumRoomBookingModel(if(children != "") children else "0", "children"))
        return listNumBookRoom
    }

    private var countRoom = 0

    private val onClickCallBack = object : SelectNumRoomBookingAdapter.OnClickCallBackLister{
        @SuppressLint("NotifyDataSetChanged")
        override fun onClickCallBack(value: NumRoomBookingModel, actionBtn : String) {
            if (maxPeople > 0 && actionBtn == "add"){
                if (maxPeople == validatePeople()) return
            }
            countRoom = value.numValues!!.toInt()
            if (actionBtn == "add"){
                countRoom = countRoom.plus(1)
            } else {
                if (countRoom > 0) {
                    countRoom = countRoom.minus(1)
                }
            }
            value.numValues = countRoom.toString()
            selectNumRoomBookingAdapter.notifyDataSetChanged()
        }

    }

    private fun validatePeople() : Int {
        var max = 0
        for (item in listNumBookRoom){
            max += item.numValues!!.toInt()
        }
        return max
    }

}