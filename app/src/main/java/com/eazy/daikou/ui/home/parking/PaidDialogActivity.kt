package com.eazy.daikou.ui.home.parking

import android.annotation.SuppressLint
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.request_data.request.parking_ws.PaymentParkingWs
import com.eazy.daikou.model.parking.CheckInOutPrinter
import com.eazy.daikou.ui.home.parking.printer.SunmiPrintHelper
import java.util.HashMap

class PaidDialogActivity : BaseActivity() {

    private lateinit var returnAmountTv :TextView
    private lateinit var amountTv : TextView
    private lateinit var receiveAmountEdt: EditText
    private lateinit var btnSubmit : TextView
    private lateinit var btnCancel : TextView
    private lateinit var btnUsd : TextView
    private lateinit var btnRiel : TextView
    private lateinit var loading : ProgressBar
    private lateinit var checkOutPrint : CheckInOutPrinter

    private var parkingFee = "0.0"
    private var currency : String = ""
    private var priceReturn = 0.00
    private var receiveAmount = 0.00
    private var parkingListId = ""
    private var userId = ""
    private var type = ""
    private var amountParking = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_paid_dailog)

        initView()

        initData()

        initAction()
    }

    private fun initView(){
        receiveAmountEdt = findViewById(R.id.receive_amount)
        returnAmountTv = findViewById(R.id.text_return_amount)
        amountTv = findViewById(R.id.text_amount)
        btnCancel = findViewById(R.id.btn_cancel)
        btnSubmit = findViewById(R.id.btn_submit)
        btnUsd = findViewById(R.id.btn_usd)
        btnRiel = findViewById(R.id.btn_riel)
        loading = findViewById(R.id.loading)

        actionClick("usd")

    }
    @SuppressLint("SetTextI18n")
    private fun initData(){
        parkingFee = if(intent!=null && intent.hasExtra("parking_fee")){
            intent.getStringExtra("parking_fee") as String
        }else{
            "0.0"
        }
        if(intent.hasExtra("user_id")){
            userId = intent.getStringExtra("user_id") as String
        }
        if(intent.hasExtra("parking_id")){
            parkingListId = intent.getStringExtra("parking_id") as String
        }

        if(intent.hasExtra("check_out")){
            checkOutPrint = intent.getSerializableExtra("check_out") as CheckInOutPrinter
        }
        if(intent.hasExtra("type")){
            type = intent.getStringExtra("type") as String
        }

        if(parkingFee.isNotEmpty()){
            if(currency == "$"){
                amountTv.text = "$currency $parkingFee"
                amountParking = parkingFee
            }else{
                amountTv.text = currency + parkingFee.toDouble()*4100
                amountParking = (parkingFee.toDouble()*4100).toString()
            }
        }

        receiveAmountEdt.hint = "$currency $receiveAmount"
        returnAmountTv.text = "$currency $priceReturn"

    }

    private fun initAction(){
        setVisibleButtonSubmit()

        receiveAmountEdt.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            @SuppressLint("SetTextI18n")
            override fun afterTextChanged(editable: Editable) {
                setVisibleButtonSubmit()
                if (editable.toString().isNotEmpty()) {
                    receiveAmount = editable.toString().toDouble()
                    priceReturn = if(currency == "$"){
                        if(receiveAmount > parkingFee.toDouble()){
                            receiveAmount - parkingFee.toDouble()
                        }else{
                            0.00
                        }
                    }else{
                        if(receiveAmount > parkingFee.toDouble()*4100){
                            receiveAmount - parkingFee.toDouble()*4100
                        }else{
                            0.00
                        }
                    }

                   returnAmountTv.text = "$currency $priceReturn"
                } else {
                    // returnAmountTv.text = "$currency $priceReturn"
                    returnAmountTv.text = "$currency $0.0"
                }
            }
        })

        btnCancel.setOnClickListener { finish() }

        btnSubmit.setOnClickListener {
            if(receiveAmountEdt.text.toString().isNotEmpty()){
                loading.visibility = View.VISIBLE
                acceptPaymentForEmployee()
            }else{
                loading.visibility = View.GONE
                Toast.makeText(this@PaidDialogActivity, getString(R.string.please_input_receip_amount), Toast.LENGTH_LONG).show()
            }

        }

        btnUsd.setOnClickListener { actionClick("usd") }
        btnRiel.setOnClickListener {actionClick("riel") }

    }

    private fun actionClick(type: String) {
        if (type == "usd") {
            btnUsd.setTextColor(Color.WHITE)
            btnUsd.background = ResourcesCompat.getDrawable(resources, R.drawable.custom_card_view_color_menu, null)
            btnRiel.setTextColor(Color.GRAY)
            btnRiel.background = null
            currency = "$"
            initData()
        } else if (type == "riel") {
            btnRiel.setTextColor(Color.WHITE)
            btnRiel.background = ResourcesCompat.getDrawable(resources, R.drawable.custom_card_view_color_menu, null)
            btnUsd.setTextColor(Color.GRAY)
            btnUsd.background = null
            currency = "R"
            initData()
        }
    }
    private fun acceptPaymentForEmployee() {
        val hashMap = HashMap<String, Any>()
        hashMap["parking_list_id"] = parkingListId
        hashMap["receive_amount"] = receiveAmount
        hashMap["return_amount"] = priceReturn
        hashMap["user_id"] = userId
        PaymentParkingWs().acceptPaymentPayByCash(this, hashMap, onRequestPaymentCallBack)
    }

    private val  onRequestPaymentCallBack = object : PaymentParkingWs.OnRequestPaymentCallBack{
        override fun onLoadSuccess(linkKESSPay: String?) {
            if(type == "printer"){
                printLayout()
            }
            val intent = Intent()
            setResult(RESULT_OK, intent)
            finish()
            Toast.makeText(this@PaidDialogActivity, linkKESSPay, Toast.LENGTH_LONG).show()
            loading.visibility = View.GONE
        }

        override fun onLoadFail(message: String?) {
            loading.visibility = View.GONE
            Toast.makeText(this@PaidDialogActivity, message, Toast.LENGTH_LONG).show()
        }

    }

    @SuppressLint("SetTextI18n")
    private fun printLayout() {
      SunmiPrintHelper.getInstance().printReceipt(this,checkOutPrint,currency,amountParking,receiveAmount.toString(),priceReturn.toString(),"Cash")
    }

    //Set action button upload
    private fun setVisibleButtonSubmit(){
        btnSubmit.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(this, if (isEnableButton()) R.color.appBarColor else R.color.gray))
        btnSubmit.isEnabled = isEnableButton()
    }

    private fun isEnableButton() : Boolean{
        if (receiveAmountEdt.text .toString().isNotEmpty()){
            return true
        }
        return false
    }

}