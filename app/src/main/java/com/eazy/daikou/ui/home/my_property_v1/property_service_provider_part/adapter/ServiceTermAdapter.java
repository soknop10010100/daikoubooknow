package com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.eazy.daikou.R;

import java.util.ArrayList;

public class ServiceTermAdapter extends RecyclerView.Adapter<ServiceTermAdapter.ViewHolder> {

    private final ArrayList<String> menuServiceProviders;
    private final ItemClickOnService itemClick;
    private final Context context;
    private int mSelectedPos = -1;
    private int stat = -1;
    private String type = "";

    public ServiceTermAdapter(ArrayList<String> menuServiceProviders,String type, int post, Context context, ItemClickOnService itemClick) {
        this.menuServiceProviders = menuServiceProviders;
        this.itemClick = itemClick;
        this.context = context;
        this.stat = post;
        this.type = type;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view  = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_adapter_pay,parent,false);
        return new ViewHolder(view);
    }

    @SuppressLint("NotifyDataSetChanged")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        String listService = menuServiceProviders.get(position);
        if(listService != null){
            holder.nameCategoryTv.setText(listService);

            if(!type.equalsIgnoreCase("weekly") && !type.equalsIgnoreCase("monthly")){
                holder.itemView.setOnClickListener(view -> {
                    mSelectedPos = position;
                    stat = mSelectedPos;
                    itemClick.onClickItem(listService,type,position,stat);
                    notifyDataSetChanged();
                });

                if(type.equals("all")){
                    holder.nameCategoryTv.setTextColor(Color.WHITE);
                    holder.linearLayout.setBackgroundResource(R.color.appBarColorOld);
                } else {
                    if (position == mSelectedPos || position == stat){
                        holder.nameCategoryTv.setTextColor(Color.WHITE);
                        holder.linearLayout.setBackgroundResource(R.color.appBarColorOld);
                    } else {
                        holder.linearLayout.setBackgroundResource(R.color.color_gray_item);
                        holder.nameCategoryTv.setTextColor(Color.GRAY);
                    }
                }

            }

        }

    }

    @Override
    public int getItemCount() {
        return menuServiceProviders.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView nameCategoryTv;
        private final LinearLayout linearLayout;
        public ViewHolder(View view) {
            super(view);
            linearLayout = view.findViewById(R.id.linear_card);
            nameCategoryTv = view.findViewById(R.id.text_category);
        }


    }
    public interface ItemClickOnService{
        void onClickItem(String listString,String type,int position,int stat);
    }
}