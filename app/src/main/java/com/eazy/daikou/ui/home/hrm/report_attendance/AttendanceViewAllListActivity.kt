package com.eazy.daikou.ui.home.hrm.report_attendance

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.request_data.request.hr_ws.MyAttendanceWs
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.hr.ItemAttendanceModel
import com.eazy.daikou.ui.home.hrm.report_attendance.adapter.AttendanceViewAllAdapter
import com.google.android.material.bottomsheet.BottomSheetDialog
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class AttendanceViewAllListActivity : BaseActivity() {

    private lateinit var progressBar: ProgressBar
    private lateinit var refreshLayout: SwipeRefreshLayout
    private lateinit var titleBar : TextView
    private lateinit var iconBack : ImageView
    private lateinit var dateMonthTv : TextView
    private lateinit var recyclerView: RecyclerView
    private lateinit var noDataForDisplay : TextView
    private lateinit var btnSelectDate : LinearLayout
    private lateinit var attendanceAdapter : AttendanceViewAllAdapter
    private lateinit var linearLayoutManager : LinearLayoutManager
    private var listAttendanceViewAll : ArrayList<ItemAttendanceModel> = ArrayList()

    private var currentItem = 0
    private var total : Int = 0
    private var scrollDown : Int = 0
    private var currentPage : Int = 1
    private var size : Int = 10
    private var isScrolling = false
    private var isScrollItem = false

    private var REQUEST_CODE_DETAIL = 579

    private var monthService: String = ""
    private lateinit var selectedCalendar : Calendar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_attendance_view_all_list)

        initView()

        initAction()
    }

    private fun initView(){
        progressBar = findViewById(R.id.progressItem)
        refreshLayout = findViewById(R.id.swipe_layouts)
        titleBar = findViewById(R.id.titleToolbar)
        iconBack = findViewById(R.id.iconBack)
        dateMonthTv = findViewById(R.id.dateMonthTv)
        btnSelectDate = findViewById(R.id.selectDateLinear)
        noDataForDisplay = findViewById(R.id.noDataForDisplay)
        recyclerView = findViewById(R.id.recyclerALlAttendance)

    }

    private fun initAction(){
        iconBack.setOnClickListener { finish() }
        titleBar.text = getString(R.string.employee_attendance_list)

        openCalender()

        linearLayoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = linearLayoutManager
        attendanceAdapter =
            AttendanceViewAllAdapter(this, listAttendanceViewAll, callBackAttendanceViewAll)
        recyclerView.adapter = attendanceAdapter

        requestListAllApi()

        initOnScrollItem(recyclerView)

        refreshLayout.setColorSchemeResources(R.color.colorPrimary)
        refreshLayout.setOnRefreshListener { this.onRefreshList() }


    }

    private fun openCalender(){

        val calendar = Calendar.getInstance()
        val formattedDate = SimpleDateFormat("yyyy-MM-dd")
        val monthFormat = SimpleDateFormat("dd-MMM-yyyy")
        selectedCalendar = calendar
        monthService = formattedDate.format(calendar.time)
        dateMonthTv.text = monthFormat.format(calendar.time)

        btnSelectDate.setOnClickListener {
            val dialog = BottomSheetDialog(this)
            val view = layoutInflater.inflate(R.layout.activity_calendar_view_dialog, null)
            val btnClose = view.findViewById<ImageView>(R.id.btnClose)
            val txtTitle = view.findViewById<TextView>(R.id.txtTitle)

            txtTitle.text = getString(R.string.select_date)
            btnClose.visibility = View.GONE
            // dialog.setCancelable(false)
            dialog.setContentView(view)
            dialog.show()

            val calendarViewAlert: CalendarView = dialog.findViewById(R.id.date_picker)!!

            // Set Select Date
            if (selectedCalendar != null) calendarViewAlert.setDate(
                selectedCalendar.timeInMillis,
                true,
                true
            )
            calendarViewAlert.setOnDateChangeListener { _: CalendarView, year: Int, month: Int, dayOfMonth: Int ->
                val checkCalendar = Calendar.getInstance()
                checkCalendar[year, month] = dayOfMonth
                calendar[year, month] = dayOfMonth

                monthService = formattedDate.format(calendar.time)
                dateMonthTv.text = monthFormat.format(calendar.time)

                onRefreshList()
                dialog.dismiss()
            }
        }
    }

    private fun onRefreshList(){
        currentPage = 1
        size = 10
        isScrolling = true
        attendanceAdapter.clear()
        requestListAllApi()
    }

    private fun initOnScrollItem(recyclerViewItem: RecyclerView) {
        recyclerViewItem.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true
                }
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                currentItem = linearLayoutManager.childCount
                total = linearLayoutManager.itemCount
                scrollDown = linearLayoutManager.findFirstVisibleItemPosition()
                if (dy > 0) {
                    if (isScrolling && currentItem + scrollDown == total) {
                        if (total == size) {
                            isScrolling = false
                            currentPage++
                            progressBar.visibility = View.VISIBLE
                            isScrollItem = true
                            recyclerViewItem.scrollToPosition(listAttendanceViewAll.size - 1)
                            requestListAllApi()
                            size += 10
                        } else {
                            progressBar.visibility = View.GONE
                        }
                    }
                }
            }
        })
    }

    private val callBackAttendanceViewAll : AttendanceViewAllAdapter.CallBackAttendanceViewAll = object :AttendanceViewAllAdapter.CallBackAttendanceViewAll{
        override fun callBackAttendanceAll(attendanceModel: ItemAttendanceModel) {
            val intent = Intent(this@AttendanceViewAllListActivity, AttendanceReportHrDetailActivity::class.java)
            intent.putExtra("item_attendance", attendanceModel)
            intent.putExtra("action_type", attendanceModel.attendance_type)
            startActivityForResult(intent, REQUEST_CODE_DETAIL)
        }

    }

    private fun requestListAllApi(){
        progressBar.visibility = View.VISIBLE
        MyAttendanceWs().getAttendanceViewAllListWs(this,currentPage,10, monthService, callBackListAllListener)
    }

    private val callBackListAllListener : MyAttendanceWs.OnCallBackListAllListener = object : MyAttendanceWs.OnCallBackListAllListener{
        override fun onSuccessFul(listAll: ArrayList<ItemAttendanceModel>) {
            progressBar.visibility = View.GONE
            refreshLayout.isRefreshing = false
            listAttendanceViewAll.addAll(listAll)

            if (listAll.size > 0){
                noDataForDisplay.visibility = View.GONE
            } else {
                noDataForDisplay.visibility = View.VISIBLE
            }

            attendanceAdapter.notifyDataSetChanged()

        }

        override fun onFailed(error: String) {
            progressBar.visibility = View.GONE
            refreshLayout.isRefreshing = false

            Utils.customToastMsgError(this@AttendanceViewAllListActivity, error, false)
        }

    }
}