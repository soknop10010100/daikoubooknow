package com.eazy.daikou.ui.home.service_provider

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.request_data.request.service_provider_ws.ServiceProviderV2Ws
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.helper.web.WebsiteActivity
import com.eazy.daikou.model.my_property.service_provider.DetailServiceProviderModel
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions


class ServiceProviderListDetailActivity : BaseActivity(), OnMapReadyCallback {

    private lateinit var progressBar: ProgressBar
    private lateinit var btnBack: ImageView
    private lateinit var imgLogo: ImageView
    private lateinit var titleServiceTv: TextView
    private lateinit var addressProviderTv: TextView
    private lateinit var websiteProviderTv: TextView
    private lateinit var phoneNumberTv: TextView

    private lateinit var btnWebsiteLinear: LinearLayout
    private lateinit var btnPhoneNumberLinear: LinearLayout
    private lateinit var mMap: GoogleMap
    private  var detailModel: DetailServiceProviderModel = DetailServiceProviderModel()
    private var latNumber: Double = 0.0
    private var longNumber: Double = 0.0
    private var idListDetail = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_service_provider_details_v2)

        initView()

        initData()

        initAction()

    }

    private fun initView(){
        btnBack = findViewById(R.id.btnBack)
        imgLogo = findViewById(R.id.ImageProfile)
        titleServiceTv = findViewById(R.id.ServiceTitleTv)
        addressProviderTv = findViewById(R.id.addressProviderTv)
        websiteProviderTv = findViewById(R.id.websiteProviderTv)
        phoneNumberTv = findViewById(R.id.phoneNumberTv)
        btnWebsiteLinear = findViewById(R.id.btnWebsiteLinear)
        btnPhoneNumberLinear = findViewById(R.id.btnCallLinear)
        progressBar = findViewById(R.id.progressItem)
    }
    private fun initData(){
       if (intent != null && intent.hasExtra("id_detail")){
           idListDetail = intent.getStringExtra("id_detail") as String
       }
    }
    private fun initAction(){
        btnBack.setOnClickListener { finish() }

        requestListApiDetail()

        btnWebsiteLinear.setOnClickListener {
            if (detailModel.website != null){
                val intent = Intent(this@ServiceProviderListDetailActivity, WebsiteActivity::class.java)
                intent.putExtra("linkUrlNews", detailModel.website)
                startActivity(intent)
            }
        }

        btnPhoneNumberLinear.setOnClickListener{
            val dialIntent = Intent(Intent.ACTION_DIAL)
            dialIntent.data = Uri.parse("tel:" + detailModel.phone_number)
            startActivity(dialIntent)
        }
    }

    private fun getDataForView(){
        if (detailModel != null){
            Glide.with(this).load(detailModel.logo).into(imgLogo)
            titleServiceTv.text = if (detailModel.name != null) detailModel.name else ("- - -")
            addressProviderTv.text = if (detailModel.address != null) detailModel.address else ("- - -")
            websiteProviderTv.text = if (detailModel.website != null) detailModel.website else ("- - -")
            phoneNumberTv.text = if (detailModel.phone_number != null) detailModel.phone_number else ("- - -")
        }

        mapLocation()
    }

    private fun  requestListApiDetail(){
        progressBar.visibility = View.VISIBLE
        ServiceProviderV2Ws().getListDetailServiceProviderWs(this, idListDetail, callBackDetail)
    }
    private val callBackDetail: ServiceProviderV2Ws.OnCallBackListDetail = object : ServiceProviderV2Ws.OnCallBackListDetail{
        override fun onLoadSuccessFull(detailServiceProvider: DetailServiceProviderModel) {
            progressBar.visibility = View.GONE
            detailModel = detailServiceProvider

            getDataForView()
        }

        override fun onLoadFailed(message: String) {
            progressBar.visibility = View.GONE
           Utils.customToastMsgError(this@ServiceProviderListDetailActivity, message, false)
        }

    }

    private fun  mapLocation(){
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        try {
            latNumber = detailModel.coord_lat!!.toDouble()
            longNumber = detailModel.coord_long!!.toDouble()

           val location = LatLng(latNumber, longNumber)
            mMap.addMarker(MarkerOptions().position(location).draggable(true).title(detailModel.name))
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 14.0f))
        }catch (e: Exception){
            Utils.customToastMsgError(this, "Wrong number latt and long map", false)
        }
        mMap.isMyLocationEnabled = true
        mMap.uiSettings.isZoomControlsEnabled = false
        mMap.uiSettings.isMyLocationButtonEnabled = true
        mMap.uiSettings.isCompassEnabled = false //hide compass
    }
}