package com.eazy.daikou.ui.home.facility_booking.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.model.facility_booking.CardItemByProperty
import com.eazy.daikou.model.facility_booking.ItemCardBookingModel
import de.hdodenhof.circleimageview.CircleImageView

class CategoryItemCardBookingAdapter(private val context : Context, private val listItem : ArrayList<ItemCardBookingModel>, private val callBackListener : ClickCallBackListener) : RecyclerView.Adapter<CategoryItemCardBookingAdapter.ItemViewHolder>() {
    private lateinit var itemCardBookingAdapter : ItemCardBookingAdapter

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        return ItemViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.category_item_my_card, parent, false))
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val itemCardBooking = listItem[position]
        if (itemCardBooking != null){
            holder.propertyNameTv.text = if (itemCardBooking.property_name != null) itemCardBooking.property_name else "- - -"
            Glide.with(holder.imgProperty).load(if (itemCardBooking.property_logo != null) itemCardBooking.property_logo else R.drawable.ic_my_profile).into(holder.imgProperty)

            if (itemCardBooking.isClick){
                holder.iconSelect.background = ResourcesCompat.getDrawable(holder.iconSelect.resources, R.drawable.circle_appbar, null)
            } else {
                holder.iconSelect.background = ResourcesCompat.getDrawable(holder.iconSelect.resources, R.drawable.bg_circle_camera, null)
            }

            val linearLayoutManager = LinearLayoutManager(context)
            holder.categoryRecycler.layoutManager = linearLayoutManager
            itemCardBookingAdapter = ItemCardBookingAdapter(itemCardBooking.cart_items, object : ItemCardBookingAdapter.ClickCallBackListener{
                override fun onClickCallBack(itemBooking: CardItemByProperty, action: String, iconAction : ImageView) {
                    callBackListener.onClickCallBack(itemCardBooking, itemBooking, action, itemCardBookingAdapter, iconAction)
                }

            })
            holder.categoryRecycler.adapter = itemCardBookingAdapter

            holder.iconSelect.setOnClickListener{callBackListener.onClickMainCallBack(itemCardBooking)}
        }
    }

    override fun getItemCount(): Int {
        return listItem.size
    }

    class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val propertyNameTv: TextView = itemView.findViewById(R.id.propertyNameTv)
        val categoryRecycler: RecyclerView = itemView.findViewById(R.id.categoryRecycler)
        val imgProperty : CircleImageView = itemView.findViewById(R.id.imgProperty)
        val iconSelect : ImageView = itemView.findViewById(R.id.iconSelect)
    }


    interface ClickCallBackListener{
        fun onClickCallBack(itemMainCategory: ItemCardBookingModel, itemBooking: CardItemByProperty, action: String, subItemCardBookingAdapter : ItemCardBookingAdapter, iconAction : ImageView)
        fun onClickMainCallBack(itemBooking: ItemCardBookingModel)
    }

    fun clear(){
        val size = listItem.size
        if (size > 0) {
            for (i in 0 until size) {
                listItem.removeAt(0)
            }
            notifyItemRangeRemoved(0, size)
        }
    }
}