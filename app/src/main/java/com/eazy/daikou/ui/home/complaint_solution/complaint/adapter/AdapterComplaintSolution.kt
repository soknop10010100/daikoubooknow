package com.eazy.daikou.ui.home.complaint_solution.complaint.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.helper.DateUtil
import com.eazy.daikou.model.complaint.ComplaintListModel

class AdapterComplaintSolution(private val listComplaint: ArrayList<ComplaintListModel>, private val context: Context, private val onItemClick: OnItemClick):
    RecyclerView.Adapter<AdapterComplaintSolution.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.custom_item_complain_solution, parent, false)
        return  ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val complaint : ComplaintListModel = listComplaint[position]
        if (complaint != null){
            holder.noTv.text = if (complaint.id != null) complaint.id else ". . ."
            holder.statusTv.text = if (complaint.status != null) complaint.status else ". . ."
            holder.subjectTv.text = if (complaint.subject != null) complaint.subject else ". . ."
            if (complaint.created_at != null){
                holder.createDateTv.text =  DateUtil.formatDateComplaint(complaint.created_at)
                holder.createDateTv.append(String.format("%s %s", " ,", DateUtil.formatDateToConvertOnlyTimeZoneNoSecond(complaint.created_at)))
            } else {
                holder.createDateTv.text =  ". . ."
            }
            holder.propertyNameTv.text = if (complaint.account!!.name != null ) complaint.account!!.name else ". . ."
            holder.typeTv.text = if (complaint.object_type != null ) complaint.object_type else ". . ."

            if (complaint.unit != null){
                holder.unitNameTv.text =  if (complaint.unit!!.name != null) complaint.unit!!.name else ". . ."
            } else {
                holder.unitNameTv.text =  ". . ."
            }

            when {
                complaint.status.equals("new",true) -> {
                    holder.statusTv.backgroundTintList = context.getColorStateList(R.color.appBarColorOld)
                }

                complaint.status.equals("resolved",true) -> {
                    holder.statusTv.backgroundTintList = context.getColorStateList(R.color.green)
                }

                else -> {
                    holder.statusTv.backgroundTintList = context.getColorStateList(R.color.red)
                }
            }

                holder.layoutItem.setOnClickListener (CustomSetOnClickViewListener{
                    onItemClick.clickItem(complaint)
                })

            }
        }

    override fun getItemCount(): Int {
        return listComplaint.size
    }

    inner class ViewHolder(itemView:View):RecyclerView.ViewHolder(itemView){
        val noTv: TextView = itemView.findViewById(R.id.idNoTv)
        val subjectTv: TextView = itemView.findViewById(R.id.subjectTv)
        val createDateTv: TextView = itemView.findViewById(R.id.createdDateTv)
        val propertyNameTv: TextView = itemView.findViewById(R.id.propertyNameTv)
        val unitNameTv : TextView  = itemView.findViewById(R.id.unitNameTv)
        val typeTv: TextView = itemView.findViewById(R.id.typeTv)
        val statusTv: TextView = itemView.findViewById(R.id.statusTv)
        val layoutItem: View = itemView.findViewById(R.id.layout_item)
    }
    
    interface OnItemClick{
        fun clickItem(complaintModel : ComplaintListModel)
    }

    fun clear() {
        val size: Int = listComplaint.size
        if (size > 0) {
            for (i in 0 until size) {
                listComplaint.removeAt(0)
            }
            notifyItemRangeRemoved(0, size)
        }
    }
}