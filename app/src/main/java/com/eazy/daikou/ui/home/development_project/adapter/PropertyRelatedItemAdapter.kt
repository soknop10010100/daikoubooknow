package com.eazy.daikou.ui.home.development_project.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.model.home_page.home_project.RelatedBranches
import com.squareup.picasso.Picasso

class PropertyRelatedItemAdapter(
    private val listPropertyNearBy: List<RelatedBranches>,
    private val clickListener: OnClickProperty
) :
    RecyclerView.Adapter<PropertyRelatedItemAdapter.ViewHolder>() {

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val imageBuilding: ImageView = itemView.findViewById(R.id.img_property)
        val textNameBuilding: TextView = itemView.findViewById(R.id.text_name_building)
        val textAddress: TextView = itemView.findViewById(R.id.text_address)
        val textAddressCity: TextView = itemView.findViewById(R.id.text_address_city)
        val btnProperty: LinearLayout = itemView.findViewById(R.id.btn_property)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.custom_item_property_featuring, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.textNameBuilding.text = listPropertyNearBy[position].name

        // set address
        if (listPropertyNearBy[position].address != null && listPropertyNearBy[position].address != "") {
            holder.textAddress.text = listPropertyNearBy[position].address
        } else {
            holder.textAddress.text = ". . ."
        }

        //set city
        if (listPropertyNearBy[position].city != null && listPropertyNearBy[position].city != "") {
            holder.textAddressCity.text = listPropertyNearBy[position].city
        } else {
            holder.textAddressCity.text = ". . ."
        }

        //set image
        if (listPropertyNearBy[position].image != null && listPropertyNearBy[position].image != "") {
            Picasso.get().load(listPropertyNearBy[position].image).placeholder(R.drawable.no_image)
                .into(holder.imageBuilding)
        } else {
            holder.imageBuilding.setImageResource(R.drawable.no_image)
        }

        holder.btnProperty.setOnClickListener {
            clickListener.click(listPropertyNearBy[position])
        }

    }

    override fun getItemCount(): Int {
        return listPropertyNearBy.size
    }

    interface OnClickProperty {
        fun click(relatedBranche: RelatedBranches)
    }
}