package com.eazy.daikou.ui.home.book_now.booking_hotel

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentContainerView
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.base.MockUpData
import com.eazy.daikou.request_data.request.book_now_ws.BookingHotelWS
import com.eazy.daikou.request_data.request.book_now_ws.RequestHashMapData
import com.eazy.daikou.helper.AppAlertCusDialog
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.booking_hotel.HotelBookingDetailModel
import com.eazy.daikou.model.booking_hotel.LocationHotelModel
import com.eazy.daikou.model.home.MainItemHomeModel
import com.eazy.daikou.model.home.SubItemHomeModel
import com.eazy.daikou.model.home.UserDeactivated
import com.eazy.daikou.ui.home.book_now.booking_hotel.adapter.BookingHomeItemAdapter
import com.eazy.daikou.ui.home.my_property_v1.EasyDialogClass
import com.michael.easydialog.EasyDialog

class HotelSearchShowHotelBookingActivity : BaseActivity() {

    private lateinit var progressBar: ProgressBar
    private lateinit var recyclerView: RecyclerView
    // Search Hotel
    private lateinit var selectLocationTv : TextView
    private lateinit var selectRoomTv : TextView
    private lateinit var startRangDateTv : TextView
    private var locationId = ""
    private var selectItemGuest = ""
    private var locationTitle = ""
    private var room = "1"
    private var adults = "1"
    private var children = "0"
    private var startDate = ""
    private var endDate = ""
    private lateinit var btnSearch : LinearLayout
    private lateinit var btnSearchTv : TextView
    private var subItemHomeModelList : ArrayList<SubItemHomeModel> = ArrayList()
    private lateinit var bookingHomeItemAdapter: BookingHomeItemAdapter
    private var infoAddress = ""
    private lateinit var titleToolbar : TextView
    private lateinit var refreshLayout: SwipeRefreshLayout
    private var locationHotelModelList: ArrayList<LocationHotelModel> = ArrayList()
    private lateinit var linearLayoutManager: LinearLayoutManager
    private var currentItem = 0
    private var total : Int = 0
    private var scrollDown : Int = 0
    private var currentPage : Int = 1
    private var size : Int = 10
    private var isScrolling = false
    private var fromAction = ""
    private var eazyhotelUserId = ""
    private var category = ""
    private lateinit var mapFragment : FragmentContainerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_booking_item_list)

        initView()

        initData()

        initAction()

    }

    private fun initView(){
        progressBar = findViewById(R.id.progressItem)
        progressBar.visibility = View.GONE
        recyclerView = findViewById(R.id.recyclerView)

        findViewById<ImageView>(R.id.iconBack).setOnClickListener { finish() }
        btnSearch = findViewById(R.id.btnsearch)
        titleToolbar = findViewById(R.id.titleToolbar)
        refreshLayout = findViewById(R.id.swipe_layouts)

        mapFragment = findViewById(R.id.mapFragment)
    }

    private fun initData(){
        val hashMapData: HashMap<String, Any> = RequestHashMapData.getStoreHotelData()
        if (hashMapData != null){
            infoAddress = hashMapData["value"].toString()
            titleToolbar.text = infoAddress

            locationId = hashMapData["location_id"].toString()
            room = hashMapData["room"].toString()
            adults = hashMapData["adults"].toString()
            children = hashMapData["children"].toString()
            locationTitle = hashMapData["location_title"].toString()
            startDate = hashMapData["start_date"].toString()
            endDate = hashMapData["end_date"].toString()
            fromAction = hashMapData["from_action"].toString()
            selectItemGuest = hashMapData["guest_num"].toString()
            locationHotelModelList = hashMapData["location_list"] as ArrayList<LocationHotelModel>
            category = hashMapData["category"].toString()
        }

        eazyhotelUserId = Utils.validateNullValue(MockUpData.getEazyHotelUserId(UserSessionManagement(this)))
    }

    private fun initAction(){
        linearLayoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = linearLayoutManager
        bookingHomeItemAdapter = BookingHomeItemAdapter(this, "bestseller_listing", subItemHomeModelList, onClickSubItemBookingListener)
        recyclerView.adapter = bookingHomeItemAdapter

        requestItemHotelList()

        onScrollItemRecycle()

        btnSearch.setOnClickListener {
            openSpinner(EasyDialog(this), btnSearch)
        }

        refreshLayout.setColorSchemeResources(R.color.appBarColorOld)
        refreshLayout.setOnRefreshListener { refreshList() }

        initMenuMainHotel()
    }

    private fun requestItemHotelList(){
        progressBar.visibility = View.VISIBLE
        BookingHotelWS().getHotelBookingAll(
            this@HotelSearchShowHotelBookingActivity,
            fromAction, category,
            RequestHashMapData.hashMapData(currentPage, category, locationId, eazyhotelUserId),
            object : BookingHotelWS.OnCallBackMyProfileListener{
                    override fun onSuccessLocation(locationHotelModel: ArrayList<LocationHotelModel>) {
                        progressBar.visibility = View.GONE
                        refreshLayout.isRefreshing = false
                    }

                override fun onSuccessShowHotelDetail(hotelDetailItemModel: HotelBookingDetailModel, mainItemHomeModelLIst: ArrayList<SubItemHomeModel>) {}

                @SuppressLint("NotifyDataSetChanged")
                    override fun onSuccessShowHotel(action: String, listImageSlider : MutableList<String>, locationHotelModel: ArrayList<MainItemHomeModel>, itemHomeList: ArrayList<SubItemHomeModel>,userDeactivated: UserDeactivated) {
                        progressBar.visibility = View.GONE
                        refreshLayout.isRefreshing = false
                        for (item in locationHotelModel){
                            subItemHomeModelList.addAll(item.subItemHomeModelList)
                        }
                        bookingHomeItemAdapter.notifyDataSetChanged()

                        Utils.validateViewNoItemFound(this@HotelSearchShowHotelBookingActivity, recyclerView, subItemHomeModelList.size <= 0)
                    }

                    override fun onFailed(message: String) {
                        progressBar.visibility = View.GONE
                        refreshLayout.isRefreshing = false
                        Utils.customToastMsgError(this@HotelSearchShowHotelBookingActivity, message, false)
                    }

                })
    }

    private fun openSpinner(easyDialog : EasyDialog, linearLayout: LinearLayout) {
        @SuppressLint("InflateParams") val layout = LayoutInflater.from(this@HotelSearchShowHotelBookingActivity).inflate(R.layout.activity_booking_select_location, null)
        EasyDialogClass.createFilterEasyDialog(easyDialog, this@HotelSearchShowHotelBookingActivity, layout, linearLayout, true)

        selectLocationTv = layout.findViewById(R.id.selectLocationTv)
        selectRoomTv = layout.findViewById(R.id.selectRoomTv)
        startRangDateTv = layout.findViewById(R.id.startDateTv)
        btnSearchTv = layout.findViewById(R.id.btnSearch)
        layout.findViewById<ProgressBar>(R.id.progressItem).visibility = View.GONE

        selectLocationTv.text = locationTitle
        selectRoomTv.text = selectItemGuest

        if (startDate != "" && endDate != "")   startRangDateTv.text =  String.format("%s - %s", startDate,  endDate)

        doActionClickMenu(easyDialog)

        checkValidateButtonPay()
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun refreshList(){
        currentPage = 1
        size = 10
        isScrolling = true

        bookingHomeItemAdapter.clear()
        requestItemHotelList()
    }


    private fun doActionClickMenu(easyDialog : EasyDialog){
        selectLocationTv.setOnClickListener(
            CustomSetOnClickViewListener{
                startSelectCategory("select_location")
            }
        )
        startRangDateTv.setOnClickListener(
            CustomSetOnClickViewListener{
                startEndDateRang()
            }
        )

        selectRoomTv.setOnClickListener(
            CustomSetOnClickViewListener{
                startSelectCategory("select_room")
            }
        )

        btnSearchTv.setOnClickListener {
            easyDialog.dismiss()
            fromAction = "search_list"
            refreshList()

            infoAddress = String.format("%s ∙ %s - %s", locationTitle, startDate, endDate)
            titleToolbar.text = infoAddress
        }
    }

    private fun startEndDateRang(){
        val selectLocationBookingFragment = SelectStartEndDateRangBottomsheet.newInstance(startDate, endDate)
        selectLocationBookingFragment.initListener(onCallBackDateListener)
        selectLocationBookingFragment.show(supportFragmentManager, "fragment")
    }

    private fun startSelectCategory(action : String){
        val selectLocationBookingFragment : SelectLocationBookingFragment = if (action == "select_location") {
            SelectLocationBookingFragment().newInstance(locationId, action, locationHotelModelList)
        } else {
            SelectLocationBookingFragment().newInstance(room, adults,children, action)
        }
        selectLocationBookingFragment.initListener(onCallBackListener)
        selectLocationBookingFragment.show(supportFragmentManager, "fragment")
    }

    private fun onScrollItemRecycle() {
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true
                }
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                currentItem = linearLayoutManager.childCount
                total = linearLayoutManager.itemCount
                scrollDown = linearLayoutManager.findFirstVisibleItemPosition()
                if (dy > 0) {
                    if (isScrolling && currentItem + scrollDown == total) {
                        if (total == size) {
                            isScrolling = false
                            currentPage++
                            progressBar.visibility = View.VISIBLE
                            recyclerView.scrollToPosition(subItemHomeModelList.size - 1)
                            requestItemHotelList()
                            size += 10
                        } else {
                            progressBar.visibility = View.GONE
                        }
                    }
                }
            }
        })
    }

    private val onCallBackDateListener = object : SelectStartEndDateRangBottomsheet.OnClickCallBackListener{
        override fun onClickSelect(getStartDate: String, getEndDate : String) {
            startDate = getStartDate
            endDate = getEndDate
            startRangDateTv.text = String.format("%s - %s", getStartDate, getEndDate)
            checkValidateButtonPay()
        }
    }

    private val onCallBackListener = object : SelectLocationBookingFragment.OnClickCallBackListener{
        override fun onClickSelectLocation(titleHeader: LocationHotelModel) {
            selectLocationTv.text = titleHeader.location_name
            locationId = titleHeader.location_id.toString()
            locationTitle = titleHeader.location_name!!
            checkValidateButtonPay()
        }

        override fun onClickSelect(titleHeader: String, r: String, ad: String, ch: String) {
            selectRoomTv.text = titleHeader
            selectItemGuest = titleHeader
            room = r
            adults = ad
            children = ch
            checkValidateButtonPay()
        }
    }

    private var onClickSubItemBookingListener = object : BookingHomeItemAdapter.PropertyClick {
        override fun onBookingClick(propertyModel: SubItemHomeModel?) {
            if (propertyModel != null) {
                val intent = Intent(this@HotelSearchShowHotelBookingActivity, HotelBookingDetailHotelActivity::class.java)
                intent.putExtra("hotel_name", propertyModel.name)
                intent.putExtra("category", propertyModel.titleCategory)
                intent.putExtra("hotel_image", propertyModel.imageUrl)
                intent.putExtra("hotel_id", propertyModel.id)
                intent.putExtra("price", propertyModel.priceVal)
                intent.putExtra("start_date", startDate)
                intent.putExtra("end_date", endDate)
                intent.putExtra("adults", adults)
                intent.putExtra("children", children)
                resultLauncher.launch(intent)
            }
        }

        override fun onBookingWishlistClick(propertyModel: SubItemHomeModel?) {
            if (AppAlertCusDialog.isSuccessLoggedIn(this@HotelSearchShowHotelBookingActivity)) {
                RequestHashMapData.editStatusHotelWs(this@HotelSearchShowHotelBookingActivity, progressBar, propertyModel!!.id!!, propertyModel.isWishlist,
                    "wishlist", eazyhotelUserId, category, object : RequestHashMapData.Companion.OnSuccessEditStatusListener{
                        @SuppressLint("NotifyDataSetChanged")
                        override fun onSuccess(msg: String) {
                            propertyModel.isWishlist = !propertyModel.isWishlist
                            bookingHomeItemAdapter.notifyDataSetChanged()
                        }
                    })
            }
        }
    }

    private fun checkValidateButtonPay(){
        if (isEnableButtonPay()){
            btnSearchTv.isEnabled = true
            Utils.setBgTint(btnSearchTv, R.color.gray)
            Utils.setBgTint(btnSearchTv, R.color.appBarColorOld)
        } else {
            btnSearchTv.isEnabled = false
            Utils.setBgTint(btnSearchTv, R.color.gray)
        }
    }

    private fun isEnableButtonPay() : Boolean{
        if (locationId == "" || selectItemGuest == "" || startDate == "" || endDate == ""){
            return false
        }
        return true
    }

    private val resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
        if (result.resultCode == Activity.RESULT_OK) {
            //Call back when click wishlist hotel on detail screen
            refreshList()
        }
    }

    private fun initMenuMainHotel(){
        val mainMenuLayout : LinearLayout = findViewById(R.id.mainMenuLayout)
        mainMenuLayout.visibility = View.GONE
        var id = 0
        for (i in 0 until mainMenuLayout.childCount) {
            mainMenuLayout.getChildAt(i).setOnClickListener(CustomSetOnClickViewListener {
                if (i == 3) {
                    if (i == id) return@CustomSetOnClickViewListener
                    mapFragment.visibility = View.VISIBLE
                    refreshLayout.visibility = View.GONE
                    replaceFragment(HotelMapsFragment.newInstance("filter_map"))
                    id = i
                } else if(i == 1 || i == 2){
                    startBottomSheetDialog()
                    id = i
                } else {
                    mapFragment.visibility = View.GONE
                    refreshLayout.visibility = View.VISIBLE
                    id = i
                }
            })
        }
    }

    private fun startBottomSheetDialog(){
        val bottomSheetFragment = HotelFilterSortFragment()
        bottomSheetFragment.show(supportFragmentManager, "list_item_overtime")
    }

    private fun replaceFragment(fragment: Fragment) {
        val manager: FragmentManager = supportFragmentManager
        val transaction = manager.beginTransaction()
        transaction.replace(R.id.mapFragment, fragment, fragment.tag)
        transaction.disallowAddToBackStack()
        transaction.commit()
    }
}
