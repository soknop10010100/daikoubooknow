package com.eazy.daikou.ui;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.eazy.daikou.helper.ImageViewTouchView;
import com.eazy.daikou.R;
import com.eazy.daikou.base.BaseActivity;

public class ShowDisplayImageLayoutActivity extends BaseActivity {

    private String getImgLink;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_see_layout_unit);

        // init view
        ImageViewTouchView photoView = findViewById(R.id.photo_view);
        findViewById(R.id.img).setOnClickListener(view -> onBackPressed());
        TextView noLayout = findViewById(R.id.noLayout);

        // init data
        if (getIntent() != null && getIntent().hasExtra("see_layout_img")) {
            getImgLink = getIntent().getStringExtra("see_layout_img");
        }

        if (getIntent() != null && getIntent().hasExtra("image")) {
            getImgLink = getIntent().getStringExtra("image");
        }

        // init action
        if (getImgLink == null) {
            noLayout.setVisibility(View.VISIBLE);
            photoView.setVisibility(View.GONE);
        } else {
            Glide.with(this).load(getImgLink).into(photoView);
            noLayout.setVisibility(View.GONE);
            photoView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
