package com.eazy.daikou.ui.home.inspection_work_order.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.eazy.daikou.R;
import com.eazy.daikou.helper.Utils;
import com.eazy.daikou.model.work_order.WorkOrderProgressModel;

import java.util.List;
import java.util.Locale;

public class WorkProgressListAdapter extends RecyclerView.Adapter<WorkProgressListAdapter.ItemViewHolder> {

    private final OnClickCallBackListener onClickCallBackListener;
    private final List<WorkOrderProgressModel> workOrderProgressModelList;
    private final Context context;

    public WorkProgressListAdapter(Context context, List<WorkOrderProgressModel> workOrderProgressModelList, OnClickCallBackListener onClickCallBackListener){
        this.context = context;
        this.workOrderProgressModelList = workOrderProgressModelList;
        this.onClickCallBackListener = onClickCallBackListener;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rowView = LayoutInflater.from(parent.getContext()).inflate(R.layout.work_order_model, parent, false);
        return new ItemViewHolder(rowView);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        WorkOrderProgressModel workOrderProgressModel = workOrderProgressModelList.get(position);
        if (workOrderProgressModel != null){
            holder.workProgressItemLayout.setVisibility(View.VISIBLE);
            holder.detailItemLayout.setVisibility(View.GONE);

            Utils.setValueOnText(holder.noTv, workOrderProgressModel.getOrderNo());
            holder.dateTv.setText(workOrderProgressModel.getWorkingDate() != null ? Utils.formatDateFromString("yyyy-MM-dd", "dd, MMM yyyy", workOrderProgressModel.getWorkingDate()) : ". . .");


            holder.startTimeTv.setText(workOrderProgressModel.getFromTime() != null ? Utils.formatDateTime(workOrderProgressModel.getFromTime(),"hh:mm", "hh:mm a") : ". . .");
            holder.endTimeTv.setText(workOrderProgressModel.getToTime() != null ? Utils.formatDateTime(workOrderProgressModel.getToTime(),"hh:mm", "hh:mm a") : ". . .");

            holder.spendTimeTv.setText(workOrderProgressModel.getSpentTime() != null ? workOrderProgressModel.getSpentTime() + " " + Utils.getText(context, R.string.minutes).toLowerCase(Locale.ROOT) : ". . ." );

            Utils.setValueOnText(holder.doneStatus, workOrderProgressModel.getDonePercent());
            holder.doneStatus.setBackgroundTintList(workOrderProgressModel.getDonePercent() != null ?
                Utils.setDoneStatusPercentOnWorkOrder(context, workOrderProgressModel.getDonePercent()) :  null);

        }
        holder.itemView.setOnClickListener(v -> onClickCallBackListener.onClickCallBack(workOrderProgressModel));
    }

    @Override
    public int getItemCount() {
        return workOrderProgressModelList.size();
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder{

        private final CardView detailItemLayout, workProgressItemLayout;
        private final TextView noTv, dateTv, startTimeTv, endTimeTv, spendTimeTv, doneStatus;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            detailItemLayout = itemView.findViewById(R.id.detailItemLayout);
            workProgressItemLayout = itemView.findViewById(R.id.workProgressItemLayout);
            noTv = itemView.findViewById(R.id.noWorkProTv);
            dateTv = itemView.findViewById(R.id.startDateWorkProTv);
            startTimeTv = itemView.findViewById(R.id.startTimeTv);
            endTimeTv = itemView.findViewById(R.id.endTimeTv);
            spendTimeTv = itemView.findViewById(R.id.spendTimeTv);
            doneStatus = itemView.findViewById(R.id.doneStatusWorkProTv);

        }
    }

    public interface OnClickCallBackListener{
        void onClickCallBack(WorkOrderProgressModel workOrderProgressModel);
    }
}
