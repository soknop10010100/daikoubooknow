package com.eazy.daikou.ui.contact;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.squareup.picasso.Picasso;
import com.eazy.daikou.R;

import java.util.ArrayList;
import java.util.List;

import static com.eazy.daikou.helper.Utils.logDebug;

public class AutoCompleteTextAdapter extends ArrayAdapter<AddContactModel> {
    private final List<AddContactModel> models;
    private final AutoCompleteClickCallback mCallback;
    private final int type;

    public AutoCompleteTextAdapter(@NonNull Context context, @NonNull List<AddContactModel> objects, AutoCompleteClickCallback callback, int type) {
        super(context, 0, objects);
        models = new ArrayList<>(objects);
        mCallback = callback;
        this.type = type;
    }


    @NonNull
    @Override
    public Filter getFilter() {
        return profileFilter;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.layout_show_search_contact, parent, false);
        }
        TextView userName = convertView.findViewById(R.id.profileName);
        ImageView profileImage = convertView.findViewById(R.id.userProfile);
        TextView profileEmail = convertView.findViewById(R.id.profile_email);

        AddContactModel contactModel = getItem(position);

        if (contactModel!=null){
            String user;
            if (contactModel.getLastName()!=null){
                user = contactModel.getFirstName()+" "+contactModel.getLastName();
            }else {
                user = contactModel.getFirstName();
            }
            userName.setText(user);
            profileEmail.setText(contactModel.getEmail());
            Picasso.get().load(contactModel.getImageUrl()).into(profileImage);
            logDebug("contactModleSS", contactModel.getFirstName());
        }
        convertView.setOnClickListener(view -> {
            mCallback.onItemClick(contactModel);

        });
        return convertView;
    }

    Filter profileFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            List<AddContactModel> suggestion = new ArrayList<>();

            if (constraint == null || constraint.length()==0){
                suggestion.addAll(models);
            }else {
                String filterpattern = constraint.toString().toLowerCase().trim();
                for (AddContactModel item : models){
                    String userName = item.getFirstName()+" "+item.getLastName();
                    if (userName.toLowerCase().contains(filterpattern)&&type==1){
                        suggestion.add(item);
                    }
                    else if (item.getEmail().toLowerCase().contains(filterpattern)&&type==2){
                        suggestion.add(item);
                    }
                }
            }
            results.values = suggestion;
            results.count=suggestion.size();
            return results;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            clear();
            addAll((List) filterResults.values);
            notifyDataSetChanged();
        }

        @Override
        public CharSequence convertResultToString(Object resultValue) {
            return super.convertResultToString(resultValue);
        }
    };

    public interface AutoCompleteClickCallback{
        void onItemClick(AddContactModel contactModel);
    }
}
