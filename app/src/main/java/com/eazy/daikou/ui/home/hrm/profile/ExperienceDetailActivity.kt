package com.eazy.daikou.ui.home.hrm.profile

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.model.hr.Experience
import com.eazy.daikou.model.hr.Qualification

class ExperienceDetailActivity : BaseActivity() {


    private lateinit var company_NameTv: TextView
    private lateinit var positionTv: TextView
    private lateinit var departmentTv: TextView
    private lateinit var reasonTv: TextView
    private lateinit var start_DateTv: TextView
    private lateinit var leave_DateTv: TextView
    private lateinit var salaryTv: TextView
    private lateinit var lastSalaryTv: TextView
    private lateinit var directorManagerNameTv: TextView
    private lateinit var directorManagerContactTV: TextView

    private lateinit var btnBack : TextView



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_experience_detail)

        initView()
        initAction()



    }

    private fun initView() {
        company_NameTv = findViewById(R.id.company_NameTv)
        positionTv = findViewById(R.id.positionTv)
        departmentTv = findViewById(R.id.departmentTv)
        reasonTv = findViewById(R.id.reasonTv)
        start_DateTv = findViewById(R.id.start_DateTv)
        leave_DateTv = findViewById(R.id.leave_DateTv)
        salaryTv = findViewById(R.id.salaryTv)
        lastSalaryTv = findViewById(R.id.lastSalaryTv)
        directorManagerNameTv = findViewById(R.id.directorManagerNameTv)
        directorManagerContactTV = findViewById(R.id.directorManagerContactTV)

        btnBack = findViewById(R.id.btn_back)


    }
    private fun initAction(){
        btnBack.setOnClickListener { finish() }

        if (intent != null && intent.hasExtra("experience_details")){
            val experience = intent.getSerializableExtra("experience_details") as Experience
            setValues(experience)
        }

    }

    private fun setValues(experience: Experience){

        if (experience.name != null){
            company_NameTv.text = experience.name
        }else{
            company_NameTv.text="- - - - -"
        }

        if (experience.position != null){
            positionTv.text = experience.position
        }else{
            positionTv.text="- - - - -"
        }

        if (experience.department != null){
            departmentTv.text = experience.department
        }else{
            departmentTv.text="- - - - -"
        }

        if (experience.reason != null){
            reasonTv.text = experience.reason
        }else{
            reasonTv.text="- - - - -"
        }

        if (experience.start_date != null){
            start_DateTv.text = experience.start_date
        }else{
            start_DateTv.text="- - - - -"
        }

        if (experience.leave_date != null){
            leave_DateTv.text = experience.leave_date
        }else{
            leave_DateTv.text="- - - - -"
        }

        if (experience.salary != null){
            salaryTv.text = experience.salary
        }else{
            salaryTv.text="- - - - -"
        }

        if (experience.last_salary != null){
            lastSalaryTv.text = experience.last_salary
        }else{
            lastSalaryTv.text="- - - - -"
        }

        if (experience.director_manager_name != null){
            directorManagerNameTv.text = experience.director_manager_name
        }else{
            directorManagerNameTv.text="- - - - -"
        }

        if (experience.director_manager_contact != null){
            directorManagerContactTV.text = experience.director_manager_contact
        }else{
            directorManagerContactTV.text="- - - - -"
        }
    }
}