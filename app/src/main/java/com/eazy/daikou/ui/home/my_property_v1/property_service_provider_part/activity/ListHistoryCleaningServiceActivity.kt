package com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.activity

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.activity.result.contract.ActivityResultContracts
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.base.MockUpData
import com.eazy.daikou.request_data.request.service_property_ws_v2.ServiceProviderPropertyWs
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.my_property.cleaning.PropertyServiceProviderListModel
import com.eazy.daikou.model.profile.User
import com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.adapter.HistoryCleaningAdapter
import com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.cleaning_v2.ServiceCleaningCreateRequestListActivity
import com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.cleaning_v2.ServiceCleaningPaymentAndBookingActivity
import com.google.gson.Gson
import kotlin.collections.ArrayList

class ListHistoryCleaningServiceActivity : BaseActivity() {

    private lateinit var recyclerView : RecyclerView
    private lateinit var historyCleaningAdapter : HistoryCleaningAdapter
    private lateinit var linearLayoutManager : LinearLayoutManager
    private lateinit var progressBar : ProgressBar
    private lateinit var refreshLayout : SwipeRefreshLayout
    private lateinit var imageNoRecord : RelativeLayout
    private lateinit var btnBack: ImageView
    private lateinit var btnCreate: ImageView
    private lateinit var titleTv: TextView
    private var sessionManagement: UserSessionManagement? = null
    private var user: User? = null

    private var listPropertyService: ArrayList<PropertyServiceProviderListModel> = ArrayList()

    private lateinit var userType: String
    private var userId = ""
    private var propertyId = ""
    private var currentItem = 0
    private var total : Int = 0
    private var scrollDown : Int = 0
    private var currentPage : Int = 1
    private var size : Int = 10
    private var isScrolling = false
    private var companyAccountId = ""

    //Notification reschedule
    private var notificationType = ""
    private var orderId = ""
    private var notification = ""
    private var operationPart = ""

    private var isActionHistory = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_history_of_cleaning_service)

        initView()

        initData()

        initAction()

    }

    private fun initView(){
        titleTv = findViewById(R.id.titleToolbar)
        btnBack = findViewById(R.id.iconBack)
        progressBar = findViewById(R.id.progressItem)
        refreshLayout = findViewById(R.id.swipe_layouts)
        imageNoRecord = findViewById(R.id.image_no_record)
        btnCreate = findViewById(R.id.btnCreate)
        recyclerView = findViewById(R.id.list_history)

    }

    private fun initData(){
        if( intent != null && intent.hasExtra("operation_part")){
            operationPart = intent.getStringExtra("operation_part") as String
        }
        if(intent != null && intent.hasExtra("action_notification")){
            notificationType = intent.getStringExtra("action_notification") as String
        }
        if(intent.hasExtra("notification")){
            notification = intent.getStringExtra("notification") as String
        }
        if(intent.hasExtra("order_id")){
            orderId = intent.getStringExtra("order_id") as String
        }
        if(intent.hasExtra("company_account_id")) {
            companyAccountId = intent.getStringExtra("company_account_id") as String
        }

        if( intent != null && intent.hasExtra("action_history")){
            isActionHistory = intent.getBooleanExtra("action_history", false)
        }
    }

    private fun initAction(){
        btnBack.setOnClickListener { finish() }
        titleTv.text = getString(R.string.cleaning_service_list)

        sessionManagement = UserSessionManagement(this)
        val gson = Gson()
        user = gson.fromJson(sessionManagement!!.userDetail, User::class.java)

        userId  = sessionManagement!!.userId
        userType = user!!.activeUserType
        propertyId = user!!.accountId

        linearLayoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = linearLayoutManager
        progressBar.visibility = View.GONE

        getListCleaning()

        refreshLayout.setColorSchemeResources(R.color.colorPrimary)
        refreshLayout.setOnRefreshListener { refreshList() }

        btnCreate.setOnClickListener(
            CustomSetOnClickViewListener {
                val intent = Intent(this, ServiceCleaningCreateRequestListActivity::class.java)
                intent.putExtra("operation_part_string", operationPart)
                intent.putExtra("id_company_account", companyAccountId)
                resultLauncher.launch(intent)
            }
        )

        onScrollListServiceProvider()

        //Notification Reschedule
        if (notificationType == "notification_reschedule"){
            startActivityToDetail(orderId)
        }
    }

    private fun refreshList(){
        currentPage = 1
        size = 10
        notification = ""
        historyCleaningAdapter.clear()
        getListCleaning()
    }

    private fun onScrollListServiceProvider() {
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true
                }
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                currentItem = linearLayoutManager.childCount
                total = linearLayoutManager.itemCount
                scrollDown = linearLayoutManager.findFirstVisibleItemPosition()
                if (dy > 0) {
                    if (isScrolling && currentItem + scrollDown == total) {
                        if (total == size) {
                            isScrolling = false
                            currentPage++
                            progressBar.visibility = View.VISIBLE
                            recyclerView.scrollToPosition(listPropertyService.size - 1)
                            size += 10
                            getListCleaning()
                        } else {
                            progressBar.visibility = View.GONE
                        }
                    }
                }
            }
        })
    }

    private fun getListCleaning() {
        progressBar.visibility = View.VISIBLE
        requestListApiPropertyService()
        historyCleaningAdapter = HistoryCleaningAdapter(this, listPropertyService, itemClick)
        recyclerView.adapter = historyCleaningAdapter
    }

    private fun requestListApiPropertyService(){
        progressBar.visibility = View.VISIBLE
        ServiceProviderPropertyWs().getListPropertyServiceProviderWs(this,currentPage, 10,operationPart, userId, companyAccountId, callBackListServiceProvider)
    }

    private val callBackListServiceProvider: ServiceProviderPropertyWs.OnCallBackListServiceListener = object : ServiceProviderPropertyWs.OnCallBackListServiceListener{
        @SuppressLint("NotifyDataSetChanged")
        override fun onLoadSuccessFull(listServiceProvider: ArrayList<PropertyServiceProviderListModel>) {
            progressBar.visibility = View.GONE
            refreshLayout.isRefreshing = false
            listPropertyService.addAll(listServiceProvider)

            imageNoRecord.visibility = if (listServiceProvider.size > 0) View.GONE else View.VISIBLE

            historyCleaningAdapter.notifyDataSetChanged()

            if (MockUpData.getUserItem(UserSessionManagement(this@ListHistoryCleaningServiceActivity)).activeUserType.equals("owner") ||
                MockUpData.getUserItem(UserSessionManagement(this@ListHistoryCleaningServiceActivity)).activeUserType.equals("tenant")){
                btnCreate.visibility = if(isActionHistory) View.GONE else View.VISIBLE
            }

        }
        override fun onLoadFailed(message: String) {
            progressBar.visibility = View.GONE
            refreshLayout.isRefreshing = false
            Utils.customToastMsgError(this@ListHistoryCleaningServiceActivity, message, false)
        }
    }

    private val itemClick = object : HistoryCleaningAdapter.ItemClickOnService {
        override fun onClick(serviceProperty: PropertyServiceProviderListModel) {
            if (MockUpData.getUserItem(UserSessionManagement(this@ListHistoryCleaningServiceActivity)).activeUserType.equals("owner") ||
                MockUpData.getUserItem(UserSessionManagement(this@ListHistoryCleaningServiceActivity)).activeUserType.equals("tenant")) {

                startActivityToDetail(serviceProperty.id.toString())

            }
        }
    }

    private fun startActivityToDetail(idList : String){
            val intent = Intent(this@ListHistoryCleaningServiceActivity, ServiceCleaningPaymentAndBookingActivity::class.java)
            intent.putExtra("action_get_list", "detail_display")
            intent.putExtra("id_register", idList)
            resultLauncher.launch(intent)

    }

    private var resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode == Activity.RESULT_OK) {
            val data: Intent? = result.data
            if (data != null) {
                //Set Result CallBack
                val isBackSuccess = data.getBooleanExtra("is_result_success", false)
                if (isBackSuccess) {
                    refreshList()
                }
            }
        }
    }
}



