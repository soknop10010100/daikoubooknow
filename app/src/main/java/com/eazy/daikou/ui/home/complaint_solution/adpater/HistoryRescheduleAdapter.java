package com.eazy.daikou.ui.home.complaint_solution.adpater;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.eazy.daikou.R;
import com.eazy.daikou.model.hr.SalaryItem;

import java.util.List;
import java.util.Objects;

public class HistoryRescheduleAdapter extends RecyclerView.Adapter<HistoryRescheduleAdapter.ItemViewHolder> {

    private final List<SalaryItem> salaryItemList;

    public HistoryRescheduleAdapter(List<SalaryItem> salaryItemList) {
        this.salaryItemList = salaryItemList;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.reschedule_history, parent, false));
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        SalaryItem salaryItem = salaryItemList.get(position);
        if (salaryItem != null){
            holder.reScheduleComplaint.setVisibility(View.GONE);
            holder.numResent.setVisibility(View.GONE);
            holder.txtDateTv.setVisibility(View.VISIBLE);
            holder.reSentFrontdesk.setVisibility(View.VISIBLE);
            holder.hisResentDateTv.setText(salaryItem.getAmount() != null ? " "+ Objects.requireNonNull(salaryItem.getAmount()).concat(" ").concat(Objects.requireNonNull(salaryItem.getCurrency_code())) : "- - -");
            holder.txtDateTv.setText(salaryItem.getItem_name() != null ? salaryItem.getItem_name() : "- - -");
            holder.viewLine.setVisibility(position == salaryItemList.size() - 1 ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return salaryItemList.size();
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder{

        private final TextView numResent, hisResentDateTv, txtDateTv;
        private final LinearLayout reScheduleComplaint, reSentFrontdesk;
        private final View viewLine;
        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            txtDateTv = itemView.findViewById(R.id.txtDateTv);
            hisResentDateTv = itemView.findViewById(R.id.hisResentDateTv);
            numResent = itemView.findViewById(R.id.numResent);
            reScheduleComplaint = itemView.findViewById(R.id.complaintHistory);
            reSentFrontdesk = itemView.findViewById(R.id.frontDeskHistory);
            viewLine = itemView.findViewById(R.id.view_line);
        }
    }
}
