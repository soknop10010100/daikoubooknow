package com.eazy.daikou.ui.home.complaint_solution.complaint.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.request_data.request.book_now_ws.GenerateRespondWs.Companion.convertListStringImage
import com.eazy.daikou.helper.DateUtil
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.complaint.ComplaintListModel
import com.eazy.daikou.ui.home.book_now.booking_hotel.adapter.CustomImageGridLayoutAdapter

class ComplaintSolutionAdapter(private val actionPostType : String, private val listComplaint: ArrayList<ComplaintListModel>, private val context: Context, private val onItemClick: OnItemClick):
    RecyclerView.Adapter<ComplaintSolutionAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.adapter_complaint_model_layout, parent, false)
        return  ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val complaint : ComplaintListModel = listComplaint[position]
        holder.onBindingView(context, complaint, onItemClick)
    }

    override fun getItemCount(): Int {
        return listComplaint.size
    }

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){

        private val recyclerView : RecyclerView = itemView.findViewById(R.id.recyclerView)
        private val numViewerTv : TextView = itemView.findViewById(R.id.numViewerTv)
        private val numCommentTv : TextView = itemView.findViewById(R.id.numCommentTv)
        private val statusTV : TextView = itemView.findViewById(R.id.statusTv)
        private val propertyTv : TextView = itemView.findViewById(R.id.propertyTv)

        fun onBindingView(context: Context, item: ComplaintListModel, onClickListener: OnItemClick) {
            // User Profile
            if (item.uploader_user != null) {
                Utils.setValueOnText(itemView.findViewById(R.id.nameTv), item.uploader_user!!.name)
                Glide.with(context)
                    .load(if (item.uploader_user!!.image != null) item.uploader_user!!.image else R.drawable.ic_my_profile)
                    .into(itemView.findViewById(R.id.profile_image))
            } else {
                Utils.setValueOnText(itemView.findViewById(R.id.nameTv), ". . .")
                Glide.with(context)
                    .load(R.drawable.ic_my_profile)
                    .into(itemView.findViewById(R.id.profile_image))
            }

            if (item.account != null) {
                Utils.setValueOnText(propertyTv, item.account!!.name)
            } else {
                propertyTv.text = ". . ."
            }

            // Content Body
            Utils.setValueOnText(itemView.findViewById(R.id.subjectTv), item.subject)

            Utils.setValueOnText(itemView.findViewById(R.id.descriptionTv), item.description)

            if (item.object_type != null){
                if (item.object_type == "unit"){
                    if (item.object_info != null) {
                        Utils.setValueOnText(itemView.findViewById(R.id.typeTv), item.object_info!!.name)
                    } else {
                        Utils.setValueOnText(itemView.findViewById(R.id.typeTv), ". . .")
                    }
                } else {
                    Utils.setValueOnText(itemView.findViewById(R.id.typeTv), item.object_type)
                }
            } else {
                Utils.setValueOnText(itemView.findViewById(R.id.typeTv), ". . .")
            }

            val durationTv = itemView.findViewById<TextView>(R.id.durationTv)
            if (item.created_at != null) {
                durationTv.text = DateUtil.getDurationDate(durationTv.context, item.created_at)
            } else {
                durationTv.text = ". . ."
            }

            if (item.total_contributor != null){
                numViewerTv.text = String.format("%s %s", item.total_contributor, "Distributors")
            } else {
                numViewerTv.text = String.format("%s %s", 0, "Distributors")
            }

            if (item.total_comments != null){
                numCommentTv.text = String.format("%s %s", item.total_comments, context.resources.getString(R.string.comment_s))
            } else {
                numCommentTv.text = String.format("%s %s", 0, context.resources.getString(R.string.comment_s))
            }

            // Show Image
            val imgList = convertListStringImage(item.files)
            recyclerView.layoutManager = Utils.spanGridImageLayoutManager(imgList, context)
            recyclerView.isNestedScrollingEnabled = false
            recyclerView.adapter = CustomImageGridLayoutAdapter(
                "show_5_items",
                context,
                imgList,
                object : CustomImageGridLayoutAdapter.OnClickCallBackLister {
                    override fun onClickCallBack(value: String) {
                        onClickListener.clickItem(item)
                    }
                })

            // Status
            when (item.post_type) {
                "evaluation" -> {
                    if (item.status != null){
                        statusTV.visibility = View.VISIBLE
                        statusTV.text = Utils.getSatisfaction(context)[item.status]
                        val drawable = Utils.getSatisfactionIcon()[item.status]
                        drawable?.let { statusTV.setCompoundDrawablesWithIntrinsicBounds(it,0,0,0) }
                    } else {
                        statusTV.visibility = View.GONE
                    }
                }
                else -> {
                    if (item.status != null){
                        statusTV.visibility = View.VISIBLE
                        statusTV.text = Utils.convertFirstCapital(item.status)
                    } else {
                        statusTV.visibility = View.GONE
                    }
                }
            }

            // Post Type
            (if (item.post_type != null) item.post_type else ". . .")?.let { postTypeItem(it, itemView.findViewById(R.id.typeOfItemTv)) }

            // Do Action
            itemView.findViewById<CardView>(R.id.mainLayout).setOnClickListener(CustomSetOnClickViewListener {
                onClickListener.clickItem(item)
            })
        }

    }

    companion object {
        fun postTypeItem(postType : String, txt : TextView) {
            var color = R.color.appBarColor
            val title: Int
            val mContext = txt.context
            when(postType) {
                "complaint" ->{
                    color = R.color.green
                    title = R.string.complaints_solutions
                }
                "frontdesk" -> {
                    color = R.color.blue
                    title = R.string.front_desk
                }
                "evaluation" -> {
                    color = R.color.appBarColorOld
                    title = R.string.evaluation
                }
                else -> {
                    title = R.string.no_
                }
            }
            txt.setBackgroundColor(Utils.getColor(mContext, color))
            txt.text = mContext.resources.getString(title)
        }
    }

    interface OnItemClick{
        fun clickItem(complaintModel : ComplaintListModel)
    }

    fun clear() {
        val size: Int = listComplaint.size
        if (size > 0) {
            for (i in 0 until size) {
                listComplaint.removeAt(0)
            }
            notifyItemRangeRemoved(0, size)
        }
    }
}