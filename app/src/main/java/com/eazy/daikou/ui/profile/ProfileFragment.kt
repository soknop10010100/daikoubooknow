package com.eazy.daikou.ui.profile

import android.app.Activity
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Bitmap
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.activity.result.ActivityResult
import androidx.appcompat.app.AlertDialog
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.Constant
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.base.BaseCameraActivity
import com.eazy.daikou.base.BaseFragment
import com.eazy.daikou.base.MockUpData
import com.eazy.daikou.request_data.request.notificationWs.PushNotificationWS
import com.eazy.daikou.request_data.request.notificationWs.PushNotificationWS.PushNotificationCallBackListener
import com.eazy.daikou.request_data.request.profile_ws.LoginWs
import com.eazy.daikou.request_data.request.profile_ws.LoginWs.OnLogoutListener
import com.eazy.daikou.request_data.request.profile_ws.ProfileWs
import com.eazy.daikou.request_data.request.profile_ws.ProfileWs.CallBackSendQrCodeListener
import com.eazy.daikou.request_data.sql_database.TrackWaterElectricityDatabase
import com.eazy.daikou.request_data.static_data.StaticUtilsKey
import com.eazy.daikou.helper.AppAlertCusDialog
import com.eazy.daikou.helper.InternetConnection
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.profile.User
import com.eazy.daikou.model.profile.UserTypeProfileModel
import com.eazy.daikou.ui.LoginActivity
import com.eazy.daikou.ui.ShowDisplayImageLayoutActivity
import com.google.gson.Gson
import de.hdodenhof.circleimageview.CircleImageView
import libs.mjn.prettydialog.PrettyDialog
import java.util.*

class ProfileFragment : BaseFragment() {

    private lateinit var noInternet: ConstraintLayout
    private lateinit var progressBar: ProgressBar
    private lateinit var txtUsername: TextView
    private lateinit var txtEmail: TextView
    private lateinit var txtPhone: TextView
    private lateinit var txtStatusNotification: TextView
    private lateinit var coverProfile: RelativeLayout
    private lateinit var languageLayout: RelativeLayout
    private lateinit var changePasswordLayout: RelativeLayout
    private lateinit var logoutLayout: RelativeLayout
    private lateinit var aboutLayout: RelativeLayout
    private lateinit var pushNotificationLayout: RelativeLayout
    private lateinit var deleteAccountLayout: RelativeLayout
    private lateinit var emailLayout: RelativeLayout
    private lateinit var phoneLayout: RelativeLayout
    private lateinit var coverProfileImg: ImageView
    private lateinit var imgCover: ImageView
    private lateinit var profileImgCamera: ImageView
    private lateinit var fabQrCode: ImageView
    private lateinit var fabQrCodeUser: ImageView
    private lateinit var profileImg: CircleImageView
    private lateinit var userSessionManagement: UserSessionManagement
    private lateinit var view: ScrollView
    private var userProfile: User = User()

    private var statusNotification: String = ""
    private var userName: String = ""
    private val REQUEST_COVER = 100
    private val REQUEST_PROFILE = 200
    private var backUploadedBitmap: Bitmap? = null
    private var REQUEST_CODE = 0

    //active account
    private var isDeactivated = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val rootView = inflater.inflate(R.layout.fragment_profile, container, false)

        initView(rootView)

        initData()

        initAction()

        return rootView
    }

    private fun initView(rootView: View) {
        fabQrCode = rootView.findViewById(R.id.fQrCode)
        fabQrCodeUser = rootView.findViewById(R.id.fQrCodeUser)

        txtUsername = rootView.findViewById(R.id.txtUsername)
        txtEmail = rootView.findViewById(R.id.tv_email)
        emailLayout = rootView.findViewById(R.id.emailLayout)
        phoneLayout = rootView.findViewById(R.id.phoneLayout)
        txtPhone = rootView.findViewById(R.id.tv_phone)
        aboutLayout = rootView.findViewById(R.id.aboutLayout)
        coverProfileImg = rootView.findViewById(R.id.img_cover_profile)
        coverProfile = rootView.findViewById(R.id.cover_profile_border)
        profileImg = rootView.findViewById(R.id.img_profile)
        imgCover = rootView.findViewById(R.id.img_cover)
        profileImgCamera = rootView.findViewById(R.id.img_camera_profile)
        languageLayout = rootView.findViewById(R.id.language_Layout)
        changePasswordLayout = rootView.findViewById(R.id.changePassword_layout)
        logoutLayout = rootView.findViewById(R.id.logoutLayout)
        pushNotificationLayout = rootView.findViewById(R.id.push_notification_layout)
        deleteAccountLayout = rootView.findViewById(R.id.deleteAccountLayout)
        txtStatusNotification = rootView.findViewById(R.id.statusNote)
        progressBar = rootView.findViewById(R.id.progressItem)
        progressBar.visibility = View.GONE
        view = rootView.findViewById(R.id.mainScrollView)

        noInternet = rootView.findViewById(R.id.no_internet_connection)

        rootView.findViewById<View>(R.id.btn_refresh_fragment).setOnClickListener(CustomSetOnClickViewListener { onRefresh() })

        rootView.findViewById<ImageView>(R.id.btnRefresh).setOnClickListener(CustomSetOnClickViewListener { onRefresh() })

    }

    private fun initData() {
        //Get String Data From Local
        userSessionManagement = UserSessionManagement(context)

        userProfile = Gson().fromJson(userSessionManagement.userDetail, User::class.java)
    }

    private fun initAction() {
        if (!InternetConnection.checkInternetConnectionActivity(context)) {
            view.visibility = View.VISIBLE
            setValueDefault(userProfile, true)
        } else {
            loadViewProfile()
        }
        view.visibility = View.VISIBLE

        deleteAccountLayout.visibility =
            if (MockUpData.isUserAsGuest(userSessionManagement)) View.VISIBLE else View.GONE

        coverProfile.setOnClickListener(CustomSetOnClickViewListener {
            if (userProfile.coverImage != null) startZoomImage(REQUEST_COVER)
        })

        imgCover.setOnClickListener(CustomSetOnClickViewListener {
            startActivityToCamera(REQUEST_COVER)
        })

        profileImg.setOnClickListener(CustomSetOnClickViewListener {
            if (userProfile.photo != null) startZoomImage(REQUEST_PROFILE)
        })

        profileImgCamera.setOnClickListener(CustomSetOnClickViewListener {
            startActivityToCamera(REQUEST_PROFILE)
        })

        languageLayout.setOnClickListener(CustomSetOnClickViewListener {
            selectChangeLanguage("language")
        })

        logoutLayout.setOnClickListener(CustomSetOnClickViewListener {
            btnLogout()
        })

        emailLayout.setOnClickListener(CustomSetOnClickViewListener {
            StaticUtilsKey.is_email_address = true
            AppAlertCusDialog().alertDialogSimple(
                mActivity,
                StaticUtilsKey.first_action, "email",
                true
            )
        })

        phoneLayout.setOnClickListener(CustomSetOnClickViewListener {
            StaticUtilsKey.is_email_address = false
            AppAlertCusDialog().alertDialogSimple(
                mActivity,
                StaticUtilsKey.first_action, "phone",
                true
            )
        })

        pushNotificationLayout.setOnClickListener(CustomSetOnClickViewListener {
            onClickPushNotification()
        })

        changePasswordLayout.setOnClickListener(CustomSetOnClickViewListener {
            val intentChangePass = Intent(mActivity, ChangePassWordActivity::class.java)
            startActivity(intentChangePass)
        })

        aboutLayout.setOnClickListener(CustomSetOnClickViewListener {
            val intent = Intent(context, AboutActivity::class.java)
            startActivity(intent)
        })

        fabQrCode.setOnClickListener(CustomSetOnClickViewListener {
            AppAlertCusDialog.underConstructionDialog(
                context,
                resources.getString(R.string.comming_soon)
            )
        })

        fabQrCodeUser.setOnClickListener(CustomSetOnClickViewListener {
            selectChangeLanguage("qr_code_user")
        })

        deleteAccountLayout.setOnClickListener(CustomSetOnClickViewListener {
            if (isDeactivated) {
                AppAlertCusDialog.customAlertDialogYesNo(mActivity, resources.getString(R.string.confirm),
                        "Your account is waiting to be deactivated and can't be recovered after successfully deactivated .", "Cancel Deactivate", "Close"
                ) { alertDialog: AlertDialog, progressBar: ProgressBar, btnOk: Button, btnCancel: Button ->
                    progressBar.visibility = View.VISIBLE
                    requestLogOut(alertDialog, progressBar, btnOk, btnCancel)
                }
            } else {
                AppAlertCusDialog.deleteUserAccount(
                    context,
                    "Are you sure to delete your account ?",
                    "Delete account information, name and profile pictures. Data will not be recover after deleting account .",
                    PrettyDialog(context)
                ) {
                    startActivity(Intent(context, ProfileDeactivedAccountActivity::class.java))
                }
            }
        })

    }

    private fun getFileCamera(imagePath: String, action: String) {
        val loadedBitmap = BaseCameraActivity.convertByteToBitmap(imagePath)
        backUploadedBitmap = loadedBitmap
        savePictureProfile(Utils.convert(loadedBitmap), action)
    }

    private fun savePictureProfile(base64: String, actionType: String) {
        progressBar.visibility = View.VISIBLE
        val imageHashMap = HashMap<String, String>()
        imageHashMap["base64_image"] = base64
        imageHashMap["action"] = actionType
        ProfileWs().changeProfile(context, imageHashMap, callBackProfile)
    }

    private fun startZoomImage(code: Int) {
        val intent = Intent(activity, ShowDisplayImageLayoutActivity::class.java)
        if (code == REQUEST_PROFILE) {
            intent.putExtra("image", userProfile.photo)
        } else {
            intent.putExtra("image", userProfile.coverImage)
        }
        intent.putExtra("key", "display_image")
        startActivity(intent)
    }

    private fun getStatus(isClick: Boolean, firstStatus: String): String {
        val isKey: String = if (isClick) {
            if (firstStatus == "enable") {
                "disable"
            } else {
                "enable"
            }
        } else {
            if (firstStatus == "enable") {
                "enable"
            } else {
                "disable"
            }
        }
        return isKey
    }

    private fun setValueDefault(user: User, isNoInternet: Boolean) {
        statusNotification = getStatus(false, user.pushNotification)

        val color = if (user.pushNotification == "enable") {
            R.color.green
        } else {
            R.color.red
        }

        txtStatusNotification.backgroundTintList =
            ColorStateList.valueOf(Utils.getColor(context, color))

        txtStatusNotification.text = Utils.convertFirstCapital(statusNotification)

        if (isNoInternet) {
            if (user.name != null) {
                txtUsername.text = user.name
                userName = user.name
            } else {
                txtUsername.text = ". . ."
            }
        } else {
            if (user.name != null) {
                txtUsername.text = user.name
                userName = user.name
            } else {
                txtUsername.text = ". . ."
            }
        }

        Utils.setValueOnText(txtEmail, user.email)

        Utils.setValueOnText(txtPhone, user.phoneNo)

        Glide.with(coverProfileImg).load(if (user.coverImage != null)   user.coverImage else R.drawable.no_image).into(coverProfileImg)

        Glide.with(profileImg).load(if (user.photo != null) user.photo else R.drawable.ic_my_profile).into(profileImg)

    }

    private val callBackProfile: ProfileWs.CallBackListener = object : ProfileWs.CallBackListener {
        override fun updateSuccessLoadImage(message: String) {
            progressBar.visibility = View.GONE
            view.visibility = View.GONE

            Utils.customToastMsgError(mActivity, message, true)

            if (REQUEST_CODE == REQUEST_COVER) {
                if (backUploadedBitmap != null) {
                    Glide.with(coverProfileImg).load(backUploadedBitmap).into(coverProfileImg)
                } else {
                    Glide.with(coverProfileImg).load(R.drawable.no_image).into(coverProfileImg)
                }
            } else {
                if (backUploadedBitmap != null) {
                    Glide.with(profileImg).load(backUploadedBitmap).into(profileImg)
                } else {
                    Glide.with(profileImg).load(R.drawable.ic_my_profile).into(profileImg)
                }
            }

            loadViewProfile()
        }

        override fun onMessageFalse(msg: String) {
            progressBar.visibility = View.GONE
            Utils.customToastMsgError(mActivity, msg, false)
        }

        override fun onSuccessViewProfile(user: User?) {
            progressBar.visibility = View.GONE
            view.visibility = View.VISIBLE
            if (user != null) {
                setValueDefault(user, false)
                userProfile = user
                userSessionManagement.saveData(userProfile, true)
            }
        }

        override fun onFailed(error: String) {
            progressBar.visibility = View.GONE
            Utils.customToastMsgError(activity, error, false)
        }
    }

    private fun loadViewProfile() {
        progressBar.visibility = View.VISIBLE
        ProfileWs().viewProfile(activity, BaseActivity.DEVICE_ID, "daikou", callBackProfile)
    }

    private fun onRefresh() {
        if (InternetConnection.checkInternetConnectionActivity(context)) {
            noInternet.visibility = View.GONE
            loadViewProfile()
        } else {
            Utils.delayProgressBar(progressBar)
        }
    }

    private fun startActivityToCamera(code: Int) {
        val intent = Intent(context, BaseCameraActivity::class.java)
        intent.putExtra("no_select", "no")
        REQUEST_CODE = code

        activityLauncher.launch(intent) { result: ActivityResult ->
            if (result.resultCode == Activity.RESULT_OK) {
                val data = result.data ?: return@launch

                // Check action
                val action = when (code) {
                    REQUEST_COVER -> "cover"
                    REQUEST_PROFILE -> "profile"
                    else -> ""
                }

                if (data.hasExtra("image_path")) {
                    val imagePath = data.getStringExtra("image_path")
                    getFileCamera(imagePath!!, action)
                }

                if (data.hasExtra("select_image")) {
                    val imagePath = data.getStringExtra("select_image")
                    getFileCamera(imagePath!!, action)
                }
            }
        }
    }

    private fun btnLogout() {
        AppAlertCusDialog.customAlertDialogYesNo(mActivity, resources.getString(R.string.confirm), resources.getString(R.string.do_you_want_to_sign_out), resources.getString(R.string.ok), resources.getString(R.string.cancel)) {
            alertDialog: AlertDialog, progressBar: ProgressBar, btnOk: Button, btnCancel: Button ->
            progressBar.visibility = View.VISIBLE
            requestLogOut(alertDialog, progressBar, btnOk, btnCancel)
        }
    }

    private fun requestLogOut(
        alertDialog: AlertDialog,
        progressBar: ProgressBar,
        btnOk: Button,
        btnCancel: Button
    ) {
        btnOk.isEnabled = false
        btnCancel.isEnabled = false
        LoginWs().logout(context, object : OnLogoutListener {
            override fun onSuccess(msg: String, data: User) {
                progressBar.visibility = View.GONE
                userSessionManagement.logoutUser()
                val database = TrackWaterElectricityDatabase(context)
                database.deleteAllFloorRecord()
                database.deleteAllFloorRecordFloorDetail()
                val intent = Intent(context, LoginActivity::class.java)
                startActivity(intent)
                Utils.customToastMsgError(context, msg, true)
                alertDialog.dismiss()
            }

            override fun onSuccessFalse(status: String) {
                progressBar.visibility = View.GONE
                Utils.customToastMsgError(context, status, false)
                alertDialog.dismiss()
                btnOk.isEnabled = true
                btnCancel.isEnabled = true
            }

            override fun onWrong(msg: String) {
                progressBar.visibility = View.GONE
                Utils.customToastMsgError(context, msg, false)
                alertDialog.dismiss()
                btnOk.isEnabled = true
                btnCancel.isEnabled = true
            }

            override fun onFailed(mess: String) {
                progressBar.visibility = View.GONE
                Utils.customToastMsgError(context, mess, false)
                alertDialog.dismiss()
                btnOk.isEnabled = true
                btnCancel.isEnabled = true
            }
        })
    }

    private fun selectChangeLanguage(key: String) {
        val changeLanguageAlert = ChangeLanguageFragment.newInstance(key)
        changeLanguageAlert.show(
            childFragmentManager,
            changeLanguageAlert.javaClass.simpleName
        )
    }

    private val callBackSendQrCodeListener: CallBackSendQrCodeListener =
        object : CallBackSendQrCodeListener {
            override fun onSuccess(msg: String) {
                Utils.saveString(Constant.FIRST_SCAN_QR, "false", activity)
            }

            override fun onSuccess(list: List<UserTypeProfileModel>) {}
            override fun onFailed(error: String) {
                // Toast.makeText(mActivity, error, Toast.LENGTH_SHORT).show();
                Utils.saveString(Constant.FIRST_SCAN_QR, "true", activity)
            }
        }

    private fun requestQrCodeToServer() {
        val hashMap = HashMap<String, Any>()
        hashMap["card_no"] = Utils.leftPadding(userSessionManagement.userId)
        hashMap["car_no"] = "2BL-2222"
        hashMap["card_type"] = "VIP"
        hashMap["card_in_date"] = "2050-09-01"
        hashMap["card_amount"] = "100.0"
        hashMap["car_type"] = "Sedan"
        hashMap["car_style"] = "Toyota"
        hashMap["car_color"] = "Red"
        hashMap["user_name"] = userName
        hashMap["user_id"] = userSessionManagement.userId
        hashMap["user_phone_number"] = "01234567"
        hashMap["user_address"] = "Phnom Penh"
        hashMap["park_no"] = "01,"
        hashMap["pay_amount"] = "0.0"
        hashMap["make_date_time"] = "2021-09-01 10:20:30"
        hashMap["operator_name"] = "Administrator"
        hashMap["enable"] = "1"
        hashMap["park_position"] = "01"
        hashMap["remark"] = "How"
        ProfileWs().putQrCodeToServer(context, hashMap, callBackSendQrCodeListener)
    }

    private fun onClickPushNotification() {
        val status = getStatus(true, statusNotification)
        val hashMap = HashMap<String, String>()
        hashMap["action"] = status
        if (progressBar.visibility == View.GONE) {
            progressBar.visibility = View.VISIBLE
            PushNotificationWS().push_notification(
                mActivity,
                hashMap,
                object : PushNotificationCallBackListener {
                    override fun onSuccess(msgSuccess: String) {
                        progressBar.visibility = View.GONE
                        txtStatusNotification.text = Utils.convertFirstCapital(status)
                        statusNotification = status
                        if (status == "enable") {
                            txtStatusNotification.backgroundTintList =
                                ColorStateList.valueOf(Utils.getColor(mActivity, R.color.green))
                        } else {
                            txtStatusNotification.backgroundTintList =
                                ColorStateList.valueOf(
                                    ContextCompat.getColor(
                                        mActivity,
                                        R.color.red
                                    )
                                )
                        }

                        Utils.customToastMsgError(mActivity, msgSuccess, true)
                    }

                    override fun onFailed(mess: String) {
                        progressBar.visibility = View.GONE
                        Utils.customToastMsgError(mActivity, mess, false)
                    }
                })
        }
    }

}