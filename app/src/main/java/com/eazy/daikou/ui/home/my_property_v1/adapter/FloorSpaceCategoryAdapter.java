package com.eazy.daikou.ui.home.my_property_v1.adapter;

import static android.view.View.VISIBLE;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.eazy.daikou.request_data.static_data.StaticUtilsKey;
import com.github.ybq.android.spinkit.sprite.Sprite;
import com.github.ybq.android.spinkit.style.MultiplePulse;
import com.google.gson.Gson;
import com.eazy.daikou.R;
import com.eazy.daikou.model.floor_plan.FloorSpaceCategoryModel;
import com.eazy.daikou.model.floor_plan.FloorSpaceItemModel;
import com.eazy.daikou.model.floor_plan.FloorUnitItemModel;
import com.eazy.daikou.model.floor_plan.FloorUnitTypeItemModel;
import com.eazy.daikou.model.profile.User;
import com.eazy.daikou.ui.home.my_property_v1.floor_plan_menu.FloorPlanActivity;
import com.eazy.daikou.request_data.request.service_property_ws.FloorListWs;
import com.eazy.daikou.helper.UserSessionManagement;

import java.util.ArrayList;
import java.util.List;

public class FloorSpaceCategoryAdapter extends RecyclerView.Adapter<FloorSpaceCategoryAdapter.ItemViewHolder> {

    private FloorSpaceListAdapter commercialAdapterSpace,commercialAdapterUnit;
    private final RelativeLayout layout;
    private final ClickBackListener backListener;
    private final UserSessionManagement cookieToken;
    private final Context context;

    private final List<FloorSpaceItemModel> spaceItemSpaceList = new ArrayList<>();
    private final List<FloorUnitItemModel> spaceItemUnitList = new ArrayList<>();
    private final List<FloorUnitTypeItemModel> spaceItemUnitTypeList = new ArrayList<>();
    private final List<FloorSpaceCategoryModel> spaceCatList;
    private final String floor_no, storey_id;
    private final int selectedPosition = -1;
    private int currentPage = 1, currentPage2 = 1, currentPage3 = 1;

    public FloorSpaceCategoryAdapter(RelativeLayout layout, Context context, UserSessionManagement cookieToken, String floor_no, String storey_id, List<FloorSpaceCategoryModel> spaceCatList, ClickBackListener backListener) {
        this.spaceCatList = spaceCatList;
        this.cookieToken = cookieToken;
        this.backListener = backListener;
        this.floor_no = floor_no;
        this.storey_id = storey_id;
        this.context = context;
        this.layout = layout;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.space_category_model, parent, false);
        return new ItemViewHolder(view);
    }

    @SuppressLint("NotifyDataSetChanged")
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        FloorSpaceCategoryModel spaceCategoryModel = spaceCatList.get(position);
        User user = new Gson().fromJson(cookieToken.getUserDetail(),User.class);
        if (spaceCategoryModel != null){
            holder.btnSpaceCat.setText(spaceCategoryModel.getName());
            holder.btnSpaceCat.setTextColor(Color.parseColor("#000000"));
            onLoading(holder.progressBar);

            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
            holder.subList.setLayoutManager(linearLayoutManager);

            if (spaceCategoryModel.isClick()){
                if (StaticUtilsKey.is_check_click_category){
                    if (spaceCategoryModel.getId().equalsIgnoreCase("unit") || spaceCategoryModel.getId().equalsIgnoreCase("unit_type")) {
                        onLoading(holder.progressBar);
                        if (spaceCategoryModel.getId().equalsIgnoreCase("unit")){
                            commercialAdapterUnit = new FloorSpaceListAdapter(spaceItemUnitList, spaceCategoryModel.getId(), clickComFloorCallback);
                        } else {
                            commercialAdapterUnit = new FloorSpaceListAdapter(spaceItemUnitTypeList, spaceCategoryModel.getId(), floor_no, clickComFloorCallback);
                        }
                        holder.subList.setAdapter(commercialAdapterUnit);

                        initRecyclerViewUnit(holder.noItem, holder.seeMore, holder.subList, holder.progressBar, spaceCategoryModel.getId(), user.getActivePropertyIdFk(), clickComFloorCallback);
                    } else {
                        onLoading(holder.progressBar);
                        commercialAdapterSpace = new FloorSpaceListAdapter(spaceItemSpaceList,"space", spaceCategoryModel.getId(), storey_id, clickComFloorCallback);
                        holder.subList.setAdapter(commercialAdapterSpace);

                        initRecyclerViewSpace(holder.noItem, holder.seeMore, holder.subList, holder.progressBar,spaceCategoryModel.getId(), clickComFloorCallback);
                    }
                }

                holder.btnSpaceCat.setBackground(context.getDrawable(R.drawable.bg_shap_color_appbar));
                holder.btnSpaceCat.setTextColor(ContextCompat.getColor(context,R.color.white));
            } else {
                holder.progressBar.setVisibility(View.GONE);
                holder.btnSpaceCat.setBackground(context.getDrawable(R.drawable.card_view_shape));
                holder.btnSpaceCat.setTextColor(ContextCompat.getColor(context,R.color.black));
            }


            holder.btnSpaceCat.setOnClickListener(view -> {
                StaticUtilsKey.is_check_click_category = false;
                for (FloorSpaceCategoryModel spaceCategory : spaceCatList) {
                    spaceCategory.setClick(spaceCategory.getName().equalsIgnoreCase(spaceCategoryModel.getName()));
                }
                holder.btnSpaceCat.setBackground(context.getDrawable(R.drawable.bg_shap_color_appbar));
                holder.btnSpaceCat.setTextColor(ContextCompat.getColor(context,R.color.white));

                if (selectedPosition != position) notifyDataSetChanged();

                if (spaceCategoryModel.getId().equalsIgnoreCase("unit") || spaceCategoryModel.getId().equalsIgnoreCase("unit_type")) {
                    if (spaceCategoryModel.getId().equalsIgnoreCase("unit")){
                        frameRemoveAllViewDraw(layout, "unit");
                        commercialAdapterUnit = new FloorSpaceListAdapter(spaceItemUnitList, spaceCategoryModel.getId(), clickComFloorCallback);

                    } else {
                        frameRemoveAllViewDraw(layout, "unit_type");
                        commercialAdapterUnit = new FloorSpaceListAdapter(spaceItemUnitTypeList, spaceCategoryModel.getId(), floor_no, clickComFloorCallback);
                    }
                    holder.subList.setAdapter(commercialAdapterUnit);

                    initRecyclerViewUnit(holder.noItem, holder.seeMore, holder.subList, holder.progressBar, spaceCategoryModel.getId(), user.getActivePropertyIdFk(), clickComFloorCallback);
                } else {
                    frameRemoveAllViewDraw(layout, "space");
                    commercialAdapterSpace = new FloorSpaceListAdapter(spaceItemSpaceList,"space", spaceCategoryModel.getId(), storey_id, clickComFloorCallback);
                    holder.subList.setAdapter(commercialAdapterSpace);

                    initRecyclerViewSpace(holder.noItem, holder.seeMore, holder.subList, holder.progressBar,spaceCategoryModel.getId(), clickComFloorCallback);
                }

            });

            if (selectedPosition == position) {
                if (holder.subList.getVisibility() == View.VISIBLE) {
                    holder.subList.setVisibility(View.GONE);
                } else {
                    holder.subList.setVisibility(View.VISIBLE);
                }
            } else {
                holder.subList.setVisibility(View.GONE);
            }
            holder.seeMore.setVisibility(View.GONE);
            holder.noItem.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return spaceCatList.size();
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder{

        private final TextView btnSpaceCat;
        private final RecyclerView subList;
        private final ProgressBar progressBar;
        private final TextView seeMore, noItem;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            noItem = itemView.findViewById(R.id.noItem);
            seeMore = itemView.findViewById(R.id.seeMore);
            progressBar = itemView.findViewById(R.id.progress);
            btnSpaceCat = itemView.findViewById(R.id.btnSpaceCategory);
            subList = itemView.findViewById(R.id.subList);
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private void frameRemoveAllViewDraw(RelativeLayout frameLayout, String type){
        boolean doBreak = false;
        while (!doBreak) {
            int childCount = frameLayout.getChildCount();
            int i;
            for(i = 0; i < childCount; i++) {
                View currentChild = frameLayout.getChildAt(i);
                if (currentChild instanceof FloorPlanActivity.SampleCanvasActivity) {
                    frameLayout.removeView(currentChild);
                    break;
                }
                if (currentChild instanceof TextView) {
                    frameLayout.removeView(currentChild);
                    break;
                }
            }

            if (i == childCount) {
                doBreak = true;
            }
        }

        if (type.equalsIgnoreCase("unit") || type.equalsIgnoreCase("unit_type")){
            if (commercialAdapterUnit != null) commercialAdapterUnit.notifyDataSetChanged();
        } else {
            if (commercialAdapterSpace != null) commercialAdapterSpace.notifyDataSetChanged();
        }
    }

    private void initRecyclerViewSpace(TextView noItem, TextView seeMore, RecyclerView recyclerView, ProgressBar progressBar, String space_id, FloorSpaceListAdapter.ClickBackListener clickComFloorCallback){
        onLoading(progressBar);
        new FloorListWs().getSpaceItemSpaceList(context, "space", space_id,
                storey_id, currentPage, 10, new FloorListWs.FloorSpaceList() {
                    @SuppressLint("NotifyDataSetChanged")
                    @Override
                    public void getSpaceList(List<FloorSpaceItemModel> floorList) {
                        recyclerView.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);
                        spaceItemSpaceList.addAll(floorList);
                        commercialAdapterSpace.notifyDataSetChanged();

                        noItem.setVisibility(spaceItemSpaceList.size() == 0 ? View.VISIBLE : View.GONE);
                        recyclerView.setVisibility(spaceItemSpaceList.size() > 0 ? View.VISIBLE : View.GONE);

                        currentPage++;
                        seeMore.setVisibility(floorList.size() < 8 ? View.GONE : View.VISIBLE);

                        seeMore.setOnClickListener(view1 -> {
                            if (floorList.size() == 0 ){
                                seeMore.setVisibility(View.INVISIBLE);
                            } else {
                                if (floorList.size() < 8 ){
                                    seeMore.setVisibility(View.INVISIBLE);
                                } else {
                                    frameRemoveAllViewDraw(layout, "space");
                                    seeMore.setVisibility(View.VISIBLE);
                                    onLoading(progressBar);
                                    initRecyclerViewSpace(noItem, seeMore, recyclerView, progressBar,space_id, clickComFloorCallback);
                                }

                            }
                        });
                    }

                    @Override
                    public void onFailed(String error) {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void initRecyclerViewUnit(TextView noItem, TextView seeMore, RecyclerView recyclerView, ProgressBar progressBar, String category_type, String property_id,  FloorSpaceListAdapter.ClickBackListener clickComFloorCallback){
        if (category_type.equalsIgnoreCase("unit")){
            onLoading(progressBar);
            new FloorListWs().getSpaceItemUnitList(context, category_type, property_id, floor_no,
                    currentPage2, 10, new FloorListWs.FloorUnitList() {
                        @SuppressLint("NotifyDataSetChanged")
                        @Override
                        public void getUnitList(List<FloorUnitItemModel> floorList) {
                            recyclerView.setVisibility(View.VISIBLE);
                            progressBar.setVisibility(View.GONE);
                            spaceItemUnitList.addAll(floorList);
                            commercialAdapterUnit.notifyDataSetChanged();

                            noItem.setVisibility(spaceItemUnitList.size() == 0 ? View.VISIBLE : View.GONE);
                            recyclerView.setVisibility(spaceItemUnitList.size() > 0 ? View.VISIBLE : View.GONE);

                            currentPage2++;
                            seeMore.setVisibility(floorList.size() < 8 ? View.GONE : View.VISIBLE);

                            seeMore.setOnClickListener(view1 -> {
                                if (floorList.size() == 0 ){
                                    seeMore.setVisibility(View.INVISIBLE);
                                } else {
                                    if (floorList.size() < 8 ){
                                        seeMore.setVisibility(View.INVISIBLE);
                                    } else {
                                        frameRemoveAllViewDraw(layout, "space");
                                        seeMore.setVisibility(View.VISIBLE);
                                        onLoading(progressBar);
                                        initRecyclerViewUnit(noItem, seeMore, recyclerView, progressBar, category_type, property_id, clickComFloorCallback);
                                    }
                                }
                            });

                        }

                        @Override
                        public void onFailed(String error) {
                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
                        }
                    });
        } else {
            onLoading(progressBar);
            new FloorListWs().getSpaceItemUnitTypeList(context, category_type, property_id, floor_no,
                    currentPage3, 10, new FloorListWs.FloorUnitTypeList() {
                        @SuppressLint("NotifyDataSetChanged")
                        @Override
                        public void getUnitTypeList(List<FloorUnitTypeItemModel> floorList) {
                            recyclerView.setVisibility(View.VISIBLE);
                            progressBar.setVisibility(View.GONE);
                            spaceItemUnitTypeList.addAll(floorList);
                            commercialAdapterUnit.notifyDataSetChanged();

                            noItem.setVisibility(spaceItemUnitTypeList.size() == 0 ? View.VISIBLE : View.GONE);
                            recyclerView.setVisibility(spaceItemUnitTypeList.size() > 0 ? View.VISIBLE : View.GONE);

                            currentPage3++;
                            seeMore.setVisibility(floorList.size() < 8 ? View.GONE : View.VISIBLE);
                            seeMore.setOnClickListener(view1 -> {
                                if (floorList.size() == 0 ){
                                    seeMore.setVisibility(View.INVISIBLE);
                                } else {
                                    if (floorList.size() < 8 ){
                                        seeMore.setVisibility(View.INVISIBLE);
                                    } else {
                                        frameRemoveAllViewDraw(layout, "space");
                                        seeMore.setVisibility(View.VISIBLE);
                                        onLoading(progressBar);
                                        initRecyclerViewUnit(noItem, seeMore, recyclerView, progressBar, category_type, property_id, clickComFloorCallback);

                                    }
                                }

                            });

                        }

                        @Override
                        public void onFailed(String error) {
                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
                        }
                    });
        }

    }

    //On loading
    private void onLoading(ProgressBar progressBar){
        Sprite doubleBounce = new MultiplePulse();
        progressBar.setIndeterminateDrawable(doubleBounce);
        progressBar.setVisibility(VISIBLE);
    }

    public final FloorSpaceListAdapter.ClickBackListener clickComFloorCallback = new FloorSpaceListAdapter.ClickBackListener() {
        @Override
        public void clickBackSpaceFloor(RelativeLayout layout, FloorSpaceItemModel floorSpaceItemModel, String storey_id, String category_type, String fullNameCategory) {
            backListener.clickBackSpaceFloor(layout, floorSpaceItemModel, storey_id, category_type, fullNameCategory);
        }

        @Override
        public void clickBackUnitFloor(RelativeLayout layout, FloorUnitItemModel floorSpaceItemModel, String category_type, String fullNameCategory) {
            backListener.clickBackUnitFloor(layout, floorSpaceItemModel, category_type, fullNameCategory);
        }

        @Override
        public void clickBackUnitTypeFloor(RelativeLayout layout, String floorUnitTypeId, String category_type, String floor_no, String fullNameCategory) {
            backListener.clickBackUnitTypeFloor(layout, floorUnitTypeId, category_type, floor_no, fullNameCategory);
        }

        @Override
        public void clickBackSpaceFloorClose(LinearLayout layout, List<FloorSpaceItemModel> spaceItemList, String fullNameCategory) {
            backListener.clickBackSpaceFloorClose(layout, spaceItemList, fullNameCategory);
        }

        @Override
        public void clickBackUnitFloorClose(LinearLayout layout,String fullNameCategory) {
            backListener.clickBackUnitFloorClose(layout, fullNameCategory);
        }

        @Override
        public void clickBackUnitTypeFloorClose(LinearLayout layout,String fullNameCategory) {
            backListener.clickBackUnitTypeFloorClose(layout, fullNameCategory);
        }

    };


    public interface ClickBackListener {
        void clickBackSpaceFloor(RelativeLayout layout, FloorSpaceItemModel floorSpaceItemModel, String storey_id, String category_type, String fullNameCategory);
        void clickBackUnitFloor(RelativeLayout layout, FloorUnitItemModel floorSpaceItemModel, String category_type, String fullNameCategory);
        void clickBackUnitTypeFloor(RelativeLayout layout, String floorUnitTypeId, String category_type, String floor_no, String fullNameCategory);

        void clickBackSpaceFloorClose(LinearLayout layout, List<FloorSpaceItemModel> spaceItemList, String fullNameCategory);
        void clickBackUnitFloorClose(LinearLayout layout, String fullNameCategory);
        void clickBackUnitTypeFloorClose(LinearLayout layout, String fullNameCategory);
    }
}
