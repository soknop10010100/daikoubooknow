package com.eazy.daikou.ui.profile

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.request_data.request.book_now_ws.RequestHashMapData
import com.eazy.daikou.request_data.request.profile_ws.LoginWs
import com.eazy.daikou.helper.CheckIsAppActive.Companion.is_daikou_active
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.ui.MainActivity
import com.eazy.daikou.ui.home.book_now.booking_hotel.HotelBookingMainActivity
import com.poovam.pinedittextfield.SquarePinField
import com.rilixtech.widget.countrycodepicker.CountryCodePicker

class ProfileDeactivedAccountActivity : BaseActivity() {

    private lateinit var pinCode: SquarePinField
    private lateinit var titleCodeTv : TextView
    private lateinit var reasonEdt : EditText
    private lateinit var progressBar: ProgressBar
    private lateinit var getCodeTv : TextView
    private lateinit var ed_phone : EditText
    private lateinit var countryCodePicker: CountryCodePicker
    private var countryCode = ""
    private var isActionBtnDone = false
    private lateinit var btnDelete : TextView
    private lateinit var mainActionDeasactive : LinearLayout
    private lateinit var doneActionDeasactive : LinearLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_confirm_phone)

        initView()

        initAction()

    }

    private fun initView(){
        pinCode = findViewById(R.id.pin_code)
        titleCodeTv = findViewById(R.id.titleCodeTv)
        reasonEdt = findViewById(R.id.reasonEdt)
        progressBar = findViewById(R.id.progressItem)
        getCodeTv = findViewById(R.id.getCodeTv)
        progressBar.visibility = View.GONE
        ed_phone = findViewById(R.id.ed_forgot_phone)
        btnDelete = findViewById(R.id.btn_delete)
        findViewById<LinearLayout>(R.id.layoutMap).visibility = View.INVISIBLE

        Utils.customOnToolbar(this, resources.getString(R.string.delete_account).uppercase()){finish()}

        countryCodePicker = findViewById(R.id.ccp)
        mainActionDeasactive = findViewById(R.id.mainActionDeasactive)
        doneActionDeasactive = findViewById(R.id.doneActionDeasactive)
    }

    private fun initAction(){
        countryCodePicker.setDefaultCountryUsingNameCodeAndApply(country_code)
        countryCode = countryCodePicker.fullNumber
        countryCodePicker.setOnCountryChangeListener { countryCode = countryCodePicker.fullNumber }

        setPinCode()

        pinCode.isEnabled = false
        getCodeTv.setOnClickListener(
            CustomSetOnClickViewListener{
                if (ed_phone.text.toString().isEmpty()){
                    Utils.customToastMsgError(this, resources.getString(R.string.phone_number_is_require), false)
                } else {
                    requestServiceWs("get_pin_code", hashMapData("get_pin_code"))
                }
            }
        )

        btnDelete.setOnClickListener(
            CustomSetOnClickViewListener{
                if (isActionBtnDone){
                    startActivity(Intent(this@ProfileDeactivedAccountActivity,
                        if (is_daikou_active) MainActivity::class.java else HotelBookingMainActivity::class.java))
                    finish()
                } else {
                    if (reasonEdt.text.toString().isEmpty()){
                        Utils.customToastMsgError(this, resources.getString(R.string.enter_reason), false)
                    } else {
                        val hashMap: HashMap<String, Any> = RequestHashMapData.hashMapUserActiveAccount("deactivate", UserSessionManagement(this).userId)
                        requestServiceWs("deactivate", hashMap)
                    }
                }
            }
        )
    }

    private fun requestServiceWs(action: String, hashMap: HashMap<String, Any>){
        progressBar.visibility = View.VISIBLE
        LoginWs().sendVerifyCode(this, action, hashMap, callBackServiceWS)
    }

    private val callBackServiceWS = object : LoginWs.OnDeleteAccListener{
        override fun onSuccess(action : String, msg: String?) {
            progressBar.visibility = View.GONE
            pinCode.isEnabled = true
            Utils.hideSoftKeyboard(findViewById(R.id.pin_code))
            Utils.customToastMsgError(this@ProfileDeactivedAccountActivity, msg, true)

            checkViewAction(action)
        }

        override fun onFailed(mess: String?) {
            progressBar.visibility = View.GONE
            Utils.hideSoftKeyboard(findViewById(R.id.pin_code))
            pinCode.text?.clear()
            Utils.customToastMsgError(this@ProfileDeactivedAccountActivity, mess, false)
        }

    }

    private fun setPinCode() {
        pinCode.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun afterTextChanged(editable: Editable) {
                if (editable.length == 6) {
                    Utils.hideSoftKeyboard(findViewById(R.id.pin_code))
                    requestServiceWs("verify_pin", hashMapData("verify"))
                }
            }
        })
    }

    private fun checkViewAction(action: String){
        when (action) {
            "get_pin_code" -> {
                pinCode.isEnabled = true
            }
            "verify_pin" -> {
                pinCode.visibility = View.GONE
                reasonEdt.visibility = View.VISIBLE
                titleCodeTv.text = resources.getString(R.string.enter_reason_for_delete_accont)
                getCodeTv.isEnabled = false
                Utils.setBgTint(getCodeTv, R.color.gray)
                ed_phone.isEnabled = false
                countryCodePicker.isClickable = false
                btnDelete.isEnabled = true
                Utils.setBgTint(btnDelete, R.color.red)
            }
            "deactivate" -> {
                btnDelete.text = resources.getString(R.string.back_to_home_screen)
                Utils.setBgTint(btnDelete, R.color.appBarColorOld)
                isActionBtnDone = true
                doneActionDeasactive.visibility = View.VISIBLE
                mainActionDeasactive.visibility = View.GONE
            }
        }
    }

    private fun hashMapData(action : String) : HashMap<String, Any>{
        val hashMap : HashMap<String, Any> = HashMap()
        hashMap["send_by"] = "by_phone"
        hashMap["phone_code"] = countryCode
        hashMap["phone_no"] = ed_phone.text.toString().trim()
        if(action == "get_pin_code"){
            hashMap["action"] = "send"
        } else{
            hashMap["action"] = "verify"
            hashMap["verify_code"] = pinCode.text.toString().trim()
        }
        return hashMap
    }

}