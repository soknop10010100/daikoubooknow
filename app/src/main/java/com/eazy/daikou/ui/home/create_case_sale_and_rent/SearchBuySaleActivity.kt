package com.eazy.daikou.ui.home.create_case_sale_and_rent

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.model.buy_sell.SaleAndRentProperties
import com.eazy.daikou.ui.home.create_case_sale_and_rent.adapter.BuyRentSaleAdapter

class SearchBuySaleActivity : BaseActivity() {

    private lateinit var textSearch: EditText
    private lateinit var recyclerView: RecyclerView
    private lateinit var btnClear: ImageView
    private lateinit var emptyTextTv: TextView
    private lateinit var progressBar: ProgressBar
    private var buySaleRentModelList : ArrayList<SaleAndRentProperties> = ArrayList()
    private var currentItem = 0
    private var total : Int = 0
    private var scrollDown : Int = 0
    private var currentPage : Int = 1
    private var size : Int = 10
    private var isScrolling = false
    private lateinit var buyRentSaleAdapter : BuyRentSaleAdapter
    private lateinit var linearLayoutManager: LinearLayoutManager
    private var purposeType = ""
    private var keySearch = ""
    private var isScroll = false
    private lateinit var numProperty : TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_buy_sale)

        // init view
        findViewById<View>(R.id.backButton).setOnClickListener { finish() }
        textSearch = findViewById(R.id.search_store_name)
        recyclerView = findViewById(R.id.list_provider)
        btnClear = findViewById(R.id.clear)
        emptyTextTv = findViewById(R.id.emptyText)
        progressBar = findViewById(R.id.progressItem)
        progressBar.visibility = View.GONE
        emptyTextTv.visibility = View.VISIBLE
        numProperty = findViewById(R.id.numProperty)

        // init data
        if (intent !=  null && intent.hasExtra("purpose")){
            purposeType = intent.getStringExtra("purpose").toString()
        }

        // init action
        linearLayoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = linearLayoutManager
        //buyRentSaleAdapter = BuyRentSaleAdapter(buySaleRentModelList)
        recyclerView.adapter = buyRentSaleAdapter

        onScrollInfoRecycle()

        textSearch.setOnEditorActionListener { _: TextView?, i: Int, _: KeyEvent? ->
            if (i == EditorInfo.IME_ACTION_SEARCH) {
                requestListItem(textSearch.text.toString())
            }
            true
        }

        textSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun afterTextChanged(editable: Editable) =
                if (editable.toString().isNotEmpty()) {
                    isScroll = false
                    keySearch = editable.toString()
                    btnClear.visibility = View.VISIBLE
                    currentPage = 1
                    size = 10
                    isScrolling = true

                    requestListItem(editable.toString())
                    btnClear.setOnClickListener {
                        clearText()
                        textSearch.setText("")
                    }
                } else {
                    keySearch = editable.toString()
                    clearText()
                }
        })
    }

    private fun requestListItem(keySearch : String){
        progressBar.visibility = View.VISIBLE
            // BuySaleRentWs().getListItemBuySale(this, currentPage, 10, UserSessionManagement(this).userId, purposeType, keySearch, "", "", callBackListener)
    }


    @SuppressLint("NotifyDataSetChanged")
    private fun clearText() {
        btnClear.visibility = View.GONE
        if (buySaleRentModelList.size > 0) buySaleRentModelList.clear()
        progressBar.visibility = View.GONE
        emptyTextTv.visibility = View.VISIBLE
        currentPage = 1
        size = 10
        isScrolling = true

        buyRentSaleAdapter.notifyDataSetChanged()
    }

    private fun onScrollInfoRecycle() {
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true
                }
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                currentItem = linearLayoutManager.childCount
                total = linearLayoutManager.itemCount
                scrollDown = linearLayoutManager.findFirstVisibleItemPosition()
                if (dy > 0) {
                    if (isScrolling && currentItem + scrollDown == total) {
                        if (total == size) {
                            isScroll = true
                            isScrolling = false
                            currentPage++
                            progressBar.visibility = View.VISIBLE
                            recyclerView.scrollToPosition(buySaleRentModelList.size - 1)
                            requestListItem(keySearch)
                            size += 10
                        } else {
                            progressBar.visibility = View.GONE
                        }
                    }
                }
            }
        })
    }
}