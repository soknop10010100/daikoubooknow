package com.eazy.daikou.ui.home.book_now.booking_hotel;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import com.anychart.AnyChart;
import com.anychart.AnyChartView;
import com.anychart.chart.common.dataentry.DataEntry;
import com.anychart.chart.common.dataentry.ValueDataEntry;
import com.anychart.charts.Cartesian;
import com.anychart.core.cartesian.series.Line;
import com.anychart.data.Mapping;
import com.anychart.data.Set;
import com.anychart.enums.Anchor;
import com.anychart.enums.MarkerType;
import com.anychart.enums.TooltipPositionMode;
import com.anychart.graphics.vector.Stroke;
import com.eazy.daikou.R;
import com.eazy.daikou.base.BaseActivity;
import com.eazy.daikou.base.MockUpData;
import com.eazy.daikou.request_data.request.book_now_ws.BookingHotelWS;
import com.eazy.daikou.helper.UserSessionManagement;
import com.eazy.daikou.helper.Utils;
import com.eazy.daikou.model.booking_hotel.HotelDashboardModel;
import com.eazy.daikou.model.booking_hotel.DashboardModel;
import com.eazy.daikou.ui.home.book_now.booking_hotel.adapter.HotelDashboardAdapter;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class HotelDashboardActivity extends BaseActivity {

    private RecyclerView recyclerView;
    private GraphView graphView;
    private ProgressBar progressBar;
    private String userEazyId = "";
    private HotelDashboardAdapter dashboardAdapter;
    private final List<DashboardModel> dashboardModelList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard_v2);

        initView();

        initAction();
    }

    private void initView() {
        recyclerView = findViewById(R.id.recyclerItemDashboard);
        graphView = findViewById(R.id.idGraphView);
        Utils.customOnToolbar(this, getResources().getString(R.string.title_dashboard), this::finish);
        progressBar = findViewById(R.id.progressItem);
    }
    @SuppressLint("SetTextI18n")
    private void initAction() {
        userEazyId = Utils.validateNullValue(MockUpData.getEazyHotelUserId(new UserSessionManagement(this)));

        recyclerView.setLayoutManager(new GridLayoutManager(this,2));
        dashboardAdapter = new HotelDashboardAdapter(this, dashboardModelList);
        recyclerView.setAdapter(dashboardAdapter);

        requestDataWs();
    }

    private void requestDataWs(){
        progressBar.setVisibility(View.VISIBLE);
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("eazyhotel_user_id", userEazyId);
        new BookingHotelWS().getDashboardHotelWs(this, hashMap, new BookingHotelWS.OnCallBackDashboardListener() {
            @Override
            public void onSuccessDataDashboard(@NonNull HotelDashboardModel hotelDashboardModel) {
                progressBar.setVisibility(View.GONE);
                dashboardModelList.clear();
                setValueItem(hotelDashboardModel);
            }

            @Override
            public void onFailed(@NonNull String message) {
                progressBar.setVisibility(View.GONE);
                Utils.customToastMsgError(HotelDashboardActivity.this, message, false);
            }
        });
    }

    @SuppressLint("NotifyDataSetChanged")
    private  void setValueItem(HotelDashboardModel hotelDashboardModel){
        dashboardModelList.add(new DashboardModel("total_pending", ResourcesCompat.getDrawable(getResources(), R.drawable.compaint_select_90, null), getResources().getString(R.string.total_peding), hotelDashboardModel.getTotal_pending()));
        dashboardModelList.add(new DashboardModel("total_earning",ResourcesCompat.getDrawable(getResources(), R.drawable.ic_baseline_sync_24, null), getResources().getString(R.string.total_earning), hotelDashboardModel.getTotal_earning()));
        dashboardModelList.add(new DashboardModel("total_booking",ResourcesCompat.getDrawable(getResources(), R.drawable.home_project_development_v2, null), getResources().getString(R.string.total_booking), hotelDashboardModel.getTotal_booking()));
        dashboardModelList.add(new DashboardModel("total_bookable_service",ResourcesCompat.getDrawable(getResources(), R.drawable.ic_property_service, null), getResources().getString(R.string.total_bookable_service), hotelDashboardModel.getTotal_bookable_service()));

        dashboardAdapter.notifyDataSetChanged();
    }

    private void  onCreateGraph(){
        LineGraphSeries<DataPoint> series = new LineGraphSeries<>(new DataPoint[]{
                new DataPoint(0, 200),
                new DataPoint(1, 400),
                new DataPoint(2, 400),
                new DataPoint(3, 300),
                new DataPoint(4, 600),
                new DataPoint(5, 700),
                new DataPoint(6, 700),
                new DataPoint(7, 900),
                new DataPoint(8, 990)
        });

        // after adding data to our line graph series.
        // on below line we are setting
        // title for our graph view.
        graphView.setTitle(getResources().getString(R.string.app_name));

        // on below line we are setting
        // text color to our graph view.
        graphView.setTitleColor(R.color.colorPrimary);

        // on below line we are setting
        // our title text size.
        graphView.setTitleTextSize(18);

        // on below line we are adding
        // data series to our graph view.
        graphView.addSeries(series);
    }

    private void drawChart(){
        AnyChartView anyChartView = findViewById(R.id.any_chart_view);
        anyChartView.setProgressBar(progressBar);

        Cartesian cartesian = AnyChart.line();
        cartesian.animation(true);
        cartesian.padding(10d, 20d, 5d, 20d);

        //Set click graph
        cartesian.crosshair().enabled(true);
        cartesian.crosshair()
                .yLabel(true)
                .yStroke( (Stroke) null, null, null, (String) null, (String) null);
        cartesian.tooltip().positionMode(TooltipPositionMode.POINT);

        //Set title Graph on xAxis and yAxis
        cartesian.xAxis(0).labels().padding(10d, 10d, 10d, 10d);
//        cartesian.xAxis(0).title(getResources().getString(R.string.day));
//        cartesian.yAxis(0).title(getResources().getString(R.string.total_usage));

        List<DataEntry> seriesData = addList(); // addList(ownerUsageList.getTrackingData());
        Set set = Set.instantiate();
        set.data(seriesData);

        //Draw grape and hover
        Mapping series1Mapping = set.mapAs("{ x: 'x', value: 'value' }");
        Line series1 = cartesian.line(series1Mapping);
        series1.name(getResources().getString(R.string.total_usage));
        series1.hovered().markers().enabled(true);
        series1.hovered().markers()
                .type(MarkerType.CIRCLE)
                .size(4d);
        series1.tooltip()
                .position("right")
                .anchor(Anchor.CENTER);
        cartesian.legend().enabled(true);
        cartesian.legend().fontSize(13d);
        cartesian.legend().padding(0d, 0d, 10d, 0d);

        anyChartView.setChart(cartesian);
    }

    private List<DataEntry> addList(){
        List<DataEntry> seriesData = new ArrayList<>();
        seriesData.add(new CustomDataEntry("1986", 3.6, 2.3, 2.8));
        seriesData.add(new CustomDataEntry("1987", 7.1, 4.0, 4.1));
        seriesData.add(new CustomDataEntry("1988", 8.5, 6.2, 5.1));
        seriesData.add(new CustomDataEntry("1989", 9.2, 11.8, 6.5));
        seriesData.add(new CustomDataEntry("1990", 10.1, 13.0, 12.5));
        seriesData.add(new CustomDataEntry("1991", 11.6, 13.9, 18.0));
        seriesData.add(new CustomDataEntry("1992", 16.4, 18.0, 21.0));
        seriesData.add(new CustomDataEntry("1993", 18.0, 23.3, 20.3));
        seriesData.add(new CustomDataEntry("1994", 13.2, 24.7, 19.2));
        seriesData.add(new CustomDataEntry("1995", 12.0, 18.0, 14.4));
        seriesData.add(new CustomDataEntry("1996", 3.2, 15.1, 9.2));
        seriesData.add(new CustomDataEntry("1997", 4.1, 11.3, 5.9));
        seriesData.add(new CustomDataEntry("1998", 6.3, 14.2, 5.2));
        seriesData.add(new CustomDataEntry("1999", 9.4, 13.7, 4.7));
        seriesData.add(new CustomDataEntry("2000", 11.5, 9.9, 4.2));
        seriesData.add(new CustomDataEntry("2001", 13.5, 12.1, 1.2));
        seriesData.add(new CustomDataEntry("2002", 14.8, 13.5, 5.4));
        seriesData.add(new CustomDataEntry("2003", 16.6, 15.1, 6.3));
        seriesData.add(new CustomDataEntry("2004", 18.1, 17.9, 8.9));
        seriesData.add(new CustomDataEntry("2005", 17.0, 18.9, 10.1));
        seriesData.add(new CustomDataEntry("2006", 16.6, 20.3, 11.5));
        seriesData.add(new CustomDataEntry("2007", 14.1, 20.7, 12.2));
        seriesData.add(new CustomDataEntry("2008", 15.7, 21.6, 10));
        seriesData.add(new CustomDataEntry("2009", 12.0, 22.5, 8.9));
//        if (list.size() > 0 ){
//            for (int i = 0; i < list.size(); i++){
//                String date = list.get(i).getTrackingDate().substring(8);
//                seriesData.add(new CustomDataEntry(date, Double.parseDouble(list.get(i).getTotalUsage())));
//            }
//        }
        return seriesData;
    }

    //Create new Subclass from ValueDataEntry
    private static class CustomDataEntry extends ValueDataEntry {
        CustomDataEntry(String x, Number value, Number value2, Number value3) {
            super(x, value);
            setValue("value2", value2);
            setValue("value3", value3);
        }
    }

}