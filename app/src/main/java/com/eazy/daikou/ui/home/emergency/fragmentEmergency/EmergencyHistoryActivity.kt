package com.eazy.daikou.ui.home.emergency.fragmentEmergency

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AbsListView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.request_data.request.home_ws.EmergencyWs
import com.eazy.daikou.request_data.request.home_ws.EmergencyWs.CallBackHistoryEmergency
import com.eazy.daikou.helper.CustomStartIntentUtilsClass.Companion.onStartSplashScreenActivity
import com.eazy.daikou.helper.GetDataUtils
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.home.HisEmergencyModel
import com.eazy.daikou.model.profile.User
import com.eazy.daikou.ui.home.emergency.adapter.HistoryEmergencyAdapter
import com.google.android.material.appbar.AppBarLayout
import com.google.gson.Gson

class EmergencyHistoryActivity : BaseActivity() {

    private lateinit var swipeRefreshLayout: SwipeRefreshLayout
    private lateinit var recyclerView: RecyclerView
    private lateinit var noHisEmergency: TextView
    private val hisEmergencyList: ArrayList<HisEmergencyModel> = ArrayList()
    private var linearLayoutManager: LinearLayoutManager? = null
    private var currentItem = 0
    private  var total:Int = 0
    private  var scrollDown:Int = 0
    private  var currentPage:Int = 1
    private  var size:Int = 10
    private var isScrolling = true
    private var user: User? = null
    private var userSession: UserSessionManagement? = null
    private lateinit var progressBar: ProgressBar
    private lateinit var emergencyInfoAdapter: HistoryEmergencyAdapter
    private var listType = ""
    private var actionNotification = ""
    private var emergencyId = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_emergency_history)
        // Change Status Bar To Red
        Utils.changeStatusBarColor(ContextCompat.getColor(this, R.color.red), window)
        findViewById<AppBarLayout>(R.id.appBarLayout).setBackgroundColor(ContextCompat.getColor(this, R.color.red))

        initView()

        initData()

        initAction()
    }

    private fun initView() {
        swipeRefreshLayout = findViewById(R.id.refreshScreen)
        recyclerView = findViewById(R.id.recyclerView)
        noHisEmergency = findViewById(R.id.noHisEmergency)
        progressBar = findViewById(R.id.progressItem)
        progressBar.visibility = View.VISIBLE

        Utils.customOnToolbar(this, resources.getString(R.string.emergency_history)){onPressBack()}
    }

    private fun initData() {
        userSession = UserSessionManagement(this)
        user = Gson().fromJson(userSession!!.userDetail, User::class.java)

        listType = GetDataUtils.getDataFromString("list_type", this)

        actionNotification = GetDataUtils.getDataFromString("action", this)
        emergencyId = GetDataUtils.getDataFromString("emergency_id", this)

    }

    private fun initAction() {
        linearLayoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        recyclerView.layoutManager = linearLayoutManager

        initRecyclerView()

        initScrollRecyclerView()

        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary)

        swipeRefreshLayout.setOnRefreshListener { refreshScreen() }

        // Start From Notification
        if (actionNotification == "emergency") {
            startEmergencyDetail(emergencyId)
        }
    }

    private fun refreshScreen() {
        currentPage = 1
        size = 10
        isScrolling = true
        if (emergencyInfoAdapter != null) emergencyInfoAdapter.clear()

        requestListService()
    }

    private fun initRecyclerView() {
        emergencyInfoAdapter = HistoryEmergencyAdapter(hisEmergencyList, this, clickBackListener)
        recyclerView.adapter = emergencyInfoAdapter

        requestListService()
    }

    private fun requestListService() {
        progressBar.visibility = View.VISIBLE
        val hisHashMap = HashMap<String, Any>()
        hisHashMap["page"] = currentPage
        hisHashMap["limit"] = 10
        hisHashMap["active_account_id"] = Utils.validateNullValue(user!!.accountId)
        hisHashMap["user_id"] = Utils.validateNullValue(userSession!!.userId)
        hisHashMap["list_type"] = listType

        EmergencyWs().getHistoryEmergency(this, hisHashMap, hisEmergencyCallBack)
    }

    private val hisEmergencyCallBack: CallBackHistoryEmergency = object : CallBackHistoryEmergency {
        @SuppressLint("NotifyDataSetChanged")
        override fun hisEmergencyModel(hisEmergencyModels: List<HisEmergencyModel>) {
            progressBar.visibility = View.GONE
            hisEmergencyList.addAll(hisEmergencyModels)
            emergencyInfoAdapter.notifyDataSetChanged()

            noHisEmergency.visibility = if (hisEmergencyList.size == 0) View.VISIBLE else View.GONE
            recyclerView.visibility = if (hisEmergencyList.size > 0) View.VISIBLE else View.GONE

            swipeRefreshLayout.isRefreshing = false
        }

        override fun onFailed(error: String) {
            swipeRefreshLayout.isRefreshing = false
            progressBar.visibility = View.GONE
            Utils.customToastMsgError(this@EmergencyHistoryActivity, error, false)
        }
    }

    private val clickBackListener: HistoryEmergencyAdapter.ClickBackListener =
        HistoryEmergencyAdapter.ClickBackListener { id -> startEmergencyDetail(id) }

    private fun startEmergencyDetail(complaint_id: String) {
        val intent = Intent(this, HistoryEmergencyDetailActivity::class.java)
        intent.putExtra("emergency_id", complaint_id)
        resultLauncher.launch(intent)
    }

    private fun initScrollRecyclerView() {
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true
                }
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                currentItem = linearLayoutManager!!.childCount
                total = linearLayoutManager!!.itemCount
                scrollDown = linearLayoutManager!!.findFirstVisibleItemPosition()
                if (dy > 0) {
                    if (isScrolling && currentItem + scrollDown == total) {
                        if (total == size) {
                            isScrolling = false
                            currentPage++
                            progressBar.visibility = View.VISIBLE
                            recyclerView.scrollToPosition(hisEmergencyList.size - 1)
                            initRecyclerView()
                            size += 10
                        }
                    }
                }
            }
        })
    }

    private var resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode == Activity.RESULT_OK) {
            refreshScreen()
        }
    }

    override fun onBackPressed() {
        onPressBack()
    }

    private fun onPressBack() {
        if (listType.equals("emergency", ignoreCase = true)) {
            onStartSplashScreenActivity(this, "already_emergency")
            finishAffinity()
        } else {
            finish()
        }
    }
}