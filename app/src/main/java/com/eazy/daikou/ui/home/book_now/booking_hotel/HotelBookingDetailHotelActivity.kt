package com.eazy.daikou.ui.home.book_now.booking_hotel

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.activity.result.contract.ActivityResultContracts
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.widget.NestedScrollView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.base.MockUpData
import com.eazy.daikou.request_data.request.book_now_ws.BookingHotelWS
import com.eazy.daikou.request_data.request.book_now_ws.RequestHashMapData
import com.eazy.daikou.request_data.static_data.StaticUtilsKey
import com.eazy.daikou.helper.*
import com.eazy.daikou.helper.Utils.spanGridImageLayoutManager
import com.eazy.daikou.helper.Utils.startOpenLinkMaps
import com.eazy.daikou.helper.map.MapsGenerateClass
import com.eazy.daikou.model.booking_hotel.*
import com.eazy.daikou.model.home.MainItemHomeModel
import com.eazy.daikou.model.home.SubItemHomeModel
import com.eazy.daikou.model.home.UserDeactivated
import com.eazy.daikou.model.profile.User
import com.eazy.daikou.ui.home.book_now.booking_hotel.adapter.*
import com.faltenreich.skeletonlayout.Skeleton
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.material.appbar.AppBarLayout
import com.google.gson.Gson
import de.hdodenhof.circleimageview.CircleImageView
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.abs


class HotelBookingDetailHotelActivity : BaseActivity(){

    private lateinit var progressBar: ProgressBar
    private lateinit var recyclerView: RecyclerView
    private var hotelId = ""
    private lateinit var priceTv : TextView
    private lateinit var ratingTv : RatingBar
    private var getPrice = ""
    private lateinit var addressTv : TextView
    private lateinit var hotelNameTv : TextView
    private lateinit var priceOriginalTv : TextView
    private lateinit var descriptionTv : TextView
    private lateinit var mapFragment : SupportMapFragment
    private lateinit var btnSelectRoom : LinearLayout
    private var startDate : String = ""
    private var endDate : String = ""
    private var adults = ""
    private var children = ""
    private lateinit var noImage : TextView
    private lateinit var facilityHotelTv : TextView
    private lateinit var serviceHotelTv : TextView
    private lateinit var businessTv : TextView
    private lateinit var hotelPoliciesRecycler : RecyclerView
    private lateinit var hotelRelatedRecycler : RecyclerView

    private var reviewHotelModelList : ArrayList<ReviewHotelModel> = ArrayList()
    private lateinit var btnSeeAllReview : TextView
    private lateinit var btnWriteAndReview : TextView
    private lateinit var hotelReviewRecycler : RecyclerView
    private lateinit var imgProfile : CircleImageView
    private var sharedLink : String = ""
    private var hotelName = ""
    private var isWishListHotel = false
    private lateinit var iconWishlist : ImageView
    private lateinit var titleToolbar : TextView

    private var extraPriceList : ArrayList<ExtraPrice> = ArrayList()
    private var price = 0.0
    private var category = ""

    private var getHotelDetailItemModel: HotelBookingDetailModel? = null
    private lateinit var skeleton: Skeleton
    private lateinit var viewCover : View
    private var userId : String = ""
    private lateinit var titleInfo : TextView
    private lateinit var businessLblTv : TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_booking_item_detail_list)

        initView()

        initData()

        initAction()

    }

    private fun initView(){
        progressBar = findViewById(R.id.progressItem)
        recyclerView = findViewById(R.id.recyclerView)
        ratingTv = findViewById(R.id.ratingStar)
        priceTv = findViewById(R.id.priceTv)
        addressTv = findViewById(R.id.addressTv)
        priceOriginalTv = findViewById(R.id.priceDiscount)
        hotelNameTv = findViewById(R.id.hotelNameTv)
        descriptionTv = findViewById(R.id.descriptionTv)
        btnSelectRoom = findViewById(R.id.btnSelectRoom)
        noImage = findViewById(R.id.noImage)
        facilityHotelTv = findViewById(R.id.facilityHotelTv)
        serviceHotelTv = findViewById(R.id.serviceHotelTv)
        businessTv = findViewById(R.id.businessTv)
        hotelPoliciesRecycler = findViewById(R.id.hotelPoliciesRecycler)
        hotelRelatedRecycler = findViewById(R.id.hotelRelatedRecycler)
        btnWriteAndReview = findViewById(R.id.btnWriteAndReview)
        btnSeeAllReview = findViewById(R.id.btnSeeAllReview)
        hotelReviewRecycler = findViewById(R.id.hotelReviewRecycler)
        imgProfile = findViewById(R.id.imgProfile)
        iconWishlist = findViewById(R.id.iconWishlist)
        titleToolbar = findViewById(R.id.titleToolbar)
        viewCover = findViewById(R.id.viewCover)

        skeleton = findViewById(R.id.skeletonLayout)
        titleInfo = findViewById(R.id.titleInfo)
        businessLblTv = findViewById(R.id.businessLblTv)

        Utils.setTextStrikeStyle(priceOriginalTv)

        findViewById<ImageView>(R.id.iconBack).setOnClickListener(
            CustomSetOnClickViewListener{ onBackPressed() }
        )
    }

    private fun initData(){
        hotelName = GetDataUtils.getDataFromString("hotel_name", this)
        startDate = GetDataUtils.getDataFromString("start_date", this)
        endDate = GetDataUtils.getDataFromString("end_date", this)
        hotelId = GetDataUtils.getDataFromString("hotel_id", this)
        getPrice = GetDataUtils.getDataFromString("price", this)
        children = GetDataUtils.getDataFromString("children", this)
        adults = GetDataUtils.getDataFromString("adults", this)
        category = GetDataUtils.getDataFromString("category", this)
        if (category == "") category = StaticUtilsKey.category

        userId = Utils.validateNullValue(MockUpData.getEazyHotelUserId(UserSessionManagement(this)))


        hotelNameTv.text = hotelName
        val user = Gson().fromJson(UserSessionManagement(this).userDetail, User::class.java)
        if (user != null) {
            Glide.with(imgProfile)
                .load(if (user.photo != null) user.photo else R.drawable.ic_my_profile)
                .into(imgProfile)
        }
        titleToolbar.text = hotelName

        //Not Hotel (Set Header Title)
        titleInfo.text = String.format("%s %s", RequestHashMapData.categoryTitle(this)[category], titleInfo.text.trim().toString())

        businessLblTv.text = String.format("%s %s", RequestHashMapData.categoryTitle(this)[category], businessLblTv.text.trim().toString())
    }

    private fun initAction(){
        requestServiceDetail()

        // init maps
        mapFragment = (supportFragmentManager.findFragmentById(R.id.google_map) as SupportMapFragment?)!!

        btnSelectRoom.setOnClickListener(
            CustomSetOnClickViewListener{
                if (category == "hotel")  {
                    startBooking(0.0)
                } else {
                    getStartEndDate()

                    val hashMap = RequestHashMapData.requestDataCheckAvailableItem(category, hotelId, startDate, endDate)
                    progressBar.visibility = View.VISIBLE
                    RequestHashMapData.checkAvailableItem(this, hashMap, progressBar, object : BookingHotelWS.OnCallBackCheckAvailableListener{
                        override fun onSuccess(price: Double, message: Boolean) {
                            if (message){
                                startBooking(price)
                            } else {
                                Utils.customToastMsgError(this@HotelBookingDetailHotelActivity, "You cannot book for date !", false)
                            }
                        }

                        override fun onFailed(message: String) {}

                    })

                }

            }
        )

        btnSeeAllReview.setOnClickListener(CustomSetOnClickViewListener{
            startReviewHotel("view_rate")
        })

        btnWriteAndReview.setOnClickListener( CustomSetOnClickViewListener{
            if (AppAlertCusDialog.isSuccessLoggedIn(this)) {
                startReviewHotel("do_rating")
            }
        })

        findViewById<LinearLayout>(R.id.btnWishList).setOnClickListener(
            CustomSetOnClickViewListener{
                if (AppAlertCusDialog.isSuccessLoggedIn(this)) {
                    RequestHashMapData.editStatusHotelWs(this, progressBar, hotelId, isWishListHotel,
                        "wishlist", Utils.validateNullValue(MockUpData.getEazyHotelUserId(UserSessionManagement(this))), category, object : RequestHashMapData.Companion.OnSuccessEditStatusListener{
                            override fun onSuccess(msg: String) {
                                isWishListHotel = !isWishListHotel

                                Glide.with(iconWishlist).load(if (isWishListHotel)    R.drawable.ic_my_favourite else R.drawable.ic_favorite_border).into(iconWishlist)

                                //call back to list have wish list
                                setResult(RESULT_OK)
                            }
                        })
                }
            }
        )

        findViewById<LinearLayout>(R.id.btnShare).setOnClickListener(
            CustomSetOnClickViewListener{ Utils.shareLinkMultipleOption(this, sharedLink, hotelName) }
        )

        //Custom app bar scroll to top
        val appBarLayout = findViewById<AppBarLayout>(R.id.appBarLayout)
        appBarLayout.addOnOffsetChangedListener { appBarLayout, verticalOffset ->
            if (abs(verticalOffset) - appBarLayout.totalScrollRange == 0) {
                // Collapsed
                titleToolbar.visibility = View.VISIBLE
            } else {
                // Expanded
                titleToolbar.visibility = View.INVISIBLE

            }
        }

    }

    private fun getStartEndDate(){
        val dateFormatServer = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())

        // Start Date
        var dateToday = Date()
        if (startDate == "")    startDate = dateFormatServer.format(dateToday)

        // Due Date
        val calendar = Calendar.getInstance()
        calendar.time = dateToday
        calendar.add(Calendar.DATE, 1)
        dateToday = calendar.time
        if (endDate == "")    endDate = dateFormatServer.format(dateToday)
    }

    private fun startBooking(price: Double){
        val intent = when (category) {
            "hotel" -> {
                Intent(this, HotelBookingSelectRoomActivity::class.java)
            }
            "event", "activity" -> {
                Intent(this, HotelSelectExtraPriceActivity::class.java)
            }
            else -> {
                Intent(this, HotelSelectExtraPriceActivity::class.java)
            }
        }
        intent.apply {
            putExtra("item_detail_model", getHotelDetailItemModel)
            putExtra("price", price)
            putExtra("hotel_id", hotelId)
            putExtra("start_date", startDate)
            putExtra("end_date", endDate)
            putExtra("adults", adults)
            putExtra("children", children)
            putExtra("category", category)
        }
        startActivity(intent)
    }

    private fun startReviewHotel(action : String){
        val intent = Intent(this, HotelShowReviewActivity::class.java)
        intent.putExtra("review_list", reviewHotelModelList)
        intent.putExtra("action", action)
        intent.putExtra("hotel_id", hotelId)
        intent.putExtra("hotel_name", hotelName)
        intent.putExtra("category", category)
        resultLauncher.launch(intent)
    }

    private fun requestServiceDetail(){
        progressBar.visibility = View.VISIBLE
        BookingHotelWS().getHotelBookingDetail(this, hotelId, userId, category, object : BookingHotelWS.OnCallBackMyProfileListener{
            override fun onSuccessLocation(locationHotelModel: ArrayList<LocationHotelModel>) {}

            override fun onSuccessShowHotel(action: String, listImageSlider: MutableList<String>, locationHotelModel: ArrayList<MainItemHomeModel>, itemHomeList: ArrayList<SubItemHomeModel>,userDeactivated: UserDeactivated) {}

            override fun onSuccessShowHotelDetail(hotelDetailItemModel: HotelBookingDetailModel, mainItemHomeModelLIst: ArrayList<SubItemHomeModel>) {
                //Visible layout
                progressBar.visibility = View.GONE

                reviewHotelModelList.clear()

                if (hotelDetailItemModel != null)    getHotelDetailItemModel = hotelDetailItemModel
                setValueOnHotelDetail(hotelDetailItemModel, mainItemHomeModelLIst)

                // Show Layout Item
                viewCover.visibility = View.GONE
                btnSelectRoom.isEnabled = true
            }

            override fun onFailed(message: String) {
                progressBar.visibility = View.GONE
                btnSelectRoom.isEnabled = false
                Utils.customToastMsgError(this@HotelBookingDetailHotelActivity, message, false)
            }

        })
    }

    private fun setValueOnHotelDetail(hotelDetailItemModel: HotelBookingDetailModel, relatedHotelList: ArrayList<SubItemHomeModel>){
        initRecyclerImage(hotelDetailItemModel.images)
        sharedLink = Utils.validateNullValue(hotelDetailItemModel.shared_link)

        if (hotelDetailItemModel.original_price_display != null) {
            priceOriginalTv.text = hotelDetailItemModel.original_price_display
        } else {
            priceOriginalTv.visibility = View.GONE
        }

        Utils.setValueOnText(priceTv, hotelDetailItemModel.price_display)

        //Wish List Hotel
        isWishListHotel = hotelDetailItemModel.is_wishlist
        Glide.with(iconWishlist).load(if (isWishListHotel)    R.drawable.ic_my_favourite else R.drawable.ic_favorite_border).into(iconWishlist)

        reviewHotelModelList.addAll(hotelDetailItemModel.rates_and_reviews)
        val starRate: Float = if (hotelDetailItemModel.star_rate != null && hotelDetailItemModel.star_rate != ""){
            if (hotelDetailItemModel.star_rate!!.toFloat() >= 0){
                hotelDetailItemModel.star_rate!!.toFloat()
            } else {
                0.0F
            }
        } else {
            0.0F
        }

        ratingTv.rating = starRate

        if (hotelDetailItemModel.description != null){
            Utils.setTextHtml(descriptionTv, hotelDetailItemModel.description)
        } else {
            descriptionTv.visibility = View.GONE
        }

        Utils.setValueOnText(addressTv, hotelDetailItemModel.address)

        setValueHotelFacilities(hotelDetailItemModel.facilities, facilityHotelTv)
        if (hotelDetailItemModel.facilities.size == 0)  findViewById<LinearLayout>(R.id.facilityLayout).visibility = View.GONE

        setValueHotelFacilities(hotelDetailItemModel.services, serviceHotelTv)
        if (hotelDetailItemModel.services.size == 0)  findViewById<LinearLayout>(R.id.serviceLayout).visibility = View.GONE

        setValueHotelFacilities(hotelDetailItemModel.types, businessTv)
        if (hotelDetailItemModel.types.size == 0)  findViewById<LinearLayout>(R.id.businessLayout).visibility = View.GONE

        //hotel policies
        hotelPoliciesRecycler.layoutManager = LinearLayoutManager(this)
        hotelPoliciesRecycler.adapter = HotelPoliciesDetailAdapter("policies", hotelDetailItemModel.policies)
        hotelPoliciesRecycler.isNestedScrollingEnabled = false
        if (hotelDetailItemModel.policies.size == 0) findViewById<LinearLayout>(R.id.hotelPoliciesLayout).visibility = View.GONE

        //Related Hotel
        hotelRelatedRecycler.layoutManager =
            AbsoluteFitLayoutManager(
                this,
                1,
                RecyclerView.HORIZONTAL,
                false,
                2
            )
        val bookingHomeItemAdapter = BookingHomeItemAdapter(this, "related_hotel_detail", relatedHotelList, object : BookingHomeItemAdapter.PropertyClick{
            override fun onBookingClick(propertyModel: SubItemHomeModel?) {
                val intent = Intent(this@HotelBookingDetailHotelActivity, HotelBookingDetailHotelActivity::class.java)
                intent.putExtra("hotel_name", propertyModel?.name)
                intent.putExtra("hotel_image", propertyModel?.imageUrl)
                intent.putExtra("hotel_id", propertyModel?.id)
                intent.putExtra("price", propertyModel?.priceVal)
                startActivity(intent)
            }

            override fun onBookingWishlistClick(propertyModel: SubItemHomeModel?) {}

        })
        hotelRelatedRecycler.adapter = bookingHomeItemAdapter
         if (relatedHotelList.size == 0) findViewById<LinearLayout>(R.id.relatedHotelLayout).visibility = View.GONE

        //Review hotel
        val reviewHotelModelListBackUp : ArrayList<ReviewHotelModel> = ArrayList()
        if (reviewHotelModelList.size > 3){
            var i = 0
            for (item in reviewHotelModelList){
                reviewHotelModelListBackUp.add(item)
                i++
                if (i == 3) break
            }
        } else {
            reviewHotelModelListBackUp.addAll(reviewHotelModelList)
        }

        hotelReviewRecycler.layoutManager = LinearLayoutManager(this)
        hotelReviewRecycler.adapter = HotelReviewAdapter(reviewHotelModelListBackUp)
        hotelReviewRecycler.isNestedScrollingEnabled = false

        findViewById<TextView>(R.id.noRateReviewTv).visibility = if (reviewHotelModelListBackUp.size > 0) View.GONE else View.VISIBLE

        //init maps
        if (hotelDetailItemModel.coord_lat != null && hotelDetailItemModel.coord_long!= null){
            MapsGenerateClass.initMapsActivity(this, hotelDetailItemModel.coord_lat!!.toDouble(), hotelDetailItemModel.coord_long!!.toDouble(), hotelDetailItemModel.name!!, mapFragment, onCallBackMapsReadyLister)
        } else {
            findViewById<TextView>(R.id.btnGetDirection).isEnabled = false
        }

        // Get Direction
        findViewById<TextView>(R.id.btnGetDirection).setOnClickListener(
            CustomSetOnClickViewListener{
                if (hotelDetailItemModel.coord_lat != null && hotelDetailItemModel.coord_long != null) {
                    startOpenLinkMaps(this, hotelDetailItemModel.coord_lat, hotelDetailItemModel.coord_long)
                }
            }
        )

        // Set value not category hotel
        initValueByCategory(category, hotelDetailItemModel)
    }

    private fun setValueHotelFacilities(list : ArrayList<String>, txt : TextView){
        var i = 0
        for (hotelFacilitiesList in list){
            txt.append("∙ $hotelFacilitiesList")
            i++
            if (i != list.size) {
                txt.append("\n")
            }
        }
    }

    private fun initRecyclerImage(slideModels: ArrayList <String>){
        recyclerView.layoutManager = spanGridImageLayoutManager(slideModels, this)
        recyclerView.adapter = CustomImageGridLayoutAdapter("show_5_items", this, slideModels, object : CustomImageGridLayoutAdapter.OnClickCallBackLister{
            override fun onClickCallBack(value: String) {
                val intent = Intent(this@HotelBookingDetailHotelActivity, ShowAllImageActivity::class.java)
                intent.putExtra("image_list", slideModels)
                intent.putExtra("hotel_name", hotelNameTv.text.toString())
                startActivity(intent)
            }

        })
        recyclerView.isNestedScrollingEnabled = false
        noImage.visibility = if (slideModels.size > 0) View.GONE else View.VISIBLE
        recyclerView.visibility = if (slideModels.size > 0) View.VISIBLE else View.GONE
    }

    private val onCallBackMapsReadyLister = object : MapsGenerateClass.CallBackOnMapReadyListener{
        override fun onCallBackLister(map: GoogleMap) {}
    }

    private var resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode == Activity.RESULT_OK) {
            requestServiceDetail()
        }
    }

    override fun onBackPressed() {
        if (progressBar != null) {
            if (progressBar.visibility == View.GONE) {
                finish()
            }
        }
    }

    private fun initValueByCategory(category : String, hotelDetailItemModel: HotelBookingDetailModel){

        extraPriceList = hotelDetailItemModel.extra_price
        price = hotelDetailItemModel.price

        // item can book
        btnSelectRoom.visibility = if (hotelDetailItemModel.booking_available)  View.VISIBLE else View.GONE

        when(category){
            "hotel" ->{
                findViewById<TextView>(R.id.selectRoomTv).text = resources.getString(R.string.select_room)
                titleInfo.text =  resources.getString(R.string.hotel_information)
                businessLblTv.text = resources.getString(R.string.hotel_type)
            }
            "space" ->{
                findViewById<TextView>(R.id.selectRoomTv).text = resources.getString(R.string.book_this_space)
                titleInfo.text = resources.getString(R.string.space_information)
                businessLblTv.text = resources.getString(R.string.space_type)

                findViewById<LinearLayout>(R.id.notHotelInfoLayout).visibility = View.VISIBLE

                findViewById<LinearLayout>(R.id.SpaceLayout).visibility = View.VISIBLE
                valueTxt(findViewById(R.id.maxPeopleTv), hotelDetailItemModel.max_people)
                valueTxt(findViewById(R.id.sizeTv), hotelDetailItemModel.size)
                valueTxt(findViewById(R.id.bedRoomTv), hotelDetailItemModel.beds)
                valueTxt(findViewById(R.id.bathRoomTv), hotelDetailItemModel.bathrooms)

                setNoMarginBottom()
            }
            "tour" ->{
                findViewById<TextView>(R.id.selectRoomTv).text = resources.getString(R.string.book_this_tour)
                titleInfo.text = resources.getString(R.string.tour_information)
                businessLblTv.text = resources.getString(R.string.tour_type)

                findViewById<LinearLayout>(R.id.notHotelInfoLayout).visibility = View.VISIBLE

                findViewById<LinearLayout>(R.id.tourLayout).visibility = View.VISIBLE
                findViewById<LinearLayout>(R.id.priceTourLayout).visibility = View.VISIBLE

                valueTxt(findViewById(R.id.maxPeopleTourTv), hotelDetailItemModel.max_people)
                valueTxt(findViewById(R.id.durationTourTv), hotelDetailItemModel.duration)
                valueTxt(findViewById(R.id.maxPeopleTourTv), hotelDetailItemModel.max_people)
                valueTxt(findViewById(R.id.dateFromTv), hotelDetailItemModel.date_form_to)
                valueTxt(findViewById(R.id.pickUpByTv), hotelDetailItemModel.pickup)
                valueTxt(findViewById(R.id.minAgeTv), hotelDetailItemModel.min_age)

                setValueHotelFacilities(hotelDetailItemModel.include, findViewById(R.id.includeTv))

                setValueHotelFacilities(hotelDetailItemModel.exclude, findViewById(R.id.excludeTv))

                initItineraryRecyclerView(findViewById(R.id.itineraryRecyclerView), hotelDetailItemModel.itinerary)

                setNoMarginBottom()
            }
            "event", "activity"->{
                if (category == "event") {
                    findViewById<TextView>(R.id.selectRoomTv).text = resources.getString(R.string.book_this_event)
                    titleInfo.text = resources.getString(R.string.event_information)
                    businessLblTv.text = resources.getString(R.string.event_type)
                } else {
                    findViewById<TextView>(R.id.selectRoomTv).text = if (category == "event")  resources.getString(R.string.book_this_event) else resources.getString(R.string.book_this_activity)
                    titleInfo.text = resources.getString(R.string.activity_information)
                    businessLblTv.text = resources.getString(R.string.activity_type)
                }

                findViewById<LinearLayout>(R.id.notHotelInfoLayout).visibility = View.VISIBLE

                findViewById<LinearLayout>(R.id.eventLayout).visibility = View.VISIBLE
                valueTxt(findViewById(R.id.startTimeTv), hotelDetailItemModel.start_time)
                valueTxt(findViewById(R.id.durationTv), hotelDetailItemModel.duration)

                setNoMarginBottom()
            }
        }

    }

    private fun setNoMarginBottom(){
        val view = findViewById<NestedScrollView>(R.id.mainLayout)
        val layoutParams = view.layoutParams as CoordinatorLayout.LayoutParams
        layoutParams.setMargins(0, 0, 0, 0)
        view.layoutParams = layoutParams
    }

    private fun initItineraryRecyclerView(recyclerView: RecyclerView, list : ArrayList<PoliciesModel>){
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = HotelPoliciesDetailAdapter("itinerary", list)
        recyclerView.isNestedScrollingEnabled = false
    }

    private fun valueTxt(textView: TextView, value: String?) {
        if (value != null){
            textView.text = String.format("%s", value)
        } else {
            textView.text = "0"
        }
    }

}