package com.eazy.daikou.ui.home.book_now.booking_hotel.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.denzcoskun.imageslider.ImageSlider
import com.denzcoskun.imageslider.constants.ScaleTypes
import com.denzcoskun.imageslider.interfaces.ItemClickListener
import com.denzcoskun.imageslider.models.SlideModel
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import java.util.ArrayList

class HotelContentDiscountAdapter(private val bannerDiscountList : ArrayList<String>, private val bannerTravelTalkList : ArrayList<String>, private val onClickListener : OnClickItemListener) : RecyclerView.Adapter<HotelContentDiscountAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.content_view_home_page_hotel, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBindingView(bannerDiscountList, bannerTravelTalkList, onClickListener)
    }

    override fun getItemCount(): Int {
        return 1
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val travelTalkLayout : CardView = itemView.findViewById(R.id.travelTalkLayout)
        private val imageSlider: ImageSlider = itemView.findViewById(R.id.slider)
        private val sliderTravelTalk: ImageSlider = itemView.findViewById(R.id.sliderTravelTalk)
        private val discountSaveLayout: CardView = itemView.findViewById(R.id.discountSaveLayout)
        private val travelArticleLayout : CardView = itemView.findViewById(R.id.travelArticleLayout)

        fun onBindingView(bannerDiscountList : ArrayList<String>, bannerTravelTalkList : ArrayList<String>, onClickListener : OnClickItemListener){
            val slideModels: MutableList<SlideModel> = ArrayList()
            slideModels.add(SlideModel("https://i.pinimg.com/474x/15/0c/d7/150cd7dec5567c7c759dc0f899d5ac5a.jpg"))
            slideModels.add(SlideModel("https://free-psd-templates.com/wp-content/uploads/2018/07/free_psd_preview_last_15-free-hotel-banners-collection-in-psd.jpg"))
            slideModels.add(SlideModel("https://indiater.com/wp-content/uploads/2019/03/sea-view-hotel-miami-beach-holiday-package-banner.jpg"))

            // Discount
            if (bannerDiscountList.size > 0){
                imageSlider.setImageList(convertSlideModelList(bannerDiscountList), ScaleTypes.CENTER_CROP)
            } else {
                imageSlider.setImageList(slideModels, ScaleTypes.CENTER_CROP)
            }

            // Travel Talk
            if (bannerTravelTalkList.size > 0){
                sliderTravelTalk.setImageList(convertSlideModelList(bannerTravelTalkList), ScaleTypes.CENTER_CROP)
            } else {
                sliderTravelTalk.setImageList(slideModels, ScaleTypes.CENTER_CROP)
            }

            imageSlider.setItemClickListener(object : ItemClickListener {
                override fun onItemSelected(position: Int) {
                    onClickListener.onClickListener("save_discount")
                }
            })

            sliderTravelTalk.setItemClickListener(object : ItemClickListener {
                override fun onItemSelected(position: Int) {
                    onClickListener.onClickListener("travel_talk")
                }
            })

            travelArticleLayout.setOnClickListener(CustomSetOnClickViewListener{
                onClickListener.onClickListener("travel_article")
            })

            discountSaveLayout.setOnClickListener(CustomSetOnClickViewListener{
                onClickListener.onClickListener("save_discount")
            })

            travelTalkLayout.setOnClickListener(CustomSetOnClickViewListener{
                onClickListener.onClickListener("travel_talk")
            })
        }

        private fun convertSlideModelList(list : ArrayList<String>) : ArrayList<SlideModel>{
            val slideList: ArrayList<SlideModel> = ArrayList()
            for (item in list){
                slideList.add(SlideModel(item))
            }
            return slideList
        }
    }

    interface OnClickItemListener {
        fun onClickListener(action : String)
    }
}