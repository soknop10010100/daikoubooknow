package com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.cleaning_v2

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AbsListView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.request_data.request.service_property_ws.MyUnitPropertyWs
import com.eazy.daikou.request_data.request.service_property_ws_v2.ServiceProviderPropertyWs
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.my_property.cleaning.PropertyServiceTypeModel
import com.eazy.daikou.model.my_property.cleaning.ServiceCategories
import com.eazy.daikou.model.my_property.cleaning.ServiceTermAndCategoryModel
import com.eazy.daikou.model.my_property.cleaning.ServiceTerms
import com.eazy.daikou.model.profile.User
import com.eazy.daikou.model.utillity_tracking.model.UnitImgModel
import com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.activity.ServicePropertyDateTimeActivity
import com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.adapter_v2.PropertyServiceProviderCategoryTermAdapter
import com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.adapter_v2.PropertyServiceProviderTypeAdapter
import com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.adapter_v2.PropertyServiceProviderUnitAdapter
import com.google.gson.Gson

class ServiceCleaningCreateRequestListActivity : BaseActivity() {

    private lateinit var serviceCategory: TextView
    private lateinit var textNoDataTv: TextView
    private lateinit var recyclerViewUnit: RecyclerView
    private lateinit var recyclerViewTerm: RecyclerView
    private lateinit var recyclerViewServiceType: RecyclerView
    private lateinit var progressBar: ProgressBar
    private lateinit var refreshListServiceCategory: SwipeRefreshLayout

    private lateinit var adapterUnitServiceProvider : PropertyServiceProviderUnitAdapter
    private var myUnitModelList: ArrayList<UnitImgModel> = ArrayList()

    private lateinit var  adapterCategoryTermProvider : PropertyServiceProviderCategoryTermAdapter
    private var selectTermList: ArrayList<ServiceTerms> = ArrayList()
    private var selectCategoryList: ArrayList<ServiceCategories> = ArrayList()

    private lateinit var adapterServiceTypeProvider : PropertyServiceProviderTypeAdapter
    private var serviceTypeList: ArrayList<PropertyServiceTypeModel> = ArrayList()
    private lateinit var gridLayoutManager: GridLayoutManager

    private var user: User? = null
    private var currentPage:Int = 1
    private var currentItem = 0
    private var total : Int = 0
    private var scrollDown : Int = 0
    private var size : Int = 10
    private var isScrolling = false
    private var isScrollItem = false

    private var unitId: String = ""
    private var unitNo: String = ""
    private var keyTerm: String = ""
    private var serviceTerm: String = ""
    private var categoryId: String = ""
    private var categoryName: String = ""
    private var dataOperationPart: String = ""
    private var accountCompanyId = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_service_cleaning_create_request_list)

        initView()

        initData()

        initAction()
    }

    private fun initView(){
        serviceCategory = findViewById(R.id.serviceCategory)
        recyclerViewUnit = findViewById(R.id.recyclerViewMyUnit)
        recyclerViewTerm = findViewById(R.id.recyclerViewServiceTern)
        recyclerViewServiceType = findViewById(R.id.recyclerViewPopularService)
        progressBar = findViewById(R.id.progressItem)
        textNoDataTv = findViewById(R.id.textNoDataTv)
        refreshListServiceCategory = findViewById(R.id.refreshListServiceCategory)

        Utils.customOnToolbar(this, resources.getString(R.string.choose_service).uppercase()){finish()}
    }

    private fun initData(){
        if (intent != null && intent.hasExtra("operation_part_string")){
            dataOperationPart = intent.getStringExtra("operation_part_string") as String
        }

        if (intent != null && intent.hasExtra("id_company_account")){
            accountCompanyId = intent.getStringExtra("id_company_account") as String
        }
    }

    private fun initAction(){
        refreshListServiceCategory.setColorSchemeResources(R.color.colorPrimary)
        refreshListServiceCategory.setOnRefreshListener { refreshList()}
        user = Gson().fromJson(UserSessionManagement(this).userDetail, User::class.java)

        //Request Service API Unit
        requestServiceApiUnit()

        //RecyclerView adapter
        val unitLayoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
        recyclerViewUnit.layoutManager = unitLayoutManager
        recyclerViewUnit()

        val termLayoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
        recyclerViewTerm.layoutManager = termLayoutManager
        recyclerViewTerm()

        gridLayoutManager = GridLayoutManager(this, 2)
        recyclerViewServiceType.layoutManager = gridLayoutManager
        recyclerViewPropertyServiceType()

        onScrollItemServiceType(recyclerViewServiceType)

        serviceCategory.setOnClickListener(
            CustomSetOnClickViewListener {
                if (selectCategoryList.size > 0) {
                    val bottomSheetFragment = ServiceCleaningsServiceCategoryFragment().newInstance(categoryId, selectCategoryList)
                    bottomSheetFragment.initListener(callBackCategoryName)
                    bottomSheetFragment.show(supportFragmentManager, "service_category")
                } else {
                    Utils.customToastMsgError(this, resources.getString(R.string.not_available), true)
                }
            }
        )
    }

    private fun refreshList(){
        currentPage = 1
        size = 10
        isScrolling = true
        adapterServiceTypeProvider.clear()
        requestServiceApiPropertyServiceName()
    }

    private fun onScrollItemServiceType(recyclerViewItem: RecyclerView) {
        recyclerViewItem.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true
                }
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                currentItem = gridLayoutManager.childCount
                total = gridLayoutManager.itemCount
                scrollDown = gridLayoutManager.findFirstVisibleItemPosition()
                if (dy > 0) {
                    if (isScrolling && currentItem + scrollDown == total) {
                        if (total == size) {
                            isScrolling = false
                            currentPage++
                            progressBar.visibility = View.VISIBLE
                            isScrollItem = true
                            recyclerViewItem.scrollToPosition(selectCategoryList.size - 1)
                            recyclerViewPropertyServiceType()
                            size += 10
                        } else {
                            progressBar.visibility = View.GONE
                        }
                    }
                }
            }
        })
    }

    // CallBack From Fragment Category
    private val callBackCategoryName:  ServiceCleaningsServiceCategoryFragment.CallBackItemCategoryName = object : ServiceCleaningsServiceCategoryFragment.CallBackItemCategoryName{
        override fun clickItemCategoryName(serviceCategoryList: ServiceCategories) {
            serviceCategory.text = serviceCategoryList.category_name
            categoryId = serviceCategoryList.id!!
            categoryName = serviceCategoryList.category_name!!
            requestServiceApiPropertyServiceName()
        }
    }

    // RecyclerView adapter
    private fun recyclerViewUnit(){
        adapterUnitServiceProvider = PropertyServiceProviderUnitAdapter(this, myUnitModelList, callBackUnitAdapter)
        recyclerViewUnit.adapter = adapterUnitServiceProvider
    }

    private fun recyclerViewTerm(){
        adapterCategoryTermProvider = PropertyServiceProviderCategoryTermAdapter(this, selectTermList, callBackTermAdapter)
        recyclerViewTerm.adapter = adapterCategoryTermProvider
    }

    private fun recyclerViewPropertyServiceType(){
        adapterServiceTypeProvider = PropertyServiceProviderTypeAdapter(this, serviceTypeList , callBackPropertyServiceTypeAdapter)
        recyclerViewServiceType.adapter = adapterServiceTypeProvider
    }

    private fun requestServiceApiUnit(){
        MyUnitPropertyWs().getServiceMyUnitV2(this,
            Utils.validateNullValue(user!!.accountId), user!!.activePropertyIdFk, 1, 100, UserSessionManagement(this).userId, callBackMyUnitListener)
    }

    private fun requestServiceApiTermAndCategory(){
        ServiceProviderPropertyWs().getServiceTermAndCategoryWs(this, accountCompanyId, Utils.validateNullValue(user!!.accountId), dataOperationPart, callBackTermAndCategoryListener)
    }

    private fun requestServiceApiPropertyServiceName(){
        progressBar.visibility = View.VISIBLE
        ServiceProviderPropertyWs().getListPropertyServiceTypeWs(this, currentPage, 10, Utils.validateNullValue(user!!.accountId), unitId , keyTerm, categoryId, accountCompanyId, callBackPropertyServiceTypeListener)
    }

    private val callBackMyUnitListener : MyUnitPropertyWs.MyUnitCallBackListener = object : MyUnitPropertyWs.MyUnitCallBackListener{
        @SuppressLint("NotifyDataSetChanged")
        override fun getServiceUnit(unitImgModels: ArrayList<UnitImgModel>) {
            progressBar.visibility = View.GONE
            myUnitModelList.clear()
            myUnitModelList.addAll(unitImgModels)

            if (myUnitModelList.size > 0) {
                myUnitModelList[0].clickTitle = true
                unitId = myUnitModelList[0].unitId
                unitNo = myUnitModelList[0].unitNo
            }

            adapterUnitServiceProvider.notifyDataSetChanged()

            requestServiceApiTermAndCategory()
        }

        override fun failed(error: String?) {
            progressBar.visibility = View.GONE
            Utils.customToastMsgError(this@ServiceCleaningCreateRequestListActivity, error, false)
        }

    }

    private val callBackTermAndCategoryListener : ServiceProviderPropertyWs.OnCallBackServiceTermAndCategory = object : ServiceProviderPropertyWs.OnCallBackServiceTermAndCategory{
        @SuppressLint("NotifyDataSetChanged")
        override fun onLoadSuccessFull(serviceTermAndCategory: ServiceTermAndCategoryModel) {
            progressBar.visibility = View.GONE

            selectTermList.clear()
            selectTermList.addAll(serviceTermAndCategory.service_terms)

            selectCategoryList.clear()
            selectCategoryList.addAll(serviceTermAndCategory.service_categories)

            if(selectTermList.size > 0) {
                selectTermList[0].isClickTitle = true
                keyTerm = selectTermList[0].term_key.toString()
                serviceTerm = selectTermList[0].term_key.toString()
            }

            if (selectCategoryList.size > 0){
                categoryId = selectCategoryList[0].id.toString()
                serviceCategory.text = selectCategoryList[0].category_name
                categoryName = selectCategoryList[0].category_name.toString()
            }

            adapterCategoryTermProvider.notifyDataSetChanged()

            requestServiceApiPropertyServiceName()

        }

        override fun onLoadFailed(message: String) {
            progressBar.visibility = View.GONE
            Utils.customToastMsgError(this@ServiceCleaningCreateRequestListActivity, message, false)
        }
    }

    private val callBackPropertyServiceTypeListener : ServiceProviderPropertyWs.OnCallBackPropertyServiceType = object : ServiceProviderPropertyWs.OnCallBackPropertyServiceType{
        @SuppressLint("NotifyDataSetChanged")
        override fun onLoadSuccessFull(propertyServiceTypeList: ArrayList<PropertyServiceTypeModel>) {
            progressBar.visibility = View.GONE
            refreshListServiceCategory.isRefreshing = false

            serviceTypeList.clear()
            serviceTypeList.addAll(propertyServiceTypeList)

            textNoDataTv.visibility =  if(serviceTypeList.size > 0) View.GONE else View.VISIBLE
            adapterServiceTypeProvider.notifyDataSetChanged()
        }

        override fun onLoadFailed(message: String) {
            progressBar.visibility = View.GONE
            refreshListServiceCategory.isRefreshing = false
            Utils.customToastMsgError(this@ServiceCleaningCreateRequestListActivity , message, false)
        }
    }

    // Call Back Adapter
    private val callBackUnitAdapter : PropertyServiceProviderUnitAdapter.CallBackItemListener = object : PropertyServiceProviderUnitAdapter.CallBackItemListener {
        @SuppressLint("NotifyDataSetChanged")
        override fun callBackItemCategory(unitImgModel: UnitImgModel) {
            for (i in myUnitModelList.indices) {
                myUnitModelList[i].clickTitle = unitImgModel.unitId == myUnitModelList[i].unitId
            }
            unitId = unitImgModel.unitId
            unitNo = unitImgModel.unitNo
            adapterUnitServiceProvider.notifyDataSetChanged()

            requestServiceApiPropertyServiceName()
        }
    }

   private val callBackTermAdapter: PropertyServiceProviderCategoryTermAdapter.CallBackItemTermListener = object : PropertyServiceProviderCategoryTermAdapter.CallBackItemTermListener{
       @SuppressLint("NotifyDataSetChanged")
       override fun callBackTerm(serviceCategoryItemModel: ServiceTerms) {
           for (i in selectTermList.indices){
               selectTermList[i].isClickTitle = serviceCategoryItemModel.term_key == selectTermList[i].term_key
           }
           keyTerm = serviceCategoryItemModel.term_key!!
           adapterCategoryTermProvider.notifyDataSetChanged()

           requestServiceApiPropertyServiceName()
       }
   }

    private val callBackPropertyServiceTypeAdapter : PropertyServiceProviderTypeAdapter.CallbackPopularItem = object  : PropertyServiceProviderTypeAdapter.CallbackPopularItem{
        override fun popularCallBackListener(servicePopular: PropertyServiceTypeModel) {
            openSomeActivityForResult(ServicePropertyDateTimeActivity::class.java, servicePopular)
        }
    }

    private fun openSomeActivityForResult(toClassActivity : Class<*>, servicePopular: PropertyServiceTypeModel) {
        val intent = Intent(this, toClassActivity)
        intent.putExtra("unit_id", unitId)
        intent.putExtra("service_term", keyTerm)
        intent.putExtra("service_category", categoryName)
        intent.putExtra("service_type_model", servicePopular)
        resultLauncher.launch(intent)
    }

    private var resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode == Activity.RESULT_OK) {
            val data: Intent? = result.data
            if (data != null) {
                //Set Result CallBack
                val isBackSuccess = data.getBooleanExtra("is_result_success", false)
                if (isBackSuccess)  Utils.setResultBack(this@ServiceCleaningCreateRequestListActivity, "", true)
            }
        }
    }
}