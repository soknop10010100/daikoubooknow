package com.eazy.daikou.ui.home.facility_booking

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.text.InputType
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.model.facility_booking.SelectNumberModel
import com.eazy.daikou.ui.home.facility_booking.adapter.SelectNumberAdapter

class SelectNumberDialogActivity : BaseActivity() {

    private lateinit var titleToolbar : TextView
    private lateinit var recyclerView : RecyclerView
    private var numberOfPeopleVal : String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_number_dailog)

        initView()

        initAction()

    }

    private fun initView(){
        titleToolbar = findViewById(R.id.txtTitle)
        recyclerView = findViewById(R.id.recyclerView)

        findViewById<ImageView>(R.id.btnClose).setOnClickListener{finish()}
    }

    private fun initAction(){
        if (intent != null && intent.hasExtra("numberOfPeopleVal")){
            numberOfPeopleVal = intent.getStringExtra("numberOfPeopleVal").toString()
        }
        recyclerView.layoutManager = GridLayoutManager(this, 4,RecyclerView.VERTICAL,false)
        recyclerView.adapter = SelectNumberAdapter(addListNumberString(), onClickCallBack)
    }

    private var onClickCallBack = object : SelectNumberAdapter.ClickCallBackListener{
        override fun onClickCallBack(numberString: String) {
            if (numberString == "+") {
                alertDialogEdit()
            } else {
                setResultBack(numberString)
            }
        }

    }

    private fun setResultBack(numberString : String){
        val intent = Intent()
        intent.putExtra("number_data", numberString)
        setResult(RESULT_OK, intent)
        finish()
    }

    private fun alertDialogEdit(){
        val alertDialog = AlertDialog.Builder(this@SelectNumberDialogActivity)
        alertDialog.setTitle(resources.getString(R.string.number_of_people))

        val input = EditText(this@SelectNumberDialogActivity)
        input.inputType = InputType.TYPE_CLASS_NUMBER
        val lp = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
        input.layoutParams = lp
        alertDialog.setView(input)
        alertDialog.setIcon(R.drawable.ic_edit_white)
        alertDialog.setCancelable(false)

        alertDialog.setPositiveButton(resources.getString(R.string.yes)) { dialog: DialogInterface, _: Int ->
            val getInputDes = input.text.toString()
            if (getInputDes.isEmpty() ||getInputDes == "0") {
                setResultBack("")
            } else {
                setResultBack(getInputDes)
            }
            dialog.dismiss()
        }

        alertDialog.setNegativeButton(resources.getString(R.string.cancel)) { dialog: DialogInterface, _: Int -> dialog.cancel() }
        alertDialog.show()
    }

    private fun addListNumberString() : List<SelectNumberModel>{
        val list : ArrayList<SelectNumberModel> = ArrayList()
        for (i in 1..15){
            val model = SelectNumberModel()
            model.isClick = numberOfPeopleVal == i.toString()
            model.numberString = i.toString()
            list.add(model)
        }
        val model = SelectNumberModel()
        model.numberString = "+"
        list.add(model)
        return list
    }

}