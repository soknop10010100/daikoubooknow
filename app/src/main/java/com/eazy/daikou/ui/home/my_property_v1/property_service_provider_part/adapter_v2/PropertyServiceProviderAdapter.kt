package com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.adapter_v2

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.model.my_property.service_provider_v2.ServiceProviderCompanyPropertyModel

class PropertyServiceProviderAdapter(private val context: Context, private val serviceProviderList: ArrayList<ServiceProviderCompanyPropertyModel>, private val callBackItemService: CallBackItemServiceProviderProperty): RecyclerView.Adapter<PropertyServiceProviderAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.layout_item_property_service, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
       val listService: ServiceProviderCompanyPropertyModel = serviceProviderList[position]

        holder.companyTv.text = if (listService.account_name != null) listService.account_name else "- - -"
        holder.locationTv.text = if (listService.account_address != null) listService.account_address else "- - -"
        holder.phoneTv.text = if (listService.contact != null) listService.contact else "- - -"
        holder.txtWebSite.text = if (listService.website != null) listService.website else "- - -"

        if (listService.image != null){
            Glide.with(holder.imageView).load(listService.image).into(holder.imageView)
        } else {
            Glide.with(holder.imageView).load(R.drawable.no_image).into(holder.imageView)
        }
        holder.itemView.setOnClickListener {  callBackItemService.callBackItem(listService)}
    }

    override fun getItemCount(): Int {
        return serviceProviderList.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view){
        var companyTv: TextView = view.findViewById(R.id.name_service)
        var phoneTv: TextView = view.findViewById(R.id.phone_number_service)
        var txtWebSite: TextView = view.findViewById(R.id.txtWebSite)
        var locationTv: TextView = view.findViewById(R.id.txt_location)
        var imageView: ImageView = view.findViewById(R.id.imageView)
    }

    interface CallBackItemServiceProviderProperty{
        fun callBackItem(listServiceProvider: ServiceProviderCompanyPropertyModel)
    }

    fun clear() {
        val size: Int = serviceProviderList.size
        if (size > 0) {
            for (i in 0 until size) {
                serviceProviderList.removeAt(0)
            }
            notifyItemRangeRemoved(0, size)
        }
    }
}