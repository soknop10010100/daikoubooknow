package com.eazy.daikou.ui.home.my_property.property_service.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.databinding.PropertyServiceCategoryModelBinding
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.home.HomeViewModel
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import java.util.ArrayList

class PropertyServiceCategoryAdapter(private val listName: List<HomeViewModel>, private val itemClick : ItemClickOnListener) :
    RecyclerView.Adapter<PropertyServiceCategoryAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = PropertyServiceCategoryModelBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data: HomeViewModel = listName[position]

        holder.nameItemTv.text = if (data.name != null) data.name else ". . ."
        Glide.with(holder.imageView).load(if (data.drawable != null) data.drawable else R.drawable.no_image).into(holder.imageView)

        holder.cardAdapter.setOnClickListener(CustomSetOnClickViewListener{
            itemClick.onItemClick(data)
        })

    }

    override fun getItemCount(): Int {
        return listName.size
    }

    class ViewHolder(mBind: PropertyServiceCategoryModelBinding) : RecyclerView.ViewHolder(mBind.root) {
        var nameItemTv : TextView = mBind.nameItemHr
        var imageView : ImageView = mBind.imageHr
        var cardAdapter : CardView =  mBind.cardHr
    }

    interface ItemClickOnListener{
        fun onItemClick(homeViewModel : HomeViewModel)
    }

    companion object {
        fun getListHomeMenu(context: Context): List<HomeViewModel> {
            val homeViewModels: MutableList<HomeViewModel> = ArrayList()
            homeViewModels.add(
                HomeViewModel(
                    "cleaning",
                    ResourcesCompat.getDrawable(context.resources, R.drawable.ic_availability, null),
                    context.resources.getString(R.string.cleaning)
                )
            )
            homeViewModels.add(
                HomeViewModel(
                    "security",
                    ResourcesCompat.getDrawable(context.resources, R.drawable.icon_security_emergency, null),
                    context.resources.getString(R.string.security)
                )
            )
            homeViewModels.add(
                HomeViewModel(
                    "spider_man",
                    ResourcesCompat.getDrawable(
                        context.resources,
                        R.drawable.ic_home_spiderman,
                        null
                    ),
                    "Spider Man"
                )
            )
            homeViewModels.add(
                HomeViewModel(
                    "landscaping",
                    ResourcesCompat.getDrawable(context.resources, R.drawable.ic_home_landscape, null),
                    "Land Scape"
                )
            )

            return homeViewModels
        }
    }

}