package com.eazy.daikou.ui.home.complaint_solution.complaint.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseBottomSheetDialogFragment
import com.eazy.daikou.helper.GetDataUtils
import com.eazy.daikou.request_data.request.complain_ws.ComplaintListWS
import com.eazy.daikou.request_data.request.my_property_ws.my_unit_ws.MyUnitPropertyWS
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.my_property.my_property.PropertyMyUnitModel
import com.eazy.daikou.model.profile.PropertyItemModel
import com.eazy.daikou.model.profile.UnitNoModel
import com.eazy.daikou.ui.home.complaint_solution.complaint.adapter.AdapterItemListComplaint

class SelectAccountAndUnitNoBottomSheet :BaseBottomSheetDialogFragment() {

    private lateinit var recyclerViewComplaint : RecyclerView
    private lateinit var adapterItemComplaint : AdapterItemListComplaint
    private var actionClick = ""
    private lateinit var tittleTv : TextView
    private lateinit var searchEt : EditText
    private lateinit var iconClearText : ImageView
    private lateinit var callBackItemClick : AdapterItemListComplaint.CallBackItemClickListener
    private var listItemComplaint : ArrayList<PropertyItemModel> = ArrayList()
    private var listItemMyUnit : ArrayList<UnitNoModel> = ArrayList()

    private lateinit var noDataTv : TextView
    private lateinit var progressBar: ProgressBar

    private var currentItem = 0
    private var total : Int = 0
    private var scrollDown : Int = 0
    private var currentPage : Int = 1
    private var size : Int = 20
    private var isScrolling = false
    private lateinit var linearLayoutManager : LinearLayoutManager
    private var keySearch = ""
    private var accountId = ""

    companion object{
        fun newInstance(actionType: String, accountId : String): SelectAccountAndUnitNoBottomSheet {
            val fragment = SelectAccountAndUnitNoBottomSheet()
            val args = Bundle()
            args.putString("action_type",actionType)
            args.putString("account_id",accountId)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_select_account_unit, container, false)

        initView(view)

        initData()

        initAction()

        return view
    }

    private fun initView(view : View){
        recyclerViewComplaint = view.findViewById(R.id.recyclerViewComplaint)
        tittleTv = view.findViewById(R.id.tittleTv)
        searchEt = view.findViewById(R.id.searchEt)
        iconClearText = view.findViewById(R.id.iconClearText)
        noDataTv = view.findViewById(R.id.noDataTv)
        progressBar = view.findViewById(R.id.progressItem)
        view.findViewById<ImageView>(R.id.iconDismiss).setOnClickListener { dismiss() }

        view.findViewById<LinearLayout>(R.id.linearSearch).visibility = View.VISIBLE
    }

    private fun initData(){
        actionClick = GetDataUtils.getDataFromString("action_type", arguments)

        accountId = GetDataUtils.getDataFromString("account_id", arguments)

        tittleTv.text = if (actionClick == "property") {
            resources.getString(R.string.select_property)
        } else {
            resources.getString(R.string.select_unit)
        }

    }

    private fun initAction(){
        linearLayoutManager = LinearLayoutManager(mActivity)
        recyclerViewComplaint.layoutManager = linearLayoutManager

        initRecyclerView()

        initOnScrollItem()

        searchItemComplaint()

    }

    private fun initRecyclerView(){
        adapterItemComplaint =
            if (actionClick == "property") {
                AdapterItemListComplaint(mActivity, actionClick, listItemComplaint, callBackItemListener)
            } else {
                AdapterItemListComplaint(mActivity, listItemMyUnit, callBackItemListener)
            }
        recyclerViewComplaint.adapter = adapterItemComplaint

        requestServiceWs("")
    }

    private val callBackItemListener : AdapterItemListComplaint.CallBackItemClickListener = object : AdapterItemListComplaint.CallBackItemClickListener {
        override fun onClickAccount(itemComplaint: PropertyItemModel) {
            callBackItemClick.onClickAccount(itemComplaint)
            dismiss()
        }

        override fun onClickUnit(myUnit: UnitNoModel) {
            callBackItemClick.onClickUnit(myUnit)
            dismiss()
        }
    }

    private fun searchItemComplaint(){
        searchEt.addTextChangedListener(object : TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

            override fun afterTextChanged(editable: Editable) {
                // Click button clear
                iconClearText.setOnClickListener {
                    searchEt.setText("")
                    editable.clear()
                }

                if (editable.toString().isNotEmpty()) {
                    iconClearText.visibility = View.VISIBLE
                    keySearch = editable.toString()
                } else {
                    iconClearText.visibility = View.GONE
                    keySearch = ""
                }

                refreshList()
            }

        })
    }

    private fun initOnScrollItem(){
        recyclerViewComplaint.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true
                }
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                currentItem = linearLayoutManager.childCount
                total = linearLayoutManager.itemCount
                scrollDown = linearLayoutManager.findFirstVisibleItemPosition()
                if (dy > 0) {
                    if (isScrolling && currentItem + scrollDown == total) {
                        if (total == size) {
                            isScrolling = false
                            currentPage++
                            progressBar.visibility = View.VISIBLE
                            if(actionClick == "property"){
                                recyclerView.scrollToPosition(listItemComplaint.size - 1)
                            } else {
                                recyclerView.scrollToPosition(listItemMyUnit.size - 1)
                            }
                            requestServiceWs(keySearch)
                            size += 20
                        } else {
                            progressBar.visibility = View.GONE
                        }
                    }
                }
            }
        })
    }

    private fun requestServiceWs(keySearch : String){
        progressBar.visibility = View.VISIBLE
        if(actionClick == "property"){
            ComplaintListWS.getAccountPropertyWs(mActivity, currentPage, 20, keySearch, callbackListProperty )
        } else{
            ComplaintListWS.getUnitNoByAccountWs(mActivity, currentPage, 20, accountId, keySearch, callbackListProperty)
        }
    }

    private val callbackListProperty = object : ComplaintListWS.CallBackListPropertyComplaint{
        @SuppressLint("NotifyDataSetChanged")
        override fun onSuccessFull(listItemProperty: ArrayList<PropertyItemModel>) {
            progressBar.visibility = View.GONE
            listItemComplaint.addAll(listItemProperty)

            noDataTv.visibility =  if (listItemComplaint.size == 0) View.VISIBLE else View.GONE

            adapterItemComplaint.notifyDataSetChanged()
        }

        @SuppressLint("NotifyDataSetChanged")
        override fun onSuccessUnitNo(listItemProperty: ArrayList<UnitNoModel>) {
            progressBar.visibility = View.GONE
            listItemMyUnit.addAll(listItemProperty)

            noDataTv.visibility =  if (listItemMyUnit.size == 0) View.VISIBLE else View.GONE

            adapterItemComplaint.notifyDataSetChanged()
        }

        override fun onFailed(message: String) {
            progressBar.visibility = View.GONE
            Utils.customToastMsgError(context, message, false)
        }

    }

    private fun refreshList(){
        currentPage = 1
        size = 20
        isScrolling = true
        adapterItemComplaint.clear()

        requestServiceWs(keySearch)
    }

    fun initDataListener(callBackItemClick : AdapterItemListComplaint.CallBackItemClickListener){
        this.callBackItemClick = callBackItemClick
    }
}