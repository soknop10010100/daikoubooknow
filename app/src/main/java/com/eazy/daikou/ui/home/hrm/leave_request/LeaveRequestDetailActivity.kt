package com.eazy.daikou.ui.home.hrm.leave_request

import android.app.Dialog
import android.content.Intent
import android.content.res.ColorStateList
import android.os.Bundle
import android.view.View
import android.view.Window
import android.widget.*
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.core.widget.NestedScrollView
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.base.MockUpData
import com.eazy.daikou.request_data.request.hr_ws.LeaveManagementWs
import com.eazy.daikou.helper.BroadcastTypes
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.hr.LeaveRequestDetail
import com.eazy.daikou.model.profile.User
import com.google.gson.Gson

class LeaveRequestDetailActivity : BaseActivity()  {

    private lateinit var iconBack : ImageView
    private lateinit var btnLeaveBalance : ImageView
    private lateinit var titleToolbar : TextView
    private lateinit var userNameTv : TextView
    private lateinit var createDateTv : TextView
    private lateinit var idNoTv :TextView
    private lateinit var designationTv : TextView
    private lateinit var leaveCategory : TextView
    private lateinit var startDateTv : TextView
    private lateinit var endDateTv : TextView
    private lateinit var leaveDaysTv : TextView
    private lateinit var dateOfReturnTv : TextView
    private lateinit var periodOfLastLeaveTv : TextView
    private lateinit var categoryOfLeaveTv :TextView
    private lateinit var leaveAddressTv : TextView
    private lateinit var performingPersonTv : TextView
    private lateinit var applyDateTv : TextView
    private lateinit var statusTv : TextView
    private lateinit var descriptionTv : TextView
    private lateinit var loading : ProgressBar
    private lateinit var viewDetail : RelativeLayout
    private lateinit var willApprovedTv : TextView
    private lateinit var assignToTv : TextView
    private lateinit var periodTv : TextView
    private lateinit var cardLeaveInfo : CardView
    private lateinit var leaveInformation : LinearLayout
    private lateinit var linearLayoutAction : LinearLayout
    private lateinit var btnRejectLeave : TextView
    private lateinit var btnApproveLeave : TextView

    private lateinit var cardDescription : CardView
    private lateinit var leaveDescription : LinearLayout
    private lateinit var remarkLayout : CardView
    private lateinit var remarkTv : TextView

    private lateinit var user : User
    private var userBusinessKey: String = ""
    private var leaveID = ""
    private var listType = ""
    private var isClick = false
    private var actionNotification: String = ""
    private var userBussinessKey = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_leave_request_detail)

        initView()

        initData()

        initAction()
    }

    private fun initView(){
        iconBack = findViewById(R.id.iconBack)
        btnLeaveBalance = findViewById(R.id.addressMap)
        findViewById<LinearLayout>(R.id.layoutMap).visibility = View.VISIBLE
        titleToolbar = findViewById(R.id.titleToolbar)

        createDateTv = findViewById(R.id.createDateTv)
        userNameTv = findViewById(R.id.user_name)
        idNoTv = findViewById(R.id.id_no)
        designationTv = findViewById(R.id.designation)
        leaveCategory = findViewById(R.id.leave_category)
        startDateTv = findViewById(R.id.start_date)
        endDateTv = findViewById(R.id.end_date)
        //information
        leaveDaysTv = findViewById(R.id.leave_day)
        dateOfReturnTv = findViewById(R.id.date_of_return)
        periodOfLastLeaveTv = findViewById(R.id.period_of_last_leave)
        categoryOfLeaveTv = findViewById(R.id.category_of_last_leave)
        leaveAddressTv = findViewById(R.id.leave_address)
        performingPersonTv = findViewById(R.id.performing_person_during_leave)
        applyDateTv = findViewById(R.id.apply_date)
        statusTv = findViewById(R.id.status)
        descriptionTv = findViewById(R.id.leave_description)
        willApprovedTv = findViewById(R.id.assign_to)
        assignToTv = findViewById(R.id.assignToTv)

        cardLeaveInfo = findViewById(R.id.card_info_leave)
        leaveInformation = findViewById(R.id.linear_info_leave)
        cardDescription = findViewById(R.id.card_description)
        leaveDescription = findViewById(R.id.linear_description)
        periodTv = findViewById(R.id.period)

        loading = findViewById(R.id.progressItem)
        viewDetail = findViewById(R.id.view_detail_leave)
        remarkLayout = findViewById(R.id.remarkLayout)
        remarkTv = findViewById(R.id.remarkTv)

        //Action Approve and Reject
        linearLayoutAction = findViewById(R.id.linearLayoutAction)
        btnRejectLeave = findViewById(R.id.btnRejectLeave)
        btnApproveLeave = findViewById(R.id.btnApproveLeave)

        btnLeaveBalance.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_home_list_balance))
    }

    private fun initData(){

        if (intent != null && intent.hasExtra("id")){
            leaveID = intent.getStringExtra("id").toString()
        }

        // From notification
        if (intent != null && intent.hasExtra("action")){
            actionNotification = intent.getStringExtra("action").toString()
        }

        user = Gson().fromJson(UserSessionManagement(this).userDetail, User::class.java)
        val businessKy = user.userBusinessKey
        if (businessKy != null){
            userBusinessKey = businessKy
        }

        getDetailLeave()
    }

    private fun initAction(){
        iconBack.setOnClickListener { finish() }
        btnLeaveBalance.visibility = View.VISIBLE

        titleToolbar.text = getString(R.string.leave_request_detail)

        cardLeaveInfo.setOnClickListener {
            isClick = MockUpData.getIsCheck()
            loadView(leaveInformation)
        }

        btnApproveLeave.setOnClickListener {
            showDialogApproveReject("approved")
        }
        btnRejectLeave.setOnClickListener {
            showDialogApproveReject("rejected")
        }
        btnLeaveBalance.setOnClickListener {
            val bttSheet = LeaveBalanceFragment().newInstance(userBussinessKey)
            bttSheet.show(supportFragmentManager, "user_bussiness_key")
        }
    }

    private fun getDetailLeave(){
        loading.visibility = View.VISIBLE
        LeaveManagementWs().leaveManagementDetail(this, leaveID, leaveDetailCallBack)
    }

    private val leaveDetailCallBack = object : LeaveManagementWs.OnCallBackLeaveTypeDetailListener{
        override fun onLoadListSuccessFull(leaveDetail: LeaveRequestDetail) {
            loading.visibility = View.GONE
            viewDetail.visibility = View.VISIBLE

            initValue(leaveDetail)

            if (leaveDetail.info_leave.status == "pending" && leaveDetail.profile_leave.user_business_key != userBusinessKey){
                linearLayoutAction.visibility = View.VISIBLE
                listType = "all"
            } else {
                linearLayoutAction.visibility = View.GONE
                listType = "my_list"

            }

            if (actionNotification != "") {
                setBackResult(listType)
            }

            // set margin bottom == 0
            if (linearLayoutAction.visibility == View.GONE) {
                Utils.setNoMarginBottomOnButton(findViewById<NestedScrollView>(R.id.mainLayoutScroll), 0)
            }

        }

        override fun onLoadFail(message: String) {
            loading.visibility = View.GONE
            Toast.makeText(this@LeaveRequestDetailActivity,message,Toast.LENGTH_SHORT).show()
        }

    }

    private fun sendActionApproveAndReject(status : String,  reason: String){
        val hashMap: HashMap<String,Any> = HashMap()

            hashMap["leave_id"] = leaveID
            hashMap["status"] = status
            hashMap["user_business_key"] = userBusinessKey
            if (status == "rejected"){
               hashMap["reason"] = reason
            }
        loading.visibility = View.VISIBLE
        LeaveManagementWs().getActionApproveAndRejectWs(this, hashMap, onCallBackActionApproveAndReject)

    }

    private val onCallBackActionApproveAndReject : LeaveManagementWs.OnCallBackCreateListener = object : LeaveManagementWs.OnCallBackCreateListener{
        override fun onLoadListSuccessFull(message: String) {
            loading.visibility = View.GONE
            Utils.customToastMsgError(this@LeaveRequestDetailActivity, message,true)
            setResult(RESULT_OK)
            finish()
        }

        override fun onLoadFail(message: String) {
            loading.visibility = View.GONE
            Utils.customToastMsgError(this@LeaveRequestDetailActivity, message,false)
        }
    }

    private fun showDialogApproveReject(status : String){
        val dialog = Dialog (this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.layout_alert_come_here)
        val btnCancel = dialog.findViewById<View>(R.id.btn_cancel) as TextView
        val btnOk = dialog.findViewById<View>(R.id.btn_ok) as TextView
        val titleTv = dialog.findViewById<TextView>(R.id.title)
        val  linearDescription = dialog.findViewById<LinearLayout>(R.id.layout)
        val  layoutEmpty = dialog.findViewById<LinearLayout>(R.id.layoutEmpty)
        val descriptionEdt : EditText = dialog.findViewById(R.id.description)

        if (status == "rejected"){
            titleTv.text = getString(R.string.are_you_sure_to_reject)
        } else {
            titleTv.text = resources.getString(R.string.are_u_sure_to_approve)
            linearDescription .visibility = View.GONE
            layoutEmpty.visibility = View.VISIBLE
        }

        btnOk.setOnClickListener {
            if (status == "rejected"){
                if(descriptionEdt.text.isNotEmpty()){
                    sendActionApproveAndReject(status,  descriptionEdt.text.toString())
                    dialog.dismiss()
                } else {
                    Toast.makeText(this,getString(R.string.please_input_your_reason), Toast.LENGTH_SHORT).show()
                }
            } else {
                sendActionApproveAndReject(status,"")
                dialog.dismiss()
            }
        }

        btnCancel.setOnClickListener { dialog.cancel() }
        dialog.show()
    }

    private fun initValue(leaveDetail: LeaveRequestDetail){

        userBussinessKey = leaveDetail.profile_leave.user_business_key

        addTextView(userNameTv,leaveDetail.profile_leave.user_name)
        addTextView(idNoTv,leaveDetail.profile_leave.id)
        addTextView(createDateTv,Utils.formatDateTime(leaveDetail.profile_leave.created_date, "yyyy-MM-dd kk:mm:ss", "dd-MMM-yyyy hh:mm a"))
        addTextView(designationTv,leaveDetail.profile_leave.designation)
        addTextView(leaveCategory,leaveDetail.profile_leave.leave_category + " "+ leaveDetail.profile_leave.total_user_leave_balance)
        addTextView(startDateTv,leaveDetail.profile_leave.start_date)
        addTextView(endDateTv,leaveDetail.profile_leave.end_date)
        leaveDetail.profile_leave.approver_user_name?.let { addTextView(assignToTv, it) }
        addTextView(willApprovedTv,leaveDetail.profile_leave.approved_by_user_name)
        addTextView(periodTv,leaveDetail.info_leave.period)

        addTextView(leaveDaysTv,leaveDetail.info_leave.during_leave)
        addTextView(dateOfReturnTv,leaveDetail.info_leave.last_leave_category)
        addTextView(periodOfLastLeaveTv,leaveDetail.info_leave.last_leave_period)
        addTextView(categoryOfLeaveTv,leaveDetail.info_leave.last_leave_category)
        addTextView(leaveAddressTv,leaveDetail.info_leave.leave_address)
        addTextView(applyDateTv,leaveDetail.info_leave.end_date)
        if (leaveDetail.profile_leave.remark != null) {
            remarkLayout.visibility = View.VISIBLE
            remarkTv.text = leaveDetail.profile_leave.remark
        }

        when (leaveDetail.info_leave.status) {
            "accepted" -> {
                statusTv.text = resources.getString(R.string.accepted)
                statusTv.backgroundTintList =
                    ColorStateList.valueOf(ContextCompat.getColor(this, R.color.green))
            }
            "pending" -> {
                statusTv.backgroundTintList =
                    ColorStateList.valueOf(ContextCompat.getColor(this, R.color.yellow))
                statusTv.text = resources.getString(R.string.pending)
            }
            "rejected" -> {
                statusTv.backgroundTintList =
                    ColorStateList.valueOf(ContextCompat.getColor(this, R.color.red))
                statusTv.text = resources.getString(R.string.rejected)
            }
            else -> {
                statusTv.backgroundTintList =
                    ColorStateList.valueOf(ContextCompat.getColor(this, R.color.red))
                statusTv.text = resources.getString(R.string.rejected)
            }
        }

        descriptionTv.text = leaveDetail.reason_leave
    }

    private fun setBackResult(listType : String){
        val intent = Intent(BroadcastTypes.UPDATE_LEAVE_MENU.name)
        intent.putExtra("listType", listType)
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
    }

    private fun addTextView(textView : TextView,value : String){
        if(value!=null && value.isNotEmpty()){
            textView.text = value
        } else{
            textView.text = "---"
        }

    }

    private fun loadView(linearLayout: LinearLayout) {
        if (isClick) {
            MockUpData.setIsCheck(false)
            linearLayout.visibility = View.VISIBLE
        } else {
            MockUpData.setIsCheck(true)
            linearLayout.visibility = View.GONE
        }

    }


}