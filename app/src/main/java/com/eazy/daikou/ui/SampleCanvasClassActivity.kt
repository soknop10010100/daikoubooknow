package com.eazy.daikou.ui

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Path
import android.view.View
import com.eazy.daikou.R
import com.eazy.daikou.helper.Utils

@SuppressLint("ViewConstructor")
class SampleCanvasClassActivity(private val isTop : Boolean, context: Context, private val fillColor: String) : View(context) {

    private val paint = Paint()
    private val path = Path()

    @SuppressLint("DrawAllocation")
    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        paint.strokeWidth = 1f
        paint.color = Color.parseColor(fillColor)
        paint.style = Paint.Style.FILL_AND_STROKE
        path.fillType = Path.FillType.EVEN_ODD

        val y  = Utils.getScreenHeight(context).toFloat()
        val x =  Utils.getScreenWidth(context).toFloat()
        var height = 200

        if (isTop){ // Top Draw
            path.moveTo(x, 0F)// top right
            path.lineTo(x, 20f) // bottom right
            // path.lineTo(x, Utils.dpToPx(context,40).toFloat()) // bottom right

            val c1= (x - x * 2 / 3)
            val c2= (x - x * 5 / 6)
            val c3= (height * 6 / 5)

            path.cubicTo(c1, 0F, c2, c3.toFloat(), 0F, height.toFloat()) // curve bottom left

            Utils.logDebug("jeeeeeeeeeeeeeeeeeeeeeeeee", "$c1         $0          $c2      $c3            $0      $height")

            path.lineTo(0F, 0F) //top left
        } else {    // Bottom Draw
//            val c4= (y - height)
//            val c1= c4 + 200
//            val c2= 560f
//            val c3= 1000f
//            val c5 = x + 60
//
//            path.moveTo(0f, y) // bottom left
//            path.lineTo(0f, y - 40) // top left
//            path.cubicTo(x, c1, c2, c3,  c5, c4) // curve to top right
//            Utils.logDebug("jeeeeeeeeeeeeeeeeeeeeeeeee", x.toString() + "         " + c1 + "          " + c2 + "      " + c3 + "            "+ c5 + "      " + c4)

            height += getStatusBarHeight()
            val c4= y - height
            val c1= x * 2 / 3
            val c2= x * 5 / 6
            val c3= y - height * 6 / 5

            path.moveTo(0f, y) // bottom left
            path.lineTo(0f, y - (40 + getStatusBarHeight())) // top left

            path.cubicTo(c1, y, c2, c3, x, c4) // curve to top right

            Utils.logDebug("jeeeeeeeeeeeeeeeeeeeeeeeee", "$c1      $y            $c2         $c3          $x      $c4")

            path.lineTo(x, y) // bottom right
        }

        path.close()
        canvas.drawPath(path, paint)
    }

    private fun getStatusBarHeight(): Int {
        var result = 0
        val resourceId = resources.getIdentifier("status_bar_height", "dimen", "android")
        if (resourceId > 0) {
            result = resources.getDimensionPixelSize(resourceId)
        }
        return result
    }
}