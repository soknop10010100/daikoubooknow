package com.eazy.daikou.ui.home.service_provider_new.fragment_service_provider

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.TextView
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.ui.home.service_provider_new.activity.QuotationProviderActivity
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class ItemDeliveryFragment : BottomSheetDialogFragment() {

    private lateinit var iconQuotationTv: TextView
    private lateinit var iconInvoiceTv: TextView

    fun newInstance(typeSelectItem: String): ItemDeliveryFragment {
        val fragment = ItemDeliveryFragment()
        val args = Bundle()
        args.putString("type_select_delivery", typeSelectItem)
        fragment.arguments = args
        return fragment

    }
    override fun onStart() {
        super.onStart()
        view?.viewTreeObserver?.addOnGlobalLayoutListener {
            val behavior = BottomSheetBehavior.from(view!!.parent as View)
            behavior.peekHeight = WindowManager.LayoutParams.MATCH_PARENT
        }
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_item_delivery, container, false)

        initView(view)

        initData()

        initAction()

        return view
    }

    private fun initView(view : View){
        iconQuotationTv = view.findViewById(R.id.iconQuotationTv)
        iconInvoiceTv = view.findViewById(R.id.iconInvoiceTv)
    }
    private fun initData(){

    }

    private fun initAction(){
        iconQuotationTv.setOnClickListener(CustomSetOnClickViewListener{
            startActivity(Intent(context, QuotationProviderActivity::class.java))
            dismiss()
        })
        iconInvoiceTv.setOnClickListener(CustomSetOnClickViewListener{
            Utils.customToastMsgError(context, "Coming soon", true)
            dismiss()
        })
    }
}