package com.eazy.daikou.ui.home.hrm.leave_request

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.core.content.ContextCompat
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.base.MockUpData
import com.eazy.daikou.request_data.request.hr_ws.LeaveManagementWs
import com.eazy.daikou.request_data.static_data.StaticUtilsKey
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import java.text.SimpleDateFormat
import java.util.*

class CreateLeaveRequestActivity : BaseActivity() {

    private lateinit var btnBackTv : TextView
    private lateinit var linearStartDate : LinearLayout
    private lateinit var linearEndDate : LinearLayout
    private lateinit var dateStartTv : TextView
    private lateinit var dateEndTv : TextView
    private lateinit var startDateCalendar : CalendarView
    private lateinit var endDateCalendar : CalendarView
    private lateinit var leaveTypeTv : TextView
    private lateinit var reasonEdt : EditText
    private lateinit var btnCreate : TextView
    private lateinit var progressBarLoading : ProgressBar
    private lateinit var willBeApproveTv : TextView
    private lateinit var periodTv : TextView

    private var monStart = 0
    private var yearStart = 0
    private var dayStart = 0
    private var yearEnd = 0
    private var monEnd = 0
    private var dayEnd = 0
    private var startDate = ""
    private var endDate = ""
    private var userId = ""
    private var leaveCategoryId = ""
    private var approvedById = ""
    private var periodId = ""
    private var REQUEST_CODE_LEAVE_TYPE = 436
    private lateinit var simpleDateFormat : SimpleDateFormat

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_leave_request)

        initView()

        initData()

        initAction()
    }

    private fun initView() {
        btnBackTv = findViewById(R.id.btn_back)
        linearStartDate = findViewById(R.id.start_date_linear)
        linearEndDate = findViewById(R.id.end_date_linear)
        startDateCalendar = findViewById(R.id.select_start_date)
        endDateCalendar = findViewById(R.id.select_date_end)
        dateStartTv = findViewById(R.id.date_start)
        dateEndTv = findViewById(R.id.date_end)
        reasonEdt = findViewById(R.id.reason_edt)

        leaveTypeTv = findViewById(R.id.leave_type)
        btnCreate = findViewById(R.id.btnSave)
        progressBarLoading = findViewById(R.id.progressItem)
        progressBarLoading.visibility = View.GONE

        willBeApproveTv = findViewById(R.id.will_be_approve)
        periodTv = findViewById(R.id.periodTv)
    }

    @SuppressLint("Range")
    private fun initData() {

        simpleDateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        val date = simpleDateFormat.format(Date())
        val c = Calendar.getInstance()
        yearStart = c.get(Calendar.YEAR)
        monStart = c.get(Calendar.MONTH + 1)
        dayStart = c.get(Calendar.DAY_OF_MONTH)
        yearEnd = c.get(Calendar.YEAR)
        monEnd = c.get(Calendar.MONTH + 1)
        dayEnd = c.get(Calendar.DAY_OF_MONTH)
        startDate = date
        endDate = date

        dateStartTv.text = startDate
        dateEndTv.text = endDate
        onClickDate("start")

        // Just Back Listener Enabled Button Check On List
        val intent = Intent()
        intent.putExtra("isClickedFirst", true)
        setResult(RESULT_OK, intent)

        val user = UserSessionManagement(this)
        userId = user.userId

        setActionButton()
    }

    private fun initAction() {
        btnBackTv.setOnClickListener { finish() }

        linearStartDate.setOnClickListener {
            onClickDate("start")
        }
        linearEndDate.setOnClickListener {
            onClickDate("end")
        }

        leaveTypeTv.setOnClickListener {
            startActivityToLeaveType(StaticUtilsKey.leave_category_action)
        }
        willBeApproveTv.setOnClickListener {
            startActivityToLeaveType(StaticUtilsKey.leave_approved_by_action)
        }

        btnCreate.setOnClickListener {
            createLeaveManagement()
        }

        periodTv.setOnClickListener{
            startActivityToLeaveType(StaticUtilsKey.leave_period_action)
        }

        startDateCalendar.setOnDateChangeListener { _, year, month, day ->
            val date = year.toString() + "-" + (month + 1).toString() + "-" + day.toString()
            monStart = month + 1
            yearStart = year
            dayStart = day
            startDate = Utils.formatDateFromString("yyyy-M-dd", "yyyy-MM-dd", date)

            dateStartTv.text = startDate

        }

        endDateCalendar.setOnDateChangeListener { _, year, month, day ->
            val date = year.toString() + "-" + (month + 1).toString() + "-" + day.toString()
            yearEnd = year
            monEnd = month + 1
            dayEnd = day
            endDate = Utils.formatDateFromString("yyyy-M-dd", "yyyy-MM-dd", date)

            dateEndTv.text = endDate
        }
    }

    private fun onClickDate(types: String) {
        if (types == "start") {
            dateStartTv.setTextColor(ContextCompat.getColor(this, R.color.light_grey))
            linearStartDate.backgroundTintList = getColorStateList(R.color.calendar_text_title)
            dateEndTv.setTextColor(Color.GRAY)
            linearEndDate.backgroundTintList = getColorStateList(R.color.color_gray_item)

            startDateCalendar.visibility = View.VISIBLE
            endDateCalendar.visibility = View.GONE
        } else {
            dateEndTv.setTextColor(ContextCompat.getColor(this, R.color.light_grey))
            linearEndDate.backgroundTintList = getColorStateList(R.color.calendar_text_title)
            dateStartTv.setTextColor(Color.GRAY)
            linearStartDate.backgroundTintList = getColorStateList(R.color.color_gray_item)
            startDateCalendar.visibility = View.GONE
            endDateCalendar.visibility = View.VISIBLE
        }

    }

    @SuppressLint("Range")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE_LEAVE_TYPE && resultCode == RESULT_OK) {
            if (data != null) {
                var name = ""
                var id = ""
                if (data.hasExtra("id_leave")){
                    id = data.getStringExtra("id_leave") as String
                }
                if (data.hasExtra("name_leave")) {
                    name = data.getStringExtra("name_leave") as String
                }
                if (data.hasExtra("action")){
                    when (data.getStringExtra("action") as String) {
                        StaticUtilsKey.leave_category_action -> {   // leave category
                            leaveTypeTv.text = name
                            leaveCategoryId = id

                            setActionButton()
                        }
                        StaticUtilsKey.leave_period_action -> {   // leave period
                            periodTv.text = name
                            periodId = id

                            setActionButton()
                        }
                        else -> {                                   // approved by
                            willBeApproveTv.text = name
                            approvedById = id

                            setActionButton()
                        }
                    }
                }

            }
        }
    }
    
    private fun startActivityToLeaveType(type : String){
        val intent = Intent(this, LeaveTypeCategoryActivity::class.java)
        intent.putExtra("action", type)
        when (type) {
            StaticUtilsKey.leave_category_action -> {
                intent.putExtra("id", leaveCategoryId)
            }
            StaticUtilsKey.leave_period_action -> {
                intent.putExtra("id", periodId)
            }
            else -> {
                intent.putExtra("id", approvedById)
            }
        }
        startActivityForResult(intent, REQUEST_CODE_LEAVE_TYPE)
    }

    private fun initHasMap(): HashMap<String, Any> {
        val hashMap = HashMap<String, Any>()
        hashMap["reason"] = reasonEdt.text.toString()
        hashMap["user_business_key"] = MockUpData.userBusinessKey(UserSessionManagement(this))
        hashMap["leave_category_id"] = leaveCategoryId
        hashMap["start_date"] = startDate
        hashMap["end_date"] = endDate
        hashMap["approved_by_id"] = approvedById
        hashMap["period"] = periodId
        return hashMap
    }

    private fun createLeaveManagement() {
        progressBarLoading.visibility = View.VISIBLE
        if (reasonEdt.text.toString().isEmpty()) {
            btnCreate.isEnabled = true
            progressBarLoading.visibility = View.GONE
            Utils.customToastMsgError(this, resources.getString(R.string.please_write_your_reason), false)
        } else {
            if (endDate < startDate) {
                progressBarLoading.visibility = View.GONE
                Utils.customToastMsgError(this@CreateLeaveRequestActivity, resources.getString(R.string.day_end_must_bigger_than_day), false)
            } else {
                btnCreate.isEnabled = false
                LeaveManagementWs().createLeaveManagement(this, initHasMap(), createCallBack)
            }
        }
    }

    private val createCallBack = object : LeaveManagementWs.OnCallBackCreateListener {
        override fun onLoadListSuccessFull(message: String) {
            btnCreate.isEnabled = true
            progressBarLoading.visibility = View.GONE
            setResult(RESULT_OK)
            finish()
            Utils.customToastMsgError(this@CreateLeaveRequestActivity, message, true)
        }

        override fun onLoadFail(message: String) {
            btnCreate.isEnabled = true
            progressBarLoading.visibility = View.GONE
            Utils.customToastMsgError(this@CreateLeaveRequestActivity, message, false)
        }

    }

    @SuppressLint("Range")
    private fun setActionButton(){
        btnCreate.isEnabled = isEnableBtn()
        Utils.setBgTint(btnCreate, if (isEnableBtn()) R.color.appBarColorOld  else R.color.gray)
    }

    private fun isEnableBtn() : Boolean{
        if (leaveCategoryId != "" && approvedById != "" && periodId != ""){
            return true
        }
        return false
    }


}