package com.eazy.daikou.ui.profile

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import androidx.activity.result.ActivityResult
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.base.MockUpData
import com.eazy.daikou.request_data.request.profile_ws.ProfileWs
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.profile.PropertyItemModel
import com.eazy.daikou.model.profile.User
import com.eazy.daikou.model.profile.UserTypeProfileModel
import com.eazy.daikou.ui.profile.adapter.SwitchProfilePropertyAdapter

class SwitchProfileAndPropertyActivity : BaseActivity() {

    private lateinit var recyclerView : RecyclerView
    private var userTypeList : ArrayList<UserTypeProfileModel> = ArrayList()
    private var action : String = ""
    private var user : User? = null
    private var propertyList : ArrayList<PropertyItemModel> = ArrayList()
    private lateinit var userSessionManagement : UserSessionManagement
    private lateinit var progressLayout : ProgressBar
    private var accountId : String = ""
    private var propertyName : String = ""
    private var switchProfilePropertyAdapter : SwitchProfilePropertyAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_switch_profile_and_property)

        initView()

        initData()

        initAction()

    }

    private fun initView(){
        recyclerView = findViewById(R.id.recyclerView)
        progressLayout = findViewById(R.id.progressItem)
        progressLayout.visibility = View.GONE
    }

    private fun initData(){
        if (intent != null && intent.hasExtra("action")){
            action = intent.getStringExtra("action").toString()
        }

        if (intent != null && intent.hasExtra("active_profile_list")){
            userTypeList  = intent.getSerializableExtra("active_profile_list") as ArrayList<UserTypeProfileModel>
        }

        userSessionManagement = UserSessionManagement(this)

        user = MockUpData.getUserItem(userSessionManagement)

        addAccountProperty()

    }

    private fun addAccountProperty() {
        for (usertype in userTypeList){
            if (usertype.user_type == user!!.activeUserType){
                propertyList.addAll(usertype.accounts)
            }
        }
    }

    private fun getUserAccountList(){
        progressLayout.visibility = View.VISIBLE
        ProfileWs().getUserAccountList(this, object : ProfileWs.CallBackSendQrCodeListener{
            override fun onSuccess(msg: String?) {}

            @SuppressLint("NotifyDataSetChanged")
            override fun onSuccess(list: MutableList<UserTypeProfileModel>) {
                progressLayout.visibility = View.GONE

                userTypeList.clear()
                userTypeList.addAll(list)

                addAccountProperty()

                if(switchProfilePropertyAdapter != null)    switchProfilePropertyAdapter!!.notifyDataSetChanged()
            }

            override fun onFailed(error: String?) {
                progressLayout.visibility = View.GONE
                Utils.customToastMsgError(this@SwitchProfileAndPropertyActivity, error, false)
            }

        })
    }

    private fun initAction(){
        val title: String = if (action == "switch_profile"){
            resources.getString(R.string.user_type)
        } else {    // action == "switch_property"
            resources.getString(R.string.property_)
        }
        Utils.customOnToolbar(this, title){finish()}


        initRecyclerView()

        // More request user account
        if (userTypeList.isEmpty()){
            getUserAccountList()
        }
    }

    private fun initRecyclerView(){
        recyclerView.layoutManager = LinearLayoutManager(this)
        switchProfilePropertyAdapter = SwitchProfilePropertyAdapter(this, action, propertyList, userTypeList, user!!, object : SwitchProfilePropertyAdapter.OnClickCallBackListener{
            override fun onClickCallBack(userTypeProfileModel: UserTypeProfileModel) {
                for (property in userTypeProfileModel.accounts){
                    if (property.id != null) {
                        if (property.id == user!!.accountId) {
                            if (property.id != null) accountId = property.id!!
                            if (property.name != null) propertyName = property.name!!
                        } else {
                            if (userTypeProfileModel.accounts[0].id != null)  accountId = userTypeProfileModel.accounts[0].id!!
                            if (userTypeProfileModel.accounts[0].name != null)    propertyName = userTypeProfileModel.accounts[0].name!!
                        }
                    }
                }

                switchProfileUser(userTypeProfileModel, accountId, propertyName)
            }

            override fun onClickCallBack(propertyItemModel: PropertyItemModel) {
                if (propertyItemModel.id != null)    accountId = propertyItemModel.id!!
                if (propertyItemModel.name != null)  propertyName = propertyItemModel.name!!
                switchProfileUser(UserTypeProfileModel(), propertyItemModel.id!!, propertyItemModel.name!!)
            }
        })
        recyclerView.adapter = switchProfilePropertyAdapter
    }

    private fun switchProfileUser(userTypeProfileModel: UserTypeProfileModel, accountId : String, accountName : String){
        progressLayout.visibility = View.VISIBLE
        val hashMap : HashMap<String, Any> = HashMap()
        hashMap["account_id"] = accountId
        ProfileWs().switchProfileUser(this, hashMap, object : ProfileWs.CallBackSendQrCodeListener{
            override fun onSuccess(msg: String?) {
                progressLayout.visibility = View.GONE
                Utils.customToastMsgError(this@SwitchProfileAndPropertyActivity, msg, true)
                if (action == "switch_profile"){
                    user!!.activeUserType = userTypeProfileModel.user_type
                    user!!.accountId = accountId
                    user!!.accountName = accountName

                    userSessionManagement.saveData(user, true)

                    setBackValue(userTypeProfileModel.user_type!!)
                } else {
                    user!!.accountId = accountId
                    user!!.accountName = accountName

                    userSessionManagement.saveData(user, true)

                    setBackValue(propertyName)
                }
            }

            override fun onSuccess(list: MutableList<UserTypeProfileModel>?) {}

            override fun onFailed(error: String?) {
                progressLayout.visibility = View.GONE
                Utils.customToastMsgError(this@SwitchProfileAndPropertyActivity, error, false)
            }

        })
    }

    private fun setBackValue(name : String){
        intent.putExtra("action", action)
        intent.putExtra("name", name)
        intent.putExtra("property_name", propertyName)
        setResult(RESULT_OK, intent)
        finish()
    }

    companion object{

        fun checkDisplayAccount(result: ActivityResult) : HashMap<String, Any> {
            val hashMap = HashMap<String, Any>()
            if (result.resultCode == RESULT_OK){
                if (result.data != null) {
                    val action: String = result.data!!.getStringExtra("action").toString()
                    val name: String = result.data!!.getStringExtra("name").toString()
                    val propertyName: String = result.data!!.getStringExtra("property_name").toString()

                    if (action.equals("switch_profile", ignoreCase = true)) {
                        hashMap["name"] = name      // name is user type
                        hashMap["property_name"] = propertyName // account name
                    } else {
                        hashMap["name"] = name  // name is account name
                    }
                }
            }
            return hashMap
        }

    }

    interface OnCallBackListener {
        fun callBackListener(accountName : String)
    }
}