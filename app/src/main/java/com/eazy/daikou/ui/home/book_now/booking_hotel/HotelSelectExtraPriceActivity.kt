package com.eazy.daikou.ui.home.book_now.booking_hotel

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.base.MockUpData
import com.eazy.daikou.request_data.request.book_now_ws.BookingHotelWS
import com.eazy.daikou.request_data.request.book_now_ws.RequestHashMapData
import com.eazy.daikou.helper.*
import com.eazy.daikou.model.booking_hotel.*
import com.eazy.daikou.ui.home.book_now.booking_hotel.adapter.HotelExtraPriceAdapter
import com.eazy.daikou.ui.home.book_now.booking_hotel.adapter.HotelSelectEventOptionAdapter
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class HotelSelectExtraPriceActivity : BaseActivity(), View.OnClickListener {

    private lateinit var progressBar: ProgressBar
    private var totalPrice : Double = 0.0
    private lateinit var btnBooking : TextView
    private var hotelId = ""
    private var adults = "1"
    private var children = "0"
    private var startDate : String = ""
    private var endDate : String = ""
    private lateinit var dateFormatServer : SimpleDateFormat
    private lateinit var adultsTv : TextView
    private lateinit var childrenTv : TextView
    private lateinit var startDateEndDateTv : LinearLayout
    private lateinit var startDateTv : TextView
    private lateinit var endDateTv : TextView
    private lateinit var guestLayout : LinearLayout

    private var extraPriceList = ArrayList<ExtraPrice>()
    private var category = ""

    private lateinit var chooseTicketRecyclerView : RecyclerView
    private lateinit var eventActivityAdapter : HotelSelectEventOptionAdapter
    private var itemEventActivityModelList : ArrayList<TicketType> = ArrayList()
    private lateinit var totalItemTv : TextView
    private lateinit var totalTv : TextView

    private var getHotelDetailItemModel: HotelBookingDetailModel? = null
    private var totalAllTicket = 0

    //Space
    private var maxPeople = 1
    private var isCanBook = true
    private lateinit var paymentFeeLayout : LinearLayout
    private var totalPriceFeeService = 0.0
    private var priceCanBookDate : Double = 0.0
    private lateinit var dateStartEndTv : TextView
    private lateinit var priceByDate : TextView
    private lateinit var totalPeopleTv : TextView
    private var duration = ""
    private val serviceFeeList = ArrayList<ExtraPrice>()
    //Tour
    private var dateFromToTour = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hotel_select_extra_price)

        initView()

        initData()

        initAction()

    }

    private fun initView(){
        progressBar = findViewById(R.id.progressItem)
        progressBar.visibility = View.GONE
        btnBooking = findViewById(R.id.linear_card)
        adultsTv = findViewById(R.id.adultsTv)
        childrenTv = findViewById(R.id.childrenTv)
        startDateEndDateTv = findViewById(R.id.startDateEndDateTv)
        startDateTv = findViewById(R.id.startDateTv)
        endDateTv = findViewById(R.id.endDateTv)
        guestLayout = findViewById(R.id.guestLayout)
        findViewById<LinearLayout>(R.id.paymentLayout).visibility = View.VISIBLE

        chooseTicketRecyclerView = findViewById(R.id.chooseTicketRecyclerView)

        totalItemTv = findViewById(R.id.totalItemTv)
        totalTv = findViewById(R.id.totalTv)

        paymentFeeLayout = findViewById(R.id.paymentFeeLayout)

        dateStartEndTv = findViewById(R.id.dateTv)
        priceByDate  = findViewById(R.id.priceDateTv)
        totalPeopleTv = findViewById(R.id.totalPeopleTv)
    }

    private fun initData(){
        hotelId = GetDataUtils.getDataFromString("hotel_id", this)
        startDate = GetDataUtils.getDataFromString("start_date", this)
        endDate = GetDataUtils.getDataFromString("end_date", this)
        children = GetDataUtils.getDataFromString("children", this)
        adults = GetDataUtils.getDataFromString("adults", this)
        dateFormatServer = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())

        if (intent != null && intent.hasExtra("price")){
            priceCanBookDate = intent.getDoubleExtra("price", 0.0)
        }
        category = GetDataUtils.getDataFromString("category", this)

        if (intent != null && intent.hasExtra("item_detail_model")){
            getHotelDetailItemModel = intent.getSerializableExtra("item_detail_model") as HotelBookingDetailModel
            if (getHotelDetailItemModel!!.max_people != null && getHotelDetailItemModel!!.max_people != ""){
                maxPeople = getHotelDetailItemModel!!.max_people!!.toInt()
            }

            duration = Utils.validateNullValue(getHotelDetailItemModel!!.duration)

            dateFromToTour = Utils.validateNullValue(getHotelDetailItemModel!!.date_form_to)

            //Service Fee Payment
            serviceFeeList.addAll(getHotelDetailItemModel!!.buyer_fees)
            serviceFeeList.addAll(getHotelDetailItemModel!!.service_fees)

            //Extra Price
            try {
                extraPriceList.addAll(getHotelDetailItemModel!!.extra_price)
            } catch (ex : Exception){
                Utils.logDebug("ghjkjhghjkjhjkl", ex.message)
            }

            addViewItem(paymentFeeLayout, serviceFeeList)
        }

        Utils.customOnToolbar(this, RequestHashMapData.categoryTitle(this)[category] + " " + resources.getString(R.string.booking)){finish()}

        if (category == "space"){
            findViewById<LinearLayout>(R.id.selectDateLayout).visibility = View.VISIBLE
            findViewById<CardView>(R.id.chooseTicketLayout).visibility = View.GONE

            findViewById<CardView>(R.id.priceTotalByDateLayout).visibility = View.VISIBLE

            totalPeopleTv.visibility = View.VISIBLE

            totalItemTv.visibility = View.GONE
            findViewById<TextView>(R.id.titleTotalItemTv).text = resources.getString(R.string.total)
        } else if (category == "tour"){
            findViewById<TextView>(R.id.headerTitleTour).text = resources.getString(R.string.choose_number_of_people)
        }


    }

    private fun addViewItem(eventLayout : LinearLayout, list : ArrayList<ExtraPrice>){
        totalPriceFeeService = 0.0
        for (item in list) {
            val view: View = LayoutInflater.from(this).inflate(R.layout.custom_textview_left_right, null)
            val keyTv = view.findViewById(R.id.keyTv) as TextView
            val valueTv = view.findViewById(R.id.valueTv) as TextView
            keyTv.text = item.name
            valueTv.text = item.price_display.toString()

            totalPriceFeeService += item.price

            eventLayout.addView(view)
        }
        if (list.size == 0) findViewById<CardView>(R.id.totalPriceCardLayout).visibility = View.GONE

        setPriceByDate()

    }

    private fun initAction(){
        if(adults == "")    adults = "1"
        if(children == "")    children = "0"
        adultsTv.text = String.format("%s %s", adults, resources.getString(R.string.adults))
        childrenTv.text = String.format("%s %s", children, resources.getString(R.string.children))
        totalPeopleTv.text = String.format("%s : %s", resources.getString(R.string.total_people), (adults.toInt() + children.toInt()))

        getStartEndDate()

        startDateEndDateTv.setOnClickListener(this)
        guestLayout.setOnClickListener(this)
        btnBooking.setOnClickListener(this)

        initExtraPriceRecyclerView()

        initItemRecycleView()

        checkValidateButtonPay()
    }

    private fun initItemRecycleView(){
        chooseTicketRecyclerView.layoutManager = LinearLayoutManager(this)
        chooseTicketRecyclerView.isNestedScrollingEnabled = false

        var startTime = ""
        var duration = ""
        if (getHotelDetailItemModel != null){
            itemEventActivityModelList.addAll(getHotelDetailItemModel!!.ticket_types)
            if (getHotelDetailItemModel!!.start_time != null) startTime = getHotelDetailItemModel!!.start_time!!
            if (getHotelDetailItemModel!!.duration != null) duration = getHotelDetailItemModel!!.duration!!
        }
        eventActivityAdapter = HotelSelectEventOptionAdapter(itemEventActivityModelList, startTime, duration, category, onClickItemCallBackListener)
        chooseTicketRecyclerView.adapter = eventActivityAdapter
    }

    private val onClickItemCallBackListener = object : HotelSelectEventOptionAdapter.OnClickCallBackLister{
        @SuppressLint("NotifyDataSetChanged")
        override fun onClickSpinnerCallBack(itemRoomHotelBookingModel: TicketType, action: String, imageView: ImageView) {
            when(action){
                "add" ->{
                    var numberCount = itemRoomHotelBookingModel.quantity
                    if (itemRoomHotelBookingModel.num > 0 && itemRoomHotelBookingModel.num > itemRoomHotelBookingModel.quantity) {
                        numberCount = numberCount.plus(1)
                        itemRoomHotelBookingModel.totalAllPrice = (itemRoomHotelBookingModel.price * numberCount)

                        itemRoomHotelBookingModel.quantity = numberCount

                        itemRoomHotelBookingModel.totalRoom = numberCount
                        itemRoomHotelBookingModel.isClick = numberCount > 0
                        itemRoomHotelBookingModel.numberRoomSelect = numberCount
                        with(eventActivityAdapter) { notifyDataSetChanged() }

                        checkValidateButtonPay()
                    }
                }
                "remove" ->{
                    var numberCount = itemRoomHotelBookingModel.quantity
                    if (itemRoomHotelBookingModel.num > 0 && numberCount > 0) {
                        numberCount = numberCount.minus(1)
                        itemRoomHotelBookingModel.totalAllPrice = (itemRoomHotelBookingModel.price * numberCount)

                        itemRoomHotelBookingModel.quantity = numberCount

                        itemRoomHotelBookingModel.totalRoom = numberCount
                        itemRoomHotelBookingModel.isClick = numberCount > 0
                        itemRoomHotelBookingModel.numberRoomSelect = numberCount
                        eventActivityAdapter.notifyDataSetChanged()

                        checkValidateButtonPay()
                    }
                }
            }
        }
    }

    private fun checkValidateButtonPay(){
        if (isEnableButtonPay()){
            btnBooking.isEnabled = true
            Utils.setBgTint(btnBooking, R.color.color_book_now)
        } else {
            btnBooking.isEnabled = false
            Utils.setBgTint(btnBooking, R.color.gray)
        }
        setValueOnPrice()
    }

    private fun setValueOnPrice() {
        totalAllTicket = 0
        totalAllTicket = 0
        totalPrice = 0.0
        for (itemCardBooking in itemEventActivityModelList) {
            if (itemCardBooking.isClick) {
                val subTotal: Double = itemCardBooking.totalAllPrice
                totalPrice += subTotal
                totalAllTicket += itemCardBooking.numberRoomSelect
            }
        }
        var total = 0.0
        for (item in extraPriceList){
            if (item.isClick){
                total += item.price
            }
        }

        totalPrice += total

        // Price total can book by date
        var getTotalPriceFeeService = totalPriceFeeService
        getTotalPriceFeeService += priceCanBookDate
        totalPrice += getTotalPriceFeeService

        totalTv.text = String.format("%s %s", "$", Utils.formatDecimalFormatValue(totalPrice))
        totalItemTv.text = String.format("%s ", totalAllTicket)
    }

    private fun isEnableButtonPay() : Boolean{
        if (category == "space")    return true

        for (itemCardBooking in itemEventActivityModelList){
            if (itemCardBooking.isClick){
                return true
            }
        }
        return false
    }


    private fun initExtraPriceRecyclerView(){
        val extraPriceRecyclerView = findViewById<RecyclerView>(R.id.extraPriceRecyclerView)
        extraPriceRecyclerView.layoutManager = LinearLayoutManager(this)
        var hotelExtraPriceAdapter : HotelExtraPriceAdapter? = null
        hotelExtraPriceAdapter = HotelExtraPriceAdapter(extraPriceList, object : HotelExtraPriceAdapter.OnClickItemListener{
            @SuppressLint("NotifyDataSetChanged")
            override fun onClickListener(item: ExtraPrice) {
                item.isClick = !item.isClick
                hotelExtraPriceAdapter!!.notifyDataSetChanged()

                checkValidateButtonPay()
            }

        })
        extraPriceRecyclerView.adapter = hotelExtraPriceAdapter
        extraPriceRecyclerView.isNestedScrollingEnabled = false

        if (extraPriceList.size == 0)   findViewById<CardView>(R.id.extraPriceCardLayout).visibility = View.GONE
    }

    private fun getStartEndDate(){
        // Start Date
        var dateToday = Date()
        if (startDate == "")    startDate = dateFormatServer.format(dateToday)
        startDateTv.text = startDate

        // Due Date
        val calendar = Calendar.getInstance()
        calendar.time = dateToday
        calendar.add(Calendar.DATE, 1)
        dateToday = calendar.time
        if (endDate == "")    endDate = dateFormatServer.format(dateToday)
        endDateTv.text = endDate
    }

    private val onCallBackRequestListener = object : BookingHotelWS.OnCallBackRoomHotelListener{
        @SuppressLint("NotifyDataSetChanged")
        override fun onSuccessRoomHotel(locationHotelModel: ArrayList<ItemRoomHotelBookingModel>) {}

        override fun onSuccessBookingRoom(bookingId: String, bookingCode: String, kessPayment : String) {
            progressBar.visibility = View.GONE
            val intent = Intent(this@HotelSelectExtraPriceActivity, HotelBookingAddAddressPaymentActivity::class.java).apply {
                putExtra("booking_id", bookingId)
                putExtra("booking_code", bookingCode)
                putExtra("kess_payment", kessPayment)
                putExtra("adults", adults)
                putExtra("children", children)
                putExtra("price", String.format("%s %s", "$", Utils.formatDecimalFormatValue(totalPrice)))
                putExtra("start_date", startDate)
                putExtra("end_date", endDate)
                putExtra("category", category)

                if (getHotelDetailItemModel != null){
                    if (getHotelDetailItemModel!!.name != null){
                        intent.putExtra("hotel_name", getHotelDetailItemModel!!.name)
                    }
                    if (getHotelDetailItemModel!!.image != null){
                        intent.putExtra("hotel_image", getHotelDetailItemModel!!.image)
                    }
                }

                putExtra("hashmap_draft_data",  bookingRoomData())

                //Event / Activity
                putExtra("extra_price_list", extraPriceList)
                putExtra("event_list", itemEventActivityModelList)
                putExtra("duration", duration)
                putExtra("service_fee_list", serviceFeeList)
                putExtra("date_form_to", dateFromToTour)

            }
            startActivity(intent)
        }

        override fun onFailed(message: String) {
            progressBar.visibility = View.GONE
            Utils.customToastMsgError(this@HotelSelectExtraPriceActivity, message, false)
        }
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.guestLayout -> {
                Utils.checkEnableView(v)
                startSelectCategory("select_room")
            }
            R.id.startDateEndDateTv -> {
                Utils.checkEnableView(v)
                startEndDateRang()
            }
            R.id.linear_card ->{
                Utils.checkEnableView(v)
                if (AppAlertCusDialog.isSuccessLoggedIn(this)) {
                    progressBar.visibility = View.VISIBLE
                    if (isCanBook){
                        BookingHotelWS().bookingRoomDraft(this, bookingRoomData(), onCallBackRequestListener)
                    } else {
                        Utils.customToastMsgError(this, "Cannot book for this date", false)
                    }
                }
            }
        }
    }

    private fun startSelectCategory(action: String){
        val selectLocationBookingFragment = SelectLocationBookingFragment().newInstance("no_room_check", adults, children, maxPeople, action)
        selectLocationBookingFragment.initListener(onCallBackListener)
        selectLocationBookingFragment.show(supportFragmentManager, "fragment")
    }

    private val onCallBackListener = object : SelectLocationBookingFragment.OnClickCallBackListener{
        override fun onClickSelectLocation(titleHeader: LocationHotelModel) {}

        override fun onClickSelect(titleHeader: String, r : String, ad : String, ch : String) {
            adults = ad
            children = ch
            adultsTv.text = String.format("%s %s", adults, resources.getString(R.string.adults))
            childrenTv.text = String.format("%s %s", children, resources.getString(R.string.children))

            totalPeopleTv.text = String.format("%s : %s", resources.getString(R.string.total_people), (adults.toInt() + children.toInt()))
        }
    }

    private fun startEndDateRang(){
        val selectLocationBookingFragment = SelectStartEndDateRangBottomsheet.newInstance(startDate, endDate)
        selectLocationBookingFragment.initListener(onCallBackDateListener)
        selectLocationBookingFragment.show(supportFragmentManager, "fragment")
    }

    private val onCallBackDateListener = object : SelectStartEndDateRangBottomsheet.OnClickCallBackListener{
        override fun onClickSelect(getStartDate: String, getEndDate : String) {
            startDate = getStartDate
            endDate = getEndDate
            startDateTv.text = startDate
            endDateTv.text = endDate

            val hashMap = RequestHashMapData.requestDataCheckAvailableItem(category, hotelId, startDate, endDate)
            progressBar.visibility = View.VISIBLE
            RequestHashMapData.checkAvailableItem(this@HotelSelectExtraPriceActivity, hashMap, progressBar, object : BookingHotelWS.OnCallBackCheckAvailableListener{
                override fun onSuccess(price: Double, message: Boolean) {
                    isCanBook = message
                    if (message) {
                        priceCanBookDate = price
                        checkValidateButtonPay()

                        setPriceByDate()
                    } else {
                        Utils.customToastMsgError(this@HotelSelectExtraPriceActivity, "You cannot book for date !", false)
                    }
                }

                override fun onFailed(message: String) {}

            })
        }
    }

    private fun bookingRoomData() : HashMap<String, Any>{
        val hashMap : HashMap<String, Any> = HashMap()
        hashMap["hotel_id"] = hotelId
        hashMap["start_date"] = startDate
        hashMap["end_date"] = endDate
        hashMap["total"] = totalPrice.toString()
        if (category == "space") {  // For Space
            hashMap["total_guests"] = (adults.toInt() + children.toInt())
        } else {
            hashMap["total_guests"] = totalAllTicket.toString()
        }
        hashMap["hotel_user_id"] = Utils.validateNullValue(MockUpData.getEazyHotelUserId(UserSessionManagement(this)))
        hashMap["category"] = category
        return hashMap
    }

    private fun setPriceByDate(){
        val day = DateUtil.calculateDuring(startDate, endDate, SimpleDateFormat("yyyy-MM-dd")).toInt() + 1 // StartDate / EndDate format "yyyy-MM-dd"
        dateStartEndTv.text = String.format("%s %s%s %s %s %s %s", startDate, "\n", endDate, "(", day, resources.getString(R.string.day).lowercase(), ")")
        priceByDate.text = String.format("%s %s", "$", priceCanBookDate)
    }

}