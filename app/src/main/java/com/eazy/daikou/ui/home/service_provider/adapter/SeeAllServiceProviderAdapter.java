package com.eazy.daikou.ui.home.service_provider.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.eazy.daikou.R;
import com.eazy.daikou.model.my_property.service_provider.ServiceProvider;

import java.util.List;

public class SeeAllServiceProviderAdapter extends RecyclerView.Adapter<SeeAllServiceProviderAdapter.ItemViewHolder> {

    private final Context context;
    private final List<ServiceProvider> serviceProvidersList;
    private final ItemClickService itemClickService;

    public SeeAllServiceProviderAdapter(Context context, List<ServiceProvider> serviceProvidersList, ItemClickService itemClickService) {
        this.context = context;
        this.serviceProvidersList = serviceProvidersList;
        this.itemClickService = itemClickService;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view  = LayoutInflater.from(context).inflate(R.layout.services_providers_layout,parent,false);

        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        ServiceProvider serviceProvider = serviceProvidersList.get(position);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        holder.layoutMain.setLayoutParams(layoutParams);
        if (position % 2 != 0) {
            holder.layoutMain.setPadding(13, 10, 0, 10);
        } else {
            holder.layoutMain.setPadding(7, 10, 0, 10);
        }

        if(serviceProvider!=null){
            if(serviceProvider.getCompanyContact() != null){
                holder.phoneNumber.setText(serviceProvider.getCompanyContact());
            }else {
                holder.phoneNumber.setText(context.getResources().getString(R.string.no_contact));
                holder.phoneNumber.setTextColor(context.getResources().getColor(R.color.red));
            }

            holder.organization.setText(serviceProvider.getContractorName());
            if(serviceProvider.getCompanyLogo() != null){
                Glide.with(holder.itemView.getContext()).load(serviceProvider.getCompanyLogo()).into(holder.imageView);
            }else {
                Glide.with(holder.itemView.getContext()).load(R.drawable.ic_person_profile_false_24).into(holder.imageView);
            }
            holder.linearItem.setOnClickListener(view -> itemClickService.itemClick(serviceProvider));

            holder.cardViewSeeAll.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return serviceProvidersList.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder{
        private final ImageView imageView;
        private final TextView organization, phoneNumber;
        private final LinearLayout linearItem;
        private final CardView cardViewSeeAll;
        private final LinearLayout layoutMain;
        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            layoutMain = itemView.findViewById(R.id.layoutMain);
            imageView = itemView.findViewById(R.id.image);
            organization = itemView.findViewById(R.id.servicename);
            phoneNumber = itemView.findViewById(R.id.custmer_phone_number);
            linearItem = itemView.findViewById(R.id.linear_item);
            cardViewSeeAll = itemView.findViewById(R.id.cardViewSeeAll);
        }
    }

    public interface ItemClickService{
        void itemClick(ServiceProvider serviceProvider);
    }

    public void clear() {
        int size = serviceProvidersList.size ();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                serviceProvidersList.remove ( 0 );
            }
            notifyItemRangeRemoved ( 0, size );
        }
    }

}
