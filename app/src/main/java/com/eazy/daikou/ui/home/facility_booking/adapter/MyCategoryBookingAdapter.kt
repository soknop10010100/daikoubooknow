package com.eazy.daikou.ui.home.facility_booking.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.helper.DateUtil
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.facility_booking.my_booking.MyBookingItemsModel
import com.eazy.daikou.model.facility_booking.my_booking.MyCategoryBookingModel
import de.hdodenhof.circleimageview.CircleImageView
import org.w3c.dom.Text

class MyCategoryBookingAdapter (private val context : Context, private val listItem : ArrayList<MyCategoryBookingModel>, private val callBackListener : ClickCallBackListener) : RecyclerView.Adapter<MyCategoryBookingAdapter.ItemViewHolder>() {

    private lateinit var myBookingAdapter : MyBookingDetailAdapter
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        return ItemViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.category_item_my_booking, parent, false))
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val itemCardBooking = listItem[position]
        if (itemCardBooking != null){
            holder.dateTv.text = if (itemCardBooking.date_time !=  null)    DateUtil.formatDateComplaint(itemCardBooking.date_time) else "- - -"

            val linearLayoutManager = LinearLayoutManager(context)
            holder.categoryRecycler.layoutManager = linearLayoutManager
            holder.categoryRecycler.isNestedScrollingEnabled = false
            myBookingAdapter = MyBookingDetailAdapter(itemCardBooking.booking_items, object : MyBookingDetailAdapter.CallBackListener{
                override fun onClickCallBack(myBookingDetail: MyBookingItemsModel) {
                    callBackListener.onClickCallBack(itemCardBooking, myBookingDetail)
                }

            })
            holder.categoryRecycler.adapter = myBookingAdapter

            holder.payNowBookingTv.setOnClickListener {callBackListener.onClickCallBack(itemCardBooking, "pay_now")}
            holder.cancelBookingTv.setOnClickListener {callBackListener.onClickCallBack(itemCardBooking, "cancel_booking")}
        }
    }

    override fun getItemCount(): Int {
        return listItem.size
    }

    class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val categoryRecycler: RecyclerView = itemView.findViewById(R.id.categoryRecycler)
        val dateTv: TextView = itemView.findViewById(R.id.dateTv)
        val payNowBookingTv: TextView = itemView.findViewById(R.id.btnAccept)
        val cancelBookingTv: TextView = itemView.findViewById(R.id.btnCancel)
    }


    interface ClickCallBackListener{
        fun onClickCallBack(itemMainCategory: MyCategoryBookingModel, myBookingItemsModel: MyBookingItemsModel)
        fun onClickCallBack(itemMainCategory: MyCategoryBookingModel, doAction: String)
    }

    fun clear(){
        val size = listItem.size
        if (size > 0) {
            for (i in 0 until size) {
                listItem.removeAt(0)
            }
            notifyItemRangeRemoved(0, size)
        }
    }
}