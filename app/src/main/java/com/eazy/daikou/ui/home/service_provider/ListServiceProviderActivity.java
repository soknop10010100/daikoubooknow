package com.eazy.daikou.ui.home.service_provider;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.eazy.daikou.R;
import com.eazy.daikou.base.BaseActivity;
import com.eazy.daikou.request_data.request.service_provider_ws.ServiceProviderWs;
import com.eazy.daikou.model.my_property.Child;
import com.eazy.daikou.model.my_property.service_provider.LatLngServiceProvider;
import com.eazy.daikou.model.my_property.service_provider.MenuServiceProvider;
import com.eazy.daikou.ui.home.service_provider.adapter.MyItemServiceProviderAdapter;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ListServiceProviderActivity extends BaseActivity implements OnMapReadyCallback,ServiceProviderBottomSheetFragment.OnClickAllAction {

    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private RelativeLayout layoutMaps;
    private SupportMapFragment mapFragment;
    private GoogleMap mMap;
    private FloatingActionMenu floatingActionMenu;
    private FloatingActionButton fabMapsType;
    private FloatingActionButton fabInfo;
    private FloatingActionButton fabMyLocation;
    private MyItemServiceProviderAdapter providerAdapter;

    private final HashMap<String, String> hashMapLatLng = new HashMap<>();
    private List<MenuServiceProvider> listServiceProvider;
    private final List<LatLng> listLateLong  = new ArrayList<>();
    private List<LatLngServiceProvider> latLngServiceProvider;

    private String catServicePro = "all", getChileName = "";
    private final int REQUEST_SEARCH =980;
    private boolean getFirstClick = false;
    private boolean getFirst = true;
    private boolean isClickMap = true;
    private boolean isSizeZero = false;
    private boolean isZoomFromSearch = false;
    private int page  = 1;

    @SuppressLint("NotifyDataSetChanged")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_list_service_provider_list);

        findViewById(R.id.backButton).setOnClickListener(view -> finish());
        progressBar = findViewById(R.id.progressItem);
        latLngServiceProvider = new ArrayList<>();

        TextView txtMapSearchTv = findViewById(R.id.txtMap);
        txtMapSearchTv.setOnClickListener(view -> {
            Intent intent = new Intent(ListServiceProviderActivity.this,SearchServiceProviderActivity.class);
            startActivityForResult(intent,REQUEST_SEARCH);
        });

        //init Maps
        layoutMaps = findViewById(R.id.layoutMaps);
        floatingActionMenu = findViewById(R.id.fab_main_menu);
        fabMapsType = findViewById(R.id.fab_maps);
        fabInfo = findViewById(R.id.fab_info_property);
        fabMyLocation = findViewById(R.id.fab_myLocation_pro);
        onClickIconMap();
        initMapsLayout();

        listServiceProvider = new ArrayList<>();

        recyclerView = findViewById(R.id.list_category);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));

        getTitleCategoryProvider();
        getLatLng(catServicePro,false);

    }

    //Get Category
    private void getTitleCategoryProvider(){
        progressBar.setVisibility(View.GONE);
        new ServiceProviderWs().getServiceProviderTitle(this, requestServiceProvider);
    }


    private final ServiceProviderWs.RequestServiceProvider requestServiceProvider = new ServiceProviderWs.RequestServiceProvider() {
        @RequiresApi(api = Build.VERSION_CODES.M)
        @SuppressLint("NotifyDataSetChanged")
        @Override
        public void onLoadSuccessFullCategory(List<MenuServiceProvider> menuServiceProvider) {
            progressBar.setVisibility(View.GONE);

            MenuServiceProvider serviceProvider = new MenuServiceProvider("0",getString(R.string.all),null,true);
            listServiceProvider.add(serviceProvider);
            listServiceProvider.addAll(menuServiceProvider);
            if (listServiceProvider.size() > 0) {
                listServiceProvider.get(0).setClick(true);
            }

            providerAdapter = new MyItemServiceProviderAdapter(listServiceProvider,ListServiceProviderActivity.this, onClickCategory);
            recyclerView.setAdapter(providerAdapter);
            getLatLng("all",false);
            providerAdapter.notifyDataSetChanged();
        }

        @Override
        public void OnLoadFail(String message) {
            progressBar.setVisibility(View.GONE);
            Toast.makeText(ListServiceProviderActivity.this, message, Toast.LENGTH_SHORT).show();
        }
    };

    //Adapter
    @SuppressLint("NotifyDataSetChanged")
    private final MyItemServiceProviderAdapter.ItemClickCategoryServiceProvider onClickCategory = (provider, pos) -> {
        isSizeZero = false;     page = 1;       isZoomFromSearch = false;
        if(!provider.getName().equals(getString(R.string.all))){
            ServiceProviderBottomSheetFragment bttSheet = new ServiceProviderBottomSheetFragment().newInstance(provider.getName(), getChileName, provider.getChild());
            bttSheet.show(getSupportFragmentManager(),"fragment");
        }else {
            catServicePro = "all";      getChileName = "";
            getLatLng(catServicePro,false);
            for (MenuServiceProvider menuServiceProvider : listServiceProvider){
                menuServiceProvider.setClick(menuServiceProvider.getName().equalsIgnoreCase(provider.getName()));
            }
            providerAdapter.notifyDataSetChanged();
        }
    };

    @SuppressLint("NotifyDataSetChanged")
    @Override
    public void onClickSelect(@NonNull String category, Child listChild) {
        catServicePro = listChild.getId();
        getChileName = listChild.getName();
        page = 1;   isSizeZero = false;
        getLatLng(catServicePro,false);
        for (MenuServiceProvider menuServiceProvider : listServiceProvider){
            menuServiceProvider.setClick(menuServiceProvider.getName().equalsIgnoreCase(category));
        }
        providerAdapter.notifyDataSetChanged();
    }


    private void getLatLng(String id,boolean isZoom){
        progressBar.setVisibility(View.GONE);
        new ServiceProviderWs().getLatLngServiceProviderV2(ListServiceProviderActivity.this, page, 100, id, new ServiceProviderWs.RequestLatLngServiceProvider() {
            @Override
            public void onLoadServiceProvider(List<LatLngServiceProvider> serviceProvider) {
                if (serviceProvider.size() <= 0)  isSizeZero = true;
                progressBar.setVisibility(View.GONE);
                if(latLngServiceProvider.size()>0 && !isZoom) latLngServiceProvider.clear();
                latLngServiceProvider.addAll(serviceProvider);
                addMarker(listLateLong,null,latLngServiceProvider, id);
            }

            @Override
            public void OnLoadFail(String message) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(ListServiceProviderActivity.this,message,Toast.LENGTH_LONG).show();
            }
        });

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_SEARCH && resultCode == RESULT_OK){
            assert data != null;
            if(data.hasExtra("id")){
                if(latLngServiceProvider.size() > 0)  latLngServiceProvider.clear();
                catServicePro = data.getStringExtra("id");
                getChileName = data.getStringExtra("name");
                LatLngServiceProvider serviceProvider = (LatLngServiceProvider) data.getSerializableExtra("service");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    isSizeZero = false;     page = 1;   isZoomFromSearch = true;
                    addMarker(listLateLong, serviceProvider,null, catServicePro);
                }

                if (serviceProvider != null){
                    for (int i = 0; i < listServiceProvider.size(); i++){
                        MenuServiceProvider menuServiceProvider = listServiceProvider.get(i);
                        if (menuServiceProvider.getId().equalsIgnoreCase(serviceProvider.getMain_category_id())){
                            recyclerView.scrollToPosition(i);
                            menuServiceProvider.setClick(true);
                        } else {
                            menuServiceProvider.setClick(false);
                        }
                    }
                    providerAdapter.notifyDataSetChanged();
                }
            }
        }

    }

    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(@NonNull GoogleMap googleMap) {
        mMap = googleMap;
        if (getFirstClick){
            mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        } else {
            mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        }
        zoomMap(BaseActivity.latitude, BaseActivity.longitude);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
        }
        mMap.setMyLocationEnabled(true);

        //map is touched..do something
        googleMap.setOnCameraMoveStartedListener(reason -> {
            if (reason == GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE) {
                if (!isSizeZero && !isZoomFromSearch) {
                    if (progressBar.getVisibility() == View.GONE) {
                        page += 1;
                        getLatLng(catServicePro, true);
                    }
                }
            }
        });
    }

    private void zoomMap(double lat , double lng) {
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), 15f));
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void addMarker(List<LatLng> latLngList,LatLngServiceProvider serviceProvider, List<LatLngServiceProvider> serviceModelList, String categoryId){
        if (latLngList.size() > 0 ) latLngList.clear();
        if (mMap != null)     mMap.clear();

        @SuppressLint("InflateParams") View view = ((LayoutInflater)this.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_location_map_circle, null);
        TextView businessName = view.findViewById(R.id.unitNoTv);
        if(serviceModelList != null){
            for (LatLngServiceProvider propertyModel : serviceModelList){
               drawPLaceOnMap(propertyModel,businessName);
            }
        }else if(serviceProvider != null){
            drawPLaceOnMap(serviceProvider,businessName);
        }

        mMap.setOnMarkerClickListener(marker -> {
            marker.showInfoWindow();
            linkToWebView(hashMapLatLng.get(marker.getTitle()));
            return true;
        });

    }

    private void  drawPLaceOnMap(LatLngServiceProvider serviceProvider, TextView businessName){
        if(serviceProvider.getCoordLat() != null && serviceProvider.getCoordLong() != null){
            try {
                hashMapLatLng.put(serviceProvider.getContractorName(), serviceProvider.getId());
                LatLng latLng = new LatLng(Double.parseDouble(serviceProvider.getCoordLat()), Double.parseDouble(serviceProvider.getCoordLong()));
                MarkerOptions markerOptions = new MarkerOptions().position(latLng).title(serviceProvider.getContractorName());
                mMap.addMarker(markerOptions);
                businessName.setText(serviceProvider.getContractorName());
            } catch (Exception exception){
                exception.printStackTrace();
            }
        }

    }

    private void linkToWebView(String markerId){
        LatLngServiceProvider serviceProvider = new LatLngServiceProvider();
        ServiceProviderBottomSheetFragment bttSheet = new ServiceProviderBottomSheetFragment().newInstance(markerId, serviceProvider);
        bttSheet.show(getSupportFragmentManager(),"fragment");
    }

    private void onClickIconMap() {
            if (isClickMap){
                layoutMaps.setVisibility(View.VISIBLE);
                isClickMap = false;
                floatingActionMenu.setVisibility(View.VISIBLE);

            } else {
                isClickMap = true;
                layoutMaps.setVisibility(View.GONE);
                floatingActionMenu.setVisibility(View.GONE);

            }

    }

    private void initMapsLayout(){
        if (getFirst){
            getFirst = false;
            mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map_google);
            assert mapFragment != null;
            mapFragment.getMapAsync(this);
        }

        //Click on Fab Menus
        floatingActionMenu.setOnClickListener(view1 ->  floatingActionMenu.close(true));

        fabMapsType.setOnClickListener(view ->{
            getFirstClick = !getFirstClick;
            mapFragment.getMapAsync(this);
            floatingActionMenu.close(true);

        });

        fabInfo.setOnClickListener(view1 -> {
            floatingActionMenu.close(true);
            if (!isSizeZero) {
                page += 1;
                getLatLng(catServicePro, true);
            }

        });

        fabMyLocation.setVisibility(View.GONE);
    }
}