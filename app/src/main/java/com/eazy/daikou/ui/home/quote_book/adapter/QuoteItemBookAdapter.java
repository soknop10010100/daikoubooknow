package com.eazy.daikou.ui.home.quote_book.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.eazy.daikou.R;
import com.eazy.daikou.helper.Utils;
import com.eazy.daikou.helper.web.WebsiteActivity;
import com.eazy.daikou.model.book.BookAllV2;
import java.util.List;

public class QuoteItemBookAdapter extends RecyclerView.Adapter<QuoteItemBookAdapter.ViewHolder> {

    private final Context context;
    private final List<BookAllV2> bookModelList;

    public QuoteItemBookAdapter(Context context, List<BookAllV2> bookModelList) {
        this.context = context;
        this.bookModelList = bookModelList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_item_book,parent,false);
        return new ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        BookAllV2 bookModel = bookModelList.get(position);
        if (bookModel != null){

            if (bookModel.getTitle() != null){
                holder.tvNameBook.setText(bookModel.getTitle());
            }else {
                holder.tvNameBook.setText("___");
            }
            if (bookModel.getFilePath() != null){
                if (bookModel.getImage() != null) {
                    Glide.with(context).load(bookModel.getImage()).into(holder.imageViewBook);
                } else {
                    Glide.with(context).load(R.drawable.no_image).into(holder.imageViewBook);
                }
            } else if (bookModel.getImage() != null) {
                if (bookModel.getImage().contains(".jpg") || bookModel.getImage().contains(".jpeg") || bookModel.getImage().contains(".png")) {
                    Glide.with(context).load(bookModel.getImage()).into(holder.imageViewBook);
                } else {
                    Glide.with(context).load(R.drawable.no_image).into(holder.imageViewBook);
                }
            } else {
                Glide.with(context).load(R.drawable.no_image).into(holder.imageViewBook);
            }

            holder.itemView.setOnClickListener(view -> {
                if (bookModel.getFilePath() != null && bookModel.getFilePath().contains(".pdf")){
                    Utils.openDefaultPdfView(context, bookModel.getFilePath());
                } else if (bookModel.getImage() != null){
                    if (bookModel.getImage().contains(".jpg") || bookModel.getImage().contains(".jpeg") || bookModel.getImage().contains(".png")) {
                        Intent intent = new Intent(context, WebsiteActivity.class);
                        intent.putExtra("linkUrlNews", bookModel.getImage());
                        context.startActivity(intent);
                    } else {
                        Toast.makeText(context,context.getResources().getText(R.string.not_available_website) , Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(context,context.getResources().getText(R.string.not_available_website) , Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
    @Override
    public int getItemCount() {
        return bookModelList.size();
    }

    public static class  ViewHolder extends RecyclerView.ViewHolder{

        private final ImageView imageViewBook;
        private final TextView tvNameBook;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            imageViewBook = itemView.findViewById(R.id.ImageView_book);
            tvNameBook = itemView.findViewById(R.id.tvNameBook);
        }
    }
}
