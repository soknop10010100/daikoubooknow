package com.eazy.daikou.ui.home.service_provider_new.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.model.my_property.service_provider.DetailServiceProviderModel
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback

class MoreInfoCompanyActivity : BaseActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    private  var detailModel: DetailServiceProviderModel = DetailServiceProviderModel()
    private var latNumber: Double = 0.0
    private var longNumber: Double = 0.0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_more_info_company)

        initView()

        initAction()
    }

    private fun initView(){

    }

    private fun initAction(){

    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        mMap.isMyLocationEnabled = true
        mMap.uiSettings.isZoomControlsEnabled = false
        mMap.uiSettings.isMyLocationButtonEnabled = true
        mMap.uiSettings.isCompassEnabled = false //hide compass
    }
}