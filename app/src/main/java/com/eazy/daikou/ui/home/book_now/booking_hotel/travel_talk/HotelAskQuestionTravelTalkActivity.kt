package com.eazy.daikou.ui.home.book_now.booking_hotel.travel_talk

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.base.BaseCameraActivity
import com.eazy.daikou.base.MockUpData
import com.eazy.daikou.request_data.request.book_now_ws.BookingHotelWS
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.booking_hotel.Category
import com.eazy.daikou.model.booking_hotel.HotelTravelTalkModel
import com.eazy.daikou.model.booking_hotel.TravelTalkReplyDetail
import com.eazy.daikou.model.work_order.ImageListModel
import com.eazy.daikou.ui.home.inspection_work_order.adapter.ImageListWorkOrderAdapter
import de.hdodenhof.circleimageview.CircleImageView


class HotelAskQuestionTravelTalkActivity : BaseActivity() {

    private lateinit var progressBar: ProgressBar
    private lateinit var btnAddImg: RelativeLayout
    private lateinit var imageRecyclerView: RecyclerView
    private var post = 0
    private val bitmapList: ArrayList<ImageListModel> = ArrayList()
    private lateinit var nameTv : TextView
    private lateinit var profileImage : CircleImageView
    private lateinit var titleEd : EditText
    private lateinit var descriptionTv : EditText
    private val hashMapCategory = HashMap<String, Any>()
    private var categoryId = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hotel_ask_question_travel_talk)

        initView()

        initAction()

        Utils.customOnToolbar(this, resources.getString(R.string.ask_question)){finish()}

    }

    private fun initView(){
        progressBar = findViewById(R.id.progressItem)
        progressBar.visibility = View.GONE
        btnAddImg = findViewById(R.id.btnAddImg)
        imageRecyclerView = findViewById(R.id.list_image)
        profileImage = findViewById(R.id.profile_image)
        nameTv = findViewById(R.id.nameTv)
        titleEd = findViewById(R.id.titleEd)
        descriptionTv = findViewById(R.id.descriptionTv)

        val user = MockUpData.getUserItem(UserSessionManagement(this))
        when {
            user.name != null -> {
                nameTv.text = user.name
            }
            user!!.lastName != null &&   user.firstName != null -> {
                nameTv.text = user.lastName + " " + user.firstName
            }
            else ->{
                nameTv.text = "- - -"
            }
        }

        Glide.with(profileImage).load(if (user.photo != null) user.photo else R.drawable.ic_my_profile).into(profileImage)
    }

    private fun initAction(){
        // Select Category
        val courses : ArrayList<String> = ArrayList()
        BookingHotelWS().getCategoryPostWs(this, object : BookingHotelWS.OnCallBackCategoryListener{
            override fun onSuccessCategoryPostList(travelTalkModelList: ArrayList<Category>) {
                progressBar.visibility = View.GONE
                for (item in travelTalkModelList){
                    courses.add(item.name!!)
                    hashMapCategory[item.name!!] = item.id!!
                }

                initCategory(courses)
            }

            override fun onFailed(message: String) {
                progressBar.visibility = View.GONE
                Utils.customToastMsgError(this@HotelAskQuestionTravelTalkActivity, message, false)
            }

        })


        // Add Image
        btnAddImg.setOnClickListener(CustomSetOnClickViewListener{
            resultLauncher.launch(Intent(this@HotelAskQuestionTravelTalkActivity, BaseCameraActivity::class.java))
        })

        // Create Post
        findViewById<CardView>(R.id.submitBtn).setOnClickListener(CustomSetOnClickViewListener{
            progressBar.visibility = View.VISIBLE
            BookingHotelWS().createPostTravelTalkWs(this,"do_post", requestData(), object : BookingHotelWS.OnCallBackTravelTalkListener{
                override fun onSuccessTravelTalkList(travelTalkModelList: ArrayList<HotelTravelTalkModel>) {}
                override fun onSuccessTravelTalkReplyDetail(travelTalkModelList: TravelTalkReplyDetail) {}

                override fun onCreatePostSuccess(message: String) {
                    progressBar.visibility = View.GONE
                    Utils.customToastMsgError(this@HotelAskQuestionTravelTalkActivity, message, true)

                    setResult(RESULT_OK)
                    finish()
                }

                override fun onFailed(message: String) {
                    progressBar.visibility = View.GONE
                    Utils.customToastMsgError(this@HotelAskQuestionTravelTalkActivity, message, false)
                }

            })
        })
    }

    private fun initCategory(courses : ArrayList<String>){
        val spinner: Spinner = findViewById<View>(R.id.planets_spinner) as Spinner
        val adapter: ArrayAdapter<*> = ArrayAdapter(this, android.R.layout.simple_spinner_item, courses.toArray())
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner.adapter = adapter

        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>?, view: View, position: Int, l: Long) {
                categoryId = courses[position]
            }

            override fun onNothingSelected(adapterView: AdapterView<*>?) {
                return
            }
        }
    }

    private fun requestData() : HashMap<String, Any>{
        val hashMap = HashMap<String, Any>()
        hashMap["user_id"] = MockUpData.getEazyHotelUserId(UserSessionManagement(this))
        hashMap["category_id"] = hashMapCategory[categoryId].toString()
        hashMap["title"] = titleEd.text.toString().trim()
        hashMap["description"] = descriptionTv.text.toString().trim()
        val listImage: MutableList<String> = java.util.ArrayList()
        if (bitmapList.isNotEmpty()) {
            for (bitmap in bitmapList) {
                listImage.add(Utils.convert(bitmap.bitmapImage))
            }
            hashMap["images"] = listImage
        }

        Utils.logDebug("getvaludkeekekheeej", Utils.getStringGson(hashMap))
        return hashMap
    }

    private val resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
        if (result.resultCode == Activity.RESULT_OK) {
            if (result.data != null){
                val data = result.data
                if (data!!.hasExtra("image_path")) {
                    val imagePath = data.getStringExtra("image_path")
                    if (imagePath != null) {
                        addUrlImage("", imagePath)
                    }
                    setImageRecyclerView(post)
                }
                if (data.hasExtra("select_image")) {
                    val imagePath = data.getStringExtra("select_image")
                    if (imagePath != null) {
                        addUrlImage("", imagePath)
                    }
                    setImageRecyclerView(post)
                }
            }
        }
    }

    private fun addUrlImage(posKeyDefine: String, imagePath: String) {
        val loadedBitmap = BaseCameraActivity.convertByteToBitmap(imagePath)
        post += 1
        val imageListModel = ImageListModel()
        imageListModel.keyImage = posKeyDefine
        imageListModel.bitmapImage = loadedBitmap
        bitmapList.add(imageListModel)
    }

    private fun setImageRecyclerView(post: Int) {
        imageRecyclerView.layoutManager = GridLayoutManager(this, 1, RecyclerView.HORIZONTAL, false)
        val pickUpAdapter = ImageListWorkOrderAdapter(this, post, bitmapList, onClearImage)
        imageRecyclerView.adapter = pickUpAdapter
        btnAddImg.visibility = visibleView(bitmapList)

        findViewById<TextView>(R.id.noImage).visibility = if (bitmapList.size > 0) View.GONE else View.VISIBLE
    }

    private fun visibleView(list: List<*>): Int {
        return if (list.size >= 5) {
            View.GONE
        } else {
            View.VISIBLE
        }
    }

    private val onClearImage: ImageListWorkOrderAdapter.OnClearImage = object : ImageListWorkOrderAdapter.OnClearImage {
            override fun onClickRemove(bitmap: ImageListModel, post: Int) {
                removeImage(post, bitmap)
            }

            override fun onViewImage(bitmap: ImageListModel) {
                if (bitmap.bitmapImage != null) {
                    Utils.openImageOnDialog(this@HotelAskQuestionTravelTalkActivity, Utils.convert(bitmap.bitmapImage))
                }
            }
        }

    private fun removeImage(position: Int, bitmap: ImageListModel) {
        bitmapList.remove(bitmap)
        setImageRecyclerView(position)
    }
}