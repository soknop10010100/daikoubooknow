package com.eazy.daikou.ui.home.my_property.payment.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.profile.PropertyItemModel

class PropertyItemAdapter(private val selectedItem: String, private val list : List<PropertyItemModel>, private val onClickListener : OnClickCallBackLister) : RecyclerView.Adapter<PropertyItemAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.custom_view_text_view_layout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item : PropertyItemModel = list[position]
        if (item != null){
            Utils.setValueOnText(holder.itemNameTv, item.name)
            holder.checkClick.setColorFilter(Utils.getColor(holder.checkClick.context, R.color.appBarColorOld))
            holder.checkClick.visibility = if(selectedItem == item.id) View.VISIBLE else View.INVISIBLE
            holder.line.visibility = if((list.size - 1) == position) View.GONE else View.VISIBLE
            holder.itemView.setOnClickListener { onClickListener.onClickCallBack(item) }
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val itemNameTv : TextView = itemView.findViewById(R.id.item_name)
        val checkClick : ImageView = itemView.findViewById(R.id.checkClick)
        val line : View = itemView.findViewById(R.id.line)
        val mainLayout : LinearLayout = itemView.findViewById(R.id.mainLayout)
    }

    interface OnClickCallBackLister{
        fun onClickCallBack(value : PropertyItemModel)
    }
}