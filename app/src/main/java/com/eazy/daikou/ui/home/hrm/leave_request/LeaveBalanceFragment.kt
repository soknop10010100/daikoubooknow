package com.eazy.daikou.ui.home.hrm.leave_request

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.request_data.request.hr_ws.LeaveManagementWs
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.hr.LeaveBalance
import com.eazy.daikou.model.hr.LeaveManagement
import com.eazy.daikou.ui.home.hrm.leave_request.adapter.TypeLeaveCategoryAdapter
import com.google.android.material.bottomsheet.BottomSheetDialogFragment


class LeaveBalanceFragment : BottomSheetDialogFragment() {

    private lateinit var progressBar: ProgressBar
    private lateinit var recyclerView: RecyclerView
    private lateinit var leaveBalanceCategoryAdapter : TypeLeaveCategoryAdapter
    private var listLeaveBalance = ArrayList<LeaveBalance>()
    private var mContext : Context? = null
    private var userkey = ""

    fun newInstance(userBussinessKey : String): LeaveBalanceFragment {
        val fragment = LeaveBalanceFragment()
        val args = Bundle()
        args.putString("user_bussiness_key",userBussinessKey)
        fragment.arguments = args

        return fragment

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view =inflater.inflate(R.layout.fragment_leave_balance, container, false)

        initView(view)

        initData()

        initAction()

        return view
    }

    private fun initView(view : View){
        recyclerView = view.findViewById(R.id.listLeaveBalanceRV)
        progressBar = view.findViewById(R.id.progressItem)
    }

     fun initData(){
         if (arguments != null) {
             userkey = requireArguments().getString("user_bussiness_key").toString()
         }
    }
    private fun initAction(){
        getDataAPi()
    }

    private fun getDataAPi(){
        recyclerView.layoutManager = GridLayoutManager(mContext, 2)
        leaveBalanceCategoryAdapter = TypeLeaveCategoryAdapter(listLeaveBalance, mContext!!)

        LeaveManagementWs().getLeaveCategoryManagement(mContext as Activity, userkey, leaveCallBack)
    }

    private val leaveCallBack = object : LeaveManagementWs.OnCallBackLeaveManagementListener{

        @SuppressLint("NotifyDataSetChanged")
        override fun onLoadListSuccessFull(listLeaveManagement: ArrayList<LeaveManagement>) {
            progressBar.visibility = View.GONE
        }

        @SuppressLint("NotifyDataSetChanged")
        override fun onLoadListCategorySuccessFull(leaveBalance: ArrayList<LeaveBalance>) {
            progressBar.visibility = View.GONE

            listLeaveBalance.addAll(leaveBalance)
            leaveBalanceCategoryAdapter = TypeLeaveCategoryAdapter(listLeaveBalance, mContext!!)
            recyclerView.adapter = leaveBalanceCategoryAdapter
            leaveBalanceCategoryAdapter.notifyDataSetChanged()

        }

        override fun onLoadFail(message: String) {
            progressBar.visibility = View.GONE
            Utils.customToastMsgError(mContext, message, false)
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
    }

}