package com.eazy.daikou.ui.home.utility_tracking.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.eazy.daikou.R;
import com.eazy.daikou.model.floor_plan.FloorSpinnerModel;

import java.util.List;

public class SpinnerFloorAdapter extends RecyclerView.Adapter<SpinnerFloorAdapter.ViewHolder>{
    private final List<FloorSpinnerModel> floorSpinnerModels;
    private final ClickCallBack clickCallBack;

    public SpinnerFloorAdapter(List<FloorSpinnerModel> floorSpinnerModels, ClickCallBack clickCallBack) {
        this.floorSpinnerModels = floorSpinnerModels;
        this.clickCallBack = clickCallBack;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.spinner_floor_model, null));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        FloorSpinnerModel floorSpinnerModel = floorSpinnerModels.get(position);
        if (floorSpinnerModel != null){
            holder.floorTv.setText(floorSpinnerModel.getFloorName());
            holder.numberRecord.setText(floorSpinnerModel.getNumberRecord());
            holder.numberRecordFloorLayout.setVisibility(floorSpinnerModel.getNumberRecord().equals("0") || floorSpinnerModel.getNumberRecord().equals("") ? View.INVISIBLE : View.VISIBLE);
            if (floorSpinnerModel.isClick()){
                holder.checkClick.setVisibility(View.VISIBLE);
            } else {
                holder.checkClick.setVisibility(View.INVISIBLE);
            }

            holder.itemView.setOnClickListener(view -> {
                clickCallBack.clickCallBack(floorSpinnerModel);
            });
        }
    }

    @Override
    public int getItemCount() {
        return floorSpinnerModels.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        private final TextView numberRecord, floorTv;
        private final ImageView checkClick;
        private final LinearLayout numberRecordFloorLayout;
        public ViewHolder(@NonNull View view) {
            super(view);
            numberRecordFloorLayout = view.findViewById(R.id.numberRecordFloorLayout);
            checkClick = view.findViewById(R.id.checkClick);
            numberRecord = view.findViewById(R.id.numberRecordFloor);
            floorTv = view.findViewById(R.id.floorTv);

        }
    }

    public interface ClickCallBack{
        void clickCallBack(FloorSpinnerModel floorSpinnerModel);
    }
}