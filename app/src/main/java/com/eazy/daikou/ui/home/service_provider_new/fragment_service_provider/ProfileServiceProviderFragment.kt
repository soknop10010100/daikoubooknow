package com.eazy.daikou.ui.home.service_provider_new.fragment_service_provider

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseFragment
import com.eazy.daikou.ui.home.service_provider_new.activity.ServiceProviderListHistoryActivity

class ProfileServiceProviderFragment : BaseFragment() {

    private lateinit var layoutBooingHistory : LinearLayout
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_profile_service_provider, container, false)

        initView(view)

        initData()

        initAction()

        return view
    }

    private fun initView(view : View){

        layoutBooingHistory = view.findViewById(R.id.layoutBooingHistory)
    }
    private fun initData(){

    }
    private fun initAction(){

        layoutBooingHistory.setOnClickListener { startActivity(Intent(mActivity, ServiceProviderListHistoryActivity::class.java))}
    }

}