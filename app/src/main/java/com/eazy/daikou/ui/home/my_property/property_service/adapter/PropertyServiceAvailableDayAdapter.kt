package com.eazy.daikou.ui.home.my_property.property_service.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.databinding.ServiceTypeItemPropertyBinding
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.facility_booking.FeeTypeModel
import java.util.*

class PropertyServiceAvailableDayAdapter(private val context : Context, private val isShowCheck : Boolean, private val freeTypeList : List<FeeTypeModel>, private val clickCallBack : ClickCallBackListener) :
    RecyclerView.Adapter<PropertyServiceAvailableDayAdapter.ItemViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val view = ServiceTypeItemPropertyBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.onBind(context, isShowCheck, freeTypeList[position], clickCallBack)
    }

    override fun getItemCount(): Int {
        return freeTypeList.size
    }

    interface ClickCallBackListener{
        fun onClickCallBack(feeTypeModel: FeeTypeModel)
    }

    class ItemViewHolder(private val mBind: ServiceTypeItemPropertyBinding) : RecyclerView.ViewHolder(mBind.root) {
        fun onBind(context : Context, isShowCheck : Boolean, item : FeeTypeModel, clickCallBack : ClickCallBackListener){
            Utils.setValueOnText(mBind.textCategory, item.freeTypeName)
            if (item.isClick) {
                mBind.textCategory.setTextColor(ContextCompat.getColor(context, R.color.white))
                mBind.mainLayout.background = Utils.setDrawable(context, R.drawable.bg_shap_color_appbar)
            } else {
                mBind.textCategory.setTextColor(ContextCompat.getColor(context, R.color.black))
                mBind.mainLayout.background = Utils.setDrawable(context, R.drawable.background_card_border)
            }

            if (isShowCheck) {
                mBind.iconActive.visibility = View.VISIBLE
            } else {
                mBind.iconActive.visibility = View.GONE
            }

            mBind.textCategory.setOnClickListener{ clickCallBack.onClickCallBack(item) }
        }
    }

    companion object {
        fun addListRegService() : ArrayList<FeeTypeModel> {
            val list = ArrayList<FeeTypeModel>()
            list.add(FeeTypeModel("time", "Time", true))
            list.add(FeeTypeModel("weekly", "Weekly"))
            list.add(FeeTypeModel("monthly", "Monthly"))
            return list
        }

        fun addListAvailableDay() : ArrayList<FeeTypeModel> {
            val list = ArrayList<FeeTypeModel>()
            list.add(FeeTypeModel("mon", "Monday"))
            list.add(FeeTypeModel("tue", "Tuesday"))
            list.add(FeeTypeModel("wed", "Wednesday"))
            list.add(FeeTypeModel("thu", "Thursday"))
            return list
        }
    }
}