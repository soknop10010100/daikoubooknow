package com.eazy.daikou.ui.profile;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.cardview.widget.CardView;

import com.eazy.daikou.R;
import com.eazy.daikou.base.BaseActivity;
import com.eazy.daikou.request_data.request.profile_ws.PasswordWs;
import com.eazy.daikou.helper.AppAlertCusDialog;
import com.eazy.daikou.helper.InternetConnection;
import com.eazy.daikou.helper.UserSessionManagement;
import com.eazy.daikou.helper.Utils;
import com.eazy.daikou.ui.LoginActivity;

import java.util.HashMap;

public class ChangePassWordActivity extends BaseActivity {

    private EditText etOldPass, etNewPass, etConfirmPass;
    private CardView btnSavePass;
    private ProgressBar progressBar;
    private AppAlertCusDialog alert;

    private final HashMap<String, String> hashMap = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_pass_word);

        initView();

        initAction();

    }
    private void initView() {
        etOldPass = findViewById(R.id.et_old_password);
        etNewPass = findViewById(R.id.et_new_password);
        etConfirmPass = findViewById(R.id.et_conf_new_password);
        btnSavePass = findViewById(R.id.card_save_pass);
        progressBar = findViewById(R.id.progressItem);
        progressBar.setVisibility(View.GONE);

        Utils.customOnToolbar(this, getResources().getString(R.string.change_password), this::onBackPressed);
        findViewById(R.id.layoutMap).setVisibility(View.INVISIBLE);
    }

    private void initAction() {
        Utils.setEdInputTypeMode(etOldPass, InputType.TYPE_TEXT_VARIATION_PASSWORD);
        Utils.setEdInputTypeMode(etNewPass, InputType.TYPE_TEXT_VARIATION_PASSWORD);
        Utils.setEdInputTypeMode(etConfirmPass, InputType.TYPE_TEXT_VARIATION_PASSWORD);

        etOldPass.setHint(getResources().getString(R.string.please_input_your_old_password));
        etNewPass.setHint(getResources().getString(R.string.please_input_your_new_password));
        etConfirmPass.setHint(getResources().getString(R.string.please_confirm_your_new_password));


        alert = new AppAlertCusDialog();

        btnSavePass.setOnClickListener(view -> {
            Utils.hideSoftKeyboard(btnSavePass);

            checkValidateData();
        });

    }

    private void checkValidateData() {
        if (InternetConnection.checkInternetConnectionActivity(getApplicationContext())) {
            String odlPass = etOldPass.getText().toString().trim();
            String newPass = etNewPass.getText().toString().trim();
            String confirmPass = etConfirmPass.getText().toString();
            if (TextUtils.isEmpty(odlPass) || TextUtils.isEmpty(newPass) || TextUtils.isEmpty(confirmPass)) {
                alert.showDialog(ChangePassWordActivity.this, getResources().getString(R.string.please_file_the_all_fields));
            } else if (newPass.length() < 6 || confirmPass.length() < 6) {
                alert.showDialog(ChangePassWordActivity.this, getResources().getString(R.string.please_file_the_all_fields_more_than_six_length));
            } else if (!newPass.equals(confirmPass)) {
                alert.showDialog(ChangePassWordActivity.this, getResources().getString(R.string.new_password_and_confirm_password_must_be_the_same));
            } else {
                progressBar.setVisibility(View.VISIBLE);
                hashMap.put("action", "change");
                hashMap.put("old_password", odlPass);
                hashMap.put("new_password", newPass);
                new PasswordWs().changeAndForgetPasswordWS(ChangePassWordActivity.this, hashMap, callBackListener);
            }
        } else {
            AppAlertCusDialog app = new AppAlertCusDialog();
            app.showDialog(ChangePassWordActivity.this, getString(R.string.please_check_your_internet_connection));
        }
    }

    private final PasswordWs.PasswordCallBackListener callBackListener = new PasswordWs.PasswordCallBackListener() {
        @Override
        public void onSuccess(String msgSuccess) {
            progressBar.setVisibility(View.GONE);

            new UserSessionManagement(ChangePassWordActivity.this).logoutUser();

            Intent intent = new Intent(ChangePassWordActivity.this, LoginActivity.class);
            startActivity(intent);
            finishAffinity();

            Toast.makeText(getApplicationContext(), msgSuccess, Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onWrong(String msg) {
            progressBar.setVisibility(View.GONE);
            alert.showDialog(ChangePassWordActivity.this, msg);
        }

        @Override
        public void onFailed(String mess) {
            progressBar.setVisibility(View.GONE);
            alert.showDialog(ChangePassWordActivity.this, mess);
        }
    };

    @Override
    public void onBackPressed() {
        finish();
    }
}