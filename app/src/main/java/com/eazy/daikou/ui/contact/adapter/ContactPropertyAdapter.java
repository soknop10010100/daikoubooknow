package com.eazy.daikou.ui.contact.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.eazy.daikou.R;
import com.eazy.daikou.model.contact.ContactProperty;
import java.util.List;

public class ContactPropertyAdapter extends RecyclerView.Adapter<ContactPropertyAdapter.ItemViewHolder> {

    private final List<ContactProperty> contactModelList ;
    private final ItemClickListener itemClickListener;

    public ContactPropertyAdapter(List<ContactProperty> contactModelList, ItemClickListener itemClickListener) {
        this.contactModelList = contactModelList;
        this.itemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rowView = LayoutInflater.from(parent.getContext()).inflate(R.layout.contact_layout_body,parent,false);
        return new ContactPropertyAdapter.ItemViewHolder(rowView);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        ContactProperty contactModel = contactModelList.get(position);
        holder.title.setText(contactModel.getDepartment_name()!=null ? contactModel.getDepartment_name():"N/A");
        holder.phoneCallBtn.setOnClickListener(view -> itemClickListener.phoneCall(contactModel));
        holder.telegramBtn.setOnClickListener(view -> itemClickListener.openTelegram(contactModel));
        holder.whatsappBtn.setOnClickListener(view -> itemClickListener.whatsApp(contactModel));
        holder.weChatBtn.setOnClickListener(view -> itemClickListener.weChat(contactModel));
        holder.videoCallBtn.setOnClickListener(view -> itemClickListener.videoCall(contactModel));
    }

    @Override
    public int getItemCount() {
        return contactModelList.size();
    }


    static class ItemViewHolder extends RecyclerView.ViewHolder{

        private final TextView title;
        private final ImageView phoneCallBtn, telegramBtn, whatsappBtn, weChatBtn, videoCallBtn;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.itemTitle);
            phoneCallBtn = itemView.findViewById(R.id.phoneCall);
            telegramBtn = itemView.findViewById(R.id.telegram);
            whatsappBtn = itemView.findViewById(R.id.whatsapp);
            weChatBtn = itemView.findViewById(R.id.wechat);
            videoCallBtn = itemView.findViewById(R.id.videoCall);
        }
    }

    public interface ItemClickListener{
        void phoneCall(ContactProperty contactModel);
        void openTelegram(ContactProperty contactModel);
        void whatsApp(ContactProperty contactModel);
        void weChat(ContactProperty contactModel);
        void videoCall(ContactProperty contactModel);
    }

    public void clear() {
        int size = contactModelList.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                contactModelList.remove(0);
            }
            notifyItemRangeRemoved(0, size);
        }
    }
}
