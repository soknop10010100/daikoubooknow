package com.eazy.daikou.ui.notifications;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.eazy.daikou.R;
import com.eazy.daikou.base.BaseActivity;
import com.eazy.daikou.base.MockUpData;
import com.eazy.daikou.request_data.request.notificationWs.ListAllNotificationWs;
import com.eazy.daikou.request_data.request.work_oder_ws.WorkOrderWs;
import com.eazy.daikou.helper.UserSessionManagement;
import com.eazy.daikou.helper.Utils;
import com.eazy.daikou.helper.web.WebsiteActivity;
import com.eazy.daikou.model.notification.ListNotification;
import com.eazy.daikou.model.profile.User;
import com.eazy.daikou.model.work_order.WorkOrderDetailModel;
import com.eazy.daikou.model.work_order.WorkOrderItemModel;
import com.eazy.daikou.ui.home.complaint_solution.complaint.ComplaintDetailActivity;
import com.eazy.daikou.ui.home.front_desk.FrontDeskDetailPropertyActivity;
import com.eazy.daikou.ui.home.inspection_work_order.WorkOrderDetailActivity;
import com.eazy.daikou.ui.home.parking.ParkingDetailActivity;
import com.eazy.daikou.ui.notifications.apdater.ListAllNotificationAdapter;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class NotificationActivity extends BaseActivity {

    private ListAllNotificationAdapter allNotificationAdapter;
    private SwipeRefreshLayout refreshLayout;
    private ProgressBar progressBar;
    private RelativeLayout noResultLayout;
    private final List<ListNotification> listNotifications = new ArrayList<>();

    private String userType= "";
    private int currentItem, total, scrollDown, currentPage = 1, size = 10;
    private boolean isScrolling;
    private String detailItemId = "";
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        // Init View
        progressBar = findViewById(R.id.progressItem);
        noResultLayout = findViewById(R.id.txtNoResult);
        refreshLayout = findViewById(R.id.swipe_layout);
        Utils.customOnToolbar(this, getResources().getString(R.string.notification).toUpperCase(Locale.ROOT), this::finish);
        findViewById(R.id.layoutMap).setVisibility(View.VISIBLE);
        ImageView btnAddToolbar = findViewById(R.id.addressMap);

        // Init Action
        boolean isUserGuest = MockUpData.isUserAsGuest(new UserSessionManagement(NotificationActivity.this));
        if (!isUserGuest){
            UserSessionManagement userSessionManagement = new UserSessionManagement(NotificationActivity.this);
            User mUser = new Gson().fromJson(userSessionManagement.getUserDetail(), User.class);
            userType = mUser.getActiveUserType();

            recyclerView = findViewById(R.id.listFrontDesk);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(NotificationActivity.this);
            recyclerView.setLayoutManager(linearLayoutManager);

            listAllData(recyclerView);

            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener(){
                @Override
                public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                    if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL){
                        isScrolling = true;
                    }
                }
                @Override
                public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    currentItem = linearLayoutManager.getChildCount();
                    total = linearLayoutManager.getItemCount();
                    scrollDown = linearLayoutManager.findFirstVisibleItemPosition();
                    if(dy > 0) {
                        if (isScrolling && (currentItem + scrollDown == total)) {
                            if(total == size) {
                                isScrolling = false;
                                currentPage++;
                                progressBar.setVisibility(View.VISIBLE);
                                recyclerView.scrollToPosition(listNotifications.size() - 1);
                                listAllData(recyclerView);
                                size += 10;
                            }
                        }
                    }

                }
            });

            refreshLayout.setColorSchemeResources(R.color.colorPrimary);
            refreshLayout.setOnRefreshListener(this::refreshList);

            btnAddToolbar.setVisibility(View.VISIBLE);
            btnAddToolbar.setImageDrawable(ContextCompat.getDrawable(NotificationActivity.this, R.drawable.ic_report_description));
            btnAddToolbar.setOnClickListener(v -> Utils.customAlertDialogConfirm(NotificationActivity.this, getResources().getString(R.string.allow_read_all_notification), () -> {
                progressBar.setVisibility(View.GONE);
                // readNotification(null, true);
            }));
        } else {
            progressBar.setVisibility(View.GONE);
            findViewById(R.id.mainLayout).setVisibility(View.GONE);
            noResultLayout.setVisibility(View.VISIBLE);
            TextView comingSoonTv = findViewById(R.id.noItemTv);
            comingSoonTv.setText(getResources().getString(R.string.coming_soon));
        }

    }

    private void refreshList(){
        currentPage = 1;
        size = 10;

        allNotificationAdapter.clear();
        listAllData(recyclerView);
    }

    private void listAllData(RecyclerView recyclerView){
        // getListNotification();
        for (int i = 0; i < 10; i++){
            ListNotification listNotification = new ListNotification();
            listNotification.setReadStatus(i % 3 == 0);
            listNotifications.add(listNotification);
            progressBar.setVisibility(View.GONE);
            refreshLayout.setRefreshing(false);
        }

        allNotificationAdapter = new ListAllNotificationAdapter(NotificationActivity.this, listNotifications, onClickCallBack);
        recyclerView.setAdapter(allNotificationAdapter);
    }

    @SuppressLint("NotifyDataSetChanged")
    private final ListAllNotificationAdapter.OnClickCallBack onClickCallBack = (listNotification, action) -> {
//        if (action.equalsIgnoreCase("delete_action")){
//            deleteNotification(listNotification);
//        } else {
//            detailItemId = listNotification.getPayloadId();
//            checkAction(listNotification.getReadStatus(), listNotification);
//        }
    };

    private void deleteNotification(ListNotification listNotification){
        progressBar.setVisibility(View.VISIBLE);
        new ListAllNotificationWs().deleteNotification(NotificationActivity.this, hashMapData(listNotification.getId(), false), listNotification, callBackListNotification);
    }

    private void checkAction(boolean isReadStatus, ListNotification listNotification){
        if (isReadStatus) {
            switch (listNotification.getCategoryPart()) {
                case "frontdesk":
                    startDetailScreen("id_list", detailItemId, FrontDeskDetailPropertyActivity.class);
                    break;
                case "parking": {
                    startDetailScreen("order_id", detailItemId, ParkingDetailActivity.class);
                    break;
                }
                case "complain":
                    startDetailScreen("id", detailItemId, ComplaintDetailActivity.class);
                    break;
                case "work_order":
                    initClickOnItemDetailWorkOrderService(detailItemId);
                    break;
                case "cleaning":
                case "security":
                    // startDetailScreen("id", detailItemId, ServicePropertyMainEmployeeListActivity.class);
                    Utils.customToastMsgError(NotificationActivity.this, getResources().getString(R.string.coming_soon), true);
                    break;
                case "invoice":
                    if (listNotification.getUrl() != null) {
                        startDetailScreen("linkUrlVideo",  listNotification.getUrl(), WebsiteActivity.class);
                    } else {
                        Toast.makeText(NotificationActivity.this, getResources().getString(R.string.page_isn_t_available), Toast.LENGTH_LONG).show();
                    }
                    break;
                case "maintenance":
                default: {
                    Utils.customToastMsgError(NotificationActivity.this, getResources().getString(R.string.coming_soon), true);
                    break;
                }
            }
            setResult(RESULT_OK);
        } else {
            readNotification(listNotification, false);
        }
    }

    private void startDetailScreen(String action, String id, Class<?> toClass){
        Intent intent = new Intent(NotificationActivity.this, toClass);
        intent.putExtra(action, id);
        intent.putExtra("type", userType);
        startActivity(intent);
    }

    private void initClickOnItemDetailWorkOrderService(String workOrderId){
        progressBar.setVisibility(View.VISIBLE);
        new WorkOrderWs().getDetailWorkOrderById(NotificationActivity.this, workOrderId, new WorkOrderWs.CallBackListener() {
            @Override
            public void onSuccessWorkOrderList(List<WorkOrderItemModel> workOrderItemModelList) { }

            @Override
            public void onSuccessWorkOrderDetail(WorkOrderDetailModel workOrderDetailModel) {
                progressBar.setVisibility(View.GONE);
                Intent intent = new Intent(NotificationActivity.this, WorkOrderDetailActivity.class);
                intent.putExtra("work_order_detail", workOrderDetailModel);
                startActivity(intent);
            }

            @Override
            public void onFailed(String msg) {
                progressBar.setVisibility(View.GONE);
                Utils.customToastMsgError(NotificationActivity.this, msg, false);
            }
        });
    }

    private void getListNotification(){
        new ListAllNotificationWs().getListAllNotification(NotificationActivity.this, currentPage, 10, new UserSessionManagement(NotificationActivity.this).getUserId(), callBackListNotification);
    }

    private void readNotification(ListNotification listNotification, boolean isReadAll){
        HashMap<String, Object> hashMap = hashMapData(listNotification != null ? listNotification.getId() : "", isReadAll);
        new ListAllNotificationWs().readItemNotification(NotificationActivity.this, hashMap, isReadAll, listNotification,callBackListNotification);
    }

    private HashMap<String, Object> hashMapData(String notificationId, boolean isReadAll){
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("user_id", new UserSessionManagement(NotificationActivity.this).getUserId());
        if (!isReadAll){
            hashMap.put("notification_id", notificationId);
        }
        return hashMap;
    }

    private final ListAllNotificationWs.OnCallBackListNotification callBackListNotification = new ListAllNotificationWs.OnCallBackListNotification() {
        @SuppressLint("NotifyDataSetChanged")
        @Override
        public void onSuccess(List<ListNotification> list) {
            progressBar.setVisibility(View.GONE);
            refreshLayout.setRefreshing(false);
            listNotifications.addAll(list);
            if (listNotifications.isEmpty()){
                noResultLayout.setVisibility(View.VISIBLE);
            } else  {
                noResultLayout.setVisibility(View.GONE);
            }
            allNotificationAdapter.notifyDataSetChanged();
        }

        @SuppressLint("NotifyDataSetChanged")
        @Override
        public void onSuccessMsg(ListNotification notification, boolean isReadAll) {
            progressBar.setVisibility(View.GONE);
            if (isReadAll){
                refreshList();
                setResult(RESULT_OK);
            } else {
                notification.setReadStatus(true);
                checkAction(true, notification);
                setResult(RESULT_OK);
                allNotificationAdapter.notifyDataSetChanged();
            }
        }

        @Override
        public void onSuccessDelete(ListNotification listNotification) {
            progressBar.setVisibility(View.GONE);
            refreshLayout.setRefreshing(false);
            refreshList();
            setResult(RESULT_OK);
        }

        @Override
        public void onFail(String message) {
            progressBar.setVisibility(View.GONE);
            refreshLayout.setRefreshing(false);
            Utils.customToastMsgError(NotificationActivity.this, message, false);
        }
    };

}