package com.eazy.daikou.ui.home.complaint_solution.complaint

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AbsListView
import android.widget.ProgressBar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseFragment
import com.eazy.daikou.base.MockUpData
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.model.booking_hotel.UserH
import com.eazy.daikou.model.complaint.ComplaintMainReplyModel
import com.eazy.daikou.ui.home.complaint_solution.complaint.adapter.ComplaintMainReplyAdapter


class ComplaintDisplayCommentFragment : BaseFragment() {

    private var id = ""
    private lateinit var recyclerView: RecyclerView
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var replyCommentAdapter : ComplaintMainReplyAdapter
    private val complaintCmtList : ArrayList<ComplaintMainReplyModel> = ArrayList()

    private var currentItem = 0
    private var total : Int = 0
    private var scrollDown : Int = 0
    private var currentPage : Int = 1
    private var size : Int = 10
    private var isScrolling = false
    private var userId = ""

    private lateinit var progressBar : ProgressBar
    private var callBackListener : InitCallBackListener?= null

    companion object {
        @JvmStatic
        fun newInstance(id: String) =
            ComplaintDisplayCommentFragment().apply {
                arguments = Bundle().apply {
                    putString("id", id)
                }
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            id = it.getString("id").toString()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_complaint_display_comment, container, false)

        initView(view)

        initData()

        initAction()

        return view
    }

    private fun initView(view: View){
        recyclerView = view.findViewById(R.id.recyclerView)
        progressBar = view.findViewById(R.id.progressItem)
    }

    private fun initData(){
        userId = MockUpData.getEazyHotelUserId(UserSessionManagement(mActivity))
    }

    private fun initAction(){
        linearLayoutManager = LinearLayoutManager(mActivity)

        recyclerView.layoutManager = linearLayoutManager
//        replyCommentAdapter = ComplaintMainReplyAdapter("main_reply", complaintCmtList, onClickItemListener)
//        recyclerView.adapter = replyCommentAdapter

        requestServiceWs()

        onScrollInfoRecycle()
    }

    private fun requestServiceWs(){
        progressBar.visibility = View.GONE
        for (i in 1..10){
            val item = addSubReplyList(i)
            for (j in 1..2){
                item.replies.add(addSubReplyList(j))
            }
            complaintCmtList.add(item)
        }

    }

    private fun addSubReplyList(i : Int) : ComplaintMainReplyModel{
        val item = ComplaintMainReplyModel()
        item.description = "Hello all how are you today for testing"
        item.id = "$i"
        val user = UserH()
        user.name = "Sok Nim"
        user.image = "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBUVFRgWFhUYGBgZGBkYGBgYGhoYGBoYGRkZGRgcGBgcIS4lHB4rHxgYJjgmKy8xNTU1GiQ7QDszPy40NTEBDAwMEA8QHhISGjQhISExNDQxNDE0NDQ0MTQ0MTQxMTQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDE0NDQ0NDQ0Pz80NDQ/Mf/AABEIAOEA4AMBIgACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAAAAQIDBAUGBwj/xAA/EAABAwIDBQUFBQcFAAMAAAABAAIRAwQSITEFQVFhkQYicYGhMlJyscEHE0LR8BQzYoKSwuEjorLS8RUWJP/EABkBAQEBAQEBAAAAAAAAAAAAAAABAgMEBf/EAB4RAQEAAgMBAQEBAAAAAAAAAAABAhESITEDQVEi/9oADAMBAAIRAxEAPwDgcR4nqUY3cT1KdhRC6oTG7iepQXu4nqUsIwoyA93E9SjG7iepShqWECYncT1SYncT1KfCSECYzxPUoL3cT1KWEQgTG7iepSOqRq4jzKcclTfXDvw673AeHGfRFiV15zdHEzE9ZTTUrRIcchJG8xwcP1puVJrS10gxEyN0Dzjom1L2cQ17sDhE+uSlumot1XviQ8nQzJ9R+ohPp1zqTq4Fwk5xmBI0B0WfTrkATpHLiSMzofzUzamLH/KctJU2q429cWtxHSZzidTk7lG/gpg+fZfiynI5+Y3Kg14xEEAgjQ5iYnLh7Ovgo2WrSC2YIJicwY70dAeibTTSLncT1KQuPE9SqVGo7GYeHNAkB28b4+at/eDFhILSQCM5aQfX56FXaaGM8T1KYXu4nqVKWphCoYXniepSF54nqU6Ehagjc93E9SmF7vePUqQhNLUEZe73j1KTG73j1KeWpHNQaUJYShOhGTIShLCXCgaAlhKAlhA2EEJ6EDIRCehBVuz3SJidTwH+ViVSMw0RnHM+S1r5wLg2NY6kyT5AeqoWtuXkROZiYzk6Qs26bkNtjUf3GyZynQRpnGq06XZ7CO/jnXuxHXVdVsjZDGNjJr8xiiRO7F5fNF1auY497Xdq3IuGUaePPfC5XK2u2OEnrlnbGYQSx5P8JGEg8Dwz+Szq+z6rMoMbyNCurr0Ax8wQHEEwSRJAzHDwVa5LqbgQcjkRuzPBWbXLGac1ReQ7vCMjrIIyOXnPySsuQCSMs5zPI6R4LobvZYqg4CGvG7SZzJ8ctea5i+sH0nEPaRz3HwPmtcnO42I6dQgzviNc4j8lNUuSMOfsxHKMxHVUYSudKu2W1aXmIAHXdxy/9CthY2zGkv8ADX9eS22hax8YpsJpCkISEKqhISEKQhNIVEZCaQpCmkINIBLCAnKMmwgBORCBISoRCBEJYRCBEQlKCgp1W95pETLzznQRu4LT2ZZ5hxHCeMgwT9PJY1tVxNBAkknLnmY64fFdZZMgcoEcJgk+nzWMq7YNqhSAaAMyWnF1y9fkpDaYmiSDhDiePL1/WabRqAZu3DPkd2XNK+8wtPM90bt/5yuTspXdhOEa90z/AEyPUKpa7JNaq1h0Jg+W9agfkBnoCfPM/NbfZm1hwdGgS3SybpP/AKlSYd+iztp9l2OBGKRwcJXa1CqFwclz3du0k08g2n2JeJLHDwP5rmbnZFZhhzD4jThqvbKyzL23a9j2ECS0weeo9YWscrvtyz+WOtx5rZWuBuftHX8lbah4zQwL1x4qCmkJ6IUVEU0hSEJpCojISEJ5CaQg0UIQoyIQhEI0EJUIBCUBCBE2pofApyiuT3HfCfkgp7KEDDvAEnm4Bx9Ml2Fvs25Aa40XYAC5swCQRqGE4i2N8b1zfYokVC4iS1j3tBEgvYwlkjQgOgxvDV242FTo0WXFWrguXy816rzLSdI8j5yuGeWunr+Py5TdulJ5exoD2kE6AjXnzVgvhuJwl0QJ0xHU+i6W0osrtwtcHtcwPa8CNRIcJ0WNa2BNVtLVzXFvmNXLMu2sseJ+ytm1KubW/ETo3kT9F1uzrA02wTJOpWhb0W02BjRkPU7yeaR1YAZhTKmKvW0WbXlRbY28WSKeGeJ/JcTe9rbqmSXsBZO4bvJZ478b5a9dJcZFZlZ5nLVZ1v2wo1cpwuO45Zqy25bIMhNWLbL4425Hfd8TvmVGFb2nhNV5YQQTOW4nMjrKqr2Tx87Lq03EiUpRCrJCUhSpCjRhSFOSFBeTgEJQVAQkKcmlAiUBCcgakT0woAJlx7LvAp4UV1OB2HWMkEfZ27bTuaAdk0uIfPu1GuYdN0PnyXoHaixfcvowz7xlNkPYDAD2nPEJmIAXNdiezTazmVntcGhshpkNdBLS4DeJb6dPWbK0Y0uPslw15ab15fp3lqPb8Osbag2FSDKbcTWteWguDPZA/C1vINwptlbj9pq1PBrfMDF8ln219Ta5zKRlrHFuuITJ0PDJaFg/U8Ssy6dbN9tqVSvaeIQpmVFFWrhS+JJ24vb+z3NDnNc0kCQ14fhnnge36rzy/vq5JH3bY0lndHPMkr1natQEEcVzQ2Qwu0Ekq45aiZY7/Xn9psd9WrAaQNSUu0b4l5pl5Y1pLSROoMZkZxPJexWHZ8MExuXnHaDYv3dao+AJeMoGeIYp6jdxW8byy7Y+k447jLpNAAgyOMzPmnFLCCF6XivpEiUNRCrJEEJUI0jQlKRBfhJCfUpuFJ9Ufh3H8XGPBZtXarQAcJzAOkjPhmFNDRQsQ7XPEj+Qf9007WPvH+hv/ZXQ3UhKwv8A5I++7yaB/ch+1Scsbx4Bv5ppNNsJrngbx1WFTexxzeZ/iynzmFesbNr36dxokkQQeAB3k/QppVt9wwfiH65rZ7KbKN5cMpjJhlznbwxkYnAebWjm7lBztpWbYa1gABIGS9B+yK0H3ty8aU206LeAnE98dGqZdQjpmUGh72tADWEU2AbmMY0AZ85S12SDOiu3VDBVfllUh4+IANcPRp8yq9fReHPfJ9D52cZpyVCwbSe8tJON+IyZ3AADyC39n6LMvhDpVqxugjpWxUGSybqpE5rTdWBCxbx2qVIyrq6hULG/Z+0UzUIbTDjicdJAJbJ4SmbTdGphVre+p4SGjFuPBJOkt7drR7aWTnlja7TAPhlwOhXFdsNoU6rpYZBd8p/NYl/ZtLw/AABqREZ/r1Ve4eDAAAA0AyC7YT/Th9MpMbEKChBXoeIiSUEpEAglIEpRo0hInOTSqOhu3B1EtAgYSAOS5Gg3FSG8tLmnyM/IrqGPBbLTLXCR4ELG2RRBFVu8VD0IH5IMp9qq76BXUPtRCp1rUKjnSwqNwWjXoxKpvGeSUiuux2VbYKbGHJzu+7xdoPJseq57ZjGl/eGKBIZ7xG48t5HJbbL8ufJGqxjOyt6o0EYuAXoP2PW+G0qP3vuHmeIaGs/tK8qurw4DuyXsX2VtjZ1LnLj/AD9/+5MvFjp7+2xtgQHDvNJ94ceREg8isOpmNIOhB1BGRBWT26+0SlZNdTpEVK+YjVrD/FxI4f8Ai5zsV27F0Pu7ggXERigNFcDQgDIVQNw9oDLMQuGWO+3b458bq+N6/pzKwn3BYeS27q9afZMjjuWBtATJlco9lrWtNqhw1UlavIyXn9XaLab/AGw08JWxY7eBgEjr8irxZ5xJtPZjq5Ic4tbwGU+ay7jYlpSbDgR/OQRzG70XUU7lrswR6LG7Q2bXtgu5pLpZZO9OTfbtBH3dUuaCZGWhneN/kpJQ+g1ndbnvceJP69U1q9OE6eH7ZcsipE5NIW3EiAhCATSlQkaNTSnuUZVFu3uIc+n7pxD4XjFl5kqvsephqVmne4H5qrtCoaddp4sHgYJj6Kk24iq4g6kfJQda94TalKQq9k2RJKuOIAWhjXdusptEueGtEkmAF1FwzE0lc5Syrs+NvzhAyrs94MxhIz19ZHNXRRc5zXYmtcBnEkE7yNI3dVeuczKrtOamg02ozLnOd4nL0heu9ntrttdkGtMYKDMPxxgb/ujovJqhhpPJdR20quobP/ZiYmpTjm1rJInf3sRWMmsfK81uSXOc8uxEkkknMk579VC0kGRuzyTVoWOyqlYOcxjiAJJA7o8XaD5rKN7Zvah+GKjyXTvGoj2ieM66aznmtVt8+tkw66BsucfBrZJXDCk9negCDGeE/wC06qV+0Dgwta1k+0WjNw4ch84HBYuMdcfrZNV1VfsVdOcHvaGNcdXvYXnmGNJI/mg8lS7V4bf7u3pDDgaKj3j2nOdIbJ4Rn/NyXKeSfUqucZcS45CSSTAyAk8lqSMXK1p2+26tOIdIUo21UqOGJ0Djr6BY2HJRq3Gb8WZ5T9dfwMzz4ohc3bXr2HIyOB0Wxb7RY7I908Dp5Fblc6upspSkVBKaSgpCgQlIXpXJhVClyYSlKaFRhVrlz8OIzhGEeCZTcZkKfaNvgeRu1HgVVBXLyq6ewv8ALNXrd5ef1kuPp1SFtWW2GsbBldJZUdHUpiIXP1aP+qw/xs/5BaFttNr8pUleh3mu4OafVDQfTUL6YV4tVdzUVWqMkYdZyHmvZaWzmGqC9mNmF7HNw4/agg4YPCPNeR2tLFVpN96rTb/U9oXstvTeCIcsZN4xi0/s+pue6p92ykzMtYGtc6N2J248hpPmtDZ3Zj7tsuILcOYjXdELqbek6MypK7IaBzlYtX908i7UdkaIY4tZm84WAA4sR0IjflpnPovNe0HZ+4snhldhaXNDmnUOaeB4jQjcvpynZNdUa4gHCMQy0JMfRUe2ewaV5QNOo0nCcbSMnNIBEtO4wfAxmiZat6fLKAuh2h2XrU31GZEU8y7i2YnTUEtEc1iVmgEgbsui1pgwOTSkSwpQiUFKGFDmwrqi/Z7QczI95vA6jwP0WzQrNeJaZ+Y8QuVU1K4c32SR+t/FWZJp0xKFFb1cbA7iPUZFSrcDHJhUjhko0AmEpyaVRiXFy58YtRvVdCFxt2oQhCgmoVS0yCuhsNpSIcVzQUtN8LeN0O4ZWaQM00sGa5qjdOGh3LQtb86FbG3smn/+miAJIe1wHNve/tXp1u+t7wb5SvPezDcVzT/hxu6Mc36hejslc8vXTHxuWVF5jFVcfCAtH7oHXMKhssmBmtLFAk7gSspl0KUZxxI6ZfRVrq8a0E6/rf6qehk1vMT1z+qzr63aHAkd0zvIzAnduRMZ24u7tmCncF7Ymk94IyktaQ4E7xMZLwuq+SQQNTmvojtbRDLCs4AZsgyATL3Bup0ynqvnas0yct66YlLStw7Q+SSvTgeCcxnBSXukkyTGZ1KtjJ1izEIPFT3FllkodnHNbjmZeKsHMPtyDoog3NbF7DRJ8AssjSciTJ5BYsg2tlj/AEx4n5q2m02hoAGgAA8E5aQ0qMqQqMqhpKRKU1UYNRhaSDqMio1a2if9V/xFVVwUoCeGpgKcHLU0FCAgK5Qt5BPAH5LUmwUGYslubO2bGZVLZLASCuhbUGkrQ3uxdAffPefwMDG/zuxO/wCDeq7um4HQrjexdIllR0Ah1Qjn3WMGvjK6kW2/vD1XO+uuPjo9mtyyKvXTgGGd8N/qMfUrNsMTWCTMnWIyHHJWzWl7WkZBpdrvkAZR4rKZRckRCpV7fHhY490Z/FwVxzWlNDRxRmacd9qFXBYOG9z2N8Yl39q8DpkOOa9o+126ItqbDqXudlP4Wx/cvEbbMrpj4l9Xv2QahZ+0DmBwGa2WugLAuquJzndPDRXKos7KHeXQ1nhjcTtFz+zDGaTad2Xd2cgm9QQXl0XuncNByVdzpMlIApW4RrnyGn+Vj0b9pOBs64QpUjXhwBGhEhKVvSEKiJUhUaoQppTimKjHvv3j/iPzVdWL394/4j81XXGqEIQoHNKnZckT4QqyFqXQtULotEBWKe0XDes1CsyHs32b3GK1J41X/Jq7e3lxAnVeO9j6z20AWuIGN2mk5f4XZbI20+m/E9xLchmTlzCza6Y+PTWUmwJMgDTxUdWuBVLCB7DY6lVrO/Y8g4huJzHllKloODxjLYcSTnrE90dAFNL+tCmAAkrVC0E5QOKbRIjPNLWpgg8IzBzCqfryb7XL0uwNLcOGmXagzjdrl8C8ostV2/2l3wdcVoOQbTY3fmAS7wzd6LhLF3eXSdMX1fvauFvOPUrFlWb2viMDRVwRwPX/AAsZXtE1O5LRAUBKdI4ev+E1LdiVlEkToPP0Ca+mRuMcSIVplFzW4iSBuEgg+RT/AL54HeiCJgtAJ/pz804i9s2fu2zzjwkq1KpWd41wDdCBAnOYG4q4tzxmkKjKeVG5VSlMTk0qjGuxD3/E75lQKSo8uJJ1JJ6pAzKVx0piErQpXUD4prYhQlISKAQhCD0f7Ob1n3D6T2yBUxA7wHtaP7CuouNlMcA9jshJI5wSPBec9gdpCjdNY+MFb/TdOgcT3Hf1QJ4OK9hqbMbADRBdImZAMGBG7RL46Y+ObtLgsJdJG7UgwTBiOUrq7LtOyYfv36/NYW1uz7y1pZBInEBMGd44f5XN3GzqrfwVW8w1zh6BWeL49atdpsfm0ghc39rO1H0rECk9zTUqBpLSQcO8SMxJheePurmh3mPBA1k4CPEFZXaftVcXDKbKlRsNBOFoGRPvOjMxwV0za529qkkNnJgjxd+I9VWD40TCUJtgIQhZAntGeseKYr2zaTXOzErWM3ROxvuxI3xjPlmSOivWtV41Yx/Egw7zBzlWv2CmRm0frmmOsHN7zHubH4T32+Gei6IytoMDXh7QW5glukGVpSqF5cB0tdhDuI09c2lWrapiYD5HxCgeUxykITHKhia8qVjCdBMa8B4ncsi8rFxIB7oPXmlukioFPbnODvyUTRkpDTMSsSNC4olpRQrlp5LQogVG56ws2vRLTBSzXcFwsa8ZaqnVpFp0SUqhaZC0qdwx4hwzV6oyUqt3FtGmiqELNmgAwvZ+yHbNtak1lcEO9kv3EjflmDOa8YW9sK+DGVG4cTgWvbJhoHsvLt/uac1GpdV7y/bVGiyajgXCWk7nQJBnTMESd0lcVtz7R2M7tBuN/EnuA83D2vAdQvNL++q14xnCwaNEho555+ZVGo1oEYpVk/pcmltfbtW4dNeo6odQCYY34WDLz1WK50pHJEtZCEKSmzE4AbyB1WRPY2he7+Ean6Dmo7qjgcW8NPA6LoaLA1oaMgP1Ko7WoYmhw1br4f4+q1Z0jFVq3z3SeRg+RCqrSs6Qe3uhuIcZE+BBySRVuheEd0uJ/hfDHeTvZd5wr1KsTk0kHeHS0+uR8lmVHkAh1PU+1GMfQjqq9CtUb7Dw4e7M/wC12fRa3pGreYXDDUbhP4XxoeZ3LOtSabix2hOR3Tu65Jat9XeIc3xJbGXMnIKtUoPAGJwG8Cc/IfkpbPVbRKr3FaMhrqeQ4pn7Y1rRiMugd1sertB4ZrMqVy4nmfTcFraaW7m+dUGEd1jdw3+OeaoOMqV7gGwPNQ02ys1QxXG+wlQtQTbK9opNq/mhCfgylJS1QhYnou0/ZVKrqhC1kI1Ytden/IIQsC1c/l9VQdqhC3QxCELmBWdn/vG+KEKjoSobj2HfCfkhC6VHNrQ2V7XmhCzj6NGr+98llbQ9ooQtZEW2+xR+MfMKpdfvX/EUIWFVHJzEIQBUlrqlQr+j/9k="
        if (i % 2 == 0){
            item.image = "https://sandboxeazy.daikou.asia/storage/20b20e781dac20cf4b0c180d21020740.jpg"
        }
        item.user = user
        item.image
        item.created_at = "2022-12-21 02:27:08"
        return item
    }

    private fun onScrollInfoRecycle() {
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true
                }
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                currentItem = linearLayoutManager.childCount
                total = linearLayoutManager.itemCount
                scrollDown = linearLayoutManager.findFirstVisibleItemPosition()
                if (dy > 0) {
                    if (isScrolling && currentItem + scrollDown == total) {
                        if (total == size) {
                            isScrolling = false
                            currentPage++
                            progressBar.visibility = View.VISIBLE
                            recyclerView.scrollToPosition(complaintCmtList.size - 1)
                            requestServiceWs()
                            size += 10
                        } else {
                            progressBar.visibility = View.GONE
                        }
                    }
                }
            }
        })
    }

    interface InitCallBackListener{
        fun onDoReplyCallBack(item: ComplaintMainReplyModel)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            callBackListener = context as InitCallBackListener
        } catch (e: ClassCastException) {
            throw ClassCastException("$context must implement LogoutUser")
        }
    }


}