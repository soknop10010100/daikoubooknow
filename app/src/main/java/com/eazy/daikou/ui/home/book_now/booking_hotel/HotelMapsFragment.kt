package com.eazy.daikou.ui.home.book_now.booking_hotel

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.*
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.FragmentContainerView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.base.BaseFragment
import com.eazy.daikou.base.MockUpData
import com.eazy.daikou.helper.CustomStartIntentUtilsClass
import com.eazy.daikou.request_data.request.book_now_ws.BookingHotelWS
import com.eazy.daikou.request_data.request.book_now_ws.RequestHashMapData
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.helper.map.GPSTrackerActivity
import com.eazy.daikou.helper.map.MapsGenerateClass
import com.eazy.daikou.helper.map.MapsGenerateClass.Companion.getMarkerBitmapFromView1
import com.eazy.daikou.helper.web.WebsiteFragment
import com.eazy.daikou.model.booking_hotel.NearbyHotelLocationModel
import com.eazy.daikou.model.home.SubItemHomeModel
import com.eazy.daikou.model.my_property.Child
import com.eazy.daikou.ui.home.book_now.booking_hotel.adapter.BookingHomeItemAdapter
import com.eazy.daikou.ui.home.book_now.booking_hotel.adapter.HotelCategoryItemAdapter
import com.eazy.daikou.ui.home.service_provider.ServiceProviderBottomSheetFragment
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions

@SuppressLint("ValidFragment")
class HotelMapsFragment : BaseFragment(), OnMapReadyCallback, ServiceProviderBottomSheetFragment.OnClickAllAction {

    private lateinit var mapFragment: SupportMapFragment
    private lateinit var mMap: GoogleMap
    private lateinit var progressBar: ProgressBar
    private var propertyListLatLng: ArrayList<NearbyHotelLocationModel> = ArrayList()
    private var hashMapLatLng: HashMap<String, Any> = HashMap()
    private lateinit var recyclerViewCategory : RecyclerView
    private var subHomeModelCategoryList: java.util.ArrayList<SubItemHomeModel> = ArrayList()
    private var category = "hotel"
    private var isImageDisplay = false
    private lateinit var btnSwitch : ImageView
    private var alertProgress : AlertDialog? = null
    private lateinit var titleToolbar: TextView

    private lateinit var tourismMapFragment : FragmentContainerView
    private lateinit var hotelMapFragment : RelativeLayout
    private val tourismLink = "https://www.google.com/maps/d/viewer?mid=1IF9C0dFdJZxIIbbTubDkge7k0HfQbIgP&g_ep=CAISBjYuNDguMRgAIN1iQgJLSA%3D%3D&g_st=it&ll=12.367077460695903%2C104.94415515000003&z=7"

    companion object {
        fun newInstance(actionType: String): HotelMapsFragment {
            val fragment = HotelMapsFragment()
            val args = Bundle()
            args.putString("action_type", actionType)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.hotel_maps_fragment, container, false)

        initView(view)

        initAction()

        return view
    }

    private fun initView(view: View) {
        progressBar = view.findViewById(R.id.progressItem)
        progressBar.visibility = View.GONE
        recyclerViewCategory = view.findViewById(R.id.recyclerView)
        btnSwitch = view.findViewById(R.id.btnSwitch)
        titleToolbar = view.findViewById(R.id.titleToolbar)
        titleToolbar.isAllCaps = true
        titleToolbar.text = resources.getString(R.string.nearby_you)

        tourismMapFragment = view.findViewById(R.id.tourismMapFragment)
        hotelMapFragment = view.findViewById(R.id.hotelMapFragment)
    }

    private fun initAction() {
        mapFragment = childFragmentManager.findFragmentById(R.id.map_google) as SupportMapFragment
        mapFragment.getMapAsync(this)

        if (BaseActivity.latitude != 0.0 && BaseActivity.longitude != 0.0){
            // initServicePropertyList()
            refreshList("tourism_map")
        } else {
            openSomeActivityForResult()
        }

        // init category
        subHomeModelCategoryList.clear()
        val drawable = ResourcesCompat.getDrawable(resources, R.drawable.ic_visit_home_page, null)
        subHomeModelCategoryList.add(RequestHashMapData.getItemCategoryBooking("Tourism", "tourism_map", true, drawable!!, "booking_category"))

        subHomeModelCategoryList.addAll(RequestHashMapData.addCategoryItemList(resources))

        initRecyclerViewCategory()

        // Switch image
        btnSwitch.setOnClickListener(CustomSetOnClickViewListener{
            isImageDisplay = !isImageDisplay
            initServicePropertyList()
        })

        // Location maps
        val websiteFragment = WebsiteFragment.newInstance(tourismLink, "tourism_map")
        CustomStartIntentUtilsClass.refreshFragment(R.id.tourismMapFragment, websiteFragment, childFragmentManager)
    }

    private fun openSomeActivityForResult() {
        val intent = Intent(mActivity, GPSTrackerActivity::class.java)
        resultLauncher.launch(intent)
    }

    private var resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode == BaseActivity.RESULT_OK) {
            if (result.data != null){
                val data = result.data!!.getBooleanExtra("isSuccess", false)
                if (!data){
                    Utils.customToastMsgError(mActivity, "Cannot find your current location !", false)
                } else {
                    refreshList("tourism_map")
                }
            }
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap.mapType = GoogleMap.MAP_TYPE_NORMAL

        zoomMap(BaseActivity.latitude, BaseActivity.longitude)

        if (ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                mActivity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }

        mMap.isMyLocationEnabled = true
        mMap.uiSettings.isZoomControlsEnabled = false
        mMap.uiSettings.isMyLocationButtonEnabled = true
        mMap.uiSettings.isCompassEnabled = false

        mMap.setOnMarkerClickListener { marker: Marker ->
            try {
                val locationModel = hashMapLatLng[marker.title] as NearbyHotelLocationModel
                val bttSheet = HotelDetailNearbyLocFragment.newInstance(category, locationModel)
                bttSheet.show(parentFragmentManager, "HotelDetailNearbyLocFragment.TAG")
            } catch (ex : Exception){
                Utils.logDebug("Jeeeeeeeeeeeee", ex.message)
            }
            true
        }

        MapsInitializer.initialize(mActivity)
    }

    private fun zoomMap(lat: Double, lng: Double) {
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(lat, lng), 15f))
    }

    private fun initServicePropertyList() {
        progressBar.visibility = View.VISIBLE
        val eazyUserId = MockUpData.getEazyHotelUserId(UserSessionManagement(mActivity))
        BookingHotelWS().getNearbyLocationWs(mActivity, category, eazyUserId,
            BaseActivity.latitude.toString(), BaseActivity.longitude.toString(),
            object : BookingHotelWS.OnCallBackNearbyListener {
                override fun onSuccessNearbyLocation(hotelDashboardModel: ArrayList<NearbyHotelLocationModel>) {
                    progressBar.visibility = View.GONE
                    propertyListLatLng.clear()
                    propertyListLatLng.addAll(hotelDashboardModel)

                    addMarker()

                }

                override fun onFailed(message: String) {
                    progressBar.visibility = View.GONE
                    Utils.customToastMsgError(mActivity, message, false)
                }

            })
    }

    private fun addMarker() {
        if (mMap == null) return

        mMap.clear()
        for (latLongPropertyModel in propertyListLatLng) {
            drawPLaceOnMap(latLongPropertyModel)
        }
    }

    private fun drawPLaceOnMap(latLongPropertyModel: NearbyHotelLocationModel?) {
        if (latLongPropertyModel != null) {
            if (latLongPropertyModel.coord_lat != null && latLongPropertyModel.coord_long != null) {
                try {
                    hashMapLatLng[latLongPropertyModel.id!!] = latLongPropertyModel

                    val latLng = LatLng(latLongPropertyModel.coord_lat!!.toDouble(), latLongPropertyModel.coord_long!!.toDouble())

                    mMap.addMarker(
                        MarkerOptions().
                        title(latLongPropertyModel.id!!).
                        position(latLng).
                        icon(BitmapDescriptorFactory.fromBitmap(
                            drawViewBitmap(latLongPropertyModel.image!!, latLongPropertyModel.price_display!!, latLongPropertyModel.isClick))
                        ).anchor(0.5f, 1f)
                    )

                } catch (exception: Exception) {
                    exception.printStackTrace()
                }
            }
        }
    }

    private fun drawViewBitmap(imageUrl : String,  name: String, isClick : Boolean) : Bitmap{
        return if (isImageDisplay){ // When show it will slow, so find solution . . .
            MapsGenerateClass.setMarkerBitmapFromView(mActivity, imageUrl, name)!!
        } else {
            getMarkerBitmapFromView1(mActivity, isClick, name)!!
        }
    }

    override fun onClickSelect(titleHeader: String, list: Child) {}

    private fun initRecyclerViewCategory(){

        setActiveItemClick("tourism_map")

        recyclerViewCategory.layoutManager = LinearLayoutManager(mActivity, RecyclerView.HORIZONTAL, false)
        var bookingHomeItemAdapter : HotelCategoryItemAdapter? = null
        bookingHomeItemAdapter = HotelCategoryItemAdapter(false, subHomeModelCategoryList, object : BookingHomeItemAdapter.PropertyClick{
            @SuppressLint("NotifyDataSetChanged")
            override fun onBookingClick(propertyModel: SubItemHomeModel?) {

                setActiveItemClick(propertyModel!!.id!!)

                category = propertyModel.id.toString()

                bookingHomeItemAdapter?.notifyDataSetChanged()

                refreshList(propertyModel.id!!)
            }

            override fun onBookingWishlistClick(propertyModel: SubItemHomeModel?) {

            }

        })
        recyclerViewCategory.adapter = bookingHomeItemAdapter
    }

    private fun setActiveItemClick(id : String) {
        for (itemSub in subHomeModelCategoryList){
            itemSub.isClickCategory = itemSub.id == id
        }
    }

    private fun refreshList(id : String) {
        if (id == "tourism_map") {
            tourismMapFragment.visibility = View.VISIBLE
            hotelMapFragment.visibility = View.GONE
            btnSwitch.visibility = View.GONE
            titleToolbar.text = "Tourism Location"
        } else {
            tourismMapFragment.visibility = View.GONE
            btnSwitch.visibility = View.VISIBLE
            hotelMapFragment.visibility = View.VISIBLE
            titleToolbar.text = resources.getString(R.string.nearby_you)
            initServicePropertyList()
        }
    }

    private fun generateProgressBar(context : Context){
        val alertLayout = LayoutInflater.from(context).inflate(R.layout.progress_bar_circle_layout, null)
        val dialog = AlertDialog.Builder(context, R.style.DialogTheme)
        dialog.setCancelable(false)
        dialog.setView(alertLayout)
        alertProgress = dialog.show()
    }

}