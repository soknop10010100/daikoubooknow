package com.eazy.daikou.ui.home.rules_Announcement.RulesAdapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.eazy.daikou.R;
import com.eazy.daikou.model.rule_announcement.RulesModel;

import java.util.ArrayList;

public class RuleAdapter extends RecyclerView.Adapter<RuleAdapter.ViewHolder>{

    private final Context  context;
    private final ArrayList<RulesModel> rulesModelArrayList;
    private  final ItemClickCallBack itemClickCallBack;

    public RuleAdapter(Context context, ArrayList<RulesModel> rulesModelArrayList, ItemClickCallBack itemClickCallBack){
        this.context = context;
        this.rulesModelArrayList = rulesModelArrayList;
        this.itemClickCallBack = itemClickCallBack;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_rule_announcement,parent,false);

        return new ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        RulesModel rulesModel = rulesModelArrayList.get(position);
        if (rulesModel != null){
            holder.title_Rule.setText(rulesModel.getTitle());
            holder.date_Rule.setText(rulesModel.getTimePostToNow().getValue() + "");
            holder.month_Rule.setText(rulesModel.getTimePostToNow().getType());

            holder.itemView.setOnClickListener(view -> itemClickCallBack.clickCallback(rulesModel));
        }

    }

    @Override
    public int getItemCount() {
        return rulesModelArrayList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        private  final TextView title_Rule,date_Rule,month_Rule;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            title_Rule = itemView.findViewById(R.id.title_Rule);
            date_Rule = itemView.findViewById(R.id.date_Rule);
            month_Rule = itemView.findViewById(R.id.month_Rule);
        }
    }

    public interface ItemClickCallBack{
        void clickCallback(RulesModel rulesModel);
    }

    public void clear(){
        if (rulesModelArrayList.size() > 0 ){
       //     for (int i = 0; i < rulesModelArrayList.size(); i++) {
                rulesModelArrayList.remove(0);
         //   }
            notifyItemRangeRemoved(0, rulesModelArrayList.size());
        }
    }

}
