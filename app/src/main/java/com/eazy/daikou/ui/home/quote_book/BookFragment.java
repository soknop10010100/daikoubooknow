package com.eazy.daikou.ui.home.quote_book;

import android.content.Intent;
import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.eazy.daikou.R;
import com.eazy.daikou.base.BaseFragment;
import com.eazy.daikou.model.book.BookModelV2;
import com.eazy.daikou.ui.home.quote_book.adapter.QuoteBookAdapter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class BookFragment extends BaseFragment {

    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private List<BookModelV2> bookModelV2List = new ArrayList<>();
    private String key_type = "";

    public static BookFragment newInstance(List<BookModelV2> bookModelV2List, String keyType) {
        BookFragment bookFragment = new BookFragment();
        Bundle args = new Bundle();
        args.putSerializable("book_list", (Serializable) bookModelV2List);
        args.putString("key_type", keyType);
        bookFragment.setArguments(args);

        return bookFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null && getArguments().containsKey("book_list")) {
            bookModelV2List = (List<BookModelV2>) getArguments().getSerializable("book_list");
        }
        if (getArguments() != null && getArguments().containsKey("key_type")) {
            key_type = getArguments().getString("key_type");
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_book, container, false);

        initView(view);

        initAction();

        return view;
    }


    private void initView(View view) {
        recyclerView = view.findViewById(R.id.Rv_recyclerViewBookQuote);
        progressBar = view.findViewById(R.id.progressItem);
    }


    private void initAction() {
        progressBar.setVisibility(View.GONE);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);

        initRecyclerView();
    }

    private void initRecyclerView() {
        QuoteBookAdapter quoteBookAdapter = new QuoteBookAdapter(mActivity, bookModelV2List, callBackQuoteBook);
        recyclerView.setAdapter(quoteBookAdapter);
    }

    private final QuoteBookAdapter.CallBackQuoteBook callBackQuoteBook = bookModelV2 -> {
        Intent intent = new Intent(getContext(), QuoteAllBookActivity.class);
        intent.putExtra("list_of_book", bookModelV2);
        intent.putExtra("key_type",key_type);
        startActivity(intent);
    };
}