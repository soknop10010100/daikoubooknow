package com.eazy.daikou.ui.home.book_now.booking_hotel

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.os.Bundle
import android.view.*
import android.widget.RelativeLayout
import com.eazy.daikou.R
import com.eazy.daikou.helper.Utils
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class HotelFilterSortFragment : BottomSheetDialogFragment() {

    private var mContext : Context? = null
    private lateinit var fragmentLayout : RelativeLayout

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupFullHeight()

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view = inflater.inflate(R.layout.fragment_hotel_filter_sort, container, false)

        fragmentLayout = view.findViewById(R.id.fragmentLayout)
        fragmentLayout.addView(SampleCanvasActivity(true, mContext!!, "#0b4891"))
        fragmentLayout.addView(SampleCanvasActivity(false, mContext!!, "#0b4891"))

        return view
    }

    private fun setupFullHeight() {
        val bottomSheetDialog = dialog as BottomSheetDialog
        val behavior = bottomSheetDialog.behavior
        behavior.state = BottomSheetBehavior.STATE_EXPANDED
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
    }
}

@SuppressLint("ViewConstructor")
class SampleCanvasActivity(private val isTop : Boolean, context: Context, private val fillColor: String) : View(context) {

    private val paint = Paint()
    private val path = Path()

    @SuppressLint("DrawAllocation")
    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        paint.strokeWidth = 1f
        paint.color = Color.parseColor(fillColor)
        paint.style = Paint.Style.FILL_AND_STROKE
        path.fillType = Path.FillType.EVEN_ODD

        val y  = Utils.getScreenHeight(context).toFloat()
        val x =  Utils.getScreenWidth(context).toFloat()
        val height = 200F

        if (!isTop){
            val c1= (y - height)
            val c2= (x * 2 / 3)
            val c3= (x * 5 / 6)
            val c4= (y - (height * 6 / 5))

            path.moveTo(0F, y) // bottom left
            path.lineTo(0F, (y - 20)) // top left
            path.cubicTo(x, c1, c2, c3, y, c4) // curve to top right
            path.lineTo(x, y) // bottom right
        }

        else {
            path.moveTo(x, 0F)// top right
            path.lineTo(x, 20F) // bottom right

            val cont1 = PointF(20F, 212F)
            val cont2 = PointF(420F, 800F)
            val cont3 = PointF(720F, 50F)

            path.cubicTo((x - x * 2 / 3), 0F, (x - x * 5 / 6), (height * 6 / 5), 0F, height) // curve bottom left
            //path.cubicTo(cont1.x, cont1.y, cont2.x, cont2.y, cont3.x, cont3.y) // curve bottom left

            path.lineTo(0F, 0F) //top left
        }

        path.close()
        canvas.drawPath(path, paint)
    }
}