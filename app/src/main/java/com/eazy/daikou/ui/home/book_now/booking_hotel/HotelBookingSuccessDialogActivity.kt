package com.eazy.daikou.ui.home.book_now.booking_hotel

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.booking_hotel.PaymentSuccessModel

class HotelBookingSuccessDialogActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hotel_booking_success_dialog)

        initAction()

    }

    private fun initAction() {
        // Info
        var paymentSuccessModel = PaymentSuccessModel()
        if (intent != null && intent.hasExtra("payment_info")){
            paymentSuccessModel = intent.getSerializableExtra("payment_info") as PaymentSuccessModel
        }
        Utils.setValueOnText(findViewById(R.id.bookingCodeTv), paymentSuccessModel.code)
        Utils.setValueOnText(findViewById(R.id.firstNameTv), paymentSuccessModel.first_name)
        Utils.setValueOnText(findViewById(R.id.lastNameTv), paymentSuccessModel.last_name)
        Utils.setValueOnText(findViewById(R.id.phoneTv), paymentSuccessModel.phone)
        Utils.setValueOnText(findViewById(R.id.totalPaidTv), paymentSuccessModel.paid)

        findViewById<Button>(R.id.btnClose).setOnClickListener(CustomSetOnClickViewListener {
            //Back To Home Page Hotel
            val intent =
                Intent(this@HotelBookingSuccessDialogActivity, HotelBookingMainActivity::class.java)
            intent.putExtra("action", "success_payment")
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
            finishAffinity()
        })
    }

}