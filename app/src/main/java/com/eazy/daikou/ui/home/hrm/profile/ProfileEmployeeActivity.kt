package com.eazy.daikou.ui.home.hrm.profile

import android.annotation.SuppressLint
import android.content.Intent
import android.content.res.ColorStateList
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.base.MockUpData
import com.eazy.daikou.request_data.request.hr_ws.MyProfileHRWs
import com.eazy.daikou.request_data.request.profile_ws.ProfileWs
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.helper.web.WebsiteActivity

import com.eazy.daikou.model.hr.*
import com.eazy.daikou.model.profile.User
import com.eazy.daikou.ui.home.hrm.adapter.*

class ProfileEmployeeActivity : BaseActivity() {

    private lateinit var btnBack: TextView
    private lateinit var firstNameTv: TextView
    private lateinit var lastNameTv: TextView
    private lateinit var fullNameTv: TextView
    private lateinit var fullNameKhmerTv: TextView
    private lateinit var genderTv: TextView
    private lateinit var employeeIdTv: TextView
    private lateinit var idNameTv: TextView
    private lateinit var idNumberTv: TextView
    private lateinit var idIssueDateTv: TextView
    private lateinit var dateOfBirthTv: TextView
    private lateinit var placeOfBirthTv: TextView
    private lateinit var religionTv: TextView
    private lateinit var motherLangTv: TextView
    private lateinit var activationStatusTv: TextView

    //contact information
    private lateinit var emailTv: TextView
    private lateinit var phoneNumberTv: TextView
    private lateinit var emergencyTv: TextView
    private lateinit var telegramTv: TextView
    private lateinit var whatsappTv: TextView
    private lateinit var weChatTv: TextView
    private lateinit var webSiteTv: TextView
    private lateinit var presentAddressTv: TextView
    private lateinit var permanentTv: TextView

    //employee information
    private lateinit var designationTv: TextView
    private lateinit var employeeConditionTv: TextView
    private lateinit var departmentTv: TextView
    private lateinit var directManagerTv: TextView
    private lateinit var joiningDatingTv: TextView
    private lateinit var endConditionDateTv: TextView
    private lateinit var nssfNoTv: TextView
    private lateinit var foreignTv: TextView
    private lateinit var shiftTv: TextView
    private lateinit var currencyTv: TextView
    private lateinit var paymentMethodTv: TextView
    private lateinit var visaExpiryDateTv: TextView
    private lateinit var visaTypeTv: TextView

    //bank information
    private lateinit var accountNoTv: TextView
    private lateinit var accountNameTv: TextView
    private lateinit var bankTv: TextView
    private lateinit var branchTv: TextView
    private lateinit var branchAddressTv: TextView
    private lateinit var ibanTv: TextView
    private lateinit var swiftCodeTv: TextView

    private lateinit var linearBackground: LinearLayout
    private lateinit var linearContact: LinearLayout
    private lateinit var linearEmployee: LinearLayout
    private lateinit var linearBank: LinearLayout
    private lateinit var linearQualification: LinearLayout
    private lateinit var linearExperience: LinearLayout
    private lateinit var linearRelationship: LinearLayout
    private lateinit var linearDocument: LinearLayout


    private lateinit var recyclerViewExperience: RecyclerView
    private lateinit var experienceAdapter: ItemExperienceAdapter
    private lateinit var recyclerViewQualification: RecyclerView
    private lateinit var qualificationAdapter: ItemQualificationAdapter
    private lateinit var recyclerViewRelationship: RecyclerView
    private lateinit var relationshipAdapter: ItemRelationshipAdapter
    private lateinit var recyclerViewDocument: RecyclerView
    private lateinit var documentAdapter: ItemDocumentAdapter

    private lateinit var viewContact: LinearLayout
    private lateinit var viewEmployee: LinearLayout
    private lateinit var viewBank: LinearLayout
    private lateinit var viewBackGround: LinearLayout

    //Image ProfileHRM
    private lateinit var imageProfileHRM: ImageView
    private lateinit var nameProfileTv: TextView
    private lateinit var loading : ProgressBar
    private lateinit var imageCoverProfile : ImageView

    private lateinit var actionNextBackGroundTV: TextView
    private lateinit var actionDropBackGroundTV: TextView
    private lateinit var actionNextContactTV: TextView
    private lateinit var actionDropContactTV: TextView
    private lateinit var actionNextEmployeeTV: TextView
    private lateinit var actionDropEmployeeTV: TextView
    private lateinit var actionNextBankTv: TextView
    private lateinit var actionDropBankTv: TextView

    private lateinit var actionNextQualification: TextView
    private lateinit var actionDropQualification: TextView
    private lateinit var actionNextExperience: TextView
    private lateinit var actionDropExperience: TextView
    private lateinit var actionNextRelationship: TextView
    private lateinit var actionDropRelationship: TextView
    private lateinit var actionNextDocument: TextView
    private lateinit var actionDropDocument: TextView

    private var isClick = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_employee)

        initView()

        initData()

        initAction()

    }

    private fun initView() {

        btnBack = findViewById(R.id.btn_back)
        loading = findViewById(R.id.progressItem)

        recyclerViewQualification = findViewById(R.id.list_qualification)
        recyclerViewExperience = findViewById(R.id.list_experience)
        recyclerViewRelationship = findViewById(R.id.list_relationship)
        recyclerViewDocument = findViewById(R.id.list_document)

        linearBackground = findViewById(R.id.linear_background)
        linearContact = findViewById(R.id.linear_contact_information)
        linearEmployee = findViewById(R.id.linear_employee_information)
        linearBank = findViewById(R.id.linear_bank_information)
        linearQualification = findViewById(R.id.linear_qualification)
        linearDocument = findViewById(R.id.linear_document)
        linearRelationship = findViewById(R.id.linear_relationship)
        linearExperience = findViewById(R.id.linear_experience)

        viewBackGround = findViewById(R.id.view_0)
        viewContact = findViewById(R.id.view_1)
        viewEmployee = findViewById(R.id.view_2)
        viewBank = findViewById(R.id.view_3)

        //background
        firstNameTv = findViewById(R.id.first_name)
        lastNameTv = findViewById(R.id.last_name)
        fullNameTv = findViewById(R.id.full_name)
        fullNameKhmerTv = findViewById(R.id.full_name_khmer)
        genderTv = findViewById(R.id.gender)
        employeeIdTv = findViewById(R.id.employee_id)
        idNameTv = findViewById(R.id.id_name)
        idNumberTv = findViewById(R.id.id_number)
        idIssueDateTv = findViewById(R.id.id_issue_date)
        dateOfBirthTv = findViewById(R.id.date_of_birth)
        placeOfBirthTv = findViewById(R.id.place_of_birth)
        religionTv = findViewById(R.id.religion)
        motherLangTv = findViewById(R.id.mother_language)
        activationStatusTv = findViewById(R.id.activation_status)
       //accountTv = findViewById(R.id.account)

        //contact information
        emailTv = findViewById(R.id.email)
        phoneNumberTv = findViewById(R.id.phone_number)
        emergencyTv = findViewById(R.id.emergency_contact)
        telegramTv = findViewById(R.id.telegram)
        whatsappTv = findViewById(R.id.whatsapp)
        webSiteTv = findViewById(R.id.website)
        weChatTv = findViewById(R.id.wechat)
        presentAddressTv = findViewById(R.id.present_address)
        permanentTv = findViewById(R.id.permanent_address)
        //employee information
        designationTv = findViewById(R.id.designation)
        employeeConditionTv = findViewById(R.id.employee_condition)
        departmentTv = findViewById(R.id.department)
        directManagerTv = findViewById(R.id.direct_manager)
        joiningDatingTv = findViewById(R.id.joining_date)
        endConditionDateTv = findViewById(R.id.end_condition_date)
        nssfNoTv = findViewById(R.id.nssf_no)
        shiftTv = findViewById(R.id.shift)
        foreignTv = findViewById(R.id.foreign_title)
        currencyTv = findViewById(R.id.currency)
        paymentMethodTv = findViewById(R.id.payment_method)
        visaExpiryDateTv = findViewById(R.id.visa_expiry_date)
        visaTypeTv = findViewById(R.id.visa_type)
        //bank information
        accountNoTv =  findViewById(R.id.account_no)
      //  accountTv = findViewById(R.id.account)
        accountNameTv = findViewById(R.id.account_name)
        bankTv = findViewById(R.id.bank)
        branchTv = findViewById(R.id.branch)
        branchAddressTv = findViewById(R.id.branch_address)
        ibanTv = findViewById(R.id.iban)
        swiftCodeTv = findViewById(R.id.swift_code)
        //ImageProfile
        imageProfileHRM = findViewById(R.id.imageProfileHRM)
        nameProfileTv = findViewById(R.id.nameProfileTv)
        imageCoverProfile = findViewById(R.id.img_cover_profile)

        actionNextBackGroundTV = findViewById(R.id.actionNextBackGround)
        actionDropBackGroundTV = findViewById(R.id.actionDropBackground)
        actionNextContactTV = findViewById(R.id.actionNextContact)
        actionDropContactTV = findViewById(R.id.actionDropContact)
        actionNextEmployeeTV = findViewById(R.id.actionNextEmployee)
        actionDropEmployeeTV = findViewById(R.id.actionDropEmployee)
        actionNextBankTv = findViewById(R.id.actionNextBank)
        actionDropBankTv = findViewById(R.id.actionDropBank)

        actionNextQualification = findViewById(R.id.actionNextQualification)
        actionDropQualification = findViewById(R.id.actionDropQualification)
        actionNextExperience = findViewById(R.id.actionNextExperience)
        actionDropExperience = findViewById(R.id.actionDropExperience)
        actionNextRelationship = findViewById(R.id.actionNextRelationship)
        actionDropRelationship = findViewById(R.id.actionDropRelationship)
        actionNextDocument = findViewById(R.id.actionNextDocument)
        actionDropDocument = findViewById(R.id.actionDropDocument)

    }

    private fun  initData(){

        //View profile to get image and user name
        loadViewProfile()

        //getValue
        MyProfileHRWs().getMyProfileEmployeeHr(this, MockUpData.userBusinessKey(UserSessionManagement(this)), callBackList)

    }

    private fun initAction() {

        btnBack.setOnClickListener { finish() }

        linearBackground.setOnClickListener {
            isClick = MockUpData.getIsCheck()
            loadView(viewBackGround,actionDropBackGroundTV,actionNextBackGroundTV)
        }

        linearContact.setOnClickListener {
            isClick = MockUpData.getIsCheck()
            loadView(viewContact,actionDropContactTV,actionNextContactTV)
        }

        linearEmployee.setOnClickListener {

            isClick = MockUpData.getIsCheck()
            loadView(viewEmployee,actionDropEmployeeTV,actionNextEmployeeTV)
        }

        linearBank.setOnClickListener {
            isClick = MockUpData.getIsCheck()
            loadView(viewBank,actionDropBankTv,actionNextBankTv)
        }

        linearQualification.setOnClickListener {
            isClick = MockUpData.getIsCheck()
            loadViewRecycleView(recyclerViewQualification,actionDropQualification,actionNextQualification)

        }

        linearExperience.setOnClickListener {
            isClick = MockUpData.getIsCheck()
            loadViewRecycleView(recyclerViewExperience,actionDropExperience,actionNextExperience)
        }

        linearRelationship.setOnClickListener {
            isClick = MockUpData.getIsCheck()
            loadViewRecycleView(recyclerViewRelationship,actionDropRelationship,actionNextRelationship)
        }

        linearDocument.setOnClickListener {
            isClick = MockUpData.getIsCheck()
            loadViewRecycleView(recyclerViewDocument,actionDropDocument,actionNextDocument)
        }
    }

    private var callBackList = object : MyProfileHRWs.OnCallBackMyProfileListener{
        override fun onLoadListSuccessFull(hrUser: HRUser) {
            loading.visibility = View.GONE

            addDataForUser(hrUser)
        }
        override fun onLoadFail(message: String) {
            loading.visibility = View.GONE
            Utils.customToastMsgError(this@ProfileEmployeeActivity, message, false)
        }
    }

    private fun loadViewProfile() {
        loading.visibility = View.VISIBLE
        ProfileWs().viewProfile(this,BaseActivity.DEVICE_ID, "daikou" , callBackProfile)
    }

    private val callBackProfile: ProfileWs.CallBackListener = object : ProfileWs.CallBackListener {
        override fun updateSuccessLoadImage(message: String) {
            loading.visibility = View.GONE
        }

        override fun onMessageFalse(msg: String) {
            loading.visibility = View.GONE
        }

        override fun onSuccessViewProfile(user: User) {
            loading.visibility = View.GONE
            nameProfileTv.text = user.name

            if(user.coverImage!=null){
                Glide.with(this@ProfileEmployeeActivity).load(user.coverImage).into(imageCoverProfile)
            }else{
                Glide.with(this@ProfileEmployeeActivity).load(R.drawable.no_image).into(imageCoverProfile)
            }

        }

        override fun onFailed(error: String) {
            loading.visibility = View.GONE
            Utils.customToastMsgError(this@ProfileEmployeeActivity, error, false)
        }
    }
    private fun loadView(linearLayout: LinearLayout,textView1: TextView,textView2: TextView) {
        if (linearLayout.visibility == View.GONE){
            textView1.visibility = View.GONE
            textView2.visibility = View.VISIBLE
            linearLayout.visibility = View.VISIBLE
        } else {
            linearLayout.visibility = View.GONE
            textView1.visibility = View.VISIBLE
            textView2.visibility = View.GONE
        }
    }

    private fun loadViewRecycleView(recyclerView: RecyclerView,textView1: TextView,textView2: TextView) {

        if (recyclerView.visibility == View.GONE){
            textView1.visibility = View.GONE
            textView2.visibility = View.VISIBLE
            recyclerView.visibility = View.VISIBLE
        } else{
            recyclerView.visibility = View.GONE
            textView1.visibility = View.VISIBLE
            textView2.visibility = View.GONE
        }
    }
    @SuppressLint("SetTextI18n")
    private fun addDataForUser(hrUserModel:HRUser ) {

        if (hrUserModel.profile_detail != null) {

            Glide.with(this@ProfileEmployeeActivity).load(hrUserModel.profile_detail!!.avatar).into(imageProfileHRM)

            if (hrUserModel.profile_detail!!.first_name != null) {
                firstNameTv.text = hrUserModel.profile_detail!!.first_name
            } else {
                firstNameTv.text = "- - - - -"
            }
            if (hrUserModel.profile_detail!!.last_name != null) {
                lastNameTv.text = hrUserModel.profile_detail!!.last_name
            } else {
                lastNameTv.text = "- - - - -"
            }
            if (hrUserModel.profile_detail!!.name != null) {
                fullNameTv.text = hrUserModel.profile_detail!!.name
            } else {
                fullNameTv.text = "- - - - -"
            }
            if (hrUserModel.profile_detail!!.first_name_native != null) {
                fullNameKhmerTv.text = hrUserModel.profile_detail!!.first_name_native + " " +
                        hrUserModel.profile_detail!!.last_name_native
            } else {
                fullNameKhmerTv.text = "- - - - -"
            }
            if (hrUserModel.profile_detail!!.gender != null) {
                genderTv.text = hrUserModel.profile_detail!!.gender
            } else {
                genderTv.text = "- - - - -"
            }
            if ( hrUserModel.profile_detail!!.employee_id != null) {
                employeeIdTv.text = hrUserModel.profile_detail!!.employee_id
            } else {
                employeeIdTv.text = "- - - - -"
            }
            if (hrUserModel.profile_detail!!.id_name != null) {
                idNameTv.text = hrUserModel.profile_detail!!.id_name
            } else {
                idNameTv.text = "- - - - -"
            }
            if (hrUserModel.profile_detail!!.id_number != null) {
                idNumberTv.text = hrUserModel.profile_detail!!.id_number
            } else {
                idNumberTv.text = "- - - - -"
            }
            if (hrUserModel.profile_detail!!.id_issue_date != null) {
                idIssueDateTv.text = hrUserModel.profile_detail!!.id_issue_date
            } else {
                idIssueDateTv.text = "- - - - -"
            }
            if (hrUserModel.profile_detail!!.date_of_birth != null) {
                dateOfBirthTv.text = hrUserModel.profile_detail!!.date_of_birth
            } else {
                dateOfBirthTv.text = "- - - - -"
            }
            if (hrUserModel.profile_detail!!.pob != null) {
                placeOfBirthTv.text = hrUserModel.profile_detail!!.pob
            } else {
                placeOfBirthTv.text = "- - - - -"
            }
            if (hrUserModel.profile_detail!!.religion != null) {
                religionTv.text = hrUserModel.profile_detail!!.religion
            } else {
                religionTv.text = "- - - - -"
            }
            if (hrUserModel.profile_detail!!.mother_language != null) {
                motherLangTv.text = hrUserModel.profile_detail!!.mother_language
            } else {
                motherLangTv.text = "- - - - -"
            }
            if ( hrUserModel.profile_detail!!.activation_status != null) {
                activationStatusTv.text = hrUserModel.profile_detail!!.activation_status

                if  (hrUserModel.profile_detail!!.activation_status == "Active"){
                    activationStatusTv.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(this, R.color.greenSea))
                } else {
                    activationStatusTv.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(this, R.color.red))
                }

            } else {
                activationStatusTv.text = "- - - - -"
            }

            if ( hrUserModel.profile_detail!!.email != null) {
                emailTv.text = hrUserModel.profile_detail!!.email
            } else {
                emailTv.text = "- - - - -"
            }
            if ( hrUserModel.profile_detail!!.contact_no_one != null) {
                phoneNumberTv.text = hrUserModel.profile_detail!!.contact_no_one
            } else {
                phoneNumberTv.text = "- - - - -"
            }
            if ( hrUserModel.profile_detail!!.emergency_contact != null) {
                emergencyTv.text = hrUserModel.profile_detail!!.emergency_contact
            } else {
                emergencyTv.text = "- - - - -"
            }
            if ( hrUserModel.profile_detail!!.telegram != null) {
                telegramTv.text = hrUserModel.profile_detail!!.telegram
            } else {
                telegramTv.text = "- - - - -"
            }
            if ( hrUserModel.profile_detail!!.whatsapp != null) {
                whatsappTv.text = hrUserModel.profile_detail!!.whatsapp
            } else {
                whatsappTv.text = "- - - - -"
            }
            if ( hrUserModel.profile_detail!!.wechat != null) {
                weChatTv.text = hrUserModel.profile_detail!!.wechat
            } else {
                weChatTv.text = "- - - - -"
            }
            if ( hrUserModel.profile_detail!!.web != null) {
                webSiteTv.text = hrUserModel.profile_detail!!.web
            } else {
                webSiteTv.text = "- - - - -"
            }
            if (hrUserModel.profile_detail!!.present_address != null) {
                presentAddressTv.text = hrUserModel.profile_detail!!.present_address
            } else {
                presentAddressTv.text = "- - - - -"
            }
            if (hrUserModel.profile_detail!!.permanent_address != null) {
                permanentTv.text = hrUserModel.profile_detail!!.permanent_address
            } else {
                permanentTv.text = "- - - - -"
            }
            if (hrUserModel.profile_detail!!.designation != null) {
                designationTv.text = hrUserModel.profile_detail!!.designation
            } else {
                designationTv.text = "- - - - -"
            }
            if (hrUserModel.profile_detail!!.employee_condition != null) {
                employeeConditionTv.text = hrUserModel.profile_detail!!.employee_condition
            } else {
                employeeConditionTv.text = "- - - - -"
            }
            if (hrUserModel.profile_detail!!.direct_manager != null) {
                directManagerTv.text = hrUserModel.profile_detail!!.direct_manager
            } else {
                directManagerTv.text = "- - - - -"
            }
            if (hrUserModel.profile_detail!!.joining_date != null){
                joiningDatingTv.text = hrUserModel.profile_detail!!.joining_date
            } else{
                joiningDatingTv.text = "- - - - -"
            }
            if (hrUserModel.profile_detail!!.end_condition_date != null){
                endConditionDateTv.text = hrUserModel.profile_detail!!.end_condition_date
            } else{
                endConditionDateTv.text = "- - - - -"
            }
            if (hrUserModel.profile_detail!!.nssf_no != null){
                nssfNoTv.text = hrUserModel.profile_detail!!.nssf_no
            } else{
                nssfNoTv.text = "- - - - -"
            }
            if (hrUserModel.profile_detail!!.shift != null){
                shiftTv.text = hrUserModel.profile_detail!!.shift
            } else{
                shiftTv.text = "- - - - -"
            }
            if (hrUserModel.profile_detail!!.foreign_title != null){
                foreignTv.text = hrUserModel.profile_detail!!.foreign_title
            } else{
                foreignTv.text = "- - - - -"
            }
            if (hrUserModel.profile_detail!!.currency != null){
                currencyTv.text = hrUserModel.profile_detail!!.currency
            } else{
                currencyTv.text = "- - - - -"
            }
            if (hrUserModel.profile_detail!!.payment_method != null){
                paymentMethodTv.text = hrUserModel.profile_detail!!.payment_method
            } else{
                paymentMethodTv.text = "- - - - -"
            }
            if (hrUserModel.profile_detail!!.visa_expiry_date != null){
                visaExpiryDateTv.text = hrUserModel.profile_detail!!.visa_expiry_date
            } else{
                visaExpiryDateTv.text = "- - - - -"
            }
            if (hrUserModel.profile_detail!!.visa_type != null){
                visaTypeTv.text = hrUserModel.profile_detail!!.visa_type
            } else{
                visaTypeTv.text = "- - - - -"
            }
        }
        /// information Bank
        if (hrUserModel.bank_detail != null) {
            if (hrUserModel.bank_detail!!.account_no != null){
                accountNoTv.text = hrUserModel.bank_detail!!.account_no
            }else{
                accountNoTv.text = "- - - - -"
            }
            if (hrUserModel.bank_detail!!.account_name != null){
                accountNameTv.text = hrUserModel.bank_detail!!.account_name
            }else{
                accountNameTv.text = "- - - - -"
            }
            if (hrUserModel.bank_detail!!.bank != null){
                bankTv.text = hrUserModel.bank_detail!!.bank
            }else{
                bankTv.text = "- - - - -"
            }
            if (hrUserModel.bank_detail!!.branch != null){
                branchTv.text = hrUserModel.bank_detail!!.branch
            }else{
                branchTv.text = "- - - - -"
            }
            if (hrUserModel.bank_detail!!.branch_addr != null){
                branchAddressTv.text = hrUserModel.bank_detail!!.branch_addr
            }else{
                branchAddressTv.text = "- - - - -"
            }
            if (hrUserModel.bank_detail!!.iban != null){
                ibanTv.text = hrUserModel.bank_detail!!.iban
            }else{
                ibanTv.text = "- - - - -"
            }
            if (hrUserModel.bank_detail!!.swift_code != null){
                swiftCodeTv.text = hrUserModel.bank_detail!!.swift_code
            }else{
                swiftCodeTv.text = "- - - - -"
            }
        }

        //AddQualification
        addQualification(hrUserModel.qualification_details)

        //AddExperience
        addExperience(hrUserModel.experience)

        //AddRelationship
        addRelationship(hrUserModel.relationship)

        //AddDocument
        addDocument(hrUserModel.document_detail)
    }

    private fun addQualification(qualificationList: ArrayList<Qualification>) {
        val list = ArrayList<Qualification>()
        list.addAll(qualificationList)
        val layoutManager = LinearLayoutManager(this)
        recyclerViewQualification.layoutManager = layoutManager
        qualificationAdapter = ItemQualificationAdapter(list)
        recyclerViewQualification.adapter = qualificationAdapter
    }

    private fun addExperience(experienceList: ArrayList<Experience>) {
        val list = ArrayList<Experience>()
        list.addAll(experienceList)
        val layoutManager = LinearLayoutManager(this)
        recyclerViewExperience.layoutManager = layoutManager
        experienceAdapter = ItemExperienceAdapter(list)
        recyclerViewExperience.adapter = experienceAdapter
    }

    private fun addRelationship(relationshipList: ArrayList<Relationship>) {
        val list = ArrayList<Relationship>()
        list.addAll(relationshipList)
        val layoutManager = LinearLayoutManager(this)
        recyclerViewRelationship.layoutManager = layoutManager
        relationshipAdapter = ItemRelationshipAdapter(list)
        recyclerViewRelationship.adapter = relationshipAdapter
    }

    private fun addDocument(documentList: ArrayList<Document>) {
        val list = ArrayList<Document>()
        list.addAll(documentList)
        val layoutManager = LinearLayoutManager(this)
        recyclerViewDocument.layoutManager = layoutManager
        documentAdapter = ItemDocumentAdapter(list, this, itemClickDocument)
        recyclerViewDocument.adapter = documentAdapter
    }

    private val itemClickDocument = object : ItemDocumentAdapter.ItemClickOnListener {
        override fun onItemClick(document: Document) {

            if (document.filename != null && document.filename.contains(".pdf")) {
                Utils.openDefaultPdfView(this@ProfileEmployeeActivity, document.filename)
            } else {
                if (document.filename != null) {
                    if (document.filename.contains(".png") || document.filename.contains(".jpg") || document.filename.contains(".jpeg")) {
                        val intent =Intent(this@ProfileEmployeeActivity, WebsiteActivity::class.java)
                        intent.putExtra("linkUrlNews", document.filename)
                        startActivity(intent)
                    } else {
                        Toast.makeText(this@ProfileEmployeeActivity, resources.getText(R.string.not_available_website), Toast.LENGTH_SHORT).show()
                    }
                } else {
                    Toast.makeText(this@ProfileEmployeeActivity, resources.getText(R.string.not_available_website), Toast.LENGTH_SHORT).show()
                }
            }
        }
    }
}