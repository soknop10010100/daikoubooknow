package com.eazy.daikou.ui.home.service_provider

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.eazy.daikou.R

class ServiceProviderMyListFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_my_list_service_provider, container, false)

        initView()

        initAction()

        return view
    }
    private fun initView(){

    }
    private fun initAction(){

    }

}