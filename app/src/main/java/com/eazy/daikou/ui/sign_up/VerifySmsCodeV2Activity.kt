package com.eazy.daikou.ui.sign_up

import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.TextView
import android.widget.Toast
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.base.MockUpData
import com.eazy.daikou.request_data.request.profile_ws.ProfileWs
import com.eazy.daikou.request_data.request.sign_up_v2.SignUpV2Ws
import com.eazy.daikou.helper.AppAlertCusDialog
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.ui.LoginActivity
import com.poovam.pinedittextfield.SquarePinField
import java.util.*

class VerifySmsCodeV2Activity : BaseActivity() {

    private lateinit var pinCode :SquarePinField
    private lateinit var btnResentTv:TextView
    private lateinit var progressBar: View
    private lateinit var phoneTv :TextView
    private lateinit var btnSubmit:View
    private val hashMapVerifyPin = HashMap<String,Any>()
    private var hashMapPinCode   = HashMap<String,Any>()
    private var action = ""
    private var phoneNumber = ""

    private var phoneAndEmail = ""
    private var phoneCountryCode = ""
    private var verifyPinCode = ""
    private var phoneCode = ""

    private lateinit var app: AppAlertCusDialog
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_verify_sms_code_v2)

        initDataIntent()

        initView()

        countTime()

        initAction()
    }



    private fun initDataIntent(){
        Utils.customOnToolbar(this, resources.getString(R.string.verification_code)) { finish() }
        app = AppAlertCusDialog()
        // get phone number
        if (intent != null && intent.hasExtra("phone_country_code")) {
            phoneCountryCode = intent.getStringExtra("phone_country_code").toString()
        }
        if (intent != null && intent.hasExtra("phone")) {
            phoneNumber = intent.getStringExtra("phone").toString()
        }
        if (intent != null && intent.hasExtra("email_and_phone")) {
            phoneAndEmail = intent.getStringExtra("email_and_phone").toString()
        }
        // get verify code
        if (intent != null && intent.hasExtra("pin_code")) {
            verifyPinCode = intent.getStringExtra("pin_code").toString()
        }
        //get action
        if (intent != null && intent.hasExtra("action")) {
            action = intent.getStringExtra("action").toString()
        }
        // get hash map pin code
        if (intent != null && intent.hasExtra("hashmap_request_code")) {
            hashMapPinCode = intent.getSerializableExtra("hashmap_request_code") as HashMap<String, Any>
        }

        // get phone code
        if (intent.hasExtra("phone_code")) {
           phoneCode = intent.getStringExtra("phone_code").toString()
        }

    }

    private fun initView() {
        pinCode = findViewById(R.id.pin_code)
        btnResentTv = findViewById(R.id.tv_resend_code)
        progressBar = findViewById(R.id.progressItem)
        progressBar.visibility = View.GONE
        phoneTv = findViewById(R.id.phone_text)
        btnSubmit = findViewById(R.id.btn_submit_verify_code)

        phoneTv.text = phoneTv.text.toString() + "( +$phoneCode ${phoneNumber.subSequence(phoneCode.length, phoneNumber.length)} )"
    }


    private fun initAction()
    {

        pinCode.addTextChangedListener(object :TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(editable: Editable?) {
                hashMapVerifyPin["verify_code"] = editable.toString()
                if (editable.toString().length == 6)
                {
                    Utils.hideSoftKeyboard(pinCode)
                    //verify code == user input
                    if (verifyPinCode.equals(editable.toString(), true))
                    {
                        Utils.logDebug("f8888", "pin code matched")
                        if (action.equals("forget_password", true))
                        {
                            val intent = intent
                            intent.putExtra("pin_code", verifyPinCode)
                            Utils.customToastMsgError(
                                this@VerifySmsCodeV2Activity,
                                resources.getString(R.string.verify_code_email_have_sent_to_user_successfully),
                                true
                            )
                            setResult(RESULT_OK, intent)
                            finish()
                        }
                        else {
                             createUser()
                        }
                    }
                    else
                    {
                        Objects.requireNonNull<Editable>(pinCode.text).clear()
                        Utils.customToastMsgError(
                            this@VerifySmsCodeV2Activity,
                            resources.getString(R.string.invalid_code_verify),
                            false
                        )
                    }
                }
            }
        })
    }

    private fun countTime() {
        object : CountDownTimer(180000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                val min = millisUntilFinished / 60000 % 60
                val sec = millisUntilFinished / 1000 % 60
                btnResentTv.text = String.format(
                    Locale.US,
                    "%s %s : %s",
                    resources.getString(R.string.available_in),
                    min,
                    sec
                )
                btnResentTv.isEnabled = false
            }

            override fun onFinish() {
                btnResentTv.setText(R.string.resend_code)
                btnResentTv.isEnabled = true
                btnResentTv.setOnClickListener{
                    //get new pin code
                    resentPinCode()
                }
            }
        }.start()
    }

    private fun createUser() {
        progressBar.visibility = View.VISIBLE
        hashMapVerifyPin.putAll(MockUpData.getHashMapAccount())

        SignUpV2Ws.createUser(this@VerifySmsCodeV2Activity,hashMapVerifyPin,object :SignUpV2Ws.OnCallBackSignUP{
            override fun onSuccess(msg: String) {
                progressBar.visibility = View.GONE
                Toast.makeText(this@VerifySmsCodeV2Activity,msg,Toast.LENGTH_SHORT).show()
                startActivity(Intent(this@VerifySmsCodeV2Activity,LoginActivity::class.java))
            }

            override fun onError(msg: String) {
                progressBar.visibility = View.GONE
                Toast.makeText(this@VerifySmsCodeV2Activity,msg,Toast.LENGTH_SHORT).show()
            }

        })
    }


    private fun resentPinCode()
    {
        progressBar.visibility = View.VISIBLE
        ProfileWs().sendAlertVerifyPinCodeEmailPhone(
            this@VerifySmsCodeV2Activity,
             hashMapPinCode,
            object :ProfileWs.CallBackUpdateEmailPhoneListener{
                override fun onSuccessVerifyCode(pinCode: String?) {
                    countTime()
                    verifyPinCode = pinCode!!
                    progressBar.visibility = View.GONE
                }

                override fun onFailed(error: String?) {
                    progressBar.visibility = View.GONE
                    Toast.makeText(this@VerifySmsCodeV2Activity,error,Toast.LENGTH_SHORT).show()
                }

            }
        )
    }

}