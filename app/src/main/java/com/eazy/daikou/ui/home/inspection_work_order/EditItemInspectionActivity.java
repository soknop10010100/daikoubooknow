package com.eazy.daikou.ui.home.inspection_work_order;

import androidx.appcompat.app.AlertDialog;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.eazy.daikou.R;
import com.eazy.daikou.base.BaseActivity;
import com.eazy.daikou.base.MockUpData;
import com.eazy.daikou.request_data.request.inspection_ws.InspectionWS;
import com.eazy.daikou.request_data.static_data.StaticUtilsKey;
import com.eazy.daikou.helper.UserSessionManagement;
import com.eazy.daikou.helper.Utils;
import com.eazy.daikou.model.inspection.ItemTemplateAPIModel;
import com.eazy.daikou.model.inspection.ItemTemplateModel;
import com.eazy.daikou.ui.home.inspection_work_order.adapter.EditItemInspectionAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

public class EditItemInspectionActivity extends BaseActivity {

    private RecyclerView recyclerView;
    private EditText inspectionNameEd;
    private ItemTemplateModel itemTemplateModel;
    private ImageView iconAdd, iconAddImage;
    private EditItemInspectionAdapter editItemInspectionAdapter;
    private ProgressBar progressBar;
    private LinearLayout btnSave;
    private ItemTemplateModel.SubItemTemplateModel backUpSubItemTemplateModel;
    private TextView optionTypeTv;

    private int count = 0;
    private final List<ItemTemplateModel.SubItemTemplateModel> subInspectionAllList = new ArrayList<>();
    private final List<ItemTemplateModel.SubItemTemplateModel> backUpSubInspectionAllList = new ArrayList<>();
    private List<ItemTemplateModel> itemTemplateModelList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_item_inspection_activity);

        initView();

        initData();

        initAction();

    }

    private void initView(){

        progressBar = findViewById(R.id.progressItem);
        progressBar.setVisibility(View.GONE);
        btnSave = findViewById(R.id.layoutMap);
        btnSave.setVisibility(View.VISIBLE);
        ImageView iconSave = findViewById(R.id.addressMap);
        iconSave.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.icons_save, null));
        ImageView iconBack = findViewById(R.id.iconBack);
        TextView titleToolbar = findViewById(R.id.titleToolbar);

        titleToolbar.setText(StaticUtilsKey.isActionMenuHome.equalsIgnoreCase(StaticUtilsKey.inspection_action) ?
                            getResources().getString(R.string.edit_inspection_form).toUpperCase(Locale.ROOT) : getResources().getString(R.string.edit_maintenance_form).toUpperCase(Locale.ROOT));

        iconBack.setOnClickListener(v -> onBackFinish(false));

        findViewById(R.id.swipe_layout).setVisibility(View.GONE);
        findViewById(R.id.editLayoutInspection).setVisibility(View.VISIBLE);

        inspectionNameEd = findViewById(R.id.editHeadItemEdt);
        recyclerView = findViewById(R.id.subInspection);
        iconAdd = findViewById(R.id.iconAddEdit);
        iconAddImage = findViewById(R.id.iconAddImage);
        optionTypeTv = findViewById(R.id.optionTypeTv);
    }

    private void initData(){
        if (getIntent() != null && getIntent().hasExtra("itemInspectionModel")) {
            itemTemplateModel = (ItemTemplateModel) getIntent().getSerializableExtra("itemInspectionModel");
        }

        if (getIntent() != null && getIntent().hasExtra("listItemInspectionModel")){
            itemTemplateModelList = (List<ItemTemplateModel>) getIntent().getSerializableExtra("listItemInspectionModel");
        }

        btnSave.setClickable(false);
        btnSave.setEnabled(false);
        btnSave.setAlpha(0.2F);
    }

    private void initAction(){
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);

        if (itemTemplateModel.getMain_category_inspection() != null) {
            inspectionNameEd.setText(itemTemplateModel.getMain_category_inspection());
        }

        Utils.setOptionTypeInspection(this, itemTemplateModel.getType_menu(), optionTypeTv);

        subInspectionAllList.addAll(Objects.requireNonNull(MockUpData.getListSubInspection().get(itemTemplateModel.getValue())));
        backUpSubInspectionAllList.addAll(subInspectionAllList);

        editItemInspectionAdapter = new EditItemInspectionAdapter(this,itemTemplateModel, subInspectionAllList, callBackSubItemListener);
        recyclerView.setAdapter(editItemInspectionAdapter);

        count = subInspectionAllList.size();
        iconAdd.setOnClickListener(v -> addSubInspection());

        btnSave.setOnClickListener(v -> updateItemInspection());

        iconAddImage.setOnClickListener(v -> addImageInspection());
    }

    private void updateItemInspection(){
        progressBar.setVisibility(View.VISIBLE);
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("user_id", new UserSessionManagement(this).getUserId());
        hashMap.put("inspection_room_id", itemTemplateModel.getItemTemplateAPIModel().getInspectionRoomId());
        hashMap.put("inspection_name", itemTemplateModel.getMain_category_inspection());

        hashMap.put("sub_inspection_info", getSubListInfo(subInspectionAllList, true));

        hashMap.put("add_new_sub_inspection_info", getSubListInfo(subInspectionAllList, false));

        new InspectionWS().updateInspectionList(EditItemInspectionActivity.this, hashMap, callBackListener);
    }

    private void deleteInspectionList(String inspectionLevel, String inspectionId){
        progressBar.setVisibility(View.VISIBLE);
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("inspection_level", inspectionLevel);
        if (inspectionLevel.equalsIgnoreCase(StaticUtilsKey.main_key)) {
            hashMap.put("inspection_room_id", inspectionId);
        } else {
            hashMap.put("inspection_room_item_id", inspectionId);
        }
        new InspectionWS().DeleteInspectionList(EditItemInspectionActivity.this, hashMap, callBackListener);
    }

    private final InspectionWS.CallBackUpdateInspectionListener callBackListener = new InspectionWS.CallBackUpdateInspectionListener() {
        @Override
        public void onSuccessSaveInspectionList(String msg, ItemTemplateAPIModel modelList) {
            progressBar.setVisibility(View.GONE);
            Utils.customToastMsgError(EditItemInspectionActivity.this, msg, true);

            itemTemplateModel.setItemTemplateAPIModel(modelList);

            for (ItemTemplateModel.SubItemTemplateModel subItemTemplateModel : subInspectionAllList){
                subItemTemplateModel.setEdit_item(false);
            }

            for (int i = 0; i < modelList.getInspectionSubTitle().size(); i++) {
                try {
                    subInspectionAllList.get(i).setItem_room_id(modelList.getInspectionSubTitle().get(i).getRoomItemId());

                    subInspectionAllList.get(i).setReportType(modelList.getInspectionSubTitle().get(i).getReportType());
//                    subInspectionAllList.get(i).setWorkOrderNo(modelList.getInspectionSubTitle().get(i).getWorkOrderNo());
                    subInspectionAllList.get(i).setWorkOrderNoDefault(modelList.getInspectionSubTitle().get(i).getWorkOrderNoDefault());
                    subInspectionAllList.get(i).setWorkOrderSubjectDefault(modelList.getInspectionSubTitle().get(i).getWorkOrderSubjectDefault());

                } catch (Exception ignored){ }
            }

            onBackFinish(true);
        }

        @Override
        public void onSuccessDeleteInspectionList(String msg) {
            progressBar.setVisibility(View.GONE);
            Utils.customToastMsgError(EditItemInspectionActivity.this, msg, true);

            Objects.requireNonNull(MockUpData.getListSubInspection().get(itemTemplateModel.getValue())).remove(backUpSubItemTemplateModel);
            backUpSubInspectionAllList.remove(backUpSubItemTemplateModel);


            if (subInspectionAllList.size() > 0) {
                subInspectionAllList.remove(backUpSubItemTemplateModel);
                editItemInspectionAdapter = new EditItemInspectionAdapter(EditItemInspectionActivity.this, itemTemplateModel, subInspectionAllList, callBackSubItemListener);
                recyclerView.setAdapter(editItemInspectionAdapter);
            }
            itemTemplateModel.getItemTemplateAPIModel().getInspectionSubTitle().removeIf(subItemTemplateAPIModel -> backUpSubItemTemplateModel.getItem_room_id().equalsIgnoreCase(subItemTemplateAPIModel.getRoomItemId()));
        }

        @Override
        public void onFailed(String msg) {
            progressBar.setVisibility(View.GONE);
            Utils.customToastMsgError(EditItemInspectionActivity.this, msg, false);
        }
    };

    private List<HashMap<String, Object>> getSubListInfo(List<ItemTemplateModel.SubItemTemplateModel> subItemTemplateModelList, boolean isOldListSaved){
        List<HashMap<String, Object>> hashMapList = new ArrayList<>();
        for (ItemTemplateModel.SubItemTemplateModel subItemTemplateModel : subItemTemplateModelList) {
            if (isOldListSaved) {   // subItemTemplateModel.isEdit_item() = false => mean not edit yet
                if (!subItemTemplateModel.isEdit_item()) {
                    hashMapList.add(hashMapSub(subItemTemplateModel, true));
                }
            } else {
                if (subItemTemplateModel.isEdit_item()){
                    hashMapList.add(hashMapSub(subItemTemplateModel, false));
                }
            }
        }
        return hashMapList;
    }

    private HashMap<String, Object> hashMapSub (ItemTemplateModel.SubItemTemplateModel subItemTemplateModel, boolean isOldListSaved){
        HashMap<String, Object> hashMapSub = new HashMap<>();

        if (isOldListSaved) {
            hashMapSub.put("room_item_id", subItemTemplateModel.getItem_room_id());
        }

        hashMapSub.put("item_no", subItemTemplateModel.getItem_no());

        if (itemTemplateModel.getType_menu().equalsIgnoreCase(StaticUtilsKey.detail_button_key)) {
            hashMapSub.put("item_name", subItemTemplateModel.getSub_item_category() != null ? subItemTemplateModel.getSub_item_category() : "");
            hashMapSub.put("item_desc", subItemTemplateModel.getItem_description() != null ? subItemTemplateModel.getItem_description() : "");
            hashMapSub.put("item_condition", subItemTemplateModel.getItem_condition() != null ? subItemTemplateModel.getItem_condition() : "");
        } else {
            hashMapSub.put("item_name", subItemTemplateModel.getSub_item_category() != null ? subItemTemplateModel.getSub_item_category() : "");
            if (itemTemplateModel.getType_menu().equalsIgnoreCase(StaticUtilsKey.simplify_button_key)) {
                hashMapSub.put("item_desc", "Clean; Undamage; Working;");
                String itemCon = "";
                itemCon += !subItemTemplateModel.getIsClickNA().equals("") ? subItemTemplateModel.getIsClickNA().toUpperCase(Locale.ROOT) + "; " : "N/A; ";
                itemCon += !subItemTemplateModel.getClickUnDamage().equals("") ? subItemTemplateModel.getClickUnDamage().toUpperCase(Locale.ROOT) + "; " : "N/A; ";
                itemCon += !subItemTemplateModel.getClickWorking().equals("") ? subItemTemplateModel.getClickWorking().toUpperCase(Locale.ROOT) : "N/A";

                hashMapSub.put("item_condition", itemCon);
            } else {
                hashMapSub.put("item_desc", "");
                String itemDes;
                if (!subItemTemplateModel.getIsClickNA().equals("")){
                    if(subItemTemplateModel.getIsClickNA().equalsIgnoreCase(Utils.getText(this, R.string.yes).toUpperCase(Locale.ROOT))) {
                        itemDes = Utils.getText(this, R.string.yes).toUpperCase(Locale.ROOT) + "; " + "N/A" + "; " + "N/A";
                    } else if(subItemTemplateModel.getIsClickNA().equalsIgnoreCase(Utils.getText(this, R.string.no).toUpperCase(Locale.ROOT))){
                        itemDes =  "N/A" + "; " + Utils.getText(this, R.string.no).toUpperCase(Locale.ROOT) +  "; " + "N/A";
                    } else {
                        itemDes = "N/A; N/A; N/A";
                    }
                } else {
                    itemDes = "N/A; N/A; N/A";
                }
                hashMapSub.put("item_condition", itemDes);

            }
        }

       return hashMapSub;
    }

    private void addSubInspection(){
        count++;
        ItemTemplateModel.SubItemTemplateModel subItemTemplateModel = new ItemTemplateModel.SubItemTemplateModel(count + itemTemplateModel.getValue(), "BB" + count, itemTemplateModel.getType_menu(),"", "", "", true);
        subInspectionAllList.add(subItemTemplateModel);
        editItemInspectionAdapter = new EditItemInspectionAdapter(this, itemTemplateModel, subInspectionAllList, callBackSubItemListener);
        recyclerView.setAdapter(editItemInspectionAdapter);
    }

    private void addImageInspection(){
        Intent intent = new Intent(EditItemInspectionActivity.this, AddImageInspectionActivity.class);
        intent.putExtra("action", "main_action");
        intent.putExtra("item_model_main", itemTemplateModel);
        startActivity(intent);
    }

    @SuppressLint("NotifyDataSetChanged")
    private final EditItemInspectionAdapter.ClickCallBackSubItemListener callBackSubItemListener = new EditItemInspectionAdapter.ClickCallBackSubItemListener() {
        @Override
        public void onClickInspection(ItemTemplateModel.SubItemTemplateModel subItemTemplateModel) {
            Intent intent = new Intent(EditItemInspectionActivity.this, AssignWorkOrderActivity.class);
            intent.putExtra("subItemInspectionModel", subItemTemplateModel);
            startActivity(intent);
        }


        @Override
        public void onClickWriteReport(ItemTemplateModel.SubItemTemplateModel subItemTemplateModel) {
            Intent intent = new Intent(EditItemInspectionActivity.this, AddImageInspectionActivity.class);
            intent.putExtra("action", "note_action");
            intent.putExtra("item_model", subItemTemplateModel);
            intent.putExtra("item_model_main", itemTemplateModel);
            startActivity(intent);
        }

        @Override
        public void onClickAddImage(ItemTemplateModel.SubItemTemplateModel subItemTemplateModel, String keyDefine) {
            Intent intent = new Intent(EditItemInspectionActivity.this, AddImageInspectionActivity.class);
            intent.putExtra("action", "sub_category_action");
            intent.putExtra("item_model", subItemTemplateModel);
            intent.putExtra("item_model_main", itemTemplateModel);
            startActivity(intent);
        }

        @Override
        public void onClickDelete(ItemTemplateModel.SubItemTemplateModel subItemTemplateModel, String categoryKey, SwipeLayout swipe_layout) {
            backUpSubItemTemplateModel = subItemTemplateModel;
            if (!subItemTemplateModel.isEdit_item()) {  // isEdit false mean data saved, no edit
                new AlertDialog.Builder(EditItemInspectionActivity.this)
                        .setTitle(getResources().getString(R.string.confirm))
                        .setMessage(getResources().getString(R.string.are_you_sure_to_delete))
                        .setPositiveButton(getResources().getString(R.string.yes), (dialog, which) -> {
                            deleteInspectionList(StaticUtilsKey.sub_key, subItemTemplateModel.getItem_room_id());
                            dialog.dismiss();
                        })
                        .setNegativeButton(getResources().getString(R.string.no), (dialog, which) -> dialog.dismiss())
                        .setCancelable(false)
                        .setIcon(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_report_problem, null))
                        .show();
            } else {
                if (subInspectionAllList.size() > 0) {
                    subInspectionAllList.remove(subItemTemplateModel);
                    editItemInspectionAdapter = new EditItemInspectionAdapter(EditItemInspectionActivity.this, itemTemplateModel, subInspectionAllList, callBackSubItemListener);
                    recyclerView.setAdapter(editItemInspectionAdapter);
                }
            }
        }

        @SuppressLint("Range")
        @Override
        public void onWriteSubItem(ItemTemplateModel.SubItemTemplateModel itemTemplateModel, String write_text, int position) {
            setButtonEnable();
        }

        @Override
        public void onClickYesNoNA(Context context, ItemTemplateModel.SubItemTemplateModel subItemTemplateModel, String categoryKey, SwipeLayout swipe_layout, String action_click) {
            switch (action_click) {
                case "clean_click":
                    if (subItemTemplateModel.getKey_menu().equalsIgnoreCase(StaticUtilsKey.simplify_button_key)) {
                        if (subItemTemplateModel.getIsClickNA().equalsIgnoreCase(Utils.getText(context, R.string.yes).toUpperCase(Locale.ROOT))) {
                            subItemTemplateModel.setIsClickNA(Utils.getText(context, R.string.no));
                        } else if (subItemTemplateModel.getIsClickNA().equalsIgnoreCase(Utils.getText(context, R.string.no))) {
                            subItemTemplateModel.setIsClickNA("N/A");
                        } else {
                            subItemTemplateModel.setIsClickNA(Utils.getText(context, R.string.yes).toUpperCase(Locale.ROOT));
                        }
                    } else {
                        subItemTemplateModel.setIsClickNA(Utils.getText(context, R.string.yes).toUpperCase(Locale.ROOT));
                    }
                    break;
                case "un_damage_click":
                    if (subItemTemplateModel.getKey_menu().equalsIgnoreCase(StaticUtilsKey.simplify_button_key)) {
                        if (subItemTemplateModel.getClickUnDamage().equalsIgnoreCase(Utils.getText(context, R.string.yes))) {
                            subItemTemplateModel.setClickUnDamage(Utils.getText(context, R.string.no));
                        } else if (subItemTemplateModel.getClickUnDamage().equalsIgnoreCase(Utils.getText(context, R.string.no))) {
                            subItemTemplateModel.setClickUnDamage("N/A");
                        } else {
                            subItemTemplateModel.setClickUnDamage(Utils.getText(context, R.string.yes));
                        }
                    } else {
                        subItemTemplateModel.setIsClickNA(Utils.getText(context, R.string.no));
                    }
                    break;
                case "working_click":
                    if (subItemTemplateModel.getKey_menu().equals(StaticUtilsKey.simplify_button_key)) {
                        if (subItemTemplateModel.getClickWorking().equalsIgnoreCase(Utils.getText(context, R.string.yes))) {
                            subItemTemplateModel.setClickWorking(Utils.getText(context, R.string.no));
                        } else if (subItemTemplateModel.getClickWorking().equalsIgnoreCase(Utils.getText(context, R.string.no))) {
                            subItemTemplateModel.setClickWorking("N/A");
                        } else {
                            subItemTemplateModel.setClickWorking(Utils.getText(context, R.string.yes));
                        }
                    } else {
                        subItemTemplateModel.setIsClickNA("N/A");
                    }
                    break;
            }
            editItemInspectionAdapter = new EditItemInspectionAdapter(EditItemInspectionActivity.this, itemTemplateModel, subInspectionAllList, callBackSubItemListener);
            recyclerView.setAdapter(editItemInspectionAdapter);
        }
    };

    @SuppressLint("Range")
    private void setButtonEnable(){
        btnSave.setClickable(isEnableButton(subInspectionAllList));
        btnSave.setEnabled(isEnableButton(subInspectionAllList));
        if (isEnableButton(subInspectionAllList)){
            btnSave.setAlpha(100F);
        } else{
            btnSave.setAlpha(0.2F);
        }
    }

    private Boolean isEnableButton(List<ItemTemplateModel.SubItemTemplateModel> list){
        for(ItemTemplateModel.SubItemTemplateModel subItemTemplateModel : list){
            if (subItemTemplateModel.getSub_item_category() == null || subItemTemplateModel.getSub_item_category().equals("")){
                return false;
            }
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        onBackFinish(false);
    }

    private void onBackFinish(boolean isClickSave){
        HashMap<String, List<ItemTemplateModel.SubItemTemplateModel>> storeHashmap = new HashMap<>();
        if (isClickSave) {
            for (ItemTemplateModel listTemplate : itemTemplateModelList){
                if(!listTemplate.getValue().equals(itemTemplateModel.getValue())){
                    storeHashmap.put(listTemplate.getValue(), MockUpData.getListSubInspection().get(listTemplate.getValue()));
                } else {
                    storeHashmap.put(itemTemplateModel.getValue(), subInspectionAllList);
                }
            }
        } else {
            for (ItemTemplateModel listTemplate : itemTemplateModelList){
                if(!listTemplate.getValue().equals(itemTemplateModel.getValue())){
                    storeHashmap.put(listTemplate.getValue(), MockUpData.getListSubInspection().get(listTemplate.getValue()));
                } else {
                    for (int i = 0; i < itemTemplateModel.getItemTemplateAPIModel().getInspectionSubTitle().size(); i++){
                        if (backUpSubInspectionAllList.size() > 0) {
                            if (backUpSubInspectionAllList.get(i).getItem_room_id().equals(itemTemplateModel.getItemTemplateAPIModel().getInspectionSubTitle().get(i).getRoomItemId())) {
                                backUpSubInspectionAllList.get(i).setSub_item_category(itemTemplateModel.getItemTemplateAPIModel().getInspectionSubTitle().get(i).getItemName());
                                if (itemTemplateModel.getType_menu().equalsIgnoreCase(StaticUtilsKey.detail_button_key)){
                                    backUpSubInspectionAllList.get(i).setItem_condition(itemTemplateModel.getItemTemplateAPIModel().getInspectionSubTitle().get(i).getItemCondition());
                                    backUpSubInspectionAllList.get(i).setItem_description(itemTemplateModel.getItemTemplateAPIModel().getInspectionSubTitle().get(i).getItemDesc());
                                } else if (itemTemplateModel.getType_menu().equalsIgnoreCase(StaticUtilsKey.simplify_button_key)){
                                    String[] itemCondition = itemTemplateModel.getItemTemplateAPIModel().getInspectionSubTitle().get(i).getItemCondition().split(";");
                                    backUpSubInspectionAllList.get(i).setIsClickNA(itemCondition[0].replace(" ", ""));
                                    backUpSubInspectionAllList.get(i).setClickUnDamage(itemCondition[1].replace(" ", ""));
                                    backUpSubInspectionAllList.get(i).setClickWorking(itemCondition[2].replace(" ", ""));
                                } else if (itemTemplateModel.getType_menu().equalsIgnoreCase(StaticUtilsKey.question_button_key)){
                                    if (itemTemplateModel.getItemTemplateAPIModel().getInspectionSubTitle().get(i).getItemCondition() != null) {
                                        String itemCondition = itemTemplateModel.getItemTemplateAPIModel().getInspectionSubTitle().get(i).getItemCondition();
                                        backUpSubInspectionAllList.get(i).setIsClickNA(itemCondition);
                                    }
                                }
                            }
                        }
                    }
                    storeHashmap.put(itemTemplateModel.getValue(), backUpSubInspectionAllList);
                }
            }
        }
        MockUpData.setListSubInspection(storeHashmap);

        Intent intent = getIntent();
        intent.putExtra("item_value", itemTemplateModel);
        setResult(RESULT_OK, intent);

        finish();
    }

}