package com.eazy.daikou.ui.home.service_provider_new.adapter_service_provider

import android.content.Context
import android.media.Image
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.model.service_provider.SubCategory
import org.w3c.dom.Text

class ServiceProviderListAdapter(private val context: Context, private val serviceProviderList: ArrayList<SubCategory>): RecyclerView.Adapter<ServiceProviderListAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.custom_service_provider_list, parent, false)
        return  ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
//        val serviceList : SubCategory = serviceProviderList[position]
//        if (serviceList != null){
//
//            Glide.with(context).load(serviceList.image).into(holder.imageServiceProvider)
//            Glide.with(context).load(serviceList.image).into(holder.imageCircleCompany)
//            holder.textNameCompany.text = serviceList.nameCategory
//            holder.textService.text = serviceList.nameCategory
//            holder.serviceNameTv.text = "Service Provider"
//            holder.serviceAddressTv.text = "Phnom Penh"
//            holder.servicePriceTv.text = " $ 90900"
//        }
    }

    override fun getItemCount(): Int {
        return serviceProviderList.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view){

        var imageServiceProvider: ImageView = view.findViewById(R.id.imageServiceProvider)
        var imageCircleCompany: ImageView = view.findViewById(R.id.imageCircleCompany)
        var textNameCompany: TextView = view.findViewById(R.id.textNameCompany)
        var textService: TextView = view.findViewById(R.id.textService)
        var serviceNameTv: TextView = view.findViewById(R.id.serviceNameTv)
        var serviceAddressTv: TextView = view.findViewById(R.id.serviceAddressTv)
        var servicePriceTv: TextView = view.findViewById(R.id.servicePriceTv)

    }
}