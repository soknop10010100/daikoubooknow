package com.eazy.daikou.ui.home.facility_booking

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.cardview.widget.CardView
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.request_data.request.facility_booking_ws.FacilityBookingWs
import com.eazy.daikou.request_data.static_data.StaticUtilsKey
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.facility_booking.FacilitySpaceDetailModel
import com.eazy.daikou.model.facility_booking.SpacePricing
import com.eazy.daikou.ui.home.facility_booking.adapter.AvailableDayAdapter
import com.eazy.daikou.ui.ViewPagerItemImagesAdapter
import com.google.android.material.tabs.TabLayout
import de.hdodenhof.circleimageview.CircleImageView
import java.util.*

class FacilityBookingDetailActivity : BaseActivity() {

    private lateinit var priceVal : String
    private lateinit var imageCoverBooking : ImageView
    private lateinit var btnOverView : TextView
    private lateinit var btnPhoto : TextView
    private lateinit var noImage : TextView
    private lateinit var propertyTv : TextView
    private lateinit var titleTv : TextView
    private lateinit var descriptionTv : TextView

    private lateinit var linearOverView: LinearLayout
    private lateinit var linearPhoto: LinearLayout

    private lateinit var linearLayout: LinearLayout

    private lateinit var viewPager: ViewPager
    private lateinit var tabLayout : TabLayout
    private var viewpagerAdapter : ViewPagerItemImagesAdapter? = null

    private lateinit var btnAddToCard : CardView
    private lateinit var progressBar : ProgressBar
    private lateinit var facilityDetailModel : FacilitySpaceDetailModel
    private lateinit var availableDate : RecyclerView
    private lateinit var imgProperty : CircleImageView
    private lateinit var availableDayTv : TextView
    private lateinit var priceTv : TextView
    private lateinit var mainLayout : RelativeLayout

    private var userId = ""
    private var spaceId = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_facility_booking_detail)

        initView()

        initData()

        initAction()

    }


    private fun initView(){
        imageCoverBooking = findViewById(R.id.img_cover)
        btnOverView = findViewById(R.id.btnOverView)
        btnPhoto = findViewById(R.id.btnPhoto)

        linearLayout = findViewById(R.id.linearLayout)
        linearOverView = findViewById(R.id.linearOverView)
        linearPhoto = findViewById(R.id.linearPhoto)

        titleTv = findViewById(R.id.titleTv)
        propertyTv = findViewById(R.id.propertyTv)
        noImage = findViewById(R.id.countItem)
        viewPager = findViewById(R.id.viewPagers)
        btnAddToCard = findViewById(R.id.linear_card)
        tabLayout = findViewById(R.id.tab_layout)
        findViewById<TextView>(R.id.btn_back).setOnClickListener{finish()}
        progressBar = findViewById(R.id.progressItem)
        descriptionTv = findViewById(R.id.descriptionTv)
        availableDate = findViewById(R.id.availableDate)
        imgProperty = findViewById(R.id.imgProperty)
        availableDayTv = findViewById(R.id.availableDayTv)
        priceTv = findViewById(R.id.priceTv)
        mainLayout = findViewById(R.id.mainLayout)
        findViewById<TextView>(R.id.titleHeader).visibility = View.GONE
    }

    private fun initData(){
        if (intent != null && intent.hasExtra("data")) {
            priceVal = intent.getStringExtra("data").toString()
        }

        if (intent != null && intent.hasExtra("space_id")) {
            spaceId = intent.getStringExtra("space_id") as String
        }
        val userSessionManagement = UserSessionManagement(this)
        userId = userSessionManagement.userId

        setLinearLayout(linearLayout)
        availableDate.layoutManager = GridLayoutManager(this, 3)

        FacilityBookingWs().getSpaceFacilityDetail(this,userId, spaceId, facilityObject)

    }

    private val facilityObject = object :FacilityBookingWs.CallBackListFacilityDetailListener{
        override fun onLoadSuccessFull(selectPropertyGuests: FacilitySpaceDetailModel) {
            progressBar.visibility = View.GONE
            mainLayout.visibility = View.VISIBLE
            if (selectPropertyGuests != null) {
                titleTv.text = selectPropertyGuests.space_name
                facilityDetailModel = selectPropertyGuests
                setValue(selectPropertyGuests)

                initRecycleViewOnAvailableDay(selectPropertyGuests.space_pricing)
            }
        }

        override fun onLoadFailed(message: String?) {
            progressBar.visibility = View.GONE
            mainLayout.visibility = View.VISIBLE
            Utils.customToastMsgError(this@FacilityBookingDetailActivity, message, false)
        }

    }

    @SuppressLint("ResourceAsColor", "SetTextI18n")
    private fun initAction(){

        btnAddToCard.setOnClickListener{
            if (facilityDetailModel != null && facilityDetailModel.space_id != null) {
                val intent = Intent(this, AddToCartBookingActivity::class.java)
                intent.putExtra("facility_detail", facilityDetailModel)
                startActivity(intent)
            }
        }

        if (priceVal != null) {
            priceTv.text = "$ $priceVal"
        } else {
            priceTv.text = "$ . . ."
        }
    }

    private fun setValue(selectPropertyGuests: FacilitySpaceDetailModel){

        propertyTv.text = if (selectPropertyGuests.property != null) { if (selectPropertyGuests.property!!.name != null)  selectPropertyGuests.property!!.name else "" } else "- - -"
        descriptionTv.text = if (selectPropertyGuests.description != null) selectPropertyGuests.description else resources.getString(R.string.no_description)
        setViewPagerFacility(selectPropertyGuests.space_images)

        if (selectPropertyGuests.property != null){
            if (selectPropertyGuests.property!!.logo != null){
                Glide.with(imgProperty).load(selectPropertyGuests.property!!.logo).into(imgProperty)
            }
        }
    }

    private fun initRecycleViewOnAvailableDay(spacePricingItem : ArrayList<SpacePricing>){
        val availableDayAdapter = AvailableDayAdapter("detail", getListAvailDay(spacePricingItem))
        availableDate.adapter = availableDayAdapter

        if (getListAvailDay(spacePricingItem).isEmpty()){
            availableDayTv.append(" : (" + resources.getString(R.string.no_fee) + ")")
        }
    }

    private fun getListAvailDay(spacePricingItem : ArrayList<SpacePricing>) : List<String>{
        val list : ArrayList<String> = ArrayList()
        for (space in spacePricingItem){
            if (StaticUtilsKey.free == space.type){
                list.addAll(space.operation_day)
            }
        }
        return list
    }


    private fun setLinearLayout(linearLayout: LinearLayout){
        for (i in 0 until linearLayout.childCount) {
            linearLayout.getChildAt(i).setOnClickListener {
                setColorTextMenu(i)
                when (i) {
                    0 -> {
                        linearOverView.visibility = View.VISIBLE
                        linearPhoto.visibility = View.GONE
                    }
                    1 -> {
                        linearPhoto.visibility = View.VISIBLE
                        linearOverView.visibility = View.GONE
                    }
                }
            }
        }
    }
    @SuppressLint("ResourceAsColor")
    private fun setColorTextMenu(position: Int){
        when (position) {
            0 -> {
                btnOverView.setTextColor(getColor(R.color.white))
                btnPhoto.setTextColor(getColor(R.color.gray))
                btnOverView.background = ResourcesCompat.getDrawable(resources, R.drawable.custom_card_view_color_menu, null)
                btnPhoto.background = null
            }
            1 -> {
                btnOverView.setTextColor(getColor(R.color.gray))
                btnPhoto.setTextColor(getColor(R.color.white))
                btnPhoto.background = ResourcesCompat.getDrawable(resources, R.drawable.custom_card_view_color_menu, null)
                btnOverView.background = null
            }
        }
    }

    private fun setViewPagerFacility(image : ArrayList<String>){
        if (image.size > 0 ) {
            Glide.with(this).load(image[0]).into(imageCoverBooking)
            viewpagerAdapter = ViewPagerItemImagesAdapter(this, image)
            viewPager.adapter = viewpagerAdapter
            tabLayout.setupWithViewPager(viewPager, true)
        } else {
            noImage.visibility = View.VISIBLE
            viewPager.visibility = View.GONE
        }
    }
}