package com.eazy.daikou.ui.home.calendar;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;

import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.eazy.daikou.R;
import com.eazy.daikou.ui.home.calendar.calendar_custom.CalendarPickerView;
import com.eazy.daikou.base.BaseActivity;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;


import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import static com.eazy.daikou.helper.Utils.logDebug;


public class CalendarDetailActivity extends BaseActivity {
    private long time;
    private ImageView backBtn, addCalBtn;
    private CalendarPickerView calendarPicker;
    private  TextView titleTool;
    private final HashMap<Integer, String> year = new HashMap<>();
    private int startYear = 2001;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar_detail);

        initView();
        initData();
        initAction();

        backBtn.setOnClickListener(view -> onBackPressed());

        addCalBtn.setOnClickListener(view -> {
//            startActivity( new Intent(CalendarDetailActivity.this , CalendarAddEventActivity.class));
            Toast.makeText(this, getResources().getString(R.string.add_event) + " " +
                    getResources().getString(R.string.coming_soon), Toast.LENGTH_SHORT).show();
        });
    }

    private void initView() {
        backBtn = findViewById(R.id.iconBackCal);
        addCalBtn = findViewById(R.id.btnAddCalendarDet);
        calendarPicker = findViewById(R.id.calendar_view);
        titleTool = findViewById(R.id.title_toolBar_Det);
    }
    private void initData() {
        if (getIntent() != null && getIntent().hasExtra("time")){
            time = getIntent().getLongExtra("time",-1);
        }
    }

    private void initAction() {
        int duringDay = 365 * 50;
        calendarPicker.init( buildMinimumDate(), DateTime.now(DateTimeZone.UTC).plusDays(duringDay).toDate() )
                .inMode(CalendarPickerView.SelectionMode.RANGE)
                .withSelectedDate(new Date());

        Date currentDate;
        if (time > 0){
            currentDate = new Date(time);
        } else {
            currentDate = new Date();
        }
        calendarPicker.scrollToDate(currentDate);

        int noteMonth = 1;
        long allMonth = 70 * 12;
        for (int i = 0 ; i < allMonth; i++){
            year.put(i, getMonth(noteMonth) + "   " + startYear);
            if (noteMonth == 12){
                noteMonth = 1;
                startYear++;
            } else {
                noteMonth++;
            }
        }

//        calendarPicker.setOnInvalidDateSelectedListener(null);
//        Calendar cal = Calendar.getInstance();
//        calendarPicker.setDateSelectableFilter(new CalendarPickerView.DateSelectableFilter() {
//            @Override
//            public boolean isDateSelectable(Date date) {
//                boolean isSelectable=true;
//                cal.setTime(date);
//                int dayOfWeek=cal.get(Calendar.DAY_OF_WEEK);
//
//                if ()
//                // disable if weekend
//                if(dayOfWeek==Calendar.SATURDAY || dayOfWeek==Calendar.SUNDAY){
//                    isSelectable=false;
//                }
//                return isSelectable;
//            }
//        });


        calendarPicker.setOnScrollListener(new AbsListView.OnScrollListener() {
            private int currentVisibleItemCount;
            private int currentFirstVisibleItem;
            private int totalItem;
            private int currentScrollState;
            private boolean isScrolling = true;

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                this.currentScrollState = scrollState;
                if(scrollState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL){
                    isScrolling = true;
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                this.currentFirstVisibleItem = firstVisibleItem;
                this.currentVisibleItemCount = visibleItemCount;
                this.totalItem = totalItemCount;
                isScrolling = false;
                logDebug("testFindVisible",currentFirstVisibleItem +" "+currentVisibleItemCount+"  "+ totalItem);

                titleTool.setText(year.get(currentFirstVisibleItem));

            }
        });

        //logDebug("pos",calendarPicker.getSelectedDate()+""+calendarPicker.getSelectedDates().get(calendarPicker.getSelectedDates().size()-1));

    }
    private Date buildMinimumDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_YEAR, 1);
        calendar.set(Calendar.YEAR, startYear);
        return calendar.getTime();
    }
    private String getMonth(int i){
        HashMap<Integer , String > maps = new HashMap<>();
        maps.put(1,getString(R.string.january));
        maps.put(2,getString(R.string.february));
        maps.put(3,getString(R.string.march));
        maps.put(4,getString(R.string.april));
        maps.put(5,getString(R.string.may));
        maps.put(6,getString(R.string.june));
        maps.put(7,getString(R.string.july));
        maps.put(8,getString(R.string.august));
        maps.put(9,getString(R.string.september));
        maps.put(10,getString(R.string.october));
        maps.put(11,getString(R.string.november));
        maps.put(12,getString(R.string.december));
        return maps.get(i);
    }
}