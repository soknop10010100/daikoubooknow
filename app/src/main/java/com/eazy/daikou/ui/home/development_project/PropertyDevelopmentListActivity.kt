package com.eazy.daikou.ui.home.development_project

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.denzcoskun.imageslider.ImageSlider
import com.denzcoskun.imageslider.constants.ScaleTypes
import com.denzcoskun.imageslider.models.SlideModel
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.request_data.request.project_development_ws.ProjectDevelopmentWs
import com.eazy.daikou.helper.AbsoluteFitLayoutManager
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.home_page.home_project.*
import com.eazy.daikou.ui.home.development_project.adapter.CategoryAdapter
import com.eazy.daikou.ui.home.development_project.adapter.PropertyFeatureItemAdapter
import com.eazy.daikou.ui.home.development_project.adapter.PropertyNearByItemAdapter
import com.eazy.daikou.ui.home.development_project.adapter.RecommendItemPropertyAdapter

class PropertyDevelopmentListActivity : BaseActivity() {

    private lateinit var layoutDiscount: LinearLayout
    private lateinit var layoutNewProject: LinearLayout
    private lateinit var layoutArticle: LinearLayout

    private lateinit var progress: View
    private lateinit var scrollView: ScrollView

    private lateinit var imageSliderImageDiscount: ImageSlider

    private lateinit var layoutBody: View

    // show category
    private lateinit var recyclerViewCategory: RecyclerView

    //show near by
    private lateinit var recyclerViewNearby: RecyclerView

    //show featuring
    private lateinit var recyclerViewFeaturing: RecyclerView

    //show recommend
    private lateinit var recyclerViewRecommend: RecyclerView

    private var limit = 10
    private var currentPage = 1
    private val listRecommend = ArrayList<AllBranches>()
    private lateinit var adapter: RecommendItemPropertyAdapter
    private var isScrollMoreProject = true
    private var isRespond = true
    private lateinit var imageSlider: ImageSlider

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_properties_list)

        initView()

        initData()

        initAction()

    }

    private fun initView() {
        scrollView = findViewById(R.id.scroll_view)

        layoutDiscount = findViewById(R.id.layout_discount)
        layoutArticle = findViewById(R.id.layout_article)
        layoutNewProject = findViewById(R.id.layout_new_project)

        recyclerViewCategory = findViewById(R.id.recyclerView_category)
        recyclerViewNearby = findViewById(R.id.recyclerView_nearby)
        recyclerViewFeaturing = findViewById(R.id.recyclerView_featuring)
        recyclerViewRecommend = findViewById(R.id.recyclerView_recommend)

        progress = findViewById(R.id.progress)

        layoutBody = findViewById(R.id.body)

        imageSliderImageDiscount = findViewById(R.id.slider)

        Utils.customOnToolbar(this, resources.getString(R.string.project_development)) { finish() }

        imageSlider = findViewById(R.id.img_slider)
    }

    private fun initData() {
        recyclerViewCategory.layoutManager = GridLayoutManager(this, 3)
        recyclerViewNearby.layoutManager = AbsoluteFitLayoutManager(this, 1, RecyclerView.HORIZONTAL, false, 2)
        recyclerViewRecommend.layoutManager = LinearLayoutManager(this)
        recyclerViewFeaturing.layoutManager = AbsoluteFitLayoutManager(this, 1, RecyclerView.HORIZONTAL, false, 2)

    }

    private fun initAction() {
        findViewById<RelativeLayout>(R.id.see_more_near_by).setOnClickListener(CustomSetOnClickViewListener {
            val intent = Intent(
                this@PropertyDevelopmentListActivity,
                PropertyShowAllFromListActivity::class.java
            )
            intent.putExtra("type", getString(R.string.near_by_property))
            startActivity(intent)
        })

        findViewById<RelativeLayout>(R.id.see_more_featuring).setOnClickListener(CustomSetOnClickViewListener {
            val intent = Intent(
                this@PropertyDevelopmentListActivity,
                PropertyShowAllFromListActivity::class.java
            )
            intent.putExtra("type", getString(R.string.featuring))
            startActivity(intent)
        })

        findViewById<RelativeLayout>(R.id.see_more_recommend).setOnClickListener(CustomSetOnClickViewListener {
            val intent = Intent(
                this@PropertyDevelopmentListActivity,
                PropertyShowAllFromListActivity::class.java
            )
            intent.putExtra("type", getString(R.string.recommend_for_you))
            startActivity(intent)
        })

        requestServiceWs()

        initSliderDiscount()

        scrollView.viewTreeObserver.addOnScrollChangedListener {
            if ((scrollView.getChildAt(0).bottom == scrollView.height + scrollView.scrollY) && isScrollMoreProject && isRespond) {
                currentPage++
                isRespond = false
                //recyclerViewRecommend.scrollToPosition(listRecommend.size - 1)
                initMoreRecommend()
            }
        }

        findViewById<TextView>(R.id.titleToolbar).setOnClickListener {
            scrollView.scrollY = 0
        }

        adapter = RecommendItemPropertyAdapter(
            listRecommend,
            clickRecommend
        )
        recyclerViewRecommend.adapter = adapter
    }

    //show slider in discount

    private fun initSliderDiscount() {
        val slideModels: MutableList<SlideModel> = java.util.ArrayList()
        slideModels.add(SlideModel("https://i.pinimg.com/474x/15/0c/d7/150cd7dec5567c7c759dc0f899d5ac5a.jpg"))
        slideModels.add(SlideModel("https://free-psd-templates.com/wp-content/uploads/2018/07/free_psd_preview_last_15-free-hotel-banners-collection-in-psd.jpg"))
        slideModels.add(SlideModel("https://indiater.com/wp-content/uploads/2019/03/sea-view-hotel-miami-beach-holiday-package-banner.jpg"))

        imageSliderImageDiscount.setImageList(slideModels, ScaleTypes.CENTER_CROP)
    }

    //set image slider
    private fun setImageSlider(listImages: ArrayList<String>) {
        val slideModels = ArrayList<SlideModel>()
        for (index in listImages.indices) {
            slideModels.add(SlideModel(listImages[index].replace("\"", "")))
        }
        imageSlider.setImageList(slideModels, ScaleTypes.CENTER_CROP)
    }

    private fun initImageSlider(listImages: ArrayList<String>) {
        val newListImages = ArrayList<String>()
        for (index in 0 until listImages.size) {
            newListImages.add(
                listImages[index].replace("\"", "")
            )
        }

        setImageSlider(newListImages)

    }

    private val clickCategory = object : CategoryAdapter.OnclickCategory {
        override fun clickCategory(business: BranchTypes) {
            val intent = Intent(
                this@PropertyDevelopmentListActivity,
                PropertyDevelopmentByCategoryActivity::class.java
            )
            intent.putExtra("key", business.key)
            intent.putExtra("value", business.value)
            startActivity(intent)
        }

    }

    private val clickFeatured = object : PropertyFeatureItemAdapter.OnClickProperty {
        override fun click(FeatureProperty: Featured) {
            val intent = Intent(
                this@PropertyDevelopmentListActivity,
                PropertiesItemDetailActivity::class.java
            )
            intent.putExtra("id", FeatureProperty.id)
            startActivity(intent)
        }

    }

    //onclick nearby item
    private val clickNearBy = object : PropertyNearByItemAdapter.OnClickProperty {
        override fun click(nearByProperty: Nearby) {
            val intent = Intent(
                this@PropertyDevelopmentListActivity,
                PropertiesItemDetailActivity::class.java
            )
            intent.putExtra("id", nearByProperty.id)
            startActivity(intent)
        }

    }

    //onclick recommend
    private val clickRecommend = object : RecommendItemPropertyAdapter.OnClickProperty {
        override fun click(property: AllBranches) {
            val intent = Intent(
                this@PropertyDevelopmentListActivity,
                PropertiesItemDetailActivity::class.java
            )
            intent.putExtra("id", property.id)
            startActivity(intent)
        }

    }


    // result api
    private val getResultApiListener = object : ProjectDevelopmentWs.CallBackGetNearBy {
        override fun onResponeListImage(listImageSlider: ArrayList<String>) {
            showProgress(false)
            layoutBody.visibility = View.VISIBLE
            initImageSlider(listImageSlider)
        }

        override fun onResponeListCategory(listCategory: ArrayList<BranchTypes>) {
            showProgress(false)
            layoutBody.visibility = View.VISIBLE
            if (listCategory.isNotEmpty()) {
                recyclerViewCategory.isNestedScrollingEnabled = false
                recyclerViewCategory.adapter = CategoryAdapter(listCategory, clickCategory)
            }
        }

        override fun onResponeListNearBy(listNearBy: ArrayList<Nearby>) {
            showProgress(false)
            layoutBody.visibility = View.VISIBLE
            if (listNearBy.isNotEmpty()) {
                recyclerViewNearby.adapter = PropertyNearByItemAdapter(
                    listNearBy,
                    this@PropertyDevelopmentListActivity,
                    clickNearBy
                )
            }
        }

        override fun onResponeListFeatured(listFeatured: ArrayList<Featured>) {
            showProgress(false)
            layoutBody.visibility = View.VISIBLE
            if (listFeatured.isNotEmpty()) {
                recyclerViewFeaturing.adapter = PropertyFeatureItemAdapter(
                    listFeatured,
                    clickFeatured
                )
            }
        }

        override fun onResponeAllListProperty(listAllProperty: ArrayList<AllBranches>) {
            showProgress(false)
            layoutBody.visibility = View.VISIBLE
            if (listAllProperty.isNotEmpty()) {
                listRecommend.addAll(listAllProperty)
                adapter.notifyItemChanged(listAllProperty.size, listRecommend.size - limit)
            }
        }

        override fun onLoadingFailed(msg: String?) {
            showProgress(false)
            Utils.customToastMsgError(this@PropertyDevelopmentListActivity, msg, false)
        }

        override fun onGettingItemFailed(msg: String?) {
            showProgress(false)
            Utils.customToastMsgError(this@PropertyDevelopmentListActivity, msg, false)
        }

    }


    private fun requestServiceWs() {
        showProgress(true)
        ProjectDevelopmentWs.getHomePage(
            this,
            currentPage.toString(),
            limit.toString(),
            latitude.toString(),
            longitude.toString(),
            getResultApiListener
        )
    }

    private val getMoreRecommend = object : ProjectDevelopmentWs.CallBackGetNearBy {
        override fun onResponeListImage(listImageSlider: ArrayList<String>) {}

        override fun onResponeListCategory(listCategory: ArrayList<BranchTypes>) {}

        override fun onResponeListNearBy(listNearBy: ArrayList<Nearby>) {}

        override fun onResponeListFeatured(listFeatured: ArrayList<Featured>) {}

        override fun onResponeAllListProperty(listAllProperty: ArrayList<AllBranches>) {
            showProgress(false)
            if (listAllProperty.isNotEmpty()) {
                listRecommend.addAll(listAllProperty)
                adapter.notifyItemChanged(listAllProperty.size, listRecommend.size - limit)
                isRespond = true
            } else {
                isScrollMoreProject = false
            }
        }

        override fun onLoadingFailed(msg: String?) {
            showProgress(false)
            isRespond = true
            Utils.customToastMsgError(this@PropertyDevelopmentListActivity, msg, false)
        }

        override fun onGettingItemFailed(msg: String?) {
            showProgress(false)
            isRespond = true
            Utils.customToastMsgError(this@PropertyDevelopmentListActivity, msg, false)
        }

    }

    private fun initMoreRecommend() {
        showProgress(true)
        ProjectDevelopmentWs.getMoreRecommend(
            this,
            currentPage.toString(),
            limit.toString(),
            latitude.toString(),
            longitude.toString(),
            getMoreRecommend
        )
    }

    private fun showProgress(show: Boolean) {
        progress.visibility = if (show) View.VISIBLE else View.GONE
    }

}