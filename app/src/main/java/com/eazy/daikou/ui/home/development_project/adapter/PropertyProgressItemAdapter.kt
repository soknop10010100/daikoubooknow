package com.eazy.daikou.ui.home.development_project.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R

class PropertyProgressItemAdapter (
    private val listTextHeader:List<String>,
    private val listTextBody  :List<String>,
    private val listIcon      :List<Int>

        ):RecyclerView.Adapter<PropertyProgressItemAdapter.ViewHolder>()
{

    inner class ViewHolder(itemView:View):RecyclerView.ViewHolder(itemView)
    {
        val textHeader = itemView.findViewById<TextView>(R.id.text_header)
        val textBody   = itemView.findViewById<TextView>(R.id.text_body)
        val imageItem  = itemView.findViewById<ImageView>(R.id.img_item)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).
            inflate(R.layout.custom_item_progress_property_detail,parent,false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.textHeader.text = listTextHeader[position]
        holder.textBody.text   = listTextBody[position]
        holder.imageItem.setImageResource(listIcon[position])
    }

    override fun getItemCount(): Int {
        return listIcon.size
    }
}