package com.eazy.daikou.ui.home.parking.adapter;

import android.annotation.SuppressLint;
import android.content.res.ColorStateList;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.eazy.daikou.R;
import com.eazy.daikou.helper.DateUtil;
import com.eazy.daikou.helper.Utils;
import com.eazy.daikou.model.parking.Parking;

import java.util.List;
import java.util.Locale;

public class ParkingAdapter extends RecyclerView.Adapter<ParkingAdapter.ItemViewHolder> {

    private final List<Parking> parkingModelList;
    private final OnClickCallBackListener clickCallBackListener;

    public ParkingAdapter(List<Parking> parkingModelList, OnClickCallBackListener clickCallBackListener) {
        this.parkingModelList = parkingModelList;
        this.clickCallBackListener = clickCallBackListener;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_parking_em_model, parent, false);
        return new ItemViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        Parking parkingModel = parkingModelList.get(position);
        if (parkingModel != null){
            if(parkingModel.getCarPlateNo()!=null){
                holder.carNoTv.setText(parkingModel.getCarPlateNo());
            }else {
                holder.carNoTv.setVisibility(View.GONE);
            }
            holder.dateTv.setText(DateUtil.formatTimeZoneLocal(parkingModel.getEntranceDate()));

            holder.priceTv.setText(String.format(Locale.US,"%s %s",parkingModel.getParkingFeeCurrency(),parkingModel.getParkingFee()));
            holder.titleTv.setText(parkingModel.getPropertyName());

            holder.timeTv.setText(Utils.covertTime(parkingModel.getParkingDurationInMinute(),parkingModel.getParkingType()));

            if(parkingModel.getPaymentStatus().equals("unpaid")){
                holder.linearLayout.setVisibility(View.GONE);
                holder.paymentMethodTv.setVisibility(View.GONE);
                holder.linearLayoutStatus.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(holder.linearLayoutStatus.getContext(),R.color.red)));
                holder.statusTv.setTextColor(ContextCompat.getColor(holder.statusTv.getContext(),R.color.white));
                holder.statusTv.setText(R.string.un_paids);
            }else {
                holder.linearLayout.setVisibility(View.VISIBLE);
                holder.paymentMethodTv.setVisibility(View.VISIBLE);
                holder.linearLayoutStatus.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(holder.linearLayoutStatus.getContext(),R.color.appBarColorOld)));
                holder.statusTv.setTextColor(ContextCompat.getColor(holder.statusTv.getContext(),R.color.white));
                if(parkingModel.getPaymentMethod()!=null)
                holder.paymentMethodTv.setText(parkingModel.getPaymentMethod());
                holder.statusTv.setText(R.string.paids);
            }

            if(parkingModel.getParkingLotTypeName()!=null){
                holder.parkingLotNoTv.setText(parkingModel.getParkingNoName());
            }else {
                holder.relateParkingLot.setVisibility(View.GONE);
            }
            if(parkingModel.getTicketNo()!=null){
                holder.ticketNoTv.setText(parkingModel.getTicketNo());
            }else {
                holder.relateTicket.setVisibility(View.GONE);
            }
            holder.noOrderTv.setText(parkingModel.getOrderNo());

            holder.itemView.setOnClickListener(view -> clickCallBackListener.clickCallBack(parkingModel));

        }
    }
    @Override
    public int getItemCount() {
        return parkingModelList.size();
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder{

        private final TextView ticketNoTv, paymentMethodTv,statusTv, dateTv,parkingLotNoTv,titleTv;
        private final TextView timeTv,priceTv,carNoTv,noOrderTv;
        private final LinearLayout linearLayout;
        private final LinearLayout linearLayoutStatus,relateParkingLot,relateTicket;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);

            dateTv = itemView.findViewById(R.id.date_Text);
            parkingLotNoTv = itemView.findViewById(R.id.parking_lot_no_Text);
            ticketNoTv = itemView.findViewById(R.id.ticket_no);
            paymentMethodTv = itemView.findViewById(R.id.payment_method_text);
            statusTv = itemView.findViewById(R.id.payment_status_Text);
            titleTv = itemView.findViewById(R.id.title_text);
            timeTv = itemView.findViewById(R.id.time_parking);
            priceTv = itemView.findViewById(R.id.price_parking);
            linearLayout = itemView.findViewById(R.id.payment_linear);
            carNoTv = itemView.findViewById(R.id.carr_no_Tex);
            linearLayoutStatus = itemView.findViewById(R.id.linear_text);
            relateParkingLot = itemView.findViewById(R.id.relate_parking_lot);
            relateTicket = itemView.findViewById(R.id.relate_ticket);
            noOrderTv = itemView.findViewById(R.id.no_order);

        }
    }

    public interface OnClickCallBackListener{
        void clickCallBack(Parking parkingModel);
    }
    public void clear() {
        int size = parkingModelList.size ();
        if (size > 0) {
            parkingModelList.subList(0, size).clear();
            notifyItemRangeRemoved ( 0, size );
        }
    }
}
