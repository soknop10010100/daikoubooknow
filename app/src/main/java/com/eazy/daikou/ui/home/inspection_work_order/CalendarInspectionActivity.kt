package com.eazy.daikou.ui.home.inspection_work_order

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.Constant
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.ui.home.inspection_work_order.adapter.CalendarInspectionAdapter
import java.text.SimpleDateFormat
import java.util.*

import android.widget.*
import com.eazy.daikou.request_data.request.inspection_ws.InspectionWS
import com.eazy.daikou.helper.DateUtil
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.model.profile.User
import com.eazy.daikou.model.inspection.InfoInspectionModel
import com.github.sundeepk.compactcalendarview.CompactCalendarView
import com.github.sundeepk.compactcalendarview.CompactCalendarView.CompactCalendarViewListener
import com.github.sundeepk.compactcalendarview.domain.Event
import com.google.gson.Gson
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

import android.content.res.ColorStateList
import androidx.core.content.ContextCompat
import com.eazy.daikou.request_data.static_data.StaticUtilsKey
import com.eazy.daikou.model.inspection.Add24HoursModel
import devs.mulham.horizontalcalendar.HorizontalCalendar
import devs.mulham.horizontalcalendar.HorizontalCalendarView
import devs.mulham.horizontalcalendar.utils.HorizontalCalendarListener


class CalendarInspectionActivity : BaseActivity(), View.OnClickListener {

    private lateinit var calendarLayout : LinearLayout
    private lateinit var btnMonth : TextView
    private lateinit var btnWeek : TextView
    private lateinit var recyclerView: RecyclerView
    private lateinit var inspectionHourRecycle: RecyclerView
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var linearLayoutHourManager: LinearLayoutManager
    private lateinit var progressBar: ProgressBar
    private lateinit var noItemTv : TextView
    private lateinit var todayTv : TextView
    private lateinit var monthYearTv : TextView
    private lateinit var showDateTv : TextView
    private lateinit var monthYearWeekActionTv : TextView
    private lateinit var showDateWeekActionTv : TextView
    private lateinit var titleNewEvent : TextView

    private val currentCalender = Calendar.getInstance(Locale.getDefault())
    private val dateFormatForDisplaying = SimpleDateFormat("dd-M-yyyy hh:mm:ss a", Locale.getDefault())
    private lateinit var compactCalendarView: CompactCalendarView
    private val dateFormatForMonth = SimpleDateFormat("MMMM yyyy", Locale.getDefault())

    private lateinit var formatter : SimpleDateFormat
    private lateinit var calendarInspectionAdapter: CalendarInspectionAdapter
    private lateinit var calendarInspectionHourAdapter: CalendarInspectionAdapter
    private lateinit var horizontalCalendar : HorizontalCalendarView

    private var infoInspectionList : ArrayList<InfoInspectionModel> = ArrayList()
    private var infoInspectionDayList : ArrayList<InfoInspectionModel> = ArrayList()
    private var add24HoursModelList : ArrayList<Add24HoursModel> = ArrayList()

    private var isClickMonth : Boolean = true
    private var isGetFirstList : Boolean = true
    private var dateVal : String = ""
    private var dateWeekVal : String = ""
    private var startDateVal : String = ""
    private var endDateVal : String = ""
    private var dateType : String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calendar_inspection)

        initView()

        initAction()

    }

    private fun initView(){
        findViewById<TextView>(R.id.titleCancel).setOnClickListener{finish()}
        progressBar = findViewById(R.id.progressItem)
        progressBar.progressTintList = ColorStateList.valueOf(Color.WHITE)

        calendarLayout = findViewById(R.id.calendarLayout)
        btnMonth = findViewById(R.id.btnMonth)
        btnWeek = findViewById(R.id.btnWeek)
        recyclerView = findViewById(R.id.inspectionRecycle)
        inspectionHourRecycle = findViewById(R.id.inspectionHourRecycle)
        compactCalendarView = findViewById(R.id.compactCalendarView)
        monthYearTv = findViewById(R.id.month_yearTv)
        showDateTv = findViewById(R.id.showDateTv)
        monthYearWeekActionTv = findViewById(R.id.month_year_weekTv)
        showDateWeekActionTv = findViewById(R.id.showDateWeekTv)
        noItemTv = findViewById(R.id.noItemTv)
        todayTv = findViewById(R.id.todayTv)
        horizontalCalendar = findViewById(R.id.calendarView)
        titleNewEvent = findViewById(R.id.titleNewEvent)

        if (intent != null && intent.hasExtra("action")){
            val action = intent.getStringExtra("action")
            if (action == StaticUtilsKey.maintenance_action){
                titleNewEvent.text = resources.getString(R.string.maintenance_schedule)
            }
        }
    }

    @SuppressLint("SimpleDateFormat")
    private fun initAction(){
        btnWeek.setOnClickListener(this)
        btnMonth.setOnClickListener(this)

        val today = Date()
        formatter = SimpleDateFormat("yyyy-MM-dd")
        dateVal = formatter.format(today)
        dateWeekVal = formatter.format(today)
        showDateTv.text = dateVal
        showDateWeekActionTv.text = dateWeekVal
        monthYearWeekActionTv.text = dateFormatForMonth.format(today)

        dateType = "month"

        initListInspection()

        linearLayoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = linearLayoutManager

        linearLayoutHourManager = LinearLayoutManager(this)
        inspectionHourRecycle.layoutManager = linearLayoutHourManager

        calendarInspectionAdapter = CalendarInspectionAdapter(infoInspectionDayList, true, onClickCallBack)
        recyclerView.adapter = calendarInspectionAdapter

        initRowCalendar()

    }

    private fun initListInspection(){
        progressBar.visibility = View.VISIBLE
        InspectionWS().getListInspectionByCalendar(this,hashMapData(), callBackListener)
    }

    private fun hashMapData(): HashMap<String, Any> {
        val hashMap = HashMap<String, Any>()
        hashMap["property_id"] = Gson().fromJson(UserSessionManagement(this).userDetail, User::class.java).accountId
        hashMap["date_type"] = dateType
        hashMap["report_type"] = Utils.getReportType()[StaticUtilsKey.isActionMenuHome].toString()
        if (dateType == "week"){
            hashMap["start_week_date"] = startDateVal
            hashMap["end_week_date"] = endDateVal
        } else {
            if (isClickMonth) {
                hashMap["date"] = dateVal
            } else{
                hashMap["date"] = dateWeekVal
            }
        }
        return hashMap
    }

    private var callBackListener = object : InspectionWS.CallBackItemInspectionInfoListener{
        @SuppressLint("NotifyDataSetChanged")
        override fun onSuccess(itemInspectionModels: MutableList<InfoInspectionModel>?) {
            progressBar.visibility = View.GONE
            if (dateType == "month") {
                if (infoInspectionList.size > 0)     infoInspectionList.clear()
                itemInspectionModels?.let { infoInspectionList.addAll(it) }

                if (isGetFirstList){
                    dateType = "day"
                    initListInspection()
                    isGetFirstList = false
                }

                initCalendar()
            } else if (dateType == "day"){
                if (infoInspectionDayList.size > 0)     infoInspectionDayList.clear()
                itemInspectionModels?.let { infoInspectionDayList.addAll(it) }

                if(isClickMonth) {
                    recyclerView.visibility = if (infoInspectionDayList.size > 0 ) View.VISIBLE else View.GONE
                    noItemTv.visibility = if (infoInspectionDayList.size > 0)   View.GONE else View.VISIBLE
                    calendarInspectionAdapter.notifyDataSetChanged()
                } else {
                    getListHours()
                }
            }

        }

        override fun onFailed(msg: String?) {
            progressBar.visibility = View.GONE
            Utils.customToastMsgError(this@CalendarInspectionActivity, msg, false)
        }

    }

    private fun showCalendar(isClickMonthAction : Boolean){
        setBackgroundTint(btnWeek, if (isClickMonthAction) R.color.gray else R.color.white, if (isClickMonthAction) R.color.transparent else R.color.color_gray_item)
        setBackgroundTint(btnMonth, if (isClickMonthAction) R.color.white else R.color.gray, if (isClickMonthAction) R.color.color_gray_item else R.color.transparent)
        isClickMonth = isClickMonthAction
        dateType = if (isClickMonthAction) "month" else "day"
        todayTv.visibility = if (isClickMonthAction) View.INVISIBLE  else View.VISIBLE
        calendarLayout.visibility = if (isClickMonthAction) View.VISIBLE  else View.GONE
        horizontalCalendar.visibility = if (isClickMonthAction) View.GONE  else View.VISIBLE
        recyclerView.visibility = if (isClickMonthAction) View.VISIBLE  else View.GONE
        inspectionHourRecycle.visibility = if (isClickMonthAction) View.GONE  else View.VISIBLE
        showDateWeekActionTv.visibility = if (isClickMonthAction) View.GONE  else View.VISIBLE
        monthYearWeekActionTv.visibility = if (isClickMonthAction) View.GONE  else View.VISIBLE
        showDateTv.visibility = if (isClickMonthAction) View.VISIBLE  else View.GONE
        monthYearTv.visibility = if (isClickMonthAction) View.VISIBLE  else View.GONE

        initListInspection()
    }

    private fun setBackgroundTint(btn : TextView, colorButton : Int, color : Int){
        btn.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(this, color))
        btn.setTextColor(Utils.getColor(this, colorButton))
    }

    private fun initCalendar(){
        compactCalendarView.setFirstDayOfWeek(Calendar.MONDAY)
        compactCalendarView.setIsRtl(false)
        compactCalendarView.displayOtherMonthDays(false)
        compactCalendarView.setUseThreeLetterAbbreviation(true)

        monthYearTv.text = dateFormatForMonth.format(compactCalendarView.firstDayOfCurrentMonth)

        loadEventsForYear()
        compactCalendarView.invalidate()

        logEventsByMonth(compactCalendarView)

        compactCalendarView.setListener(object : CompactCalendarViewListener {
            override fun onDayClick(dateClicked: Date) {
                dateVal = formatter.format(dateClicked)
                dateType = "day"
                showDateTv.text = dateVal

                initListInspection()
            }

            override fun onMonthScroll(firstDayOfNewMonth: Date) {
                dateVal = formatter.format(firstDayOfNewMonth)
                dateType = "month"
                monthYearTv.text = dateFormatForMonth.format(firstDayOfNewMonth)

                initListInspection()
            }
        })
    }

    private fun loadEventsForYear() {
        compactCalendarView.removeAllEvents()
        for (infoInspectionModel in infoInspectionList) {
            if(infoInspectionModel.conductDt != null){
                addEvents( (Integer.parseInt(DateUtil.formatOnlyMonth(infoInspectionModel.conductDt, false)) - 1) ,
                    Integer.parseInt(DateUtil.formatOnlyYear(infoInspectionModel.conductDt)),
                    Integer.parseInt(DateUtil.formatOnlyDay(infoInspectionModel.conductDt)) - 1)
            }
        }
    }

    private fun logEventsByMonth(compactCalendarView: CompactCalendarView) {
        currentCalender.time = Date()
        currentCalender[Calendar.DAY_OF_MONTH] = 1
        currentCalender[Calendar.MONTH] = Calendar.AUGUST
        val dates: MutableList<String> = java.util.ArrayList()
        for (e in compactCalendarView.getEventsForMonth(Date())) {
            dates.add(dateFormatForDisplaying.format(e.timeInMillis))
        }

        Utils.logDebug(Constant.TAG, "Events for Aug with simple date formatter: $dates")
        Utils.logDebug(Constant.TAG, "Events for Aug month using default local and timezone: " + compactCalendarView.getEventsForMonth(
                currentCalender.time
            )
        )
    }

    private fun addEvents(month: Int, year: Int, day : Int) {
        currentCalender.time = Date()
        currentCalender[Calendar.DAY_OF_MONTH] = 1
        val firstDayOfMonth = currentCalender.time
        currentCalender.time = firstDayOfMonth
        if (month > -1) {
            currentCalender[Calendar.MONTH] = month
        }
        if (year > -1) {
            currentCalender[Calendar.ERA] = GregorianCalendar.AD
            currentCalender[Calendar.YEAR] = year
        }
        currentCalender.add(Calendar.DATE, day)
        setToMidnight(currentCalender)
        val timeInMillis = currentCalender.timeInMillis
        val events = getEvents(timeInMillis)
        compactCalendarView.addEvents(events)

    }

    private fun getEvents(timeInMillis: Long): List<Event?> {
        return listOf(Event(Utils.getColor(this, R.color.green), timeInMillis, "Have Event" + Date(timeInMillis)))
    }

    private fun setToMidnight(calendar : Calendar) {
        calendar.set(Calendar.HOUR_OF_DAY, 0)
        calendar.set(Calendar.MINUTE, 0)
        calendar.set(Calendar.SECOND, 0)
        calendar.set(Calendar.MILLISECOND, 0)
    }

    private fun initRowCalendar(){
        val startDate = Calendar.getInstance()
        startDate.add(Calendar.MONTH, -100)
        val endDate = Calendar.getInstance()
        endDate.add(Calendar.MONTH, 100)

        val horizontalCalendar = HorizontalCalendar.Builder(this, R.id.calendarView)
            .range(startDate, endDate)
            .build()

        horizontalCalendar.calendarListener = object : HorizontalCalendarListener() {
            override fun onDateSelected(date: Calendar, position: Int) {
                dateWeekVal = formatter.format(date.time)
                dateType = "day"
                showDateWeekActionTv.text = dateWeekVal

                monthYearWeekActionTv.text = dateFormatForMonth.format(date.time)

                initListInspection()
            }

            override fun onCalendarScroll(calendarView: HorizontalCalendarView, dx: Int, dy: Int) { }

            override fun onDateLongClicked(date: Calendar, position: Int): Boolean {
                return true
            }
        }

        todayTv.setOnClickListener { horizontalCalendar.goToday(true) }

    }

    @SuppressLint("SimpleDateFormat")
    private fun addListHour() : List<String>{
        val timeFormat = SimpleDateFormat("h:mm a", Locale.getDefault())
        val list: MutableList<String> = ArrayList()

        for (i in 0..23) {
            val calendar = Calendar.getInstance()
            calendar[Calendar.HOUR_OF_DAY] = i
            calendar[Calendar.MINUTE] = 0
            list.add(timeFormat.format(calendar.time))
        }
        return list
    }

    private fun getListHours(){
        var stringList: MutableList<InfoInspectionModel?>
        add24HoursModelList = ArrayList()
        for (hourList in addListHour()) {
            stringList = ArrayList()
            for (info in infoInspectionDayList) {
                if (info.conductTime != null) {
                    val conductTimeArr = info.conductTime.split(":").toTypedArray()
                    // Compare Hour
                    if (conductTimeArr.isNotEmpty()) {
                        if (hourList.split(":").toTypedArray()[0] ==
                            DateUtil.convertTimeByCalendar(Integer.parseInt(conductTimeArr[0]), Integer.parseInt(conductTimeArr[1])).split(":").toTypedArray()[0]) {
                            // Compare AM Or PM
                            if (hourList.split(" ").toTypedArray()[1] ==
                                DateUtil.convertTimeByCalendar(Integer.parseInt(info.conductTime.split(":").toTypedArray()[0]),
                                    Integer.parseInt(info.conductTime.split(":").toTypedArray()[1])).split(" ").toTypedArray()[1]) {
                                stringList.add(info)
                            }
                        }
                    }
                }
            }
            val testModel = Add24HoursModel(hourList, stringList)
            add24HoursModelList.add(testModel)
        }
        calendarInspectionHourAdapter = CalendarInspectionAdapter(ArrayList(), add24HoursModelList, false, onClickCallBack)
        inspectionHourRecycle.adapter = calendarInspectionHourAdapter

    }

    private var onClickCallBack = object : CalendarInspectionAdapter.OnClickCallBackListener{
        override fun onClickCallBackItemDay(add24HoursModel: Add24HoursModel) {
            if(add24HoursModel.hourList.size > 0) {
                val intent = Intent(this@CalendarInspectionActivity, DetailConductTimeActivity::class.java)
                intent.putExtra("add24HoursModelList", add24HoursModel)
                startActivity(intent)
            } else{
                Toast.makeText(this@CalendarInspectionActivity, resources.getString(R.string.not_available), Toast.LENGTH_SHORT).show()
            }
        }

        override fun onClickCallBackItemInMonth(infoInspectionModel: InfoInspectionModel) {
            val intent = Intent(this@CalendarInspectionActivity, ChooseInspectionTypeActivity::class.java)
            intent.putExtra("inspection_template_id", infoInspectionModel.id)
            intent.putExtra("template_id", infoInspectionModel.inspectionTemplateId)
            intent.putExtra("action", StaticUtilsKey.form_template_type)
            intent.putExtra("action_type", "action_detail")
            startActivity(intent)
        }

    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btnMonth -> {
                showCalendar(true)
            }
            R.id.btnWeek -> {
                showCalendar(false)
            }
        }
    }

}