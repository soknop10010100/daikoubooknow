package com.eazy.daikou.ui.home.service_provider_new.activity

import android.content.Intent
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.base.BaseActivity

class QuotationProviderActivity : BaseActivity() {

    private lateinit var addItemTv: TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_quotation_provider)

        initView()

        initAction()
    }

    private fun initView(){

        addItemTv = findViewById(R.id.addItemTv)
    }
    private fun initAction(){

        findViewById<ImageView>(R.id.imageBack).setOnClickListener { finish() }
        findViewById<TextView>(R.id.toolbarTv).text = "Quote PlatForm"

        addItemTv.setOnClickListener( CustomSetOnClickViewListener{
            startActivity(Intent(this@QuotationProviderActivity, AddQuoteProviderActivity::class.java))
        })
    }

}