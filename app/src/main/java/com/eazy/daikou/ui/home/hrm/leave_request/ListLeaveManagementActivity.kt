package com.eazy.daikou.ui.home.hrm.leave_request

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.viewpager.widget.ViewPager
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.helper.BroadcastTypes
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.ui.home.hrm.leave_request.adapter.TabsLeaveAdapter
import com.eazy.daikou.ui.home.hrm.over_time.OverTimeHRActivity
import com.google.android.material.tabs.TabLayout

class ListLeaveManagementActivity : BaseActivity() {

    private lateinit var btnBack : ImageView
    private lateinit var titleTv : TextView
    private lateinit var btnFilter : TextView
    private lateinit var viewPager : ViewPager
    private lateinit var tabLayout : TabLayout
    private var orderId = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_leave_management)

        initView()

        initAction()

    }

    private fun initView(){

        btnBack = findViewById(R.id.iconBack)
        titleTv = findViewById(R.id.titleToolbar)
        btnFilter = findViewById(R.id.notification_d)
        viewPager = findViewById(R.id.viewPagerTab)
        tabLayout = findViewById(R.id.tabLayouts)

        titleTv.text = resources.getString(R.string.leave_magement)

    }

    private fun initAction(){
        btnBack.setOnClickListener { finish() }

        val adapter = TabsLeaveAdapter(supportFragmentManager)

        // add fragment to the list
        adapter.addFragment(MyRequestLeaveFragment(), getString(R.string.my_request))

        adapter.addFragment(ManageLeaveFragment(), getString(R.string.manage_request))

        viewPager.adapter = adapter
        tabLayout.setupWithViewPager(viewPager)

        // From Notification
        if (intent != null && intent.hasExtra("id")){
            orderId = intent.getStringExtra("id").toString()
            startLeaveDetail()
            registerActionReceiver()
        }

    }


    private fun startLeaveDetail(){
        val intent = Intent(this,LeaveRequestDetailActivity::class.java)
        intent.putExtra("action", intent.getStringExtra("action"))
        intent.putExtra("id", orderId)
        startActivity(intent)
    }

    override fun onDestroy() {
        unRegisterActionReceiver(mMessageReceiver)
        super.onDestroy()
    }

    private val mMessageReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val listType = intent.getStringExtra("listType").toString()
            if (listType != "listType") {
                tabLayout.setScrollPosition(1, 0f, true) // When refresh from detail leave
                viewPager.currentItem = 1
            }
        }
    }

    private fun unRegisterActionReceiver(mActionReceiver: BroadcastReceiver) {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mActionReceiver)
    }

    private fun registerActionReceiver() {
        LocalBroadcastManager.getInstance(this).
        registerReceiver(mMessageReceiver, IntentFilter(BroadcastTypes.UPDATE_LEAVE_MENU.name))
    }

}