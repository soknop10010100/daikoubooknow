package com.eazy.daikou.ui.home.book_now.booking_hotel.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.booking_hotel.RoomBookingHistory
import de.hdodenhof.circleimageview.CircleImageView

class RoomHisDetailBookedAdapter(private val list: List<RoomBookingHistory>) : RecyclerView.Adapter<RoomHisDetailBookedAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.hotel_room_booked_layout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item : RoomBookingHistory = list[position]
        if (item != null){
            holder.iconProfile.visibility = View.VISIBLE
            Glide.with(holder.iconProfile).load(if (item.image != null) item.image else R.drawable.no_image).into(holder.iconProfile)
            Utils.setValueOnText(holder.nameTv, item.room_name)
            Utils.setValueOnText(holder.numRoomTv, item.number)
            if (item.price != null){
                holder.priceTv.text = String.format("%s %s", "$", item.price)
            } else {
                holder.priceTv.text = ". . ."

            }
            holder.lin.visibility = if (list.size - 1 == position) View.GONE else View.VISIBLE
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val nameTv : TextView = itemView.findViewById(R.id.item_name)
        val numRoomTv : TextView = itemView.findViewById(R.id.numRoomTv)
        val priceTv : TextView = itemView.findViewById(R.id.priceTv)
        val iconProfile : CircleImageView = itemView.findViewById(R.id.imgProfile)
        val lin : View = itemView.findViewById(R.id.lin)
    }

}