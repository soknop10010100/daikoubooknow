package com.eazy.daikou.ui.profile.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.profile.PropertyItemModel
import com.eazy.daikou.model.profile.User
import com.eazy.daikou.model.profile.UserTypeProfileModel
import java.util.*

class SwitchProfilePropertyAdapter(private val context : Context, private val action : String, private val propertyList : ArrayList<PropertyItemModel>, private val list : ArrayList<UserTypeProfileModel>, private val userModel : User, private val onClick : OnClickCallBackListener) :
    RecyclerView.Adapter<SwitchProfilePropertyAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.my_role_main_model, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if(action == "switch_profile") {
            val feeTypeModel: UserTypeProfileModel = list[position]
            if (feeTypeModel != null) {
                holder.title.text = String.format(Locale.US, "%s", feeTypeModel.user_type)
                holder.descriptionTv.text = context.getString(R.string.tap_to_switch)
                holder.icon.setImageResource(R.drawable.profile_assign_work_order)
                if (feeTypeModel.user_type == userModel.activeUserType){
                    holder.iconDropDown.setImageResource(R.drawable.ic_tech_black)
                    Utils.setBgTint(holder.cardIcon, R.color.appBarColorOld)
                    holder.iconDropDown.setColorFilter(Utils.getColor(context, R.color.appBarColorOld))
                } else {
                    holder.iconDropDown.setImageResource(R.drawable.ic_arrow_next)
                    holder.iconDropDown.setColorFilter(Utils.getColor(context, R.color.gray))
                    Utils.setBgTint(holder.cardIcon, R.color.blue)
                }

                holder.cardLayout.setOnClickListener(
                    CustomSetOnClickViewListener{
                        onClick.onClickCallBack(feeTypeModel)
                    }
                )
            }
        } else {
            val feeTypeModel: PropertyItemModel = propertyList[position]
            if (feeTypeModel != null) {
                holder.title.text = String.format(Locale.US, "%s", if (feeTypeModel.name != null) feeTypeModel.name else ". . .")
                holder.descriptionTv.text = context.getString(R.string.tap_to_switch)
                holder.icon.setImageResource(R.drawable.home_my_property)
                if (feeTypeModel.id == userModel.accountId){
                    holder.iconDropDown.setImageResource(R.drawable.ic_tech_black)
                    Utils.setBgTint(holder.cardIcon, R.color.appBarColorOld)
                    holder.iconDropDown.setColorFilter(Utils.getColor(context, R.color.appBarColorOld))
                }
                else {
                    holder.iconDropDown.setImageResource(R.drawable.ic_arrow_next)
                    Utils.setBgTint(holder.cardIcon, R.color.blue)
                    holder.iconDropDown.setColorFilter(Utils.getColor(context, R.color.color_light_gray_item))
                }
                holder.cardLayout.setOnClickListener {
                    onClick.onClickCallBack(feeTypeModel)
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return if (action == "switch_profile"){
            list.size
        } else {
            propertyList.size
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var icon: ImageView = itemView.findViewById(R.id.imgProfile)
        var title: TextView = itemView.findViewById(R.id.name_item_hr)
        var descriptionTv: TextView = itemView.findViewById(R.id.descriptionTv)
        var cardLayout : CardView = itemView.findViewById(R.id.card_hr)
        var cardIcon : CardView = itemView.findViewById(R.id.cardIcon)
        var iconDropDown : ImageView = itemView.findViewById(R.id.iconDropDown)
    }

    interface OnClickCallBackListener{
        fun onClickCallBack(userTypeProfileModel: UserTypeProfileModel)
        fun onClickCallBack(propertyItemModel: PropertyItemModel)
    }

}