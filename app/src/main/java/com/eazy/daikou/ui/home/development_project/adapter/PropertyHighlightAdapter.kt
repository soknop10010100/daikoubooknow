package com.eazy.daikou.ui.home.development_project.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R

class PropertyHighlightAdapter(
    private val listTitle: List<String>,
    private val listIcon : List<Int>
):RecyclerView.Adapter<PropertyHighlightAdapter.ViewHolder>()
{

    inner class ViewHolder(itemView:View):RecyclerView.ViewHolder(itemView)
    {
        val textTitle = itemView.findViewById<TextView>(R.id.txtTitle)
        val imageItem = itemView.findViewById<ImageView>(R.id.img_changeItem)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).
            inflate(R.layout.item_project_highlight,parent,false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.textTitle.text = listTitle[position]
        holder.imageItem.setImageResource(listIcon[position])
    }

    override fun getItemCount(): Int {
        return listTitle.size
    }
}