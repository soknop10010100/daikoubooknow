package com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.adapter

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.base.MockUpData
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.my_property.service_provider.ServicePropertyModel
import com.squareup.picasso.Picasso


class PropertyAndServiceAdapter(private var activity: Activity, private var viewTypeDraw: Int, private val propertyServiceList: ArrayList<ServicePropertyModel>, private val onclickProduct: OnClickProperty) :
    RecyclerView.Adapter<PropertyAndServiceAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return if (MockUpData.SWITCH_VIEW == 1){
            val v = LayoutInflater.from(parent.context).inflate(R.layout.layout_buysell_product_horizontal, parent, false)
            ViewHolder(v)
        } else {
            val v = LayoutInflater.from(parent.context).inflate(R.layout.layout_buysell_product_vertical, parent, false)
            val itemViewParam = LinearLayout.LayoutParams(Utils.getWidth(activity) / 2, LinearLayout.LayoutParams.WRAP_CONTENT)
            v.layoutParams = itemViewParam
            ViewHolder(v)
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(propertyServiceList[position],activity)
        holder.itemView.setOnClickListener{
            onclickProduct.onClickCompany(propertyServiceList[position])
        }


    }

    override fun getItemCount(): Int {
        return propertyServiceList.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imageView : ImageView? = null
        var contexts : Context ?=null
        var companyNameTV : TextView? = null
        var phoneTv : TextView? = null
        var descriptionTv : TextView ?= null
        var locationTv :TextView ?= null

        @SuppressLint("SetTextI18n")
        fun bindItems(service: ServicePropertyModel, activity: Context) {
            contexts= activity
            companyNameTV = itemView.findViewById(R.id.txtCompanyName)
            imageView = itemView.findViewById(R.id.productImg)
            phoneTv = itemView.findViewById(R.id.contact_tex)
            descriptionTv = itemView.findViewById(R.id.txtDescription)
            locationTv = itemView.findViewById(R.id.txtLocation)

//            companyNameTV!!.text = service.contractorName
//            phoneTv!!.text = service.companyContact
//            descriptionTv!!.text = service.productDesc
//            locationTv!!.text = service.address
//            if(service.companyLogo!=null){
//                Picasso.get().load(service.companyLogo).into(imageView)
//            }else{
//                Picasso.get().load(R.drawable.no_image).into(imageView)
//            }

            companyNameTV!!.text = if (service.serviceName != null) service.serviceName else activity.getString(R.string.no_name)
            phoneTv!!.text = if (service.servicePhoneNumber != null) service.servicePhoneNumber else activity.getString(R.string.no_phone)
            descriptionTv!!.text = if (service.serviceDesc != null) service.serviceDesc else activity.getString(R.string.no_description)
            locationTv!!.text = if (service.serviceAddress != null) service.serviceAddress else activity.getString(R.string.no_address)
            if(service.serviceLogo!=null){
                Picasso.get().load(service.serviceLogo).into(imageView)
            }else{
                Picasso.get().load(R.drawable.no_image).into(imageView)
            }

        }
    }

    interface OnClickProperty{
        fun onClickCompany(category: ServicePropertyModel)
        fun onClickMenu(title : String , product : ServicePropertyModel)
    }

    fun clear() {
        val size: Int = propertyServiceList.size
        if (size > 0) {
            for (i in 0 until size) {
                propertyServiceList.removeAt(0)
            }
            notifyItemRangeRemoved(0, size)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return viewTypeDraw
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setViewType(){
        notifyDataSetChanged()
    }

}