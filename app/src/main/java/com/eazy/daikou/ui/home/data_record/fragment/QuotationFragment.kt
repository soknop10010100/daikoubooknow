package com.eazy.daikou.ui.home.data_record.fragment

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AbsListView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseFragment
import com.eazy.daikou.request_data.request.data_record.DataRecordWS
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.data_record.SubListDataRecord
import com.eazy.daikou.ui.home.data_record.DetailDataRecordActivity
import com.eazy.daikou.ui.home.data_record.DetailHoldAndReservedActivity
import com.eazy.daikou.ui.home.data_record.DetailQuotationDRActivity
import com.eazy.daikou.ui.home.data_record.DetailSaleAgreementsActivity
import com.eazy.daikou.ui.home.data_record.adapter.AdapterOpportunityDR

class QuotationFragment : BaseFragment() {
    private lateinit var refreshLayoutDataRecord : SwipeRefreshLayout
    private lateinit var textNoDataTv : TextView
    private lateinit var progressBar: ProgressBar
    private lateinit var recyclerDataRecord : RecyclerView
    private lateinit var adapterOpportunityDR : AdapterOpportunityDR
    private lateinit var linearLayoutManager : LinearLayoutManager

    private var currentItem = 0
    private var total : Int = 0
    private var scrollDown : Int = 0
    private var currentPage : Int = 1
    private var size : Int = 10
    private var isScrolling = false
    private var action: String = ""

    private val dataRecordModelList : ArrayList<SubListDataRecord> = ArrayList()

    companion object{
        fun newInstance(action: String) : QuotationFragment {
            val args = Bundle()
            val fragment = QuotationFragment()
            args.putString("action", action)
            fragment.arguments = args
            return fragment
        }
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_quotation, container, false)

        initView(view)

        initData()

        initAction()

        return view
    }

    private fun initView(view : View){
        refreshLayoutDataRecord = view.findViewById(R.id.refreshLayoutDataRecord)
        textNoDataTv = view.findViewById(R.id.textNoDataTv)
        progressBar = view.findViewById(R.id.progressItem)
        recyclerDataRecord = view.findViewById(R.id.recyclerDataRecord)
    }

    private fun initData(){
        arguments?.let {
            action = it.getString("action").toString()
        }
    }

    private fun initAction(){

        requestServiceApi()

        onScrollInfoRecycle()

        linearLayoutManager = LinearLayoutManager(mActivity)
        recyclerDataRecord.layoutManager = linearLayoutManager
        adapterOpportunityDR = AdapterOpportunityDR(mActivity, action, dataRecordModelList, callBackClickAdapter)
        recyclerDataRecord.adapter = adapterOpportunityDR

        refreshLayoutDataRecord.setColorSchemeResources(R.color.colorPrimary)
        refreshLayoutDataRecord.setOnRefreshListener { this.onRefreshLayout() }

    }

    private fun onRefreshLayout(){
        currentPage = 1
        size = 10
        isScrolling = true
         adapterOpportunityDR.clear()
        requestServiceApi()
    }

    private val callBackClickAdapter = object : AdapterOpportunityDR.CallBackHoldAndReservedListener{

        override fun clickItemHoldAndReservedListener(subDataRecord: SubListDataRecord) {
            when (action) {
                "opportunity" -> {
                    val intent = Intent(mActivity, DetailDataRecordActivity::class.java)
                    intent.putExtra("id_opportunity", subDataRecord.id)
                    startActivity(intent)
                }

                "quotation" -> {
                    val intent = Intent(mActivity, DetailQuotationDRActivity::class.java)
                    intent.putExtra("id_quotation", subDataRecord.id)
                    startActivity(intent)
                }

                "hold_reserved" -> {
                    val intent = Intent(mActivity, DetailHoldAndReservedActivity::class.java)
                    intent.putExtra("id_list", subDataRecord.id)
                    intent.putExtra("action", "hold_reserved")
                    startActivity(intent)
                }

                "booking" -> {
                    val intent = Intent(mActivity, DetailHoldAndReservedActivity::class.java)
                    intent.putExtra("id_list", subDataRecord.id)
                    intent.putExtra("action", "booking")
                    startActivity(intent)
                }

                "sale_agreement" -> {
                    val intent = Intent(mActivity, DetailSaleAgreementsActivity::class.java)
                    intent.putExtra("id_list", subDataRecord.id)
                    startActivity(intent)
                }
            }

        }
    }

    private fun requestServiceApi(){
        progressBar.visibility = View.VISIBLE
        when (action) {
            "opportunity" -> {
                DataRecordWS().getListOpportunityWS(mActivity, currentPage, 10, callBackOpportunity)
            }

            "quotation" -> {
                DataRecordWS().getListQuotationWs(mActivity, currentPage, 10, callBackOpportunity)
            }

            "hold_reserved" -> {
                DataRecordWS().getListHoldWithReservedWs(mActivity, currentPage, 10, callBackOpportunity)
            }

            "booking" -> {
                DataRecordWS().getListBookingWS(mActivity, currentPage, 10, callBackOpportunity)
            }

            "sale_agreement" -> {
                DataRecordWS().getListSaleAgreementsWS(mActivity, currentPage, 10, callBackOpportunity)
            }

            else -> {
                DataRecordWS().getListOpportunityWS(mActivity, currentPage, 10, callBackOpportunity)
            }
        }
    }

    private val callBackOpportunity : DataRecordWS.OnCallBackOpportunityListener = object : DataRecordWS.OnCallBackOpportunityListener{
        @SuppressLint("NotifyDataSetChanged")
        override fun onSuccess(listOpportunityDR: ArrayList<SubListDataRecord>) {
            progressBar.visibility = View.GONE
            refreshLayoutDataRecord.isRefreshing = false

            dataRecordModelList.clear()
            dataRecordModelList.addAll(listOpportunityDR)
            adapterOpportunityDR.notifyDataSetChanged()
        }

        override fun onFailed(error: String) {
            progressBar.visibility = View.GONE
            refreshLayoutDataRecord.isRefreshing = false

            Utils.customToastMsgError(mActivity, error, false)
        }
    }

    private fun onScrollInfoRecycle() {
        recyclerDataRecord.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true
                }
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                currentItem = linearLayoutManager.childCount
                total = linearLayoutManager.itemCount
                scrollDown = linearLayoutManager.findFirstVisibleItemPosition()
                if (dy > 0) {
                    if (isScrolling && currentItem + scrollDown == total) {
                        if (total == size) {
                            isScrolling = false
                            currentPage++
                            progressBar.visibility = View.VISIBLE
                            recyclerView.scrollToPosition(dataRecordModelList.size - 1)
                            requestServiceApi()
                            size += 10
                        } else {
                            progressBar.visibility = View.GONE
                        }
                    }
                }
            }
        })
    }

}