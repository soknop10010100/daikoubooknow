package com.eazy.daikou.ui.home.service_provider

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AbsListView
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.request_data.request.service_provider_ws.ServiceProviderV2Ws
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.my_property.service_provider.SubCategoryServiceProviderV2Model
import com.eazy.daikou.ui.home.service_provider.adapter.SubServiceProviderAdapter

class ServiceProviderListActivity : BaseActivity() {

    private lateinit var titleBar: TextView
    private lateinit var btnBack: ImageView
    private lateinit var refreshLayout: SwipeRefreshLayout
    private lateinit var recyclerView: RecyclerView
    private lateinit var noDataForDisplay: TextView
    private lateinit var progressBar: ProgressBar
    private var categoryKeyword : String = ""
    private var categoryName : String = ""
    private var currentItem = 0
    private var total : Int = 0
    private var scrollDown : Int = 0
    private var currentPage : Int = 1
    private var size : Int = 10
    private var isScrolling = false
    private var isScrollItem = false

    private lateinit var gridLayoutManager : GridLayoutManager
    private lateinit var adapterSubService: SubServiceProviderAdapter
    private var listSubService : ArrayList<SubCategoryServiceProviderV2Model> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sub_list_service_provider_v2)

        initView()

        initData()

        initAction()
    }

    private fun initView(){

        titleBar = findViewById(R.id.titleToolbar)
        btnBack = findViewById(R.id.iconBack)
        recyclerView = findViewById(R.id.recyclerViewSubService)
        noDataForDisplay = findViewById(R.id.noDataForDisplay)
        refreshLayout = findViewById(R.id.refreshLayout)
        progressBar = findViewById(R.id.progressItem)

    }

    private fun initData(){
        if ( intent != null && intent.hasExtra("category_keyword_item") || intent.hasExtra("category_name_item") ){
            categoryKeyword = intent.getStringExtra("category_keyword_item").toString()
            categoryName = intent.getStringExtra("category_name_item").toString()
        }
    }
    private fun initAction(){
        titleBar.text = categoryName
        btnBack.setOnClickListener { finish() }

        progressBar.visibility = View.GONE

        getServiceApiSub()
        initOnScrollItem()

        refreshLayout.setOnRefreshListener { onRefreshView() }
    }

    private fun onRefreshView(){
        currentPage = 1
        size = 10
        isScrolling = true
        adapterSubService.clear()
        getServiceApiSub()
    }

    private fun initOnScrollItem() {
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener(){
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true
                }
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                currentItem = gridLayoutManager.childCount
                total = gridLayoutManager.itemCount
                scrollDown = gridLayoutManager.findFirstVisibleItemPosition()
                if (dy > 0) {
                    if (isScrolling && currentItem + scrollDown == total) {
                        if (total == size) {
                            isScrolling = false
                            currentPage++
                            progressBar.visibility = View.VISIBLE
                            isScrollItem = true
                            recyclerView.scrollToPosition(listSubService.size - 1)
                            getServiceApiSub()
                            size += 10
                        } else {
                            progressBar.visibility = View.GONE
                        }
                    }
                }
            }
        })
    }

    private fun getServiceApiSub(){
        progressBar.visibility = View.VISIBLE
        ServiceProviderV2Ws().getSubCategoryServiceProviderWs(this, currentPage, size, categoryKeyword, callBackSubCategory)
    }

    private val callBackSubCategory : ServiceProviderV2Ws.OnCallBackSubCategory = object : ServiceProviderV2Ws.OnCallBackSubCategory{
        override fun onLoadSuccessFull(listSubServiceProvider: ArrayList<SubCategoryServiceProviderV2Model>) {
            progressBar.visibility = View.GONE
            refreshLayout.isRefreshing = false

            listSubService.addAll(listSubServiceProvider)
            initRecyclerView()
            noDataForDisplay.visibility = if (listSubServiceProvider.size > 0) View.GONE else View.VISIBLE

            adapterSubService.notifyDataSetChanged()

        }

        override fun onLoadFailed(message: String) {
            progressBar.visibility = View.GONE
            refreshLayout.isRefreshing = false
            Utils.customToastMsgError(this@ServiceProviderListActivity, message,false)
        }
    }
    private fun initRecyclerView(){
        gridLayoutManager = GridLayoutManager(this,1)
        recyclerView.layoutManager = gridLayoutManager
        adapterSubService = SubServiceProviderAdapter(this, listSubService, onClickCallBackSub)
        recyclerView.adapter = adapterSubService
    }

    private val  onClickCallBackSub = object  : SubServiceProviderAdapter.CallBackSubService{
        override fun callBackSubListener(subServiceModel: SubCategoryServiceProviderV2Model) {
            val intent = Intent(this@ServiceProviderListActivity,ServiceProviderListDetailActivity::class.java)
            intent.putExtra("id_detail", subServiceModel.id)
            startActivity(intent)
        }
    }
}