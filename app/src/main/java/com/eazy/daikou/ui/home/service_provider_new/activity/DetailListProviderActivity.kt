package com.eazy.daikou.ui.home.service_provider_new.activity

import android.content.Intent
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.ui.home.service_provider_new.adapter_service_provider.ListItemMyCartAdapter

class DetailListProviderActivity : BaseActivity() {

    private lateinit var chatMessageTv: TextView
    private lateinit var recyclerHistoryDetail : RecyclerView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_list_provider)

        initView()

        initData()

        initAction()
    }

    private fun initView(){
        chatMessageTv = findViewById(R.id.chatMessageTv)

        recyclerHistoryDetail = findViewById(R.id.recyclerHistoryDetail)
    }
    private fun initData(){

    }
    private fun initAction(){
        findViewById<ImageView>(R.id.btnBack).setOnClickListener { finish() }

        chatMessageTv.setOnClickListener( CustomSetOnClickViewListener{
            startActivity(Intent(this, ChatMessageActivity::class.java))
        })

        val linearLayoutManager = LinearLayoutManager(this)
        recyclerHistoryDetail.layoutManager = linearLayoutManager
        val adapterListHistory = ListItemMyCartAdapter(this)
        recyclerHistoryDetail.adapter = adapterListHistory
    }
}