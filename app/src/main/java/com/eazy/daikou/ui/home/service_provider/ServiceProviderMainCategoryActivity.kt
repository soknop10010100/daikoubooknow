package com.eazy.daikou.ui.home.service_provider

import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.request_data.request.service_provider_ws.ServiceProviderNewViewV3WS
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.hr.ItemHR
import com.eazy.daikou.model.my_property.service_provider.ServiceProviderCategoryModel
import com.eazy.daikou.model.service_provider.ListAllServiceProviderModel
import com.eazy.daikou.ui.home.service_provider_new.adapter_service_provider.MoreMenuCategoryAdapter

class ServiceProviderMainCategoryActivity : BaseActivity() {

    private lateinit var titleBar: TextView
    private lateinit var btnBack: ImageView
    private lateinit var btnSearch: ImageView
    private lateinit var noDataForDisplay: TextView
    private lateinit var progressBar: ProgressBar
    private lateinit var recyclerView: RecyclerView
    private var listItemService : ArrayList<ListAllServiceProviderModel> = ArrayList()
    private val listItem : ArrayList<ItemHR> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_service_provider_v2)

        initView()

        initAction()
    }

    private fun initView(){
        titleBar = findViewById(R.id.titleToolbar)
        btnBack = findViewById(R.id.iconBack)
        btnSearch = findViewById(R.id.addressMap)
        progressBar = findViewById(R.id.progressItem)
        noDataForDisplay = findViewById(R.id.noDataForDisplay)
        recyclerView = findViewById(R.id.recyclerListServiceV2)
        findViewById<LinearLayout>(R.id.layoutMap).visibility = View.VISIBLE
    }

    private fun  initAction(){
        titleBar.text = getString(R.string.service_provider)
       // btnSearch.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_home_create_list))

        btnBack.setOnClickListener { finish() }

//        btnSearch.setOnClickListener {
//            startActivity(Intent(this@ServiceProviderMainCategoryActivity,ServiceProviderListAndRegisterActivity::class.java))
//        }
        initRecyclerView()
        getServiceAPi()
    }

    private fun getListHomeMenu(listItemService : ArrayList<ServiceProviderCategoryModel>, context: Context): List<ItemHR> {
         val listItem : ArrayList<ItemHR> = ArrayList()
        var i = 0
        for (item in listItemService){
            i++
            listItem.add(ItemHR(item.category_keyword!!, setDrawableList(i), item.category_name!!, "", setColorBackground(i)))
        }
        return listItem
    }

    private fun getServiceAPi(){
        progressBar.visibility = View.VISIBLE
        ServiceProviderNewViewV3WS().getListAllOfServiceProviderWS(this, 1, 10,"1999", "category", callListAllServiceProvider)
    }

    private val callListAllServiceProvider: ServiceProviderNewViewV3WS.CallBackListAllProviderListener = object : ServiceProviderNewViewV3WS.CallBackListAllProviderListener{
        override fun onSuccessFul(allServiceProviderList: ArrayList<ListAllServiceProviderModel>) {
            progressBar.visibility = View.GONE
            listItemService.addAll(allServiceProviderList)
            noDataForDisplay.visibility = if (listItemService.size > 0) View.GONE else View.VISIBLE
        }
        override fun onLoadFailed(error: String) {
            progressBar.visibility = View.GONE
        }
    }

    private fun initRecyclerView(){
        val gridLayoutManager = GridLayoutManager(this, 2, RecyclerView.VERTICAL, false)
        recyclerView.layoutManager = gridLayoutManager
        val adapter = MoreMenuCategoryAdapter(this, listItemService , onClickCallBack)
        recyclerView.adapter = adapter
    }

    private val onClickCallBack: MoreMenuCategoryAdapter.CallBackItemListAllCategoryListener = object : MoreMenuCategoryAdapter.CallBackItemListAllCategoryListener{
        override fun clickViewAllCallBack(listAllProvider: ListAllServiceProviderModel) {
           Utils.customToastMsgError(this@ServiceProviderMainCategoryActivity, "service", true)
        }
    }

    private fun setColorBackground(id: Int): Int {
        return when (id) {
            1, 8, 15, 22, 29, 36 -> R.color.greenSea
            2, 9, 16, 23, 30 -> R.color.yellow
            3, 10, 17, 24, 31 -> R.color.red
            4, 11, 18, 25, 32 -> R.color.blue
            5, 12, 19, 26, 33-> R.color.color_wooden
            6, 13, 20, 27, 34-> R.color.appBarColorOld
            7, 14, 21, 28,36 ->  R.color.light_blue_600

            else -> R.color.appBarColorOld
        }
    }
    private fun setDrawableList(id: Int): Int{
        return when (id) {

            1 -> R.drawable.ic_cleaning_service_type
            2 -> R.drawable.security_guard
            3 -> R.drawable.ic_home_parting_v2
            4 -> R.drawable.ic_home_agency
            5 -> R.drawable.ic_home_property_list
            6 -> R.drawable.ic_home_landscape
            7 -> R.drawable.ic_home_pest_control
            8 -> R.drawable.ic_home_mep
            9 -> R.drawable.ic_home_elevator
            10 -> R.drawable.ic_home_cctv

            else -> R.drawable.no_image
        }
    }

}

