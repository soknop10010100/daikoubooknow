package com.eazy.daikou.ui.home.facility_booking.adapter;

import android.content.Context;
import android.content.res.ColorStateList;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.eazy.daikou.R;
import com.eazy.daikou.helper.Utils;
import com.eazy.daikou.model.facility_booking.VenuePropertyBookingModel;


import java.util.ArrayList;

public class VanueBookingAdapter extends RecyclerView.Adapter<VanueBookingAdapter.ViewHolder> {

    private final Context context;
    private final ArrayList<VenuePropertyBookingModel> venuePropertyBookingModelList;
    private final clickItemCallBackListener clickItemCallBackListener;

    public VanueBookingAdapter(Context context, ArrayList<VenuePropertyBookingModel> venuePropertyBookingModelList, clickItemCallBackListener clickItemCallBackListener){
        this.context = context;
        this.venuePropertyBookingModelList = venuePropertyBookingModelList;
        this.clickItemCallBackListener = clickItemCallBackListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
       View view = LayoutInflater.from(context).inflate(R.layout.layout_scroll_property_booking,parent,false);
       return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        VenuePropertyBookingModel venuePropertyBookingModel = venuePropertyBookingModelList.get(position);

        if (venuePropertyBookingModel != null){
            holder.name.setText(venuePropertyBookingModel.getVenueName());
            if (venuePropertyBookingModel.getClickTitle() != null){
                if (venuePropertyBookingModel.getClickTitle()){
                    holder.name.setTextColor(Utils.getColor(context,R.color.white));
                    holder.linearLayout.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(context, R.color.greenSea)));
                }else {
                    holder.name.setTextColor(Utils.getColor(context,R.color.gray));
                    holder.linearLayout.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(context, R.color.color_gray_item)));
                }
            }

            holder.itemView.setOnClickListener(view -> clickItemCallBackListener.clickItemCallBack(venuePropertyBookingModel));
        }
       }

    @Override
    public int getItemCount() {
        return venuePropertyBookingModelList.size();
    }

    public  static  class ViewHolder extends RecyclerView.ViewHolder{
        private final TextView name;
        private final LinearLayout linearLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.textScrollPropertyBooking);
            linearLayout = itemView.findViewById(R.id.cardView_TabScroll);
        }
    }

    public interface clickItemCallBackListener{
        void clickItemCallBack(VenuePropertyBookingModel venuePropertyBookingModel);
    }
}
