package com.eazy.daikou.ui.home.book_now.booking_hotel

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RatingBar
import android.widget.TextView
import androidx.cardview.widget.CardView
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.booking_hotel.NearbyHotelLocationModel
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.squareup.picasso.Picasso

class HotelDetailNearbyLocFragment : BottomSheetDialogFragment() {

    private lateinit var hotelName: TextView
    private lateinit var  address: TextView
    private lateinit var  imageView: ImageView
    private lateinit var  priceLayout : LinearLayout
    private lateinit var  ratingStar : RatingBar
    private lateinit var  priceTv : TextView
    private lateinit var  iconWishlist : ImageView
    private lateinit var hotelLocationModel: NearbyHotelLocationModel
    private var mContext : Context? = null
    private lateinit var cardViewLayout : CardView
    private lateinit var priceDiscount : TextView
    private lateinit var distanceTv : TextView
    private var category = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view =  inflater.inflate(R.layout.fragment_hotel_detail_nearby_loc, container, false)

        initView(view)

        initAction()

        return view
    }

    private fun initView(itemView: View){
        hotelName = itemView.findViewById(R.id.property_name)
        address = itemView.findViewById(R.id.addressTv)
        imageView = itemView.findViewById(R.id.image)
        priceLayout = itemView.findViewById(R.id.priceLayout)
        ratingStar = itemView.findViewById(R.id.ratingStar)
        priceTv =  itemView.findViewById(R.id.priceTv)
        iconWishlist = itemView.findViewById(R.id.iconWishlist)
        cardViewLayout = itemView.findViewById(R.id.cardViewLayout)
        priceDiscount = itemView.findViewById(R.id.priceDiscount)
        distanceTv = itemView.findViewById(R.id.distanceTv)
    }

    private fun initAction(){
        if (arguments != null && requireArguments().containsKey("nearby_hotel_location")) {
            hotelLocationModel = requireArguments().getSerializable("nearby_hotel_location") as NearbyHotelLocationModel
        }

        if (arguments != null && requireArguments().containsKey("category")) {
            category = requireArguments().getString("category") as String
        }

        if (hotelLocationModel.distance != null){
            distanceTv.text = hotelLocationModel.distance
        } else {
            distanceTv.visibility = View.GONE
        }

        if (hotelLocationModel.image != null) {
            Picasso.get().load(hotelLocationModel.image).into(imageView)
        } else {
            Picasso.get().load(R.drawable.no_image).into(imageView)
        }

        Utils.setTextStrikeStyle(priceDiscount)
        if (hotelLocationModel.original_price_display != null) {
            priceDiscount.text = String.format("%s %s", " ", hotelLocationModel.original_price_display)
        } else {
            priceDiscount.visibility = View.GONE
        }

        if (hotelLocationModel.price_display != null) {
            priceTv.text = String.format("%s %s", " ", hotelLocationModel.price_display)
        } else {
            priceTv.text = ". . ."
        }
        Utils.setValueOnText(hotelName, hotelLocationModel.name)

        ratingStar.rating =
            if (hotelLocationModel.star_rate != null && hotelLocationModel.star_rate != "") {
                if (hotelLocationModel.star_rate!!.toFloat() >= 0) {
                    hotelLocationModel.star_rate!!.toFloat()
                } else {
                    0.0F
                }
            } else {
                0.0F
            }

        ratingStar.visibility = View.VISIBLE
        priceLayout.visibility = View.VISIBLE

        if (hotelLocationModel.location != null)
        Utils.setValueOnText(address, hotelLocationModel.location!!.name)

        iconWishlist.visibility = View.VISIBLE
        Glide.with(iconWishlist)
            .load(if (hotelLocationModel.is_wishlist) R.drawable.ic_my_favourite else R.drawable.ic_favorite_border)
            .into(iconWishlist)

        cardViewLayout.setOnClickListener(
            CustomSetOnClickViewListener{
                if (hotelLocationModel != null) {
                    val intent = Intent(mContext, HotelBookingDetailHotelActivity::class.java)
                    intent.putExtra("category", category)
                    intent.putExtra("hotel_name", hotelLocationModel.name)
                    intent.putExtra("hotel_image", hotelLocationModel.image)
                    intent.putExtra("hotel_id", hotelLocationModel.id)
                    intent.putExtra("price", hotelLocationModel.price_display)
                    intent.putExtra("start_date", "")
                    intent.putExtra("end_date", "")
                    intent.putExtra("adults", "1")
                    intent.putExtra("children", "0")
                    startActivity(intent)
                    dismiss()
                }
            }
        )

    }

    companion object {
        @JvmName("newInstance1")
        @JvmStatic
        fun newInstance(category : String, nearbyHotelLocationModel: NearbyHotelLocationModel) =
            HotelDetailNearbyLocFragment().apply {
                arguments = Bundle().apply {
                    putSerializable("nearby_hotel_location", nearbyHotelLocationModel)
                    putSerializable("category", category)
                }
            }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
    }


}