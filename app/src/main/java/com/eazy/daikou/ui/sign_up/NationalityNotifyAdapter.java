package com.eazy.daikou.ui.sign_up;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.eazy.daikou.R;
import com.eazy.daikou.model.profile.NationalityList;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class NationalityNotifyAdapter extends RecyclerView.Adapter<NationalityNotifyAdapter.ViewHolder> {

    private final Context context;
    private final List<NationalityList> dataList;
    private final OnClickCallBack onClickCallBack;
    private int mSelectedPos = -1;
    private int stat;
    private boolean isClick = false;

    public NationalityNotifyAdapter(Context context, List<NationalityList> dataList, int stat  , OnClickCallBack onClickCallBack) {
        this.context = context;
        this.onClickCallBack = onClickCallBack;
        this.dataList = dataList;
        this.stat = stat;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.content_search, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        NationalityList nationalityList = dataList.get(position);
        holder.nameCategoryTv.setText(nationalityList.getNationality());

        holder.itemView.setOnClickListener(view -> {
            isClick = true;
            mSelectedPos = position;
            stat = mSelectedPos;
            onClickCallBack.onCLickAdapter(nationalityList.getNaum_code(), nationalityList.getNationality(),position,stat);
        });

        if(!isClick){
            if(stat == position){
                holder.checkClick.setVisibility(View.VISIBLE);
            }else {
                holder.checkClick.setVisibility(View.INVISIBLE);
            }
        }
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView nameCategoryTv;
        private final ImageView checkClick;
        public ViewHolder(@NonNull @NotNull View view) {
            super(view);
            checkClick = view.findViewById(R.id.checkClick);
            nameCategoryTv = view.findViewById(R.id.name);
        }
    }
    public interface OnClickCallBack{
        void onCLickAdapter(String id , String name, int post, int stat);
    }
}
