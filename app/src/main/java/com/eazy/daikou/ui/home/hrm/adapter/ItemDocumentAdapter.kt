package com.eazy.daikou.ui.home.hrm.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.model.hr.Document
import com.eazy.daikou.model.hr.Experience
import com.eazy.daikou.model.hr.ItemHR
import com.eazy.daikou.model.hr.Relationship

class ItemDocumentAdapter(private val listName: List<Document>, private val context : Context, private val itemClick : ItemClickOnListener) : RecyclerView.Adapter<ItemDocumentAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.layout_item_document_adapter, parent, false)
        return ViewHolder(v)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data: Document = listName[position]

        if(data.name != null){
            holder.nameTv.text = data.name
        }else{
            holder.nameTv.text = "- - - -"
        }

        if(data.issue_dt != null){
            holder.issueDateTv.text = data.issue_dt
        }else{
            holder.issueDateTv.text = "- - - -"
        }


        holder.btnReadFileTv.setOnClickListener {
            itemClick.onItemClick(data)
        }
    }

    override fun getItemCount(): Int {
        return listName.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var nameTv : TextView = view.findViewById(R.id.nameTv)
        var issueDateTv : TextView = view.findViewById(R.id.issueDateTv)
        var btnReadFileTv : CardView = view.findViewById(R.id.btnReadFile)
    }

    interface ItemClickOnListener{
        fun onItemClick(document: Document)
    }

}