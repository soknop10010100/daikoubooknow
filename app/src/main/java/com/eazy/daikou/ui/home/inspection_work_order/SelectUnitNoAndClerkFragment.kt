package com.eazy.daikou.ui.home.inspection_work_order

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.request_data.request.inspection_ws.InspectionWS
import com.eazy.daikou.request_data.static_data.StaticUtilsKey
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.model.my_property.service_provider.SelectPropertyModel
import com.eazy.daikou.model.profile.User
import com.eazy.daikou.model.inspection.Author_ClerkUserInfo
import com.eazy.daikou.ui.home.service_provider.adapter.ItemCategoryAdapter
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.gson.Gson
import java.util.*
import kotlin.collections.HashMap

class SelectUnitNoAndClerkFragment : BottomSheetDialogFragment() {

    private var linearLayoutManager : LinearLayoutManager? = null
    private lateinit var itemCategoryAdapter : ItemCategoryAdapter
    private lateinit var recycleView : RecyclerView
    private lateinit var progressBar : ProgressBar
    private lateinit var onClickAllAction: OnClickItemListener
    private lateinit var noItem : TextView
    private lateinit var searchPropertyEd : EditText
    private lateinit var txtCancel : TextView

    private var selectPropertyList : ArrayList<SelectPropertyModel> = ArrayList()
    private var selectEmployeeDepartmentList : ArrayList<Author_ClerkUserInfo> = ArrayList()

    private var clickOnCancel = false
    private var clickOnClearText = false
    private lateinit var action: String
    private var activeUserType: String = ""
    private var selectProperty: String = ""

    fun newInstance(propertyName: String?, userType : String, action: String): SelectUnitNoAndClerkFragment {
        val fragment = SelectUnitNoAndClerkFragment()
        val args = Bundle()
        args.putString("property_name", propertyName)
        args.putString("user_type", userType)
        args.putString("action", action)
        fragment.arguments = args
        return fragment
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {

        val view: View = inflater.inflate(R.layout.fragment_botton_sheet_provider, container, false)

        initView(view)

        initData()

        initAction()

        return view
    }

    private fun initView(view : View){
        view.findViewById<TextView>(R.id.titleItem).visibility = View.GONE
        view.findViewById<LinearLayout>(R.id.searchLayout).visibility = View.VISIBLE
        searchPropertyEd = view.findViewById(R.id.et_searchProject)
        txtCancel = view.findViewById(R.id.txtCancel)
        noItem = view.findViewById(R.id.noItemTv)
        noItem.visibility = View.VISIBLE
        noItem.text = noItem.context.getString(R.string.no_item)

        recycleView = view.findViewById(R.id.recyclerView)
        progressBar = view.findViewById(R.id.progress)
    }

    private fun initData(){
        if (arguments != null) {
            selectProperty = requireArguments().getString("property_name").toString()
        }
        if (arguments != null) {
            activeUserType = requireArguments().getString("user_type").toString()
        }
        if (arguments != null) {
            action = requireArguments().getString("action").toString()
        }

    }

    private fun initAction(){
        linearLayoutManager = LinearLayoutManager(requireContext())
        recycleView.layoutManager = linearLayoutManager

        if (action == StaticUtilsKey.department_action){
            initRecyclerView(StaticUtilsKey.department_action)
            searchPropertyEd.hint = context?.resources?.getString(R.string.search_clerk)
        } else {
            initRecyclerView(StaticUtilsKey.unit_no_action)
            searchPropertyEd.hint = context?.resources?.getString(R.string.search) + " " + context?.resources?.getString(R.string.unit_no)
        }

        initSearchAction()
    }

    private fun initRecyclerView(action_type: String) {

        if (action_type == StaticUtilsKey.unit_no_action){
            itemCategoryAdapter = ItemCategoryAdapter(selectPropertyList, action, clickCallBack)
            recycleView.adapter = itemCategoryAdapter

            initUnitNoList(false, "")
        } else if (action_type == StaticUtilsKey.department_action){
            itemCategoryAdapter = ItemCategoryAdapter(selectEmployeeDepartmentList, clickEmployeeCallBack)
            recycleView.adapter = itemCategoryAdapter

            requestEmployeeListByDepartment(false, "")
        }
    }

    private fun initUnitNoList(isSearch: Boolean, key_search: String){
        InspectionWS().getUnitNoInspection(requireContext(), hashMapData(isSearch, key_search), listCallBack)
    }

    private fun requestEmployeeListByDepartment(isSearch: Boolean, key_search: String){
        InspectionWS().selectEmployeeByDepartment(context, hashMapData(isSearch, key_search), listCallBack)
    }

    private fun hashMapData(isSearch : Boolean, key_search: String) : HashMap<String, Any>{
        val hashMap = HashMap<String, Any>()
        hashMap["property_id"] = Gson().fromJson(UserSessionManagement(requireContext()).userDetail, User::class.java).accountId
        if (activeUserType != "")   hashMap["user_type"] = activeUserType
        if (isSearch){
            hashMap["search"] = key_search
        }
        return hashMap
    }

    @SuppressLint("NotifyDataSetChanged")
    private val listCallBack: InspectionWS.CallBackUnitNoListener = object : InspectionWS.CallBackUnitNoListener {
        override fun onSuccessUnitList(selectPropertyGuests: List<SelectPropertyModel>?) {
            progressBar.visibility = View.GONE
            if (clickOnClearText){
                selectPropertyList.clear()
                clickOnClearText = false
            }
            selectPropertyList.addAll(selectPropertyGuests!!)

            for (property in selectPropertyList) {
                property.isClick = property.id == selectProperty
            }

            if (selectPropertyList.isEmpty()){
                recycleView.visibility = View.GONE
                noItem.visibility = View.VISIBLE
            } else{
                recycleView.visibility = View.VISIBLE
                noItem.visibility = View.INVISIBLE
            }

            itemCategoryAdapter.notifyDataSetChanged()
        }

        override fun onSuccessDepartmentEmployee(itemInspectionModels: MutableList<Author_ClerkUserInfo>?) {
            progressBar.visibility = View.GONE
            itemInspectionModels?.let { selectEmployeeDepartmentList.addAll(it) }

            for (employeeDepartment in selectEmployeeDepartmentList) {
                employeeDepartment.isClick = employeeDepartment.employeeId == selectProperty
            }

            if (selectEmployeeDepartmentList.isEmpty()){
                recycleView.visibility = View.GONE
                noItem.visibility = View.VISIBLE
            } else{
                recycleView.visibility = View.VISIBLE
                noItem.visibility = View.INVISIBLE
            }

            itemCategoryAdapter.notifyDataSetChanged()

        }

        override fun onFailed(message: String?) {
            progressBar.visibility = View.GONE
            Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
        }

    }

    private var clickCallBack  = ItemCategoryAdapter.ClickPropertyCallBack { selectPropertyItem->
        onClickAllAction.onClickSelect(selectPropertyItem)
        dismiss()
    }

    private var clickEmployeeCallBack  = ItemCategoryAdapter.ClickEmployeeCallBack { selectPropertyItem->
        onClickAllAction.onClickSelectEmployee(selectPropertyItem)
        dismiss()
    }

    interface OnClickItemListener{
        fun onClickSelect(selectPropertyGuest: SelectPropertyModel)
        fun onClickSelectEmployee(selectEmployee: Author_ClerkUserInfo)
    }


    private fun initSearchAction(){

        txtCancel.setOnClickListener {
            clickOnCancel = true
            searchPropertyEd.setText("")
            txtCancel.visibility = View.GONE
            clearText("")
        }

        searchPropertyEd.setOnEditorActionListener { _: TextView?, i: Int, _: KeyEvent? ->
            if (i == EditorInfo.IME_ACTION_SEARCH) {
                if (action == StaticUtilsKey.unit_no_action) {
                    initUnitNoList(true, searchPropertyEd.text.toString())
                } else{
                    requestEmployeeListByDepartment(true, searchPropertyEd.text.toString())
                }
            }
            true
        }

        searchPropertyEd.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun afterTextChanged(editable: Editable) {
                if (editable.toString().isNotEmpty()) {
                    txtCancel.visibility = View.VISIBLE
                    if (action == StaticUtilsKey.unit_no_action) {
                        if (selectPropertyList.isNotEmpty()) selectPropertyList.clear()
                        initUnitNoList(true, editable.toString())
                    } else{
                        if (selectEmployeeDepartmentList.isNotEmpty()) selectEmployeeDepartmentList.clear()
                        requestEmployeeListByDepartment(true, editable.toString())
                    }
                } else {
                    txtCancel.visibility = View.GONE
                    if (!clickOnCancel) {
                        clickOnClearText = true
                        clearText("")
                    }
                }
            }
        })
    }

    private fun clearText(key : String) {
        if (clickOnCancel) {
            clickOnCancel = false
        }
        progressBar.visibility = View.VISIBLE
        if(action == StaticUtilsKey.unit_no_action) {
            if (selectPropertyList.isNotEmpty()) selectPropertyList.clear()
            initUnitNoList(false, key)
        } else{
            if (selectEmployeeDepartmentList.isNotEmpty()) selectEmployeeDepartmentList.clear()
            requestEmployeeListByDepartment(false, key)
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        val parent = parentFragment
        onClickAllAction = if (parent != null) {
            parent as OnClickItemListener
        } else {
            context as OnClickItemListener
        }
    }


}