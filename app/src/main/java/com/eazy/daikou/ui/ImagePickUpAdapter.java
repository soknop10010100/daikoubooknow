package com.eazy.daikou.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.Image;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.eazy.daikou.R;
import com.eazy.daikou.base.BaseAdapter;

import java.util.ArrayList;
import java.util.List;

public class ImagePickUpAdapter extends BaseAdapter {

    private ImageView imageView;
    private ImageView imageClear;
    private final OnClearImage onClearImage;
    private final List<Bitmap> listImage;
    private final int post;

    public ImagePickUpAdapter(Context context,int post, List<?> dataList, OnClearImage onClearImage) {
        super(context, R.layout.layout_notification_pickup_image, dataList);
        this.onClearImage = onClearImage;
        this.listImage = (List<Bitmap>) dataList;
        this.post = post;
    }

    @Override
    public View getView(View view) {
        imageClear = view.findViewById(R.id.btn_clear);
        imageView = view.findViewById(R.id.image_view);
        return view;
    }

    @Override
    public void onBindViewHold(int position, Object itemView) {
        Bitmap bitmap = listImage.get(position);
        Glide.with(imageView).load(bitmap).into(imageView);
        imageClear.setOnClickListener(v -> onClearImage.onClickRemove(bitmap, post, (position +1)));
    }

    public interface OnClearImage{
        void onClickRemove(Bitmap bitmap, int post, int position_image);
    }
}
