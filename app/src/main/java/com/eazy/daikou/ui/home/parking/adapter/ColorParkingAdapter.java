package com.eazy.daikou.ui.home.parking.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.ColorStateList;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.eazy.daikou.R;
import com.eazy.daikou.model.parking.ColorCar;

import java.util.ArrayList;

public class ColorParkingAdapter extends RecyclerView.Adapter<ColorParkingAdapter.ViewHolder> {

    private final ArrayList<ColorCar> colorCars;
    private final ItemClickColor itemClick;
    private int mSelectedPos = -1;
    private int stat;
    private boolean isClick = false ;

    public ColorParkingAdapter(int post, ArrayList<ColorCar> colorCars, ItemClickColor itemClick) {
        this.colorCars = colorCars;
        this.itemClick = itemClick;
        this.stat = post;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view  = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_item_color_parking,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        ColorCar color = colorCars.get(position);
        if(color!=null){

            holder.linearLayout.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(holder.linearLayout.getContext(),color.getColorId())));

            holder.itemView.setOnClickListener(view -> {
                isClick = true;
                mSelectedPos = position;
                stat = mSelectedPos;
                itemClick.onClickItem(color.getColorName(),position,stat);
            });
            if(isClick){
                if (position == mSelectedPos||position == stat){
                    holder.checkImage.setVisibility(View.VISIBLE);
                } else {
                    holder.checkImage.setVisibility(View.GONE);

                }
            }

        }

    }

    @Override
    public int getItemCount() {
        return colorCars.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        ImageView checkImage;
        RelativeLayout linearLayout;
        public ViewHolder(View view) {
            super(view);
            linearLayout = view.findViewById(R.id.linear_color);
            checkImage = view.findViewById(R.id.tech_image);
        }


    }
    public interface ItemClickColor {
        void onClickItem(String listString,int position,int stat);
    }
}