package com.eazy.daikou.ui.home.data_record

import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.ui.home.data_record.adapter.AdapterPayLateRequestPayment

class PayLateRequestDataRecordActivity : BaseActivity() {

    private lateinit var recyclerPayLate : RecyclerView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pay_late_request_data_record)

        initView()

        initAction()
    }

    private fun initView(){
        recyclerPayLate = findViewById(R.id.recyclerPayLate)
    }

    private fun initAction(){
        findViewById<ImageView>(R.id.iconBack).setOnClickListener{ finish()}
        findViewById<TextView>(R.id.titleToolbar).text = getString(R.string.request_late_payment)

        val linearLayout = LinearLayoutManager(this)
        recyclerPayLate.layoutManager = linearLayout
        val adapterPayLate = AdapterPayLateRequestPayment(this)
        recyclerPayLate.adapter = adapterPayLate
    }
}