package com.eazy.daikou.ui.home.rules_Announcement.AnnouncementAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.eazy.daikou.R;
import com.eazy.daikou.model.rule_announcement.RulesModel;

import java.util.List;

public class AnnouncementAdapter extends RecyclerView.Adapter<AnnouncementAdapter.ViewHolder>{

    private Context context;
    private List<RulesModel> rulesModelList;
    private ItemClickCallBack itemClickCallBack;

    public AnnouncementAdapter(Context context, List<RulesModel> rulesModelList, ItemClickCallBack itemClickCallBack){
        this.context = context;
        this.rulesModelList = rulesModelList;
        this.itemClickCallBack = itemClickCallBack;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_rule_announcement,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        RulesModel rulesModel = rulesModelList.get(position);
        if (rulesModel != null){
            holder.title_Rule.setText(rulesModel.getTitle());
            holder.date_Rule.setText(rulesModel.getTimePostToNow().getValue() + "");
            holder.month_Rule.setText(rulesModel.getTimePostToNow().getType());

            holder.itemView.setOnClickListener(view -> itemClickCallBack.clickCallback(rulesModel));
        }

    }

    @Override
    public int getItemCount() {
        return rulesModelList.size();
    }

    public  static  class  ViewHolder extends RecyclerView.ViewHolder{

        private  final TextView title_Rule,date_Rule,month_Rule;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            title_Rule = itemView.findViewById(R.id.title_Rule);
            date_Rule = itemView.findViewById(R.id.date_Rule);
            month_Rule = itemView.findViewById(R.id.month_Rule);
        }
    }

    public interface ItemClickCallBack{
        void clickCallback(RulesModel rulesModel);
    }
}
