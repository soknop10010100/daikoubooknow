package com.eazy.daikou.ui.home.facility_booking.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.helper.DateUtil
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.facility_booking.my_booking.MyBookingModel

class MyBookingAdapter(private val list : ArrayList<MyBookingModel>, private val onClickCallBack : ClickCallBackListener) : RecyclerView.Adapter<MyBookingAdapter.ItemViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        return ItemViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.my_booking_item_model, parent, false))
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val myBookingModel = list[position]
        if (myBookingModel != null){
            holder.dateTv.text = if (myBookingModel.date_time != null) DateUtil.formatDateComplaint(myBookingModel.created_at) else "- - - - -"
            holder.timeTv.text = if (myBookingModel.date_time != null) Utils.formatDateTime(myBookingModel.created_at,"yyyy-MM-dd h:mm:ss","kk : mm") else "- - - - -"

            Utils.setValueOnText(holder.priceTv, myBookingModel.amount_to_pay + " $")
            Utils.setValueOnText(holder.paymentStatusTv, myBookingModel.payment_status)
            Utils.setValueOnText(holder.statusTvTv, myBookingModel.status)

            holder.cardViewRingLayout.setOnClickListener{ onClickCallBack.onClickCallBack(myBookingModel)}
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val dateTv: TextView = itemView.findViewById(R.id.dateTv)
        val priceTv: TextView = itemView.findViewById(R.id.priceTv)
        val cardViewRingLayout : CardView = itemView.findViewById(R.id.cardViewRing)
        val paymentStatusTv : TextView = itemView.findViewById(R.id.paymentStatusTv)
        val statusTvTv : TextView = itemView.findViewById(R.id.statusTv)
        val timeTv : TextView = itemView.findViewById(R.id.timeTv)
    }

    interface ClickCallBackListener{
        fun onClickCallBack(itemBooking: MyBookingModel)
    }

    fun clear(){
        val size = list.size
        if (size > 0) {
            for (i in 0 until size) {
                list.removeAt(0)
            }
            notifyItemRangeRemoved(0, size)
        }
    }

}