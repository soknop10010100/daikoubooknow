package com.eazy.daikou.ui.home.home_menu.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.home.HomeViewModel

class HomeIconMenuAdapter(private val listName: List<HomeViewModel>, private val itemClick : ItemClickOnListener) : RecyclerView.Adapter<HomeIconMenuAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.home_icon_menu_layout, parent, false)
        return ViewHolder(v)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data: HomeViewModel = listName[position]
        if (data != null) {
            holder.nameItemTv.text = if (data.name != null) data.name else "- - -"
            Glide.with(holder.imageView).load(if (data.drawable != null) data.drawable else R.drawable.no_image).into(holder.imageView)
            if (data.action == "booking"){
                holder.imageView.setColorFilter(Utils.getColor(holder.imageView.context, R.color.transparent))
            }
            holder.cardAdapter.setOnClickListener(CustomSetOnClickViewListener{
                itemClick.onItemClick(data)
            })
        }
    }

    override fun getItemCount(): Int {
        return listName.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var nameItemTv : TextView = view.findViewById(R.id.name_item_hr)
        var imageView : ImageView = view.findViewById(R.id.image_hr)
        var cardAdapter : CardView = view.findViewById(R.id.card_hr)
    }

    interface ItemClickOnListener{
        fun onItemClick(homeViewModel : HomeViewModel)
    }

}