package com.eazy.daikou.ui.home.facility_booking

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.request_data.request.facility_booking_ws.FacilityBookingWs
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.facility_booking.my_booking.MyBookingDetailByItemModel
import com.eazy.daikou.ui.home.facility_booking.adapter.ActivityLogBookingAdapter
import com.eazy.daikou.ui.home.facility_booking.adapter.AvailableDayAdapter
import com.eazy.daikou.ui.ViewPagerItemImagesAdapter
import com.eazy.daikou.ui.profile.ChangeLanguageFragment
import com.github.clans.fab.FloatingActionButton
import com.github.clans.fab.FloatingActionMenu
import com.google.android.material.tabs.TabLayout
import de.hdodenhof.circleimageview.CircleImageView

class MyBookingByItemDetailActivity : BaseActivity() {

    private lateinit var imageCoverBooking: ImageView
    private lateinit var noImage: TextView
    private lateinit var propertyTv: TextView
    private lateinit var titleTv: TextView
    private lateinit var logActivityRecycle : RecyclerView
    private lateinit var availableDayRecycle : RecyclerView
    private lateinit var availableDayTv : TextView

    private lateinit var linearOverView: LinearLayout
    private lateinit var linearPhoto: LinearLayout
    private lateinit var linearLogActivity : LinearLayout

    private lateinit var linearLayoutMenus: LinearLayout

    private lateinit var viewPager: ViewPager
    private lateinit var tabLayout: TabLayout
    private lateinit var viewpagerAdapter: ViewPagerItemImagesAdapter
    private lateinit var progressBar: ProgressBar
    private lateinit var imgProperty : CircleImageView
    private lateinit var totalPriceTv : TextView
    private lateinit var specificTaxTv : TextView
    private lateinit var originalPriceTv : TextView
    private lateinit var lightingTaxTv : TextView
    private lateinit var vatPriceTv : TextView
    private lateinit var descriptionTv : TextView
    private lateinit var mainScrollLayout : ScrollView

    private lateinit var floatingActionMenu: FloatingActionMenu
    private lateinit var fabScanQrCode: FloatingActionButton
    private  lateinit var fabMyQrCode : FloatingActionButton

    private var myBookingId = ""
    private var bookingStatus = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_booking_by_item_detail)

        initView()

        initData()

        initAction()

    }

    private  fun initData(){
        if (intent != null && intent.hasExtra("my_booking_id")) {
            myBookingId = intent.getStringExtra("my_booking_id").toString()
            bookingStatus = intent.getStringExtra("booking_status").toString()
        }
    }

    private fun initView(){
        findViewById<TextView>(R.id.btn_back).setOnClickListener{finish()}
        imageCoverBooking = findViewById(R.id.img_cover)

        linearLayoutMenus = findViewById(R.id.linearLayout)
        linearOverView = findViewById(R.id.linearOverView)
        linearPhoto = findViewById(R.id.linearPhoto)
        linearLogActivity = findViewById(R.id.linearLogActivity)
        logActivityRecycle = findViewById(R.id.logActivityRecycle)
        availableDayRecycle = findViewById(R.id.availableDayRecycle)
        availableDayTv = findViewById(R.id.availableDayTv)

        titleTv = findViewById(R.id.titleTv)
        propertyTv = findViewById(R.id.propertyTv)
        noImage = findViewById(R.id.countItem)
        viewPager = findViewById(R.id.viewPagers)
        tabLayout = findViewById(R.id.tab_layout)
        progressBar = findViewById(R.id.progressItem)
        imgProperty = findViewById(R.id.imgProperty)

        totalPriceTv = findViewById(R.id.totalPriceTv)
        originalPriceTv = findViewById(R.id.originalPriceTv)
        specificTaxTv = findViewById(R.id.specificTv)
        lightingTaxTv = findViewById(R.id.lightingTaxTv)
        vatPriceTv = findViewById(R.id.vatTv)
        descriptionTv = findViewById(R.id.descriptionTv)
        mainScrollLayout = findViewById(R.id.main_layout)

        floatingActionMenu = findViewById(R.id.fabMenu)
        fabMyQrCode = findViewById(R.id.iconQrCode)
        fabScanQrCode = findViewById(R.id.iconScanQr)
    }

    @SuppressLint("ResourceAsColor")
    private fun initAction(){
        FacilityBookingWs().getMyBookingDetailByDetail(this, myBookingId, object  : FacilityBookingWs.CallBackBookingItemDetailListener{
            override fun onLoadSuccessFull(selectPropertyGuests: MyBookingDetailByItemModel) {
                progressBar.visibility = View.GONE
                mainScrollLayout.visibility = View.VISIBLE

                if (selectPropertyGuests != null)   setValue(selectPropertyGuests)
            }

            override fun onLoadFailed(message: String?) {
                progressBar.visibility = View.GONE
                mainScrollLayout.visibility = View.VISIBLE
                Utils.customToastMsgError(this@MyBookingByItemDetailActivity, message, false)
            }

        })

        setLinearLayout()

        initActionOnMenuFab()
    }

    private fun setValue(myBookingItemDetail: MyBookingDetailByItemModel){
        if (myBookingItemDetail.space_info != null){
            if (myBookingItemDetail.space_info!!.name != null){
                titleTv.text = myBookingItemDetail.space_info!!.name
                setViewPagerFacility(myBookingItemDetail.space_info!!.images as ArrayList<String>)
            } else {
                titleTv.text = ". . ."
            }
            descriptionTv.text = if (myBookingItemDetail.space_info!!.description != null) myBookingItemDetail.space_info!!.description else ". . ."
        } else {
            titleTv.text = ". . ."
        }
        if (myBookingItemDetail.property_info != null){
            if (myBookingItemDetail.property_info!!.logo != null){
                Glide.with(imgProperty).load(myBookingItemDetail.property_info!!.logo).into(imgProperty)
            }
            propertyTv.text = if (myBookingItemDetail.property_info!!.name != null) myBookingItemDetail.property_info!!.name else ". . . . ."
        }


        setValueOnPrice(totalPriceTv, myBookingItemDetail.total_amount)
        setValueOnPrice(originalPriceTv, myBookingItemDetail.amount)
        setValueOnPrice(specificTaxTv, myBookingItemDetail.tax_show_amount)
        setValueOnPrice(vatPriceTv, myBookingItemDetail.tax_vat_amount)
        setValueOnPrice(lightingTaxTv, myBookingItemDetail.tax_relax_amount)

        floatingActionMenu.visibility = if (bookingStatus == "completed") View.VISIBLE else View.GONE

        initRecycleLog()

        initRecycleOnAvailableDay(myBookingItemDetail.available_day)
    }

    private fun setValueOnPrice(textView : TextView, value : String?){
        textView.text = if (value != null) "$value $" else "0 $"
    }

    private fun setLinearLayout() {
        for (i in 0 until linearLayoutMenus.childCount) {
            linearLayoutMenus.getChildAt(i).setOnClickListener {
                setClickOnMenuHeader(i)
                when (i) {
                    0 -> {
                        linearOverView.visibility = View.VISIBLE
                        linearPhoto.visibility = View.GONE
                        linearLogActivity.visibility = View.GONE
                    }
                    1 -> {
                        linearOverView.visibility = View.GONE
                        linearPhoto.visibility = View.VISIBLE
                        linearLogActivity.visibility = View.GONE
                    }
                    2 -> {
                        linearPhoto.visibility = View.GONE
                        linearOverView.visibility = View.GONE
                        linearLogActivity.visibility = View.VISIBLE
                    }
                }
            }
        }
    }

    private fun setClickOnMenuHeader(backUpId : Int){
        for (i in 0 until linearLayoutMenus.childCount) {
            val rowView: View = linearLayoutMenus.getChildAt(i)
            if (rowView is TextView) {
                if (i == backUpId) {
                    rowView.background = ResourcesCompat.getDrawable(resources, R.drawable.custom_shape_line_bottom_appbar, null)
                    rowView.setTextColor(Utils.getColor(this, R.color.white))
                } else {
                    rowView.background = null
                    rowView.setTextColor(Utils.getColor(this, R.color.gray))
                }
            }
        }

    }

    private fun setViewPagerFacility(image : ArrayList<String>){
        if (image.size > 0 ) {
            Glide.with(this).load(image[0]).into(imageCoverBooking)
            viewpagerAdapter = ViewPagerItemImagesAdapter(this, image)
            viewPager.adapter = viewpagerAdapter
            tabLayout.setupWithViewPager(viewPager, true)
        } else {
            noImage.visibility = View.VISIBLE
            viewPager.visibility = View.GONE
        }
    }

    private fun initRecycleLog(){
        logActivityRecycle.layoutManager = LinearLayoutManager(this)
        logActivityRecycle.adapter = ActivityLogBookingAdapter()
        logActivityRecycle.isNestedScrollingEnabled = false
    }

    private fun initRecycleOnAvailableDay(spacePricingItem : ArrayList<String>){
        availableDayRecycle.layoutManager = GridLayoutManager(this, 3)
        val availableDayAdapter = AvailableDayAdapter("detail", spacePricingItem)
        availableDayRecycle.adapter = availableDayAdapter

        if (spacePricingItem.isEmpty()){
            availableDayTv.append(" : (" + resources.getString(R.string.no_fee) + ")")
        }
    }


    private fun initActionOnMenuFab(){
        val viewCover = findViewById<View>(R.id.view)
        hideShowOnFab(false)
        floatingActionMenu.setOnMenuToggleListener {
            if (floatingActionMenu.isOpened) {
                viewCover.visibility = View.VISIBLE
                hideShowOnFab(true)
            } else {
                viewCover.visibility = View.GONE
                hideShowOnFab(false)
            }
        }

        viewCover.setOnClickListener {
            floatingActionMenu.close(true)
        }

        fabScanQrCode.setOnClickListener{
            // startActivity(Intent(this, ScannerActivity::class.java))
            floatingActionMenu.close(true)
        }

        fabMyQrCode.setOnClickListener{
            selectChangeLanguage("qr_code_user")
            floatingActionMenu.close(true)
        }
    }

    private fun selectChangeLanguage(key: String) {
        val changeLanguageAlert = ChangeLanguageFragment.newInstance(key)
        changeLanguageAlert.show(supportFragmentManager, changeLanguageAlert.javaClass.simpleName)
    }

    private fun hideShowOnFab(isHideMenu : Boolean) {
        fabScanQrCode.visibility = if (isHideMenu)  View.VISIBLE else View.INVISIBLE
        fabMyQrCode.visibility = if (isHideMenu)  View.VISIBLE else View.INVISIBLE
    }
}