package com.eazy.daikou.ui.home.my_property_v1.floor_plan_menu;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.bumptech.glide.Glide;
import com.github.chrisbanes.photoview.PhotoView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.eazy.daikou.R;
import com.eazy.daikou.helper.Utils;

import java.util.Objects;

public class ZoomImageDetailActivity extends AppCompatActivity {

    private String getImgLink;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zoom_image_detail);
        Objects.requireNonNull(getSupportActionBar()).hide();
        Utils.hideStatusBar(this);


        FloatingActionButton iconClose = findViewById(R.id.close);
        PhotoView photoView = findViewById(R.id.photo_view);
        Objects.requireNonNull(iconClose.getContentBackground()).setAlpha(38);
        iconClose.setOnClickListener(view -> finish());

        if (getIntent() != null && getIntent().hasExtra("image")){
            getImgLink = getIntent().getStringExtra("image");
        }
        Glide.with(this).load(getImgLink).into(photoView);
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}