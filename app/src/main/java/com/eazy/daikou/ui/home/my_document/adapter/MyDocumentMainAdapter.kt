package com.eazy.daikou.ui.home.my_document.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.home.HomeViewModel

class MyDocumentMainAdapter(private val action: String, private val listName: List<HomeViewModel>, private val itemClick: ItemClickOnListener) :
    RecyclerView.Adapter<MyDocumentMainAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.my_document_main_model, parent, false)
        return ViewHolder(v)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data: HomeViewModel = listName[position]
        if (data != null) {
            holder.myUnitMenuLayout.visibility = if (action == "my_document")   View.GONE else View.VISIBLE
            holder.myDocumentLayout.visibility = if (action == "my_document")   View.VISIBLE else View.GONE
            if (action == "my_document"){
                holder.nameItemTv.text = if (data.name != null) data.name else "- - -"
                Glide.with(holder.imageView).load(if (data.drawable != null) data.drawable else R.drawable.no_image).into(holder.imageView)
                holder.numberRecord.text = if (data.category_item != null) data.category_item else "- - -"
            } else {
                holder.nameTv.text = if (data.name != null) data.name else "- - -"
                Glide.with(holder.imageMyUnit).load(if (data.drawable != null) data.drawable else R.drawable.no_image).into(holder.imageMyUnit)
                holder.descriptionTv.text = if (data.category_item != null) data.category_item else "- - -"
                Utils.setBgTint(holder.cardIcon, data.color)
            }
            holder.cardAdapter.setOnClickListener { itemClick.onItemClick(data) }
        }
    }

    override fun getItemCount(): Int {
        return listName.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var nameItemTv : TextView = view.findViewById(R.id.name_item_hr)
        var imageView : ImageView = view.findViewById(R.id.image_hr)
        var cardAdapter : CardView = view.findViewById(R.id.card_hr)
        var numberRecord : TextView = view.findViewById(R.id.numberRecord)

        val nameTv : TextView = view.findViewById(R.id.nameTv)
        var imageMyUnit : ImageView = view.findViewById(R.id.imageMyUnit)
        var descriptionTv : TextView = view.findViewById(R.id.descriptionTv)
        var myUnitMenuLayout : LinearLayout = view.findViewById(R.id.myUnitMenuLayout)
        var myDocumentLayout : LinearLayout = view.findViewById(R.id.myDocumentLayout)
        var cardIcon : CardView = itemView.findViewById(R.id.cardIcon)
    }

    interface ItemClickOnListener{
        fun onItemClick(homeViewModel : HomeViewModel)
    }

}