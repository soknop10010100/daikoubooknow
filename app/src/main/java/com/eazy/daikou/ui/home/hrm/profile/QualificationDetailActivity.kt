package com.eazy.daikou.ui.home.hrm.profile

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.model.hr.Qualification
import com.eazy.daikou.ui.home.hrm.adapter.ItemQualificationAdapter

class QualificationDetailActivity : BaseActivity() {

   private lateinit var qualificationTv: TextView
   private lateinit var majorTv: TextView
   private lateinit var schoolTv: TextView
   private lateinit var schoolAddressTv: TextView
   private lateinit var joiningDateTv: TextView
   private lateinit var graduateDateTv: TextView
   private lateinit var issueDateTv: TextView

   private lateinit var btnBack:TextView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_qualidication_detail)

        initView()

        initData()

    }

    private fun initView() {
        qualificationTv = findViewById(R.id.qualificationTV)
        majorTv = findViewById(R.id.majorTv)
        schoolTv = findViewById(R.id.schoolTv)
        schoolAddressTv = findViewById(R.id.schoolAddress)
        joiningDateTv = findViewById(R.id.joiningDate)
        graduateDateTv = findViewById(R.id.graduateDate)
        issueDateTv = findViewById(R.id.issueDateTv)

        btnBack = findViewById(R.id.btn_back)


    }

    private fun initData() {

        btnBack.setOnClickListener { finish() }

        if (intent != null && intent.hasExtra("qualification_model")){
            val qualification = intent.getSerializableExtra("qualification_model") as Qualification
            setValues(qualification)
        }

    }

    private fun setValues(qualification: Qualification){

        if (qualification.qualification != null){
            qualificationTv.text = qualification.qualification
        }else{
            qualificationTv.text="- - - - -"
        }

        if (qualification.major != null){
            majorTv.text = qualification.major
        }else{
            majorTv.text="- - - - -"
        }

        if (qualification.school != null){
            schoolTv.text = qualification.school
        }else{
            schoolTv.text="- - - - -"
        }
        if (qualification.school_addr != null){
            schoolAddressTv.text = qualification.school_addr
        }else{
            schoolAddressTv.text="- - - - -"
        }

        if (qualification.joining_date != null){
            joiningDateTv.text = qualification.joining_date
        }else{
            joiningDateTv.text="- - - - -"
        }

        if (qualification.gratuate_date != null){
            graduateDateTv.text = qualification.gratuate_date
        }else{
            graduateDateTv.text="- - - - -"
        }

        if (qualification.issue_date != null){
            issueDateTv.text = qualification.issue_date
        }else{
            issueDateTv.text="- - - - -"
        }

    }

}