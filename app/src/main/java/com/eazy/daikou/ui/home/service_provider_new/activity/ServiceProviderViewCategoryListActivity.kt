package com.eazy.daikou.ui.home.service_provider_new.activity

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.CompositePageTransformer
import androidx.viewpager2.widget.MarginPageTransformer
import androidx.viewpager2.widget.ViewPager2
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.service_provider.ImageSliderModel
import com.eazy.daikou.model.service_provider.MainItemModel
import com.eazy.daikou.model.service_provider.SubCategory
import com.eazy.daikou.ui.home.service_provider_new.adapter_service_provider.MainItemAdapter
import com.eazy.daikou.ui.home.service_provider_new.adapter_service_provider.ServiceProviderListAdapter
import com.eazy.daikou.ui.home.service_provider_new.adapter_service_provider.SlideImageAdapter
import kotlin.math.abs

class ServiceProviderViewCategoryListActivity : BaseActivity() {

    private lateinit var recyclerCategoryProvider: RecyclerView
    private lateinit var imageSliderViewCategoryVP: ViewPager2
    private val sliderHandler: Handler = Handler(Looper.getMainLooper())
    private var dataimageslider: ArrayList<ImageSliderModel> = ArrayList()
    private var mainItemList: ArrayList<MainItemModel> = ArrayList()
    private var titleCategory = ""

    private lateinit var progressBar: ProgressBar
    private var serviceList : ArrayList<SubCategory> = ArrayList()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_service_provider_list)

        initView()

        initData()

        initAction()
    }

    private fun initView(){
        recyclerCategoryProvider = findViewById(R.id.recyclerCategoryProvider)
        imageSliderViewCategoryVP = findViewById(R.id.imageSliderViewCategoryVP)
    }

    private fun initData(){
        if (intent != null && intent.hasExtra("data_image_slider")){
            dataimageslider = intent.getSerializableExtra("data_image_slider") as ArrayList<ImageSliderModel>
        }
        if (intent != null && intent.hasExtra("data_main_model")){
            mainItemList = intent.getSerializableExtra("data_main_model") as ArrayList<MainItemModel>
        }
        if (intent != null && intent.hasExtra("name_category")){
            titleCategory = intent.getStringExtra("name_category") as String
        }
    }
    private fun initAction(){

        findViewById<TextView>(R.id.titleToolbar).text = titleCategory
        findViewById<ImageView>(R.id.iconBack).setOnClickListener { finish() }
        progressBar = findViewById(R.id.progressItem)

        slideImageView()
        progressBar.visibility = View.GONE


        val linearLayoutManager = LinearLayoutManager (this)
        recyclerCategoryProvider.layoutManager = linearLayoutManager
        val mainCategoryAdapter = MainItemAdapter(this, this, mainItemList, callBackCategory)
        recyclerCategoryProvider.adapter = mainCategoryAdapter
    }

    private val callBackCategory: MainItemAdapter.CallBackItemSeeAll = object : MainItemAdapter.CallBackItemSeeAll{
        override fun clickSeeAllListener(mainItem: MainItemModel) {
            Utils.customToastMsgError(this@ServiceProviderViewCategoryListActivity, "coming soon!!", false)
        }

        override fun clickCompanyToActivityListener(companyData: SubCategory) {
            Utils.customToastMsgError(this@ServiceProviderViewCategoryListActivity, "coming soon!!", false)
        }

        override fun clickServiceProviderListener(Service: SubCategory) {
            Utils.customToastMsgError(this@ServiceProviderViewCategoryListActivity, "coming soon!!", false)
        }

    }
    private fun slideImageView(){
        imageSliderViewCategoryVP.adapter = SlideImageAdapter(dataimageslider, imageSliderViewCategoryVP)
        imageSliderViewCategoryVP.clipToPadding = false
        imageSliderViewCategoryVP.clipChildren = false
        imageSliderViewCategoryVP.offscreenPageLimit = 3
        imageSliderViewCategoryVP.getChildAt(0).overScrollMode = RecyclerView.OVER_SCROLL_NEVER;
        val compositePageTransformer = CompositePageTransformer()
        compositePageTransformer.addTransformer(MarginPageTransformer(40))

        compositePageTransformer.addTransformer { page, position ->
            val r = 1 - abs(position)
            page.scaleY = 0.85f + r * 0.15f
        }

        imageSliderViewCategoryVP.setPageTransformer(compositePageTransformer)

        imageSliderViewCategoryVP.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                sliderHandler.removeCallbacks(sliderRunnable)
                sliderHandler.postDelayed(sliderRunnable, 3000)// slide duration 4 seconds
            }
        })
    }

    private val sliderRunnable =
        Runnable { imageSliderViewCategoryVP.currentItem = imageSliderViewCategoryVP.currentItem + 1 }

    override fun onPause() {
        super.onPause()
        sliderHandler.removeCallbacks(sliderRunnable)
    }

    override fun onResume() {
        super.onResume()
        sliderHandler.postDelayed(sliderRunnable, 3000)
    }
}