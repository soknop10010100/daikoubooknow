package com.eazy.daikou.ui.contact;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentContainerView;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.eazy.daikou.base.BaseFragment;
import com.github.ybq.android.spinkit.sprite.Sprite;
import com.github.ybq.android.spinkit.style.MultiplePulse;
import com.eazy.daikou.R;
import com.eazy.daikou.helper.Utils;

import java.util.Locale;

public class ContactFragment extends BaseFragment {

    private TextView propertyBtn;
    private View view1,view2;
    private FragmentContainerView fragmentContainerView;
    private View   root;
    @SuppressLint("StaticFieldLeak")
    public static LinearLayout btnAdd;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @SuppressLint("ResourceAsColor")
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.contact_layout, container, false);

        //Title Toolbar
        btnAdd = root.findViewById(R.id.layoutMap);
        root.findViewById(R.id.iconBack).setVisibility(View.GONE);
        root.findViewById(R.id.customToolbar).setVisibility(View.VISIBLE);
        TextView titleToolbar = root.findViewById(R.id.titleToolbar);
        titleToolbar.setText(Utils.getText(mActivity, R.string.contact).toUpperCase(Locale.ROOT));
        ImageView iconAdd = root.findViewById(R.id.addressMap);
        iconAdd.setImageDrawable(ContextCompat.getDrawable(mActivity, R.drawable.fab_add));

        int screenWidth = Utils.getScreenWidth(requireContext());

        propertyBtn = root.findViewById(R.id.property_btn);
        ImageView imageBtn = root.findViewById(R.id.image_btn);
        view1 = root.findViewById(R.id.view1);
        view2 = root.findViewById(R.id.view2);
        LinearLayout menuLayout = root.findViewById(R.id.menuLayout);
        fragmentContainerView = root.findViewById(R.id.container);

        view1.getLayoutParams().width = screenWidth /2;
        view2.setBackgroundResource(R.color.white);
        view2.getLayoutParams().width = screenWidth /2;
        propertyBtn.getLayoutParams().width = screenWidth /2;
        imageBtn.getLayoutParams().width = screenWidth /2;

        getAllLinearLayout(menuLayout);

        propertyBtn.setTextColor(Color.parseColor("#005D8C"));
        if(container != null && inflater != null){
            replaceFragment(new ContactPropertyFragment());
        }

        ProgressBar progressBar = root.findViewById(R.id.progress);
        progressBar.setVisibility(View.GONE);
        Sprite doubleBounce = new MultiplePulse();
        progressBar.setIndeterminateDrawable(doubleBounce);

        return root;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.menu_contact, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

   private void getAllLinearLayout(LinearLayout linearLayout){
        for (int i = 0; i < linearLayout.getChildCount(); i++) {
            int finalI = i;
            linearLayout.getChildAt(i).setOnClickListener(view -> {
                if (finalI == 0){
                    setMenuColor();
                    replaceFragment(new ContactPropertyFragment());
                }else {
                    setDiakuMenuClick();
                    replaceFragment(new DaiKuFragment());
                }
            });
        }
    }

    private void replaceFragment(Fragment fragment){
        FragmentManager manager = getParentFragmentManager();
        fragmentContainerView = root.findViewById(R.id.container);
        FragmentTransaction transaction = manager.beginTransaction();
        if (fragmentContainerView != null) {
            transaction.replace(R.id.container, fragment, fragment.getTag());
            transaction.disallowAddToBackStack();
            transaction.commit();
        }

    }

    private void setMenuColor(){
        propertyBtn.setTextColor(Color.parseColor("#005D8C"));
        view1.setBackgroundResource(R.color.appBarColor);
        view2.setBackgroundResource(R.color.white);
    }

    void setDiakuMenuClick(){
        view2.setBackgroundResource(R.color.appBarColor);
        propertyBtn.setTextColor(Color.BLACK);
        view1.setBackgroundResource(R.color.white);
    }

}
