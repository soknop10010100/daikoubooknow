package com.eazy.daikou.ui.home.utility_tracking.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.eazy.daikou.R;
import com.eazy.daikou.helper.Utils;
import com.eazy.daikou.model.utillity_tracking.model.ListUsageW_EModel;

import java.util.List;

public class TableWaterElectricAdapter extends RecyclerView.Adapter<TableWaterElectricAdapter.ItemViewHolder> {

    private final List<ListUsageW_EModel.TrackingDatum> usageList;

    public TableWaterElectricAdapter(List<ListUsageW_EModel.TrackingDatum> usageList) {
        this.usageList = usageList;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.table_water_electric, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        ListUsageW_EModel.TrackingDatum listUsageW_eModel = usageList.get(position);
        if (listUsageW_eModel != null){
            if (listUsageW_eModel.getTrackingDate() != null) {
                String getTrackingDate = listUsageW_eModel.getTrackingDate();
                String formatDate = Utils.formatDateFromString("yyyy-MM-dd", "dd, MMM yyyy", getTrackingDate);
                holder.date.setText(formatDate);
            }

            if (listUsageW_eModel.getTotalUsage() != null) {
                holder.kilo.setText(listUsageW_eModel.getTotalUsage());
            }
        }
    }

    @Override
    public int getItemCount() {
        return usageList.size();
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder{

        private final TextView date, kilo;
        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            date = itemView.findViewById(R.id.date);
            kilo = itemView.findViewById(R.id.kwh);
        }
    }
}
