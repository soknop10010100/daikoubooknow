package com.eazy.daikou.ui.home.laws.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.eazy.daikou.R;
import com.eazy.daikou.helper.Utils;
import com.eazy.daikou.model.law.LawsModel;

import java.util.List;

public class LawsAdapter extends RecyclerView.Adapter<LawsAdapter.ItemViewHolder> {
    private final List<LawsModel> lawsList;
    private final ClickCallBackListener onClickCallBack;
    private final Context context;

    public LawsAdapter(Context context, String action, List<LawsModel> lawsList, ClickCallBackListener onClickCallBack) {
        this.lawsList = lawsList;
        this.onClickCallBack = onClickCallBack;
        this.context = context;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rowView = LayoutInflater.from(parent.getContext()).inflate(R.layout.my_role_main_model, parent, false);
        return new ItemViewHolder(rowView);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        LawsModel lawsModel = lawsList.get(position);
        if (lawsModel != null){
            holder.icon.setImageDrawable(ResourcesCompat.getDrawable(context.getResources(), R.drawable.ic_home_law, null));
            holder.title.setText(lawsModel.getName() != null ? lawsModel.getName() : "- - -");
            holder.descriptionTv.setText(String.format("%s", context.getString(R.string.see_more)));
            Utils.setBgTint(holder.cardIcon, R.color.greenSea);
            holder.cardLayout.setOnClickListener(v -> onClickCallBack.onClickCallBack(lawsModel));
        }
    }

    @Override
    public int getItemCount() {
        return lawsList.size();
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder{
        private final ImageView icon;
        private final TextView title, descriptionTv;
        private final CardView cardIcon;
        private final CardView cardLayout;
        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            icon = itemView.findViewById(R.id.imgProfile);
            title = itemView.findViewById(R.id.name_item_hr);
            descriptionTv = itemView.findViewById(R.id.descriptionTv);
            cardIcon = itemView.findViewById(R.id.cardIcon);
            cardLayout = itemView.findViewById(R.id.card_hr);
        }
    }

    public interface ClickCallBackListener {
        void onClickCallBack(LawsModel lawsModel);
    }

    public void clear() {
        int size = lawsList.size ();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                lawsList.remove ( 0 );
            }
            notifyItemRangeRemoved ( 0, size );
        }
    }


}
