package com.eazy.daikou.ui.home.hrm.resignation

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.res.ColorStateList
import android.os.Bundle
import android.view.View
import android.view.Window
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.core.widget.NestedScrollView
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.request_data.request.hr_ws.ResignationWs
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.hr.ResignationDetailModel
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class ResignationDetailHrActivity : BaseActivity() {

    private lateinit var progressBar : ProgressBar
    private lateinit var idResignationTv : TextView
    private lateinit var createDateTv : TextView
    private lateinit var nameTv : TextView
    private lateinit var phoneTv : TextView
    private lateinit var positionTv : TextView
    private lateinit var dateStartWorkTv : TextView
    private lateinit var lastDayOfWorkTv : TextView
    private lateinit var dateApprovedTv : TextView
    private lateinit var descriptionTv : TextView
    private lateinit var typeTv : TextView
    private lateinit var remarkTv : TextView
    private lateinit var assignToTv : TextView
    private lateinit var approvedByTv : TextView
    private lateinit var statusTv : TextView
    private lateinit var btnRejectList : TextView
    private lateinit var btnApproveList : TextView
    private lateinit var linearLayoutAction : LinearLayout
    private lateinit var view_detail_Resignation : RelativeLayout
    private var resignListId = ""
    private var userBusinessKey = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_resignation_detail_hr)

        initView()

        initData()

        initAction()

    }

    private fun initView(){
        progressBar = findViewById(R.id.progressItem)
        idResignationTv = findViewById(R.id.idResignationTv)
        createDateTv = findViewById(R.id.createDateTv)
        nameTv = findViewById(R.id.nameTv)
        phoneTv = findViewById(R.id.phoneTv)
        positionTv = findViewById(R.id.positionTv)
        dateStartWorkTv = findViewById(R.id.dateStartWorkTv)
        lastDayOfWorkTv = findViewById(R.id.lastDayOfWorkTv)
        dateApprovedTv = findViewById(R.id.dateApprovedTv)
        typeTv = findViewById(R.id.typeTv)
        remarkTv = findViewById(R.id.remarkTv)
        assignToTv = findViewById(R.id.assignToTv)
        approvedByTv = findViewById(R.id.approvedByTv)
        statusTv = findViewById(R.id.statusTv)
        descriptionTv = findViewById(R.id.descriptionTv)
        linearLayoutAction = findViewById(R.id.linearLayoutAction)
        btnApproveList = findViewById(R.id.btnApproveList)
        btnRejectList = findViewById(R.id.btnRejectList)
        view_detail_Resignation = findViewById(R.id.view_detail_leave)


    }
    private fun initData(){
        if (intent != null && intent.hasExtra("resign_list_id")){
            resignListId = intent.getStringExtra("resign_list_id").toString()
            userBusinessKey = intent.getStringExtra("user_business_key").toString()
        }
    }

    private fun initAction(){
        Utils.customOnToolbarImageHeader(this, "Resignation Detail", null, R.drawable.ic_cv_home_resignation) { finish() }

        progressBar.visibility = View.GONE
        linearLayoutAction.visibility = View.GONE

        requestServiceAPI()

        btnApproveList.setOnClickListener { showDialogApproveReject("approved") }

        btnRejectList.setOnClickListener { showDialogApproveReject("rejected") }

    }

    @SuppressLint("SimpleDateFormat")
    private fun showDialogApproveReject(status : String){
        val dialog = Dialog (this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.layout_alert_come_here)
        val btnCancel = dialog.findViewById<View>(R.id.btn_cancel) as TextView
        val btnOk = dialog.findViewById<View>(R.id.btn_ok) as TextView
        val titleTv = dialog.findViewById<TextView>(R.id.title)
        val calenderView = dialog.findViewById<CalendarView>(R.id.calendarView)
        val textCalender = dialog.findViewById<TextView>(R.id.textCalender)
        val  linearDescription = dialog.findViewById<LinearLayout>(R.id.layout)
        val descriptionEdt : EditText = dialog.findViewById(R.id.description)
        val calendar = Calendar.getInstance()

        if (status == "rejected"){
            titleTv.text = getString(R.string.are_you_sure_to_reject)
        } else {
            titleTv.text = resources.getString(R.string.are_u_sure_to_approve)
            linearDescription.visibility = View.GONE
            dialog.findViewById<LinearLayout>(R.id.layoutCalendar).visibility = View.VISIBLE
        }
        val dateFormatter = SimpleDateFormat("dd, MMMM yyyy")
        val dateFormatterServer = SimpleDateFormat("yyyy-MM-dd")
        textCalender.text = dateFormatter.format(Date())
        calenderView.setOnDateChangeListener { _, year, month, dayOfMonth ->
            calendar.set(year,month,dayOfMonth)
            calenderView.date = calendar.timeInMillis
            textCalender.text = dateFormatter.format(calendar.time)
        }

        btnOk.setOnClickListener {
            if (status == "rejected"){
                if(descriptionEdt.text.isNotEmpty()){
                    sendListActionButtonAPI(status,  descriptionEdt.text.toString(),"")
                    dialog.dismiss()
                } else {
                    Utils.customToastMsgError(this, getString(R.string.please_input_your_reason), false)
                }
            } else {
                sendListActionButtonAPI(status,"", dateFormatterServer.format(calendar.time))
                dialog.dismiss()
            }
        }

        btnCancel.setOnClickListener { dialog.cancel() }
        dialog.show()
    }

    private fun addDataResignationDetail(resignationDetailModel: ResignationDetailModel){

        if (resignationDetailModel.id != null) idResignationTv.text = resignationDetailModel.id else idResignationTv.text = "---"

        if (resignationDetailModel.name != null) nameTv.text = resignationDetailModel.name else nameTv.text = "---"

        if (resignationDetailModel.created_at != null) createDateTv.text = Utils.formatDateTime(resignationDetailModel.created_at, "yyyy-MM-dd kk:mm:ss", "dd-MMM-yyyy hh:mm a")

        if (resignationDetailModel.contact != null) phoneTv.text = resignationDetailModel.contact else phoneTv.text = "---"

        if (resignationDetailModel.designation != null) positionTv.text = resignationDetailModel.designation else positionTv.text = "---"

        if (resignationDetailModel.date_start_worked != null) dateStartWorkTv.text = resignationDetailModel.date_start_worked else dateStartWorkTv.text = "---"

        if (resignationDetailModel.last_date != null) lastDayOfWorkTv.text = resignationDetailModel.last_date else lastDayOfWorkTv.text = "---"

        if (resignationDetailModel.approved_last_date != null) dateApprovedTv.text = resignationDetailModel.approved_last_date else dateApprovedTv.text = "---"

        if (resignationDetailModel.type != null) typeTv.text = resignationDetailModel.type else typeTv.text = "---"

        if (resignationDetailModel.remark != null) remarkTv.text = resignationDetailModel.remark else remarkTv.text = "---"

        if (resignationDetailModel.approver_name != null) assignToTv.text = resignationDetailModel.approver_name else assignToTv.text = "----"

        if (resignationDetailModel.approved_by_name != null) approvedByTv.text = resignationDetailModel.approved_by_name else approvedByTv.text = "---"

        if (resignationDetailModel.status != null) statusTv.text = resignationDetailModel.status else statusTv.text = "---"

        if (resignationDetailModel.reason != null) descriptionTv.text = resignationDetailModel.reason else descriptionTv.text = "---"

        when (resignationDetailModel.status) {
            "pending" -> {
                statusTv.text = Utils.getText(this, R.string.pending)
                statusTv.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(this, R.color.yellow))
            }
            "rejected" -> {
                statusTv.text = Utils.getText(this, R.string.rejected)
                statusTv.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(this, R.color.red))
            }
            "approved" ->{
                statusTv.text = Utils.getText(this, R.string.approved)
                statusTv.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(this, R.color.green))
            }
            else -> {
                statusTv.text = "- - -"
                statusTv.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(this, R.color.appBarColor))
            }
        }

    }

    private fun sendListActionButtonAPI(status: String, remark: String, approvedLastDate : String) {
        val hashMap: HashMap<String, Any> = HashMap()
        hashMap["resignation_id"] = resignListId
        hashMap["status"] = status
        hashMap["user_business_key"] = userBusinessKey
        hashMap["approved_last_date"] = approvedLastDate

        if (status == "rejected") {
            hashMap["remark"] = remark
        }

        ResignationWs().getActionApproveRejectWS(this, hashMap, onCallBackActionButton)
    }

    private val onCallBackActionButton : ResignationWs.OnCallBackCreateResignationListener = object : ResignationWs.OnCallBackCreateResignationListener{
        override fun onLoadSuccessFull(message: String) {
            progressBar.visibility = View.GONE
            Utils.customToastMsgError(this@ResignationDetailHrActivity, message, true)
            setResult(RESULT_OK)
            finish()
        }

        override fun onLoadFailed(message: String) {
            progressBar.visibility = View.GONE
            Utils.customToastMsgError(this@ResignationDetailHrActivity, message, false)
        }

    }


    private fun requestServiceAPI(){
        progressBar.visibility = View.VISIBLE
        ResignationWs().getResignationDetailWs(this, resignListId, onCallBackResignationDetail)
    }

    private val onCallBackResignationDetail : ResignationWs.OnResignationDetailCallBackListener = object : ResignationWs.OnResignationDetailCallBackListener{
        override fun onSuccessful(resignationDetailModel: ResignationDetailModel) {
            progressBar.visibility = View.GONE
            view_detail_Resignation.visibility = View.VISIBLE
            if (resignationDetailModel.status == "pending" && resignationDetailModel.user_business_key != userBusinessKey){
                linearLayoutAction.visibility = View.VISIBLE
            } else {
                 linearLayoutAction.visibility = View.GONE
            }
            addDataResignationDetail(resignationDetailModel)
            if (linearLayoutAction.visibility == View.GONE) {
                Utils.setNoMarginBottomOnButton(findViewById<NestedScrollView>(R.id.mainScroll), 0)
            }
        }

        override fun onFailed(error: String) {
            progressBar.visibility = View.GONE
            Utils.customToastMsgError(this@ResignationDetailHrActivity, error, false)
        }

    }

}