package com.eazy.daikou.ui.home.my_property.payment.adapter;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.eazy.daikou.R;
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener;
import com.eazy.daikou.helper.Utils;
import com.eazy.daikou.model.my_property.payment_invoice.PaymentInvoiceModel;
import com.eazy.daikou.ui.QRCodeAlertDialog;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class MyUnitPaymentAdapter extends RecyclerView.Adapter<MyUnitPaymentAdapter.ViewHolder> {

    private final Context context;
    private final List<PaymentInvoiceModel> payments;
    private final ItemClickNotification itemClickNotification;

    public MyUnitPaymentAdapter(ItemClickNotification itemClickNotification, Context context, List<PaymentInvoiceModel> dataList) {
        this.itemClickNotification = itemClickNotification;
        this.context = context;
        this.payments = dataList;
    }

    @NonNull
    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.layout_payment_notification, parent, false));
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(@NonNull @NotNull ViewHolder holder, int position) {
        PaymentInvoiceModel notificationPayment = payments.get(position);
        if(notificationPayment != null){

            holder.stateTv.setText(notificationPayment.getInvType() != null ? notificationPayment.getInvType() : ". . .");

            holder.issueDateTv.setText(notificationPayment.getIssueDt() != null ? notificationPayment.getIssueDt() : ". . .");

            holder.dueDateTv.setText(notificationPayment.getDueDt() != null ? notificationPayment.getDueDt() : ". . .");

            if (notificationPayment.getUnit() != null){
                holder.txtUtilNo.setText(notificationPayment.getUnit().getName() != null ? notificationPayment.getUnit().getName() : ". . .");
            } else {
                holder.txtUtilNo.setText(". . .");
            }

            if (notificationPayment.getUser() != null){
                holder.txtOwner.setText(notificationPayment.getUser().getName() != null ? notificationPayment.getUser().getName() : ". . .");
            } else {
                holder.txtOwner.setText(". . .");
            }

            holder.txtInvoice.setText(notificationPayment.getId() != null ? notificationPayment.getId() : ". . .");
            holder.txtMonth.setText(". . .");

            String status = "";
            int color = R.color.appBarColorOld;
            if (notificationPayment.getPaymentStatus() != null){
                switch (notificationPayment.getPaymentStatus()) {
                    case "paid":
                        holder.message.setVisibility(View.GONE);
                        color = R.color.green;
                        status = context.getResources().getString(R.string.paid);
                        break;
                    case "unpaid":
                        holder.message.setVisibility(View.GONE);
                        color = R.color.red;
                        status = context.getResources().getString(R.string.un_paid);
                        break;
                    case "overdue":
                        holder.message.setVisibility(View.GONE);
                        // holder.message.setVisibility(notificationPayment.getMessage() != null ? View.VISIBLE : View.GONE);
                        color = R.color.yellow;
                        status = context.getResources().getString(R.string.overdue);
                        break;
                }
            } else {
                status = ". . .";
            }
            holder.txtStatus.setText(status);
            holder.txtStatus.setBackgroundTintList(ColorStateList.valueOf(Utils.getColor(context, color)));

            holder.txtTotal.setText(notificationPayment.getTotal() != null ? notificationPayment.getTotal() : ". . .");

            holder.mainLayout.setOnClickListener(new CustomSetOnClickViewListener(view -> itemClickNotification.onClickItem(notificationPayment,"message")));
            holder.message.setOnClickListener(new CustomSetOnClickViewListener(view -> itemClickNotification.onClickItem(notificationPayment,"message")));

            if (notificationPayment.getLinkPaymentKess() != null){
                holder.imageView.setImageResource(R.drawable.ic_home_qr_code_v2);

                holder.qrCodeLayout.setOnClickListener(v -> {
                    Bitmap qrCodeImageBitmap = Utils.getQRCodeImage512(notificationPayment.getLinkPaymentKess());
                    QRCodeAlertDialog qrCodeAlertDialog = new QRCodeAlertDialog(context,qrCodeImageBitmap);
                    qrCodeAlertDialog.show();
                });

            } else {
                holder.imageView.setImageResource(R.drawable.no_image);
            }
        }
    }

    @Override
    public int getItemCount() {
        return payments.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        private final CardView mainLayout, qrCodeLayout;
        private final ImageView imageView;
        private final TextView issueDateTv, dueDateTv, stateTv, txtUtilNo, txtOwner, txtInvoice, txtMonth, txtTotal, txtStatus ,message;

        public ViewHolder(@NonNull @NotNull View view) {
            super(view);
            imageView = view.findViewById(R.id.img_front);
            issueDateTv = view.findViewById(R.id.issueDateTv);
            dueDateTv = view.findViewById(R.id.dueDateTv);
            stateTv = view.findViewById(R.id.stateTv);
            txtUtilNo = view.findViewById(R.id.txt_util);
            txtOwner = view.findViewById(R.id.txt_owner);
            txtInvoice = view.findViewById(R.id.txt_invoice);
            txtMonth = view.findViewById(R.id.txt_month);
            txtStatus = view.findViewById(R.id.txt_status_payment);
            txtTotal = view.findViewById(R.id.txt_total);
            message = view.findViewById(R.id.message);
            mainLayout = view.findViewById(R.id.mainLayout);
            qrCodeLayout = view.findViewById(R.id.qrCodeLayout);
        }
    }

    public void clear() {
        int size = payments.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                payments.remove(0);
            }
            notifyItemRangeRemoved(0, size);
        }
    }

    public interface ItemClickNotification{
        void onClickItem(PaymentInvoiceModel text , String action);
    }
}
