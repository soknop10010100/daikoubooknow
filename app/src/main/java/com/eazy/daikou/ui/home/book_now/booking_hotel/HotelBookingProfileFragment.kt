package com.eazy.daikou.ui.home.book_now.booking_hotel

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.core.content.res.ResourcesCompat
import androidx.core.widget.NestedScrollView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eazy.daikou.BuildConfig
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.Constant
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.base.BaseFragment
import com.eazy.daikou.base.MockUpData
import com.eazy.daikou.request_data.request.profile_ws.LoginWs
import com.eazy.daikou.request_data.request.profile_ws.LoginWs.OnLogoutListener
import com.eazy.daikou.request_data.sql_database.TrackWaterElectricityDatabase
import com.eazy.daikou.fcm.MyFirebaseMessagingService
import com.eazy.daikou.helper.AppAlertCusDialog
import com.eazy.daikou.helper.CheckIsAppActive.Companion.is_daikou_active
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.home.HomeViewModel
import com.eazy.daikou.model.profile.User
import com.eazy.daikou.ui.home.book_now.booking_account.LoginBookNowActivity
import com.eazy.daikou.ui.home.hrm.adapter.MyRoleMainItemAdapter
import com.eazy.daikou.ui.profile.ProfileDeactivedAccountActivity
import com.eazy.daikou.ui.home.book_now.booking_account.SignUpBookNowActivity
import de.hdodenhof.circleimageview.CircleImageView
import libs.mjn.prettydialog.PrettyDialog
import java.util.*

@SuppressLint("ValidFragment")
class HotelBookingProfileFragment : BaseFragment {

    private lateinit var recycleView : RecyclerView
    private var i = 0
    private lateinit var switchMenuTop : ImageView
    private lateinit var txtUsername : TextView
    private lateinit var profileImg: CircleImageView
    private var isVendorUser = false
    private lateinit var txtEmail : TextView
    private lateinit var userSessionManagement : UserSessionManagement

    private lateinit var itemProfileLayout : NestedScrollView
    private lateinit var notYetLoginLayout : RelativeLayout

    @SuppressLint("ValidFragment")
    constructor()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_hotel_profile_dash_board, container, false)

        initView(view)

        initData()

        initAction()

        return view
    }

    private fun initView(view: View){
        recycleView = view.findViewById(R.id.recyclerView)
        view.findViewById<ImageView>(R.id.iconBack).visibility = View.GONE
        val titleToolbar : TextView = view.findViewById(R.id.titleToolbar)
        titleToolbar.text = resources.getString(R.string.title_me).uppercase()
        switchMenuTop = view.findViewById(R.id.addressMap)
        txtUsername = view.findViewById(R.id.txtUsername)
        profileImg = view.findViewById(R.id.img_profile)
        txtEmail = view.findViewById(R.id.tv_email)

        val versionCodeTv: TextView = view.findViewById(R.id.version_code)
        versionCodeTv.text = String.format(Locale.US, "%s %s (%s)", resources.getString(R.string.version), BuildConfig.VERSION_NAME, BuildConfig.VERSION_CODE)
        //define app
        if (is_daikou_active) {
            versionCodeTv.visibility = View.GONE
        }

        itemProfileLayout = view.findViewById(R.id.itemProfileLayout)
        notYetLoginLayout = view.findViewById(R.id.notYetLoginLayout)

        view.findViewById<LinearLayout>(R.id.btnLoginLayout).setOnClickListener(CustomSetOnClickViewListener {
            val intent = Intent(mActivity, LoginBookNowActivity::class.java)
            intent.putExtra("action", "from_hotel_profile")
            startActivity(intent)
        })

        view.findViewById<LinearLayout>(R.id.btnSignUpLayout).setOnClickListener(CustomSetOnClickViewListener {
            startActivity(Intent(mActivity, SignUpBookNowActivity::class.java))
        })

    }

    private fun initData(){
        itemProfileLayout.visibility =  if (Constant.isLoggedIn(mActivity)) View.VISIBLE else View.GONE     //When already login ,it will show
        notYetLoginLayout.visibility = if (Constant.isLoggedIn(mActivity)) View.GONE else View.VISIBLE  //When already login ,it will hide

        userSessionManagement = UserSessionManagement(mActivity)
    }

    private fun initAction(){
        val user = MockUpData.getUserItem(userSessionManagement) ?: return

        isVendorUser = user.vendor
        when {
            user.name != null -> {
                txtUsername.text = user.name
            }
            user.lastName != null && user.lastName != null-> {
                txtUsername.text = user.lastName + " " + user.firstName
            }
            else -> {
                txtUsername.text = "- - -"
            }
        }

        if (user.photo != null) {
            Glide.with(profileImg).load(user.photo).into(profileImg)
        }

        if (user.email != null) {
            txtEmail.text = user.email
        } else {
            txtEmail.visibility = View.GONE
        }

        recycleView.layoutManager = GridLayoutManager(mActivity, 1)
        recycleView.isNestedScrollingEnabled = false
        recycleView.adapter = MyRoleMainItemAdapter(getListHomeMenu(mActivity), object : MyRoleMainItemAdapter.HomeCallBack{
            override fun clickIconListener(item: HomeViewModel) {
                startNewActivity(item.action)
            }
        })
    }

    private fun startNewActivity(action : String){
        when(action){
            "hotel_dashboard_hotel_admin"->{
                val dashBoard = Intent(mActivity, HotelDashboardActivity::class.java)
                startActivity(dashBoard)
            }
            "hotel_hotel_control_admin" , "hotel_wishlist_hotel"->{
                val dashBoard = Intent(mActivity, HotelBookingVendorManageHotelActivity::class.java)
                dashBoard.putExtra("action", action)
                startActivity(dashBoard)
            }
            "hotel_history_hotel", "hotel_booking_report"->{
                val videoUrl = Intent(mActivity, HotelBookingHistoryActivity::class.java)
                videoUrl.putExtra("action", action)
                startActivity(videoUrl)
            }
            "hotel_logout"->{
                btnLogout()
            }
            "hotel_delete_user_account" ->{
                if (MockUpData.isDeactivated) {
                    AppAlertCusDialog.customAlertDialogYesNo(
                        mActivity,
                        resources.getString(R.string.confirm),
                        "Your account is waiting to be deactivated and can't be recovered after successfully deactivated .",
                        "Cancel Deactivate",
                        "Close") { alertDialog: AlertDialog?, progressBar: ProgressBar, btnOk: Button?, btnCancel: Button? ->
                        progressBar.visibility = View.VISIBLE
                        requestLogOut(alertDialog!!, progressBar, btnOk!!, btnCancel!!)
                    }
                } else {
                    AppAlertCusDialog.deleteUserAccount(mActivity, "Are you sure to delete your account ?", "Delete account information, name and profile pictures. Data will not be recover after deleting account .", PrettyDialog(mActivity)) {
                        startActivity(Intent(mActivity, ProfileDeactivedAccountActivity::class.java))
                    }
                }

            }
        }
    }

    private fun btnLogout() {
        AppAlertCusDialog.customAlertDialogYesNo(mActivity, resources.getString(R.string.confirm), resources.getString(R.string.do_you_want_to_sign_out), resources.getString(R.string.ok), resources.getString(R.string.cancel)) {
                alertDialog: AlertDialog?, progressBar: ProgressBar, _: Button?, _: Button? ->
            // Request Api Logout
            MyFirebaseMessagingService.sendRegistrationToServer(Utils.getString(Constant.FIREBASE_TOKEN, mActivity), mActivity, "remove")

            //logout facebook
            progressBar.visibility = View.GONE
            userSessionManagement.logoutUser()
            alertDialog?.dismiss()

            val intent = Intent(mActivity, LoginBookNowActivity::class.java)
            startActivity(intent)
            mActivity.finish()
            Utils.customToastMsgError(mActivity, resources.getString(R.string.logout_successfully), true)

            //Logout facebook and google
            Utils.disconnectFromFacebook()
        }
    }

    private fun requestLogOut(alertDialog: AlertDialog, progressBar: ProgressBar, btnOk: Button, btnCancel: Button) {
        btnOk.isEnabled = false
        btnCancel.isEnabled = false

        LoginWs().logout(mActivity, object : OnLogoutListener {
            override fun onSuccess(msg: String, data: User) {
                progressBar.visibility = View.GONE
                userSessionManagement.logoutUser()
                val database = TrackWaterElectricityDatabase(mActivity)
                database.deleteAllFloorRecord()
                database.deleteAllFloorRecordFloorDetail()
                val intent = Intent(mActivity, LoginBookNowActivity::class.java)
                startActivity(intent)
                mActivity.finish()
                Utils.customToastMsgError(mActivity, msg, true)
                alertDialog.dismiss()
            }

            override fun onSuccessFalse(status: String) {
                progressBar.visibility = View.GONE
                Utils.customToastMsgError(mActivity, status, false)
                alertDialog.dismiss()
                btnOk.isEnabled = true
                btnCancel.isEnabled = true
            }

            override fun onWrong(msg: String) {
                progressBar.visibility = View.GONE
                Utils.customToastMsgError(mActivity, msg, false)
                alertDialog.dismiss()
                btnOk.isEnabled = true
                btnCancel.isEnabled = true
            }

            override fun onFailed(mess: String) {
                progressBar.visibility = View.GONE
                Utils.customToastMsgError(mActivity, mess, false)
                alertDialog.dismiss()
                btnOk.isEnabled = true
                btnCancel.isEnabled = true
            }
        })
    }

    private fun getListHomeMenu(context: Context): List<HomeViewModel> {
        val homeViewModels: MutableList<HomeViewModel> = ArrayList()
        i = 0
        //Vendor User
        if (isVendorUser){
            homeViewModels.add(
                HomeViewModel(
                    resources.getString(R.string.overview_of_earning),
                    "hotel_dashboard_hotel_admin",
                    ResourcesCompat.getDrawable(
                        context.resources,
                        R.drawable.ic_baseline_dashboard_24,
                        null
                    ),
                    resources.getString(R.string.title_dashboard),
                    color()
                )
            )
            homeViewModels.add(
                HomeViewModel(
                    resources.getString(R.string.you_can_clos_open_or_add_room),
                    "hotel_hotel_control_admin",
                    ResourcesCompat.getDrawable(
                        context.resources,
                        R.drawable.ic_property_service,
                        null
                    ),
                    resources.getString(R.string.manage_hotel),
                    color()
                )
            )
            homeViewModels.add(
                HomeViewModel(
                    resources.getString(R.string.you_can_see_guest_booking_your_list),
                    "hotel_booking_report",
                    ResourcesCompat.getDrawable(
                        context.resources,
                        R.drawable.ic_report_description,
                        null
                    ),
                    resources.getString(R.string.booking_report),
                    color()
                )
            )
        }

        //General User
        homeViewModels.add(
            HomeViewModel(
                resources.getString(R.string.this_is_hotel_of_your_booking),
                "hotel_history_hotel",
                ResourcesCompat.getDrawable(
                    context.resources,
                    R.drawable.add_to_card,
                    null
                ),
                resources.getString(R.string.my_booking_history),
                color()
            )
        )

        homeViewModels.add(
            HomeViewModel(
                resources.getString(R.string.show_your_favourite),
                "hotel_wishlist_hotel",
                ResourcesCompat.getDrawable(
                    context.resources,
                    R.drawable.ic_favorite_border,
                    null
                ),
                resources.getString(R.string.my_wishlist),
                color()
            )
        )

        //define app
        if (MockUpData.isUserAsGuest(userSessionManagement)){
            homeViewModels.add(
                HomeViewModel(
                    "You can delete account",
                    "hotel_delete_user_account",
                    ResourcesCompat.getDrawable(
                        context.resources,
                        R.drawable.profile_delete_account_ic,
                        null
                    ),
                    resources.getString(R.string.delete_account),
                    color()
                )
            )
        }

        homeViewModels.add(
            HomeViewModel(
                resources.getString(R.string.log_out_from_your_account),
                "hotel_logout",
                ResourcesCompat.getDrawable(
                    context.resources,
                    R.drawable.ic_logout,
                    null
                ),
                resources.getString(R.string.logout),
                R.color.calendar_active_month_bg
            )
        )

        return homeViewModels
    }

    private fun color() : Int{
        return Utils.setColorBackground(i) { index -> i = index }
    }
}