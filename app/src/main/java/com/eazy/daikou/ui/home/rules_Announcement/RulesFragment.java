package com.eazy.daikou.ui.home.rules_Announcement;

import android.annotation.SuppressLint;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.eazy.daikou.R;
import com.eazy.daikou.base.BaseFragment;
import com.eazy.daikou.request_data.request.book_quote_ws.RuleAndNewsWs;
import com.eazy.daikou.helper.UserSessionManagement;
import com.eazy.daikou.helper.Utils;
import com.eazy.daikou.model.profile.User;
import com.eazy.daikou.model.rule_announcement.RulesModel;
import com.eazy.daikou.ui.home.rules_Announcement.RulesAdapter.RuleAdapter;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class RulesFragment extends BaseFragment {

    private RecyclerView recyclerView;
    private RuleAdapter ruleAdapter;
    private ProgressBar progressItem;
    private TextView textNoItem;
    private final ArrayList<RulesModel> rulesModelArrayList = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_rules, container, false);

        initView(view);

        initAction();

        return view;
    }

    private void initView(View view) {

        recyclerView = view.findViewById(R.id.recyclerViewRule);
        progressItem = view.findViewById(R.id.progressItem);
        textNoItem = view.findViewById(R.id.textNoItem);

    }

    private void initAction() {

        onRequestContact();

        LinearLayoutManager lm = new LinearLayoutManager(mActivity);
        recyclerView.setLayoutManager(lm);
        ruleAdapter = new RuleAdapter(mActivity, rulesModelArrayList, itemClickCallBack);
        recyclerView.setAdapter(ruleAdapter);
    }

    private void onRequestContact() {
        String propertyID = new Gson().fromJson(new UserSessionManagement(mActivity).getUserDetail(), User.class).getActivePropertyIdFk();
        new RuleAndNewsWs().getRules(mActivity, 1, 20, "rule", propertyID, rulesCallBackListener);
    }

    private final RuleAdapter.ItemClickCallBack itemClickCallBack = rulesModel -> {

        if (rulesModel.getFilePath() != null && rulesModel.getFilePath().contains(".pdf")) {
            Utils.openDefaultPdfView(mActivity, rulesModel.getFilePath());
        } else {
            Toast.makeText(mActivity, getResources().getText(R.string.not_available_website), Toast.LENGTH_SHORT).show();
        }

    };
    private final RuleAndNewsWs.RulesCallBackListener rulesCallBackListener = new RuleAndNewsWs.RulesCallBackListener() {
        @SuppressLint("NotifyDataSetChanged")
        @Override
        public void getRules(List<RulesModel> rulesModels) {
            progressItem.setVisibility(View.GONE);
            rulesModelArrayList.addAll(rulesModels);
            if (rulesModelArrayList.size() > 0) {
                textNoItem.setVisibility(View.GONE);
            } else {
                recyclerView.setVisibility(View.GONE);
                textNoItem.setVisibility(View.VISIBLE);
            }
            ruleAdapter.notifyDataSetChanged();
        }

        @Override
        public void onFailed(String error) {
            Toast.makeText(mActivity, error, Toast.LENGTH_SHORT).show();
        }
    };
}