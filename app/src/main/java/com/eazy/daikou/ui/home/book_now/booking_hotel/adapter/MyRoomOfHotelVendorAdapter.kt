package com.eazy.daikou.ui.home.book_now.booking_hotel.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.daimajia.swipe.SwipeLayout
import com.eazy.daikou.R
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.booking_hotel.HotelMyRoomOfHotelVendorModel

class MyRoomOfHotelVendorAdapter(private val context : Context, private val bookingList: ArrayList<HotelMyRoomOfHotelVendorModel>, private val propertyClick: PropertyClick) : RecyclerView.Adapter<MyRoomOfHotelVendorAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.hotel_my_vendor_layout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val hotelMyVendorModel = bookingList[position]
        if (hotelMyVendorModel != null){
            Utils.setValueOnText(holder.nameTv, hotelMyVendorModel.room_name)
            Utils.setValueOnText(holder.numberTv, hotelMyVendorModel.number)
            Utils.setValueOnText(holder.priceRoomTv, hotelMyVendorModel.price_display)
            Utils.setValueOnText(holder.lastUpdateRoomTv, hotelMyVendorModel.updated_at)
            holder.swipeLayout.visibility = View.VISIBLE
            holder.swipelayoutHotelManage.visibility = View.GONE
            Glide.with(holder.imageRoom).load(if(hotelMyVendorModel.image != null) hotelMyVendorModel.image else R.drawable.no_image).into(holder.imageRoom)

            if (hotelMyVendorModel.status != null) {
                if (hotelMyVendorModel.status == "publish") {
                    holder.statusRoomTv.text = context.resources.getString(R.string.publish)
                    Utils.setBgTint(holder.statusRoomTv, R.color.book_now_secondary)
                } else {
                    holder.statusRoomTv.text = context.resources.getString(R.string.draft)
                    Utils.setBgTint(holder.statusRoomTv, R.color.appBarColorOld)
                }
            } else {
                holder.statusRoomTv.text = ". . ."
            }

            holder.buttonDraft.setOnClickListener {
                if (hotelMyVendorModel.status != null && hotelMyVendorModel.status != "draft") {
                    propertyClick.onBookingClick("draft", hotelMyVendorModel)
                } else {
                    holder.swipeLayout.close()
                }
            }

            holder.buttonPublish.setOnClickListener {
                if (hotelMyVendorModel.status != null && hotelMyVendorModel.status != "publish") {
                    propertyClick.onBookingClick("publish", hotelMyVendorModel)
                } else {
                    holder.swipeLayout.close()
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return bookingList.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val allRoomVendorLayout : LinearLayout = itemView.findViewById(R.id.allRoomVendorLayout)
        val nameTv: TextView = itemView.findViewById(R.id.roomNameTv)
        val numberTv: TextView = itemView.findViewById(R.id.numberTv)
        val priceRoomTv: TextView = itemView.findViewById(R.id.priceRoomTv)
        val lastUpdateRoomTv: TextView = itemView.findViewById(R.id.lastUpdateRoomTv)
        val statusRoomTv: TextView = itemView.findViewById(R.id.statusRoomTv)
        val swipeLayout: SwipeLayout = itemView.findViewById(R.id.swipe_layout)
        val swipelayoutHotelManage : SwipeLayout = itemView.findViewById(R.id.swipe_layoutHotelManage)
        val imageRoom : ImageView = itemView.findViewById(R.id.imageRoom)
        val buttonPublish: LinearLayout = itemView.findViewById(R.id.buttonPublish)
        val buttonDraft: LinearLayout = itemView.findViewById(R.id.buttonDraft)

    }

    interface PropertyClick {
        fun onBookingClick(action : String, propertyModel: HotelMyRoomOfHotelVendorModel)
    }

    fun clear() {
        val size: Int = bookingList.size
        if (size > 0) {
            for (i in 0 until size) {
                bookingList.removeAt(0)
            }
            notifyItemRangeRemoved(0, size)
        }
    }
}