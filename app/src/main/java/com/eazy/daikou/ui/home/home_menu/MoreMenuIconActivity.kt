package com.eazy.daikou.ui.home.home_menu

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.Constant
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.base.MockUpData
import com.eazy.daikou.helper.AppAlertCusDialog
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.helper.web.WebsiteActivity
import com.eazy.daikou.model.home.MainItemHomeModel
import com.eazy.daikou.model.home.SubItemHomeModel
import com.eazy.daikou.model.hr.ItemHR
import com.eazy.daikou.model.profile.User
import com.eazy.daikou.ui.ScannerQRCodeAllActivity
import com.eazy.daikou.ui.home.facility_booking.FacilityBookingActivity
import com.eazy.daikou.ui.home.facility_booking.MyBookingActivity
import com.eazy.daikou.ui.home.facility_booking.MyCardAddToCardBookingActivity
import com.eazy.daikou.ui.home.home_menu.adapter.MainHomeShowItemAdapter
import com.eazy.daikou.ui.home.hrm.MyRoleMainActivity
import com.eazy.daikou.ui.home.hrm.leave_request.ListLeaveManagementActivity
import com.eazy.daikou.ui.home.hrm.over_time.OverTimeHRActivity
import com.eazy.daikou.ui.home.hrm.payroll.ListPayRollManagementActivity
import com.eazy.daikou.ui.home.hrm.profile.ProfileEmployeeActivity
import com.eazy.daikou.ui.home.hrm.report_attendance.AttendanceHrReportActivity
import com.eazy.daikou.ui.home.hrm.resignation.ResignationHrActivity
import com.eazy.daikou.ui.home.laws.LawsActivity
import com.eazy.daikou.ui.home.parking.ParkingListActivity
import com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.cleaning_v2.PropertyServiceIndoorActivity
import com.eazy.daikou.ui.home.development_project.PropertyDevelopmentListActivity
import com.eazy.daikou.ui.home.quote_book.QuoteBookActivity
import com.eazy.daikou.ui.profile.ChangeLanguageFragment

class MoreMenuIconActivity : BaseActivity() {
    private lateinit var recyclerView: RecyclerView
    private var list : ArrayList<MainItemHomeModel> = ArrayList()
    private lateinit var user : User

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_more_menu_icon)

        initView()

        initAction()
    }

    private fun initView(){
        Utils.customOnToolbar(this, resources.getString(R.string.more).uppercase()){finish()}
        findViewById<LinearLayout>(R.id.layoutMap).visibility = View.INVISIBLE
        recyclerView = findViewById(R.id.recyclerView)
    }

    private fun initAction(){
        user = MockUpData.getUserItem(UserSessionManagement(this))
        recyclerView.layoutManager = LinearLayoutManager(this)

        getItemList()
        recyclerView.adapter = MainHomeShowItemAdapter("more_menu_activity", list, this, object : MainHomeShowItemAdapter.PropertyClick{
            override fun onClickCallBackListener(actionWishlist: String, subItemHomeModel: SubItemHomeModel) {}

            override fun onClickSeeAllCallBackListener(action: String, actionType : String, subItemHomeModel : ArrayList<*>) {
                when (action) {
                    "my_property" -> {
                        startNewActivity(MyRoleMainActivity::class.java, "sub_my_property")
                    }
                    "property_listing" -> {
                        startNewActivity(MyRoleMainActivity::class.java, "property_listing")
                    }
                    "transportation_booking" ->{
                        startNewActivity(MyRoleMainActivity::class.java, "transportation_booking")
                    }
                    "development_project" ->{
                        startNewActivity(PropertyDevelopmentListActivity::class.java, "development_project")
                    }
                    "payment" ->{
                        // startNewActivity(FrontDeskListActivity::class.java, "payment")
                    }
                    "my_unit" -> {
                        startNewActivity(MyRoleMainActivity::class.java, "my_unit")
                    }
                    "property_service" ->{
                        startNewActivity(PropertyServiceIndoorActivity::class.java, "property_service")
                    }
                    "constructor" ->{
                        AppAlertCusDialog.underConstructionDialog(this@MoreMenuIconActivity, resources.getString(R.string.coming_soon))
                        // startNewActivity(PropertyServiceOutActivity::class.java, "constructor")
                    }
                    "more" ->{
                        startNewActivity(MyRoleMainActivity::class.java, "more")
                    }
                    "scan" ->{
                        startNewActivity(ScannerQRCodeAllActivity::class.java, "home_screen")
                    }
                    "my_qr_code"->{
                        selectChangeLanguage("qr_code_user")
                    }
                    "my_parking" ->{
                        startActivity(Intent(this@MoreMenuIconActivity, ParkingListActivity::class.java))
                    }
                    "books" ->{
                        startActivity(Intent(this@MoreMenuIconActivity, QuoteBookActivity::class.java))
                    }
                    "beetube" ->{
                        val videoUrl = Intent(this@MoreMenuIconActivity, WebsiteActivity::class.java)
                        videoUrl.putExtra("action", "beetube")
                        videoUrl.putExtra("linkUrlVideo", Constant.url_beetube)
                        startActivity(videoUrl)
                    }
                    "laws" ->{startNewActivity(LawsActivity::class.java, "law")}
                    "news" ->{
                        val videoUrl = Intent(this@MoreMenuIconActivity, WebsiteActivity::class.java)
                        videoUrl.putExtra("action", "beetube")
                        videoUrl.putExtra("linkUrlVideo", Constant.url_beetube_news)
                        startActivity(videoUrl)
                    }
                    "attendance" -> {
                        startActivity(Intent(this@MoreMenuIconActivity, AttendanceHrReportActivity::class.java))
                    }
                    "leave_management" -> {
                        startActivity(Intent(this@MoreMenuIconActivity, ListLeaveManagementActivity::class.java))
                    }
                    "pay_role" -> {
                        startActivity(Intent(this@MoreMenuIconActivity, ListPayRollManagementActivity::class.java))
                    }
                    "profile" -> {
                        startActivity(Intent(this@MoreMenuIconActivity, ProfileEmployeeActivity::class.java))
                    }
                    "overtime" -> {
                        startActivity(Intent(this@MoreMenuIconActivity, OverTimeHRActivity::class.java))
                    }
                    "resigned" -> {
                        startActivity(Intent(this@MoreMenuIconActivity, ResignationHrActivity::class.java))
                    }
                    "my_role" ->{
                        val intent = Intent(this@MoreMenuIconActivity, MyRoleMainActivity::class.java)
                        startActivity(intent)
                    }
                    "facility" ->{
                        startNewActivity(FacilityBookingActivity::class.java, "facility")
                    }
                    "my_card" ->{
                        startNewActivity(MyCardAddToCardBookingActivity::class.java, "my_card")
                    }
                    "my_booking" ->{
                        startNewActivity(MyBookingActivity::class.java, "my_booking")
                    }
                    else ->{
                        Utils.customToastMsgError(this@MoreMenuIconActivity,resources.getString(R.string.coming_soon), true)
                    }
                }
            }

        })
    }

    private fun selectChangeLanguage(key: String) {
        val changeLanguageAlert = ChangeLanguageFragment.newInstance(key)
        changeLanguageAlert.show(supportFragmentManager, changeLanguageAlert.javaClass.simpleName)
    }

    private fun startNewActivity(toActivityClass: Class<*>, action: String) {
        val intent = Intent(this@MoreMenuIconActivity, toActivityClass)
        intent.putExtra("action", action)
        startActivity(intent)
    }


    //Static Menu More
    private fun getItemList(){
        var mainItemHomeModel = MainItemHomeModel()
        var homeViewModelList: ArrayList<ItemHR> = ArrayList()
        mainItemHomeModel.headerTitle = resources.getString(R.string.property_)
        homeViewModelList.add(ItemHR("my_property", R.drawable.home_my_property, resources.getString(R.string.my_property),"You can view your own property here", R.color.greenSea))
        homeViewModelList.add(ItemHR("development_project", R.drawable.home_project_development_v2, resources.getString(R.string.project_development),getString(R.string.all_the_projects_in_cambodia), R.color.calendar_active_month_bg))
        homeViewModelList.add(ItemHR("property_listing", R.drawable.home_project_sale_v2, resources.getString(R.string.property_listing),"You can search property buy sale and rent", R.color.yellow))
        mainItemHomeModel.subItemMoreModelList = homeViewModelList
        list.add(mainItemHomeModel)

        if (!user.activeUserType.equals("owner", ignoreCase = true) && !user.activeUserType.equals("tenant", ignoreCase = true) && !user.activeUserType.equals("guest", ignoreCase = true)) {
            mainItemHomeModel = MainItemHomeModel()
            mainItemHomeModel.headerTitle = resources.getString(R.string.hrm)
            homeViewModelList = ArrayList()
            homeViewModelList.add(ItemHR("attendance", R.drawable.ic_home_attendance_v2, resources.getString(R.string.report_attendance),getString(R.string.employee_attendance_report), R.color.greenSea))
            homeViewModelList.add(ItemHR("leave_management", R.drawable.ic_home_leave_v2, resources.getString(R.string.leave_magement),getString(R.string.annual_leave_sick_special), R.color.yellow))
            homeViewModelList.add(ItemHR("pay_role", R.drawable.ic_home_overtime_v2, resources.getString(R.string.payroll_management),getString(R.string.you_can_manage_your_payroll_with_our_wallet), R.color.red))
            homeViewModelList.add(ItemHR("overtime", R.drawable.ic_home_overtime_v2, resources.getString(R.string.overtime_management),getString(R.string.you_can_apply_for_overtime_approve_by_here), R.color.blue))
            homeViewModelList.add(ItemHR("resigned", R.drawable.ic_home_resignation_v2, resources.getString(R.string.resignation),getString(R.string.create_resignation), R.color.color_wooden))
            homeViewModelList.add(ItemHR("my_role", R.drawable.profile_assign_work_order, resources.getString(R.string.my_rool),"See all works that you must do !", R.color.appBarColorOld))
            homeViewModelList.add(ItemHR("profile", R.drawable.ic_home_profile_v2, resources.getString(R.string.my_profile),getString(R.string.see_your_own_information), R.color.light_blue_600))
            mainItemHomeModel.subItemMoreModelList = homeViewModelList
            list.add(mainItemHomeModel)
        }

        mainItemHomeModel = MainItemHomeModel()
        mainItemHomeModel.headerTitle = resources.getString(R.string.education)
        homeViewModelList = ArrayList()
        homeViewModelList.add(ItemHR("news", R.drawable.ic_home_rule_news, resources.getString(R.string.news),"Realtime news & broadcasting", R.color.greenSea))
        homeViewModelList.add(ItemHR("beetube", R.drawable.ic_home_rule_news, resources.getString(R.string.beetube),"Entertainment movies and videos", R.color.yellow))
        homeViewModelList.add(ItemHR("laws", R.drawable.ic_home_law, resources.getString(R.string.laws),"Regulation & permit", R.color.red))
        homeViewModelList.add(ItemHR("books", R.drawable.home_books, resources.getString(R.string.book),"Novels historical horrible", R.color.blue))
        homeViewModelList.add(ItemHR("professional_training", R.drawable.ic_home_online_video_v2, resources.getString(R.string.professional_training),"Educational videos training & skills", R.color.appBarColorOld))
        mainItemHomeModel.subItemMoreModelList = homeViewModelList
        list.add(mainItemHomeModel)

        mainItemHomeModel = MainItemHomeModel()
        mainItemHomeModel.headerTitle = resources.getString(R.string.accessibility)
        homeViewModelList = ArrayList()
        homeViewModelList.add(ItemHR("scan", R.drawable.ic_home_scan_qr, resources.getString(R.string.scan_qr_code),getString(R.string.coming_soon), R.color.greenSea))
        homeViewModelList.add(ItemHR("my_qr_code", R.drawable.ic_qr_code, resources.getString(R.string.my_qr_code),getString(R.string.coming_soon), R.color.yellow))
        homeViewModelList.add(ItemHR("my_parking", R.drawable.home_parking, resources.getString(R.string.my_parking),getString(R.string.coming_soon), R.color.red))
        mainItemHomeModel.subItemMoreModelList = homeViewModelList
        list.add(mainItemHomeModel)

    }


}