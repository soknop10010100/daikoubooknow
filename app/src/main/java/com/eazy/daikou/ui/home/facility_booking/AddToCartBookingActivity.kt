package com.eazy.daikou.ui.home.facility_booking

import android.annotation.SuppressLint
import android.content.Intent
import android.content.res.ColorStateList
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.animation.TranslateAnimation
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.request_data.request.facility_booking_ws.FacilityBookingWs
import com.eazy.daikou.request_data.static_data.StaticUtilsKey
import com.eazy.daikou.helper.DateUtil
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.facility_booking.CardItemByProperty
import com.eazy.daikou.model.facility_booking.FacilitySpaceDetailModel
import com.eazy.daikou.model.facility_booking.FeeTypeModel
import com.eazy.daikou.model.facility_booking.SpacePricing
import com.eazy.daikou.model.facility_booking.my_booking.CouponBookingModel
import com.eazy.daikou.ui.home.facility_booking.adapter.AvailableDayAdapter
import com.eazy.daikou.ui.home.facility_booking.adapter.SelectFreeTypeAdapter
import com.eazy.daikou.helper.DateNumPickerFragment
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*

class AddToCartBookingActivity : BaseActivity(), View.OnClickListener {

    private lateinit var feeTypeRecycle : RecyclerView
    private lateinit var availableDate : RecyclerView
    private lateinit var noAvailableDayTv : TextView
    private lateinit var selectFreeTypeAdapter: SelectFreeTypeAdapter
    private lateinit var dateLayout : RelativeLayout
    private lateinit var dateTv : TextView
    private lateinit var startTimeTv : TextView
    private lateinit var endTimeTv : TextView
    private lateinit var btnAddToCard : CardView
    private lateinit var periodTime : TextView
    private lateinit var availableTime : TextView
    private lateinit var periodTimeVal : TextView
    private lateinit var membershipTv : TextView
    private lateinit var dateFormatServer : SimpleDateFormat
    private lateinit var dateFormShow : SimpleDateFormat
    private lateinit var timeFormat : SimpleDateFormat
    private lateinit var msgCoupon : TextView
    private lateinit var numberAdd : TextView
    private lateinit var progressBar: ProgressBar
    private lateinit var btnAdd : TextView
    private lateinit var iconStockItem : ImageView

    private lateinit var priceTv : TextView
    private lateinit var expiredDateTv : TextView
    private lateinit var memberLayout : RelativeLayout
    private lateinit var selectTimeLayout : LinearLayout
    private lateinit var availableDayLayout : LinearLayout
    private lateinit var availableTimeLayout : LinearLayout
    private lateinit var periodLayout : LinearLayout
    private lateinit var priceCouponTv : TextView
    // Calender
    private lateinit var noFreeOptionLayout : LinearLayout
    private lateinit var vatTaxTv : TextView
    private lateinit var specificTaxTv : TextView
    private lateinit var lightingTaxTv : TextView
    private lateinit var totalPriceTv : TextView
    private lateinit var formatDecimal : DecimalFormat
    private lateinit var couponEdt : EditText
    private lateinit var btnApply : TextView
    private lateinit var btnCloseOkCoupon : ImageView
    private lateinit var infoLayoutBottom : LinearLayout
    private lateinit var dayTv : TextView
    private lateinit var numberOfPeopleTv : TextView

    private lateinit var availableDayAdapter : AvailableDayAdapter
    private lateinit var lastSelectedCalendar: Calendar
    private lateinit var dateFormatForDay : SimpleDateFormat

    private var titleMembershipList: ArrayList<String> = ArrayList()
    private var spacePricingItem : ArrayList<SpacePricing> = ArrayList()
    private var feeTypeList : ArrayList<FeeTypeModel> = ArrayList()

    private var REQUEST_CODE_ADD_ITEM = 432
    private var REQUEST_CODE_SELECT_NUM = 242
    private var count = 0

    private var isFreeSpace = false
    private var dayVal : String = ""
    private var titleMembershipVal : String = ""
    private var getPeriodTime : String = ""
    private var getPeriodUnitType : String = ""
    private var isFirstTitleMembershipVal : Boolean = true

    private var idFeeType : String = StaticUtilsKey.hourly
    private var selectDateBooking: String = ""
    private var startTimeFormat: String = ""
    private var endTimeFormat: String = ""
    private var startTimeServer: String = ""
    private var endTimeServer: String = ""
    private var isStartTime = true
    private var spaceId : String = ""
    private var propertyId : String = ""
    private var totalPriceAll = 0.0
    private var isClickBtnYesNoCoupon = false
    private var couponPrice = 0.0
    private var vatTaxPercent : String = "0"
    private var lightingTaxPercent : String = "0"
    private var specificTaxPercent : String = "0"
    private var numberOfPeopleVal : String = "1"
    private var totalPrice = 0.0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_booking_now_facility)

        initView()

        initData()

        initAction()

    }

    private fun initView(){
        feeTypeRecycle = findViewById(R.id.freeType)
        dateLayout = findViewById(R.id.dateLayout)
        dateTv = findViewById(R.id.dateTv)
        startTimeTv = findViewById(R.id.startTimeTv)
        endTimeTv = findViewById(R.id.endTimeTv)
        btnAddToCard = findViewById(R.id.linear_card)
        numberAdd = findViewById(R.id.numberAdd)
        progressBar = findViewById(R.id.progressItem)
        progressBar.visibility = View.GONE
        btnAdd = findViewById(R.id.btnAdd)
        iconStockItem = findViewById(R.id.iconAddToCard)
        priceTv = findViewById(R.id.priceTv)
        expiredDateTv = findViewById(R.id.expiredDateTv)
        memberLayout = findViewById(R.id.memberLayout)
        periodTime = findViewById(R.id.periodTime)
        availableTime = findViewById(R.id.availableTimeTv)
        periodTimeVal = findViewById(R.id.numberOfUsage)
        selectTimeLayout = findViewById(R.id.selectTimeLayout)
        availableDate = findViewById(R.id.availableDate)
        availableDayLayout = findViewById(R.id.availableDayLayout)
        periodLayout = findViewById(R.id.periodLayout)
        availableTimeLayout = findViewById(R.id.availableTimeLayout)
        noFreeOptionLayout = findViewById(R.id.noFreeOptionLayout)
        infoLayoutBottom = findViewById(R.id.infoLayout)
        membershipTv = findViewById(R.id.membershipTv)
        numberOfPeopleTv = findViewById(R.id.numberOfPeopleTv)
        dayTv = findViewById(R.id.dayTv)

        totalPriceTv = findViewById(R.id.totalPriceTv)
        specificTaxTv = findViewById(R.id.specificTv)
        lightingTaxTv = findViewById(R.id.lightingTaxTv)
        vatTaxTv = findViewById(R.id.vatTv)

        btnApply = findViewById(R.id.btnApply)
        couponEdt = findViewById(R.id.couponEdt)
        btnCloseOkCoupon = findViewById(R.id.btnYesOrNo)
        msgCoupon = findViewById(R.id.msgCoupon)
        priceCouponTv = findViewById(R.id.priceCouponTv)
        noAvailableDayTv = findViewById(R.id.noItemTv)

        findViewById<TextView>(R.id.btn_back).setOnClickListener{finish()}
    }

    private fun initData(){
        if (intent != null && intent.hasExtra("facility_detail")){
            val facilityDetailModel = intent.getSerializableExtra("facility_detail") as FacilitySpaceDetailModel
            spacePricingItem.addAll(facilityDetailModel.space_pricing)
            if (facilityDetailModel.space_tax_setting_percent != null){
                if (facilityDetailModel.space_tax_setting_percent!!.vat_tax != null){
                    vatTaxPercent = facilityDetailModel.space_tax_setting_percent!!.vat_tax.toString()
                }
                if (facilityDetailModel.space_tax_setting_percent!!.specific_tax != null){
                    specificTaxPercent = facilityDetailModel.space_tax_setting_percent!!.specific_tax.toString()
                }
                if (facilityDetailModel.space_tax_setting_percent!!.lighting_tax != null){
                    lightingTaxPercent = facilityDetailModel.space_tax_setting_percent!!.lighting_tax.toString()
                }
            }

            val imageCoverBooking = findViewById<ImageView>(R.id.img_cover)
            if (facilityDetailModel.space_images.isNotEmpty()) {
                Glide.with(this).load(facilityDetailModel.space_images[0]).into(imageCoverBooking)
            }
            spaceId = facilityDetailModel.space_id.toString()

            propertyId = facilityDetailModel.property?.id.toString()
            isFreeSpace = facilityDetailModel.is_free
        }
    }

    private fun initAction(){
        // Init RecyclerView
        availableDate.layoutManager = GridLayoutManager(this, 3)
        val layoutManager = GridLayoutManager(this, 2, RecyclerView.VERTICAL, false)
        feeTypeRecycle.layoutManager = layoutManager

        // Fee Type
        for (feeType in addListFreeType()){
            if (getFeeTypeAvailable(spacePricingItem, feeType.id)){
                feeType.isEnableItem = true
                feeTypeList.add(feeType)
            } else {
                feeType.isEnableItem = false
                feeTypeList.add(feeType)
            }
        }

        for (feeType in feeTypeList){
            if (feeType.isEnableItem){
                idFeeType = feeType.id
                feeType.isClick = true
                break
            }
        }
        selectFreeTypeAdapter = SelectFreeTypeAdapter(this, feeTypeList, onClickItemFeeTypeListener)
        feeTypeRecycle.adapter = selectFreeTypeAdapter

        // Set Value On Scheduled Date
        dateFormatServer = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        dateFormShow = SimpleDateFormat("dd, MMM yyyy", Locale.getDefault())
        timeFormat = SimpleDateFormat("k : mm", Locale.getDefault())
        formatDecimal = DecimalFormat("#.##")
        dateFormatForDay = SimpleDateFormat("EEEE", Locale.getDefault())

        lastSelectedCalendar = Calendar.getInstance()

        setDefaultValues()

        clearValuePrice()

        setActionOnCoupon()

        setActionOnAddToCard()

        startTimeTv.setOnClickListener(this)
        endTimeTv.setOnClickListener(this)
        dateTv.setOnClickListener(this)
        btnAddToCard.setOnClickListener(this)
        iconStockItem.setOnClickListener(this)
        membershipTv.setOnClickListener(this)
        numberOfPeopleTv.setOnClickListener(this)
        btnApply.setOnClickListener(this)
        btnCloseOkCoupon.setOnClickListener(this)

        couponEdt.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun afterTextChanged(editable: Editable) {
                if (editable.toString().isNotEmpty()) {
                    btnApply.isEnabled = true
                    btnApply.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(this@AddToCartBookingActivity, R.color.greenSea))
                } else {
                    btnApply.isEnabled = false
                    btnApply.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(this@AddToCartBookingActivity, R.color.gray))
                }
            }
        })
    }

    private fun setDefaultValues(){
        dateTv.text = dateFormShow.format(Date())
        selectDateBooking = dateFormatServer.format(Date())

        dayVal = dateFormatForDay.format(Date())
        dayTv.text = dayVal
        if (!validateDay(spacePricingItem)){
            Toast.makeText(this, "$dayVal " + resources.getString(R.string.is_not_available_day), Toast.LENGTH_SHORT).show()
        }

        startTimeTv.hint = startTimeFormat
        endTimeTv.hint = endTimeFormat

        numberOfPeopleTv.text = numberOfPeopleVal

        // Set available day
        initRecycleViewOnAvailableDay()
    }

    private val onClickItemFeeTypeListener = object : SelectFreeTypeAdapter.ClickCallBackListener{
        @SuppressLint("NotifyDataSetChanged")
        override fun onClickCallBack(feeTypeModel: FeeTypeModel) {
            for (itemFeeType in feeTypeList){
                itemFeeType.isClick = itemFeeType.id == feeTypeModel.id
            }
            selectFreeTypeAdapter.notifyDataSetChanged()
            idFeeType = feeTypeModel.id

            memberLayout.visibility = visibleDate(feeTypeModel.id, "option_membership")
            selectTimeLayout.visibility = visibleDate(feeTypeModel.id, "select_time")

            periodLayout.visibility = visibleDate(feeTypeModel.id, "option_membership")
            availableTimeLayout.visibility = visibleDate(feeTypeModel.id, "option_membership")

            isFirstTitleMembershipVal = true
            titleMembershipVal = ""

            //Set Available day
            initRecycleViewOnAvailableDay()

            if (!validateDay(spacePricingItem)){
                Toast.makeText(this@AddToCartBookingActivity, "$dayVal " + resources.getString(R.string.is_not_available_day), Toast.LENGTH_SHORT).show()
                resetStartTimeEndTime("1 : 00","24 : 00", false)
            }

            setValueOnMember()

            startTimeTv.setBackgroundResource(R.drawable.card_border_black)
        }
    }

    private fun resetStartTimeEndTime(startTime : String, endTime : String, isEnable : Boolean){
        endTimeTv.text = ""
        startTimeTv.text = ""
        endTimeTv.hint = endTime
        startTimeTv.hint = startTime
        startTimeTv.isEnabled = isEnable
        endTimeTv.isEnabled = isEnable
    }

    @SuppressLint("SetTextI18n")
    private fun setValueOnMember(){
        expiredDateTv.text = "$selectDateBooking , $endTimeFormat"
        availableTime.text = "$startTimeServer - $endTimeServer"
        if (idFeeType == StaticUtilsKey.time_membership){
            periodTime.text = resources.getString(R.string.number_of_usages)
        } else {
            periodTime.text = resources.getString(R.string.period)
        }
        periodTimeVal.text = "$getPeriodTime $getPeriodUnitType"
    }

    private fun validateDay(spacePricing : List<SpacePricing>) : Boolean{
        for (space in spacePricing){
            if (space.type == idFeeType){
                for (day in space.operation_day) {
                    if (day == dayVal) {
                        getStartEndTime(spacePricing)
                        return true
                    } else {
                        if (titleMembershipVal == space.title) {    // For space type in time_membership or schedule_membership
                            startTimeServer = space.from_time.toString()
                            endTimeServer = space.to_time.toString()
                            getPeriodTime = space.time_number.toString()
                            getPeriodUnitType = if (space.unit_type == "time"){
                                resources.getString(R.string.time_s)
                            } else {
                                resources.getString(R.string.month_s)
                            }
                        }
                    }
                }
            }
        }
        return false
    }

    private fun getStartEndTime(spacePricing : List<SpacePricing>){
        var startTime: Int
        var endTime: Int
        var getStartTime = 0
        var getEndTime = 0
        var isFirst = true
        for (space in spacePricing){
            if (space.type == idFeeType){
                for (day in space.operation_day){
                    if (dayVal == day){
                        val toTimeArr = space.to_time!!.split(":").toTypedArray()
                        val fromTimeArr = space.from_time!!.split(":").toTypedArray()
                        startTime = calculateHour(fromTimeArr[0].replace(" ", "").toInt(), fromTimeArr[1].replace(" ", "").toInt())
                        endTime = calculateHour(toTimeArr[0].replace(" ", "").toInt(), toTimeArr[1].replace(" ", "").toInt())

                        if (isFirst) {
                            getStartTime = startTime
                            getEndTime = endTime
                            isFirst = false
                        }

                        getStartTime = if (getStartTime <= startTime){
                            getStartTime
                        } else {
                            startTime
                        }

                        getEndTime = if (getEndTime <= endTime){
                            endTime
                        } else {
                            getEndTime
                        }
                    }
                }
            }
        }

        startTimeFormat = DateUtil.convertMinutesToHours(getStartTime.toString())
        endTimeFormat = DateUtil.convertMinutesToHours(getEndTime.toString())

        startTimeServer = startTimeFormat
        endTimeServer = endTimeFormat

        isSelectStartTimeFirst = false
        resetStartTimeEndTime(startTimeFormat, endTimeFormat,true)

        clearValuePrice()

    }

    private fun calculateHour(hour : Int, min : Int) : Int {
        return (hour * 60) + min
    }

    private fun visibleDate(key : String, action : String) : Int{
        if (action == "select_time"){
            if (key == StaticUtilsKey.time_membership || key == StaticUtilsKey.schedule_membership){
                return View.GONE
            }
            return View.VISIBLE
        } else {
            if (key == StaticUtilsKey.hourly || key == StaticUtilsKey.daily || key == StaticUtilsKey.free){
                return View.GONE
            }
            return View.VISIBLE
        }
    }

    private fun addListFreeType() : ArrayList<FeeTypeModel>{
        val list : ArrayList<FeeTypeModel> = ArrayList()
        var feeTypeModel = FeeTypeModel(StaticUtilsKey.hourly, Utils.getText(this, R.string.hourly))
        list.add(feeTypeModel)

        feeTypeModel = FeeTypeModel(StaticUtilsKey.daily,  Utils.getText(this, R.string.daily))
        list.add(feeTypeModel)

        feeTypeModel = FeeTypeModel(StaticUtilsKey.time_membership, Utils.getText(this, R.string.time_membership))
        list.add(feeTypeModel)

        feeTypeModel = FeeTypeModel(StaticUtilsKey.schedule_membership, Utils.getText(this, R.string.schedule_membership))
        list.add(feeTypeModel)

        if (isFreeSpace) {
            feeTypeModel = FeeTypeModel(StaticUtilsKey.free, Utils.getText(this, R.string.free))
            list.add(feeTypeModel)
        }

        return list
    }

    private fun getFeeTypeAvailable(spacePricing : List<SpacePricing>, feeTypeItem : String) : Boolean{
        for (space in spacePricing){
            if (space.type == feeTypeItem){
                return true
            }
        }
        return false
    }

    // ===== Part show available day ============
    private fun initRecycleViewOnAvailableDay(){
        val spaceAvailDayList: ArrayList<String> = ArrayList()
        val spaceAvailDayIntList: ArrayList<Int> = ArrayList()
        // Get day from server add number of day
        for (dayString in getListAvailDay()) {
            if (!spaceAvailDayIntList.contains(getIndexDay()[dayString]!!.toInt())) {
                spaceAvailDayIntList.add(getIndexDay()[dayString]!!.toInt())
            }
        }

        // Sort number of day
        spaceAvailDayIntList.sort()

        // Convert number of day in to character of day
        for (dayInt in spaceAvailDayIntList){
            spaceAvailDayList.add(getFullDayName(dayInt))
        }

        availableDayAdapter = AvailableDayAdapter("add_to_card", spaceAvailDayList)
        availableDate.adapter = availableDayAdapter

        noAvailableDayTv.visibility = if(spaceAvailDayList.isEmpty())   View.VISIBLE else View.GONE
    }

    @SuppressLint("SimpleDateFormat")
    private fun getFullDayName(day: Int): String {
        val calendar = Calendar.getInstance()
        calendar[2022, 7, 1, 0, 0] = 0
        calendar.add(Calendar.DAY_OF_MONTH, day)
        val dateFormatForDay = SimpleDateFormat("EEEE")
        return dateFormatForDay.format(calendar.time)
    }

    private fun getIndexDay() : HashMap<String, Int> {
        val hashMapDay : HashMap<String, Int> = HashMap()
        hashMapDay["Monday"] = 0
        hashMapDay["Tuesday"] = 1
        hashMapDay["Wednesday"] = 2
        hashMapDay["Thursday"] = 3
        hashMapDay["Friday"] = 4
        hashMapDay["Saturday"] = 5
        hashMapDay["Sunday"] = 6
        return hashMapDay
    }

    private fun getListAvailDay() : List<String>{
        val list : ArrayList<String> = ArrayList()
        titleMembershipList = ArrayList()
        for (space in spacePricingItem){
            if (idFeeType == space.type){
                if (space.type == StaticUtilsKey.time_membership || space.type == StaticUtilsKey.schedule_membership){
                    if (space.title != null){
                        titleMembershipList.add(space.title!!)
                    }
                    if (titleMembershipList.size > 0 && isFirstTitleMembershipVal){
                        isFirstTitleMembershipVal = false
                        membershipTv.text = titleMembershipList[0]
                        titleMembershipVal = titleMembershipList[0]
                    }

                    if (titleMembershipVal == space.title) {
                        list.addAll(space.operation_day)
                    }
                } else {
                    if (space.title != null){
                        titleMembershipList.add(space.title!!)
                    }
                    if (titleMembershipList.size > 0){
                        membershipTv.text = titleMembershipList[0]
                        titleMembershipVal = titleMembershipList[0]
                    }
                    list.addAll(space.operation_day)
                }

            }

        }
        return list
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onClick(v: View) {
        when (v.id) {
            R.id.startTimeTv -> {
                isStartTime = true
                endTimeFormat = endTimeServer
                selectFromTimeToTimeDialog("from_time")
            }
            R.id.endTimeTv -> {
                isStartTime = false
                if (isSelectStartTimeFirst) {
                    selectFromTimeToTimeDialog("to_time")
                } else {
                    startTimeTv.setBackgroundResource(R.drawable.shape_error)
                }
            }
            R.id.dateTv -> {
                calendarDate()
            }
            R.id.linear_card -> {
                val hashMap = hashmapDataAddToCard()
                progressBar.visibility = View.VISIBLE
                FacilityBookingWs().addToCardBooking(this, hashMap, "add_to_card", callBackBookingListener)
            }
            R.id.iconAddToCard -> {
                if (count > 0){
                    val intent = Intent(this, MyCardAddToCardBookingActivity::class.java)
                    intent.putExtra("action", "my_card_from_add_to_card")
                    startActivityForResult(intent, REQUEST_CODE_ADD_ITEM)
                }
            }
            R.id.membershipTv ->{
                val monthDialog = DateNumPickerFragment.newInstance("title_membership", titleMembershipList)
                monthDialog.CallBackListener { _, value, _ ->
                    titleMembershipVal = value
                    membershipTv.text = titleMembershipVal

                    initRecycleViewOnAvailableDay()

                    if (!validateDay(spacePricingItem)){
                        Toast.makeText(this, "$dayVal " + resources.getString(R.string.is_not_available_day), Toast.LENGTH_SHORT).show()
                    }

                    priceTv.text = getTotalPrice(calculatePrice(spacePricingItem))

                    setValueOnMember()
                }
                monthDialog.show(supportFragmentManager, "dialog_fragment")
            }
            R.id.numberOfPeopleTv->{
                val intentSelectNum = Intent(this@AddToCartBookingActivity, SelectNumberDialogActivity::class.java)
                intentSelectNum.putExtra("numberOfPeopleVal", numberOfPeopleVal)
                startActivityForResult(intentSelectNum, REQUEST_CODE_SELECT_NUM)
            }
            R.id.btnApply ->{
                if (isClickBtnYesNoCoupon){     // Click to apply coupon clear text coupon
                    resetViewCoupon()
                } else {    // Click to apply coupon
                    progressBar.visibility = View.VISIBLE
                    FacilityBookingWs()
                        .applyCouponBooking(this, spaceId, couponEdt.text.toString(), selectDateBooking, dayVal, totalPriceAll.toString(), callBackBookingListener)
                }
            }
            R.id.btnYesOrNo ->{
                if (!isClickBtnYesNoCoupon) {
                    btnCloseOkCoupon.visibility = View.GONE
                    btnApply.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(this, R.color.greenSea))
                    btnApply.text = resources.getString(R.string.apply)
                    priceCouponTv.visibility = View.INVISIBLE
                    msgCoupon.visibility = View.GONE
                }
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun resetViewCoupon(){
        btnCloseOkCoupon.visibility = View.GONE
        btnApply.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(this, R.color.greenSea))
        btnApply.text = resources.getString(R.string.apply)
        priceCouponTv.visibility = View.INVISIBLE
        couponEdt.setText("")
        couponEdt.isEnabled = true
        msgCoupon.visibility = View.GONE

        couponPrice = 0.0
        totalPriceTv.text = "${formatValue(totalPriceAll.toString())} $"

        isClickBtnYesNoCoupon = false
    }

    private var callBackBookingListener = object : FacilityBookingWs.BookingCallBackListener{
        override fun onLoadSuccessFull(successMsg: String?) {
            progressBar.visibility = View.GONE

            numberAdd.visibility = View.VISIBLE
            btnAdd.visibility = View.VISIBLE
            numberAdd.text = count.toString()
            count++
            btnAdd.text = count.toString()
            move(btnAdd)
        }

        override fun onLoadCheckOutBookingSuccessFull(successMsg: String?) {}

        override fun onLoadCouponSuccessFull(couponBookingModel: CouponBookingModel?) {
            progressBar.visibility = View.GONE
            if(couponBookingModel != null){
                msgCoupon.text = setValueOnMessageCoupon(couponBookingModel.coupon_check.toString(), couponBookingModel.minimum_purchase.toString(), couponBookingModel.amount.toString())
            }
        }

        override fun onSuccessUpdateItemCard(numberCount: Int, priceItem: Double?, itemBooking: CardItemByProperty?, action: String?, iconAction : ImageView) {

        }


        override fun onLoadFailed(error: String?) {
            progressBar.visibility = View.GONE
            Utils.customToastMsgError(this@AddToCartBookingActivity, error, false)
        }

    }

    @SuppressLint("SetTextI18n")
    private fun setValueOnMessageCoupon(checkType : String, minimumPurchase: String, amount : String) : String{
        setColorActionOnBtnYesNoCoupon(checkType)
        return when (checkType) {
            "invalid" -> {
                resources.getString(R.string.this_coupon_code_is_invalid)
            }
            "out_of_usage" -> {
                resources.getString(R.string.sorry_your_coupon_has_exceeded_the_maximum_limit_for_usage)
            }
            "price_invalid" -> {
                resources.getString(R.string.a_minimum_booking_of_) + " " + minimumPurchase + " " + resources.getString(R.string.is_require_in_order_to_use_this_coupon)
            }
            "available" -> {
                if (amount != null) {
                    priceCouponTv.visibility = View.VISIBLE
                    priceCouponTv.text = "$amount $"
                    couponPrice = amount.toDouble()

                    val totalPriceAfterCoupon = totalPriceAll - couponPrice
                    totalPriceTv.text = "${formatValue(totalPriceAfterCoupon.toString())} $"
                }
                ""
            }
            else -> {
                if (amount != null) {
                    priceCouponTv.visibility = View.VISIBLE
                    priceCouponTv.text = "$amount $"
                    couponPrice = amount.toDouble()

                    val totalPriceAfterCoupon = totalPriceAll - couponPrice
                    totalPriceTv.text = "${formatValue(totalPriceAfterCoupon.toString())} $"
                }
                ""
            }
        }
    }

    private fun setColorActionOnBtnYesNoCoupon(key : String){
        when (key) {
            "invalid" -> {
                couponEdt.isEnabled = true
                btnCloseOkCoupon.visibility = View.VISIBLE
                btnCloseOkCoupon.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_close))
                btnCloseOkCoupon.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(this, R.color.red))
                msgCoupon.visibility = View.VISIBLE

                btnApply.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(this, R.color.greenSea))
                btnApply.text = resources.getString(R.string.apply)

                isClickBtnYesNoCoupon = false
            }
            "out_of_usage" -> {
                couponEdt.isEnabled = true
                btnCloseOkCoupon.visibility = View.VISIBLE
                btnCloseOkCoupon.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_close))
                btnCloseOkCoupon.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(this, R.color.red))
                msgCoupon.visibility = View.VISIBLE

                btnApply.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(this, R.color.greenSea))
                btnApply.text = resources.getString(R.string.apply)
                isClickBtnYesNoCoupon = false
            }
            "price_invalid" -> {
                couponEdt.isEnabled = true
                btnCloseOkCoupon.visibility = View.VISIBLE
                btnCloseOkCoupon.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_close))
                btnCloseOkCoupon.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(this, R.color.red))
                msgCoupon.visibility = View.VISIBLE

                btnApply.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(this, R.color.greenSea))
                btnApply.text = resources.getString(R.string.apply)
                isClickBtnYesNoCoupon = false
            }
            "available" -> {
                couponEdt.isEnabled = false
                btnCloseOkCoupon.visibility = View.VISIBLE
                btnCloseOkCoupon.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_tech_white))
                btnCloseOkCoupon.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(this, R.color.greenSea))
                msgCoupon.visibility = View.GONE

                btnApply.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(this, R.color.red))
                btnApply.text = resources.getString(R.string.remove)
                isClickBtnYesNoCoupon = true
            }
            else -> {
                couponEdt.isEnabled = false
                btnCloseOkCoupon.visibility = View.VISIBLE
                btnCloseOkCoupon.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_tech_white))
                btnCloseOkCoupon.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(this, R.color.greenSea))
                msgCoupon.visibility = View.GONE

                btnApply.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(this, R.color.red))
                btnApply.text = resources.getString(R.string.remove)
                isClickBtnYesNoCoupon = true
            }
        }
    }

    private var isFirst = false
    private var isSelectStartTimeFirst = false
    @SuppressLint("SimpleDateFormat")
    private fun addListHour() : List<String>{
        val list: MutableList<String> = ArrayList()
        val size: Int
        val minute: String
        var indexStart = 0
        var indexEnd = 0
        val min: String

        if (startTimeFormat != "" && endTimeFormat != "") {
            if (isStartTime) {
                val endTime = endTimeFormat.split(":").toTypedArray()
                minute = endTime[1].replace(" ", "")
                size = endTime[0].replace(" ", "").toInt()

                val startTime = startTimeFormat.split(":").toTypedArray()
                min = startTime[1].replace(" ", "")
                isFirst = true
                val startTimeServer = startTimeServer.split(":").toTypedArray()
                indexStart = startTimeServer[0].replace(" ", "").toInt()
            } else {
                val startTime = startTimeFormat.split(":").toTypedArray()
                minute = startTime[1].replace(" ", "")
                size = startTime[0].replace(" ", "").toInt()

                val endTime = endTimeFormat.split(":").toTypedArray()
                val endTimeServer = endTimeServer.split(":").toTypedArray()
                indexEnd = endTimeServer[0].replace(" ", "").toInt()
                min = endTime[1].replace(" ", "")
            }

            if (isStartTime) {
                val calendar = Calendar.getInstance()
                for (i in indexStart until size) {
                    var k = 0
                    var indexJ = 1
                    if (isFirst) {
                        if (min == "00") {
                            indexJ = 1
                        } else {
                            indexJ = 0
                            k += 30
                        }
                        isFirst = false
                    }
                    for (j in 0..indexJ) {
                        list.add(addHourMinutes(i, k, calendar))
                        k += 30
                    }
                }
                if (minute != "00") {
                    list.add(addHourMinutes(size, 0, calendar))
                }
            } else {
                val calendar = Calendar.getInstance()
                if (minute == "00") {
                    list.add(addHourMinutes(size, 30, calendar))
                }
                for (i in (size + 1)..indexEnd) {
                    var k = 0
                    var indexJ = 1
                    if (i == indexEnd) {
                        indexJ = if (min == "00") { 0 } else { 1 }
                    }
                    for (j in 0..indexJ) {
                        list.add(addHourMinutes(i, k, calendar))
                        k += 30
                    }
                }
            }
        }

        return list
    }

    private fun addHourMinutes(hr : Int, min : Int, calendar : Calendar) : String{
        calendar[Calendar.HOUR_OF_DAY] = hr
        calendar[Calendar.MINUTE] = min
        return timeFormat.format(calendar.time)
    }

    private fun selectFromTimeToTimeDialog(actionType : String){
        val list: MutableList<String> = ArrayList()
        list.addAll(addListHour())
        if (list.size > 0) {
            val monthDialog = DateNumPickerFragment.newInstance(actionType, list)
            monthDialog.CallBackListener { _, value, _ ->
                if (actionType == "to_time") {
                    endTimeTv.text = value
                    endTimeFormat = value
                    priceTv.text = getTotalPrice(calculatePrice(spacePricingItem))

                    setValueOnMember()

                    setActionOnCoupon()

                    setActionOnAddToCard()

                    btnApply.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(this@AddToCartBookingActivity, R.color.gray))
                    couponPrice = 0.0
                    btnApply.text = resources.getString(R.string.apply)
                    btnCloseOkCoupon.visibility = View.GONE
                    priceCouponTv.visibility = View.INVISIBLE
                    couponEdt.setText("")
                    couponEdt.isEnabled = true
                    msgCoupon.visibility = View.GONE
                    setActionOnCoupon()

                } else {
                    startTimeTv.text = value
                    startTimeFormat = value
                    isSelectStartTimeFirst = true

                    endTimeTv.hint = endTimeServer
                    endTimeFormat = endTimeServer
                    endTimeTv.text = ""

                    clearValuePrice()

                    setValueOnMember()

                    startTimeTv.setBackgroundResource(R.drawable.card_border_black)
                }
            }
            monthDialog.show(supportFragmentManager, "dialog_fragment")
        }
    }

    fun move(view: TextView) {
        val animation = TranslateAnimation(TranslateAnimation.ABSOLUTE, 0f,
            TranslateAnimation.ABSOLUTE, 0f,
            TranslateAnimation.RELATIVE_TO_PARENT, 1.0f,
            TranslateAnimation.RELATIVE_TO_PARENT, 0f)
        animation.repeatMode = 0
        animation.duration = 1000
        animation.fillAfter = true
        view.startAnimation(animation)
    }

    private fun calendarDate(){
        val alertLayout = LayoutInflater.from(this).inflate(R.layout.activity_calendar_view_dialog, null)
        val dialog = AlertDialog.Builder(this)
        dialog.setView(alertLayout)
        dialog.setCancelable(false)
        val alertDialog = dialog.show()

        alertLayout.findViewById<ImageView>(R.id.btnClose).setOnClickListener{alertDialog.dismiss()}
        val calendarViewAlert: CalendarView = alertLayout.findViewById(R.id.date_picker)

        // Set Select Date
        if (lastSelectedCalendar != null)     calendarViewAlert.setDate (lastSelectedCalendar.timeInMillis, true, true)

        calendarViewAlert.setOnDateChangeListener { _: CalendarView, year: Int, month: Int, dayOfMonth: Int ->
            val checkCalendar = Calendar.getInstance()
            checkCalendar[year, month] = dayOfMonth

            val calendar = Calendar.getInstance()
            calendar[year, month] = dayOfMonth

            val backUpDayVal = dayVal
            dayVal = dateFormatForDay.format(calendar.time)
            if (checkCalendar == lastSelectedCalendar)
                return@setOnDateChangeListener

            if (validateDay(spacePricingItem)){
                lastSelectedCalendar = checkCalendar
                selectDateBooking = dateFormatServer.format(calendar.time)
                dateTv.text = Utils.formatDateFromString("yyyy-MM-dd", "dd, MMM yyyy", selectDateBooking)
                dayVal = dateFormatForDay.format(calendar.time)
                alertDialog.dismiss()

                startTimeTv.setBackgroundResource(R.drawable.card_border_black)
            } else {
                calendarViewAlert.date = lastSelectedCalendar.timeInMillis
                Toast.makeText(this, "$dayVal " + resources.getString(R.string.is_not_available_day), Toast.LENGTH_SHORT).show()
                dayVal = backUpDayVal
            }

            setValueOnMember()
            dayTv.text = dayVal
        }
        calendarViewAlert.minDate = System.currentTimeMillis() - 1000

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CODE_ADD_ITEM) {
                if (data != null) {
                    val countItem = data.getIntExtra("count_item", count)
                    numberAdd.text = countItem.toString()
                    count = countItem
                    btnAdd.text = countItem.toString()
                }
            } else if (requestCode == REQUEST_CODE_SELECT_NUM){
                if (data != null) {
                    val number = data.getStringExtra("number_data")
                    if (number != "") {
                        numberOfPeopleTv.text = number.toString()
                        numberOfPeopleVal = number.toString()

                        priceTv.text = getTotalPrice(totalPrice)
                    }
                }
            }
        }
    }

    private fun calculatePrice(spacePricing : List<SpacePricing>) : Double{
        totalPrice = 0.0
        val fromTimeArr = startTimeFormat.split(":").toTypedArray()
        val toTimeArr = endTimeFormat.split(":").toTypedArray()
        val minStartTime = calculateHour(fromTimeArr[0].replace(" ", "").toInt(), fromTimeArr[1].replace(" ", "").toInt())
        val minEndTime = calculateHour(toTimeArr[0].replace(" ", "").toInt(), toTimeArr[1].replace(" ", "").toInt())

        for (space in spacePricing){
            if (space.type == idFeeType){
                for (day in space.operation_day) {
                    if (day == dayVal) {
                        val fromTimeServerArr = space.from_time!!.split(":").toTypedArray()
                        val toTimeServerArr = space.to_time!!.split(":").toTypedArray()
                        val minStartTimeServer = calculateHour(fromTimeServerArr[0].replace(" ", "").toInt(), fromTimeServerArr[1].replace(" ", "").toInt())
                        val minEndTimeServer = calculateHour(toTimeServerArr[0].replace(" ", "").toInt(), toTimeServerArr[1].replace(" ", "").toInt())

                        when (idFeeType) {
                            // Fee Type Is Hourly
                            StaticUtilsKey.hourly -> {
                                if (minStartTime >= minStartTimeServer){
                                    if(minStartTime <= minEndTimeServer) {
                                        val amountTime: Int = if (minEndTime >= minEndTimeServer) {
                                            minEndTimeServer - minStartTime
                                        } else {
                                            minEndTime - minStartTime
                                        }
                                        val amountHours = DateUtil.convertMinutesToHours(amountTime.toString()).split(":").toTypedArray()
                                        val hours = amountHours[0].replace(" ", "")
                                        val min = amountHours[1].replace(" ", "")

                                        val priceHours = hours.toInt() * validateSpacePrice(space.price)
                                        var priceMin = 0.0
                                        if (min.toInt() == 30) {
                                            priceMin = validateSpacePrice(space.price) / 2
                                        }
                                        val price = priceHours + priceMin
                                        totalPrice += price
                                    } else {
                                        continue
                                    }
                                } else if (minEndTime > minStartTimeServer){    // minStartTime < minStartTimeServer && minEndTime > minStartTimeServer
                                    val amountTime: Int = if (minEndTime > minEndTimeServer){
                                        minEndTimeServer - minStartTimeServer
                                    } else {
                                        minEndTime - minStartTimeServer
                                    }
                                    val amountHours = DateUtil.convertMinutesToHours(amountTime.toString()).split(":").toTypedArray()
                                    val hours = amountHours[0].replace(" ", "")
                                    val min = amountHours[1].replace(" ", "")

                                    val priceHours = hours.toInt() * validateSpacePrice(space.price)
                                    var priceMin = 0.0
                                    if (min.toInt() == 30){
                                        priceMin = validateSpacePrice(space.price) / 2
                                    }
                                    val price = priceHours + priceMin
                                    totalPrice += price
                                }
                            }
                            // Fee Type Is Daily
                            StaticUtilsKey.daily -> {
                                if (minStartTime >= minStartTimeServer){
                                    totalPrice += validateSpacePrice(space.price)
                                } else if (minEndTime > minStartTimeServer){    // minStartTime < minStartTimeServer && minEndTime > minStartTimeServer
                                    totalPrice += validateSpacePrice(space.price)
                                }
                            }
                            // Fee Type Is Membership
                            StaticUtilsKey.time_membership -> {
                                if (titleMembershipVal == space.title) {
                                    totalPrice += validateSpacePrice(space.price)
                                    break
                                }
                            }
                            StaticUtilsKey.schedule_membership -> {
                                if (titleMembershipVal == space.title) {
                                    totalPrice += validateSpacePrice(space.price)
                                    break
                                }
                            }
                            else -> {
                                totalPrice += validateSpacePrice(space.price)
                            }
                        }

                    }
                }
            }
        }
        return totalPrice
    }

    private fun validateSpacePrice(spacePrice : String?) : Double{
        return spacePrice?.toDouble() ?: 0.0
    }

    @SuppressLint("SetTextI18n")
    private fun getTotalPrice(price : Double) : String{
        var totalPrice = 0.0
        try {
            totalPrice = price * numberOfPeopleVal.toInt()
        } catch (ex : Exception){
            ex.printStackTrace()
        }

        val vatTaxPrice = (vatTaxPercent.toDouble() / 100) * totalPrice
        val lightingTaxPrice =  totalPrice * 0.2 * (lightingTaxPercent.toDouble() / 100)
        val specificTaxPrice = (specificTaxPercent.toDouble() / 100) * totalPrice

        val totalPr = (totalPrice + vatTaxPrice + lightingTaxPrice + specificTaxPrice)
        totalPriceAll = totalPr
        totalPriceTv.text = "${formatValue(totalPr.toString())} $"

        return "${formatValue(totalPrice.toString())} $"
    }

    @SuppressLint("SetTextI18n")
    private fun clearValuePrice(){
        priceTv.text = "0 $"
        totalPriceTv.text = "0 $"
        vatTaxTv.text = "$vatTaxPercent %"
        lightingTaxTv.text = "$lightingTaxPercent %"
        specificTaxTv.text = "$specificTaxPercent %"
        totalPriceAll = 0.0

        // Re-Coupon
        btnApply.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(this@AddToCartBookingActivity, R.color.gray))
        couponPrice = 0.0
        btnApply.text = resources.getString(R.string.apply)
        btnCloseOkCoupon.visibility = View.GONE
        priceCouponTv.visibility = View.INVISIBLE

        couponEdt.setText("")
        couponEdt.isEnabled = true

        msgCoupon.visibility = View.GONE
        setActionOnCoupon()

        totalPriceTv.text = "${formatValue(totalPriceAll.toString())} $"

        isClickBtnYesNoCoupon = false

        setActionOnAddToCard()
    }

    private fun formatValue(fPri : String): Float {
        return java.lang.Float.valueOf(formatDecimal.format(fPri.toDouble()))
    }

    private fun setActionOnCoupon(){
        btnApply.isClickable = isEnableAddCoupon()
        btnApply.isEnabled = isEnableAddCoupon()
        couponEdt.isEnabled = isEnableAddCoupon()
    }

    private fun setActionOnAddToCard(){
        btnAddToCard.isEnabled = isEnableAddCoupon()
        if (isEnableAddCoupon()){
            btnAddToCard.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(this@AddToCartBookingActivity, R.color.appBarColor))
        } else {
            btnAddToCard.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(this@AddToCartBookingActivity, R.color.gray))
        }
    }

    private fun isEnableAddCoupon() : Boolean{
        if (totalPriceAll == 0.0 && couponEdt.text.isEmpty()){
            return false
        }
        return true
    }

    private fun hashmapDataAddToCard() : HashMap<String, Any>{
        val hashMap : HashMap<String, Any> = HashMap()
        hashMap["date"] = selectDateBooking
        hashMap["from_time"] = "${startTimeFormat.replace(" ", "")}:00"
        hashMap["to_time"] = "${endTimeFormat.replace(" ", "")}:00"
        hashMap["type_price"] = idFeeType
        if (idFeeType != StaticUtilsKey.time_membership && idFeeType != StaticUtilsKey.schedule_membership) {
            hashMap["option_name"] = idFeeType
        } else {
            hashMap["option_name"] = titleMembershipVal
        }
        hashMap["space_id"] = spaceId
        hashMap["quantity"] = numberOfPeopleVal

        hashMap["tax_vat_percent"] = vatTaxPercent
        hashMap["tax_lighting_percent"] = lightingTaxPercent
        hashMap["tax_specific_percent"] = specificTaxPercent
        hashMap["amount"] = formatDecimal.format(calculatePrice(spacePricingItem)).toString()
        hashMap["total_amount"] = formatDecimal.format(totalPriceAll).toString()
        hashMap["user_id"] = UserSessionManagement(this@AddToCartBookingActivity).userId
        if (couponPrice != 0.0) hashMap["coupon_id"] = couponPrice.toString()
        hashMap["property_id"] = propertyId

        return hashMap
    }
}