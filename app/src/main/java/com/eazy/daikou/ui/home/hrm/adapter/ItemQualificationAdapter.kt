package com.eazy.daikou.ui.home.hrm.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.model.hr.Qualification


class ItemQualificationAdapter(private val listName: List<Qualification>) : RecyclerView.Adapter<ItemQualificationAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.layout_item_qualification_adapter, parent, false)
        return ViewHolder(v)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data: Qualification = listName[position]

        holder.qualificationTextTv.text = data.qualification
        holder.majorTextTv.text = data.major
        holder.schoolTextTv.text = data.school
        holder.joiningDateTv.text = data.joining_date
        holder.graduateDateTv.text = data.gratuate_date
        holder.issueDateTv.text = data.issue_date
        holder.schoolAddressTv.text = data.school_addr

    }

    override fun getItemCount(): Int {
        return listName.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val qualificationTextTv : TextView = view.findViewById(R.id.qualificationText)
        val majorTextTv : TextView = view.findViewById(R.id.major_text)
        val schoolTextTv : TextView = view.findViewById(R.id.school_text)
        val joiningDateTv : TextView = view.findViewById(R.id.joining_date)
        val graduateDateTv : TextView = view.findViewById(R.id.graduateDate)
        val issueDateTv : TextView = view.findViewById(R.id.issueDate)
        val schoolAddressTv : TextView = view.findViewById(R.id.schoolAddress)

    }

    interface ItemClickOnListener{
        fun onItemClick(qualification: Qualification)
    }

}