package com.eazy.daikou.ui.home.hrm.resignation

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AbsListView
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.request_data.request.hr_ws.ResignationWs
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.hr.ResignationHrModel
import com.eazy.daikou.model.profile.User
import com.eazy.daikou.ui.home.hrm.resignation.adapter.ResignationHrAdapter
import com.google.gson.Gson
import kotlin.collections.ArrayList

class ResignationHrActivity : BaseActivity() {

    private lateinit var progressBar: ProgressBar
    private lateinit var recyclerView : RecyclerView
    private lateinit var refreshLayout : SwipeRefreshLayout
    private lateinit var linearLayoutManager : LinearLayoutManager
    private lateinit var resignationAdapter : ResignationHrAdapter
    private var resignationHrModelList : ArrayList<ResignationHrModel> = ArrayList()
    private lateinit var noItemTv : TextView
    private lateinit var btnCreate : ImageView
    private var userBusinessKey = ""
    private var accountBusinessKey = ""
    private lateinit var user : User
    private var currentItem = 0
    private var total : Int = 0
    private var scrollDown : Int = 0
    private var currentPage : Int = 1
    private var size : Int = 10
    private var isScrolling = false
    private var isScrollItem = false
    private var action = ""
    private var idResign = ""

    private var REQUEST_CODE_DETAIL = 235
    private var REQUEST_CODE_CREATE = 468

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_resignation_hr)

        initView()

        initData()

        initAction()

    }

    private fun initView(){
        refreshLayout = findViewById(R.id.refreshLayout)
        recyclerView = findViewById(R.id.itemRecycle)
        progressBar = findViewById(R.id.progressItem)
        btnCreate = findViewById(R.id.btnCreate)
        noItemTv = findViewById(R.id.noDataDisplayTv)

        Utils.customOnToolbar(this, resources.getString(R.string.resignation)){finish()}
    }

    private fun initData(){
        user = Gson().fromJson(UserSessionManagement(this).userDetail, User::class.java)
        val businessKy = user.userBusinessKey
        val accountKey = user.activeAccountBusinessKey
        if (businessKy != null && accountKey != null){
            userBusinessKey = businessKy
            accountBusinessKey = accountKey
        }

        if (intent != null && intent.hasExtra("action")){
            action = intent.getStringExtra("action").toString()
            val id = intent.getStringExtra("id").toString()
            if (intent != null && intent.hasExtra("id")) {
                idResign = id
            }
        }

        // Start detail from notification after request list already
        if (action == "resignation") {
            setActivityDetail(idResign)
            action = ""
        }
    }

    private fun initAction(){
        linearLayoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = linearLayoutManager
        resignationAdapter = ResignationHrAdapter(this, resignationHrModelList, itemClickCallBack)
        recyclerView.adapter = resignationAdapter

        requestServiceAPI()
        initOnScrollItem()
        recyclerView.visibility = View.VISIBLE

        refreshLayout.setColorSchemeResources(R.color.colorPrimary)
        refreshLayout.setOnRefreshListener { this.onRefreshList() }

        btnCreate.setOnClickListener {
            startActivityForResult(Intent(this@ResignationHrActivity, CreateResignRequestActivity::class.java), REQUEST_CODE_CREATE)
            btnCreate.isEnabled = false

        }
    }

    private fun onRefreshList() {
        currentPage = 1
        size = 10
        isScrolling = true
        resignationAdapter.clear()
        requestServiceAPI()
    }
    private fun initOnScrollItem() {
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener(){
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true
                }
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                currentItem = linearLayoutManager.childCount
                total = linearLayoutManager.itemCount
                scrollDown = linearLayoutManager.findFirstVisibleItemPosition()
                if (dy > 0) {
                    if (isScrolling && currentItem + scrollDown == total) {
                        if (total == size) {
                            isScrolling = false
                            currentPage++
                            progressBar.visibility = View.VISIBLE
                            isScrollItem = true
                            recyclerView.scrollToPosition(resignationHrModelList.size - 1)
                            requestServiceAPI()
                            size += 10
                        } else {
                            progressBar.visibility = View.GONE
                        }
                    }
                }
            }
        })
    }

    private val itemClickCallBack = object : ResignationHrAdapter.ItemClickOnListener{
        override fun onItemClick(resignationHrModel: ResignationHrModel?) {
//            val intent = Intent(this@ResignationHrActivity, ResignationDetailHrActivity::class.java)
//            if (resignationHrModel != null) {
//                intent.putExtra("resign_list_id", resignationHrModel.id)
//                intent.putExtra("user_business_key", userBusinessKey)
//            }
//            startActivityForResult(intent, REQUEST_CODE_DETAIL)
            if (progressBar.visibility == View.GONE){
                resignationHrModel!!.id?.let { setActivityDetail(it) }
            }

        }

    }

    private fun setActivityDetail(resignID : String){
        val intent = Intent(this@ResignationHrActivity, ResignationDetailHrActivity::class.java)
        intent.putExtra("action_click", action)
        intent.putExtra("resign_list_id", resignID)
        intent.putExtra("user_business_key", userBusinessKey)

        startActivityForResult(intent, REQUEST_CODE_DETAIL)
    }

    private fun requestServiceAPI(){
        progressBar.visibility = View.VISIBLE
        ResignationWs().getResignationWs(this,currentPage,10, accountBusinessKey , userBusinessKey, callBackResignation)
    }

    private val callBackResignation : ResignationWs.OnResignationCallBackListener = object : ResignationWs.OnResignationCallBackListener{
        @SuppressLint("NotifyDataSetChanged")
        override fun onSuccessful(listResignation: ArrayList<ResignationHrModel>) {
            progressBar.visibility = View.GONE
            refreshLayout.isRefreshing = false
            btnCreate.visibility = View.VISIBLE

            resignationHrModelList.addAll(listResignation)

            if (listResignation.size > 0) {
                noItemTv.visibility = View.GONE
            } else {
                noItemTv.visibility = View.VISIBLE
            }

            Utils.validateViewNoItemFound(this@ResignationHrActivity, recyclerView, resignationHrModelList.size <= 0)

            resignationAdapter.notifyDataSetChanged()
        }

        override fun onFailed(error: String) {
            progressBar.visibility = View.GONE
            refreshLayout.isRefreshing = false
            Utils.customToastMsgError(this@ResignationHrActivity, error, false)
        }

    }
    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (data != null ){
            if (data.hasExtra("isClickedFirst")){
                val statusResult = data.getBooleanExtra("isClickedFirst", false)
                if (statusResult) {
                    btnCreate.isEnabled = true
                }
            }
        } else{
            if (resultCode == RESULT_OK && requestCode == REQUEST_CODE_CREATE){
                onRefreshList()
                btnCreate.isEnabled = true
            }
            if (resultCode == RESULT_OK && requestCode == REQUEST_CODE_DETAIL){
                onRefreshList()
            }
        }

    }
}