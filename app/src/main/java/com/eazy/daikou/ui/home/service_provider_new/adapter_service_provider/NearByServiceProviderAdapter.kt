package com.eazy.daikou.ui.home.service_provider_new.adapter_service_provider

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.helper.Utils

class NearByServiceProviderAdapter(private val activity: Activity, private val context: Context): RecyclerView.Adapter<NearByServiceProviderAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.custom_layout_item_feature_provider,parent, false)
        val itemViewParam = LinearLayout.LayoutParams(Utils.getWidth(activity) / 2, LinearLayout.LayoutParams.WRAP_CONTENT)
        view.layoutParams = itemViewParam
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        Glide.with(context).load("https://sandboxeazy.daikou.asia/img/service_parking.jpg").into(holder.imageViewFeature)

        holder.nameServiceTv.text = "Cleaning"

        Glide.with(context).load(R.drawable.evaluate_img).into(holder.imageIcon)

        holder.textNameCompanyTv.text = "TTP"

        val heightImg = Utils.dpToPx(context, 220)
        if (position % 3 == 0) {
            holder.cardMainLayout.layoutParams.height = heightImg + Utils.dpToPx(context, 10)
        } else{
            holder.cardMainLayout.layoutParams.height = heightImg / 2
        }

    }

    override fun getItemCount(): Int {
        return 20
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view){
        var imageViewFeature : ImageView = view.findViewById(R.id.imageViewFeature)
        var nameServiceTv : TextView = view.findViewById(R.id.nameServiceTv)
        var imageIcon : ImageView = view.findViewById(R.id.imageIcon)
        var textNameCompanyTv : TextView = view.findViewById(R.id.textNameCompanyTv)
        var cardMainLayout : CardView = view.findViewById(R.id.cardMainLayout)
    }

}