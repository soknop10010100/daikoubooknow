package com.eazy.daikou.ui.home.data_record.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R

class AdapterPayLateRequestPayment(private val context: Context): RecyclerView.Adapter<AdapterPayLateRequestPayment.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view : View = LayoutInflater.from(context).inflate(R.layout.custom_layout_pay_late_request, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

    }

    override fun getItemCount(): Int {
       return 10
    }

    class ViewHolder( view : View) : RecyclerView.ViewHolder(view){

    }
}