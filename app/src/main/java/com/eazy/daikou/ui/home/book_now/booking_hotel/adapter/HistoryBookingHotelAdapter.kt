package com.eazy.daikou.ui.home.book_now.booking_hotel.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.request_data.request.book_now_ws.RequestHashMapData
import com.eazy.daikou.helper.DateUtil
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.booking_hotel.HotelBookingHistoryModel

class HistoryBookingHotelAdapter(private val context : Context, private val historyList : ArrayList<HotelBookingHistoryModel>, private val onClickListener : OnClickCallBackLister) : RecyclerView.Adapter<HistoryBookingHotelAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.history_booking_hotel_layout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val itemHistory = historyList[position]
        if (itemHistory != null){
            Utils.setValueOnText(holder.noTv, itemHistory.code)
            Utils.setValueOnText(holder.hotelNameTv, itemHistory.hotel_name)

            if (itemHistory.created_at != null){
                holder.createdDateTv.text = DateUtil.formatDateComplaint(itemHistory.created_at)
            } else {
                holder.createdDateTv.text = ". . ."
            }

            if (itemHistory.start_date != null){
                holder.startDateBookingTv.text = DateUtil.formatDateComplaint(itemHistory.start_date)
            } else {
                holder.startDateBookingTv.text = ". . ."
            }
            if (itemHistory.end_date != null){
                holder.endDateTv.text = DateUtil.formatDateComplaint(itemHistory.end_date)
            } else {
                holder.endDateTv.text = ". . ."
            }

            if (itemHistory.category != null){
                holder.categoryTv.text = RequestHashMapData.categoryTitle(context)[itemHistory.category]
            } else {
                holder.categoryTv.text = ". . ."
            }

            holder.paymentMethodTv.text = context.resources.getString(R.string.online_payment)

            if (itemHistory.status != null) {
                var statusVal = ""
                var color = R.color.appBarColorOld
                when(itemHistory.status){
                    "pending"->{
                        statusVal = context.resources.getString(R.string.pending)
                        color = R.color.yellow
                    }
                    "completed" ->{
                        statusVal = context.resources.getString(R.string.completed)
                        color = R.color.green
                    }
                    "cancelled" ->{
                        statusVal = context.resources.getString(R.string.cancel)
                        color = R.color.red
                    }
                    "draft" ->{
                        statusVal = context.resources.getString(R.string.draft)
                        color = R.color.yellow
                    }
                    "paid" ->{
                        statusVal = context.resources.getString(R.string.paid)
                        color = R.color.green
                    }
                    "unpaid" ->{
                        statusVal = context.resources.getString(R.string.un_paid)
                        color = R.color.red
                    }
                }
                Utils.setValueOnText(holder.status, statusVal)
                Utils.setBgTint(holder.status, color)
                Utils.setBgTint(holder.styleTv, color)
            } else {
                Utils.setValueOnText(holder.status, ". . .")
            }

            if (itemHistory.total != null){
               holder.totalPriceTv.text = if (itemHistory.total!!.contains("$"))    itemHistory.total!! else "$ " + itemHistory.total!!
            } else {
                holder.totalPriceTv.text = ". . ."
            }

            holder.mainLayout.setOnClickListener (
                CustomSetOnClickViewListener{
                    onClickListener.onClickCallBack(itemHistory)
                }
            )

        }
    }

    override fun getItemCount(): Int {
        return historyList.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val hotelNameTv : TextView = itemView.findViewById(R.id.hotelNameTv)
        val createdDateTv : TextView = itemView.findViewById(R.id.createdDateTv)
        val startDateBookingTv : TextView = itemView.findViewById(R.id.startDateBookingTv)
        val endDateTv : TextView = itemView.findViewById(R.id.endDateTv)
        val totalPriceTv : TextView = itemView.findViewById(R.id.totalPriceTv)
        val noTv : TextView = itemView.findViewById(R.id.noTv)
        val paymentMethodTv : TextView = itemView.findViewById(R.id.paymentMethodTv)
        val status : TextView = itemView.findViewById(R.id.status)
        val categoryTv : TextView = itemView.findViewById(R.id.categoryTv)
        val styleTv : TextView = itemView.findViewById(R.id.style)
        val mainLayout : CardView = itemView.findViewById(R.id.mainLayout)
    }

    fun clear() {
        val size: Int = historyList.size
        if (size > 0) {
            for (i in 0 until size) {
                historyList.removeAt(0)
            }
            notifyItemRangeRemoved(0, size)
        }
    }

    interface OnClickCallBackLister{
        fun onClickCallBack(hotelBookingHistoryModel : HotelBookingHistoryModel)
    }
}