package com.eazy.daikou.ui.home.service_provider

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.*
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.request_data.request.service_property_ws.PropertyListWs
import com.eazy.daikou.request_data.request.service_property_ws.PropertyListWs.CallBackDetailListLatLongListener
import com.eazy.daikou.request_data.request.service_provider_ws.ServiceProviderWs
import com.eazy.daikou.helper.AbsoluteFitLayoutManager
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.helper.web.WebsitePropertyActivity
import com.eazy.daikou.model.my_property.Child
import com.eazy.daikou.model.my_property.service_provider.LatLngServiceProvider
import com.eazy.daikou.model.my_property.service_provider.LatLongPropertyModel
import com.eazy.daikou.ui.home.service_provider.adapter.ItemCategoryAdapter
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.gson.Gson
import java.util.*


class ServiceProviderBottomSheetFragment : BottomSheetDialogFragment() {

    private var onClickAllAction: OnClickAllAction?=null
    private lateinit var serviceProvider : LatLngServiceProvider
    private lateinit var latLongPropertyModel: LatLongPropertyModel
    private lateinit var propertyHeadNameTv: TextView
    private lateinit var imageView : ImageView
    private lateinit var propertyWebsite : TextView
    private lateinit var addressTv : TextView

    private var listChild = ArrayList<Child>()
    private var listString = ArrayList<String>()

    private var titleHeader : String = ""
    private var childName : String = ""
    private var action : String = ""
    private var homeMenuType = ""

    //SubCategory on Service Provider
    fun newInstance(title: String, childName : String, listChild: ArrayList<Child>?) : ServiceProviderBottomSheetFragment {
        val args = Bundle()
        val fragment = ServiceProviderBottomSheetFragment()
        args.putString("title", title)
        args.putString("action", "")
        args.putString("home_menu_type", "")
        args.putString("child_name",childName)
        args.putSerializable("listString", listChild)
        fragment.arguments = args
        return fragment
    }

    //Detail By LatLong
    fun newInstance(action : String, serviceProvider : LatLngServiceProvider) : ServiceProviderBottomSheetFragment {
        val args = Bundle()
        val fragment = ServiceProviderBottomSheetFragment()
        args.putString("action", action)
        args.putString("home_menu_type", "service_provider")
        args.putSerializable("serviceProvider", serviceProvider)
        fragment.arguments = args
        return fragment
    }

    //Detail By LatLong
    fun newInstance(action : String) : ServiceProviderBottomSheetFragment {
        val args = Bundle()
        val fragment = ServiceProviderBottomSheetFragment()
        args.putString("action", action)
        args.putString("home_menu_type", "property_list")
        fragment.arguments = args
        return fragment
    }

    @SuppressLint("UseRequireInsteadOfGet")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val roof =  inflater.inflate(R.layout.fragment_botton_sheet_provider, container, false)
        dialog!!.window!!.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
        if (arguments != null) {
            action = requireArguments().getString("action").toString()
            homeMenuType = requireArguments().getString("home_menu_type").toString()
            if (action != "") {
                if (homeMenuType == "service_provider") {
                    serviceProvider = requireArguments().getSerializable("serviceProvider") as LatLngServiceProvider
                }
                roof.findViewById<FrameLayout>(R.id.subCategory).visibility = View.GONE
                roof.findViewById<LinearLayout>(R.id.cardView).visibility = View.VISIBLE
            } else {
                childName = requireArguments().getString("child_name").toString()
                titleHeader = requireArguments().getString("title") as String
                listChild = requireArguments().getSerializable("listString") as ArrayList<Child>
                if (listChild.size > 0) {
                    for (listChildService in listChild) {
                        listChildService.isClick = listChildService.name.equals(childName)
                        listString.add(listChildService.name)
                    }
                }

                //For get List name category
                val storeDate = Gson().toJson(listString)
                Utils.logDebug("Jeheeejejee", storeDate)
            }
        }
        return roof
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        if (dialog is BottomSheetDialog) {
            dialog.behavior.skipCollapsed = true
            dialog.behavior.state = BottomSheetBehavior.STATE_EXPANDED
        }
        return dialog
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val titleTv:TextView= view.findViewById(R.id.titleItem)
        val recycleView: RecyclerView = view.findViewById(R.id.recyclerView)
        val progressBar : ProgressBar = view.findViewById(R.id.progress)

        imageView = view.findViewById(R.id.image)
        propertyWebsite = view.findViewById(R.id.propertyWebsite)
        addressTv = view.findViewById(R.id.propertyNameTv)
        propertyHeadNameTv = view.findViewById(R.id.propertyHeadNameTv)

        if (action != "") {
            titleTv.visibility = View.GONE
            propertyHeadNameTv.visibility = View.VISIBLE
            if (homeMenuType == "service_provider"){
                ServiceProviderWs()
                    .getDetailByIdLatLong(propertyHeadNameTv.context, action, object : ServiceProviderWs.DetailByIdServiceProvider{
                    override fun onLoadServiceProvider(detailServiceProvider: LatLngServiceProvider?) {
                        progressBar.visibility = View.GONE
                        if (detailServiceProvider != null) {
                            serviceProvider = detailServiceProvider
                        }
                        setValues(serviceProvider, null)
                    }

                    override fun OnLoadFail(message: String?) {
                        progressBar.visibility = View.GONE
                        Toast.makeText(progressBar.context, message, Toast.LENGTH_LONG).show()
                    }

                })
            } else{
                PropertyListWs()
                    .detailByLatLong(propertyHeadNameTv.context, action, object : CallBackDetailListLatLongListener {
                    override fun getDetailListLatLong(latLongPropertyModels: LatLongPropertyModel) {
                        progressBar.visibility = View.GONE
                        latLongPropertyModel = latLongPropertyModels
                        setValues(null, latLongPropertyModels)
                    }
                    override fun onLoadingFailed(msg: String) {
                        progressBar.visibility = View.GONE
                        Toast.makeText(progressBar.context, msg, Toast.LENGTH_LONG).show()

                    }
                })
            }
        } else{
            progressBar.visibility = View.GONE
            titleTv.text = titleHeader
            val layoutManager = AbsoluteFitLayoutManager(
                context,
                1,
                RecyclerView.VERTICAL,
                false,
                2
            )
            recycleView.layoutManager = layoutManager
            recycleView.adapter = ItemCategoryAdapter(listChild, itemClick)
        }

        //Click go to web to detail property
        view.findViewById<LinearLayout>(R.id.cardView).setOnClickListener{
            if (homeMenuType == "service_provider") {
                if (serviceProvider.website != null) {
                    if (progressBar.visibility == View.GONE) {
                        gotoPropertyWebsite(propertyHeadNameTv.context, serviceProvider.website, serviceProvider.contractorName, serviceProvider.coordLat, serviceProvider.coordLong);
                        dismiss()
                    }
                }
            } else{
                if (latLongPropertyModel.branchWebsite != null) {
                    if (progressBar.visibility == View.GONE) {
                        gotoPropertyWebsite(propertyHeadNameTv.context, latLongPropertyModel.branchWebsite, latLongPropertyModel.branchName, latLongPropertyModel.coordLat, latLongPropertyModel.coordLong);
                        dismiss()
                    }
                }
            }
        }

    }

    private fun setValues(serviceProvider: LatLngServiceProvider?, latLongPropertyModel: LatLongPropertyModel?){
        if (serviceProvider != null){
            propertyHeadNameTv.text = serviceProvider.contractorName
            Glide.with(imageView.context).load(serviceProvider.companyLogo).into(imageView)
            if(serviceProvider.website != null){
                propertyWebsite.text = serviceProvider.website
            } else{
                propertyWebsite.text = propertyHeadNameTv.context.resources.getString(R.string.not_available_website)
            }
            addressTv.text = serviceProvider.address
        } else if (latLongPropertyModel != null){
            propertyHeadNameTv.text = latLongPropertyModel.branchName
            Glide.with(imageView.context).load(latLongPropertyModel.branchImage).into(imageView)
            if(latLongPropertyModel.branchWebsite != null){
                propertyWebsite.text = latLongPropertyModel.branchWebsite
            } else{
                propertyWebsite.text = propertyHeadNameTv.context.resources.getString(R.string.not_available_website)
            }
            addressTv.text = latLongPropertyModel.branchAddress
        }
    }

    private fun gotoPropertyWebsite(context: Context, url: String?, buildingName: String?, lat: String?, lng: String?) {
        val intent = Intent(context, WebsitePropertyActivity::class.java)
        intent.putExtra("BuildingName", buildingName)
        intent.putExtra("URL_Property_List", url)
        intent.putExtra("type", "provider")
        intent.putExtra("current_lat", BaseActivity.latitude)
        intent.putExtra("current_lng", BaseActivity.longitude)
        intent.putExtra("lat", lat)
        intent.putExtra("lng", lng)
        startActivity(intent)
    }

    private  val itemClick = ItemCategoryAdapter.ItemClickListCategory { listChildClick->
        onClickAllAction!!.onClickSelect(titleHeader, listChildClick)
        dismiss()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        val parent = parentFragment
        onClickAllAction = if (parent != null) {
            parent as OnClickAllAction
        } else {
            context as OnClickAllAction
        }
    }

    interface OnClickAllAction {
        fun onClickSelect(titleHeader : String, list: Child)
    }
}