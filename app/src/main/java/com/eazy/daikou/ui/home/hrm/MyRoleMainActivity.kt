package com.eazy.daikou.ui.home.hrm

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import android.widget.ProgressBar
import androidx.activity.result.ActivityResult
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.base.MockUpData
import com.eazy.daikou.request_data.request.home_ws.HomeWs
import com.eazy.daikou.request_data.static_data.StaticUtilsKey
import com.eazy.daikou.helper.AppAlertCusDialog
import com.eazy.daikou.helper.InternetConnection
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.CustomCategoryModel
import com.eazy.daikou.model.home.HomeViewModel
import com.eazy.daikou.model.home.MyRolePermissionModel
import com.eazy.daikou.model.home.PermissionModel
import com.eazy.daikou.model.my_property.payment_schedule_my_unit.PaymentSchedulesModel
import com.eazy.daikou.model.profile.User
import com.eazy.daikou.ui.CustomMenuHeaderAdapter
import com.eazy.daikou.ui.CustomMenuHeaderAdapter.Companion.addMenuStepList
import com.eazy.daikou.ui.home.complaint_solution.complaint.ComplaintsSolutionActivity
import com.eazy.daikou.ui.home.create_case_sale_and_rent.BuyRentSaleActivity
import com.eazy.daikou.ui.home.create_case_sale_and_rent.sale_and_rent.SaleRentRequestActivity
import com.eazy.daikou.ui.home.data_record.ListDataRecordActivity
import com.eazy.daikou.ui.home.hrm.adapter.MyRoleMainItemAdapter
import com.eazy.daikou.ui.home.inspection_work_order.InspectionListActivity
import com.eazy.daikou.ui.home.my_property.my_unit.MyUnitPropertyActivity
import com.eazy.daikou.ui.home.my_property.payment.PropertyPaymentActivity
import com.eazy.daikou.ui.home.my_property.property_service.PropertyServiceListActivity
import com.eazy.daikou.ui.home.my_property_v1.property_service_employee.work_service_employee.ServicePropertyMainEmployeeListActivity
import com.eazy.daikou.ui.home.parking.ParkingListActivity
import com.eazy.daikou.ui.home.utility_tracking.UtilityTrackingActivity
import com.google.gson.Gson

class MyRoleMainActivity : BaseActivity() {

    private lateinit var recycleView: RecyclerView
    private var userPermissionModel: HashMap<String, MyRolePermissionModel> = HashMap()
    private var actionCategory: String = ""
    private lateinit var progressBar: ProgressBar
    private var user: User? = null
    private lateinit var paymentScheduled: PaymentSchedulesModel
    private var i = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_role_main)

        initView()

        initData()

        initAction(getListHomeMenu(this))

    }

    private fun initData() {

        if (intent != null && intent.hasExtra("action")) {
            actionCategory = intent.getStringExtra("action").toString()
        }

        //From Payment Scheduled
        if (intent != null && intent.hasExtra("payment_schedule_model")) {
            paymentScheduled =
                intent.getSerializableExtra("payment_schedule_model") as PaymentSchedulesModel
        }

        user = Gson().fromJson(UserSessionManagement(this).userDetail, User::class.java)
    }

    private fun initView() {
        findViewById<LinearLayout>(R.id.layoutMap).visibility = View.INVISIBLE
        recycleView = findViewById(R.id.recyclerView)
        progressBar = findViewById(R.id.progressItem)
        progressBar.visibility = View.GONE
    }

    private fun initAction(homeViewModels: List<HomeViewModel>) {
        recycleView.layoutManager = GridLayoutManager(this, 1)
        recycleView.adapter =
            MyRoleMainItemAdapter(homeViewModels, object : MyRoleMainItemAdapter.HomeCallBack {
                override fun clickIconListener(item: HomeViewModel) {
                    startNewActivity(item.action)
                }
            })

        if (actionCategory == "sub_my_property") {
            setMenuStepAdapter(this)
        }
    }

    private fun startNewActivity(action: String) {
        if (progressBar.visibility == View.VISIBLE) return
        when (action) {
            "complaint" -> {
                startNewActivity(ComplaintsSolutionActivity::class.java, "complaint", "by_uploader")
            }
            "sub_my_property" -> {
                startNewActivity(MyRoleMainActivity::class.java, "sub_my_property")
            }
            "my_unit" -> {
                val intent = Intent(this, MyUnitPropertyActivity::class.java)
                activityLauncher.launch(intent) { result: ActivityResult ->
                    if (result.resultCode == RESULT_OK) {
                        // From menu home
                        if (result.data != null) {
                            val menu = result.data!!.getStringExtra("menu")
                            if (menu == "home") {
                                finish()
                            }
                        } else {
                            finish()
                        }
                    }
                }
            }
            "payment" -> {
                startNewActivity(PropertyPaymentActivity::class.java, "payment")
            }
            "property_service" -> {
                startNewActivity(PropertyServiceListActivity::class.java, "property_service")
            }

            "data_record" -> {
                startNewActivity(ListDataRecordActivity::class.java, "data_record")
            }

            "more" -> {
                startNewActivity(MyRoleMainActivity::class.java, "more")
            }

            "complaint_frontdesk_evaluate_user" -> {
                startNewActivity(ComplaintsSolutionActivity::class.java, "all", "by_user")
            }

            "complaint_suggestion_user" -> {
                startNewActivity(ComplaintsSolutionActivity::class.java, "complaint", "by_user")
            }
            "front_desk", "front-desk" -> {
                if (userPermissionModel[action]!!.is_role) {
                    startNewActivity(ComplaintsSolutionActivity::class.java, "frontdesk", "by_uploader")
                } else {
                    AppAlertCusDialog.underConstructionDialog(
                        this,
                        resources.getString(R.string.you_do_not_permission_to_access)
                    )
                }
            }
            "front_desk_user" -> {
                startNewActivity(ComplaintsSolutionActivity::class.java, "frontdesk", "by_user")
            }
            "electric" -> {
                val intentElectricity =
                    Intent(this@MyRoleMainActivity, UtilityTrackingActivity::class.java)
                intentElectricity.putExtra("action", "electricity")
                intentElectricity.putExtra(
                    "user_permission",
                    userPermissionModel[StaticUtilsKey.electric_record_permission]
                )
                startActivity(intentElectricity)
            }
            "water" -> {
                val intentWater =
                    Intent(this@MyRoleMainActivity, UtilityTrackingActivity::class.java)
                intentWater.putExtra("action", "water")
                intentWater.putExtra(
                    "user_permission",
                    userPermissionModel[StaticUtilsKey.water_record_permission]
                )
                startActivity(intentWater)
            }
            "inspection_user" -> {
                AppAlertCusDialog.underConstructionDialog(
                    this,
                    resources.getString(R.string.coming_soon)
                )
                // val intent = Intent(this@MyRoleMainActivity, InspectionListActivity::class.java)
                // intent.putExtra("action", StaticUtilsKey.inspection_action)
                // startActivity(intent)
            }
            "inspection" -> {
                if (userPermissionModel[action]!!.is_role) {
                    val intent = Intent(this@MyRoleMainActivity, InspectionListActivity::class.java)
                    intent.putExtra("action", StaticUtilsKey.inspection_action)
                    intent.putExtra(
                        "user_permission",
                        userPermissionModel[action]
                    )
                    startActivity(intent)
                } else {
                    AppAlertCusDialog.underConstructionDialog(
                        this,
                        resources.getString(R.string.you_do_not_permission_to_access)
                    )
                }
            }
            "maintenance_user" -> {
                AppAlertCusDialog.underConstructionDialog(
                    this,
                    resources.getString(R.string.coming_soon)
                )

                // val intent = Intent(this@MyRoleMainActivity, InspectionListActivity::class.java)
                // intent.putExtra("action", StaticUtilsKey.maintenance_action)
                // startActivity(intent)
            }
            "maintenance" -> {
                if (userPermissionModel[action]!!.is_role) {
                    val intent = Intent(this@MyRoleMainActivity, InspectionListActivity::class.java)
                    intent.putExtra("action", StaticUtilsKey.maintenance_action)
                    intent.putExtra(
                        "user_permission",
                        userPermissionModel[action]
                    )
                    startActivity(intent)
                } else {
                    AppAlertCusDialog.underConstructionDialog(
                        this,
                        resources.getString(R.string.you_do_not_permission_to_access)
                    )
                }
            }
            "work_order" -> startNewActivity(
                InspectionListActivity::class.java,
                StaticUtilsKey.work_order_action
            )

            "parking" ->
                if (userPermissionModel[action]!!.is_role) {
                    startActivity(Intent(this, ParkingListActivity::class.java))
                } else {
                    AppAlertCusDialog.underConstructionDialog(
                        this,
                        resources.getString(R.string.you_do_not_permission_to_access)
                    )
                }
            "emergency" -> {
                AppAlertCusDialog.underConstructionDialog(
                    this,
                    resources.getString(R.string.coming_soon)
                )
            }
            "evaluate_user" ->{
                startNewActivity(ComplaintsSolutionActivity::class.java, "evaluation", "by_user")
            }
            "evaluate" -> {
                startNewActivity(ComplaintsSolutionActivity::class.java, "evaluation", "by_uploader")
            }
            "sale" -> {
                startNewActivity(SaleRentRequestActivity::class.java, "sale")
            }
            "buy" -> {
                startNewActivity(BuyRentSaleActivity::class.java, "buy")
            }
            "rent" -> {
                startNewActivity(BuyRentSaleActivity::class.java, "rent")
            }
            "electricity_tracking_user" -> {
                AppAlertCusDialog.underConstructionDialog(
                    this,
                    resources.getString(R.string.coming_soon)
                )
            }
            "water_tracking_user" -> {
                AppAlertCusDialog.underConstructionDialog(
                    this,
                    resources.getString(R.string.coming_soon)
                )
            }

            "cleaning", "security", "spider_man", "landscaping", "maintenance_work_engine", "pest_control" -> {
                if (userPermissionModel[action]!!.is_role) {
                    startNewActivity(ServicePropertyMainEmployeeListActivity::class.java, action)
                } else {
                    AppAlertCusDialog.underConstructionDialog(
                        this,
                        resources.getString(R.string.you_do_not_permission_to_access)
                    )
                }
            }

            else -> {
                AppAlertCusDialog.underConstructionDialog(
                    this@MyRoleMainActivity,
                    resources.getString(R.string.coming_soon)
                )
            }
        }
    }

    private fun startNewActivity(toActivityClass: Class<*>, action: String) {
        Utils.startNewActivity(this, toActivityClass, "action", action)
    }

    private fun startNewActivity(toActivityClass: Class<*>, action: String, listType : String) {
        val intent = Intent(this@MyRoleMainActivity, toActivityClass)
        intent.putExtra("action", action)
        intent.putExtra("list_type", listType)
        startActivity(intent)
    }

    private fun getListHomeMenu(context: Context): List<HomeViewModel> {
        val homeViewModels: MutableList<HomeViewModel> = ArrayList()
        val title: String
        when (actionCategory) {

            "sub_my_property" -> {
                title = resources.getString(R.string.my_property).uppercase()
                homeViewModels.add(
                    HomeViewModel(
                        resources.getString(R.string.your_own_property_unit),
                        "my_unit",
                        ResourcesCompat.getDrawable(context.resources, R.drawable.room, null),
                        resources.getString(R.string.my_unit_),
                        color()
                    )
                )

                homeViewModels.add(
                    HomeViewModel(
                        resources.getString(R.string.your_property_payment),
                        "payment",
                        ResourcesCompat.getDrawable(
                            context.resources,
                            R.drawable.home_payments_v2,
                            null
                        ),
                        resources.getString(R.string.payment),
                        color()
                    )
                )

                homeViewModels.add(
                    HomeViewModel(
                        resources.getString(R.string.you_can_call_the_services_in_property),
                        "property_service",
                        ResourcesCompat.getDrawable(
                            context.resources,
                            R.drawable.ic_home_leave_v2,
                            null
                        ),
                        resources.getString(R.string.property_service),
                        color()
                    )
                )

                homeViewModels.add(
                    HomeViewModel(
                        getString(R.string.data_record),
                        "data_record",
                        ResourcesCompat.getDrawable(
                            context.resources,
                            R.drawable.ic_document,
                            null
                        ),
                        resources.getString(R.string.data_record),
                        color()
                    )
                )

                homeViewModels.add(
                    HomeViewModel(
                        "You can make complaint, front desk, evaluate",
                        "complaint_frontdesk_evaluate_user",
                        ResourcesCompat.getDrawable(
                            context.resources,
                            R.drawable.ic_home_complain,
                            null
                        ),
                        "Complaint / Front Desk / Evaluate",
                        color()
                    )
                )

//                homeViewModels.add(
//                    HomeViewModel(
//                        "You can make complaint or suggestion",
//                        "complaint_suggestion_user",
//                        ResourcesCompat.getDrawable(
//                            context.resources,
//                            R.drawable.ic_home_complain,
//                            null
//                        ),
//                        resources.getString(R.string.complaints_suggestion),
//                        color()
//                    )
//                )
                homeViewModels.add(
                    HomeViewModel(
                        "You can see all inspection report in property",
                        "inspection_user",
                        ResourcesCompat.getDrawable(
                            context.resources,
                            R.drawable.ic_home_inspection,
                            null
                        ),
                        resources.getString(R.string.inspection),
                        color()
                    )
                )
//                homeViewModels.add(
//                    HomeViewModel(
//                        "You can see front desk item",
//                        "front_desk_user",
//                        ResourcesCompat.getDrawable(
//                            context.resources,
//                            R.drawable.ic_front_notification,
//                            null
//                        ),
//                        resources.getString(R.string.front_desk),
//                        color()
//                    )
//                )
                homeViewModels.add(
                    HomeViewModel(
                        "You can see all maintenance report in property",
                        "maintenance_user",
                        ResourcesCompat.getDrawable(
                            context.resources,
                            R.drawable.corrective_maintenance,
                            null
                        ),
                        resources.getString(R.string.maintenance),
                        color()
                    )
                )
                if (!Utils.isEmployeeRole(this)) {  //if guest now function my property, so no need check for guest
                    homeViewModels.add(
                        HomeViewModel(
                            "See the data report of your your electric usage",
                            "electricity_tracking_user",
                            ResourcesCompat.getDrawable(
                                context.resources,
                                R.drawable.home_electri_meter,
                                null
                            ),
                            context.getString(R.string.electricity_tracking),
                            color()
                        )
                    )
                    homeViewModels.add(
                        HomeViewModel(
                            "See the data report of your your water usage", "water_tracking_user",
                            ResourcesCompat.getDrawable(
                                context.resources,
                                R.drawable.ic_home_water_records,
                                null
                            ),
                            context.getString(R.string.water_tracking),
                            color()
                        )
                    )
                }
                homeViewModels.add(
                    HomeViewModel(
                        getString(R.string.emergency_detail),
                        "emergency",
                        ResourcesCompat.getDrawable(
                            context.resources,
                            R.drawable.ic_emergency_notification,
                            null
                        ),
                        context.getString(R.string.emergency),
                        R.color.red
                    )
                )

//                homeViewModels.add(
//                    HomeViewModel(
//                        "You can evaluate property unit, scan unit qr code",
//                        "evaluate_user",
//                        ResourcesCompat.getDrawable(
//                            context.resources,
//                            R.drawable.home_evaluate_v2,
//                            null
//                        ),
//                        resources.getString(R.string.evaluate),
//                        color()
//                    )
//                )

            }

            "property_listing" -> {
                title = resources.getString(R.string.property_listing).uppercase()
                homeViewModels.add(
                    HomeViewModel(
                        "You can upload property for sale",
                        "sale",
                        ResourcesCompat.getDrawable(
                            context.resources,
                            R.drawable.home_project_sale_v2,
                            null
                        ),
                        context.getString(R.string.sale),
                        R.color.greenSea
                    )
                )
                homeViewModels.add(
                    HomeViewModel(
                        "You can view properties for buy",
                        "buy",
                        ResourcesCompat.getDrawable(
                            context.resources,
                            R.drawable.ic_home_property_buy_v2,
                            null
                        ),
                        context.getString(R.string.buy),
                        R.color.blue
                    )
                )
                homeViewModels.add(
                    HomeViewModel(
                        "You can view properties for rent",
                        "rent",
                        ResourcesCompat.getDrawable(
                            context.resources,
                            R.drawable.ic_home_property_rent_v2,
                            null
                        ),
                        context.getString(R.string.rent),
                        R.color.yellow
                    )
                )
            }

            // Get Role Item
            else -> {
                title = resources.getString(R.string.my_rool).uppercase()

                // when finish request service, we get list
                requestPermission()
            }

        }

        Utils.customOnToolbar(this, title) { finish() }
        return homeViewModels
    }

    private fun requestPermission() {
        if (InternetConnection.checkInternetConnection(recycleView)) {
            requestService()
        } else {
            progressBar.visibility = View.GONE
            val permissionModels =
                Utils.getUserPermissionList(StaticUtilsKey.user_permission_key, this)
            if (permissionModels != null && permissionModels.isNotEmpty()) {
                storePermission(permissionModels)
                addListRecyclerView(permissionModels)
            } else {
                AppAlertCusDialog.underConstructionDialog(
                    this,
                    resources.getString(R.string.please_get_new_data_before_close_internet),
                )
            }
        }
    }

    private fun requestService() {
        progressBar.visibility = View.VISIBLE
        val permissionMaps = java.util.HashMap<String, Any>()
        permissionMaps["user_id"] = Utils.validateNullValue(UserSessionManagement(this).userId)
        permissionMaps["active_account_id"] =
            Utils.validateNullValue(MockUpData.getUserItem(UserSessionManagement(this)).accountId)
        HomeWs().getUserRolePermission(this, permissionMaps, onCallBackPermissionListener)
    }

    private val onCallBackPermissionListener = object : HomeWs.UserPermissionCallBack {
        override fun onSuccess(userPermissionModels: List<PermissionModel>) {
            // Last User Permission
            progressBar.visibility = View.GONE
        }

        override fun onSuccessUserRolePermission(userPermissionModels: MutableList<MyRolePermissionModel>) {
            progressBar.visibility = View.GONE
            Utils.saveUserPermissionList(
                userPermissionModels,
                StaticUtilsKey.user_permission_key,
                this@MyRoleMainActivity
            )
            storePermission(userPermissionModels)

            addListRecyclerView(userPermissionModels)
        }

        override fun onFailure(errorMessage: String) {
            progressBar.visibility = View.GONE
            Utils.customToastMsgError(this@MyRoleMainActivity, errorMessage, false)
        }
    }

    private fun addListRecyclerView(userPermissionModels: MutableList<MyRolePermissionModel>) {
        val homeViewModelList: MutableList<HomeViewModel> = ArrayList()
        for (item in userPermissionModels) {
            homeViewModelList.add(
                HomeViewModel(
                    item.description, item.operation,
                    getAction(item.operation!!), item.title, color()
                )
            )
        }

        initAction(homeViewModelList)
    }

    private fun getAction(operation_part: String): Drawable? {
        when (operation_part) {
            "work_order" -> {
                return ResourcesCompat.getDrawable(resources, R.drawable.home_work_order, null)
            }
            "complaint" -> {
                return ResourcesCompat.getDrawable(resources, R.drawable.ic_home_complain, null)
            }
            "inspection" -> {
                return ResourcesCompat.getDrawable(resources, R.drawable.ic_home_inspection, null)
            }
            "maintenance" -> {
                return ResourcesCompat.getDrawable(
                    resources,
                    R.drawable.corrective_maintenance,
                    null
                )
            }
            "parking" -> {
                return ResourcesCompat.getDrawable(resources, R.drawable.ic_home_parking, null)
            }
            "emergency" -> {
                return ResourcesCompat.getDrawable(
                    resources,
                    R.drawable.ic_emergency_notification,
                    null
                )
            }
            "electric" -> {
                return ResourcesCompat.getDrawable(resources, R.drawable.home_electri_meter, null)
            }
            "water" -> {
                return ResourcesCompat.getDrawable(
                    resources,
                    R.drawable.ic_home_water_records,
                    null
                )
            }
            "front-desk", "front_desk" -> {
                return ResourcesCompat.getDrawable(resources, R.drawable.ic_front_desk, null)
            }
            "evaluate" -> {
                return ResourcesCompat.getDrawable(resources, R.drawable.ic_availability, null)
            }
            "cleaning" -> {
                return ResourcesCompat.getDrawable(resources, R.drawable.ic_availability, null)
            }
            "security" -> {
                return ResourcesCompat.getDrawable(
                    resources,
                    R.drawable.icon_security_emergency,
                    null
                )
            }
            "spider_man" -> {
                return ResourcesCompat.getDrawable(resources, R.drawable.ic_home_spiderman, null)
            }
            "landscaping" -> {
                return ResourcesCompat.getDrawable(resources, R.drawable.ic_home_landscape, null)
            }
            "maintenance_work_engine" -> {
                return ResourcesCompat.getDrawable(resources, R.drawable.ic_home_spiderman, null)
            }
        }
        return ResourcesCompat.getDrawable(resources, R.drawable.ic_home_notebook_v2, null)
    }

    private fun color(): Int {
        return Utils.setColorBackground(i) { index -> i = index }
    }

    private fun storePermission(userPermissionModelList: MutableList<MyRolePermissionModel>) {
        // Get Permission
        for (permissionModel in userPermissionModelList) {
            if (permissionModel.operation != null) userPermissionModel[permissionModel.operation!!] =
                permissionModel
        }

        // Store Permission
        if (userPermissionModel.size > 0) {
            if (userPermissionModel[StaticUtilsKey.complaint_solutions_permission] != null) MockUpData.UPDATE_COMPLAINT =
                userPermissionModel[StaticUtilsKey.complaint_solutions_permission]!!.is_role

            if (userPermissionModel[StaticUtilsKey.emergency_permission] != null) {
                MockUpData.ROLE_EMERGENCY =
                    userPermissionModel[StaticUtilsKey.emergency_permission]!!.is_role
                MockUpData.READ_EMERGENCY = true
            }
            if (userPermissionModel[StaticUtilsKey.frontdesk_permission] != null) MockUpData.ROLE_FRONT_DEST =
                userPermissionModel[StaticUtilsKey.frontdesk_permission]!!.is_role
            if (userPermissionModel[StaticUtilsKey.parking_permission] != null) MockUpData.ROLE_PARKING =
                userPermissionModel[StaticUtilsKey.parking_permission]!!.is_role
            if (userPermissionModel[StaticUtilsKey.cleaning_and_service_permission] != null) MockUpData.ROLE_CLEANER =
                userPermissionModel[StaticUtilsKey.cleaning_and_service_permission]!!.is_role
            if (userPermissionModel[StaticUtilsKey.inspection_permission] != null) MockUpData.ROLE_INSPECTION_WORK_ORDER =
                userPermissionModel[StaticUtilsKey.inspection_permission]!!.is_role
        }
    }

    // More step
    private fun setMenuStepAdapter(context: Activity) {
        val list = ArrayList<CustomCategoryModel>()
        list.addAll(addMenuStepList(context, "", actionCategory))

        CustomMenuHeaderAdapter.setMenuStepAdapter(
            context,
            "step_menus",
            list,
            object : CustomMenuHeaderAdapter.ClickCallBackListener {
                override fun onClickCallBack(item: CustomCategoryModel) {
                    finish()
                }
            })
    }

}