package com.eazy.daikou.ui.home.front_desk.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseBottomSheetDialogFragment
import com.eazy.daikou.base.MockUpData
import com.eazy.daikou.request_data.request.front_desk_ws.FrontDeskWS
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.front_desk.SearchFrontDeskModel
import com.eazy.daikou.model.profile.User
import com.eazy.daikou.ui.home.front_desk.adapter.AdapterSearchFrontDesk
import com.google.gson.Gson

class ItemCategoryUnitFragment : BaseBottomSheetDialogFragment() {

    private lateinit var noDataTv : TextView
    private lateinit var searchEt : EditText
    private lateinit var iconClearText : ImageView
    private lateinit var recyclerViewFrontDesk : RecyclerView
    private lateinit var progressBar: ProgressBar
    private var listItemSearchFrontDesk : ArrayList<SearchFrontDeskModel> = ArrayList()
    private lateinit var adapterItemComplaint : AdapterSearchFrontDesk
    private lateinit var callBackItemClick : CallBackItemAfterClick
    private lateinit var linearLayoutManager : LinearLayoutManager
    private var currentItem = 0
    private var total : Int = 0
    private var scrollDown : Int = 0
    private var currentPage : Int = 1
    private var size : Int = 20
    private var isScrolling = false
    private var isScrollItem = false

    private var user: User? = null
    private var accountID = ""

    fun newInstance() : ItemCategoryUnitFragment{
        val fragment = ItemCategoryUnitFragment()
        val args = Bundle()
        fragment.arguments = args
        return fragment
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view =  inflater.inflate(R.layout.fragment_select_account_unit, container, false)

        initView(view)

        initData()

        initAction()

        return view
    }

    private fun initView(view : View){
        view.findViewById<TextView>(R.id.tittleTv).text = getString(R.string.select_unit)
        searchEt = view.findViewById(R.id.searchEt)
        iconClearText = view.findViewById(R.id.iconClearText)
        noDataTv = view.findViewById(R.id.noDataTv)
        recyclerViewFrontDesk = view.findViewById(R.id.recyclerViewComplaint)
        progressBar = view.findViewById(R.id.progressItem)

        view.findViewById<ImageView>(R.id.iconDismiss).setOnClickListener { dismiss() }
    }

    private fun initData(){
        val sessionManagement = UserSessionManagement(mActivity)
        user = Gson().fromJson(sessionManagement.userDetail, User::class.java)
        accountID = MockUpData.getAccountUserId(sessionManagement)
    }

    private fun initAction() {

        initOnScrollItem(recyclerViewFrontDesk)

        requestServiceAPI("")

        searchItemComplaint()

        linearLayoutManager = LinearLayoutManager(mActivity)
        recyclerViewFrontDesk.layoutManager = linearLayoutManager
        adapterItemComplaint = AdapterSearchFrontDesk(mActivity, listItemSearchFrontDesk, callBackItem)
        recyclerViewFrontDesk.adapter = adapterItemComplaint
    }

    private fun searchItemComplaint(){
        searchEt.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

            override fun afterTextChanged(editable: Editable) {
                iconClearText.setOnClickListener {
                    searchEt.setText("")
                    editable.clear()
                }
                if (editable.toString().isNotEmpty()) {
                    progressBar.visibility = View.VISIBLE
                    iconClearText.visibility = View.VISIBLE
                    listItemSearchFrontDesk.clear()
                    requestServiceAPI(editable.toString())
                } else {
                    listItemSearchFrontDesk.clear()
                    iconClearText.visibility = View.GONE
                    requestServiceAPI( "")
                }
            }
        })
    }

    private fun initOnScrollItem(recyclerViewItem: RecyclerView) {
        recyclerViewItem.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true
                }
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                currentItem = linearLayoutManager.childCount
                total = linearLayoutManager.itemCount
                scrollDown = linearLayoutManager.findFirstVisibleItemPosition()
                if (dy > 0) {
                    if (isScrolling && currentItem + scrollDown == total) {
                        if (total == size) {
                            isScrolling = false
                            currentPage++
                            progressBar.visibility = View.VISIBLE
                            isScrollItem = true
                            recyclerViewItem.scrollToPosition(listItemSearchFrontDesk.size - 1)
                            requestServiceAPI("")
                            size += 20
                        } else {
                            progressBar.visibility = View.GONE
                        }
                    }
                }
            }
        })
    }

    private fun requestServiceAPI(  keySearch : String){
        progressBar.visibility = View.VISIBLE
        FrontDeskWS().searchFrontDeskWS(mActivity, currentPage, 20, accountID, keySearch, callBackSearchFrontDesk)
    }

    private val callBackSearchFrontDesk = object : FrontDeskWS.CallBackSearchFrontDeskListener{
        @SuppressLint("NotifyDataSetChanged")
        override fun onSuccessFul(listSearch: ArrayList<SearchFrontDeskModel>) {
            progressBar.visibility = View.GONE

            listItemSearchFrontDesk.addAll(listSearch)

            noDataTv.visibility =  if (listItemSearchFrontDesk.size >= 0) View.GONE else View.VISIBLE

            adapterItemComplaint.notifyDataSetChanged()
        }

        override fun onFailed(mess: String) {
            progressBar.visibility = View.GONE
            Utils.customToastMsgError(mActivity, mess, false)
        }
    }

    private val callBackItem : AdapterSearchFrontDesk.CallBackSearchListener = object : AdapterSearchFrontDesk.CallBackSearchListener{
        override fun onClickItemSearch(searchFrontDesk: SearchFrontDeskModel) {
            callBackItemClick.clickItemListener(searchFrontDesk)
            dismiss()
        }
    }

    fun initDataListener(callBackItemClick : CallBackItemAfterClick){
        this.callBackItemClick = callBackItemClick
    }

    interface CallBackItemAfterClick{
        fun clickItemListener(searchFrontDesk: SearchFrontDeskModel)
    }

}