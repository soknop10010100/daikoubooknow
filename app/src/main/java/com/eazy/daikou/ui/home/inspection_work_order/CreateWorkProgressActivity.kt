package com.eazy.daikou.ui.home.inspection_work_order

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.base.BaseCameraActivity
import com.eazy.daikou.request_data.request.work_oder_ws.WorkOrderWs
import com.eazy.daikou.request_data.request.work_oder_ws.WorkOrderWs.CallBackWorkOrderListener
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.work_order.WorkOrderDetailModel
import com.eazy.daikou.model.work_order.WorkOrderProgressModel
import com.eazy.daikou.helper.DateNumPickerFragment
import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog
import java.text.ParseException
import java.text.SimpleDateFormat
import java.time.LocalTime
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import com.eazy.daikou.request_data.static_data.StaticUtilsKey
import com.eazy.daikou.helper.DateUtil
import com.eazy.daikou.model.work_order.ImageListModel
import com.eazy.daikou.ui.home.inspection_work_order.adapter.ImageListWorkOrderAdapter


class CreateWorkProgressActivity : BaseActivity(), View.OnClickListener {

    private val REQUEST_IMAGE = 882
    private lateinit var progressBar : ProgressBar
    private var action : String = ""
    private lateinit var workOrderDetailModel: WorkOrderProgressModel
    private lateinit var titleToolbar : TextView
    private lateinit var btnSave : TextView
    private lateinit var startTimeTv : TextView
    private lateinit var endTimeTv : TextView
    private lateinit var spendTimeTv : TextView
    private lateinit var dateTv : TextView
    private lateinit var calendarLayout : LinearLayout
    private lateinit var calendarView: CalendarView
    private lateinit var btnAddImage : ImageView

    // Start Time, End Time
    private var startTimeFormat: String = ""
    private var endTimeFormat: String = ""
    private var isStartTime = true
    private var isFirstSet = false
    private lateinit var startTimeDate : Date
    private lateinit var endTimeDate : Date
    private lateinit var start : LocalTime
    private lateinit var end : LocalTime

    private var selectConductDate: String = ""
    private lateinit var commentEdt : EditText
    private lateinit var descriptionTv : EditText
    private lateinit var txtNoImage: TextView
    private lateinit var imageRecyclerView: RecyclerView

    private lateinit var doneStatusTv : TextView
    private val bitmapList: ArrayList<ImageListModel> = ArrayList()
    private var post = 0
    private lateinit var workOrderModel: WorkOrderDetailModel
    private var hashMapImageList : HashMap<String, Any> = HashMap()
    private var workOrderId : String = ""

    private lateinit var dateFormatServer : SimpleDateFormat
    private lateinit var dateFormShow : SimpleDateFormat
    private lateinit var timeFormat : SimpleDateFormat

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_work_order)

        initView()

        initData()

        initAction()

    }

    private fun initView(){
        titleToolbar = findViewById(R.id.titleToolbar)
        findViewById<View>(R.id.layoutMap).visibility = View.INVISIBLE
        findViewById<View>(R.id.iconBack).setOnClickListener { finish() }
        progressBar = findViewById(R.id.progressItem)
        progressBar.visibility = View.GONE

        btnSave = findViewById(R.id.btnSave)

        startTimeTv = findViewById(R.id.startTimeTv)
        endTimeTv = findViewById(R.id.endTimeTv)
        spendTimeTv = findViewById(R.id.duringTv)
        dateTv = findViewById(R.id.dateTv)
        doneStatusTv =  findViewById(R.id.doneStatusTv)
        calendarLayout = findViewById(R.id.calendarLayout)
        calendarView  = findViewById(R.id.date_picker)
        btnAddImage = findViewById(R.id.btnAddImage)
        commentEdt = findViewById(R.id.totalComponentListEdt)
        descriptionTv = findViewById(R.id.descriptionTv)

        txtNoImage = findViewById(R.id.txtNoImageAdd)
        imageRecyclerView = findViewById(R.id.list_image)

    }

    private fun initData(){
        if (intent != null && intent.hasExtra("action")){
            action = intent.getStringExtra("action").toString()
        }
        if(action == "create_new_work_order"){
            titleToolbar.text = resources.getString(R.string.create_work_progress).uppercase(Locale.ROOT)
        } else{
            titleToolbar.text = resources.getString(R.string.edit_work_progress).uppercase(Locale.ROOT)
            btnSave.text = resources.getString(R.string.update)
        }
        if (intent != null && intent.hasExtra("work_order_detail")){
            workOrderDetailModel = intent.getSerializableExtra("work_order_detail") as WorkOrderProgressModel
        }
        if (intent != null && intent.hasExtra("work_order_model")){
            workOrderModel = intent.getSerializableExtra("work_order_model") as WorkOrderDetailModel
        }

        if (intent != null && intent.hasExtra("work_order_id")){
            workOrderId = intent.getStringExtra("work_order_id").toString()
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun initAction(){
        startTimeTv.setOnClickListener(this)
        endTimeTv.setOnClickListener(this)
        dateTv.setOnClickListener(this)
        btnAddImage.setOnClickListener(this)
        doneStatusTv.setOnClickListener(this)
        btnSave.setOnClickListener(this)

        dateFormatServer = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        dateFormShow = SimpleDateFormat("dd, MMM yyyy", Locale.getDefault())
        timeFormat = SimpleDateFormat("HH:mm", Locale.getDefault())

        if(action == "update_work_order"){
            setValue()
        } else {
            setDefaultValues()
        }
    }

    private fun setValue() {
        if (workOrderDetailModel.workingDate != null){
            dateTv.text = Utils.formatDateFromString("yyyy-MM-dd", "dd, MMM yyyy", workOrderDetailModel.workingDate)
            selectConductDate = workOrderDetailModel.workingDate
        }

        if (workOrderDetailModel.fromTime != null){
            startTimeTv.text = workOrderDetailModel.fromTime
            startTimeFormat = workOrderDetailModel.fromTime
            startTimeDate = DateUtil.formatSimpleDateToTimeZone(dateFormatServer.format(Date()) + " " + workOrderDetailModel.fromTime)
        }
        if (workOrderDetailModel.toTime != null){
            endTimeTv.text = workOrderDetailModel.toTime
            endTimeFormat = workOrderDetailModel.toTime
            endTimeDate = DateUtil.formatSimpleDateToTimeZone(dateFormatServer.format(Date()) + " " + workOrderDetailModel.toTime)
        }
        if (workOrderDetailModel.spentTime != null){
            spendTimeTv.text = DateUtil.convertMinutesToHours(workOrderDetailModel.spentTime)
        }

        if (workOrderDetailModel.donePercent != null) doneStatusTv.text = workOrderDetailModel.donePercent

        if (workOrderDetailModel.comment != null) commentEdt.setText(workOrderDetailModel.comment)
        if (workOrderDetailModel.description != null) descriptionTv.setText(workOrderDetailModel.description)

        for (image in workOrderDetailModel.workOrderProgressImages) {
            val convertUrlToBitmap = Utils.convertUrlToBase64(image)
            hashMapImageList[post.toString()] = image
            addUrlImage(post.toString(), convertUrlToBitmap)
        }

        setImageRecyclerView(post)

    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun setDefaultValues(){
        if (workOrderModel.maxDonePercentSelection[0] != null) doneStatusTv.text = workOrderModel.maxDonePercentSelection[0]

        dateTv.text = dateFormShow.format(Date())
        selectConductDate = dateFormatServer.format(Date())

        startTimeFormat = timeFormat.format(Date())
        startTimeTv.text = startTimeFormat
        startTimeDate = Date()

        val calendar = Calendar.getInstance()
        calendar.time = Date()
        calendar.add(Calendar.MINUTE, 15)
        endTimeFormat = timeFormat.format(calendar.time)
        endTimeTv.text = endTimeFormat
        endTimeDate = calendar.time

        isFirstSet = true
        compareTime(startTimeDate, endTimeDate, startTimeFormat, endTimeFormat)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onClick(v: View) {
        when (v.id) {
            R.id.startTimeTv -> {
                calendarTimeDialog()
                isStartTime = true
            }
            R.id.endTimeTv -> {
                calendarTimeDialog()
                isStartTime = false
            }
            R.id.dateTv -> {
                calendarDate()
            }
            R.id.btnAddImage -> {
                addAttachmentImg()
            }
            R.id.doneStatusTv -> {
                if (action == "update_work_order"){
                    selectDonePercent(workOrderDetailModel.donePercentSelection)
                } else {
                    selectDonePercent(workOrderModel.maxDonePercentSelection)
                }
            }
            R.id.btnSave ->{
                progressBar.visibility = View.VISIBLE
                if (action == "update_work_order") {
                    val hashMap: HashMap<String, Any> = hashMapGetData("edit")
                    WorkOrderWs()
                        .saveWorkOrderDetail(this, action, hashMap, callBackListener)
                } else {
                    val hashMap: HashMap<String, Any> = hashMapGetData("add")
                    WorkOrderWs()
                        .saveWorkOrderDetail(this, action, hashMap, callBackListener)
                }
            }
        }
    }

    private fun selectDonePercent(donePercentList : List<String>){
        if(donePercentList.isNotEmpty()) {
            val dateNumPickerFragment = DateNumPickerFragment.newInstance("done_percent", donePercentList)
            dateNumPickerFragment.CallBackListener { _: String, values: String, _: String? ->
                doneStatusTv.text = values
            }
            dateNumPickerFragment.show(supportFragmentManager, "Dialog Fragment")
        } else {
            Utils.customToastMsgError(this, "Done Percent is none", false);
        }
    }

    private fun addAttachmentImg() {
        startActivityForResult(
            Intent(this@CreateWorkProgressActivity, BaseCameraActivity::class.java), REQUEST_IMAGE
        )
    }

    private fun calendarDate(){

        if (calendarLayout.visibility == View.VISIBLE) {
            setDrawableEnd(dateTv, R.drawable.ic_drop_down)
            calendarLayout.visibility = View.GONE
        } else {
            setDrawableEnd(dateTv, R.drawable.right_arrow_back)
            calendarLayout.visibility = View.VISIBLE
        }

        calendarView.setOnDateChangeListener { _: CalendarView?, year: Int, month: Int, dayOfMonth: Int ->
            selectConductDate = year.toString() + "-" + (month + 1) + "-" + dayOfMonth
            dateTv.text = Utils.formatDateFromString("yyyy-MM-dd", "dd, MMM yyyy", selectConductDate)
        }

    }

    private fun setDrawableEnd(view : TextView, icon : Int){
        view.setCompoundDrawablesWithIntrinsicBounds(0, 0, icon, 0)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun calendarTimeDialog() {
        SingleDateAndTimePickerDialog.Builder(this@CreateWorkProgressActivity)
            .minutesStep(15)
            .bottomSheet()
            .curved()
            .displayMinutes(true)
            .displayHours(true)
            .displayDays(false)
            .displayMonth(false)
            .displayYears(false)
            .displayAmPm(false)
            .displayDaysOfMonth(false)
            .listener { date: Date? ->
                val sdf = SimpleDateFormat("kk:mm")
                if (isStartTime) {
                    if(endTimeFormat == ""){
                        startTimeFormat = sdf.format(date)
                        startTimeTv.text = startTimeFormat
                        startTimeDate = date!!
                    } else {
                        date?.let { validateEndTimeTime(it, sdf.format(date), endTimeFormat) }
                    }
                } else {
                    date?.let { validateEndTimeTime(it, startTimeFormat, sdf.format(date)) }
                }
            }.display()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun spendTime(inTime : Date, outTime : Date, startTime : String , endTime : String){

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            val timeFormatter = DateTimeFormatter.ofPattern("hh:mm a", Locale.ENGLISH)
//            val sdf = SimpleDateFormat("kk:mm a")
//            val startT = sdf.format(inTime)
//            val endT = sdf.format(outTime)
//
//            if (isStartTime && !isFirstSet){
//                val startArr = startT.split(":").toTypedArray()
//                start = if (startArr[0] =="12"){
//                    LocalTime.parse(startT, timeFormatter)
//                } else {
//                    LocalTime.parse(Utils.formatDateTime(startTime,"hh:mm", "hh:mm a"), timeFormatter)
//                }
//                val endArr = endT.split(":").toTypedArray()
//                end = if (endArr[0] =="12"){
//                    LocalTime.parse(endT, timeFormatter)
//                } else {
//                    LocalTime.parse(Utils.formatDateTime(endTime,"hh:mm", "hh:mm a"), timeFormatter)
//                }
//            } else if (!isStartTime && !isFirstSet){
//                val startArr = startT.split(":").toTypedArray()
//                start = if (startArr[0] =="12"){
//                    LocalTime.parse(startT, timeFormatter)
//                } else {
//                    LocalTime.parse(Utils.formatDateTime(startTime,"hh:mm", "hh:mm a"), timeFormatter)
//                }
//                val endArr = endT.split(":").toTypedArray()
//                end = if (endArr[0] =="12"){
//                    LocalTime.parse(endT, timeFormatter)
//                } else {
//                    LocalTime.parse(Utils.formatDateTime(endTime,"hh:mm", "hh:mm a"), timeFormatter)
//                }
//            } else {
//                val startArr = startT.split(":").toTypedArray()
//                start = if (startArr[0] =="12"){
//                    LocalTime.parse(startT, timeFormatter)
//                } else {
//                    LocalTime.parse(Utils.formatDateTime(startTime,"hh:mm", "hh:mm a"), timeFormatter)
//                }
//
//                val endArr = endT.split(":").toTypedArray()
//                end = if (endArr[0] =="12"){
//                    LocalTime.parse(endT, timeFormatter)
//                } else {
//                    LocalTime.parse(Utils.formatDateTime(endTime,"hh:mm", "hh:mm a"), timeFormatter)
//                }
//                isFirstSet = false
//            }
//
//            val diff = Duration.between(start, end)
//
//            val hours = diff.toHours()
//            val minutes = diff.minusHours(hours).toMinutes()
//            val totalTimeString = String.format("%02d : %02d", hours, minutes)
//
//            if (isStartTime) {
//                startTimeTv.text = startTime
//            } else {
//                endTimeTv.text = endTime
//            }
//            spendTimeTv.text = totalTimeString
//        } else {
//            val img: Drawable = resources.getDrawable(R.drawable.ic__drop_down)
//            spendTimeTv.setCompoundDrawablesWithIntrinsicBounds(null, null, img, null)
//        }

        val simpleDateFormat = SimpleDateFormat("HH:mm")
        var startDate: Date? = null
        try {
            startDate = simpleDateFormat.parse(startTime)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        var endDate: Date? = null
        try {
            endDate = simpleDateFormat.parse(endTime)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        var difference = endDate!!.time - startDate!!.time
        if (difference < 0) {
            var dateMax: Date? = null
            try {
                dateMax = simpleDateFormat.parse("24:00")
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            var dateMin: Date? = null
            try {
                dateMin = simpleDateFormat.parse("00:00")
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            difference = dateMax!!.time - startDate.time + (endDate.time - dateMin!!.time)
        }
        val days = (difference / (1000 * 60 * 60 * 24)).toInt()
        val hours = ((difference - 1000 * 60 * 60 * 24 * days) / (1000 * 60 * 60)).toInt()
        val minutes = (difference - 1000 * 60 * 60 * 24 * days - 1000 * 60 * 60 * hours).toInt() / (1000 * 60)

        if (isStartTime) {
            startTimeTv.text = startTime
        } else {
            endTimeTv.text = endTime
        }
        val totalTimeString = String.format("%02d : %02d", hours, minutes)
        spendTimeTv.text = totalTimeString

    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun validateEndTimeTime(date : Date, startTime : String, endTime : String){
        try {
            if(isStartTime){
                compareTime(date, endTimeDate, startTime, endTime)
            } else {
                compareTime(startTimeDate , date, startTime, endTime)
            }

        } catch (e: ParseException) {
            e.printStackTrace()
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun compareTime(inTime : Date, outTime : Date, startTime : String, endTime : String){
        if (isTimeAfter(inTime, outTime)) {
            val sdf2 = SimpleDateFormat("kk:mm")
            if(isStartTime){
                startTimeFormat = sdf2.format(inTime)
                startTimeDate = inTime
            } else {
                endTimeFormat = sdf2.format(outTime)
                endTimeDate = outTime
            }
            spendTime(inTime, outTime, startTime , endTime)
        } else {
            if(isStartTime){
                Utils.customToastMsgError(this@CreateWorkProgressActivity, resources.getString(R.string.start_time_must_bigger_than_end_time), false)
            } else {
                Utils.customToastMsgError(this@CreateWorkProgressActivity, resources.getString(R.string.end_time_must_bigger_than_start_time), false)
            }
        }
    }

    private fun isTimeAfter(startTime: Date?, endTime: Date): Boolean {
        return !endTime.before(startTime)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK && requestCode == REQUEST_IMAGE) {
            assert(data != null)
            if (data!!.hasExtra("image_path")) {
                val imagePath = data.getStringExtra("image_path")
                addUrlImage("", imagePath!!)
                setImageRecyclerView(post)
            }
            if (data.hasExtra("select_image")) {
                val imagePath = data.getStringExtra("select_image")
                addUrlImage("", imagePath!!)
                setImageRecyclerView(post)
            }
        }
    }

    private fun addUrlImage(keyDefineImage : String, imagePath: String) {
        val loadedBitmap = BaseCameraActivity.convertByteToBitmap(imagePath)
        post += 1
        val imageListModel = ImageListModel()
        imageListModel.keyImage = keyDefineImage
        imageListModel.bitmapImage = loadedBitmap
        bitmapList.add(imageListModel)
    }

    private fun setImageRecyclerView(post: Int) {
        txtNoImage.visibility = if (bitmapList.isNotEmpty()) View.GONE else View.VISIBLE

        imageRecyclerView.layoutManager = GridLayoutManager(this, 1, RecyclerView.HORIZONTAL, false)
        val pickUpAdapter = ImageListWorkOrderAdapter(this, post, bitmapList, object : ImageListWorkOrderAdapter.OnClearImage{
            override fun onClickRemove(bitmap: ImageListModel, post: Int) {
                if (bitmap.keyImage == ""){
                    removeImage(post, bitmap)
                } else {
                    var hashMapSt = hashMapImageList[bitmap.keyImage]
                    if (hashMapImageList[bitmap.keyImage] != null){
                        deleteImageAlert(this@CreateWorkProgressActivity, StaticUtilsKey.work_progress_image, workOrderDetailModel.workProgressId,
                            hashMapImageList[bitmap.keyImage] as String, bitmap, post
                        )
                    }

                }
            }

            override fun onViewImage(bitmap: ImageListModel) {
                if (bitmap.bitmapImage != null) {
                    Utils.openImageOnDialog(this@CreateWorkProgressActivity, Utils.convert(bitmap.bitmapImage))
                }
            }

        })
        imageRecyclerView.adapter = pickUpAdapter

        btnAddImage.visibility = Utils.visibleView(bitmapList)
    }

    private fun hashMapGetData(actionType : String): HashMap<String, Any> {
        val hashMap = HashMap<String, Any>()
        val listImage: MutableList<String> = ArrayList()
        if (bitmapList.size == 0) {
            listImage.add("")
        }
        for (bitmap in bitmapList) {
            if (bitmap.keyImage == "") {    // when key image is empty mean add new
                listImage.add(Utils.convert(bitmap.bitmapImage))
            }
        }
        hashMap["user_id"] = UserSessionManagement(this@CreateWorkProgressActivity).userId
        hashMap["function_type"] = actionType
        hashMap["work_order_id"] = workOrderId
        if(actionType == "add"){
            val valArr: Array<String> = spendTimeTv.text.toString().trim().split(":".toRegex()).toTypedArray()
            val spentTimeMinutes = (valArr[0].replace(" ", "").toInt() * 60) + (valArr[1].replace(" ", "").toInt())
            hashMap["spent_time"] = spentTimeMinutes
        } else{
            hashMap["work_progress_id"] = workOrderDetailModel.workProgressId
            val valArr: Array<String> = spendTimeTv.text.toString().trim().split(":".toRegex()).toTypedArray()
            val spentTimeMinutes = (valArr[0].replace(" ", "").toInt() * 60) + (valArr[1].replace(" ", "").toInt())
            hashMap["spent_time"] = spentTimeMinutes
        }
        hashMap["working_date"] = selectConductDate
        hashMap["from_time"] = startTimeFormat
        hashMap["to_time"] = endTimeFormat
        hashMap["done_percent"] = doneStatusTv.text.toString().replace("%", "").trim()
        hashMap["work_order_progress_images"] = listImage
        hashMap["comment"] = commentEdt.text.toString().trim()
        hashMap["description"] = descriptionTv.text.toString().trim()

        return hashMap
    }

    private var callBackListener = object : CallBackWorkOrderListener{
        override fun onSuccessDeleteImageWorkOrder(msg: String?) {
            progressBar.visibility = View.GONE
            Utils.customToastMsgError(this@CreateWorkProgressActivity, msg, true)
            setResult(RESULT_OK)
            finish()
        }

        override fun onFailed(msg: String?) {
            progressBar.visibility = View.GONE
            Utils.customToastMsgError(this@CreateWorkProgressActivity, msg, false)
        }

    }

    private fun deleteImageAlert(context: Context, actionType: String, workOrderId: String, filePathImage: String, bitmap: ImageListModel, position : Int) {
        AlertDialog.Builder(context)
            .setTitle(resources.getString(R.string.confirm))
            .setMessage(resources.getString(R.string.are_you_sure_to_delete))
            .setPositiveButton(resources.getString(R.string.yes)) { dialog: DialogInterface, _: Int ->
                requestServiceDelete(actionType, workOrderId, filePathImage, bitmap, position)
                dialog.dismiss()
            }
            .setNegativeButton(
                resources.getString(R.string.no)
            ) { dialog: DialogInterface, _: Int -> dialog.dismiss() }
            .setCancelable(false)
            .setIcon(ResourcesCompat.getDrawable(resources, R.drawable.ic_report_problem, null))
            .show()
    }

    private fun requestServiceDelete(type: String, workOrderId: String, filePathImage: String, bitmap: ImageListModel, position: Int) {
        val hashMap = HashMap<String, Any>()
        hashMap["delete_image_type"] = type
        if (type == StaticUtilsKey.work_order_image){
            hashMap["work_order_id"] = workOrderId
        } else {
            hashMap["work_progress_id"] = workOrderId
        }
        hashMap["file_path"] = filePathImage
        progressBar.visibility = View.VISIBLE
        WorkOrderWs()
            .deleteImageWorkOrder(this@CreateWorkProgressActivity, hashMap,  object : CallBackWorkOrderListener {
            override fun onSuccessDeleteImageWorkOrder(msg: String) {
                progressBar.visibility = View.GONE
                Utils.customToastMsgError(this@CreateWorkProgressActivity, msg, true)

                removeImage(position, bitmap)
                hashMapImageList.remove(bitmap.keyImage)
            }

            override fun onFailed(msg: String) {
                progressBar.visibility = View.GONE
                Utils.customToastMsgError(this@CreateWorkProgressActivity, msg, false)
            }
        })
    }

    private fun removeImage(position: Int, bitmap: ImageListModel){
        bitmapList.remove(bitmap)
        setImageRecyclerView(position)
    }

}