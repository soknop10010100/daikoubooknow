package com.eazy.daikou.ui.home.facility_booking.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.helper.DateUtil
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.facility_booking.CardItemByProperty
import com.eazy.daikou.model.facility_booking.my_booking.MyBookingItemsModel

class MyBookingDetailAdapter (private val list : ArrayList<MyBookingItemsModel>, private val onClickCallBack : CallBackListener) : RecyclerView.Adapter<MyBookingDetailAdapter.ItemViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        return ItemViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.my_booking_item_model, parent, false))
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val myBookingModel = list[position]
        if (myBookingModel != null){
            Glide.with(holder.imgSpace).load(if(myBookingModel.space_image != null) myBookingModel.space_image else R.drawable.no_image).into(holder.imgSpace)
            holder.priceTv.text = if (myBookingModel.total_amount != null) "$ ${myBookingModel.total_amount}" else "- - -"
            holder.bookingDateTv.text = if (myBookingModel.booking_date != null) Utils.formatDateFromString("yyyy-MM-dd", "dd, MMM yyyy", myBookingModel.booking_date) else "- - -"

            Utils.setValueOnText(holder.itemNameTv, myBookingModel.space_name)
            Utils.setValueOnText(holder.numberOfPeopleTv, myBookingModel.quantity)

            holder.unitPriceTv.text = if (myBookingModel.type_price != null) "${Utils.getValueTypePriceBooking(holder.unitPriceTv.context)[myBookingModel.type_price]}" else "- - -"
            holder.priceTv.text = if (myBookingModel.total_amount != null) "$ ${myBookingModel.total_amount}" else "- - -"
            holder.taxTv.text = if (myBookingModel.total_tax_amount != null) "$ ${myBookingModel.total_tax_amount}" else "- - -"

            holder.cardViewRing2.setOnClickListener{onClickCallBack.onClickCallBack(myBookingModel)}
        }
    }

    override fun getItemCount(): Int {
        return  list.size
    }

    class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val itemNameTv: TextView = itemView.findViewById(R.id.itemNameTv2)
        val priceTv: TextView = itemView.findViewById(R.id.priceTv2)
        val cardViewRing2 : CardView = itemView.findViewById(R.id.cardViewRing2)
        val imgSpace : ImageView = itemView.findViewById(R.id.imgSpace)
        val taxTv : TextView = itemView.findViewById(R.id.taxTv)
        val numberOfPeopleTv : TextView = itemView.findViewById(R.id.numberOfPeopleTv)
        val unitPriceTv : TextView = itemView.findViewById(R.id.unitPriceTv)
        val bookingDateTv : TextView = itemView.findViewById(R.id.bookingDateTv)
    }

    interface CallBackListener{
        fun onClickCallBack(myBookingDetail : MyBookingItemsModel)
    }

    fun clear(){
        val size = list.size
        if (size > 0) {
            for (i in 0 until size) {
                list.removeAt(0)
            }
            notifyItemRangeRemoved(0, size)
        }
    }

}