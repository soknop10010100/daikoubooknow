package com.eazy.daikou.ui.home.service_provider_new.adapter_service_provider

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.model.service_provider.ListAllServiceProviderModel

class MoreMenuCategoryAdapter(private val context: Context, private val listAllProvider: ArrayList<ListAllServiceProviderModel>, private val callBackListAll: CallBackItemListAllCategoryListener): RecyclerView.Adapter<MoreMenuCategoryAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
     val view = LayoutInflater.from(context).inflate(R.layout.custom_layout_view_more_category, parent, false)
        return  ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val listAllProvider : ListAllServiceProviderModel = listAllProvider[position]
        if (listAllProvider != null){
            if (listAllProvider.icon != null){
                Glide.with(context).load(listAllProvider.icon).into(holder.imageIcon)
            } else {
                Glide.with(context).load(R.drawable.no_image).into(holder.imageIcon)
            }
            holder.nameItemTv.text = if(listAllProvider.name != null) listAllProvider.name else "- - -"

            holder.itemView.setOnClickListener { callBackListAll.clickViewAllCallBack(listAllProvider) }
        }
    }

    override fun getItemCount(): Int {
        return listAllProvider.size
    }

    class ViewHolder(view : View) : RecyclerView.ViewHolder(view){
        var imageIcon : ImageView = view.findViewById(R.id.imageIcon)
        var nameItemTv : TextView = view.findViewById(R.id.nameItemTv)
    }

    interface CallBackItemListAllCategoryListener{
        fun clickViewAllCallBack(listAllProvider : ListAllServiceProviderModel)
    }
}