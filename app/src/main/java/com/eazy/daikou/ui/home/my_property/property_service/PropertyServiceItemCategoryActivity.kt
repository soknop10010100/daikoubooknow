package com.eazy.daikou.ui.home.my_property.property_service

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.activity.result.ActivityResult
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.databinding.ActivityPropertyServiceItemCategoryBinding
import com.eazy.daikou.helper.CustomSliderImageClass
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.home.HomeViewModel
import com.eazy.daikou.model.work_order.ImageListModel
import com.eazy.daikou.ui.home.book_now.booking_hotel.adapter.AdapterImageSlider
import com.eazy.daikou.ui.home.my_property.property_service.adapter.PropertyServiceCategoryAdapter

class PropertyServiceItemCategoryActivity : BaseActivity() {

    private lateinit var mBind : ActivityPropertyServiceItemCategoryBinding
    private lateinit var recycleView: RecyclerView
    private val sliderHandler: Handler = Handler(Looper.getMainLooper())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBind = ActivityPropertyServiceItemCategoryBinding.inflate(layoutInflater)
        setContentView(mBind.root)

        Utils.customOnToolbar(this, "Choose Service") { finish() }
        recycleView = mBind.recyclerView.recyclerView

        recycleView.layoutManager = GridLayoutManager(this, 3)
        val homeIconMenuAdapter = PropertyServiceCategoryAdapter(PropertyServiceCategoryAdapter.getListHomeMenu(this), object : PropertyServiceCategoryAdapter.ItemClickOnListener {
            override fun onItemClick(homeViewModel: HomeViewModel) {
                val intent = Intent(this@PropertyServiceItemCategoryActivity, PropertyServiceAllCategoryActivity::class.java)
                activityLauncher.launch(intent) { result: ActivityResult ->
                    if (result.resultCode == RESULT_OK) {
                        setResult(RESULT_OK)
                        finish()
                    }
                }
            }

        })
        recycleView.adapter = homeIconMenuAdapter

        // Slide
        initSlideBanner()

    }

    private fun initSlideBanner() {
        val imageList = listOf(
            "https://dev.booknow.asia/images/home_slider_1.jpg",
            "https://dev.booknow.asia/images/home_slider_2.jpg",
            "https://dev.booknow.asia/images/home_slider_3.jpg",
            "https://dev.booknow.asia/images/home_slider_4.jpg",
            "https://dev.booknow.asia/images/home_slider_5.jpg",
            "https://dev.booknow.asia/images/home_slider_6.jpg"
        )

        val slideModels = ArrayList(imageList)

        //Slider Image
        CustomSliderImageClass.initImageSlideViewPager(20, mBind.viewPagerImageSlider, sliderHandler, sliderRunnable, ArrayList(), slideModels, object : AdapterImageSlider.OnClickItemListener{
            override fun onClickCallBack(sliderItems: ImageListModel) {}
        })
    }

    private val sliderRunnable = Runnable { mBind.viewPagerImageSlider.currentItem = mBind.viewPagerImageSlider.currentItem + 1 }

    override fun onPause() {
        super.onPause()
        if (sliderHandler != null)  sliderHandler.removeCallbacks(sliderRunnable)
    }

    override fun onResume() {
        super.onResume()
        if (sliderHandler != null) sliderHandler.postDelayed(sliderRunnable, 3000)
    }


}