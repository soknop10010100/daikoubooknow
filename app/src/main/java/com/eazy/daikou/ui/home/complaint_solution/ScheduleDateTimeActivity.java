package com.eazy.daikou.ui.home.complaint_solution;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.eazy.daikou.R;
import com.eazy.daikou.base.BaseActivity;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class ScheduleDateTimeActivity extends BaseActivity implements View.OnClickListener {

    private TextView dateTv;
    private TextView timeTv;
    private String dateFormat = "";
    private String timeFormat = "";
    private String type = "";

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule_date_time);

        if (getIntent().hasExtra("complaint_date") && getIntent() != null){
            type = getIntent().getStringExtra("complaint_date");
            if (type.equalsIgnoreCase("complaint")){
                findViewById(R.id.descriptionLayout).setVisibility(View.VISIBLE);
            } else if (type.equalsIgnoreCase("complaint_from_guest")){
                TextView txtTitle = findViewById(R.id.txtTitle);
                txtTitle.setText(R.string.respond_and_solution);
                findViewById(R.id.layoutDateTime).setVisibility(View.GONE);
                findViewById(R.id.descriptionLayout).setVisibility(View.VISIBLE);
            }

        }

        dateTv = findViewById(R.id.date);
        timeTv = findViewById(R.id.time);
        TextView btnCancel = findViewById(R.id.btnCancel);
        TextView btnOk =findViewById(R.id.btnOk);

        dateTv.setOnClickListener(this);
        timeTv.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
        btnOk.setOnClickListener(this);
    }

    private void CalendarDialogPicker(String type){
        final Calendar calendar = Calendar.getInstance();
        if (type.equalsIgnoreCase("date")){
            int yy = calendar.get(Calendar.YEAR);
            int mm = calendar.get(Calendar.MONTH);
            int dd = calendar.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog datePicker = new DatePickerDialog(this, (view, year, monthOfYear, dayOfMonth) -> {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                SimpleDateFormat df = new SimpleDateFormat("dd-MMMM-yyyy" , Locale.getDefault());
                dateTv.setText(df.format(calendar.getTime()));

                //Format to Give to server
                SimpleDateFormat dateFormatToServer = new SimpleDateFormat("yyyy-MM-dd" , Locale.getDefault());
                dateFormat = dateFormatToServer.format(calendar.getTime());
            }, yy, mm, dd);
            datePicker.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            datePicker.show();

        } else if (type.equalsIgnoreCase("time")){
            int hour = calendar.get(Calendar.HOUR_OF_DAY);
            int minute = calendar.get(Calendar.MINUTE);
            TimePickerDialog mTimePicker = new TimePickerDialog(ScheduleDateTimeActivity.this,  (timePicker, selectedHour, selectedMinute) -> {
                calendar.set(Calendar.HOUR_OF_DAY,selectedHour);
                calendar.set(Calendar.MINUTE,selectedMinute);

                SimpleDateFormat tf = new SimpleDateFormat("h:mm a" , Locale.getDefault());
                timeTv.setText(tf.format(calendar.getTime()));

                //Format to Give to server
                SimpleDateFormat timeFormatToServer = new SimpleDateFormat("kk:mm" , Locale.getDefault());
                timeFormat = timeFormatToServer.format(calendar.getTime());
            }, hour, minute, true);
            mTimePicker.show();
        }

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.date){
            CalendarDialogPicker("date");
        } else if (view.getId() == R.id.time){
            CalendarDialogPicker("time");
        } else if (view.getId() == R.id.btnCancel){
            finish();
        } else if (view.getId() == R.id.btnOk){
            EditText description = findViewById(R.id.description);
            if (type.equalsIgnoreCase("complaint") && (dateTv.getText().toString().equalsIgnoreCase("") || timeTv.getText().toString().equalsIgnoreCase("") || description.getText().toString().equalsIgnoreCase(""))){
                Toast.makeText(ScheduleDateTimeActivity.this, getResources().getText(R.string.please_file_the_all_fields), Toast.LENGTH_SHORT).show();
            } else if (type.equalsIgnoreCase("complaint_from_guest") && description.getText().toString().equalsIgnoreCase("")) {
                Toast.makeText(ScheduleDateTimeActivity.this, getResources().getText(R.string.please_file_the_all_fields), Toast.LENGTH_SHORT).show();
            } else {
                Intent intent = getIntent();
                intent.putExtra("re_date", dateFormat);
                intent.putExtra("re_time", timeFormat);
                intent.putExtra("description", description.getText().toString());
                setResult(RESULT_OK, intent);
                finish();
            }
        }
    }
}