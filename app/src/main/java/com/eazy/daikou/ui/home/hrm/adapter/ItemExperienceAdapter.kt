package com.eazy.daikou.ui.home.hrm.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.model.hr.Experience

class ItemExperienceAdapter(private val listName: List<Experience>) : RecyclerView.Adapter<ItemExperienceAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.layout_item_experience_adapter, parent, false)
        return ViewHolder(v)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data: Experience = listName[position]

        holder.companyNameTv.text = data.name
        holder.departmentTv.text = data.department
        holder.positionTv.text = data.position
        holder.startDateTv.text = data.start_date
        holder.leaveDateTv.text = data.leave_date
        holder.lastSalaryTv.text = data.last_salary
        holder.salaryTextTv.text = "$"+" "+ data.salary
        holder.lastSalaryTv.text = "$"+" "+ data.last_salary
    }

    override fun getItemCount(): Int {
        return listName.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var companyNameTv : TextView = view.findViewById(R.id.companyName)
        var departmentTv : TextView = view.findViewById(R.id.department)
        var positionTv : TextView = view.findViewById(R.id.position)
        var startDateTv : TextView = view.findViewById(R.id.startDate)
        var leaveDateTv : TextView = view.findViewById(R.id.leaveDate)
        var salaryTextTv : TextView = view.findViewById(R.id.salaryText)
        var lastSalaryTv : TextView = view.findViewById(R.id.lastSalary)

    }

    interface ItemClickOnListener{
        fun onItemClick(experience: Experience)
    }

}