package com.eazy.daikou.ui.home.facility_booking.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R

class ActivityLogBookingAdapter : RecyclerView.Adapter<ActivityLogBookingAdapter.ItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        return ItemViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.my_booking_item_model, parent, false))
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.cardViewActivityLog.visibility = View.VISIBLE
        holder.cardViewRing2.visibility = View.GONE
    }

    override fun getItemCount(): Int {
        return 10
    }

    class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val cardViewActivityLog: CardView = itemView.findViewById(R.id.cardViewActivityLog)
        val cardViewRing2: CardView = itemView.findViewById(R.id.cardViewRing2)
    }
}