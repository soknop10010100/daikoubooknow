package com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.activity

import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.request_data.request.service_property_ws.cleaning_service.AcceptAndRescheduleWs
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.my_property.service_property_employee.CleaningService
import com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.adapter.ServiceTermAdapter
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class RescheduleToCustomerActivity : BaseActivity() {

    private lateinit var descriptionEdt :EditText
    private  var timeTv : TextView? = null
    private lateinit var serviceTermAdapter : ServiceTermAdapter
    private lateinit var btnOk : TextView
    private lateinit var recyclerView : RecyclerView
    private lateinit var btnBack :TextView
    private lateinit var linearStartDate : LinearLayout
    private lateinit var linearEndDate : LinearLayout
    private lateinit var dateStartTv : TextView
    private lateinit var dateEndTv : TextView
    private lateinit var startDateCalendar: CalendarView
    private lateinit var endDateCalendar: CalendarView
    private lateinit var recycleViewDaySchedule : RecyclerView
    private lateinit var cleaningService : CleaningService

    private var listTime: ArrayList<String> = ArrayList()

    private var timeTotal = ""
    private var startDate= ""
    private var endDate = ""
    private var userId = ""
    private var propertyId = ""
    private var cleaningId = ""
    private var timeStart = 0
    private var timeEnd = 0
    private var monStart = 0
    private var yearStart = 0
    private var dayStart = 0
    private var yearEnd = 0
    private var monEnd = 0
    private var dayEnd = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_assign_service_to_cleaning)

        initView()

        initData()

        initAction()

    }

    private fun initView(){

        timeTv = findViewById(R.id.time_re)
        btnOk = findViewById(R.id.btnOk)
        descriptionEdt = findViewById(R.id.description)
        recyclerView = findViewById(R.id.list_re_schedule)

        startDateCalendar = findViewById(R.id.select_start_date)
        endDateCalendar = findViewById(R.id.select_date_end)

        linearStartDate = findViewById(R.id.start_date_linear)
        linearEndDate = findViewById(R.id.end_date_linear)

        recycleViewDaySchedule = findViewById(R.id.list_day_re_schedule)

        dateStartTv = findViewById(R.id.date_start)
        dateEndTv = findViewById(R.id.date_end)
        btnBack = findViewById(R.id.btn_back)
    }

    private fun initData(){

        if (intent.hasExtra("user_id")){
            userId = intent.getStringExtra("user_id") as String
        }
        if(intent.hasExtra("property_id")){
            propertyId = intent.getStringExtra("property_id") as String
        }
        if(intent.hasExtra("cleaning_id")){
            cleaningId = intent.getStringExtra("cleaning_id") as String
        }

        if(intent.hasExtra("time_start")){
            timeStart = intent.getIntExtra("time_start",-1)
        }
        if(intent.hasExtra("time_end")){
            timeEnd = intent.getIntExtra("time_end",-1)
        }

        if(intent.hasExtra("service")){
            cleaningService = intent.getSerializableExtra("service") as CleaningService
        }

        listTime.add(Utils.covertTimes(timeStart))
        timeTotal = Utils.covertTimes(timeStart)

        var i = 30
        while (i < timeEnd) {
            if (timeStart + 30 <= timeEnd) {
                timeStart += 30
                i+=30
                listTime.add(Utils.covertTimes(timeStart))
            } else {
                break
            }
        }

        if (cleaningService.availableDay!=null){
            addTapDay()
        }


        addTapTime()

        val date = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(Date())
        val c = Calendar.getInstance();
        yearStart = c.get(Calendar.YEAR)
        monStart = c.get(Calendar.MONTH + 1)
        dayStart = c.get(Calendar.DAY_OF_MONTH)
        yearEnd = c.get(Calendar.YEAR)
        monEnd = c.get(Calendar.MONTH + 1)
        dayEnd = c.get(Calendar.DAY_OF_MONTH)
        startDate = date
        endDate = date

        dateStartTv.text = startDate
        dateEndTv.text = endDate
        onClickDate("start")

    }

    private fun initAction(){

        btnBack.setOnClickListener { finish() }

        linearStartDate.setOnClickListener {
            onClickDate("start")
        }
        linearEndDate.setOnClickListener {
            onClickDate("end")
        }

        startDateCalendar.setOnDateChangeListener { _, year, month, day ->
            val date = year.toString() + "-" + (month + 1).toString() + "-" + day.toString()
            monStart = month + 1
            yearStart = year
            dayStart = day
            startDate = date
            dateStartTv.text = startDate

        }
        endDateCalendar.setOnDateChangeListener { _, year, month, day ->
            val date = year.toString() + "-" + (month + 1).toString() + "-" + day.toString()
            yearEnd = year
            monEnd = month + 1
            dayEnd = day
            endDate = date
            dateEndTv.text = endDate
        }

        btnOk.setOnClickListener{

            when {
                dateEndTv.text.toString() == "" -> {
                    Toast.makeText(this, resources.getText(R.string.please_file_the_all_fields), Toast.LENGTH_SHORT).show()
                }
                dateStartTv.text.toString() == "" -> {
                    Toast.makeText(this, resources.getText(R.string.please_file_the_all_fields), Toast.LENGTH_SHORT).show()
                }
                else -> {
                    reschedule("reschedule")
                }
            }
        }
    }

    private fun addTapDay(){
        val layout = GridLayoutManager(this,4, RecyclerView.VERTICAL,false)
        recycleViewDaySchedule.layoutManager = layout
        val type: String = if(cleaningService.serviceTerm == "time"){
            "select"
        } else {
            "all"
        }
        val serviceTermAdapter = ServiceTermAdapter(cleaningService.availableDay,type,0,this,  itemClickTap)
        recycleViewDaySchedule.adapter = serviceTermAdapter
    }

    private fun addTapTime(){
        val layout = GridLayoutManager(this,4, RecyclerView.VERTICAL,false)
        recyclerView.layoutManager = layout
        serviceTermAdapter = ServiceTermAdapter(listTime,"serviceTime",0,this,  itemClickTap)
        recyclerView.adapter = serviceTermAdapter
    }

    private val itemClickTap = ServiceTermAdapter.ItemClickOnService { listString, _, _, _ ->
        timeTotal = listString
    }

    private fun reschedule(status:String){
        btnOk.isEnabled = false
        val hashMap: HashMap<String,Any> = HashMap()
        hashMap["property_id"] = propertyId
        hashMap["cleaning_service_history_list_id"] = cleaningId
        hashMap["user_id"] = userId
        hashMap["status_update"] = status
        hashMap["start_date"] = startDate
        hashMap["end_date"] = endDate
        hashMap["start_time"] = timeTotal
        hashMap["reason"] = descriptionEdt.text.toString()

        AcceptAndRescheduleWs().acceptAndReschedule(this,hashMap,rescheduleObject)
    }

    private val rescheduleObject = object : AcceptAndRescheduleWs.OnLoadRequestAcceptCallBack{
        override fun onLoadSuccessFull(message: String?) {
            Toast.makeText(this@RescheduleToCustomerActivity,message,Toast.LENGTH_SHORT).show()
            val intent = intent
            setResult(RESULT_OK, intent)
            finish()

        }

        override fun onLoadFail(message: String?) {
            btnOk.isEnabled = true
            Toast.makeText(this@RescheduleToCustomerActivity,message,Toast.LENGTH_SHORT).show()
        }

    }
    private fun onClickDate(types:String){
        if (types == "start") {
            dateStartTv.setTextColor(ContextCompat.getColor(this, R.color.white))
            linearStartDate.background = ResourcesCompat.getDrawable(resources, R.drawable.custom_card_view_color_menu, null)
            dateEndTv.setTextColor(Color.GRAY)
            linearEndDate.background = null
            startDateCalendar.visibility = View.VISIBLE
            endDateCalendar.visibility = View.GONE
        } else{
            dateEndTv.setTextColor(ContextCompat.getColor(this, R.color.white))
            linearEndDate.background = ResourcesCompat.getDrawable(resources, R.drawable.custom_card_view_color_menu, null)
            dateStartTv.setTextColor(Color.GRAY)
            linearStartDate.background = null
            startDateCalendar.visibility = View.GONE
            endDateCalendar.visibility = View.VISIBLE
        }
    }


}