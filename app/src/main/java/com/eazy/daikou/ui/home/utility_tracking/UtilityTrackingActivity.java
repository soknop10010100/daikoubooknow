package com.eazy.daikou.ui.home.utility_tracking;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.eazy.daikou.R;
import com.eazy.daikou.base.BaseActivity;
import com.eazy.daikou.request_data.static_data.TypeTrackingWaterElectricity;

public class UtilityTrackingActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_utility_tracking);

        TextView title = findViewById(R.id.titleToolbar);
        ImageView btnBack = findViewById(R.id.iconBack);
        RelativeLayout btnWaterTrack = findViewById(R.id.btnWaterTrack);
        RelativeLayout btnWaterCommon = findViewById(R.id.btnWaterCommon);
        RelativeLayout btnPPWC = findViewById(R.id.btnPPWC);
        RelativeLayout btnElectricityTrack = findViewById(R.id.btnElectricityTrack);
        RelativeLayout btnElectricityCommon = findViewById(R.id.btnElectricityCommon);
        RelativeLayout btnEDC = findViewById(R.id.btnEDC);
        LinearLayout linearLayoutWater = findViewById(R.id.linearWater);
        LinearLayout linearLayoutElectricity = findViewById(R.id.linearElectricity);

        btnBack.setOnClickListener(v -> finish());

        if (getIntent().hasExtra("action")){
            String action = getIntent().getStringExtra("action");
            if (action.equals("water")){
                linearLayoutWater.setVisibility(View.VISIBLE);
                linearLayoutElectricity.setVisibility(View.GONE);
                title.setText(getString(R.string.water_record));
            } else {
                linearLayoutWater.setVisibility(View.GONE);
                linearLayoutElectricity.setVisibility(View.VISIBLE);
                title.setText(getString(R.string.electricity_record));
            }
        }


        Intent intent = new Intent(UtilityTrackingActivity.this, ListWaterElectricityActivity.class);
        intent.putExtra("user_permission", getIntent().getSerializableExtra("user_permission"));

        btnWaterTrack.setOnClickListener(v -> {
            intent.putExtra("action", TypeTrackingWaterElectricity.WATER_TRACK);
            startActivity(intent);
        });

        btnWaterCommon.setOnClickListener(v -> {
            intent.putExtra("action",TypeTrackingWaterElectricity.WATER_COMMON);
            startActivity(intent);
        });

        btnPPWC.setOnClickListener(v -> {
            intent.putExtra("action",TypeTrackingWaterElectricity.WATER_PPWSA);
            startActivity(intent);
        });

        btnElectricityTrack.setOnClickListener(v ->{
            intent.putExtra("action",TypeTrackingWaterElectricity.ELECTRICITY_TRACK);
            startActivity(intent);
        });

        btnElectricityCommon.setOnClickListener(v ->{
            intent.putExtra("action",TypeTrackingWaterElectricity.ELECTRICITY_COMMON);
            startActivity(intent);
        });

        btnEDC.setOnClickListener(v ->{
            intent.putExtra("action",TypeTrackingWaterElectricity.ELECTRICITY_EDC);
            startActivity(intent);
        });
    }
}
