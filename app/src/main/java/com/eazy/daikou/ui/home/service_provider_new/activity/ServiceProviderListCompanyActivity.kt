package com.eazy.daikou.ui.home.service_provider_new.activity

import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.model.service_provider.SubCategory
import com.eazy.daikou.ui.home.service_provider_new.adapter_service_provider.CompanyListAdapter

class ServiceProviderListCompanyActivity : BaseActivity() {

    private lateinit var recyclerListCompany: RecyclerView
    private var companyList: ArrayList<SubCategory> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_service_provicer_listcomapany)

        initView()

        initAction()
    }

    private fun initView(){

        recyclerListCompany = findViewById(R.id.recyclerListCompany)

    }

    private fun initAction(){

        val linearManager = LinearLayoutManager(this)
        recyclerListCompany.layoutManager = linearManager
        val adapterCompanyList =  CompanyListAdapter(this)
        recyclerListCompany.adapter = adapterCompanyList
    }

    private val callBackListCompany : CompanyListAdapter.CallBackListCompany = object : CompanyListAdapter.CallBackListCompany{
        override fun clickCompanyListener(company: SubCategory) {
            val intent = Intent(this@ServiceProviderListCompanyActivity, ServiceProviderCompanyDetailActivity::class.java)
            startActivity(intent)
        }

    }
}