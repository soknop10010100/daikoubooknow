package com.eazy.daikou.ui.home.utility_tracking;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.eazy.daikou.R;
import com.eazy.daikou.base.BaseActivity;
import com.eazy.daikou.helper.Utils;
import com.eazy.daikou.model.floor_plan.FloorSpinnerModel;
import com.eazy.daikou.ui.home.utility_tracking.adapter.SpinnerFloorAdapter;

import java.util.ArrayList;
import java.util.List;

public class SelectFloorActivity extends BaseActivity {

    private List<FloorSpinnerModel> list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_floor);

        RecyclerView recyclerView = findViewById(R.id.selectProject);
        Utils.customOnToolbar(this, getResources().getString(R.string.select_floor), this::finish);

        if (getIntent() != null && getIntent().hasExtra("floorList")){
            list = (List<FloorSpinnerModel>)getIntent().getSerializableExtra("floorList");
            String floorName = getIntent().getStringExtra("floorName");
            if (list.size() > 0){
                for (FloorSpinnerModel floorSpinnerModel : list){
                    floorSpinnerModel.setClick(floorSpinnerModel.getFloorName().equalsIgnoreCase(floorName));
                }
            }
        }


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);

        SpinnerFloorAdapter selectProjectAdapter = new SpinnerFloorAdapter(list, clickCallBack);
        recyclerView.setAdapter(selectProjectAdapter);

    }

    private final SpinnerFloorAdapter.ClickCallBack clickCallBack = floorSpinnerModel -> {
        for (FloorSpinnerModel floorSpinner : list){
            floorSpinner.setClick(floorSpinnerModel.getFloorName().equalsIgnoreCase(floorSpinner.getFloorName()));
        }
        Intent intent = getIntent();
        intent.putExtra("floor_name",floorSpinnerModel.getFloorName());
        setResult(RESULT_OK, intent);
        finish();
    };


}