package com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.model.my_property.service_property_employee.AllService
import com.eazy.daikou.model.my_property.service_provider.ServiceTypeCleaning


class ServiceCleaningNameAdapter(
    private val context: Context,
    private val serviceList: List<ServiceTypeCleaning>,
    private val itemClickSeeMore : ItemClickSeeMore,
    private val itemClickService : ServiceCleaningAdapter.ItemClickOnServiceName
) : RecyclerView.Adapter<ServiceCleaningNameAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.layout_service_name, parent, false)
        return ViewHolder(v)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val service: ServiceTypeCleaning = serviceList[position]
        if (service != null) {
            if (service.allServices != null) {
                holder.nameCleaning.visibility = View.VISIBLE
                holder.nameCleaning.text = service.serviceType
                val layoutManager = GridLayoutManager(context, 1, RecyclerView.HORIZONTAL, false)
                holder.recyclerView.layoutManager = layoutManager
                val adapter = ServiceCleaningAdapter(service.allServices, itemClick)
                holder.recyclerView.adapter = adapter
            } else {
                holder.nameCleaning.visibility = View.GONE
                holder.recyclerView.visibility = View.GONE
            }

            if (service.allServices.size == 0) {
                holder.linearLayout.visibility = View.GONE
            } else if (service.allServices.size < 5) {
                holder.btnSee.visibility = View.GONE
            }

            holder.btnSee.setOnClickListener {
                itemClickSeeMore.onItemClickSeeMore(service)
            }
        }


    }
    private var itemClick = object : ServiceCleaningAdapter.ItemClickOnServiceName{
        override fun onClick(service: AllService, stat: Int) {
            itemClickService.onClick(service,stat)
        }

    }

    override fun getItemCount(): Int {
        return serviceList.size
    }
    interface ItemClickSeeMore{
        fun onItemClickSeeMore(service: ServiceTypeCleaning)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var nameCleaning : TextView = view.findViewById(R.id.name_service)
        var recyclerView :RecyclerView = view.findViewById(R.id.list_service)
        var btnSee : TextView = view.findViewById(R.id.text_see_more)
        var linearLayout : LinearLayout = view.findViewById(R.id.view)

    }

}