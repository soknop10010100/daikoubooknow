package com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.adapter

import android.content.Context
import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.my_property.cleaning.PropertyServiceProviderListModel
import kotlin.collections.ArrayList


class HistoryCleaningAdapter(private val context: Context, private val serviceList: ArrayList<PropertyServiceProviderListModel>, private val itemClickService: ItemClickOnService):
    RecyclerView.Adapter<HistoryCleaningAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(context).inflate(R.layout.layout_property_service_provider, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
       val service : PropertyServiceProviderListModel = serviceList[position]
        if(service!=null){
            holder.orderNoTv.text = service.reg_no
            holder.unitNoTv.text = service.unit_no
            holder.serviceNameTv.text = service.sv_name
            holder.startDateTv.text = Utils.formatDateFromString("yyyy-MM-dd", "dd, MMM yyyy", service.reg_start_date)
            holder.endDateTv.text =  Utils.formatDateFromString("yyyy-MM-dd", "dd, MMM yyyy", service.reg_end_date)
            holder.timeStartTv.text =  Utils.formatDateTime(service.reg_start_time,"hh:mm:ss","hh:mm a")
            holder.timeEndTv.text =  Utils.formatDateTime(service.reg_end_time,"hh:mm:ss","hh:mm a")
            holder.taxTv.text = service.reg_tax_amount
            holder.totalPriceTv.text = service.reg_total_amount

            if (service.reg_status != null) {
                holder.statusTv.backgroundTintList = setColorStatusOnWorkOrder(context, service.reg_status)
                holder.style.backgroundTintList = setColorStatusOnWorkOrder(context, service.reg_status)
                holder.statusTv.text = getValueFromKeyStatus(context)[service.reg_status]
            } else {
                holder.statusTv.backgroundTintList = setColorStatusOnWorkOrder(context, "")
                holder.statusTv.text = "- - -"
            }
            holder.itemView.setOnClickListener {
                itemClickService.onClick(service)
            }
        }
    }

    override fun getItemCount(): Int {
        return serviceList.size
    }
    interface ItemClickOnService {
        fun onClick(serviceProperty: PropertyServiceProviderListModel)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var orderNoTv : TextView = view.findViewById(R.id.orderNoTv)
        var unitNoTv : TextView = view.findViewById(R.id.unitNoTv)
        var serviceNameTv : TextView = view.findViewById(R.id.serviceNameTv)
        var startDateTv : TextView = view.findViewById(R.id.startDateTv)
        var endDateTv : TextView = view.findViewById(R.id.endDateTv)
        var timeStartTv : TextView = view.findViewById(R.id.startTimeTv)
        var timeEndTv: TextView = view.findViewById(R.id.endTimeTv)
        var taxTv : TextView = view.findViewById(R.id.taxTv)
        var totalPriceTv : TextView = view.findViewById(R.id.totalPriceTv)
        var statusTv : TextView = view.findViewById(R.id.statusTv)
        var style : TextView = view.findViewById(R.id.style)
    }

    private fun setColorStatusOnWorkOrder(context: Context?, valueStatus: String?): ColorStateList {
        return when (valueStatus) {
            "new" -> ColorStateList.valueOf(ContextCompat.getColor(context!!, R.color.blue))
            "pending" -> ColorStateList.valueOf(ContextCompat.getColor(context!!, R.color.yellow))
            "accepted" -> ColorStateList.valueOf(ContextCompat.getColor(context!!, R.color.green))
            "cancelled" -> ColorStateList.valueOf(ContextCompat.getColor(context!!, R.color.red))
            "confirmed" -> ColorStateList.valueOf(ContextCompat.getColor(context!!, R.color.appBarColorOld))
            else -> ColorStateList.valueOf(ContextCompat.getColor(context!!, R.color.appBarColorOld))
        }
    }

    private fun getValueFromKeyStatus(context: Context?): HashMap<String, String> {
        val hashMap = HashMap<String, String>()
        hashMap["new"] = Utils.getText(context, R.string.new_)
        hashMap["pending"] = Utils.getText(context, R.string.pending)
        hashMap["accepted"] = Utils.getText(context, R.string.accepted)
        hashMap["cancelled"] = Utils.getText(context, R.string.cancel)
        hashMap["confirmed"] = Utils.getText(context, R.string.confirm)
        return hashMap
    }

    fun clear() {
        val size: Int = serviceList.size
        if (size > 0) {
            for (i in 0 until size) {
                serviceList.removeAt(0)
            }
            notifyItemRangeRemoved(0, size)
        }
    }
}