package com.eazy.daikou.ui.home.my_property;

import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.VideoView;

import com.eazy.daikou.R;
import com.eazy.daikou.base.BaseActivity;

public class VideoPlayerActivity extends BaseActivity {

    private String getFilePath;
    private VideoView videoUpload;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_player);

        if (getIntent() != null && getIntent().hasExtra("video")){
            getFilePath = getIntent().getStringExtra("video");
        }

        findViewById(R.id.backBtn).setOnClickListener(view -> finish());
        videoUpload = findViewById(R.id.videoPlayer);
        ProgressBar progressBar = findViewById(R.id.progress);

        MediaController mediaController = new MediaController(this);
        mediaController.setAnchorView(videoUpload);
        mediaController.setMediaPlayer(videoUpload);
        videoUpload.setMediaController(mediaController);
        Uri uri = Uri.parse(getFilePath);
        videoUpload.setVideoURI(uri);
        videoUpload.requestFocus();
        videoUpload.setOnPreparedListener(mp -> {
            mp.setLooping(false);
            progressBar.setVisibility(View.GONE);
            videoUpload.start();
        });

    }

//    @Override
//    public void onStart() {
//        super.onStart();
//        videoUpload.setVideoURI(Uri.parse(getFilePath));
//        videoUpload.start();
//    }

//    @Override
//    public void onResume() {
//        super.onResume();
//        videoUpload.setVideoURI(Uri.parse(getFilePath));
//        videoUpload.resume();
//    }

    @Override
    public void onPause() {
        super.onPause();
        videoUpload.pause();
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}