package com.eazy.daikou.ui.home.book_now.booking_hotel.adapter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.booking_hotel.LocationHotelModel

class SelectLocationAdapter(private val selectedItem: String, private val list : List<LocationHotelModel>, private val onClickListener : OnClickCallBackLister) : RecyclerView.Adapter<SelectLocationAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.custom_view_text_view_layout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item : LocationHotelModel = list[position]
        if (item != null){
            if (item.location_name != null){
                holder.itemNameTv.text = String.format("%s %s %s %s", item.location_name, "(", item.total_hotels, ")")
            } else {
                holder.itemNameTv.text = ". . ."
            }
            holder.itemNameTv.setTextColor(Utils.getColor(holder.itemNameTv.context, R.color.black))
            holder.checkClick.setColorFilter(Utils.getColor(holder.checkClick.context, R.color.book_now_secondary))
            holder.checkClick.visibility = if(selectedItem == item.location_id) View.VISIBLE else View.INVISIBLE
            holder.line.visibility = if((list.size - 1) == position) View.GONE else View.VISIBLE
            holder.line.setBackgroundColor(Utils.getColor(holder.itemNameTv.context, R.color.light_grey))

            holder.itemView.setOnClickListener { onClickListener.onClickCallBack(item) }

            holder.mainLayout.setBackgroundColor(Color.WHITE)
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val itemNameTv : TextView = itemView.findViewById(R.id.item_name)
        val checkClick : ImageView = itemView.findViewById(R.id.checkClick)
        val line : View = itemView.findViewById(R.id.line)
        val mainLayout : LinearLayout = itemView.findViewById(R.id.mainLayout)
    }

    interface OnClickCallBackLister{
        fun onClickCallBack(value : LocationHotelModel)
    }
}