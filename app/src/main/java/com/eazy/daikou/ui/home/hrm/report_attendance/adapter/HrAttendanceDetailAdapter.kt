package com.eazy.daikou.ui.home.hrm.report_attendance.adapter

import android.content.Context
import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.helper.DateUtil
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.hr.LogListAttendance
import org.jsoup.helper.DataUtil

class HrAttendanceDetailAdapter(private val context : Context, private val hrAttendanceDetailModelList : ArrayList<LogListAttendance>) :
    RecyclerView.Adapter<HrAttendanceDetailAdapter.ItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val rowView = LayoutInflater.from(parent.context).inflate(R.layout.report_attendance_detail_content, parent, false)
        return ItemViewHolder(rowView)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val itemAttendanceDetail : LogListAttendance = hrAttendanceDetailModelList[position]
        if (itemAttendanceDetail != null){
            holder.dateCheckInTv.text = if (itemAttendanceDetail.attendance_date != null) Utils.formatDateFromString("yyyy-MM-dd", "dd-MMM-yyyy",itemAttendanceDetail.attendance_date) else "- - -"
            holder.timeCheckInTv.text = DateUtil.formatTimeServer(itemAttendanceDetail.check_datetime)
            holder.checkByTv.text = itemAttendanceDetail.check_by ?: ". . ."

            holder.statusTv.text = Utils.statusAttendanceDetail()[itemAttendanceDetail.status]
            if(itemAttendanceDetail.status != null) {
                when {
                    itemAttendanceDetail.status.equals("check_out", ignoreCase = true) -> {
                        holder.statusTv.backgroundTintList =
                            ColorStateList.valueOf(ContextCompat.getColor(context, R.color.red))
                        holder.styleImg.backgroundTintList =
                            ColorStateList.valueOf(ContextCompat.getColor(context, R.color.red))
                    }
                    itemAttendanceDetail.status.equals("check_in", ignoreCase = true) -> {
                        holder.statusTv.backgroundTintList =
                            ColorStateList.valueOf(ContextCompat.getColor(context, R.color.greenSea))
                        holder.styleImg.backgroundTintList =
                            ColorStateList.valueOf(ContextCompat.getColor(context, R.color.greenSea))

                    }
                    itemAttendanceDetail.status.equals("break_in", ignoreCase = true) -> {
                        holder.statusTv.backgroundTintList =
                            ColorStateList.valueOf(ContextCompat.getColor(context, R.color.color_button))
                        holder.styleImg.backgroundTintList =
                            ColorStateList.valueOf(ContextCompat.getColor(context, R.color.color_button))

                    }
                    itemAttendanceDetail.status.equals("break_out", ignoreCase = true) -> {
                        holder.statusTv.backgroundTintList =
                            ColorStateList.valueOf(ContextCompat.getColor(context, R.color.yellow))
                        holder.styleImg.backgroundTintList =
                            ColorStateList.valueOf(ContextCompat.getColor(context, R.color.yellow))
                    }
                }
            }

            // Set Status late or early
            if (itemAttendanceDetail.action_log_status != null){
                if (itemAttendanceDetail.action_log_status!!.status != null){

                    var timeStatusVal = ". . ."
                    if (itemAttendanceDetail.action_log_status!!.total_hour != null && itemAttendanceDetail.action_log_status!!.total_minute != null)
                        timeStatusVal = itemAttendanceDetail.action_log_status!!.total_hour + " h " +
                                itemAttendanceDetail.action_log_status!!.total_minute + " min"

                    holder.timeStatus.text = timeStatusVal

                    when (itemAttendanceDetail.action_log_status!!.status) {
                        "late" -> {
                            holder.statusLate.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(context, R.color.red))
                            holder.typeStatus.text = String.format("%s ( %s )", context.resources.getString(R.string.duration), context.resources.getString(R.string.late))
                            holder.statusLate.text = context.resources.getString(R.string.late)
                            holder.timeStatus.setTextColor(Utils.getColor(context, R.color.red))
                        }
                        "normal" -> {
                            holder.statusLate.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(context, R.color.green))
                            holder.typeStatus.text = String.format("%s ( %s )", context.resources.getString(R.string.duration), context.resources.getString(R.string.normal))
                            holder.statusLate.text = context.resources.getString(R.string.normal)
                            holder.timeStatus.setTextColor(Utils.getColor(context, R.color.green))
                        }
                        else -> {
                            holder.statusLate.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(context, R.color.greenSea))
                            holder.typeStatus.text = String.format("%s ( %s )", context.resources.getString(R.string.duration), context.resources.getString(R.string.early))
                            holder.statusLate.text = context.resources.getString(R.string.early)
                            holder.timeStatus.setTextColor(Utils.getColor(context, R.color.greenSea))
                        }
                    }
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return hrAttendanceDetailModelList.size
    }

    class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var dateCheckInTv : TextView = view.findViewById(R.id.dateCheckInTv)
        var timeCheckInTv : TextView = view.findViewById(R.id.timeCheckInTv)
        var statusTv : TextView = view.findViewById(R.id.status)
        var styleImg : TextView = view.findViewById(R.id.style)
        var checkByTv : TextView = view.findViewById(R.id.checkByTv)
        var statusLate : TextView = view.findViewById(R.id.statusLate)
        var timeStatus : TextView = view.findViewById(R.id.timeStatus)
        var typeStatus : TextView = view.findViewById(R.id.typeStatus)
    }
}