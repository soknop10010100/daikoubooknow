package com.eazy.daikou.ui.home.front_desk;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.eazy.daikou.R;
import com.eazy.daikou.base.BaseFragment;
import com.eazy.daikou.helper.Utils;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class FrontDeskImageListFragment extends BaseFragment {

    private String photo;

    public static FrontDeskImageListFragment newInstance(int position,String photo,List<String> listPhoto) {
        Bundle args = new Bundle();
        args.putInt("position",position);
        args.putString("photo",photo);
        args.putSerializable("listPhoto", (Serializable) listPhoto);
        FrontDeskImageListFragment fragment = new FrontDeskImageListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @org.jetbrains.annotations.Nullable
    @Override
    public View onCreateView(@NonNull @NotNull LayoutInflater inflater, @Nullable @org.jetbrains.annotations.Nullable ViewGroup container, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.layout_view_pager_image, container, false);
        ImageView image = root.findViewById(R.id.image_view);

        if(getArguments() != null&& getArguments().containsKey("photo")){
            photo = getArguments().getString("photo");
        }

        Glide.with(image).load(photo).into(image);

        image.setOnClickListener(v -> Utils.openZoomImage(mActivity, photo, new ArrayList<String>()));

        return root;
    }
}
