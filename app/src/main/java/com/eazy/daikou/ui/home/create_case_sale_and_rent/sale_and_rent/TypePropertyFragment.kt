package com.eazy.daikou.ui.home.create_case_sale_and_rent.sale_and_rent

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.model.buy_sell.ItemSaleAndRentModel
import com.eazy.daikou.ui.home.create_case_sale_and_rent.adapter.ItemTypeAdapter
import com.google.android.material.bottomsheet.BottomSheetDialogFragment


class TypePropertyFragment :  BottomSheetDialogFragment() {

    private lateinit var recyclerViewItem: RecyclerView
    private lateinit var itemTypeAdapter: ItemTypeAdapter
    private var mContext : Context? = null
    private lateinit var callBackItemDataListener: CallBackItemDataListener

    private  var listItem : ArrayList<ItemSaleAndRentModel> = ArrayList()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.BottomSheetDialogTheme)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_type_property, container, false)

        initView(view)

        initAction()

        return view
    }
    private fun initView(view: View){
        recyclerViewItem = view.findViewById(R.id.recyclerTypeProperty)
        view.findViewById<TextView>(R.id.btnClose).setOnClickListener { dismiss() }


    }
    private fun initAction(){

        listItem.add(ItemSaleAndRentModel("land", "Land"))
        listItem.add(ItemSaleAndRentModel("house", "House"))
        listItem.add(ItemSaleAndRentModel("building", "Building"))

        val linearManage = LinearLayoutManager(mContext)
        recyclerViewItem.layoutManager = linearManage
        itemTypeAdapter = ItemTypeAdapter(mContext!!,listItem, callBackItem)
        recyclerViewItem.adapter = itemTypeAdapter
    }


    private val callBackItem : ItemTypeAdapter.CallBackItemClick = object : ItemTypeAdapter.CallBackItemClick{
        override fun clickItemCallListener(data: ItemSaleAndRentModel) {
            callBackItemDataListener.callBackDate(data)
            dismiss()
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
    }

    fun initListener(callBackItemDataListener: CallBackItemDataListener){
        this.callBackItemDataListener = callBackItemDataListener
    }

    interface CallBackItemDataListener{
        fun callBackDate(itemSaleAndRentModel: ItemSaleAndRentModel)
    }
}