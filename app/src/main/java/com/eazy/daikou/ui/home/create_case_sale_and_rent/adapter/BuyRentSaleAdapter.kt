package com.eazy.daikou.ui.home.create_case_sale_and_rent.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.buy_sell.SaleAndRentProperties

class BuyRentSaleAdapter(private val list : ArrayList<SaleAndRentProperties>, private val callBackItem : CallBackClickItemListener): RecyclerView.Adapter<BuyRentSaleAdapter.ItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        return ItemViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_buy_sale_rent_model, parent, false))
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val properties : SaleAndRentProperties = list[position]
        if (properties != null){
            Utils.setValueOnText(holder.property_name, properties.title)
            Utils.setValueOnText(holder.addressTv, properties.address)
            Utils.setValueOnText(holder.price, properties.price)
            Utils.setValueOnText(holder.totalAreaTv, properties.size)
            if (properties.total_bedrooms != null){
                holder.bedRoomTv.text = properties.total_bedrooms
            } else {
                holder.bedRoomTv.text = "0"
            }

            if (properties.total_bathrooms != null){
                holder.bathRoomTv.text = properties.total_bathrooms
            } else {
                holder.bathRoomTv.text = "0"
            }

            Glide.with(holder.imageView.context).load(if (properties.image !=  null) properties.image else R.color.color_gray_item).into(holder.imageView)

            holder.itemView.setOnClickListener { callBackItem.onClickItemProperty(properties)  }
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var property_name: TextView = itemView.findViewById(R.id.property_name)
        var addressTv: TextView = itemView.findViewById(R.id.addressTv)
        var price: TextView = itemView.findViewById(R.id.price)
        var imageView : ImageView = itemView.findViewById(R.id.image)
        var bedRoomTv : TextView = itemView.findViewById(R.id.bedRoomTv)
        var bathRoomTv : TextView = itemView.findViewById(R.id.bathRoomTv)
        var totalAreaTv : TextView = itemView.findViewById(R.id.sofaTv)
    }

    fun clear(){
        val size = list.size
        if (size > 0) {
            for (i in 0 until size) {
                list.removeAt(0)
            }
            notifyItemRangeRemoved(0, size)
        }
    }

    interface CallBackClickItemListener{
        fun onClickItemProperty(saleAndRentProperties : SaleAndRentProperties)
    }
}