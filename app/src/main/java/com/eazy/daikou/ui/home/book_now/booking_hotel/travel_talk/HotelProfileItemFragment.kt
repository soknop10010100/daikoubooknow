package com.eazy.daikou.ui.home.book_now.booking_hotel.travel_talk

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AbsListView
import android.widget.ProgressBar
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseFragment
import com.eazy.daikou.request_data.request.book_now_ws.BookingHotelWS
import com.eazy.daikou.request_data.request.book_now_ws.RequestHashMapData
import com.eazy.daikou.helper.AppAlertCusDialog
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.helper.Utils.shareLinkMultipleOption
import com.eazy.daikou.model.booking_hotel.HotelTravelTalkModel
import com.eazy.daikou.model.booking_hotel.TravelTalkReplyDetail
import com.eazy.daikou.ui.home.book_now.booking_hotel.adapter.HotelTravelTalkAdapter

class HotelProfileItemFragment : BaseFragment() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var progressBar: ProgressBar
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var travelTalkAdapter: HotelTravelTalkAdapter
    private var travelTalkList : ArrayList<HotelTravelTalkModel> = ArrayList()
    private var currentItem = 0
    private var total : Int = 0
    private var scrollDown : Int = 0
    private var currentPage : Int = 1
    private var size : Int = 10
    private var isScrolling = false
    private var userId = ""
    private var categoryType = ""
    private var backUpHotelArticleModel: HotelTravelTalkModel? = null

    companion object {
        @JvmStatic
        fun newInstance(category: String, userId : String) =
            HotelProfileItemFragment().apply {
                arguments = Bundle().apply {
                    putString("type_category", category)
                    putString("user_id", userId)
                }
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            categoryType = it.getString("type_category").toString()
            userId = it.getString("user_id").toString()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_hotel_profile_item, container, false)

        initView(view)

        initAction()

        return view
    }

    private fun initView(view: View){
        recyclerView = view.findViewById(R.id.recyclerView)
        progressBar = view.findViewById(R.id.progressItem)
    }

    private fun initAction(){
        linearLayoutManager = LinearLayoutManager(mActivity)
        recyclerView.layoutManager = linearLayoutManager

        travelTalkAdapter = HotelTravelTalkAdapter(mActivity, travelTalkList, onClickItemListener)
        recyclerView.adapter = travelTalkAdapter

        requestServiceWs()

        onScrollInfoRecycle()

    }

    private fun requestServiceWs(){
        BookingHotelWS().getLikeSharePostByUserWs(mActivity, currentPage, 10, userId, categoryType, onCallBackListener)
    }

    private val onCallBackListener = object : BookingHotelWS.OnCallBackTravelTalkListener{
        @SuppressLint("NotifyDataSetChanged")
        override fun onSuccessTravelTalkList(travelTalkModelList: ArrayList<HotelTravelTalkModel>) {
            progressBar.visibility = View.GONE
            travelTalkList.addAll(travelTalkModelList)

            travelTalkAdapter.notifyDataSetChanged()

            Utils.validateViewNoItemFound(mActivity, recyclerView, travelTalkList.size <= 0)
        }

        override fun onSuccessTravelTalkReplyDetail(travelTalkModelList: TravelTalkReplyDetail) {}

        override fun onCreatePostSuccess(message: String) {}

        override fun onFailed(message: String) {
            progressBar.visibility = View.GONE
            Utils.customToastMsgError(mActivity, message, false)
        }

    }

    private fun onScrollInfoRecycle() {
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true
                }
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                currentItem = linearLayoutManager.childCount
                total = linearLayoutManager.itemCount
                scrollDown = linearLayoutManager.findFirstVisibleItemPosition()
                if (dy > 0) {
                    if (isScrolling && currentItem + scrollDown == total) {
                        if (total == size) {
                            isScrolling = false
                            currentPage++
                            progressBar.visibility = View.VISIBLE
                            recyclerView.scrollToPosition(travelTalkList.size - 1)

                            requestServiceWs()
                            size += 10
                        } else {
                            progressBar.visibility = View.GONE
                        }
                    }
                }
            }
        })
    }

    private val onClickItemListener = object : HotelTravelTalkAdapter.OnClickItemListener{
        @SuppressLint("NotifyDataSetChanged")
        override fun onClickListener(hotelArticleModel: HotelTravelTalkModel, action: String) {
            backUpHotelArticleModel = hotelArticleModel
            when(action){
                "like_action"->{
                    if (AppAlertCusDialog.isSuccessLoggedIn(mActivity)) {
                        // On request service
                        val hashMap = RequestHashMapData.requestDataLike(userId, "thread", hotelArticleModel.id!!, if (hotelArticleModel.is_user_liked) "unlike" else "like")
                        RequestHashMapData.requestServiceLikeUnLike(mActivity,progressBar, "do_like", hashMap, object : RequestHashMapData.Companion.OnSuccessEditStatusListener{
                            override fun onSuccess(msg: String) {}
                        })

                        // On set like / unlike item
                        var numLike = hotelArticleModel.created_by!!.total_likes!!.toInt()  // Profile user
                        var numLikeItem = hotelArticleModel.total_likes!!.toInt()  // Item Like
                        for (item in travelTalkList) {
                            if (item.id == hotelArticleModel.id) {
                                if (item.is_user_liked){
                                    item.is_user_liked = false
                                    numLike -= 1
                                    numLikeItem -= 1
                                } else {
                                    item.is_user_liked = true
                                    numLike += 1
                                    numLikeItem += 1
                                }
                                item.created_by!!.total_likes = numLike.toString()  // Profile user
                                item.total_likes = numLikeItem.toString()  // Item Like
                                break
                            }
                        }
                        travelTalkAdapter.notifyDataSetChanged()
                    }
                }
                "share_action" ->{
                    shareLinkMultipleOption(mActivity, hotelArticleModel.share_link!!, "")
                }
                "view_profile_action" ->{
                    val intent = Intent(mActivity, HotelTravelTalkViewProfileActivity::class.java)
                    intent.putExtra("hotel_travel_talk", hotelArticleModel)
                    startActivity(intent)
                }
                "comment_action", "detail_action"->{
                    startDetailTravelActivity(hotelArticleModel.id!!, action)
                }
            }
        }

    }

    private fun startDetailTravelActivity(id : String, action : String){
        val intent = Intent(mActivity, HotelTravelTalkDetailActivity::class.java)
        intent.putExtra("id", id)
        intent.putExtra("action", action)
        resultLauncher.launch(intent)
    }

    @SuppressLint("NotifyDataSetChanged")
    private val resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
        if (result.resultCode == Activity.RESULT_OK) {
            if (result.data != null){
                if (result.data!!.hasExtra("hotel_travel_item")){
                    backUpHotelArticleModel = result.data!!.getSerializableExtra("hotel_travel_item") as HotelTravelTalkModel
                    for (i in 0 until travelTalkList.size) {
                        if(travelTalkList[i].id == backUpHotelArticleModel!!.id){
                            travelTalkList[i] = backUpHotelArticleModel as HotelTravelTalkModel
                            break
                        }
                    }
                    travelTalkAdapter.notifyDataSetChanged()
                }
            }
        } else {
            Utils.logDebug("jeeeeeeeeeeeeeeeeee", "Failed")
        }
    }
}