package com.eazy.daikou.ui.home.inspection_work_order.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.model.inspection.InspectorModel

class ItemInspectorAdapter (private val inspectorList : ArrayList<InspectorModel>, private val onClickCallBack : OnClickCallBackListener):
    RecyclerView.Adapter<ItemInspectorAdapter.ItemViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        return ItemViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_inspector_selected, parent, false))
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val inspectorModel = inspectorList[position]
        if (inspectorModel != null){
            holder.title.text = inspectorModel.userName
            holder.title.setOnClickListener {
                onClickCallBack.onClickCallBack(inspectorModel)
            }
        }
    }

    override fun getItemCount(): Int {
        return inspectorList.size
    }

    class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var title : TextView = itemView.findViewById(R.id.text_category)
    }

    interface OnClickCallBackListener {
        fun onClickCallBack(inspectorModel: InspectorModel)
    }
}