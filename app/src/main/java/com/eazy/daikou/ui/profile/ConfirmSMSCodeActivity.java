package com.eazy.daikou.ui.profile;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener;
import com.eazy.daikou.helper.CheckIsAppActive;
import com.eazy.daikou.ui.SampleCanvasClassActivity;
import com.poovam.pinedittextfield.SquarePinField;
import com.eazy.daikou.R;
import com.eazy.daikou.request_data.request.profile_ws.PasswordWs;
import com.eazy.daikou.base.BaseActivity;
import com.eazy.daikou.helper.Utils;

import java.util.HashMap;
import java.util.Locale;
import java.util.Objects;

public class ConfirmSMSCodeActivity extends BaseActivity {

    private TextView countTime;
    private SquarePinField pinCode;
    private LinearLayout btnSubmitCode;
    private ProgressBar progressBar;
    private String getEmailPhone = "", sendType = "", countryCode = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        int layout = (CheckIsAppActive.Companion.is_daikou_active()) ? R.layout.activity_confirm_sms_code : R.layout.hotel_confirm_sms_code;
        setContentView(layout);

        initView();

        initData();

        initAction();
    }

    private void initView() {
        countTime = findViewById(R.id.tv_resend_code);
        pinCode = findViewById(R.id.pin_code);

        btnSubmitCode = findViewById(R.id.btn_submit_verify_code);
        progressBar = findViewById(R.id.progressItem);
        progressBar.setVisibility(View.GONE);

        if (CheckIsAppActive.Companion.is_daikou_active()){
            findViewById(R.id.backBtnVerify).setOnClickListener(view -> onBackPressed());
        } else {
            RelativeLayout fragmentLayout = findViewById(R.id.fragmentLayout);
            fragmentLayout.addView(new SampleCanvasClassActivity( false,this, "#0b4891"));
            Utils.customOnToolbar(this, getResources().getString(R.string.verify_code), this::finish);
        }
    }

    private void initData() {
        if (getIntent() != null && getIntent().hasExtra("VER_PHONE_EMAIL")) {
            getEmailPhone = getIntent().getStringExtra("VER_PHONE_EMAIL");
        }
        if (getIntent() != null && getIntent().hasExtra("send_type")) {
            sendType = getIntent().getStringExtra("send_type");
        }
        if (getIntent() != null && getIntent().hasExtra("phone_code")) {
            countryCode = getIntent().getStringExtra("phone_code");
        }
    }

    private void initAction(){
        countTime();

        setPinCode();

        clickOnResendCode();

        btnSubmitCode.setOnClickListener(new CustomSetOnClickViewListener(view -> {
            if (pinCode.getText() != null && !pinCode.getText().toString().isEmpty() && pinCode.length() == 6) {
                verifyCode();
            } else {
                Utils.customToastMsgError(ConfirmSMSCodeActivity.this, getResources().getString(R.string.invalid_code_verify), false);
            }
        }));
    }

    private void setPinCode() {
        pinCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() == 6) {
                    Utils.hideSoftKeyboard(findViewById(R.id.pin_code));
                    verifyCode();
                }
            }
        });
    }

    private void countTime() {
        new CountDownTimer(180000, 1000) {
            public void onTick(long millisUntilFinished) {
                long min = (millisUntilFinished / 60000) % 60;
                long sec = (millisUntilFinished / 1000) % 60;
                countTime.setText(String.format(Locale.US, "%s %s : %s", getResources().getString(R.string.available_in), min, sec));
                countTime.setEnabled(false);
            }

            public void onFinish() {
                countTime.setText(R.string.resend_code);
                countTime.setEnabled(true);
            }
        }.start();
    }

    private void verifyCode() {
        progressBar.setVisibility(View.VISIBLE);
        HashMap<String, Object> getVerify = new HashMap<>();
        String getPinCode = Objects.requireNonNull(pinCode.getText()).toString();
        getVerify.put("reset_password_type", "verify");
        getVerify.put("verify_code", getPinCode);
        new PasswordWs().verifyCode(ConfirmSMSCodeActivity.this, getVerify, getPinCode, verifyCallBack);
    }

    private final PasswordWs.VerifyCodeListener verifyCallBack = new PasswordWs.VerifyCodeListener() {
        @Override
        public void onSuccess(String getPinCode) {
            progressBar.setVisibility(View.GONE);
            Intent intent = getIntent();
            intent.putExtra("pin_code", getPinCode);
            Utils.customToastMsgError(ConfirmSMSCodeActivity.this, getResources().getString(R.string.verify_code_email_have_sent_to_user_successfully), true);
            setResult(RESULT_OK, intent);
            finish();
        }

        @Override
        public void onWrong() {
            progressBar.setVisibility(View.GONE);
            Utils.customToastMsgError(ConfirmSMSCodeActivity.this, getResources().getString(R.string.invalid_code_verify), false);
            Objects.requireNonNull(pinCode.getText()).clear();
        }

        @Override
        public void onFailed(String error) {
            progressBar.setVisibility(View.GONE);
            Utils.customToastMsgError(ConfirmSMSCodeActivity.this, error, false);
            Objects.requireNonNull(pinCode.getText()).clear();
        }
    };

    private void clickOnResendCode() {
        countTime.setEnabled(true);
        countTime.setOnClickListener(v -> {
            v.setEnabled(false);
            v.postDelayed(() -> v.setEnabled(true), 100);
            countTime();
            forgetPassParam(getEmailPhone);
        });
    }

    public void forgetPassParam(String phone_email) {
        HashMap<String, Object> userForgotPass = new HashMap<>();
        userForgotPass.put("send_type", sendType);
        if (sendType.equalsIgnoreCase("phone_number")) {
            userForgotPass.put("phone_number", phone_email);
            userForgotPass.put("phone_code", countryCode);
        } else {
            userForgotPass.put("email", phone_email);
        }
        progressBar.setVisibility(View.VISIBLE);
        new PasswordWs().forgotPassword(ConfirmSMSCodeActivity.this, userForgotPass, phone_email, callBackListener);
    }

    private final PasswordWs.ForgotPasswordCallBackListener callBackListener = new PasswordWs.ForgotPasswordCallBackListener() {
        @Override
        public void onSuccess(String phone_email) {
            progressBar.setVisibility(View.GONE);
        }

        @Override
        public void onSuccessFalse(String message) {
            progressBar.setVisibility(View.GONE);
            Utils.customToastMsgError(ConfirmSMSCodeActivity.this, message, false);
        }

        @Override
        public void onWrong(String msg) {
            progressBar.setVisibility(View.GONE);
            Utils.customToastMsgError(ConfirmSMSCodeActivity.this, msg, false);
        }

        @Override
        public void onFailed(String mess) {
            progressBar.setVisibility(View.GONE);
            Utils.customToastMsgError(ConfirmSMSCodeActivity.this, mess, false);
        }
    };

}