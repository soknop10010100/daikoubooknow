package com.eazy.daikou.ui.home.facility_booking.adapter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import java.util.*

class AvailableDayAdapter : RecyclerView.Adapter<AvailableDayAdapter.ItemViewHolder>{

    private var action : String
    private var freeTypeList : List<String>
    private lateinit var onClickCallBackListener : OnClickCallBackListener

    constructor(action : String, freeTypeList : List<String>) : super() {
        this.action = action
        this.freeTypeList = freeTypeList
    }

    constructor(action : String, freeTypeList : List<String>, onClickCallBackListener : OnClickCallBackListener) : super() {
        this.action = action
        this.freeTypeList = freeTypeList
        this.onClickCallBackListener = onClickCallBackListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.suggestion_complaint_model, parent, false)
        return ItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val feeTypeModel : String = freeTypeList[position]
        if (feeTypeModel != null) {
            if (action == "available_day_service_property"){
                holder.layoutAvailableDay.visibility = View.VISIBLE
                holder.linearLayout.visibility = View.GONE

                val day = feeTypeModel.replace(" ", "\n")
                holder.date.text = day
                val params = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                holder.mainLayout.layoutParams = params

                holder.itemView.setOnClickListener { onClickCallBackListener.onClickBack(feeTypeModel) }
            } else {
                holder.nameCategoryTv.setTextColor(if (action == "detail") Color.WHITE else Color.BLACK)
                holder.nameCategoryTv.text = String.format(Locale.US, "%s", feeTypeModel)
                holder.linearLayoutView.visibility = View.VISIBLE
                holder.linearLayout.visibility = View.GONE
                holder.linearLayoutView.setBackgroundResource(R.drawable.bg_swap_app_appbar_strok)
            }
        }

    }

    override fun getItemCount(): Int {
        return freeTypeList.size
    }

    class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val mainLayout: LinearLayout = itemView.findViewById(R.id.mainLayout)
        val linearLayout: LinearLayout = itemView.findViewById(R.id.linear_card)
        val linearLayoutView: LinearLayout = itemView.findViewById(R.id.linear_card_wrap_content)
        val layoutAvailableDay : LinearLayout = itemView.findViewById(R.id.layoutAvailableDay)
        val date : TextView = itemView.findViewById(R.id.date)
        val nameCategoryTv: TextView = itemView.findViewById(R.id.text_category_wrap_content)
    }

    interface OnClickCallBackListener{
        fun onClickBack(data : String)
    }
}