package com.eazy.daikou.ui.home.book_now.booking_account

import android.annotation.SuppressLint
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.os.Bundle
import android.text.InputType
import android.text.TextUtils
import android.text.method.PasswordTransformationMethod
import android.util.Log
import android.view.View
import android.widget.*
import androidx.annotation.Nullable
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import com.eazy.daikou.BuildConfig
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.Constant
import com.eazy.daikou.repository_ws.Constant.TAG
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.request_data.request.book_now_ws.BookingHotelAccountWS
import com.eazy.daikou.request_data.request.profile_ws.LoginWs.OnRecives
import com.eazy.daikou.fcm.MyFirebaseMessagingService
import com.eazy.daikou.helper.AppAlertCusDialog
import com.eazy.daikou.helper.InternetConnection
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.profile.User
import com.eazy.daikou.ui.SampleCanvasClassActivity
import com.eazy.daikou.ui.home.book_now.booking_hotel.HotelBookingMainActivity
import com.eazy.daikou.ui.profile.ForgotPasswordActivity
import com.facebook.*
import com.facebook.internal.ImageRequest
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.facebook.login.widget.LoginButton
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.auth.api.signin.GoogleSignInResult
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.FirebaseApp
import com.google.firebase.messaging.FirebaseMessaging
import com.rilixtech.widget.countrycodepicker.CountryCodePicker
import org.json.JSONException
import java.net.URL
import java.util.*


class LoginBookNowActivity : BaseActivity(), GoogleApiClient.OnConnectionFailedListener{

    private lateinit var fragmentLayout : RelativeLayout
    private lateinit var progressBar: ProgressBar
    private lateinit var edUsername: EditText
    private  lateinit var edPassword:EditText
    private lateinit var userSaveData: UserSessionManagement
    private lateinit var countryCodePicker: CountryCodePicker
    private lateinit var emailTv: TextView
    private lateinit var phoneTv:TextView
    private var dataUser : HashMap<String, Any> = HashMap()

    private var emailPhone: String = ""
    private  var password: String =""
    private var countryCode: String = ""
    private var action: String = ""

    //Login with Facebook, Google
    private lateinit var loginButton: LoginButton
    private var callbackManager: CallbackManager? = null
    private var googleSignInClient: GoogleSignInClient? = null
    private var googleApiClient: GoogleApiClient? = null
    private val RC_SIGN_IN = 154

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_book_now)
        window.statusBarColor = ContextCompat.getColor(this, R.color.book_now_secondary)

        getExistingFirebaseToken()

        initView()

        initData()

        initAction()

    }

    private fun getExistingFirebaseToken() {
        FirebaseApp.initializeApp(this)
        FirebaseMessaging.getInstance().token.addOnCompleteListener(this@LoginBookNowActivity,
            OnCompleteListener { task: Task<String?> ->
                if (!task.isSuccessful) {
                    return@OnCompleteListener
                }
                if (task.result != null) {
                    val token = task.result
                    Utils.logDebug("gettokenstken", token)
                    Utils.saveString(
                        Constant.FIREBASE_TOKEN,
                        token,
                        this@LoginBookNowActivity
                    )
                }
            })
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun initView() {
        edUsername = findViewById(R.id.ed_mail)
        edPassword = findViewById(R.id.ed_password)
        emailTv = findViewById(R.id.emailTv)
        phoneTv = findViewById(R.id.phoneTv)
        progressBar = findViewById(R.id.progressItem)
        progressBar.visibility = View.GONE
        countryCodePicker = findViewById(R.id.ccp)
        val versionCodeTv = findViewById<TextView>(R.id.version_code)
        versionCodeTv.text = String.format(Locale.US, "%s %s (%s)", resources.getString(R.string.version), BuildConfig.VERSION_NAME, BuildConfig.VERSION_CODE)

        fragmentLayout = findViewById(R.id.fragmentLayout)
        fragmentLayout.addView(SampleCanvasClassActivity(false, this, "#0b4891"))
        fragmentLayout.addView(SampleCanvasClassActivity(true, this, "#0b4891"))

        loginButton = findViewById(R.id.login_button)
    }

    private fun initData() {
        userSaveData = UserSessionManagement(this)
        if (intent != null && intent.hasExtra("action")) {
            action = intent.getStringExtra("action").toString()
        }
    }

    private fun initAction() {
        edPassword.inputType = InputType.TYPE_TEXT_VARIATION_PASSWORD
        edPassword.transformationMethod = PasswordTransformationMethod.getInstance()

        // Get Country Code
        countryCodePicker.setDefaultCountryUsingNameCodeAndApply(country_code)
        countryCode = countryCodePicker.fullNumber
        countryCodePicker.setOnCountryChangeListener { countryCode = countryCodePicker.fullNumber }
        edUsername.hint = resources.getString(R.string.your_email_or_phone_number)
        edPassword.hint = resources.getString(R.string.your_password)

        val phoneLayout = findViewById<CardView>(R.id.phoneLayout)
        val emailLayout = findViewById<CardView>(R.id.emailLayout)

        emailLayout.setOnClickListener(CustomSetOnClickViewListener { onPhoneEmailClick(phoneLayout, emailLayout, "email") })

        phoneTv.setOnClickListener(CustomSetOnClickViewListener { onPhoneEmailClick(phoneLayout, emailLayout, "phone") })

        // Forgot Password
        findViewById<View>(R.id.tv_forgot_password).setOnClickListener(CustomSetOnClickViewListener {
            startActivity(Intent(this, ForgotPasswordActivity::class.java))
        })

        // Sign Up
        findViewById<View>(R.id.signUpTv).setOnClickListener(CustomSetOnClickViewListener {
            startActivity(Intent(this@LoginBookNowActivity, SignUpBookNowActivity::class.java))
        })

        doActionLogin()
    }

    private fun doActionLogin(){
        //Login with email, phone
        findViewById<View>(R.id.card_login).setOnClickListener(CustomSetOnClickViewListener {
            if (InternetConnection.checkInternetConnectionActivity(applicationContext)) {
                emailPhone = edUsername.text.toString()
                password = edPassword.text.toString()
                if (TextUtils.isEmpty(emailPhone) || TextUtils.isEmpty(password)) {
                    AppAlertCusDialog().showDialog(this@LoginBookNowActivity, resources.getString(R.string.please_file_the_all_fields))
                } else {
                    emailPhone = if (!emailPhone.contains("@")) {   //Login email always contain @
                        countryCode + edUsername.text.toString().trim { it <= ' ' }
                    } else {
                        edUsername.text.toString().trim { it <= ' ' }
                    }
                    dataUser["username"] = emailPhone
                    dataUser["password"] = password
                    progressBar.visibility = View.VISIBLE
                    BookingHotelAccountWS().loginWs(this@LoginBookNowActivity, dataUser, onReceiveCallBack)
                }
            } else {
                AppAlertCusDialog().showDialog(this@LoginBookNowActivity, resources.getString(R.string.please_check_your_internet_connection))
            }
        })

        //Sign in facebook
        loginButton.setPermissions(listOf("public_profile", "email").toString())
        callbackManager = CallbackManager.Factory.create()
        loginButton.registerCallback(callbackManager!!, object : FacebookCallback<LoginResult> {
            override fun onSuccess(result: LoginResult) {
                val graphRequest = GraphRequest.newMeRequest(result.accessToken) { `object`, respone ->
                    try {
                        val accountFacebook: HashMap<String, Any?> = HashMap()
                        if (`object` != null){
                            if (`object`.has("id")) accountFacebook["social_media_id"] = `object`.getString("id")
                            if (`object`.has("first_name")) accountFacebook["first_name"] = `object`.getString("first_name")
                            if (`object`.has("last_name")) accountFacebook["last_name"] = `object`.getString("last_name")
                            accountFacebook["social_media_type"] = "facebook"
                            if (Profile.getCurrentProfile() != null) {
                                val dimensionPixelSize: Int = this@LoginBookNowActivity.resources.getDimensionPixelSize(com.facebook.R.dimen.com_facebook_profilepictureview_preset_size_large)
                                val profileURL: String = ImageRequest.getProfilePictureUri(Profile.getCurrentProfile()!!.id, 400, 400).toString()
                                val profile = Profile.getCurrentProfile()?.getProfilePictureUri(dimensionPixelSize, dimensionPixelSize).toString()
                                val profileProfile = Profile.getCurrentProfile()!!.getProfilePictureUri(400, 400).toString()
                                Utils.logDebug("testgetprofileitem", Profile.getCurrentProfile()?.getProfilePictureUri(dimensionPixelSize, dimensionPixelSize).toString())

                                accountFacebook["image_profile"] = profileURL
                            }

                            progressBar.visibility = View.VISIBLE
                            BookingHotelAccountWS().loginFBGoogleWs(this@LoginBookNowActivity, "facebook",accountFacebook, onRegisterFbGoogleListener)
                        }
                    } catch (e: JSONException) {
                        Utils.customAlertDialogConfirm(this@LoginBookNowActivity, resources.getString(R.string.something_went_wrong)){}
                    }
                }
                val parameter = Bundle()
                parameter.putString("fields", "id,first_name,last_name,picture.type(large)")
                graphRequest.parameters = parameter
                graphRequest.executeAsync()
            }

            override fun onCancel() {}
            override fun onError(error: FacebookException) {
                Utils.customAlertDialogConfirm(this@LoginBookNowActivity, error.message){}
            }
        })

        //Login with google
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .build()
        googleApiClient = GoogleApiClient.Builder(this)
            .enableAutoManage(this , this)
            .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
            .build()
        googleSignInClient = GoogleSignIn.getClient(this, gso)
    }

    fun getFacebookProfilePicture(userID: String): Bitmap? {
        val imageURL = URL("https://graph.facebook.com/$userID/picture?type=large")
        return BitmapFactory.decodeStream(imageURL.openConnection().getInputStream())
    }

    private fun onPhoneEmailClick(phoneLayout : CardView, emailLayout : CardView, type: String) {
        if (type.equals("email", ignoreCase = true)) {
            emailTv.setTextColor(Color.WHITE)
            phoneTv.setTextColor(Color.GRAY)
            emailLayout.backgroundTintList =
                ColorStateList.valueOf(ContextCompat.getColor(this, R.color.appBarColorOld))
            phoneLayout.backgroundTintList =
                ColorStateList.valueOf(ContextCompat.getColor(this, R.color.whiteSmoke))
            countryCodePicker.visibility = View.GONE
            edUsername.text.clear()
            edUsername.hint = ""
            edUsername.hint = resources.getString(R.string.your_email_or_phone_number)
            edUsername.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_email_profile, 0, 0, 0)
            edUsername.inputType = InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
        } else {
            phoneTv.setTextColor(Color.WHITE)
            emailTv.setTextColor(Color.GRAY)
            emailLayout.backgroundTintList =
                ColorStateList.valueOf(ContextCompat.getColor(this, R.color.whiteSmoke))
            phoneLayout.backgroundTintList =
                ColorStateList.valueOf(ContextCompat.getColor(this, R.color.appBarColorOld))
            countryCodePicker.visibility = View.VISIBLE
            edUsername.text.clear()
            edUsername.hint = ""
            edUsername.hint = resources.getString(R.string.enter_your_phone_number)
            edUsername.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
            edUsername.inputType = InputType.TYPE_CLASS_NUMBER
        }
    }

    private val onRegisterFbGoogleListener = object : BookingHotelAccountWS.OnRegisterFbGoogleListener {
        override fun onSuccess(action: String?, user: User?, hashMap : HashMap<String, Any?>) {
            progressBar.visibility = View.GONE

            //Logout facebook and google
            if (action == "google") signOutGoogle()
            Utils.disconnectFromFacebook()

            if (user != null){
                user.name = hashMap["first_name"].toString() + " " + hashMap["last_name"].toString()
                user.firstName = hashMap["first_name"].toString()
                user.lastName = hashMap["last_name"].toString()
                user.lastName = hashMap["last_name"].toString()
                if (hashMap.containsKey("image_profile")){
                    if (hashMap["image_profile"] != null){
                        user.photo = hashMap["image_profile"].toString()
                    }
                }
                if (hashMap.containsKey("email")){
                    if (hashMap["email"] != null){
                        user.email = hashMap["email"].toString()
                    }
                }
                Utils.saveString("token", user.accessToken, this@LoginBookNowActivity)
                userSaveData.saveData(user, true)
            }

            MyFirebaseMessagingService.sendRegistrationToServer(Utils.getString(Constant.FIREBASE_TOKEN, this@LoginBookNowActivity), this@LoginBookNowActivity, "add")

            val intent = Intent(this@LoginBookNowActivity, HotelBookingMainActivity::class.java)
            intent.putExtra("is_hotel_app", true)
            startActivity(intent)
            progressBar.visibility = View.GONE
            finish()
        }

        override fun onFailed(mess: String?) {
            progressBar.visibility = View.GONE
            Utils.customToastMsgError(this@LoginBookNowActivity, mess, false)

            //Logout facebook and google
            signOutGoogle()
            Utils.disconnectFromFacebook()
        }

    }

    private val onReceiveCallBack: OnRecives = object : OnRecives {
        override fun onSuccess(data: User) {
            progressBar.visibility = View.GONE

            val accessToken = data.accessToken
            val userid = data.id
            userSaveData.saveData(data, true)
            userSaveData.saveUserCookie(accessToken, userid)

            UserSessionManagement.saveString("user_type", data.activeUserType, this@LoginBookNowActivity)

            Utils.saveString("token", data.accessToken, this@LoginBookNowActivity)
            Utils.saveString(Constant.FIRST_SCAN_QR, "true", applicationContext)
            MyFirebaseMessagingService.sendRegistrationToServer(Utils.getString(Constant.FIREBASE_TOKEN, this@LoginBookNowActivity), this@LoginBookNowActivity, "add")

            val intent = Intent(this@LoginBookNowActivity, HotelBookingMainActivity::class.java)
            intent.putExtra("is_hotel_app", true)
            startActivity(intent)
            finish()
        }

        override fun onWrong(msg: String) {
            progressBar.visibility = View.GONE
            AppAlertCusDialog().showDialog(this@LoginBookNowActivity, msg)
        }

        override fun onFailed(mess: String) {
            progressBar.visibility = View.GONE
            AppAlertCusDialog().showDialog(this@LoginBookNowActivity, mess)
        }
    }

    private fun signIn() {
        val intent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient!!)
        startActivityForResult(intent, RC_SIGN_IN)
    }

    private fun signOutGoogle() { //when logout, must logout google the same
        googleSignInClient!!.signOut()
            .addOnSuccessListener(this) { Log.d(TAG, "Sign out success") }
            .addOnFailureListener(this) { e -> Log.e(TAG, "Sign out failed", e) }
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, @Nullable data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SIGN_IN) {
            val result = Auth.GoogleSignInApi.getSignInResultFromIntent(data!!)
            if (result != null) {
                handleSignInResult(result)
            } else {
                Utils.customAlertDialogConfirm(this@LoginBookNowActivity, resources.getString(R.string.something_went_wrong)){}
            }
        } else {
            callbackManager!!.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun handleSignInResult(result: GoogleSignInResult) {
        if (result.isSuccess) {
            val account = result.signInAccount
            val accountGoogle: HashMap<String, Any?> = HashMap()
            if (account != null) {
                if (account.id != null) accountGoogle["social_media_id"] = account.id
                if (account.familyName != null) accountGoogle["first_name"] = account.familyName
                if (account.givenName != null) accountGoogle["last_name"] = account.givenName
                if (account.email != null) accountGoogle["email"] = account.email
                accountGoogle["social_media_type"] = "google"
                if (account.photoUrl != null) {
                    accountGoogle["image_profile"] = account.photoUrl.toString()
                }
            }
            progressBar.visibility = View.VISIBLE
            BookingHotelAccountWS().loginFBGoogleWs(this@LoginBookNowActivity, "google", accountGoogle, onRegisterFbGoogleListener)
        }
    }

    // Override method GoogleApiClient.OnConnectionFailedListener
    override fun onConnectionFailed(connectionResult: ConnectionResult) {
        Utils.logDebug(TAG, "OnConnectionFailed: $connectionResult")
    }

    override fun onStop() {
        super.onStop()
        LoginManager.getInstance().logOut()
    }

    fun onClick(v: View) {  //override onClick method from view facebook and google(xml)
        if (v === findViewById<View>(R.id.view_facebook)) {
            Utils.checkEnableView(v)
            loginButton.performClick()
        } else if (v === findViewById<View>(R.id.view_google)) {
            Utils.checkEnableView(v)
            signIn()
        }
    }

    override fun onBackPressed() {
        if (action == "from_hotel_profile"){
            finish()
        } else {
            finishAffinity()
        }
    }
}