package com.eazy.daikou.ui.home.utility_tracking;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.eazy.daikou.R;
import com.eazy.daikou.base.BaseActivity;
import com.eazy.daikou.base.BaseCameraActivity;
import com.eazy.daikou.request_data.request.track_water_ws.TrackWaterElectricityWs;
import com.eazy.daikou.request_data.sql_database.TrackWaterElectricityDatabase;
import com.eazy.daikou.request_data.static_data.TypeTrackingWaterElectricity;
import com.eazy.daikou.helper.InternetConnection;
import com.eazy.daikou.helper.UserSessionManagement;
import com.eazy.daikou.helper.Utils;
import com.eazy.daikou.model.profile.User;
import com.eazy.daikou.model.utillity_tracking.model.W_ETrackingModel;
import com.eazy.daikou.model.utillity_tracking.model.TrackingWaterElectricityModel;
import com.eazy.daikou.model.utillity_tracking.model.UtilNoCommonAreaModel;
import com.eazy.daikou.ui.QRCodeAlertDialog;
import com.google.gson.Gson;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class UtilityUploadDialog extends BaseActivity {

    private TrackWaterElectricityDatabase database;
    private EditText currentEt ,totalEt,lastTrackEt;
    private W_ETrackingModel trackingModel;
    private ProgressBar progressBar;
    private ImageView imageView;
    private UtilNoCommonAreaModel utilNoModel;
    private User user;

    private final int REQUEST_IMAGE = 5321;
    private String action;
    private String purpose;
    private String image;
    private String clickAction = "";
    private String tracking_date = "";
    private boolean isWater = false;
    private boolean isClickSave = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_utility_detail_dialog);

        imageView = findViewById(R.id.image);
        Button btn_cancel = findViewById(R.id.btn_cancel);
        Button btn_save = findViewById(R.id.btn_save);
        currentEt  = findViewById(R.id.currentEt);
        totalEt  = findViewById(R.id.totalEt);
        TextView title = findViewById(R.id.title);
        lastTrackEt  = findViewById(R.id.lastTrackEt);
        TextView txtSymbolWater1 = findViewById(R.id.txtWaterSym1);
        TextView txtSymbolWater2 = findViewById(R.id.txtWaterSym2);
        TextView txtSymbolWater3 = findViewById(R.id.txtWaterSym3);
        TextView txtSymbol1 = findViewById(R.id.txtSym1);
        TextView txtSymbol2 = findViewById(R.id.txtSym2);
        TextView txtSymbol3 = findViewById(R.id.txtSym3);
        ImageView btnImage= findViewById(R.id.btnImage);
        progressBar = findViewById(R.id.progressItem);

        UserSessionManagement sessionManagement = new UserSessionManagement(this);
        user = new Gson().fromJson(sessionManagement.getUserDetail(), User.class);
        database = new TrackWaterElectricityDatabase(getBaseContext());
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String dateToday = formatter.format(new Date());

        //=====================Get Data=========================

        if (getIntent().hasExtra("trackingDate")){
            trackingModel = (W_ETrackingModel) getIntent().getSerializableExtra("trackingDate");
        }

        if (getIntent().hasExtra("tracking_date")){
            tracking_date = getIntent().getStringExtra("tracking_date");
        }

        if (getIntent().hasExtra("utilNoModel")){
            utilNoModel = (UtilNoCommonAreaModel) getIntent().getSerializableExtra("utilNoModel");
            action = getIntent().getStringExtra("action");
            title.setText(utilNoModel.getName());
            switch (action) {
                case "water_track":
                case "water_common":
                case "water_ppwsa":
                    isWater = true;
                    if (utilNoModel.getLastTrackDataWater() != null)
                        lastTrackEt.setText( !utilNoModel.getLastTrackDataWater().getLastCurrentUsage().equals("null") &&
                                             !utilNoModel.getLastTrackDataWater().getLastCurrentUsage().equals("") ?
                                             utilNoModel.getLastTrackDataWater().getLastCurrentUsage() : "0");
                    assert utilNoModel.getLastTrackDataWater() != null;
                    if (utilNoModel.getLastTrackDataWater().getCurrent_usage() != null && !utilNoModel.getLastTrackDataWater().getCurrent_usage().equals(""))
                        currentEt.setText(utilNoModel.getLastTrackDataWater().getCurrent_usage());
                    if (utilNoModel.getLastTrackDataWater().getCurrent_usage() != null && !utilNoModel.getLastTrackDataWater().getCurrent_usage().equals("")) {
                        double currentUsage = Double.parseDouble(utilNoModel.getLastTrackDataWater().getCurrent_usage());
                        double previousUsage = 0.0;
                        if (utilNoModel.getLastTrackDataWater().getLastCurrentUsage() != null && !utilNoModel.getLastTrackDataWater().getLastCurrentUsage().equals("")) {
                            previousUsage = Double.parseDouble(utilNoModel.getLastTrackDataWater().getLastCurrentUsage());
                        }
                        totalEt.setText(String.format("%s", currentUsage - previousUsage));
                    }
                    break;
                case "electricity_track":
                case "electricity_common":
                case "electricity_edc":
                    txtSymbolWater1.setVisibility(View.GONE);
                    txtSymbolWater2.setVisibility(View.GONE);
                    txtSymbolWater3.setVisibility(View.GONE);
                    txtSymbol1.setText("KWH");
                    txtSymbol2.setText("KWH");
                    txtSymbol3.setText("KWH");
                    if (utilNoModel.getLastTrackDataElectricity() != null)
                        lastTrackEt.setText( !utilNoModel.getLastTrackDataElectricity().getLastCurrentUsage().equals("null") &&
                                !utilNoModel.getLastTrackDataElectricity().getLastCurrentUsage().equals("") ?
                                utilNoModel.getLastTrackDataElectricity().getLastCurrentUsage() : "0");

                    assert utilNoModel.getLastTrackDataElectricity() != null;
                    if (utilNoModel.getLastTrackDataElectricity().getCurrent_usage() != null && !utilNoModel.getLastTrackDataElectricity().getCurrent_usage().equals(""))
                        currentEt.setText(utilNoModel.getLastTrackDataElectricity().getCurrent_usage());
                    if (utilNoModel.getLastTrackDataElectricity().getCurrent_usage() != null && !utilNoModel.getLastTrackDataElectricity().getCurrent_usage().equals("")) {
                        double currentUsage = Double.parseDouble(utilNoModel.getLastTrackDataElectricity().getCurrent_usage());
                        double previousUsage = 0.0;
                        if (utilNoModel.getLastTrackDataElectricity().getLastCurrentUsage() != null && !utilNoModel.getLastTrackDataElectricity().getLastCurrentUsage().equals("")){
                            previousUsage = Double.parseDouble(utilNoModel.getLastTrackDataElectricity().getLastCurrentUsage());
                        }

                        totalEt.setText(String.format("%s", currentUsage - previousUsage));
                    }
                    break;
            }
            if (action.equals("water_track") || action.equals("electricity_track")){
                boolean isUnit = true;
            }
        }

        if (getIntent().hasExtra("click_action")){
            clickAction = getIntent().getStringExtra("click_action");
        }

        if (getIntent().hasExtra("purpose")){
            purpose = getIntent().getStringExtra("purpose");

            boolean isAlreadyHaveTrack;
            if (isWater){
                isAlreadyHaveTrack = utilNoModel.getLastTrackDataWater().getIsAlreadyHaveTrack();
            } else {
               isAlreadyHaveTrack = utilNoModel.getLastTrackDataElectricity().getIsAlreadyHaveTrack();
            }

            if (isAlreadyHaveTrack){           //Check If unit is tracked already and we update
                if (purpose.equals("update") || purpose.equals("create")){
                    if (InternetConnection.checkInternetConnection(btn_save)){
                        if (clickAction.equals("warning")){
                            TrackingWaterElectricityModel trackingWaterElectricityModel = database.getFloorDetail(action, utilNoModel.getId());
                            image = trackingWaterElectricityModel.getTrackingImage();
                            if (image != null && !image.equals("")){
                                byte[] decodedString = Base64.decode(image, Base64.DEFAULT);
                                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                                Glide.with(imageView).load(decodedByte).into(imageView);
                            }
                            totalEt.setText(trackingWaterElectricityModel.getTotalUsage());
                            currentEt.setText(trackingWaterElectricityModel.getCurrentUsage());
                            progressBar.setVisibility(View.GONE);
                        } else {
                            getUtilityRecordDetail(utilNoModel);
                        }
                    } else {
                        if (dateToday.equals(tracking_date)){
                            TrackingWaterElectricityModel trackingWaterElectricityModel = database.getFloorDetail(action, utilNoModel.getId());
                            image = trackingWaterElectricityModel.getTrackingImage();
                            if (image != null && !image.equals("")){
                                byte[] decodedString = Base64.decode(image, Base64.DEFAULT);
                                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                                Glide.with(imageView).load(decodedByte).into(imageView);
                            }
                            if (trackingWaterElectricityModel.getTotalUsage() != null){
                                totalEt.setText(trackingWaterElectricityModel.getTotalUsage());
                            }
                            if (trackingWaterElectricityModel.getCurrentUsage() != null) {
                                currentEt.setText(trackingWaterElectricityModel.getCurrentUsage());
                            } else {
                                if (utilNoModel.getLastTrackDataElectricity().getCurrent_usage() != null && !utilNoModel.getLastTrackDataElectricity().getCurrent_usage().equals(""))
                                    currentEt.setText(utilNoModel.getLastTrackDataElectricity().getCurrent_usage());
                            }
                        } else {
                            List<HashMap<String, String>> list = new TrackWaterElectricityDatabase(UtilityUploadDialog.this).getFloorDetailList(action);
                            if (!list.isEmpty()) {
                                for (int i = 0; i < list.size(); i++) {
                                    if (utilNoModel.getId().equals(list.get(i).get("location_id"))) {
                                        TrackingWaterElectricityModel trackingWaterElectricityModel = database.getFloorDetail(action, utilNoModel.getId());
                                        image = trackingWaterElectricityModel.getTrackingImage();
                                        if (image != null && !image.equals("")) {
                                            byte[] decodedString = Base64.decode(image, Base64.DEFAULT);
                                            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                                            Glide.with(imageView).load(decodedByte).into(imageView);
                                        }
                                        if (trackingWaterElectricityModel.getTotalUsage() != null) {
                                            totalEt.setText(trackingWaterElectricityModel.getTotalUsage());
                                        }
                                        if (trackingWaterElectricityModel.getCurrentUsage() != null) {
                                            currentEt.setText(trackingWaterElectricityModel.getCurrentUsage());
                                        } else {
                                            if (utilNoModel.getLastTrackDataElectricity().getCurrent_usage() != null && !utilNoModel.getLastTrackDataElectricity().getCurrent_usage().equals(""))
                                                currentEt.setText(utilNoModel.getLastTrackDataElectricity().getCurrent_usage());
                                        }
                                        break;
                                    } else {
                                        currentEt.setText("");  currentEt.setHint(getResources().getString(R.string.current_usage));
                                        totalEt.setText("");  totalEt.setHint(getResources().getString(R.string.total_usage));
                                        lastTrackEt.setText("0");
                                    }
                                }
                            } else {
                                currentEt.setText("");  currentEt.setHint(getResources().getString(R.string.current_usage));
                                totalEt.setText("");    totalEt.setHint(getResources().getString(R.string.total_usage));
                                lastTrackEt.setText("0");
                            }
                        }
                        progressBar.setVisibility(View.GONE);
                    }
                } else {
                    if (clickAction.equals("warning") || clickAction.equalsIgnoreCase("add")){
                        TrackingWaterElectricityModel trackingWaterElectricityModel = database.getFloorDetail(action, utilNoModel.getId());
                        image = trackingWaterElectricityModel.getTrackingImage();
                        if (image != null && !image.equals("")){
                            byte[] decodedString = Base64.decode(image, Base64.DEFAULT);
                            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                            Glide.with(imageView).load(decodedByte).into(imageView);
                        }
                        totalEt.setText(trackingWaterElectricityModel.getTotalUsage());
                        currentEt.setText(trackingWaterElectricityModel.getCurrentUsage());
                    }
                    progressBar.setVisibility(View.GONE);
                }
            } else {
                if (InternetConnection.checkInternetConnection(btn_save)) {
                    getUtilityRecordDetail(utilNoModel);
                } else {
                    if (dateToday.equals(tracking_date)) {
                        TrackingWaterElectricityModel trackingWaterElectricityModel = database.getFloorDetail(action, utilNoModel.getId());
                        image = trackingWaterElectricityModel.getTrackingImage();
                        if (image != null && !image.equals("")) {
                            byte[] decodedString = Base64.decode(image, Base64.DEFAULT);
                            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                            Glide.with(imageView).load(decodedByte).into(imageView);
                        }
                        totalEt.setText(trackingWaterElectricityModel.getTotalUsage());
                        currentEt.setText(trackingWaterElectricityModel.getCurrentUsage());
                    } else {
                        List<HashMap<String, String>> list = new TrackWaterElectricityDatabase(UtilityUploadDialog.this).getFloorDetailList(action);
                        if (!list.isEmpty()) {
                            for (int i = 0; i < list.size(); i++) {
                                if (utilNoModel.getId().equals(list.get(i).get("location_id"))) {
                                    TrackingWaterElectricityModel trackingWaterElectricityModel = database.getFloorDetail(action, utilNoModel.getId());
                                    image = trackingWaterElectricityModel.getTrackingImage();
                                    if (image != null && !image.equals("")) {
                                        byte[] decodedString = Base64.decode(image, Base64.DEFAULT);
                                        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                                        Glide.with(imageView).load(decodedByte).into(imageView);
                                    }
                                    if (trackingWaterElectricityModel.getTotalUsage() != null) {
                                        totalEt.setText(trackingWaterElectricityModel.getTotalUsage());
                                    }
                                    if (trackingWaterElectricityModel.getCurrentUsage() != null) {
                                        currentEt.setText(trackingWaterElectricityModel.getCurrentUsage());
                                    } else {
                                        if (utilNoModel.getLastTrackDataElectricity().getCurrent_usage() != null && !utilNoModel.getLastTrackDataElectricity().getCurrent_usage().equals(""))
                                            currentEt.setText(utilNoModel.getLastTrackDataElectricity().getCurrent_usage());
                                    }
                                    break;
                                } else {
                                    currentEt.setText("");    currentEt.setHint(getResources().getString(R.string.current_usage));
                                    totalEt.setText("");    totalEt.setHint(getResources().getString(R.string.total_usage));
                                    lastTrackEt.setText("0");
                                }
                            }
                        } else {
                            currentEt.setText("");    currentEt.setHint(getResources().getString(R.string.current_usage));
                            totalEt.setText("");    totalEt.setHint(getResources().getString(R.string.total_usage));
                            lastTrackEt.setText("0");
                        }
                    }
                    progressBar.setVisibility(View.GONE);
                }
            }

        }

        currentEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().equals("") && s.toString().length() < 9){
                    switch (action) {
                        case "water_track":
                        case "water_common":
                        case "water_ppwsa":
                        case "electricity_track":
                        case "electricity_common":
                        case "electricity_edc":
                            Double current = Double.parseDouble(s.toString());
                            Double last = Double.parseDouble(lastTrackEt.getText().toString());
                            totalEt.setText((current - last) + "");
                            break;
                    }
                } else {
                    totalEt.setText(s.toString());
                }
            }
        });

        imageView.setOnClickListener(v -> {
            if (image != null && !image.equals("")){
                QRCodeAlertDialog qrCodeAlertDialog = new QRCodeAlertDialog(UtilityUploadDialog.this,image);
                qrCodeAlertDialog.show();
            }
        });

        btnImage.setOnClickListener(v ->{
            startActivityForResult(new Intent(UtilityUploadDialog.this,BaseCameraActivity.class),REQUEST_IMAGE);
        });

        btn_cancel.setOnClickListener(v -> finish());

        btn_save.setOnClickListener(v -> {
            isClickSave = true;
            progressBar.setVisibility(View.VISIBLE);
            HashMap<String,Object> body = getDataHashMap();
            if (image != null && !image.equals("") && !currentEt.getText().toString().equals("") && !totalEt.getText().toString().equals("")){
                if (InternetConnection.checkInternetConnection(btn_save)){
                    saveUtility(body);
                } else {
                    if (clickAction.equals("warning") || clickAction.equals("add")){
                        body.put("data_type",action);
                        body.put("purpose", purpose);
                        body.put("total_usage",totalEt.getText().toString());
                        database.insertFloorDetail(body);
                        Intent intent = new Intent();
                        intent.putExtra("utilNoModel",utilNoModel);
                        intent.putExtra("tracking_date_upload", tracking_date);
                        setResult(RESULT_OK,intent);
                        finish();
                    } else {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(UtilityUploadDialog.this, getString(R.string.you_can_not_update_without_internet),Toast.LENGTH_SHORT).show();
                    }
                }
            } else {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(UtilityUploadDialog.this,getString(R.string.please_input_all_fields_require),Toast.LENGTH_SHORT).show();
            }


        });
    }

    private HashMap<String, Object> getDataHashMap(){
        HashMap<String,Object> body = new HashMap<>();
        body.put("property_id", user.getActivePropertyIdFk());
        body.put("location_id", Integer.parseInt(utilNoModel.getId()));
        if (purpose.equals("update")){
            body.put("tracking_date",trackingModel.getTrackingDate());
        } else {
            body.put("tracking_date",tracking_date);
        }
        body.put("current_usage",currentEt.getText().toString());
        body.put("utility_type",TypeTrackingWaterElectricity.getTypeOfUtility().get(action));
        body.put("utility_area", TypeTrackingWaterElectricity.getUtilityType().get(action));
        body.put("utility_photo_file", image);
        return body;
    }

    private void saveUtility(HashMap<String,Object> hashMap){
        new TrackWaterElectricityWs().createWaterElectricUtility(UtilityUploadDialog.this,hashMap,onShowingMessageListener);
    }

    private final TrackWaterElectricityWs.OnShowingMessage onShowingMessageListener = new TrackWaterElectricityWs.OnShowingMessage() {
        @Override
        public void onSuccess(String message, String id) {
            progressBar.setVisibility(View.GONE);
            Intent intent = new Intent();
            intent.putExtra("utilNoModel", utilNoModel);
            intent.putExtra("is_click_save", isClickSave);
            setResult(RESULT_OK, intent);
            finish();
            Toast.makeText(UtilityUploadDialog.this,message,Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onFail(String message) {
            progressBar.setVisibility(View.GONE);
            Toast.makeText(UtilityUploadDialog.this,message,Toast.LENGTH_SHORT).show();
        }
    };

    private void getUtilityRecordDetail(UtilNoCommonAreaModel utilNoCommonAreaModel){
        String date_tracking;
        if (purpose.equals("update")){
            date_tracking = trackingModel.getTrackingDate();
        } else {
            date_tracking = tracking_date;
        }
        new TrackWaterElectricityWs().getUtilityRecordDetail(UtilityUploadDialog.this, user.getActivePropertyIdFk(), date_tracking, action, utilNoCommonAreaModel.getId(),new TrackWaterElectricityWs.OnCallBackUtility() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onSuccess(TrackingWaterElectricityModel trackingWaterElectricityModel) {
                progressBar.setVisibility(View.GONE);
                if (trackingWaterElectricityModel.getTrackingImage() != null) {
                    image = Utils.convertUrlToBase64(trackingWaterElectricityModel.getTrackingImage());
                    Glide.with(imageView).load(trackingWaterElectricityModel.getTrackingImage()).into(imageView);
                }
                if (trackingWaterElectricityModel.getPreviousUsage() == null){
                    lastTrackEt.setText("0");
                } else {
                    lastTrackEt.setText(trackingWaterElectricityModel.getPreviousUsage());
                }
                currentEt.setText(trackingWaterElectricityModel.getCurrentUsage());
                if (trackingWaterElectricityModel.getCurrentUsage() != null){
                    totalEt.setText((Double.parseDouble(trackingWaterElectricityModel.getCurrentUsage()) - Double.parseDouble(lastTrackEt.getText().toString())) + "");
                }

            }

            @Override
            public void onFail(String message) {
                if (!message.equals("false")){
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(UtilityUploadDialog.this,message,Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    progressBar.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_IMAGE ){
            assert data != null;
            if(data.hasExtra("image_path")){
                String imagePath = data.getStringExtra("image_path");
                image = imagePath;
                Bitmap loadedBitmap = BaseCameraActivity.convertByteToBitmap(imagePath);
                Glide.with(imageView).load(loadedBitmap).into(imageView);
            }
            if(data.hasExtra("select_image")){
                String imagePath = data.getStringExtra("select_image");
                image = imagePath;
                Bitmap loadedBitmap = BaseCameraActivity.convertByteToBitmap(imagePath);
                Glide.with(imageView).load(loadedBitmap).into(imageView);
            }

        }
    }
}
