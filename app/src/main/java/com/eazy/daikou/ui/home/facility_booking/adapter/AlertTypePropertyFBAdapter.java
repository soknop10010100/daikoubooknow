package com.eazy.daikou.ui.home.facility_booking.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.eazy.daikou.R;
import com.eazy.daikou.model.facility_booking.AlertPropertyBookingModel;
import com.eazy.daikou.model.facility_booking.VenuePropertyBookingModel;

import java.util.List;

public class AlertTypePropertyFBAdapter extends RecyclerView.Adapter<AlertTypePropertyFBAdapter.ViewHolder> {
    private final Context context;
    private List<AlertPropertyBookingModel> alertPropertyBookingModelList;
    private List<VenuePropertyBookingModel> vanueList;
    private final ItemClickBack itemClickBack;
    private boolean isSelectVanue = false;

    public AlertTypePropertyFBAdapter(Context context, List<AlertPropertyBookingModel> alertPropertyBookingModelList, ItemClickBack itemClickBack){
        this.context = context;
        this.alertPropertyBookingModelList = alertPropertyBookingModelList;
        this.itemClickBack = itemClickBack;
    }

    public AlertTypePropertyFBAdapter(Context context, List<VenuePropertyBookingModel> vanueList, boolean isSelectVanue,ItemClickBack itemClickBack){
        this.context = context;
        this.vanueList = vanueList;
        this.isSelectVanue = isSelectVanue;
        this.itemClickBack = itemClickBack;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
      View view = LayoutInflater.from(context).inflate(R.layout.custom_view_text_view_layout,parent,false);
      return  new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (isSelectVanue){
            VenuePropertyBookingModel venuePropertyBookingModel = vanueList.get(position);
            if (venuePropertyBookingModel != null){
                if (venuePropertyBookingModel.getVenueName() != null){
                    holder.itemNameTv.setText(venuePropertyBookingModel.getVenueName());
                } else {
                    holder.itemNameTv.setText("- - -");
                }
                holder.checkClick.setVisibility(venuePropertyBookingModel.getClickTitle() ? View.VISIBLE : View.INVISIBLE);
                holder.line.setVisibility(((alertPropertyBookingModelList.size() - 1) == position) ? View.GONE : View.VISIBLE);

                holder.itemView.setOnClickListener(view -> itemClickBack.onClickItemBack(null, venuePropertyBookingModel));
            }
        } else {
            AlertPropertyBookingModel alertPropertyBookingModel =  alertPropertyBookingModelList.get(position);
            if (alertPropertyBookingModel != null) {
                if (alertPropertyBookingModel.getPropertyName() != null){
                    holder.itemNameTv.setText(alertPropertyBookingModel.getPropertyName());
                } else {
                    holder.itemNameTv.setText("- - -");
                }
                holder.checkClick.setVisibility(alertPropertyBookingModel.isClick() ? View.VISIBLE : View.INVISIBLE);
                holder.line.setVisibility(((alertPropertyBookingModelList.size() - 1) == position) ? View.GONE : View.VISIBLE);

                holder.itemView.setOnClickListener(view -> itemClickBack.onClickItemBack(alertPropertyBookingModel, null));
            }
        }
    }

    @Override
    public int getItemCount() {
        if (isSelectVanue){
            return vanueList.size();
        } else {
            return alertPropertyBookingModelList.size();
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        private  final TextView itemNameTv;
        private final ImageView checkClick;
        private final View line;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            itemNameTv = itemView.findViewById(R.id.item_name);
            checkClick = itemView.findViewById(R.id.checkClick);
            line = itemView.findViewById(R.id.line);
        }
    }


    public interface ItemClickBack{
        void onClickItemBack(AlertPropertyBookingModel alertPropertyBookingModel, VenuePropertyBookingModel venuePropertyBookingModel);
    }
}
