package com.eazy.daikou.ui.home.book_now.booking_hotel.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.booking_hotel.TicketType

class HotelSelectEventOptionAdapter(
    private val list: ArrayList<TicketType>,
    private val startTime : String,
    private var duration: String,
    private var category : String,
    private val onClickListener: OnClickCallBackLister) :
    RecyclerView.Adapter<HotelSelectEventOptionAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.choose_event_model, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBindingValue(list[position], startTime, duration, category, onClickListener)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val soldOutTv: TextView = itemView.findViewById(R.id.soldOutTv)
        val iconRemove: ImageView = itemView.findViewById(R.id.iconMinus)
        val iconAdd: ImageView = itemView.findViewById(R.id.iconAdd)

        fun onBindingValue(itemModel: TicketType, startTime: String, duration: String, category : String, onClickListener: OnClickCallBackLister) {
            Utils.setValueOnText(itemView.findViewById(R.id.eventTitleTv), itemModel.name)
            Utils.setValueOnText(itemView.findViewById(R.id.priceTv), itemModel.price_display)
            Utils.setValueOnText(itemView.findViewById(R.id.dateTv), itemModel.date_form_to)
            Utils.setValueOnText(itemView.findViewById(R.id.durationTv), duration)
            Utils.setValueOnText(itemView.findViewById(R.id.availableTv), itemModel.number)

            if (category == "space"){
                Utils.setValueOnText(itemView.findViewById(R.id.titleStartTimeTv), iconAdd.context.getString(R.string.min_age))
                Utils.setValueOnText(itemView.findViewById(R.id.startTimeTv), itemModel.min_age)
            } else {
                Utils.setValueOnText(itemView.findViewById(R.id.titleStartTimeTv), iconAdd.context.getString(R.string.start_time))
                Utils.setValueOnText(itemView.findViewById(R.id.startTimeTv), startTime)
            }

            Utils.setValueOnText(
                itemView.findViewById(R.id.numTicketTv),
                String.format("%s %s", "$", Utils.formatDecimalFormatValue(itemModel.totalAllPrice))
            )

            if (itemModel.number != null){
                soldOutTv.visibility = if (itemModel.number == "0") View.VISIBLE else View.GONE
            } else {
                soldOutTv.visibility = View.GONE
            }

            Utils.setValueOnText(
                itemView.findViewById(R.id.quantityTv),
                itemModel.totalRoom.toString()
            )

            iconAdd.isEnabled = itemModel.num > itemModel.quantity
            iconRemove.isEnabled = itemModel.quantity > 0

            if (itemModel.num > 0 && itemModel.num > itemModel.quantity) {
                Utils.setBgTint(iconAdd, R.color.yellow)
            } else {
                Utils.setBgTint(iconAdd, R.color.gray)
            }

            if (itemModel.num > 0 && itemModel.quantity > 0) {
                Utils.setBgTint(iconRemove, R.color.yellow)
            } else {
                Utils.setBgTint(iconRemove, R.color.gray)
            }

            iconAdd.setOnClickListener(
                CustomSetOnClickViewListener {
                    if (itemModel.num > 0) {
                        onClickListener.onClickSpinnerCallBack(itemModel, "add", iconAdd)
                    }
                }
            )
            iconRemove.setOnClickListener(
                CustomSetOnClickViewListener {
                    if (itemModel.num > 0) {
                        onClickListener.onClickSpinnerCallBack(itemModel, "remove", iconRemove)
                    }
                }
            )
        }
}

interface OnClickCallBackLister {
    fun onClickSpinnerCallBack(
        itemRoomHotelBookingModel: TicketType,
        action: String,
        imageView: ImageView
    )
}

fun clear() {
    val size: Int = list.size
    if (size > 0) {
        for (i in 0 until size) {
            list.removeAt(0)
        }
        notifyItemRangeRemoved(0, size)
    }
}
}