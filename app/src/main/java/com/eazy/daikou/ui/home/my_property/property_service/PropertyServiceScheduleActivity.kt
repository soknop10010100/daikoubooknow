package com.eazy.daikou.ui.home.my_property.property_service

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.databinding.ActivityPropertyServiceScheduleBinding
import com.eazy.daikou.helper.DateNumPickerFragment
import com.eazy.daikou.helper.GetDataUtils
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.facility_booking.FeeTypeModel
import com.eazy.daikou.model.my_property.property_service.ServiceAvailableTimeModel
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.ui.home.my_property.property_service.adapter.MainServiceTypeAdapter
import com.eazy.daikou.ui.home.my_property.property_service.adapter.PropertyServiceAvailableDayAdapter
import com.eazy.daikou.ui.home.my_property.property_service.adapter.PropertyServiceAvailableDayAdapter.Companion.addListAvailableDay
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class PropertyServiceScheduleActivity : BaseActivity(), DatePickerDialog.OnDateSetListener {

    private lateinit var mBind : ActivityPropertyServiceScheduleBinding
    private var serviceType = ""
    private val MAX_SELECTABLE_DATE_IN_FUTURE = 365
    private var dpd : DatePickerDialog ?= null
    private var listAvailableDay = ArrayList<FeeTypeModel>()
    private var timeAdapter: MainServiceTypeAdapter? = null
    private var timeList = ArrayList<ServiceAvailableTimeModel>()
    private var day = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBind = ActivityPropertyServiceScheduleBinding.inflate(layoutInflater)
        setContentView(mBind.root)

        initView()

        initData()

        initAction()

    }

    private fun initView() {
        Utils.customOnToolbar(this, resources.getString(R.string.scheduled)) { finish() }
    }

    private fun initData() {
        serviceType = GetDataUtils.getDataFromString("service_type", this)

        if (serviceType == "time"){
            mBind.numQty.setText("1")
            mBind.numQty.isEnabled = false
        }
    }

    private fun initAction() {
        // Time
        val recyclerViewTime = mBind.recyclerViewTime.recyclerView
        recyclerViewTime.layoutManager = LinearLayoutManager(this)

        timeAdapter = MainServiceTypeAdapter(
            timeList, object : MainServiceTypeAdapter.ClickCallBackListener {
                @SuppressLint("NotifyDataSetChanged")
                override fun onClickCallBack(feeTypeModel: ServiceAvailableTimeModel) {
                    showTimeAlert(feeTypeModel.timeAvailableList).CallBackListener{ _: String, values: String, _: String? ->
                        feeTypeModel.numberOfTime = values
                        timeAdapter!!.notifyDataSetChanged()
                    }
                }

            })

        recyclerViewTime.adapter = timeAdapter

        // Available Day
        val recyclerView = mBind.recyclerView.recyclerView
        listAvailableDay = addListAvailableDay()
        recyclerView.layoutManager = GridLayoutManager(this, 3)
        var adapter: PropertyServiceAvailableDayAdapter? = null
        adapter = PropertyServiceAvailableDayAdapter(this, serviceType != "time",
            listAvailableDay, object : PropertyServiceAvailableDayAdapter.ClickCallBackListener {
                @SuppressLint("NotifyDataSetChanged")
                override fun onClickCallBack(feeTypeModel: FeeTypeModel) {
                    if (serviceType == "time") return
                    for (subItem in listAvailableDay){
                        if (subItem.id == feeTypeModel.id){
                            subItem.isClick = !subItem.isClick
                        }
                    }
                    addListTime()
                    adapter!!.notifyDataSetChanged()
                }
            })

        recyclerView.adapter = adapter


        // Date
        mBind.dateTV.setOnClickListener(CustomSetOnClickViewListener { showCalender() } )

        mBind.btnBook.setOnClickListener(CustomSetOnClickViewListener {
            val intent = Intent(this, PropertyServiceListDetailActivity::class.java).apply {
                putExtra("action", "request_detail")
            }
            startActivity(intent)
        } )
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun addListTime() {
        timeList.clear()
        for (subItem in listAvailableDay){
            if (serviceType == "time") {
                if (subItem.id.equals(day, true)){
                    timeList.add(MainServiceTypeAdapter.addListRegService(subItem.freeTypeName))
                }
            } else {
                if (subItem.isClick){
                    timeList.add(MainServiceTypeAdapter.addListRegService(subItem.freeTypeName))
                }
            }
        }
        timeAdapter!!.notifyDataSetChanged()
    }

    private fun showTimeAlert(timeList : ArrayList<String>) : DateNumPickerFragment {
        val dateNumPickerFragment = DateNumPickerFragment.newInstance("done_percent", timeList)
        dateNumPickerFragment.show(supportFragmentManager, "Dialog Fragment")
        return dateNumPickerFragment
    }

    override fun onDestroy() {
        super.onDestroy()
        dpd = null
    }

    private fun showCalender() {
        val now = Calendar.getInstance()
        if (dpd == null) {
            dpd = DatePickerDialog.newInstance(
                this@PropertyServiceScheduleActivity,
                now[Calendar.YEAR],
                now[Calendar.MONTH],
                now[Calendar.DAY_OF_MONTH]
            )
        } else {
            dpd!!.initialize(
                this@PropertyServiceScheduleActivity,
                now[Calendar.YEAR],
                now[Calendar.MONTH],
                now[Calendar.DAY_OF_MONTH]
            )
        }

        // restrict to weekdays only
        val weekdaysList = ArrayList<Calendar>()
        val day = Calendar.getInstance()
        val weekdays = ArrayList<Calendar>()
        for (i in 0 until MAX_SELECTABLE_DATE_IN_FUTURE) {
            for (item in listAvailableDay){
                if (day[Calendar.DAY_OF_WEEK] == dayOfWeekList()[item.id]) {
                    val d = day.clone() as Calendar
                    weekdays.add(d)
                }
            }

            day.add(Calendar.DATE, 1)
        }
        weekdaysList.addAll(weekdays)

        val weekdayDays: Array<Calendar> = weekdaysList.toArray(arrayOfNulls(weekdaysList.size))
        dpd!!.selectableDays = weekdayDays

        dpd!!.setOnCancelListener { dpd = null }
        dpd!!.show(supportFragmentManager, "date_picker_dialog")
    }

    override fun onDateSet(view: DatePickerDialog?, year: Int, monthOfYear: Int, dayOfMonth: Int) {
        val calendar = Calendar.getInstance()
        calendar.set(Calendar.YEAR, year)
        calendar.set(Calendar.MONTH, monthOfYear)
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
        val sdf = SimpleDateFormat("EEEE-dd-MM-yyyy", Locale.getDefault())
        mBind.dateTV.setText(sdf.format(calendar.time))
        dpd = null

        if (serviceType == "time") {
            day = SimpleDateFormat("EEE", Locale.getDefault()).format(calendar.time)
            addListTime()
        }
    }

    private fun dayOfWeekList() : HashMap<String, Any> {
        val hashMap = HashMap<String, Any>()
        hashMap["mon"] = 2
        hashMap["tue"] = 3
        hashMap["wed"] = 4
        hashMap["thu"] = 5
        hashMap["fri"] = 6
        hashMap["sat"] = 7
        hashMap["sun"] = 1
        return hashMap
    }

}