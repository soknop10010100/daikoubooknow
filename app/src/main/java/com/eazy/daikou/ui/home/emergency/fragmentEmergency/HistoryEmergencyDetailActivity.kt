package com.eazy.daikou.ui.home.emergency.fragmentEmergency

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.request_data.request.home_ws.EmergencyWs
import com.eazy.daikou.request_data.request.home_ws.EmergencyWs.CallBackEmergency
import com.eazy.daikou.helper.DateUtil
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.home.HisEmergencyModel
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.appbar.AppBarLayout

class HistoryEmergencyDetailActivity : BaseActivity(), OnMapReadyCallback {

    private var dateTv: TextView? = null
    private  var timeTv:TextView? = null
    private  var senderTv:TextView? = null
    private  var statusTv:TextView? = null
    private lateinit var senderPhoneTv : TextView
    private  var descriptionTv:TextView? = null
    private  var btnAccept:TextView? = null
    private  var btnDirection:TextView? = null
    private  var nameAcceptorTv:TextView? = null
    private  var positionTv:TextView? = null
    private  var phoneAcceptorTv:TextView? = null
    private  var noInfoAcceptorTv:RelativeLayout? = null
    private  var infoAcceptorLayout:LinearLayout? = null
    private lateinit var progressBar : ProgressBar
    private var mainLayout : RelativeLayout ? = null
    private lateinit var emergencyId : String
    private var mMap: GoogleMap? = null
    private lateinit var mainScrollView : ScrollView
    private var userId = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_history_emergency_detail)

        // Change Status Bar To Red
        Utils.changeStatusBarColor(ContextCompat.getColor(this, R.color.red), window)
        findViewById<AppBarLayout>(R.id.appBarLayout).setBackgroundColor(ContextCompat.getColor(this, R.color.red))

        initView()

        initData()

        initAction()

    }

    private fun initView(){
        dateTv = findViewById(R.id.date)
        timeTv = findViewById(R.id.time)
        senderTv = findViewById(R.id.senderTv)
        statusTv = findViewById(R.id.status)
        descriptionTv = findViewById(R.id.descriptionTv)
        btnAccept = findViewById(R.id.btnAccept)
        btnDirection = findViewById(R.id.btnGetDirection)
        progressBar = findViewById(R.id.progressItem)
        mainLayout = findViewById(R.id.mainLayout)
        nameAcceptorTv = findViewById(R.id.nameAcceptorTv)
        positionTv = findViewById(R.id.positionTv)
        phoneAcceptorTv = findViewById(R.id.phoneTv)
        nameAcceptorTv = findViewById(R.id.nameAcceptorTv)
        infoAcceptorLayout = findViewById(R.id.infoAcceptLayout)
        noInfoAcceptorTv = findViewById(R.id.noInfoAcceptor)
        mainScrollView = findViewById(R.id.mainScroll)
        senderPhoneTv = findViewById(R.id.senderPhoneTv)
    }

    private fun initData(){
        Utils.customOnToolbar(this, resources.getString(R.string.emergency_detail)){finish()}
        findViewById<LinearLayout>(R.id.layoutMap).visibility = View.INVISIBLE

        if(intent != null && intent.hasExtra("emergency_id")){
            emergencyId = intent.getStringExtra("emergency_id").toString()
        }

        userId = Utils.validateNullValue(UserSessionManagement(this).userId)

        val mapFragment = (supportFragmentManager.findFragmentById(R.id.map_google) as SupportMapFragment?)!!
        mapFragment.getMapAsync(this)

    }

    private fun initAction(){
        requestInfo()
    }

    private fun requestInfo(){
        progressBar.visibility = View.VISIBLE
        EmergencyWs().getHistoryEmergencyDetail(this, emergencyId, object : EmergencyWs.CallBackHistoryEmergencyDetail {
            override fun hisEmergencyModel(hisEmergencyModels: HisEmergencyModel?) {
                progressBar.visibility = View.GONE
                mainLayout!!.visibility = View.VISIBLE
                hisEmergencyModels?.let { setValue(it) }
            }

            override fun onFailed(msg: String?) {
                progressBar.visibility = View.GONE
                Utils.customToastMsgError(this@HistoryEmergencyDetailActivity, msg, false)
            }

        })
    }

    private fun setValue(emergency : HisEmergencyModel){
        progressBar.visibility = View.GONE
        dateTv!!.text  = DateUtil.formatDateComplaint(emergency.createdDt)
        timeTv!!.text  = DateUtil.formatDateToConvertOnlyTimeZoneNoSecond(emergency.createdDt)
        senderTv!!.text = if (emergency.senderFullName != null) String.format("%s", emergency.senderFullName) else "- - -"
        statusTv!!.text = if (emergency.status != null) String.format("%s", Utils.convertFirstCapital(emergency.status)) else "- - -"
        senderPhoneTv.text = if (emergency.sender_user_phone != null) String.format("%s", emergency.sender_user_phone) else "- - -"

        if (emergency.description != null) {
            descriptionTv!!.text = String.format("%s", emergency.description)
        } else {
            descriptionTv!!.text = String.format("%s", resources.getString(R.string.no_reason))
        }

        if(emergency.status != null){
            if (emergency.status.equals("pending")){
                Utils.setBgTint(statusTv, R.color.yellow)
                noInfoAcceptorTv!!.visibility = View.VISIBLE
                infoAcceptorLayout!!.visibility = View.GONE
            } else {
                Utils.setBgTint(statusTv, R.color.green)
                btnAccept!!.visibility = View.GONE

                noInfoAcceptorTv!!.visibility = View.GONE
                infoAcceptorLayout!!.visibility = View.VISIBLE

                if (emergency.receiverFullName != null){
                    nameAcceptorTv!!.text = emergency.receiverFullName
                }
                if (emergency.receiver_position != null){
                    positionTv!!.text = emergency.receiver_position
                }
                if (emergency.receiver_phone_number != null){
                    phoneAcceptorTv!!.text = emergency.receiver_phone_number
                }
            }

            //Check Button Action
            if (emergency.senderUserIdFk != null) {
                if (userId != emergency.senderUserIdFk && emergency.status.equals("pending")){
                    btnAccept!!.visibility = View.VISIBLE
                    btnDirection!!.visibility = View.VISIBLE
                } else {
                    btnAccept!!.visibility = View.GONE
                    btnDirection!!.visibility = if (userId != emergency.senderUserIdFk) View.VISIBLE else View.GONE
                }
            } else {
                btnAccept!!.visibility = View.GONE
                btnDirection!!.visibility = View.GONE
            }
        }

        btnAccept?.setOnClickListener(
            CustomSetOnClickViewListener{
                requestServiceStatus(emergencyId, "accept")
            }
        )

        btnDirection?.setOnClickListener(
            CustomSetOnClickViewListener{
                if (emergency.coordLat != null && emergency.coordLong != null) {
                    Utils.startOpenLinkMaps(this, emergency.coordLat, emergency.coordLong)
                }
            }
        )
        if( emergency.coordLat !=null && emergency.coordLong != null){
            zoomMap(emergency.coordLat.toDouble(), emergency.coordLong.toDouble())
            val fullName : String = if (emergency.senderFullName != null){
                emergency.senderFullName
            } else{
                resources.getString(R.string.sender)
            }
            addMarker(emergency.coordLat.toDouble(), emergency.coordLong.toDouble(), fullName)
        }

        if (btnAccept!!.visibility == View.GONE && btnDirection!!.visibility == View.GONE){
            val scrollView = findViewById<ScrollView>(R.id.mainScroll)
            val layoutParams = scrollView.layoutParams as RelativeLayout.LayoutParams
            layoutParams.setMargins(0, 0, 0, 0)
            scrollView.layoutParams = layoutParams
        }

    }

    private fun requestServiceStatus(id: String, status: String) {
        progressBar.visibility = View.VISIBLE
        val hashMap = HashMap<String, Any>()
        hashMap["emergency_history_id"] = id
        hashMap["user_id"] = UserSessionManagement(this@HistoryEmergencyDetailActivity).userId
        hashMap["status"] = status
        EmergencyWs().statusEmergency(this@HistoryEmergencyDetailActivity, hashMap, statusEmergencyCallBack)
    }

    private val statusEmergencyCallBack: CallBackEmergency = object : CallBackEmergency {
        override fun onSuccess(msgSuccess: String) {
            Utils.customToastMsgError(this@HistoryEmergencyDetailActivity, msgSuccess, true)
            progressBar.visibility = View.GONE

            requestInfo()

            setResult(RESULT_OK)
        }

        override fun onSuccessFalse(status: String) {
            progressBar.visibility = View.GONE
            Utils.customToastMsgError(this@HistoryEmergencyDetailActivity, status, false)
        }

        override fun onFailed(mess: String) {
            progressBar.visibility = View.GONE
            Utils.customToastMsgError(this@HistoryEmergencyDetailActivity, mess, false)
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }

        mMap!!.isMyLocationEnabled = true
        mMap!!.uiSettings.isZoomControlsEnabled = false
        mMap!!.uiSettings.isMyLocationButtonEnabled = false
        mMap!!.uiSettings.isCompassEnabled = false
    }

    private fun addMarker(lat: Double, lon: Double, name: String) {
        val latLng = LatLng(lat, lon)
        mMap?.addMarker(MarkerOptions().position(latLng).title(name))
            ?.showInfoWindow()
    }

    private fun zoomMap(lat: Double, lng: Double) {
        mMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(lat, lng), 15f))
    }

}