package com.eazy.daikou.ui.contact;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.eazy.daikou.base.BaseFragment;
import com.eazy.daikou.base.MockUpData;
import com.eazy.daikou.helper.UserSessionManagement;
import com.eazy.daikou.model.contact.ContactProperty;
import com.eazy.daikou.model.profile.User;
import com.eazy.daikou.ui.contact.adapter.ContactPropertyAdapter;
import com.eazy.daikou.R;
import com.eazy.daikou.request_data.request.home_ws.ContactWs;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class ContactPropertyFragment extends BaseFragment {
    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private ContactPropertyAdapter contactPropertyAdapter;
    private int currentItem, total , scrollDown, currentPage = 1 ,size = 10;
    private boolean isScrolling;
    private List<ContactProperty> list = new ArrayList<>();
    private SwipeRefreshLayout swipeRefreshLayout;
    private TextView txtNoResult;
    private LinearLayoutManager linearLayoutManager;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_property, container, false);
        recyclerView = view.findViewById(R.id.recyclerView);

        progressBar = view.findViewById(R.id.progressItem);
        swipeRefreshLayout = view.findViewById(R.id.refreshScreen);
        txtNoResult = view.findViewById(R.id.txtNoResult);

        linearLayoutManager = new LinearLayoutManager(mActivity);
        recyclerView.setLayoutManager(linearLayoutManager);

        boolean isUserGuest = MockUpData.isUserAsGuest(new UserSessionManagement(mActivity));
        if (isUserGuest){
            swipeRefreshLayout.setVisibility(View.GONE);
            recyclerView.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
            txtNoResult.setVisibility(View.VISIBLE);
            txtNoResult.setText(mActivity.getResources().getString(R.string.coming_soon));
        } else {
            ContactFragment.btnAdd.setVisibility(View.INVISIBLE);
            onProgressLoading();
            initPropertyView();
            initRecycleViewScroll();

            swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
            swipeRefreshLayout.setOnRefreshListener(() -> {
                currentPage = 1;
                size = 10;
                contactPropertyAdapter.clear();
                list = new ArrayList<>();
                initPropertyView();
            });

        }

        return view;
    }

    private void onProgressLoading(){
        progressBar.setVisibility(View.VISIBLE);
    }

    void initPropertyView(){
        new ContactWs().getContactDataPropertyAndDaiKou(mActivity,new Gson().fromJson(new UserSessionManagement(mActivity).getUserDetail(), User.class).getActivePropertyIdFk(),currentPage ,size ,contactCallBackListener);
        contactPropertyAdapter = new ContactPropertyAdapter(list , phoneCall);
        recyclerView.setAdapter(contactPropertyAdapter);
    }

    private void initRecycleViewScroll(){
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener(){
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL){
                    isScrolling = true;
                }
            }
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                currentItem = linearLayoutManager.getChildCount();
                total = linearLayoutManager.getItemCount();
                scrollDown = linearLayoutManager.findFirstVisibleItemPosition();
                if(dy > 0) {
                    if (isScrolling && (currentItem + scrollDown == total)) {
                        if(total == size) {
                            isScrolling = false;
                            currentPage++;
                            onProgressLoading();
                            initPropertyView();
                            size += 10;
                        }
                    }
                }

            }
        });
    }

    private final ContactPropertyAdapter.ItemClickListener phoneCall = new ContactPropertyAdapter.ItemClickListener() {
        @Override
        public void phoneCall(ContactProperty contactModel) {
            String mobile_phone = contactModel.getPhone_number();
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:" + mobile_phone));
            startActivity(intent);
        }

        @Override
        public void openTelegram(ContactProperty contactModel) {
            String TELEGRAM_PACKAGE = "org.telegram.messenger";
            if(contactModel.getTelegram() != null && !contactModel.getTelegram().equalsIgnoreCase(""))
                intentMessageTelegram(contactModel.getTelegram(), TELEGRAM_PACKAGE);
            else Toast.makeText(mActivity,getString(R.string.no_contact),Toast.LENGTH_SHORT).show();
        }

        @Override
        public void whatsApp(ContactProperty contactModel) {
            String WHATSAPP_PACKAGE = "com.whatsapp";
            if(contactModel.getWhatsapp() != null && !contactModel.getWhatsapp().equalsIgnoreCase(""))
                intentMessageTelegram(contactModel.getWhatsapp(), WHATSAPP_PACKAGE);
            else Toast.makeText(mActivity,getString(R.string.no_contact),Toast.LENGTH_SHORT).show();
        }

        @Override
        public void weChat(ContactProperty contactModel) {
            String WECHAT_PACKAGE = "com.tencent.mm";
            if(contactModel.getWechat() != null && !contactModel.getWechat().equalsIgnoreCase(""))
                intentMessageTelegram(contactModel.getWechat(), WECHAT_PACKAGE);
            else Toast.makeText(mActivity,getString(R.string.no_contact),Toast.LENGTH_SHORT).show();
        }

        @Override
        public void videoCall(ContactProperty contactModel) {
            Toast.makeText(getContext(),getResources().getString(R.string.under_construction),Toast.LENGTH_SHORT).show();
        }
    };

    public static boolean isAppAvailable(Context context, String appName) {
        PackageManager pm = context.getPackageManager();
        try
        {
            pm.getPackageInfo(appName, PackageManager.GET_ACTIVITIES);
            return true;
        }
        catch (PackageManager.NameNotFoundException e)
        {
            return false;
        }
    }

    void intentMessageTelegram(String msg,String packageName) {
        final boolean isAppInstalled = isAppAvailable(mActivity, packageName);
        if (isAppInstalled) {
            Intent myIntent = new Intent(Intent.ACTION_SEND);
            myIntent.setType("text/plain");
            myIntent.setPackage(packageName);
            myIntent.putExtra(Intent.EXTRA_TEXT, msg);//
            mActivity.startActivity(Intent.createChooser(myIntent, "Share with"));
        } else {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/details?id=" + packageName)));
        }
    }

    private final ContactWs.ContactCallBackListener contactCallBackListener = new ContactWs.ContactCallBackListener() {
        @SuppressLint("NotifyDataSetChanged")
        @Override
        public void getContactModel(List<ContactProperty> contactModelList) {
            progressBar.setVisibility(View.GONE);
            swipeRefreshLayout.setRefreshing(false);
            list.addAll(contactModelList);
            if (list.isEmpty()) {
                txtNoResult.setVisibility(View.VISIBLE);
            }
            contactPropertyAdapter.notifyDataSetChanged();
        }

        @Override
        public void onFailed(String error) {
            progressBar.setVisibility(View.GONE);
            Toast.makeText(mActivity,error,Toast.LENGTH_SHORT).show();
        }
    };
    
}