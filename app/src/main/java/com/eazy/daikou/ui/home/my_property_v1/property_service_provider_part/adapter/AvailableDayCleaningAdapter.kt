package com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.facility_booking.FeeTypeModel

class AvailableDayCleaningAdapter(private val context: Context, private val dayList: ArrayList<FeeTypeModel>, private val serviceType: String, private val itemClick: ItemClickOnService):
    RecyclerView.Adapter<AvailableDayCleaningAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.layout_adapter_pay, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {
        val listService = dayList[position]
        if (listService != null) {
            holder.nameCategoryTv.text = listService.freeTypeName
            if (serviceType == "time") {
                if (listService.isClick) {
                    holder.nameCategoryTv.setTextColor(Utils.getColor(context, R.color.white))
                    holder.linearLayout.setBackgroundResource(R.color.appBarColorOld)
                } else {
                    holder.linearLayout.setBackgroundResource(R.color.color_gray_item)
                    holder.nameCategoryTv.setTextColor(Utils.getColor(context, R.color.gray))
                }

                holder.itemView.setOnClickListener { itemClick.onClickItem(listService) }
            } else {
                if (listService.isClick) {
                    holder.nameCategoryTv.setTextColor(Utils.getColor(context, R.color.white))
                    holder.linearLayout.setBackgroundResource(R.color.appBarColorOld)
                } else {
                    holder.linearLayout.setBackgroundResource(R.color.color_gray_item)
                    holder.nameCategoryTv.setTextColor(Utils.getColor(context, R.color.gray))
                }
                holder.iconRemove.visibility = if (listService.isClick) View.VISIBLE else View.GONE

                holder.itemView.setOnClickListener { itemClick.onClickItem(listService) }
            }
        }
    }

    override fun getItemCount(): Int {
        return dayList.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var nameCategoryTv: TextView = view.findViewById(R.id.text_category)
        var linearLayout: LinearLayout = view.findViewById(R.id.linear_card)
        var iconRemove : ImageView = view.findViewById(R.id.iconRemove)
    }

    interface ItemClickOnService {
        fun onClickItem(listString: FeeTypeModel)
    }
}