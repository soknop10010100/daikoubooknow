package com.eazy.daikou.ui.home.book_now.booking_hotel.adapter

import android.app.Activity
import android.os.Build
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.home.SubItemHomeModel
import com.google.android.material.card.MaterialCardView
import com.squareup.picasso.Picasso

class HotelVisitItemAdapter(private var activity: Activity, private val action: String, private val bookingList: ArrayList<SubItemHomeModel>, private val propertyClick: BookingHomeItemAdapter.PropertyClick) :
    RecyclerView.Adapter<HotelVisitItemAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.hotel_visit_item_adapter, parent, false)
        if (action == "visits_sub" || action == "visits_sub_detail"){
            val itemViewParam = LinearLayout.LayoutParams(Utils.getWidth(activity) / 2, LinearLayout.LayoutParams.WRAP_CONTENT)
            view.layoutParams = itemViewParam
        }
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val propertyModel = bookingList[position]
        if (propertyModel != null) {
            if (propertyModel.imageUrl != null) {
                Picasso.get().load(propertyModel.imageUrl).into(holder.imageView)
            } else {
                Picasso.get().load(R.drawable.no_image).into(holder.imageView)
            }

            holder.hotelName.text = if (propertyModel.name != null) propertyModel.name else ""
            holder.hotelNameV2.text = if (propertyModel.name != null) propertyModel.name else ""

            // Home page visit
            when (propertyModel.action) {
                "news", "popular", "visits_sub_detail" -> {
                    Utils.setValueOnText(holder.address, propertyModel.views)
                    Utils.setValueOnText(holder.descriptionOnImgTv , propertyModel.views)
                    holder.mainLayout.setPadding(5,0,0,0)
                }
                "activity", "food"-> {
                    val description: String
                    if (propertyModel.address != null){
                        description = propertyModel.address!!.replace("black", "white")
                        holder.descriptionOnImgTv.text = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            Html.fromHtml(description, Html.FROM_HTML_MODE_COMPACT)
                        } else {
                            Html.fromHtml(description)
                        }
                    } else {
                        holder.descriptionOnImgTv.text = ". . ."
                    }
                    holder.mainLayout.setPadding(5,0,0,0)
                }
                else -> {   // Visit Place
                    Utils.setValueOnText(holder.address, propertyModel.titleKh)
                    Utils.setValueOnText(holder.descriptionOnImgTv , propertyModel.titleKh)
                }
            }
            if (propertyModel.action == "visits_sub" || propertyModel.action == "visits_sub_detail" ||  propertyModel.action == "food" || propertyModel.action == "popular" || propertyModel.action == "activity") {
                holder.cardViewLayout.layoutParams.height = if (propertyModel.action == "popular" || propertyModel.action == "activity") Utils.dpToPx(activity, 180) else Utils.dpToPx(activity, 130)
                holder.locationTxtLayout.visibility = View.VISIBLE
                holder.notLocationTxtLayout.visibility = View.GONE
            } else {
                holder.locationTxtLayout.visibility = View.GONE
                holder.notLocationTxtLayout.visibility = View.VISIBLE
            }

            if (propertyModel.action == "food") {
                val heightImg = Utils.dpToPx(activity, 220)
                if (position % 3 == 0) {
                    holder.cardViewLayout.layoutParams.height = heightImg + Utils.dpToPx(activity, 10)
                } else{
                    holder.cardViewLayout.layoutParams.height = heightImg / 2
                }
            }

            // Show visit by location
            if (action == "visit_by_location"){
                holder.cardViewLayout.layoutParams.height =  Utils.dpToPx(activity, 220)
                holder.locationTxtLayout.visibility = View.VISIBLE
                holder.notLocationTxtLayout.visibility = View.GONE
            } else if (action == "nearby_item_detail_visit"){
                holder.cardViewLayout.layoutParams.height =  Utils.dpToPx(activity, 180)
                holder.locationTxtLayout.visibility = View.VISIBLE
                holder.notLocationTxtLayout.visibility = View.GONE
            }

            // On click item
            holder.materialLayout.setOnClickListener(
                CustomSetOnClickViewListener{
                    propertyClick.onBookingClick(propertyModel)
                }
            )
        }
    }

    override fun getItemCount(): Int {
        return bookingList.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val hotelName: TextView = itemView.findViewById(R.id.property_name)
        val hotelNameV2: TextView = itemView.findViewById(R.id.property_name_v2)
        val address: TextView = itemView.findViewById(R.id.addressTv)
        val descriptionOnImgTv : TextView = itemView.findViewById(R.id.addressV2Tv)
        val imageView: ImageView = itemView.findViewById(R.id.image)
        val mainLayout : LinearLayout = itemView.findViewById(R.id.mainLayout)
        val cardViewLayout : CardView = itemView.findViewById(R.id.cardViewLayout)
        val locationTxtLayout : LinearLayout = itemView.findViewById(R.id.locationTxtLayout)
        val notLocationTxtLayout : LinearLayout = itemView.findViewById(R.id.notLocationTxtLayout)
        val materialLayout : MaterialCardView = itemView.findViewById(R.id.materialLayout)
    }

    interface PropertyClick {
        fun onBookingClick(propertyModel: SubItemHomeModel?)
        fun onBookingWishlistClick(propertyModel: SubItemHomeModel?)
    }

    fun clear() {
        val size: Int = bookingList.size
        if (size > 0) {
            for (i in 0 until size) {
                bookingList.removeAt(0)
            }
            notifyItemRangeRemoved(0, size)
        }
    }
}