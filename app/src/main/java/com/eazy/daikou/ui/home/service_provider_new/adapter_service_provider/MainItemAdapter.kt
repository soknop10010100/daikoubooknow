package com.eazy.daikou.ui.home.service_provider_new.adapter_service_provider

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.helper.AbsoluteFitLayoutManager
import com.eazy.daikou.model.service_provider.MainItemModel
import com.eazy.daikou.model.service_provider.SubCategory

class MainItemAdapter(private val activity: Activity, private val context: Context, private val mainItemList: ArrayList<MainItemModel>, private val callBackSeeAll: CallBackItemSeeAll):RecyclerView.Adapter<MainItemAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.custom_layout_main_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
       val mainItem : MainItemModel =  mainItemList[position]
        if (mainItem != null){
            holder.titleMainTv.text = mainItem.titleHeader

            if (mainItem.titleHeader == "Special offer"){
                holder.titleMainTv.text = "Top Destination"
                holder.descriptionTv.text = "Show about location that you want . . ."
                holder.recyclerViewMain.layoutManager =
                    AbsoluteFitLayoutManager(
                        context,
                        1,
                        RecyclerView.HORIZONTAL,
                        false,
                        2
                    )
                val popularAdapter = PopularProviderAdapter(activity)
                holder.recyclerViewMain.adapter = popularAdapter
            }

            if (mainItem.titleHeader == "Company") {
                holder.descriptionTv.text = "Show all companies that you want . . ."
                val gridLayoutManager = GridLayoutManager(context,2, GridLayoutManager.HORIZONTAL, false)
                holder.recyclerViewMain.layoutManager = gridLayoutManager
                val companyAdapter = CompanyServiceProviderAdapter(context, mainItem.subCategoryList, subServiceCallBack)
                holder.recyclerViewMain.adapter = companyAdapter
            }

            if (mainItem.titleHeader == "Service Near By") {
                holder.titleMainTv.text = "Near by"
                holder.descriptionTv.text = "Show services that you near you . . ."
                holder.recyclerViewMain.layoutManager = spanGridLayoutManager(context)
                val featureAdapter = NearByServiceProviderAdapter(activity, context)
                holder.recyclerViewMain.adapter = featureAdapter
            }

            if (mainItem.titleHeader == "Service") {
                holder.titleMainTv.text = "Featuring Services"
                holder.descriptionTv.text = "Show services that more popular . . ."
                val gridLayoutManager  = GridLayoutManager(context, 2,RecyclerView.VERTICAL, false)
                holder.recyclerViewMain.layoutManager = gridLayoutManager
                val serviceAdapter = ServiceProviderAdapter(context, mainItem.subCategoryList, serviceCallBack)
                holder.recyclerViewMain.adapter = serviceAdapter
            }
            holder.seeMoreTv.setOnClickListener { callBackSeeAll.clickSeeAllListener(mainItem) }
        }
    }

    override fun getItemCount(): Int {
      return mainItemList.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view){
        var recyclerViewMain: RecyclerView = view.findViewById(R.id.recyclerViewMain)
        var titleMainTv: TextView = view.findViewById(R.id.titleMainTv)
        var seeMoreTv: TextView = view.findViewById(R.id.seeMoreTv)
        var descriptionTv : TextView = view.findViewById(R.id.descriptionTv)
    }

    private val subServiceCallBack : CompanyServiceProviderAdapter.ClickItemCallBackListener = object : CompanyServiceProviderAdapter.ClickItemCallBackListener{
        override fun clickItemListener(companyData: SubCategory) {
            callBackSeeAll.clickCompanyToActivityListener(companyData)
        }
    }
    
    private val serviceCallBack: ServiceProviderAdapter.CallBackItemListService = object  : ServiceProviderAdapter.CallBackItemListService{
        override fun clickItemServiceListener(service: SubCategory) {
            callBackSeeAll.clickServiceProviderListener(service)
        }
    }

    interface CallBackItemSeeAll{
        fun clickSeeAllListener(mainItem : MainItemModel)
        fun clickCompanyToActivityListener(companyData: SubCategory)
        fun clickServiceProviderListener(Service: SubCategory)
    }

    private fun spanGridLayoutManager(context: Context): RecyclerView.LayoutManager {
        val layoutManager = GridLayoutManager(context, 2, GridLayoutManager.HORIZONTAL, false)
        layoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                return if (position % 3 == 0) {
                    2
                } else
                    1
            }
        }
        return layoutManager
    }

}