package com.eazy.daikou.ui.home.hrm.notice_board.adapter

import android.app.Activity
import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.hr.NoticeBoardHrModel
import com.eazy.daikou.model.hr.Relationship

class NoticeBoardAdapter(private val context : Activity, private val listNotice : ArrayList<NoticeBoardHrModel>,private val callBackItem: CallBackNoticeListener) : RecyclerView.Adapter<NoticeBoardAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.notice_board_item_model, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val noticeBoardHrModel: NoticeBoardHrModel = listNotice[position]
        if (noticeBoardHrModel != null){

            if (noticeBoardHrModel.notice_title != null){
                holder.noticeTitleTv.text = noticeBoardHrModel.notice_title
            }else {
                holder.noticeTitleTv.text = "- - -"
            }

            if (noticeBoardHrModel.created_at != null){
                holder.dateTv.text = noticeBoardHrModel.created_at
            }else {
                holder.dateTv.text = "- - -"
            }

            if (noticeBoardHrModel.description != null){
                holder.descriptionTv.text = noticeBoardHrModel.description
            }else {
                holder.descriptionTv.text = "- - -"
            }

            if (noticeBoardHrModel.status.equals("public")){
                holder.iconNoticeBoard.setImageResource(R.drawable.megaphone_min)
                holder.iconNoticeBoard.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(context, R.color.greenSea))
            } else {
                holder.iconNoticeBoard.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(context, R.color.red))
                holder.iconNoticeBoard.setImageResource(R.drawable.expired)
            }

            holder.itemView.setOnClickListener{
                callBackItem.onItemClick(noticeBoardHrModel)
            }
        }
    }

    override fun getItemCount(): Int {
        return listNotice.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val  iconNoticeBoard : ImageView = view.findViewById(R.id.iconNoticeBoard)
        var noticeTitleTv : TextView = view.findViewById(R.id.noticeTitleTv)
        var  dateTv : TextView = view.findViewById(R.id.dateTv)
        var descriptionTv : TextView = view.findViewById(R.id.descriptionTv)
    }

    interface CallBackNoticeListener{
        fun onItemClick(noticeBoardHrModel: NoticeBoardHrModel)
    }

    fun clear() {
        val size: Int = listNotice.size
        if (size > 0) {
            for (i in 0 until size) {
                listNotice.removeAt(0)
            }
            notifyItemRangeRemoved(0, size)
        }
    }


}