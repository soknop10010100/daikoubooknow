package com.eazy.daikou.ui.home.service_provider_new.adapter_service_provider

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.model.service_provider.SubCategory

class ServiceProviderAdapter(private val context: Context, private val serviceList: ArrayList<SubCategory>, private val callBackService: CallBackItemListService ): RecyclerView.Adapter<ServiceProviderAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
       val view = LayoutInflater.from(context).inflate(R.layout.custom_layout_service_provider, parent, false)
        return  ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
       val service : SubCategory = serviceList[position]
        if (service != null){
            if (service.urlImage != null){
                Glide.with(context).load(service.urlImage).into(holder.imageViewService)
            } else {
                Glide.with(context).load(R.drawable.no_image).into(holder.imageViewService)
            }
            holder.nameServiceTv.text = if(service.title != null) service.title else "- - -"
            if (service.complyInfo!!.urlImage != null){
                Glide.with(context).load(service.complyInfo!!.urlImage).into(holder.imageIcon)
            } else {
                Glide.with(context).load(R.drawable.no_image).into(holder.imageIcon)
            }

            holder.textNameCompanyTv.text = if (service.complyInfo!!.urlImage != null) service.complyInfo!!.title else "- - -"

            holder.itemView.setOnClickListener { callBackService.clickItemServiceListener(service) }
        }
    }

    override fun getItemCount(): Int { return serviceList.size }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        var imageViewService : ImageView = view.findViewById(R.id.imageViewFeature)
        var nameServiceTv : TextView = view.findViewById(R.id.nameServiceTv)
        var imageIcon : ImageView = view.findViewById(R.id.imageIcon)
        var priceTv : TextView = view.findViewById(R.id.priceTv)
        var addressTv : TextView = view.findViewById(R.id.addressTv)
        var textNameCompanyTv : TextView = view.findViewById(R.id.textNameCompanyTv)
    }
    interface CallBackItemListService{
        fun clickItemServiceListener(service : SubCategory)
    }

}