package com.eazy.daikou.ui.home.book_now.booking_hotel

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AbsListView
import android.widget.ProgressBar
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.base.MockUpData
import com.eazy.daikou.request_data.request.book_now_ws.BookingHotelWS
import com.eazy.daikou.request_data.request.book_now_ws.RequestHashMapData
import com.eazy.daikou.helper.AppAlertCusDialog
import com.eazy.daikou.helper.GetDataUtils
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.booking_hotel.LocationHotelModel
import com.eazy.daikou.model.home.MainItemHomeModel
import com.eazy.daikou.model.home.SubItemHomeModel
import com.eazy.daikou.model.home.UserDeactivated
import com.eazy.daikou.ui.home.book_now.booking_hotel.adapter.BookingHomeItemAdapter

class HotelShowAllItemFromHomeActivity : BaseActivity() {

    private lateinit var progressBar: ProgressBar
    private lateinit var recyclerView: RecyclerView
    private lateinit var refreshLayout: SwipeRefreshLayout
    private lateinit var bookingHomeItemAdapter: BookingHomeItemAdapter
    private lateinit var linearLayoutManager : LinearLayoutManager
    private var eazyhotelUserId = ""
    private var subItemHomeModelList : ArrayList<SubItemHomeModel> = ArrayList()
    private var locationHotelModelList: ArrayList<LocationHotelModel> = ArrayList()

    private var currentItem = 0
    private var total : Int = 0
    private var scrollDown : Int = 0
    private var currentPage : Int = 1
    private var size : Int = 10
    private var isScrolling = false
    private var category = ""
    private var actionType = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hotel_show_all_item_from_home)

        initView()

        initData()

        initAction()

    }

    private fun initView(){
        progressBar = findViewById(R.id.progressItem)
        recyclerView = findViewById(R.id.recyclerView)
        refreshLayout = findViewById(R.id.swipe_layouts)
    }

    private fun initData(){
        eazyhotelUserId = Utils.validateNullValue(MockUpData.getEazyHotelUserId(UserSessionManagement(this)))

        val titleHeader = GetDataUtils.getDataFromString("header_title", this)
        val type = GetDataUtils.getDataFromString("action_type", this)
        category = GetDataUtils.getDataFromString("category", this)

        Utils.customOnToolbar(this, titleHeader){ finish() }

        if (intent != null && intent.hasExtra("list_item")){
            if (type == "top_destination"){
                progressBar.visibility = View.GONE
                refreshLayout.isEnabled = false
                subItemHomeModelList = intent.getSerializableExtra("list_item") as ArrayList<SubItemHomeModel>
                for (item in subItemHomeModelList) { // Use for top destination
                    item.action = "see_all_top_destination"
                }
            }
        }

        actionType = getType(type)

        if (intent != null && intent.hasExtra("location_list")){
            locationHotelModelList = intent.getSerializableExtra("location_list") as ArrayList<LocationHotelModel>
        }

    }

    private fun getType(action : String) : String {
        return when (action) {
            "top_destination" -> "location"
            "all_nearby_hotels" -> "nearby"
            "featuring_hotel" -> "feature"
            else -> "all"
        }
    }

    private fun initAction(){
        refreshLayout.setColorSchemeResources(R.color.appBarColorOld)
        refreshLayout.setOnRefreshListener { refreshList() }

        linearLayoutManager = LinearLayoutManager(this)

        initItemRecycle()

        if (actionType != "location") {
            requestServiceWs()

            onScrollInfoRecycle()
        }
    }

    private fun initItemRecycle(){
        recyclerView.layoutManager = linearLayoutManager
        bookingHomeItemAdapter = BookingHomeItemAdapter(this, "bestseller_listing", subItemHomeModelList, onClickSubItemBookingListener)
        recyclerView.adapter = bookingHomeItemAdapter
    }

    private fun refreshList(){
        currentPage = 1
        size = 10
        isScrolling = true

        bookingHomeItemAdapter.clear()
        requestServiceWs()
    }

    private fun hashMap() : HashMap<String, Any>{
        val hashMap = HashMap<String, Any>()
        hashMap["page"] = currentPage
        hashMap["limit"] = 10
        hashMap["category"] = category
        hashMap["type"] = actionType
        hashMap["user_id"] = eazyhotelUserId
        return hashMap
    }

    private fun requestServiceWs(){
        progressBar.visibility = View.VISIBLE
        BookingHotelWS().getSeeAllBookingByCategoryAll(this, "see_all_function_hotel", category, hashMap(), object : BookingHotelWS.OnCallBackHomePageItemListener{
            override fun onSuccessShowHotel(travelTalkBanner: ArrayList<String>, discountBanner: ArrayList<String>, listImageSlider: MutableList<String>,
                mainItemHomeModelLIst: ArrayList<MainItemHomeModel>,
                itemHomeList: ArrayList<SubItemHomeModel>,
                userDeactivated: UserDeactivated) { }

            @SuppressLint("NotifyDataSetChanged")
            override fun onSuccessShowHotelAll(itemHomeList: ArrayList<SubItemHomeModel>) {
                progressBar.visibility = View.GONE
                refreshLayout.isRefreshing = false

                subItemHomeModelList.addAll(itemHomeList)

                bookingHomeItemAdapter.notifyDataSetChanged()

                Utils.validateViewNoItemFound(this@HotelShowAllItemFromHomeActivity, recyclerView, subItemHomeModelList.size <= 0)
            }

            override fun onFailed(message: String) {
                progressBar.visibility = View.GONE
                refreshLayout.isRefreshing = false
                Utils.customToastMsgError(this@HotelShowAllItemFromHomeActivity, message, false)
            }

        })
    }

    private var onClickSubItemBookingListener = object : BookingHomeItemAdapter.PropertyClick {
        override fun onBookingClick(propertyModel: SubItemHomeModel?) {
            if (propertyModel != null){
                if(propertyModel.action == "bestseller_listing" || propertyModel.action == "all_nearby_hotels" || propertyModel.action == "featuring_hotel" || propertyModel.action == "see_all_function_hotel"){
                    val intent = Intent(this@HotelShowAllItemFromHomeActivity, HotelBookingDetailHotelActivity::class.java)
                    intent.putExtra("hotel_name", propertyModel.name)
                    intent.putExtra("category", propertyModel.titleCategory)
                    intent.putExtra("hotel_image", propertyModel.imageUrl)
                    intent.putExtra("hotel_id", propertyModel.id)
                    intent.putExtra("price", propertyModel.priceVal)
                    resultLauncher.launch(intent)
                } else {
                    initStartSearch(false, propertyModel.id!!, propertyModel.name!!)
                }
            }
        }

        override fun onBookingWishlistClick(propertyModel: SubItemHomeModel?) {
            if (AppAlertCusDialog.isSuccessLoggedIn(this@HotelShowAllItemFromHomeActivity)) {
                RequestHashMapData.editStatusHotelWs(this@HotelShowAllItemFromHomeActivity, progressBar, propertyModel!!.id!!, propertyModel.isWishlist,
                    "wishlist", eazyhotelUserId, category, object : RequestHashMapData.Companion.OnSuccessEditStatusListener{
                        @SuppressLint("NotifyDataSetChanged")
                        override fun onSuccess(msg: String) {
                            propertyModel.isWishlist = !propertyModel.isWishlist
                            bookingHomeItemAdapter.notifyDataSetChanged()

                            setResult(RESULT_OK)
                        }
                    })
            }
        }
    }

    private fun initStartSearch(isClickSearch : Boolean, locationId : String, locationHotel : String){
        val value = String.format("%s ∙ %s - %s", locationHotel, "", "")
        val hashMapData : HashMap<String, Any> = HashMap()
        hashMapData["value"] = if(isClickSearch) value else locationHotel
        hashMapData["location_list"] = locationHotelModelList
        hashMapData["location_id"] = locationId
        hashMapData["location_title"] = locationHotel
        hashMapData["adults"] = ""
        hashMapData["room"] = ""
        hashMapData["children"] = ""
        hashMapData["start_date"] = ""
        hashMapData["end_date"] = ""
        hashMapData["guest_num"] = ""
        hashMapData["from_action"] = "hotel_location_detail"
        RequestHashMapData.storeHotelItemData(hashMapData)

        val intent = Intent(this, HotelTopDestinationDetailActivity::class.java)
        startActivity(intent)
    }

    private val resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
        if (result.resultCode == Activity.RESULT_OK) {
            //Call back when click wishlist hotel on detail screen
            refreshList()

            setResult(RESULT_OK)
        }
    }

    private fun onScrollInfoRecycle() {
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true
                }
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                currentItem = linearLayoutManager.childCount
                total = linearLayoutManager.itemCount
                scrollDown = linearLayoutManager.findFirstVisibleItemPosition()
                if (dy > 0) {
                    if (isScrolling && currentItem + scrollDown == total) {
                        if (total == size) {
                            isScrolling = false
                            currentPage++
                            progressBar.visibility = View.VISIBLE
                            recyclerView.scrollToPosition(subItemHomeModelList.size - 1)
                            requestServiceWs()
                            size += 10
                        } else {
                            progressBar.visibility = View.GONE
                        }
                    }
                }
            }
        })
    }

}