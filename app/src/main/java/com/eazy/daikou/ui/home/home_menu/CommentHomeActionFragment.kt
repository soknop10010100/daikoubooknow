package com.eazy.daikou.ui.home.home_menu

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AbsListView
import android.widget.ImageView
import android.widget.ProgressBar
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.base.BaseFragment
import com.eazy.daikou.base.MockUpData
import com.eazy.daikou.request_data.request.complain_ws.ComplaintListWS
import com.eazy.daikou.databinding.ActivityComplaintsSolutionBinding
import com.eazy.daikou.helper.AppAlertCusDialog
import com.eazy.daikou.helper.BroadcastTypes
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.complaint.ComplaintListModel
import com.eazy.daikou.ui.home.complaint_solution.complaint.ComplaintDetailActivity
import com.eazy.daikou.ui.home.complaint_solution.complaint.ComplaintSolutionCreateActivity
import com.eazy.daikou.ui.home.complaint_solution.complaint.adapter.ComplaintSolutionAdapter
import com.eazy.daikou.ui.home.evaluate.EvaluateUnitScanActivity
import com.eazy.daikou.ui.home.front_desk.FrontDeskCreateActivity
import libs.mjn.prettydialog.PrettyDialog

class CommentHomeActionFragment : BaseFragment() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var progressBar : ProgressBar

    private var currentItem = 0
    private var total: Int = 0
    private var scrollDown: Int = 0
    private var currentPage: Int = 1
    private var size: Int = 10
    private var isScrolling = false

    private var listType = "by_user"
    private var actionPostType = "all"
    private var accountId = ""
    private var accountName = ""

    private lateinit var adapterListComplaint: ComplaintSolutionAdapter
    private lateinit var layoutManager: LinearLayoutManager
    private var listDataListComplaint = ArrayList<ComplaintListModel>()

    private lateinit var btnCreate : ImageView

    // ViewBinding
    private lateinit var viewBinding : ActivityComplaintsSolutionBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        viewBinding = ActivityComplaintsSolutionBinding.inflate(layoutInflater)

        initView()

        initData()

        initAction()

        return viewBinding.root
    }

    private fun initView(){
        recyclerView = viewBinding.body.recyclerView.recyclerView
        progressBar = viewBinding.progressLayout.progressItem

        viewBinding.toolBar.titleToolbar.text = resources.getString(R.string.complaint_frontdesk_evaluate)
        viewBinding.toolBar.iconBack.visibility = View.INVISIBLE

        btnCreate = viewBinding.toolBar.addressMap
        btnCreate.setImageDrawable(ContextCompat.getDrawable(mActivity, R.drawable.fab_add))
        viewBinding.toolBar.layoutMap.visibility = View.VISIBLE

        viewBinding.createFrontDesk.visibility = View.GONE

        listType = if (Utils.isEmployeeRole(mActivity)) {
            "by_uploader"
        } else {
            "by_user"
        }

    }

    private fun initData() {
        val user = MockUpData.getUserItem(UserSessionManagement(mActivity))
        accountId = Utils.validateNullValue(user.accountId)
        accountName = Utils.validateNullValue(user.accountName)
    }

    private fun initAction() {

        layoutManager = LinearLayoutManager(mActivity)
        recyclerView.layoutManager = layoutManager

        initItemRecycle()

        initOnScrollItem()

        viewBinding.body.swipeLayouts.setColorSchemeResources(R.color.colorPrimary)
        viewBinding.body.swipeLayouts.setOnRefreshListener { this.onRefreshList() }

        btnCreate.setOnClickListener(CustomSetOnClickViewListener {
            AppAlertCusDialog.optionRequestDialog(mActivity, PrettyDialog(mActivity)) { action ->
                if (action == "frontdesk" && !Utils.isEmployeeRole(mActivity)){
                    AppAlertCusDialog.underConstructionDialog(mActivity, resources.getString(R.string.you_do_not_permission_to_access))
                } else {
                    startCreateComplaint(action)
                }
            }
        })

        // Register broadcast call back when create
        registerActionReceiver()
    }

    private fun initItemRecycle() {
        adapterListComplaint =
            ComplaintSolutionAdapter(actionPostType, listDataListComplaint, mActivity, onItemComplaintClick)
        recyclerView.adapter = adapterListComplaint

        requestServiceWs()
    }

    private val onItemComplaintClick = object : ComplaintSolutionAdapter.OnItemClick {
        override fun clickItem(complaintModel: ComplaintListModel) {
            val intent =
                Intent(mActivity, ComplaintDetailActivity::class.java)
            intent.putExtra("id", complaintModel.id)
            intent.putExtra("action_post", complaintModel.post_type)
            resultLauncher.launch(intent)
        }
    }

    private fun onRefreshList() {
        currentPage = 1
        size = 10
        isScrolling = true
        adapterListComplaint.clear()

        requestServiceWs()
    }

    private fun initOnScrollItem() {
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true
                }
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                currentItem = layoutManager.childCount
                total = layoutManager.itemCount
                scrollDown = layoutManager.findFirstVisibleItemPosition()
                if (dy > 0) {
                    if (isScrolling && currentItem + scrollDown == total) {
                        if (total == size) {
                            isScrolling = false
                            currentPage++
                            progressBar.visibility = View.VISIBLE
                            recyclerView.scrollToPosition(listDataListComplaint.size - 1)
                            initItemRecycle()
                            size += 10
                        } else {
                            progressBar.visibility = View.GONE
                        }
                    }
                }
            }
        })
    }

    private fun requestServiceWs() {
        progressBar.visibility = View.VISIBLE
        ComplaintListWS.getComplaintSolutionWs(
            mActivity,
            currentPage,
            10,
            listType,
            actionPostType,
            accountId,
            callBackListener
        )
    }

    private val callBackListener = object : ComplaintListWS.OnResponseList {
        @SuppressLint("NotifyDataSetChanged")
        override fun onSuccess(list: List<ComplaintListModel>) {
            progressBar.visibility = View.GONE
            viewBinding.body.swipeLayouts.isRefreshing = false
            listDataListComplaint.addAll(list)
            adapterListComplaint.notifyDataSetChanged()

            Utils.validateViewNoItemFound(
                mActivity,
                recyclerView,
                listDataListComplaint.size <= 0
            )
        }

        override fun onError(msg: String) {
            progressBar.visibility = View.GONE
            viewBinding.body.swipeLayouts.isRefreshing = false

            Utils.customToastMsgError(mActivity, msg, false)
        }
    }

    private val resultLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            if (result.resultCode == BaseActivity.RESULT_OK) {
                onRefreshList()
            }
        }

    private fun startCreateComplaint(action: String){
        val intent : Intent = when (action) {
            "evaluation" -> {
                Intent(mActivity, EvaluateUnitScanActivity::class.java)
            }
            "frontdesk" -> {
                Intent(mActivity, FrontDeskCreateActivity::class.java)
            }
            else -> {
                Intent(mActivity, ComplaintSolutionCreateActivity::class.java)
            }
        }

        intent.putExtra("object_type", "account")  // scan object type can be unit or space, now I static unit
        intent.putExtra("object_id", accountId) // scan account id can be id of unit or space, now I static 153
        intent.putExtra("account_id", accountId) // scan account id can be id of unit or space, now I static 153
        intent.putExtra("account_name", accountName) // scan account id can be id of unit or space, now I static 153

        intent.putExtra("action", action)
        resultLauncher.launch(intent)
    }

    private fun registerActionReceiver() {
        LocalBroadcastManager.getInstance(mActivity)
            .registerReceiver(mMessageReceiver, IntentFilter(BroadcastTypes.REFRESH_HOME_CREATE.name))
    }

    private val mMessageReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent?) {
            onRefreshList()
        }
    }

    private fun unRegisterActionReceiver(mActionReceiver: BroadcastReceiver) {
        LocalBroadcastManager.getInstance(mActivity).unregisterReceiver(mActionReceiver)
    }

    override fun onDestroy() {
        unRegisterActionReceiver(mMessageReceiver)
        super.onDestroy()
    }

}