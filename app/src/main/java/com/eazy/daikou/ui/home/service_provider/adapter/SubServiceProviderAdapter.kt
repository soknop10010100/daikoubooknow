package com.eazy.daikou.ui.home.service_provider.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.model.my_property.service_provider.SubCategoryServiceProviderV2Model

class SubServiceProviderAdapter (private val context: Context, private val listSubService : ArrayList<SubCategoryServiceProviderV2Model>,private val callBackSub: CallBackSubService): RecyclerView.Adapter<SubServiceProviderAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.layout_sub_category_service_provider, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data : SubCategoryServiceProviderV2Model = listSubService[position]
        if (data != null){
                if (data.logo != null) Glide.with(context).load(data.logo).into(holder.imgProfile);
                holder.nameItemHr.text = if (data.name != null) data.name else ("- - - ")
                holder.locationTv.text = if (data.address != null) data.address else ("- - - ")
                holder.itemView.setOnClickListener { callBackSub.callBackSubListener(data) }
        }
    }

    override fun getItemCount(): Int {
        return listSubService.size
    }

    class ViewHolder (view: View): RecyclerView.ViewHolder(view){
        var imgProfile: ImageView = view.findViewById(R.id.imgProfile)
        var nameItemHr: TextView = view.findViewById(R.id.name_item_hr)
        var locationTv: TextView = view.findViewById(R.id.descriptionTv)
    }

    interface CallBackSubService{
        fun callBackSubListener(subServiceModel: SubCategoryServiceProviderV2Model)
    }

    fun clear() {
        val size: Int = listSubService.size
        if (size > 0) {
            for (i in 0 until size) {
                listSubService.removeAt(0)
            }
            notifyItemRangeRemoved(0, size)
        }
    }
}