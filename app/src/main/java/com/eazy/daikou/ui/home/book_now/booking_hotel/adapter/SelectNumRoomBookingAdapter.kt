package com.eazy.daikou.ui.home.book_now.booking_hotel.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.booking_hotel.NumRoomBookingModel

class SelectNumRoomBookingAdapter(private val context : Context, private val list: List<NumRoomBookingModel>, private val onClickListener : OnClickCallBackLister) : RecyclerView.Adapter<SelectNumRoomBookingAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.select_number_of_room_booking, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item : NumRoomBookingModel = list[position]
        if (item != null){
            Utils.setValueOnText(holder.numVal, item.numValues)
            when (item.action) {
                "room" -> {
                    holder.selectLabelTv.text = Utils.getText(context, R.string.room)
                    Glide.with(holder.iconMenu).load(R.drawable.room).into(holder.iconMenu)
                }
                "adults" -> {
                    holder.selectLabelTv.text = Utils.getText(context, R.string.adults)
                    Glide.with(holder.iconMenu).load(R.drawable.profile_assign_work_order).into(holder.iconMenu)
                }
                else -> {
                    holder.selectLabelTv.text = Utils.getText(context, R.string.children)
                    Glide.with(holder.iconMenu).load(R.drawable.icon_children).into(holder.iconMenu)
                }
            }
            holder.line.visibility = if ((list.size - 1) == position) View.GONE else View.VISIBLE
            holder.add.setOnClickListener { onClickListener.onClickCallBack(item, "add") }
            holder.minus.setOnClickListener { onClickListener.onClickCallBack(item, "minus") }
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val selectLabelTv : TextView = itemView.findViewById(R.id.selectLabelTv)
        val iconMenu : ImageView = itemView.findViewById(R.id.iconMenu)
        val minus : ImageView = itemView.findViewById(R.id.minus)
        val add : ImageView = itemView.findViewById(R.id.add)
        val numVal : TextView = itemView.findViewById(R.id.numVal)
        val line : View = itemView.findViewById(R.id.line)
    }

    interface OnClickCallBackLister{
        fun onClickCallBack(value : NumRoomBookingModel, actionBtn : String)
    }
}