package com.eazy.daikou.ui.home.home_menu.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.model.home_page.PropertyListing
import com.squareup.picasso.Picasso

class PropertyListBuySellRentAdapter(
    private val listBuySellRent:List<PropertyListing>,
    private val context: Context,
    private val itemClick: OnClickItem
) :RecyclerView.Adapter<PropertyListBuySellRentAdapter.ViewHolder>(){


    inner class ViewHolder(itemView:View):RecyclerView.ViewHolder(itemView)
    {
        //card 1
        val layoutImage1 = itemView.findViewById<View>(R.id.layoutImage)
        val imageView1   = itemView.findViewById<ImageView>(R.id.image)
        val textTitle1 = itemView.findViewById<TextView>(R.id.property_name)
        val textAddress1 = itemView.findViewById<TextView>(R.id.buildingAddress)
        val textPrice1 = itemView.findViewById<TextView>(R.id.buildingPrice)

        //card 2
        val layoutImage2 = itemView.findViewById<View>(R.id.layoutImage1)
        val imageView2   = itemView.findViewById<ImageView>(R.id.image1)
        val textTitle2 = itemView.findViewById<TextView>(R.id.property_name1)
        val textAddress2 = itemView.findViewById<TextView>(R.id.buildingAddress1)
        val textPrice2 = itemView.findViewById<TextView>(R.id.buildingPrice1)

        //card 3
        val layoutImage3 = itemView.findViewById<View>(R.id.layoutImage2)
        val imageView3   = itemView.findViewById<ImageView>(R.id.image2)
        val textTitle3 = itemView.findViewById<TextView>(R.id.property_name2)
        val textAddress3 = itemView.findViewById<TextView>(R.id.buildingAddress2)
        val textPrice3 = itemView.findViewById<TextView>(R.id.buildingPrice2)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).
            inflate(R.layout.custom_item_property_buy_sell_rent_home_v2,parent,false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        when(position){
            0->{
                setViewFirstCard(holder,0)
                setViewSecondCard(holder,1)
                setViewThirdCard(holder,2)
            }

            1->
            {
                setViewFirstCard(holder,3)
                setViewSecondCard(holder,4)
                setViewThirdCard(holder,5)
            }

            2->
            {
                setViewFirstCard(holder,6)
                setViewSecondCard(holder,7)
                setViewThirdCard(holder,8)
            }
            3->
            {
                setViewFirstCard(holder,9)
                setViewSecondCard(holder,10)
                setViewThirdCard(holder,11)
            }
            4->
            {
                setViewFirstCard(holder,12)
                setViewSecondCard(holder,13)
                setViewThirdCard(holder,14)
            }
        }
    }

    private fun setViewFirstCard(holder: ViewHolder, position: Int){
        //set image
        if (listBuySellRent[position].image!=null && listBuySellRent[position].image!="")
        {
            Picasso.get().load(listBuySellRent[position].image).into(holder.imageView1)
        }
        else
        {
            holder.imageView1.setImageResource(R.drawable.no_image)
        }

        //set title
        if (listBuySellRent[position].name!=null && listBuySellRent[position].name!="")
        {
            holder.textTitle1.text = listBuySellRent[position].name
        }
        else
        {
            holder.textTitle1.text= context.getString(R.string.not_available)
        }

        //set address
        if (listBuySellRent[position].address!=null && listBuySellRent[position].address!="")
        {
            holder.textAddress1.text = listBuySellRent[position].address
        }
        else
        {
            holder.textAddress1.text = context.getString(R.string.not_available)
        }

        holder.textPrice1.text = listBuySellRent[position].priceDisplay

        holder.layoutImage1.setOnClickListener {
            itemClick.onItemClick(listBuySellRent[position])
        }
    }

    private fun setViewSecondCard(holder: ViewHolder, position: Int){
        //set image
        if (listBuySellRent[position].image!=null && listBuySellRent[position].image!="")
        {
            Picasso.get().load(listBuySellRent[position].image).into(holder.imageView2)
        }
        else
        {
            holder.imageView2.setImageResource(R.drawable.no_image)
        }

        //set title
        if (listBuySellRent[position].name!=null && listBuySellRent[position].name!="")
        {
            holder.textTitle2.text = listBuySellRent[position].name
        }
        else
        {
            holder.textTitle2.text= context.getString(R.string.not_available)
        }

        //set address
        if (listBuySellRent[position].address!=null && listBuySellRent[position].address!="")
        {
            holder.textAddress2.text = listBuySellRent[position].address
        }
        else
        {
            holder.textAddress2.text = context.getString(R.string.not_available)
        }

        holder.textPrice2.text = listBuySellRent[position].priceDisplay

        holder.layoutImage2.setOnClickListener {
            itemClick.onItemClick(listBuySellRent[position])
        }
    }

    private fun setViewThirdCard(holder: ViewHolder, position: Int){
        //set image
        if (listBuySellRent[position].image!=null && listBuySellRent[position].image!="")
        {
            Picasso.get().load(listBuySellRent[position].image).into(holder.imageView3)
        }
        else
        {
            holder.imageView3.setImageResource(R.drawable.no_image)
        }

        //set title
        if (listBuySellRent[position].name!=null && listBuySellRent[position].name!="")
        {
            holder.textTitle3.text = listBuySellRent[position].name
        }
        else
        {
            holder.textTitle3.text= context.getString(R.string.not_available)
        }

        //set address
        if (listBuySellRent[position].address!=null && listBuySellRent[position].address!="")
        {
            holder.textAddress3.text = listBuySellRent[position].address
        }
        else
        {
            holder.textAddress3.text = context.getString(R.string.not_available)
        }

        holder.textPrice3.text = listBuySellRent[position].priceDisplay

        holder.layoutImage3.setOnClickListener {
            itemClick.onItemClick(listBuySellRent[position])
        }
    }
    override fun getItemCount(): Int {
       return listBuySellRent.size/3
    }

    interface OnClickItem{
        fun onItemClick(data:PropertyListing)
    }
}