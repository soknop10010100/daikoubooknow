package com.eazy.daikou.ui.home.book_now.booking_hotel.visit

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.RelativeLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.viewpager2.widget.ViewPager2
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseFragment
import com.eazy.daikou.base.MockUpData
import com.eazy.daikou.request_data.request.book_now_ws.BookingVisitWS
import com.eazy.daikou.helper.AbsoluteFitLayoutManager
import com.eazy.daikou.helper.CustomSliderImageClass
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.home.MainItemHomeModel
import com.eazy.daikou.model.home.SubItemHomeModel
import com.eazy.daikou.model.work_order.ImageListModel
import com.eazy.daikou.ui.home.book_now.booking_hotel.adapter.AdapterImageSlider
import com.eazy.daikou.ui.home.book_now.booking_hotel.adapter.BookingHomeItemAdapter
import com.eazy.daikou.ui.home.book_now.booking_hotel.adapter.HotelCategoryItemAdapter
import com.eazy.daikou.ui.home.home_menu.adapter.MainHomeShowItemAdapter
import com.google.android.material.appbar.AppBarLayout
import kotlin.math.abs

class HotelVisitShowFragment : BaseFragment() {

    private lateinit var layoutImage : RelativeLayout

    // image slider
    private lateinit var viewPager2: ViewPager2
    private val sliderHandler: Handler = Handler(Looper.getMainLooper())

    private lateinit var recyclerViewCategory : RecyclerView

    private lateinit var recyclerViewItem : RecyclerView
    private lateinit var mainHomeShowItemAdapter : MainHomeShowItemAdapter
    private var mainItemAllHotelList: ArrayList<MainItemHomeModel> = ArrayList()
    private var eazyhotelUserId = ""
    private lateinit var swipeRefreshLayout : SwipeRefreshLayout
    private lateinit var appBar : AppBarLayout
    private lateinit var progressBar: ProgressBar

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_hotel_visit_show, container, false)

        initView(view)

        initAction()

        return view
    }

    private fun initView(view: View){
        layoutImage = view.findViewById(R.id.layoutImage)
        viewPager2 = view.findViewById(R.id.viewPagerImageSlider)
        recyclerViewCategory = view.findViewById(R.id.recyclerViewCategory)
        recyclerViewItem = view.findViewById(R.id.recyclerViewItem)
        swipeRefreshLayout = view.findViewById(R.id.swipe_layout)
        appBar = view.findViewById(R.id.appBarLayout)
        progressBar = view.findViewById(R.id.progressItem)
    }

    private fun initAction(){
        // Set height on image slide
        layoutImage.layoutParams.height = Utils.setHeightOfScreen( mActivity, 4.0)
        eazyhotelUserId = Utils.validateNullValue(MockUpData.getEazyHotelUserId(UserSessionManagement(mActivity)))

        initItemRecyclerView()

        initRefreshLayout()
    }

    private fun initRefreshLayout(){
        val start = resources.getDimensionPixelSize(R.dimen.indicator_start)
        val end = resources.getDimensionPixelSize(R.dimen.indicator_end)
        swipeRefreshLayout.setColorSchemeResources(R.color.appBarColorOld)
        swipeRefreshLayout.setProgressViewOffset(true, start, end)
        swipeRefreshLayout.setOnRefreshListener {
            refreshList()
        }

        //set swipe for only expanded of app bar
        appBar.addOnOffsetChangedListener(AppBarLayout.BaseOnOffsetChangedListener<AppBarLayout> { appBarLayout, verticalOffset ->
            if (abs(verticalOffset) == appBarLayout.totalScrollRange) {
                // Collapsed
                swipeRefreshLayout.isEnabled = false
            } else swipeRefreshLayout.isEnabled = verticalOffset == 0
        })
    }

    // Category Visit
    private fun initRecyclerViewCategory(subHomeModelCategoryList: ArrayList<SubItemHomeModel>){
        recyclerViewCategory.isNestedScrollingEnabled = false
        recyclerViewCategory.layoutManager =   AbsoluteFitLayoutManager(mActivity, 1, RecyclerView.HORIZONTAL, false, 5)
        val bookingHomeItemAdapter = HotelCategoryItemAdapter(true, subHomeModelCategoryList, object : BookingHomeItemAdapter.PropertyClick{
            @SuppressLint("NotifyDataSetChanged")
            override fun onBookingClick(propertyModel: SubItemHomeModel?) {
                if (propertyModel!!.id != null){
                    startNewActivity(propertyModel.id!!, propertyModel.name!!, "menu_category", HotelVisitByLocationActivity::class.java)
                }
            }

            override fun onBookingWishlistClick(propertyModel: SubItemHomeModel?) {}

        })
        recyclerViewCategory.adapter = bookingHomeItemAdapter
    }

    // recyclerview item
    private fun initItemRecyclerView(){
        recyclerViewItem.layoutManager = LinearLayoutManager(mActivity)

        mainHomeShowItemAdapter = MainHomeShowItemAdapter("booking_visit", mainItemAllHotelList, mActivity, onClickItemLister)
        recyclerViewItem.adapter = mainHomeShowItemAdapter

        requestItemHotelList()
    }

    private fun refreshList(){
        mainHomeShowItemAdapter.clear()
        requestItemHotelList()
    }

    private val onClickItemLister = object : MainHomeShowItemAdapter.PropertyClick{
        @SuppressLint("NotifyDataSetChanged")
        override fun onClickCallBackListener(actionWishlist: String, subItemHomeModel: SubItemHomeModel) {
            startVisitDetail(subItemHomeModel.id!!, subItemHomeModel.name!!, subItemHomeModel.action!!)
        }

        override fun onClickSeeAllCallBackListener(action: String , actionType : String, subItemHomeModel: ArrayList<*>) {}
    }

    private fun requestItemHotelList(){
        progressBar.visibility = View.VISIBLE
        BookingVisitWS().getHomePageVisitAllWs(mActivity, onCallBackItemListener)
    }

    private val onCallBackItemListener = object : BookingVisitWS.OnCallBackHomePageItemListener{
        @SuppressLint("NotifyDataSetChanged")
        override fun onSuccess(listImageSlider: ArrayList<ImageListModel>, subHomeModelCategoryList: ArrayList<SubItemHomeModel>, mainItemHomeModelLIst: ArrayList<MainItemHomeModel>) {
            progressBar.visibility = View.GONE
            swipeRefreshLayout.isRefreshing = false
            mainItemAllHotelList.addAll(mainItemHomeModelLIst)
            mainHomeShowItemAdapter.notifyDataSetChanged()

            // Category visit
            initRecyclerViewCategory(subHomeModelCategoryList)

            // Slider visit
            initSliderImage(listImageSlider)
        }

        override fun onFailed(message: String) {
            progressBar.visibility = View.GONE
            swipeRefreshLayout.isRefreshing = false
            Utils.customToastMsgError(mActivity, message, false)
        }

    }

    private fun startVisitDetail(id : String, name : String, action : String){
        if (action == "visits_sub"){
            startNewActivity(id, name, "visits_place", HotelVisitByLocationActivity::class.java)
        } else {
            startNewActivity(id, name, action, HotelVisitDetailActivity::class.java)
        }
    }

    private fun initSliderImage(slideModelList: ArrayList<ImageListModel>){
        CustomSliderImageClass.initImageSlideViewPager(55, viewPager2, sliderHandler, sliderRunnable, slideModelList, ArrayList(), object : AdapterImageSlider.OnClickItemListener{
            override fun onClickCallBack(sliderItems: ImageListModel) {
                startNewActivity(sliderItems.keyImage, "", "advertise_banner", HotelVisitDetailActivity::class.java)
            }
        })
    }

    private fun startNewActivity(id: String, name : String, action: String, toClass: Class<*>){
        val intent = Intent(mActivity, toClass).apply {
            putExtra("id", id)
            putExtra("name", name)
            putExtra("action", action)
        }
        startActivity(intent)
    }

    private val sliderRunnable = Runnable { viewPager2.currentItem = viewPager2.currentItem + 1 }

    override fun onPause() {
        super.onPause()
        if (sliderHandler != null)  sliderHandler.removeCallbacks(sliderRunnable)
    }

    override fun onResume() {
        super.onResume()
        if (sliderHandler != null) sliderHandler.postDelayed(sliderRunnable, 3000)
    }
}