package com.eazy.daikou.ui.home.service_provider_new.activity

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.CalendarView
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.ui.home.service_provider.AddressServiceProviderActivity
import com.eazy.daikou.ui.home.service_provider_new.adapter_service_provider.ListItemMyCartAdapter
import com.eazy.daikou.ui.home.service_provider_new.fragment_service_provider.ItemDeliveryFragment
import java.text.SimpleDateFormat
import java.util.*

class MyCartProviderActivity : BaseActivity() {

    private lateinit var selectAddressTv: TextView
    private lateinit var dateTimeTv: TextView
    private lateinit var deliveryMethodTv: TextView
    private lateinit var paymentMethodTv: TextView
    private lateinit var recyclerListItem: RecyclerView
    private lateinit var lastSelectedCalendar : Calendar
    private lateinit var mainDeliveryMethod : RelativeLayout
    private lateinit var mainPaymentMethod : RelativeLayout
    private lateinit var titleToolbar : TextView
    private lateinit var btnAddToCart : TextView

    private var addressLocation = ""
    private var actionService = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_cart_provider)

        initView()

        initData()

        initAction()
    }

    private fun initView(){
        selectAddressTv = findViewById(R.id.selectAddressBtn)
        dateTimeTv = findViewById(R.id.dateTimeTv)
        deliveryMethodTv = findViewById(R.id.deliveryMethodTv)
        paymentMethodTv = findViewById(R.id.paymentMethodTv)
        recyclerListItem = findViewById(R.id.recyclerListItem)

        mainDeliveryMethod = findViewById(R.id.mainDeliveryMethod)
        mainPaymentMethod = findViewById(R.id.mainPaymentMethod)
        titleToolbar = findViewById(R.id.titleToolbar)
        btnAddToCart = findViewById(R.id.btnAddToCart)
    }

    private fun initData(){
        if (intent != null && intent.hasExtra("action_service")){
            actionService = intent.getStringExtra("action_service") as String
        }
    }

    private fun initAction(){
        findViewById<ImageView>(R.id.iconBack).setOnClickListener { finish() }
        selectAddressTv.setOnClickListener(
            CustomSetOnClickViewListener {
                val intent = Intent(this@MyCartProviderActivity, AddressServiceProviderActivity::class.java)
                resultLauncher.launch(intent) })
        dateTimeTv.setOnClickListener(CustomSetOnClickViewListener{ calendarDate() })

        deliveryMethodTv.setOnClickListener(CustomSetOnClickViewListener{
            getFragmentDeliveryMethod("delivery_method") })
        paymentMethodTv.setOnClickListener( CustomSetOnClickViewListener{
            getFragmentDeliveryMethod("payment_method") })
        initTypeProduct()

        initViewListItem()
    }
    private fun  initTypeProduct(){
        if (actionService == "service"){
            mainDeliveryMethod.visibility = View.GONE
            mainPaymentMethod.visibility = View.GONE
            titleToolbar.text = "Booking Service"
        } else {
            mainDeliveryMethod.visibility = View.VISIBLE
            mainPaymentMethod.visibility = View.VISIBLE
            titleToolbar.text = "My Cart"
        }
    }

    private fun getFragmentDeliveryMethod(typeSelect : String){
        val bttSheet = ItemDeliveryFragment().newInstance(typeSelect)
        bttSheet.show(supportFragmentManager, "type_select_delivery")
    }
    private fun initViewListItem(){
        val linearLayoutManager = LinearLayoutManager(this)
        recyclerListItem.layoutManager = linearLayoutManager
        val adapterMyCart = ListItemMyCartAdapter(this)
        recyclerListItem.adapter = adapterMyCart
    }
    private fun calendarDate(){
        lastSelectedCalendar = Calendar.getInstance()
        val alertLayout = LayoutInflater.from(this).inflate(R.layout.activity_calendar_view_dialog, null)
        val textTitle = alertLayout.findViewById<TextView>(R.id.txtTitle)
        val dialog = AlertDialog.Builder(this, R.style.AlertShape)
        dialog.setView(alertLayout)
        dialog.setCancelable(false)
        textTitle.text = getString(R.string.date_of_overtime)
        val alertDialog = dialog.show()

        alertLayout.findViewById<ImageView>(R.id.btnClose).setOnClickListener{alertDialog.dismiss()}
        val calendarViewAlert: CalendarView = alertLayout.findViewById(R.id.date_picker)

        // Set Select Date
        if (lastSelectedCalendar != null)     calendarViewAlert.setDate (lastSelectedCalendar.timeInMillis, true, true)

        calendarViewAlert.setOnDateChangeListener { _: CalendarView, year: Int, month: Int, dayOfMonth: Int ->
            val checkCalendar = Calendar.getInstance()
            checkCalendar[year, month] = dayOfMonth

            val calendar = Calendar.getInstance()
            calendar[year, month] = dayOfMonth

            lastSelectedCalendar = calendar
            val formattedDate = SimpleDateFormat("dd-MMM-yyyy")
            dateTimeTv.text =formattedDate.format(calendar.time)

            alertDialog.dismiss()
        }
        calendarViewAlert.minDate = System.currentTimeMillis() - 1000

    }


    private var resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()){ result ->
        if (result.resultCode == Activity.RESULT_OK){
                if (result.data!!.hasExtra("address_location")|| result.data!!.hasExtra("lat")|| result.data!!.hasExtra("lng")){
                    val address = result.data!!.getStringExtra("address_location")
//                    val latLocation = data.getStringExtra("lat")
//                    val longLocation = data.getStringExtra("lng")
                    addressLocation = address.toString()
                    selectAddressTv.text = addressLocation
                }
            }
    }
}
