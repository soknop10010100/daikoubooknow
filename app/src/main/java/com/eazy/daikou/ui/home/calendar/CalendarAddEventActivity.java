package com.eazy.daikou.ui.home.calendar;

import androidx.annotation.Nullable;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.eazy.daikou.R;
import com.eazy.daikou.base.BaseActivity;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class CalendarAddEventActivity extends BaseActivity implements View.OnClickListener {

    private static final int REPEAT_CODE = 510;
    private static final int ALERT_CODE = 515;
    private static final int SHOWAS_CODE = 520;
    private TextView txtCancel, txtAdd;
    private LinearLayout titleEvent, locationEvent, startDateTime, endDateTime, repeatLayout, inviteesLayout, alertLayout, showAsLayout;
    private LinearLayout addAttachmentLayout, noteEvent;
    private TextView startDate, startTime , endDate, endTime;
    private TextView repeatValTxt , alertValTxt , inviteeTxt , showAsTxt ,addAttachmentTxt;
    private String formattedDate , formattedTime;
    private EditText titleEdt, locationEdt , noteEdt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar_add_event);

        initView();
        initAction();

    }

    private void initView() {
        txtCancel  = findViewById(R.id.titleCancel);
        txtAdd = findViewById(R.id.AddEventBtn);

        titleEvent = findViewById(R.id.titleLayout);
        locationEvent = findViewById(R.id.locationLayout);
        startDateTime = findViewById(R.id.startTimeLayout);
        endDateTime = findViewById(R.id.endTimeLayout);
        repeatLayout = findViewById(R.id.repeatLayout);
        inviteesLayout = findViewById(R.id.inviteeLayout);
        alertLayout = findViewById(R.id.alertLayout);
        showAsLayout = findViewById(R.id.showAsLayout);
        addAttachmentLayout = findViewById(R.id.addAttachmentLayout);
        noteEvent = findViewById(R.id.writeNoteLayout);

        startDate = findViewById(R.id.startDateTxt);
        startTime = findViewById(R.id.startTimeTxt);
        endDate = findViewById(R.id.endDateTxt);
        endTime = findViewById(R.id.endTimeTxt);
        repeatValTxt = findViewById(R.id.repeatValTxt);
        alertValTxt = findViewById(R.id.alertValTxt);
        inviteeTxt = findViewById(R.id.inviteValTxt);
        showAsTxt = findViewById(R.id.showAsValTxt);
        addAttachmentTxt = findViewById(R.id.addAttachmentValTxt);

        titleEdt = findViewById(R.id.titleEt);
        locationEdt = findViewById(R.id.locationEt);
        noteEdt = findViewById(R.id.special_instruction);
    }

    private void initAction(){
        txtCancel.setOnClickListener(view -> onBackPressed());
        txtAdd.setOnClickListener(view -> Toast.makeText(getApplicationContext(), getString(R.string.under_construction),Toast.LENGTH_SHORT).show());

        startTDateTimeCalendar();
        endTDateTimeCalendar();

        startDateTime.setOnClickListener(this);
        endDateTime.setOnClickListener(this);
        repeatLayout.setOnClickListener(this);
        alertLayout.setOnClickListener(this);
        inviteesLayout.setOnClickListener(this);
        showAsLayout.setOnClickListener(this);
        addAttachmentLayout.setOnClickListener(this);

    }

    private void startTDateTimeCalendar(){
        formattedDateTime();
        startDate.setText(formattedDate);
        startTime.setText(formattedTime);
    }
    private void endTDateTimeCalendar(){
        formattedDateTime();
        endDate.setText(formattedDate);
        endTime.setText(formattedTime);
    }
    private void formattedDateTime(){
        //format date
        Calendar cal = Calendar.getInstance();
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy" , Locale.getDefault());
        formattedDate = df.format(cal.getTime());

        //format time
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat tf = new SimpleDateFormat("h:mm a" , Locale.getDefault());
        formattedTime = tf.format(cal.getTime());
    }
    private void CalendarDialogPicker(int i){
        final Calendar calendar = Calendar.getInstance();
        int yy = calendar.get(Calendar.YEAR);
        int mm = calendar.get(Calendar.MONTH);
        int dd = calendar.get(Calendar.DAY_OF_MONTH);
        formattedDateTime();
        DatePickerDialog datePicker = new DatePickerDialog(this, (view, year, monthOfYear, dayOfMonth) -> {
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, monthOfYear);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            int hour = calendar.get(Calendar.HOUR_OF_DAY);
            int minute = calendar.get(Calendar.MINUTE);

            //Load to set time
            TimePickerDialog mTimePicker = new TimePickerDialog(CalendarAddEventActivity.this, (timePicker, selectedHour, selectedMinute) -> {
                calendar.set(Calendar.HOUR_OF_DAY,selectedHour);
                calendar.set(Calendar.MINUTE,selectedMinute);

                SimpleDateFormat tf = new SimpleDateFormat("h:mm a" , Locale.getDefault());
                if (i == 0)
                    startTime.setText(tf.format(calendar.getTime()));
                else if (i == 1)
                    endTime.setText(tf.format(calendar.getTime()));
            }, hour, minute, true);
            mTimePicker.show();

            String dateFormat = "dd MMM yyyy";
            SimpleDateFormat df = new SimpleDateFormat(dateFormat , Locale.getDefault());
            if (i == 0)
                startDate.setText(df.format(calendar.getTime()));
            else if (i == 1)
                endDate.setText(df.format(calendar.getTime()));
        }, yy, mm, dd);
        datePicker.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePicker.show();
    }
    @Override
    public void onClick(View view) {
        int idView = view.getId();
        if (idView == R.id.startTimeLayout){
            CalendarDialogPicker(0);
        } else if (idView == R.id.endTimeLayout){
            CalendarDialogPicker(1);
        } else if (idView == R.id.repeatLayout){
            Intent intent = new Intent(CalendarAddEventActivity.this, RepeatActivity.class);
            intent.putExtra("repeat_alert", REPEAT_CODE);
            startActivityForResult(intent , REPEAT_CODE);
        } else if (idView == R.id.alertLayout){
            Intent intent = new Intent(CalendarAddEventActivity.this, RepeatActivity.class);
            intent.putExtra("repeat_alert",ALERT_CODE);
            startActivityForResult(intent , ALERT_CODE);
        } else if (idView == R.id.inviteeLayout){
            Toast.makeText(getApplicationContext(), getString(R.string.under_construction),Toast.LENGTH_SHORT).show();
        } else if (idView == R.id.showAsLayout){
            Intent intent = new Intent(CalendarAddEventActivity.this, RepeatActivity.class);
            intent.putExtra("repeat_alert",SHOWAS_CODE);
            startActivityForResult(intent , SHOWAS_CODE);
        } else if (idView == R.id.addAttachmentLayout){
            Toast.makeText(getApplicationContext(), getString(R.string.under_construction),Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && data != null){
            if ( requestCode == REPEAT_CODE ){
                //  Repeat Event
                if (data.hasExtra("never_key")) {
                    String neverStringBack = data.getStringExtra("never_key");
                    repeatValTxt.setText(neverStringBack);
                } else if (data.hasExtra("everyday_key")) {
                    String everyDayStringBack = data.getStringExtra("everyday_key");
                    repeatValTxt.setText(everyDayStringBack);
                } else if (data.hasExtra("everyWeek_key")) {
                    String everyWeekStringBack = data.getStringExtra("everyWeek_key");
                    repeatValTxt.setText(everyWeekStringBack);
                } else if (data.hasExtra("everyMonth_key")) {
                    String everyMonthStringBack = data.getStringExtra("everyMonth_key");
                    repeatValTxt.setText(everyMonthStringBack);
                }  else if (data.hasExtra("everyYear_key")) {
                    String everyYearStringBack = data.getStringExtra("everyYear_key");
                    repeatValTxt.setText(everyYearStringBack);
                }
            //  Alert Event
            } else if (requestCode == ALERT_CODE ){
                if (data.hasExtra("never_key")) {
                    String alertStringBack = data.getStringExtra("never_key");
                    alertValTxt.setText(alertStringBack);
                } else if (data.hasExtra("everyday_key")) {
                    String alertStringBack = data.getStringExtra("everyday_key");
                    alertValTxt.setText(alertStringBack);
                } else if (data.hasExtra("everyWeek_key")) {
                    String alertStringBack = data.getStringExtra("everyWeek_key");
                    alertValTxt.setText(alertStringBack);
                } else if (data.hasExtra("everyMonth_key")) {
                    String alertStringBack = data.getStringExtra("everyMonth_key");
                    alertValTxt.setText(alertStringBack);
                }  else if (data.hasExtra("everyYear_key")) {
                    String alertStringBack = data.getStringExtra("everyYear_key");
                    alertValTxt.setText(alertStringBack);
                } else if (data.hasExtra("alert30mn_key")) {
                    String alertStringBack = data.getStringExtra("alert30mn_key");
                    alertValTxt.setText(alertStringBack);
                } else if (data.hasExtra("alert30mn_key")) {
                    String alertStringBack = data.getStringExtra("alert30mn_key");
                    alertValTxt.setText(alertStringBack);
                } else if (data.hasExtra("alert1hr_key")) {
                    String alertStringBack = data.getStringExtra("alert1hr_key");
                    alertValTxt.setText(alertStringBack);
                }
            //  Show As
            } else if (requestCode == SHOWAS_CODE){
                if (data.hasExtra("never_key")) {
                    String busyStringBack = data.getStringExtra("never_key");
                    showAsTxt.setText(busyStringBack);
                } else if (data.hasExtra("everyday_key")) {
                    String freeStringBack = data.getStringExtra("everyday_key");
                    showAsTxt.setText(freeStringBack);
                }
            }
        }
    }
}