package com.eazy.daikou.ui.home.create_case_sale_and_rent.sale_and_rent

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.request_data.request.buy_sell_ws.BuySaleRentWs
import com.eazy.daikou.helper.AppAlertCusDialog
import com.eazy.daikou.helper.DateUtil
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.buy_sell.DetailBuyAndSaleRentModel
import com.google.android.material.tabs.TabLayout
import de.hdodenhof.circleimageview.CircleImageView

class DetailListSaleRentActivity : BaseActivity() {

    private lateinit var progressBarItem: ProgressBar
    private lateinit var btnBack: ImageView
    private lateinit var btnCallPhone: ImageView
    private lateinit var usernameTv: TextView
    private lateinit var phoneTv: TextView
    private lateinit var propertyTypeTv: TextView
    private lateinit var priceTv: TextView
    private lateinit var locationTv: TextView
    private lateinit var bedRoomTv: TextView
    private lateinit var bathRoomTv: TextView
    private lateinit var descriptionTv: TextView
    private lateinit var titleTv: TextView
    private var idDetail = ""
    private lateinit var viewPager: ViewPager
    private lateinit var tabLayout : TabLayout
    private lateinit var imageEmployee : CircleImageView
    private lateinit var widthTv : TextView
    private lateinit var lengthTv : TextView
    private lateinit var sizeTv : TextView
    private lateinit var createDateTv : TextView
    private lateinit var contactTv : TextView
    private lateinit var layoutBathRoom : LinearLayout
    private lateinit var layoutBedRoom : LinearLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_list_sale_rent)

        initView()

        initData()

        initAction()

    }

    private fun initView(){
        layoutBedRoom = findViewById(R.id.layoutBedRoom)
        layoutBathRoom = findViewById(R.id.layoutBathRoom)
        progressBarItem = findViewById(R.id.progressItem)
        btnBack = findViewById(R.id.btnBack)
        titleTv = findViewById(R.id.titleTv)
        btnCallPhone = findViewById(R.id.btnCallPhone)
        usernameTv = findViewById(R.id.usernameTv)
        phoneTv = findViewById(R.id.phoneTv)
        propertyTypeTv = findViewById(R.id.propertyTypeTv)
        priceTv = findViewById(R.id.priceTv)
        locationTv = findViewById(R.id.locationTv)
        bedRoomTv = findViewById(R.id.bedRoomTv)
        bathRoomTv = findViewById(R.id.bathRoomTv)
        descriptionTv = findViewById(R.id.descriptionTv)
        viewPager = findViewById(R.id.viewPagers)
        tabLayout = findViewById(R.id.tab_layout)
        imageEmployee = findViewById(R.id.imageEmployee)
        widthTv = findViewById(R.id.widthTv)
        lengthTv = findViewById(R.id.lengthTv)
        sizeTv = findViewById(R.id.sizeRoomTv)
        createDateTv = findViewById(R.id.createDateTv)
        contactTv = findViewById(R.id.contactTv)
    }

    private fun initData(){
        if (intent != null && intent.hasExtra("id_list_detail")){
            idDetail = intent.getStringExtra("id_list_detail") as String
        }
    }

    private fun initAction(){
        findViewById<ImageView>(R.id.btnBack).setOnClickListener { finish() }

        requestServiceDetailAPI()
    }

    private fun initTextView(detailBuySaleAndRent: DetailBuyAndSaleRentModel){
        Utils.setValueOnText(titleTv,detailBuySaleAndRent.title)
        Utils.setValueOnText(usernameTv, detailBuySaleAndRent.uploader_user_name)
        Utils.setValueOnText(phoneTv, detailBuySaleAndRent.uploader_user_phone)
        Utils.setValueOnText(priceTv, detailBuySaleAndRent.price)
        Utils.setValueOnText(locationTv, detailBuySaleAndRent.address)
        Utils.setValueOnText(bedRoomTv, detailBuySaleAndRent.total_bedrooms)
        Utils.setValueOnText(bathRoomTv, detailBuySaleAndRent.total_bathrooms)
        Utils.setValueOnText(descriptionTv, detailBuySaleAndRent.description)
        Utils.setValueOnText(widthTv, detailBuySaleAndRent.width)
        Utils.setValueOnText(lengthTv, detailBuySaleAndRent.length)
        Utils.setValueOnText(sizeTv, detailBuySaleAndRent.size)
        Utils.setValueOnText(contactTv, detailBuySaleAndRent.phone_no)

        Glide.with(imageEmployee.context).load(if (detailBuySaleAndRent.uploader_user_profile_image != null) detailBuySaleAndRent.uploader_user_profile_image else R.drawable.ic_my_profile).into(imageEmployee)
        Utils.displayImageMultiple(viewPager, tabLayout, detailBuySaleAndRent.images)

        if (detailBuySaleAndRent.created_dt != null){
            createDateTv.text = String.format("%s %s", DateUtil.formatDateComplaint(detailBuySaleAndRent.created_dt) ,DateUtil.formatDateToConvertOnlyTimeZoneNoSecond(detailBuySaleAndRent.created_dt))
        } else {
            createDateTv.text = "- - -"
        }

        if (detailBuySaleAndRent.total_bedrooms == null) layoutBedRoom.visibility = View.GONE
        if (detailBuySaleAndRent.total_bathrooms == null) layoutBathRoom.visibility = View.GONE

        if (detailBuySaleAndRent.property_type != null){
            when (detailBuySaleAndRent.property_type) {
                "land" -> {
                    propertyTypeTv.text = resources.getString(R.string.land)
                }
                "house" -> {
                    propertyTypeTv.text = resources.getString(R.string.house)
                }
                "building" -> {
                    propertyTypeTv.text = resources.getString(R.string.building)
                }
            }
        }

        //Click btn call
        btnCallPhone.setOnClickListener(
            CustomSetOnClickViewListener{
                Utils.customAlertDialogConfirm(this, detailBuySaleAndRent.phone_no){
                    AppAlertCusDialog.alertPhoneCall(this, detailBuySaleAndRent.phone_no)
                }
            }
        )

    }

    private fun requestServiceDetailAPI(){
        progressBarItem.visibility = View.VISIBLE
        BuySaleRentWs().getListDetailSaleAndRent(this,idDetail, callBackDetailLister)
    }

    private val callBackDetailLister : BuySaleRentWs.OnDetailCallBackListener = object : BuySaleRentWs.OnDetailCallBackListener{
        override fun onSuccess(detailBuySaleAndRentModel: DetailBuyAndSaleRentModel) {
            progressBarItem.visibility = View.GONE
            if (detailBuySaleAndRentModel != null) {
                initTextView(detailBuySaleAndRentModel)
            }
        }

        override fun onFailed(error: String) {
            progressBarItem.visibility = View.GONE
           Utils.customToastMsgError(this@DetailListSaleRentActivity, error, false)
        }
    }

}