package com.eazy.daikou.ui;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Base64;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.eazy.daikou.R;

public class QRCodeAlertDialog extends Dialog {

    private Bitmap bitmap;
    private String image;

    public QRCodeAlertDialog(@NonNull Context context, Bitmap bitmap) {
        super(context);
        this.bitmap = bitmap;
    }

    public QRCodeAlertDialog(@NonNull Context context, String bitmap) {
        super(context);
        this.image = bitmap;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_dialog_qr_code);

        ImageView imageView = findViewById(R.id.my_code);
        if (bitmap != null){
            Glide.with(imageView).load(bitmap).into(imageView);
        } else {
            if(image.contains(".jpg") || image.contains(".jpeg") || image.contains(".png")) {
                Glide.with(imageView).load(image).into(imageView);
            } else {
                if (image != null) {
                    byte[] imageAsBytes = Base64.decode(image, Base64.DEFAULT);
                    imageView.setImageBitmap(BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length));
                }
            }
        }

    }
}
