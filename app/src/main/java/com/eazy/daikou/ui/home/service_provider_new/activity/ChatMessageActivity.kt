package com.eazy.daikou.ui.home.service_provider_new.activity

import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.ui.home.service_provider_new.fragment_service_provider.ItemDeliveryFragment

class ChatMessageActivity : BaseActivity() {
    private lateinit var toolbarTv : TextView
    private lateinit var iconMore : ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat_message)

        initView()

        initAction()
    }

    private fun  initView(){
        findViewById<ImageView>(R.id.imageBack).setOnClickListener { finish() }
        toolbarTv = findViewById(R.id.toolbarTv)
        iconMore = findViewById(R.id.iconMore)
    }
    private fun  initAction(){
       toolbarTv.text = "Chat"
        iconMore.setOnClickListener(CustomSetOnClickViewListener{
            val bttSheet = ItemDeliveryFragment().newInstance("more")
            bttSheet.show(supportFragmentManager, "type_select_delivery")
        })
    }
}