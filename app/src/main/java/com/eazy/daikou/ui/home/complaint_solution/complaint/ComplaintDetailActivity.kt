package com.eazy.daikou.ui.home.complaint_solution.complaint

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.ContextWrapper
import android.content.Intent
import android.content.pm.PackageManager
import android.media.AudioAttributes
import android.media.MediaPlayer
import android.media.MediaRecorder
import android.os.*
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.*
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.cardview.widget.CardView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.base.BaseCameraActivity
import com.eazy.daikou.request_data.request.book_now_ws.GenerateRespondWs.Companion.convertListStringImage
import com.eazy.daikou.request_data.request.complain_ws.ComplaintListWS
import com.eazy.daikou.helper.DateUtil
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.complaint.Comment
import com.eazy.daikou.model.complaint.ComplaintListModel
import com.eazy.daikou.ui.home.book_now.booking_hotel.adapter.CustomImageGridLayoutAdapter
import com.eazy.daikou.ui.home.complaint_solution.complaint.adapter.ComplaintMainReplyAdapter
import com.eazy.daikou.ui.home.complaint_solution.complaint.adapter.ComplaintSolutionAdapter
import java.io.File
import java.io.IOException
import java.lang.Exception
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

open class ComplaintDetailActivity : BaseActivity() {

    private lateinit var progressBar: ProgressBar
    private lateinit var descriptionReplyTv : EditText

    private var id = ""
    private var actionPost = ""
    private var urlImage = ""
    private lateinit var imageLayout : CardView

    private lateinit var replyLayout : LinearLayout
    private lateinit var bottomReplyLayout : LinearLayout

    private lateinit var btnSendComment : ImageView
    private var replyCommentAdapter : ComplaintMainReplyAdapter ?= null

    private var mMediaPlayer: MediaPlayer? = null
    private lateinit var statusTv : TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_complaint_detail)

        initView()

        initData()

        initAction()

    }

    fun initView(){
        progressBar = findViewById(R.id.progressItem)
        descriptionReplyTv = findViewById(R.id.descriptionReplyTv)
        imageLayout = findViewById(R.id.imageLayout)
        replyLayout = findViewById(R.id.replyLayout)
        bottomReplyLayout = findViewById(R.id.bottomReplyLayout)

        // Voice Record
        recordImg = findViewById(R.id.recordImg)
        imgRecord = findViewById(R.id.imgRecord)
        dotLoadRecord = findViewById(R.id.dotLoadRecord)
        btnActionAudio = findViewById(R.id.btnActionAudio)
        btnRemove = findViewById(R.id.btnRemove)
        recordVoiceLayout = findViewById(R.id.recordVoiceLayout)
        seekRecordBar = findViewById(R.id.seekRecordBar)
        maxRecordSecondTv = findViewById(R.id.maxRecordSecondTv)
        listenVoice = findViewById(R.id.listenVoice)
        statusTv = findViewById(R.id.statusTv)
    }

    private fun initData(){
        if (intent != null && intent.hasExtra("id")){
            id = intent.getStringExtra("id") as String
        }

        if (intent != null && intent.hasExtra("action_post")){
            actionPost = intent.getStringExtra("action_post") as String
        }

        Utils.customOnToolbar(this, action()){ onBackPressed() }

    }

    private fun action() : String {
        return when (actionPost) {
            "complaint" -> {
                statusTv.visibility = View.GONE
                resources.getString(R.string.complaints_solutions_detail)
            }
            "frontdesk" -> {
                statusTv.visibility = View.VISIBLE
                findViewById<CardView>(R.id.mainLayoutFrontDesk).visibility = View.VISIBLE
                resources.getString(R.string.front_desk_detail)
            }
            "evaluation" -> {
                statusTv.visibility = View.VISIBLE
                resources.getString(R.string.evaluation_detail)
            }
            else -> {
                ""
            }
        }
    }

    fun initAction(){

        // Do action click
        findViewById<ImageView>(R.id.cameraImg).setOnClickListener(CustomSetOnClickViewListener{
            resultLauncher.launch(Intent(this@ComplaintDetailActivity, BaseCameraActivity::class.java))
        })

        // Clear image upload
        validateImage(imageLayout, isImageValidate = true, isHaveImage = false)    // First Screen
        findViewById<ImageView>(R.id.btn_clear).setOnClickListener(CustomSetOnClickViewListener{
            validateImage(imageLayout, isImageValidate = true, isHaveImage = false)
        })

        btnSendComment = findViewById(R.id.sendImg)
        btnSendComment.setOnClickListener(CustomSetOnClickViewListener{
            onReplyComment()
        })

        isEnableButton()

        descriptionReplyTv.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun afterTextChanged(editable: Editable) {
                isEnableButton()
            }
        })

        requestServiceDetail()

        // init voice
        initVoice()
    }

    private fun requestServiceDetail() {
        progressBar.visibility = View.VISIBLE
        ComplaintListWS.getDetailComplaint(this, id, callBackDetailListener)
    }

    private val callBackDetailListener = object : ComplaintListWS.OnResponseDetail {
        override fun onSuccess(dataComplaintDetail: ComplaintListModel) {
            progressBar.visibility = View.GONE

            onBindingView(dataComplaintDetail)

        }

        override fun onError(msg: String) {
            progressBar.visibility = View.GONE
            Utils.customToastMsgError(this@ComplaintDetailActivity, msg, false)
        }
    }

    private fun onBindingView(item : ComplaintListModel) {
        // User Profile
        if (item.uploader_user != null) {
            Utils.setValueOnText(findViewById(R.id.nameTv), item.uploader_user!!.name)
            Glide.with(this)
                .load(if (item.uploader_user!!.image != null) item.uploader_user!!.image else R.drawable.ic_my_profile)
                .into(findViewById(R.id.profile_image))
        } else {
            Utils.setValueOnText(findViewById(R.id.nameTv), ". . .")
            Glide.with(this)
                .load(R.drawable.ic_my_profile)
                .into(findViewById(R.id.profile_image))
        }

        // Account
        if (item.account != null) {
            Utils.setValueOnText(findViewById(R.id.propertyTv), item.account!!.name)
        } else {
            Utils.setValueOnText(findViewById(R.id.propertyTv), ". . .")
        }

        // Content Body
        Utils.setTextHtml(findViewById(R.id.subjectTv), item.subject)

        Utils.setValueOnText(findViewById(R.id.descriptionTv), item.description)

        val durationTv = findViewById<TextView>(R.id.durationTv)
        if (item.created_at != null) {
            durationTv.text = DateUtil.getDurationDate(durationTv.context, item.created_at)
        } else {
            durationTv.text = ". . ."
        }

        val numViewerTv : TextView = findViewById(R.id.numViewerTv)
        val numCommentTv : TextView = findViewById(R.id.numCommentTv)
        if (item.total_contributor != null){
            numViewerTv.text = String.format("%s %s", item.total_contributor, "Distributors")
        } else {
            numViewerTv.text = String.format("%s %s", 0, "Distributors")
        }

        if (item.total_comments != null){
            numCommentTv.text = String.format("%s %s", item.total_comments, resources.getString(R.string.comment))
        } else {
            numCommentTv.text = String.format("%s %s", 0, resources.getString(R.string.comment))
        }

        // Show Image
        val recyclerView : RecyclerView = findViewById(R.id.recyclerView)
        val imgList = convertListStringImage(item.files)
        recyclerView.layoutManager = Utils.spanGridImageLayoutManager(imgList, this)
        recyclerView.isNestedScrollingEnabled = false
        recyclerView.adapter = CustomImageGridLayoutAdapter(
            "show_5_items",
            this,
            imgList,
            object : CustomImageGridLayoutAdapter.OnClickCallBackLister {
                override fun onClickCallBack(value: String) {
                    Utils.openZoomImage(this@ComplaintDetailActivity, value, ArrayList<String>())
                }
            })

        complaintCmtList = item.comments

        initReplyRecyclerView()

        // Post Type
        (if (item.post_type != null) item.post_type else ". . .")?.let {
            ComplaintSolutionAdapter.postTypeItem(
                it,
                findViewById(R.id.typeOfItemTv)
            )
        }

        // Status
        when (item.post_type) {
            "evaluation" -> {
                if (item.status != null){
                    statusTv.text = Utils.getSatisfaction(this)[item.status]
                    val drawable = Utils.getSatisfactionIcon()[item.status]
                    drawable?.let { statusTv.setCompoundDrawablesWithIntrinsicBounds(it,0,0,0) }
                } else {
                    statusTv.text = ". . ."
                }
            }
            else -> {
                if (item.status != null){
                    statusTv.visibility = View.VISIBLE
                    statusTv.text = Utils.convertFirstCapital(item.status)
                } else {
                    statusTv.visibility = View.GONE
                }
            }
        }

        if (item.object_type != null){
            if (item.object_type == "unit"){
                if (item.object_info != null) {
                    Utils.setValueOnText(findViewById(R.id.typeTv), item.object_info!!.name)
                } else {
                    Utils.setValueOnText(findViewById(R.id.typeTv), ". . .")
                }
            } else {
                Utils.setValueOnText(findViewById(R.id.typeTv), item.object_type)
            }
        } else {
            Utils.setValueOnText(findViewById(R.id.typeTv), ". . .")
        }

        // Front Desk
        if (actionPost == "frontdesk"){
            Utils.setValueOnText(findViewById(R.id.idTv), item.id)
            Utils.setValueOnText(findViewById(R.id.ownerNameTv), item.user_name)
            Utils.setValueOnText(findViewById(R.id.ownerPhoneTv), item.user_phone)
            Utils.setValueOnText(findViewById(R.id.deliveryByTv), item.delivery_name)
            Utils.setValueOnText(findViewById(R.id.deliveryPhoneTv), item.delivery_phone)
        }

    }

    private fun isEnableButton(){
        val isEnable: Boolean
        val color: Int
        if (descriptionReplyTv.text.toString().isNotEmpty() || mFileName != null){
            isEnable = true
            color = R.color.appBarColorOld
        } else {
            isEnable = false
            color = R.color.gray
        }
        btnSendComment.isEnabled = isEnable
        btnSendComment.setColorFilter(Utils.getColor(this, color))
    }

    private fun onReplyComment() {
        progressBar.visibility = View.VISIBLE
        val hashMap = HashMap<String, Any>()
        hashMap["post_id"] = id
        if (descriptionReplyTv.text.trim().toString().isNotEmpty()){
            hashMap["comment"] = descriptionReplyTv.text.toString()
        }
        if (urlImage != ""){
            hashMap["image"] = urlImage
        }

        if (mFileName != null) {
            stopRecording()
            val fileAudioBase64 = Utils.convertAudioToBase64(this, mFileName)
            hashMap["voice"] = fileAudioBase64
        }

        ComplaintListWS.doReplyCmtComplaint(this, hashMap, object : ComplaintListWS.CallBackCreateComplaint {
            override fun onSuccessFull(message: String) {
                progressBar.visibility = View.GONE
                Utils.customToastMsgError(this@ComplaintDetailActivity, message, true)

                // Reload Screen
                descriptionReplyTv.clearFocus()
                descriptionReplyTv.setText("")
                Utils.hideKeyboard(this@ComplaintDetailActivity)

                // If have image
                validateImage(imageLayout, isImageValidate = true, isHaveImage = false)

                // If have voice
                validateImage(recordVoiceLayout, isImageValidate = false, isHaveImage = false)
                if (mFileName != null) { deleteAudio() }

                requestServiceDetail()

                setResult(RESULT_OK)
            }

            override fun onFailed(mes: String) {
                progressBar.visibility = View.GONE
                Utils.customToastMsgError(this@ComplaintDetailActivity, mes, false)
            }

        })
    }

    private fun validateImage(cardViewLayout : CardView, isImageValidate : Boolean, isHaveImage : Boolean){
        if (isImageValidate){ // Add Image
            if (isHaveImage){
                cardViewLayout.visibility = View.VISIBLE
                Glide.with(this).load(BaseCameraActivity.convertByteToBitmap(urlImage)).into(findViewById(R.id.imageReplyCmt))
            } else {
                urlImage = ""
                cardViewLayout.visibility = View.GONE
            }
        } else {    // Add Voice
            if (isHaveImage){
                cardViewLayout.visibility = View.VISIBLE
            } else {
                cardViewLayout.visibility = View.GONE
            }
        }

        // Set Bottom Layout
        Utils.widthHeightLayout(bottomReplyLayout) { _, height ->
            Utils.setMarginBottomOnCoordinatorLayout(replyLayout, height)
        }
    }

    private val resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
        if (result.resultCode == Activity.RESULT_OK) {
            if (result.data != null){
                val data = result.data
                if (data!!.hasExtra("image_path")) {
                    val imagePath = data.getStringExtra("image_path")
                    if (imagePath != null) {
                        urlImage = imagePath
                    }
                }
                if (data.hasExtra("select_image")) {
                    val imagePath = data.getStringExtra("select_image")
                    if (imagePath != null) {
                        urlImage = imagePath
                    }
                }
                if (urlImage != ""){
                    Utils.logDebug("jeeeeeeeeeeeeeeee", urlImage)
                    validateImage(imageLayout, isImageValidate = true, isHaveImage = true)
                }
            }
        }
    }

    private var complaintCmtList = ArrayList<Comment>()
    private fun initReplyRecyclerView(){
        val replyRecyclerView : RecyclerView = findViewById(R.id.replyRecyclerView)
        replyRecyclerView.layoutManager = LinearLayoutManager(this)
        replyRecyclerView.isNestedScrollingEnabled = false

        replyCommentAdapter = ComplaintMainReplyAdapter(complaintCmtList, object : ComplaintMainReplyAdapter.OnClickItemListener{
            override fun onClickListener(item: Comment, action: String) {
                if(action == "view_image"){
                    Utils.openZoomImage(this@ComplaintDetailActivity, item.image, ArrayList<String>())
                }
            }

            @SuppressLint("NotifyDataSetChanged")
            override fun onClickVoiceListener(item: Comment, action: String, playVoiceBtn: ImageView, maxSecondTv: TextView, seekBar: SeekBar) {
                if (item.voice == null) return  // sample voice : "https://filesamples.com/samples/audio/mp3/sample3.mp3"
                refreshVariableVoice(item.actionDisplay, playVoiceBtn, seekBar, maxSecondTv, item.voice!!)

                val actionDisplay = actionPlaySt(item.actionDisplay)

                for (item_ in complaintCmtList) {
                    if (item.id == item_.id){
                        item_.actionDisplay = actionDisplay
                    } else {
                        item_.actionDisplay = ""
                    }
                }

                replyCommentAdapter!!.notifyDataSetChanged()
            }

            override fun onCallBackListener(mediaPlayer: MediaPlayer) {
                if (mediaPlayer != null){
                    mMediaPlayer = mediaPlayer
                }
            }
        })
        replyRecyclerView.adapter = replyCommentAdapter
    }

    override fun onBackPressed() {
        onBackButton()
    }

    // Init Voice
    private var mediaPlayer: MediaPlayer?= null
    private lateinit var handler: Handler
    private var timer : Timer?= null

    // Display voice
    private var actionPlay = ""
    private var finalTime = 0
    private var length = 0
    private var currentProgressSeekbar : Int = 0
    private var timeLeftInMilliseconds: Long = 0
    private var countDownTimer: CountDownTimer? = null
    private var backUpTimeSecond: Long = 0

    private fun refreshVariableVoice(action : String, imagePlayButton : ImageView, seekBar : SeekBar, maxSecondTv : TextView, urlVoice: String){
        if (action == "" || action == "play_audio") {
            releaseMediaPlayer()

            mediaPlayer = MediaPlayer()
            handler = Handler(Looper.getMainLooper())

            timer = Timer()

            actionPlay = ""
            finalTime = 0
            length = 0
            if (countDownTimer != null) countDownTimer!!.cancel()
            currentProgressSeekbar = 0
            timeLeftInMilliseconds = 0
            backUpTimeSecond = 0
        }

        actionPlayAudio(imagePlayButton, seekBar, maxSecondTv, urlVoice)
    }

    private fun initVoice() {
        // Voice Display
        mediaPlayer = MediaPlayer()
        handler = Handler(Looper.getMainLooper())

        timer = Timer()

        // Voice Record
        validateImage(recordVoiceLayout, isImageValidate = false, isHaveImage = false)    // First Screen

        initRecordVoice()
    }

    private fun actionPlayAudio(imagePlayButton : ImageView, seekBar : SeekBar, maxSecondTv : TextView, url: String) {
        try {
            when (actionPlay){
                "", "play_audio" -> {
                    setImagePlayAction(imagePlayButton)

                    mediaPlayer = MediaPlayer().apply {
                        setAudioAttributes(
                            AudioAttributes.Builder()
                                .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                                .build()
                        )
                        setDataSource(url)
                        prepare()
                        start()
                    }

                    finalTime = mediaPlayer!!.duration
                    seekBar.max = mediaPlayer!!.duration

                    setSeekBarProgressVoice(seekBar)

                    startTimer(maxSecondTv, finalTime.toLong(), false)
                }

                "pause_audio" -> {
                    setImagePlayAction(imagePlayButton)

                    if (mediaPlayer != null) {
                        if (mediaPlayer!!.isPlaying) {
                            mediaPlayer!!.pause()
                            length = mediaPlayer!!.currentPosition

                            stopTimer(maxSecondTv, finalTime.toLong())   // Stop counter voice
                        }
                    }
                }

                else -> {   // resume_audio
                    setImagePlayAction(imagePlayButton)

                    backUpTimeSecond = (finalTime - timeLeftInMilliseconds) / 1000

                    if (mediaPlayer != null) {
                        mediaPlayer!!.seekTo(length)
                        mediaPlayer!!.start()

                        setSeekBarProgressVoice(seekBar)

                        startTimer(maxSecondTv, timeLeftInMilliseconds, true)
                    }

                }
            }

        } catch (e: Exception) {
            e.printStackTrace()
        }

        seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                currentProgressSeekbar = progress
                if (currentProgressSeekbar == finalTime) {
                    actionPlay = "final_play"
                    setImagePlayAction(imagePlayButton)
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {}
            override fun onStopTrackingTouch(seekBar: SeekBar) {}
        })
    }

    private fun setImagePlayAction(imagePlayButton : ImageView){
        var drawable =  R.drawable.ic_pause
        actionPlay = actionPlaySt(actionPlay)

        when (actionPlay) {
            "", "play_audio" -> {
                drawable =  R.drawable.ic_pause
            }
            "pause_audio" -> {
                drawable =  R.drawable.ic_play
            }
            "resume_audio" -> {
                drawable =  R.drawable.ic_pause
            }
            "final_play" -> {
                drawable = R.drawable.ic_play
            }
        }

        imagePlayButton.setImageDrawable(ContextCompat.getDrawable(this, drawable))
    }

    private fun actionPlaySt(actionPlay : String) : String {
        when (actionPlay) {
            "", "play_audio" -> {
                 return "pause_audio"
            }
            "pause_audio" -> {
                return "resume_audio"
            }
            "resume_audio" -> {
                return "pause_audio"
            }
            "final_play" -> {
                return ""
            }
        }
        return ""
    }


    private fun stopTimer(maxSecondTv : TextView, second: Long) {
        countDownTimer!!.cancel()
        maxSecondTv.text =
            String.format(Locale.US, "%s : %s", 0, (second - timeLeftInMilliseconds) / 1000)
    }

    private fun startTimer(maxSecondTv : TextView, second: Long, isResume: Boolean) {
        countDownTimer = object : CountDownTimer(second, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                timeLeftInMilliseconds = millisUntilFinished
                val minutes = timeLeftInMilliseconds.toInt() / 60000

                val countTime: String = if (isResume) { // Resume voice
                    String.format(Locale.US, "%s : %s", 0, (second - millisUntilFinished) / 1000 + backUpTimeSecond)
                } else {
                    String.format(Locale.US, "%s : %s", 0, (second - millisUntilFinished) / 1000
                    )
                }

                maxSecondTv.text = countTime
            }

            override fun onFinish() {}

        }.start()
    }

    private  fun setSeekBarProgressVoice(seekBar: SeekBar) { // Seekbar Progress
        timer = Timer()
        timer!!.scheduleAtFixedRate(object : TimerTask() {
            override fun run() {
                if (mediaPlayer != null) {
                    while (mediaPlayer!!.isPlaying) {
                        seekBar.progress = mediaPlayer!!.currentPosition
                    }
                    timer!!.cancel()
                }
            }
        }, 0, 1000) // 1000 = 1 Second
    }

    override fun onPause() {
        super.onPause()
        releaseMediaPlayer()
    }

    private fun onBackButton() {
        releaseMediaPlayer()
        finish()
    }

    private fun releaseMediaPlayer() {
        if (mediaPlayer != null) {
            if (mediaPlayer!!.isPlaying) {
                mediaPlayer!!.stop()
            }
            mediaPlayer!!.release()
            mediaPlayer = null
        }

        if (timer != null)  timer!!.cancel()
        if (countDownTimer != null) countDownTimer!!.cancel()

        // Play in adapter
        try {
            if (mMediaPlayer != null) {
                if (mMediaPlayer!!.isPlaying) {
                    mMediaPlayer!!.stop()
                }
                mMediaPlayer!!.release()
                mMediaPlayer = null
            }
            replyCommentAdapter!!.clearMediaPlayer()
        } catch (ex : Exception){
            ex.toString()
        }
    }

    override fun onDestroy() {
        releaseMediaPlayer()
        super.onDestroy()
    }

    // Voice Record
    private var REQUEST_AUDIO_PERMISSION_CODE = 290
    private lateinit var imgRecord : ImageView
    private lateinit var dotLoadRecord : ImageView
    private lateinit var btnActionAudio : ImageView
    private lateinit var btnRemove : ImageView
    private var isClickStartRecord = "start_record"
    private var mFileName: String? = null
    private lateinit var recordImg : ImageView
    private lateinit var recordVoiceLayout : CardView
    private lateinit var seekRecordBar : SeekBar
    private lateinit var maxRecordSecondTv : TextView
    private lateinit var listenVoice: LinearLayout
    private var mRecorder: MediaRecorder? = null
    private val maxSecondVoice: Long = 30000
    private var min: Long = 0

    private fun initRecordVoice() {
        imgRecord.setOnClickListener (CustomSetOnClickViewListener{
            isEnableButton()
            stopRecording()
        })

        recordImg.setOnClickListener (CustomSetOnClickViewListener {
            if (isClickStartRecord.equals("start_record", ignoreCase = true)) {
                if (checkPermissions()) {
                    try {
                        startRecording()
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                } else {
                    requestPermissions()
                }
            }
            else if (isClickStartRecord.equals("stop_record", ignoreCase = true)) {
                stopRecording()
            }
        })

        btnActionAudio.setOnClickListener(CustomSetOnClickViewListener {
             actionPlayAudio(btnActionAudio, seekRecordBar, maxRecordSecondTv, mFileName!!)
        })

        btnRemove.setOnClickListener(CustomSetOnClickViewListener {
            deleteAudio()
        })
    }

    private fun visibleLayoutVoice(isRecord: Boolean) {
        recordImg.isEnabled = !isRecord
        recordImg.setColorFilter(if (isRecord) Utils.getColor(this, R.color.gray) else Utils.getColor(this, R.color.appBarColorOld))
        validateImage(recordVoiceLayout, false, isRecord)
    }

    private fun checkPermissions(): Boolean {
        val result = ContextCompat.checkSelfPermission(this@ComplaintDetailActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        val result1 = ContextCompat.checkSelfPermission(this@ComplaintDetailActivity, Manifest.permission.RECORD_AUDIO)
        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermissions() {
        ActivityCompat.requestPermissions(this@ComplaintDetailActivity, arrayOf(Manifest.permission.RECORD_AUDIO, Manifest.permission.WRITE_EXTERNAL_STORAGE), REQUEST_AUDIO_PERMISSION_CODE)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String?>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_AUDIO_PERMISSION_CODE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                try {
                    startRecording()
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            } else {
                Toast.makeText(this@ComplaintDetailActivity, "Permission is not Grand", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun deleteAudio() {
        val videoFiles = File(mFileName!!)
        if (videoFiles.exists()) {
            videoFiles.delete()
            visibleActionVoice(false)
            isClickStartRecord = "start_record"
            releaseMediaPlayer()
            maxRecordSecondTv.text = resources.getString(R.string.voice_record_is_max_30_seconds)
            actionPlay = "play_audio"
            if (countDownTimer != null) countDownTimer!!.cancel()
            btnActionAudio.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_play))
            seekRecordBar.progress = 0
            isEnableButton()
            visibleLayoutVoice(false)
            mFileName = null
        }
    }

    @Throws(IOException::class)
    private fun startRecording() {
        val contextWrapper = ContextWrapper(this)
        var index = 0
        mFileName = contextWrapper.getExternalFilesDir(Environment.DIRECTORY_MUSIC).toString()
        mFileName += "/AudioRecordingFile.mp3"

        //        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
        //            mRecorder = MediaRecorder(this@ComplaintDetailActivity)
        //        } else {
        //            return
        //        }

        mRecorder = MediaRecorder()
        mRecorder!!.setAudioSource(MediaRecorder.AudioSource.MIC)
        // mRecorder!!.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP)
        // mRecorder!!.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB)
        mRecorder!!.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4)
        mRecorder!!.setAudioEncoder(MediaRecorder.AudioEncoder.AAC)
        mRecorder!!.setMaxDuration(30000)
        mRecorder!!.setOutputFile(mFileName)
        visibleLayoutVoice(true)

        try {
            imgRecord.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.card_view_shape))
            isClickStartRecord = "stop_record"
            mRecorder!!.prepare()
            mRecorder!!.start()
            countDownTimer = object : CountDownTimer(maxSecondVoice, 1000) {
                override fun onTick(millisUntilFinished: Long) {
                    timeLeftInMilliseconds = millisUntilFinished
                    min = millisUntilFinished / 60000 % 60
                    maxRecordSecondTv.text = String.format(Locale.US, "%s : %s", min, (maxSecondVoice - millisUntilFinished) / 1000)

                    index ++
                    dotLoadRecord.visibility = if (index % 2 == 0) View.VISIBLE else View.GONE
                }

                override fun onFinish() {
                    stopRecording()
                }
            }.start()
        } catch (e: IOException) {
            Toast.makeText(applicationContext, "Recording not started", Toast.LENGTH_SHORT).show()
            Utils.logDebug("SoundRecordingActivity", "sdcard access error")
        }
    }

    private fun visibleActionVoice(isReadVoice: Boolean) {
        dotLoadRecord.visibility = if (isReadVoice) View.GONE else View.VISIBLE
        imgRecord.visibility = if (isReadVoice) View.GONE else View.VISIBLE
        listenVoice.visibility = if (isReadVoice) View.VISIBLE else View.GONE
        btnRemove.visibility = if (isReadVoice) View.VISIBLE else View.GONE
    }

    private fun stopRecording() {
        if (mRecorder != null) {
            imgRecord.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.icon_dot_online))
            mRecorder!!.stop()
            mRecorder!!.release()
            mRecorder = null
            isClickStartRecord = "read_voice"
            stopTimer(maxRecordSecondTv, maxSecondVoice)   // Stop counter voice
            visibleActionVoice(true)
        }
    }

}