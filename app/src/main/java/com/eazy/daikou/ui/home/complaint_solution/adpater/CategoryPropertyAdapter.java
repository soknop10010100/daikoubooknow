package com.eazy.daikou.ui.home.complaint_solution.adpater;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.eazy.daikou.R;
import com.eazy.daikou.helper.Utils;
import com.eazy.daikou.model.inspection.ItemInspectionModel;

import java.util.List;
import java.util.Locale;

public class CategoryPropertyAdapter extends RecyclerView.Adapter<CategoryPropertyAdapter.ItemViewHolder> {

    private final ClickInspectionCallBack clickInspectionCallBack;
    private final List<ItemInspectionModel> inspectionModelList;
    private final String action;
    private final Context context;

    public CategoryPropertyAdapter(Context context, List<ItemInspectionModel> inspectionModelList, ClickInspectionCallBack clickInspectionCallBack) {
        this.inspectionModelList = inspectionModelList;
        this.action = "inspection_list";
        this.clickInspectionCallBack = clickInspectionCallBack;
        this.context = context;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view  = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_tap_document_adapter,parent,false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        if (action.equals("inspection_list")) {
            ItemInspectionModel suggestionModel = inspectionModelList.get(position);
            if (suggestionModel != null) {
                holder.nameCategoryTv.setText(String.format(Locale.US, "%s", suggestionModel.getInspection_name()));

                if (suggestionModel.isClick()) {
                    holder.nameCategoryTv.setTextColor(Utils.getColor(context, R.color.white));
                    holder.linearLayout.setBackground(Utils.setDrawable(context.getResources(), R.color.appBarColorOld));
                } else {
                    holder.linearLayout.setBackground(Utils.setDrawable(context.getResources(), R.color.color_gray_item));
                    holder.nameCategoryTv.setTextColor(Utils.getColor(context, R.color.gray));
                }

                holder.itemView.setOnClickListener(view -> clickInspectionCallBack.clickSelectPropertyBack(suggestionModel));

            }
        }

    }

    @Override
    public int getItemCount() {
        return inspectionModelList.size();
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder{
        private final TextView nameCategoryTv;
        private final LinearLayout linearLayout;
        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            linearLayout = itemView.findViewById(R.id.linear_card);
            nameCategoryTv = itemView.findViewById(R.id.text_category);
        }
    }

    public interface ClickInspectionCallBack{
        void clickSelectPropertyBack(ItemInspectionModel floorSpinnerModel);
    }

}
