package com.eazy.daikou.ui.home.book_now.booking_hotel

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.activity.result.contract.ActivityResultContracts
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.viewpager.widget.ViewPager
import androidx.viewpager2.widget.ViewPager2
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.Constant
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.base.BaseFragment
import com.eazy.daikou.base.MockUpData
import com.eazy.daikou.request_data.request.book_now_ws.BookingHotelWS
import com.eazy.daikou.request_data.request.book_now_ws.RequestHashMapData
import com.eazy.daikou.request_data.request.profile_ws.LoginWs
import com.eazy.daikou.request_data.static_data.StaticUtilsKey
import com.eazy.daikou.helper.*
import com.eazy.daikou.helper.map.GPSTrackerActivity
import com.eazy.daikou.model.booking_hotel.HotelBookingDetailModel
import com.eazy.daikou.model.booking_hotel.LocationHotelModel
import com.eazy.daikou.model.home.MainItemHomeModel
import com.eazy.daikou.model.home.SubItemHomeModel
import com.eazy.daikou.model.home.UserDeactivated
import com.eazy.daikou.model.work_order.ImageListModel
import com.eazy.daikou.ui.home.book_now.booking_hotel.adapter.*
import com.eazy.daikou.ui.home.book_now.booking_hotel.travel_article.HotelArticlesActivity
import com.eazy.daikou.ui.home.book_now.booking_hotel.visit.HotelTravelTalkActivity
import com.eazy.daikou.ui.home.my_property_v1.EasyDialogClass
import com.eazy.daikou.ui.profile.ChangeLanguageFragment
import com.faltenreich.skeletonlayout.Skeleton
import com.faltenreich.skeletonlayout.applySkeleton
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.appbar.AppBarLayout.BaseOnOffsetChangedListener
import com.michael.easydialog.EasyDialog
import kotlin.math.abs

class HotelBookingHomePageFragment : BaseFragment(), HotelHomeListHotelFragment.CallBackListener {

    private lateinit var progressBar: ProgressBar
    private lateinit var layoutImage : RelativeLayout

    // Search Hotel
    private lateinit var selectLocationTv : TextView
    private lateinit var selectRoomTv : TextView
    private lateinit var startRangDateTv : TextView
    private var locationId = ""
    private var locationHotel = ""
    private var selectItemGuest = ""
    private var room = "1"
    private var adults = "1"
    private var children = "0"
    private var startDate = ""
    private var endDate = ""
    private lateinit var btnSearch : ImageView
    private lateinit var iconLanguage : ImageView
    private lateinit var btnSearchTv : TextView
    private var progressBarDialog : ProgressBar? = null

    private var locationHotelModelList: ArrayList<LocationHotelModel> = ArrayList()
    private lateinit var toolbarLayout : LinearLayout
    private lateinit var titleToolbar : TextView
    private var isFirstGetLoc = true
    private var eazyhotelUserId = ""
    private lateinit var cancelDeactivate: TextView
    private lateinit var activeAccLayout: LinearLayout
    private lateinit var iconWishlist : ImageView
    private lateinit var iconHistoryBooking : ImageView

    // image slider
    private lateinit var viewPager2: ViewPager2
    private val sliderHandler: Handler = Handler(Looper.getMainLooper())

     private lateinit var skeleton: Skeleton
    private lateinit var recyclerViewDiscount : RecyclerView
    private lateinit var recyclerViewCategory : RecyclerView
    private var subHomeModelCategoryList: ArrayList<SubItemHomeModel> = ArrayList()
    private lateinit var toolbarView : androidx.appcompat.widget.Toolbar
    private lateinit var swipeRefreshLayout : SwipeRefreshLayout
    private lateinit var appBar : AppBarLayout

    private var category = "hotel"
    private var bannerDiscountList : ArrayList<String> = ArrayList()
    private var bannerTravelTalkList : ArrayList<String> = ArrayList()

    private lateinit var viewpager : ViewPager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.booking_hotel_home_screen, container, false)

        initView(view)

        initAction()

        return view
    }

    private fun initView(view: View){
        progressBar = view.findViewById(R.id.progressItem)
        progressBar.visibility = View.GONE
        layoutImage = view.findViewById(R.id.layoutImage)
        btnSearch = view.findViewById(R.id.btnsearch)
        toolbarLayout = view.findViewById(R.id.toolbar)
        titleToolbar = view.findViewById(R.id.infoAdd)
        activeAccLayout = view.findViewById(R.id.activeAccLayout)
        cancelDeactivate = view.findViewById(R.id.cancelDeactivate)
        iconWishlist = view.findViewById(R.id.iconWishlist)
        iconHistoryBooking = view.findViewById(R.id.icon_history)
        iconLanguage = view.findViewById(R.id.icon_lang)

        viewPager2 = view.findViewById(R.id.viewPagerImageSlider)
        skeleton = view.findViewById(R.id.skeletonLayout)
        recyclerViewDiscount = view.findViewById(R.id.recyclerViewDiscount)
        recyclerViewCategory = view.findViewById(R.id.recyclerViewCategory)
        toolbarView = view.findViewById(R.id.toolbarView)
        appBar = view.findViewById(R.id.appBarLayout)
        swipeRefreshLayout = view.findViewById(R.id.swipe_layout)

        viewpager = view.findViewById(R.id.viewpager)
    }

    private fun initAction(){
        // Set height on image slide
        layoutImage.layoutParams.height = Utils.setHeightOfScreen( mActivity, 4.0)
        eazyhotelUserId = Utils.validateNullValue(MockUpData.getEazyHotelUserId(UserSessionManagement(mActivity)))

        // When click on btn search home page hotel
        btnSearch.setOnClickListener {
            openSpinner(EasyDialog(mActivity), btnSearch)
        }

        // Change language
        when (Utils.getString("language", mActivity)) {
            Constant.LANG_EN, "" -> {
                iconLanguage.setImageResource(R.drawable.flag_united_kingdom)
            }
            Constant.LANG_KH -> {
                iconLanguage.setImageResource(R.drawable.cambodia_flag)
            }
            Constant.LANG_CN -> {
                iconLanguage.setImageResource(R.drawable.china_flag)
            }
        }

        iconLanguage.setOnClickListener(
            CustomSetOnClickViewListener{
                val changeLanguageAlert = ChangeLanguageFragment.newInstance(true,"language")
                 changeLanguageAlert.show(childFragmentManager, changeLanguageAlert.javaClass.simpleName)
            }
        )

        //Click on wishlist hotel
        iconWishlist.setOnClickListener(
            CustomSetOnClickViewListener{
                if (AppAlertCusDialog.isSuccessLoggedIn(mActivity)) {
                    val dashBoard = Intent(mActivity, HotelBookingVendorManageHotelActivity::class.java)
                    dashBoard.putExtra("action", "hotel_wishlist_hotel")
                    startActivity(dashBoard)
                }
            }
        )

        //History
        iconHistoryBooking.setOnClickListener(
            CustomSetOnClickViewListener{
                if (AppAlertCusDialog.isSuccessLoggedIn(mActivity)) {
                    val videoUrl = Intent(mActivity, HotelBookingHistoryActivity::class.java)
                    videoUrl.putExtra("action", "hotel_history_hotel")
                    startActivity(videoUrl)
                }
            }
        )

        recyclerViewCategory.layoutManager = GridLayoutManager(mActivity, 5)

        requestItemLocationList()

        subHomeModelCategoryList = RequestHashMapData.addCategoryItemList(resources)

        // Category
        initRecyclerViewCategory()

        // Discount
        recyclerViewDiscount.layoutManager = LinearLayoutManager(mActivity)
        initItemBannerDiscount()

        reLoadFragment()

        //Click btn cancel deactivated account
        clickOnUserDeactivated()

        initSkeleton()

        onDataLoaded()

        initRefreshLayout()

        //init lat lon location
        if (BaseActivity.longitude == 0.0 || BaseActivity.longitude == 0.0) {
            val intent = Intent(mActivity, GPSTrackerActivity::class.java)
            resultLauncher.launch(intent)
        }

        if (locationHotelModelList.size == 0)   requestItemLocationList()   // Request Location

    }

    private fun clickOnUserDeactivated(){
        cancelDeactivate.setOnClickListener(
            CustomSetOnClickViewListener {
                val hashMap: java.util.HashMap<String, Any> = RequestHashMapData.hashMapUserActiveAccount("activate", UserSessionManagement(mActivity).userId)
                progressBar.visibility = View.VISIBLE
                LoginWs().sendVerifyCode(mActivity, "activate", hashMap, object : LoginWs.OnDeleteAccListener{
                    override fun onSuccess(action: String?, msg: String?) {
                        requestItemHotelList()
                    }

                    override fun onFailed(mess: String?) {
                        progressBar.visibility = View.GONE
                        Utils.customToastMsgError(mActivity, mess, false)
                    }

                })
            }
        )
    }

    private var resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode == BaseActivity.RESULT_OK) {
            if (result.data != null){
                val data = result.data!!.getBooleanExtra("isSuccess", false)
                if (data){
                    requestItemHotelList()
                } else {
                    Utils.customToastMsgError(mActivity, "You can check in / out without no your current location", false)
                }
            }
        }
    }

    private fun initRefreshLayout(){
        val start = resources.getDimensionPixelSize(R.dimen.indicator_start)
        val end = resources.getDimensionPixelSize(R.dimen.indicator_end)
        swipeRefreshLayout.setColorSchemeResources(R.color.appBarColorOld)
        swipeRefreshLayout.setProgressViewOffset(true, start, end)
        swipeRefreshLayout.setOnRefreshListener {
            requestItemHotelList()
        }

        //set swipe for only expanded of app bar
        appBar.addOnOffsetChangedListener(BaseOnOffsetChangedListener<AppBarLayout> { appBarLayout, verticalOffset ->
            Utils.logDebug("caleudjheejlaotejeje", "${abs(verticalOffset)}                     ${appBarLayout!!.totalScrollRange}               $verticalOffset")
            if (abs(verticalOffset) == appBarLayout.totalScrollRange) {
                // Collapsed
                swipeRefreshLayout.isEnabled = false
            } else swipeRefreshLayout.isEnabled = verticalOffset == 0
        })
    }

    private fun initItemBannerDiscount(){
        recyclerViewDiscount.adapter = HotelContentDiscountAdapter(bannerDiscountList, bannerTravelTalkList, object : HotelContentDiscountAdapter.OnClickItemListener{
            override fun onClickListener(action: String) {
                when(action){
                    "travel_article" ->{
                        startActivity(Intent(mActivity, HotelArticlesActivity::class.java))
                    }
                    "travel_talk"->{
                        startActivity(Intent(mActivity, HotelTravelTalkActivity::class.java))
                    }
                    "save_discount" ->{
                        startActivity(Intent(mActivity, HotelShowDiscountActivity::class.java))
                    }
                }
            }
        })
    }

    private fun initRecyclerViewCategory(){
        StaticUtilsKey.category = category
        var bookingHomeItemAdapter : HotelCategoryItemAdapter? = null
        bookingHomeItemAdapter = HotelCategoryItemAdapter(true, subHomeModelCategoryList, object : BookingHomeItemAdapter.PropertyClick{
            @SuppressLint("NotifyDataSetChanged")
            override fun onBookingClick(propertyModel: SubItemHomeModel?) {
                for (itemSub in subHomeModelCategoryList){
                    if (propertyModel != null) {
                        itemSub.isClickCategory = itemSub.id == propertyModel.id
                    }
                }
                bookingHomeItemAdapter?.notifyDataSetChanged()

                category = propertyModel!!.id.toString()
                StaticUtilsKey.category = category
                reLoadFragment()
            }

            override fun onBookingWishlistClick(propertyModel: SubItemHomeModel?) {}

        })
        recyclerViewCategory.adapter = bookingHomeItemAdapter
    }

    private fun initSkeleton(){
        progressBar.visibility = View.VISIBLE

        skeleton = viewPager2.applySkeleton(R.layout.custom_image_slide_model, 1).apply { showSkeleton() }

        skeleton = recyclerViewDiscount.applySkeleton(R.layout.custom__skeleton_category_hotel, 1).apply { showSkeleton() }

        skeleton.showSkeleton()
    }

    private fun onDataLoaded() {
        skeleton.showOriginal()
        progressBar.visibility = View.GONE
    }

    private fun updateImageSlider(slideModels: MutableList<String>, userDeactivated: UserDeactivated) {
        MockUpData.isDeactivated = userDeactivated.is_deactivated
        activeAccLayout.visibility = if (userDeactivated.is_deactivated) View.VISIBLE else View.GONE
        if (userDeactivated.is_expired) {
            AppAlertCusDialog.requestLogOut(mActivity, progressBar)
        }

        //Slider Image
        CustomSliderImageClass.initImageSlideViewPager(55, viewPager2, sliderHandler, sliderRunnable, ArrayList(), slideModels, object : AdapterImageSlider.OnClickItemListener{
            override fun onClickCallBack(sliderItems: ImageListModel) {}
        })

        onDataLoaded()
    }

    private fun requestItemLocationList(){
        BookingHotelWS().getLocationHotelBooking(mActivity, onCallBackItemListener)
    }

    private fun requestItemHotelList(){
        progressBar.visibility = View.VISIBLE
        BookingHotelWS().getHotelBookingAll(
            mActivity,
            "hotel_home_page", category,
            RequestHashMapData.hashMapHomePageData(1, StaticUtilsKey.category, eazyhotelUserId, BaseActivity.latitude.toString(), BaseActivity.longitude.toString()),
            onCallBackItemListener
        )
    }

    private val onCallBackItemListener = object : BookingHotelWS.OnCallBackMyProfileListener{
        override fun onSuccessLocation(locationHotelModel: ArrayList<LocationHotelModel>) {
            progressBar.visibility = View.GONE
            swipeRefreshLayout.isRefreshing = false
            locationHotelModelList.clear()
            locationHotelModelList.addAll(locationHotelModel)
            RequestHashMapData.storeLocationHotelData(locationHotelModelList)

            if (progressBarDialog != null)  progressBarDialog!!.visibility = View.GONE

            if (!isFirstGetLoc)  {
                startSelectCategory("select_location", locationHotelModelList)
                isFirstGetLoc = true
            }
        }

        override fun onSuccessShowHotelDetail(hotelDetailItemModel: HotelBookingDetailModel, mainItemHomeModelLIst: ArrayList<SubItemHomeModel>) {}

        @SuppressLint("NotifyDataSetChanged")
        override fun onSuccessShowHotel(action: String, listImageSlider : MutableList<String>, locationHotelModel: ArrayList<MainItemHomeModel>, itemHomeList: ArrayList<SubItemHomeModel>, userDeactivated : UserDeactivated) {
            progressBar.visibility = View.GONE
            swipeRefreshLayout.isRefreshing = false

            // Slide image
            updateImageSlider(listImageSlider, userDeactivated)  // Image slider and with check deactivated account user

            reLoadFragment()

        }

        override fun onFailed(message: String) {
            progressBar.visibility = View.GONE
            swipeRefreshLayout.isRefreshing = false
            if (progressBarDialog != null)  progressBarDialog!!.visibility = View.GONE
            Utils.customToastMsgError(mActivity, message, false)
        }

    }

    private fun startEndDateRang(){
        val selectLocationBookingFragment = SelectStartEndDateRangBottomsheet.newInstance(startDate, endDate)
        selectLocationBookingFragment.initListener(onCallBackDateListener)
        selectLocationBookingFragment.show(childFragmentManager, "fragment")
    }

    private fun startSelectCategory(action : String, locationHotelModel: ArrayList<LocationHotelModel>){
        val selectLocationBookingFragment : SelectLocationBookingFragment = if (action == "select_location") {
            SelectLocationBookingFragment().newInstance(locationId, action, locationHotelModel)
        } else {
            SelectLocationBookingFragment().newInstance(room, adults,children, action)
        }
        selectLocationBookingFragment.initListener(onCallBackListener)
        selectLocationBookingFragment.show(childFragmentManager, "fragment")
    }

    private val onCallBackDateListener = object : SelectStartEndDateRangBottomsheet.OnClickCallBackListener{
        override fun onClickSelect(getStartDate: String, getEndDate : String) {
            startDate = getStartDate
            endDate = getEndDate
            startRangDateTv.text = String.format("%s - %s", getStartDate, getEndDate)
            checkValidateButtonPay()
        }
    }

    private val onCallBackListener = object : SelectLocationBookingFragment.OnClickCallBackListener{
        override fun onClickSelectLocation(titleHeader: LocationHotelModel) {
            selectLocationTv.text = titleHeader.location_name
            locationId = titleHeader.location_id.toString()
            locationHotel = titleHeader.location_name!!
            checkValidateButtonPay()
        }

        override fun onClickSelect(titleHeader: String, r : String, ad : String, ch : String) {
            selectRoomTv.text = titleHeader
            selectItemGuest = titleHeader
            room = r
            adults = ad
            children = ch
            checkValidateButtonPay()
        }
    }

    private fun openSpinner(easyDialog : EasyDialog, linearLayout: ImageView) {
        @SuppressLint("InflateParams") val layout = LayoutInflater.from(mActivity).inflate(R.layout.activity_booking_select_location, null)
        EasyDialogClass.createFilterEasyDialog(easyDialog, mActivity, layout, linearLayout, true)

        selectLocationTv = layout.findViewById(R.id.selectLocationTv)
        selectRoomTv = layout.findViewById(R.id.selectRoomTv)
        startRangDateTv = layout.findViewById(R.id.startDateTv)
        btnSearchTv = layout.findViewById(R.id.btnSearch)
        val progressBar = layout.findViewById<ProgressBar>(R.id.progressItem)
        progressBar.visibility = View.GONE
        progressBarDialog = progressBar

        selectLocationTv.text = locationHotel
        selectRoomTv.text = selectItemGuest
        startRangDateTv.text =  if (startDate != "" && endDate != "")   String.format("%s - %s", startDate,  endDate) else ""

        doActionClickMenu(easyDialog)

        checkValidateButtonPay()
    }

    private fun doActionClickMenu(easyDialog : EasyDialog){
        selectLocationTv.setOnClickListener(
            CustomSetOnClickViewListener{
                if (locationHotelModelList.size > 0){
                    startSelectCategory("select_location", locationHotelModelList)
                } else {
                    isFirstGetLoc = false
                    if (progressBarDialog != null)  progressBarDialog!!.visibility = View.VISIBLE
                    requestItemLocationList()
                }
            }
        )

        startRangDateTv.setOnClickListener(
            CustomSetOnClickViewListener{
                startEndDateRang()
            }
        )

        selectRoomTv.setOnClickListener(
            CustomSetOnClickViewListener{
                startSelectCategory("select_room", ArrayList())
            }
        )

        btnSearchTv.setOnClickListener(
            CustomSetOnClickViewListener{
                easyDialog.dismiss()
                initStartSearch(true, locationId, locationHotel)
            }
        )
    }

    private fun initStartSearch(isClickSearch : Boolean, locationId : String, locationHotel : String){
        val value = String.format("%s ∙ %s - %s", locationHotel, startDate, endDate)
        val hashMapData : HashMap<String, Any> = HashMap()
        hashMapData["value"] = if(isClickSearch) value else locationHotel
        hashMapData["location_list"] = locationHotelModelList
        hashMapData["location_id"] = locationId
        hashMapData["location_title"] = locationHotel
        hashMapData["adults"] = if(isClickSearch) adults else ""
        hashMapData["room"] = if(isClickSearch) room else ""
        hashMapData["children"] = if(isClickSearch) children else ""
        hashMapData["start_date"] =  if(isClickSearch) startDate else ""
        hashMapData["end_date"] = if(isClickSearch) endDate else ""
        hashMapData["guest_num"] = if (isClickSearch) selectItemGuest else ""
        hashMapData["from_action"] =  if (isClickSearch) "search_list" else "hotel_location_detail"
        hashMapData["category"] = if (isClickSearch) "hotel" else category
        RequestHashMapData.storeHotelItemData(hashMapData)

        titleToolbar.text = if (isClickSearch) value else ""
        val intent = Intent(mActivity, HotelSearchShowHotelBookingActivity::class.java)
        startActivity(intent)
    }

    private fun checkValidateButtonPay(){
        if (isEnableButtonPay()){
            btnSearchTv.isEnabled = true
            Utils.setBgTint(btnSearchTv, R.color.gray)
            Utils.setBgTint(btnSearchTv, R.color.appBarColorOld)
        } else {
            btnSearchTv.isEnabled = false
            Utils.setBgTint(btnSearchTv, R.color.gray)
        }
    }

    private fun isEnableButtonPay() : Boolean{
        if (locationId == "" || selectItemGuest == "" || startDate == "" || endDate == ""){
            return false
        }
        return true
    }

    private val sliderRunnable = Runnable { viewPager2.currentItem = viewPager2.currentItem + 1 }

    override fun onPause() {
        super.onPause()
        sliderHandler.removeCallbacks(sliderRunnable)
    }

    override fun onResume() {
        super.onResume()
        sliderHandler.postDelayed(sliderRunnable, 3000)
    }

    @SuppressLint("DetachAndAttachSameFragment")
    private fun reLoadFragment() {
        val hotelHomeListHotelFragment = HotelHomeListHotelFragment.newInstance(category)
        val pagerAdapter = HotelHomeListViewPager(childFragmentManager)
        pagerAdapter.addFragment(hotelHomeListHotelFragment, "fragment")
        viewpager.adapter = pagerAdapter
    }

    override fun onCallBack(travelTalkBanner : ArrayList<String>, bannerDiscountSlide : ArrayList<String>, sliderBanner: ArrayList<String>, userDeactivated: UserDeactivated) {
        updateImageSlider(sliderBanner, userDeactivated)  // Image slider and with check deactivated account user

        // Show Image Discount
        bannerDiscountList = bannerDiscountSlide
        bannerTravelTalkList = travelTalkBanner
        initItemBannerDiscount()

    }

}