package com.eazy.daikou.ui.home.my_property.property_service.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.databinding.MainItemHomeLayoutBinding
import com.eazy.daikou.helper.AbsoluteFitLayoutManager
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.my_property.property_service.MainCategoryServiceModel
import com.eazy.daikou.model.my_property.property_service.PropertyServiceListModel
import com.eazy.daikou.model.my_property.property_service.ServiceByCategoryModel
import com.google.android.gms.ads_identifier.R

class MainCategoryServiceAdapter(private val context: Activity, private val list: ArrayList<MainCategoryServiceModel>, private val onClickListener: SubServiceByCategoryAdapter.OnClickItemListener):
    RecyclerView.Adapter<MainCategoryServiceAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = MainItemHomeLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val service : MainCategoryServiceModel = list[position]
        holder.onBindView(context, service, onClickListener)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ViewHolder(private val mBind: MainItemHomeLayoutBinding) : RecyclerView.ViewHolder(mBind.root) {

        fun onBindView(context: Activity, service: MainCategoryServiceModel, onClickListener: SubServiceByCategoryAdapter.OnClickItemListener){
            Utils.setValueOnText(mBind.headerInfoTv, service.title)
            mBind.headerInfoTv.setPadding(Utils.dpToPx(context, 10),0,Utils.dpToPx(context, 10),0)
            mBind.btnSeeAll.visibility = View.GONE

            val adapter = SubServiceByCategoryAdapter(context, service.service_item_list, object : SubServiceByCategoryAdapter.OnClickItemListener{
                override fun onClick(propertyService: ServiceByCategoryModel) {
                    onClickListener.onClick(propertyService)
                }
            })
            mBind.recyclerView.recyclerView.layoutManager = AbsoluteFitLayoutManager(context, 1, RecyclerView.HORIZONTAL, false, 2)
            mBind.recyclerView.recyclerView.adapter = adapter
        }
    }

    fun clear() {
        val size: Int = list.size
        if (size > 0) {
            for (i in 0 until size) {
                list.removeAt(0)
            }
            notifyItemRangeRemoved(0, size)
        }
    }

    fun addListRegService() : ArrayList<MainCategoryServiceModel> {
        val list = ArrayList<MainCategoryServiceModel>()
        for (i in 1..10){
            val item = MainCategoryServiceModel("Category Service $i")
            item.id = "$i"
            for (j in 1..5){
                item.service_item_list.add(ServiceByCategoryModel("Cleaning $j", "https://dev.booknow.asia/uploads/0000/82/2022/11/18/699152-15070212070031545453.jpg", "2$i $", "Duration : 1h 30mn"))
            }
            list.add(item)
        }
        return list
    }
}