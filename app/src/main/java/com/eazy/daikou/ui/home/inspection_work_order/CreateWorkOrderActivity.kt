package com.eazy.daikou.ui.home.inspection_work_order

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.base.BaseCameraActivity
import com.eazy.daikou.request_data.request.work_oder_ws.WorkOrderWs
import com.eazy.daikou.request_data.request.work_oder_ws.WorkOrderWs.CallBackWorkOrderListener
import com.eazy.daikou.request_data.static_data.StaticUtilsKey
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.profile.User
import com.eazy.daikou.model.inspection.ItemTemplateModel
import com.eazy.daikou.model.work_order.AssignWorkOrderModel
import com.eazy.daikou.model.work_order.ImageListModel
import com.eazy.daikou.ui.home.inspection_work_order.adapter.ImageListWorkOrderAdapter
import com.eazy.daikou.helper.DateNumPickerFragment
import com.eazy.daikou.ui.sign_up.NationalityActivity
import com.google.gson.Gson
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class CreateWorkOrderActivity : BaseActivity(), View.OnClickListener{

    private lateinit var btnSave: TextView
    private lateinit var subjectEdt:TextView
    private lateinit var noTv:TextView
    private lateinit var startDateTv:TextView
    private lateinit var dueDateTv:TextView
    private lateinit var doneStatusTv:TextView
    private lateinit var departmentTvTv:TextView
    private lateinit var assigneeWorkOrderTv: TextView

    private lateinit var  txtNoImage: TextView
    private lateinit var statusTv:TextView
    private lateinit var priorityTv:TextView
    private lateinit var estimateTimeTv:TextView
    private lateinit var totalComponentListEdt: EditText
    private lateinit var descriptionTv:EditText
    private lateinit var progressBar: ProgressBar
    private lateinit var btnAddImage: ImageView

    private lateinit var imageRecyclerView: RecyclerView
    private lateinit var assignWorkOrderModel: AssignWorkOrderModel
    private lateinit var subItemInspection : ItemTemplateModel.SubItemTemplateModel
    private lateinit var dateFormatServer : SimpleDateFormat
    private lateinit var dateFormShow : SimpleDateFormat

    private val bitmapList: ArrayList<ImageListModel> = ArrayList()
    private var post = 0
    private val REQUEST_IMAGE = 561
    private val REQUEST_CODE_ESTIMATE_TIME = 422
    private val REQUEST_CODE_DEPARTMENT = 332
    private var getStatusVal = ""
    private var getPriorityVal:String = ""
    private var getEstimateTimeVal:String = ""
    private var getStartDateVal = ""
    private var getDueDateVal = ""
    private var getDepartmentIdVal = ""
    private var getAssignIdVal = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_work_order_detail)

        initView()

        initData()

        initAction()

    }

    private fun initView(){
        progressBar = findViewById(R.id.progressItem)
        progressBar.visibility = View.GONE

        btnSave = findViewById(R.id.btnSave)
        btnAddImage = findViewById(R.id.btnAddImage)
        imageRecyclerView = findViewById(R.id.list_image)
        txtNoImage = findViewById(R.id.txtNoImageAdd)

        subjectEdt = findViewById(R.id.subjectWorkOrderEdt)
        noTv = findViewById(R.id.noWorkOrderTv)
        startDateTv = findViewById(R.id.startDateWorkOrderTv)
        dueDateTv = findViewById(R.id.dueDateWorkOrderTv)

        doneStatusTv = findViewById(R.id.doneStatusWorkOrderTv)
        totalComponentListEdt = findViewById(R.id.totalComponentListEdt)
        descriptionTv = findViewById(R.id.descriptionTv)
        statusTv = findViewById(R.id.statusTv)
        priorityTv = findViewById(R.id.priorityTv)
        estimateTimeTv = findViewById(R.id.estimateTimeTv)
        departmentTvTv = findViewById(R.id.departmentTvTv)
        assigneeWorkOrderTv = findViewById(R.id.assigneeWorkOrderTv)

        findViewById<ImageView>(R.id.btnCreateWorkOrder).visibility = View.GONE
        findViewById<LinearLayout>(R.id.layoutMap).visibility = View.INVISIBLE
        findViewById<LinearLayout>(R.id.itemCreateWorkOrderLayout).visibility = View.VISIBLE
        findViewById<LinearLayout>(R.id.menuMainTemplateLayout).visibility = View.GONE
        findViewById<CardView>(R.id.layoutItemImage).visibility = View.GONE
        findViewById<CardView>(R.id.detailItemLayout).visibility = View.GONE

    }

    private fun initData(){
        Utils.customOnToolbar(this@CreateWorkOrderActivity, resources.getString(R.string.create_work_order), backBtnCallBack)

        if (intent != null && intent.hasExtra("subItemInspectionModel")){
            subItemInspection = intent.getSerializableExtra("subItemInspectionModel") as ItemTemplateModel.SubItemTemplateModel
        }
    }

    private var backBtnCallBack = Utils.ClickCallBackBtnListener { finish() }

    private fun initAction(){

        dateFormatServer = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        dateFormShow = SimpleDateFormat("dd, MMM yyyy", Locale.getDefault())

        setDefaultValue()

        setImageRecyclerView(post)

        btnSave.setOnClickListener(this)
        btnAddImage.setOnClickListener(this)
        statusTv.setOnClickListener(this)
        priorityTv.setOnClickListener(this)
        estimateTimeTv.setOnClickListener(this)
        subjectEdt.setOnClickListener(this)
        startDateTv.setOnClickListener(this)
        dueDateTv.setOnClickListener(this)
        assigneeWorkOrderTv.setOnClickListener(this)
        departmentTvTv.setOnClickListener(this)
    }

    @SuppressLint("SetTextI18n")
    private fun setDefaultValue(){
        // Start Date
        var dateToday = Date()
        startDateTv.text = dateFormShow.format(dateToday)
        getStartDateVal = dateFormatServer.format(dateToday)

        // Due Date
        val calendar = Calendar.getInstance()
        calendar.time = dateToday
        calendar.add(Calendar.DATE, 1)
        dateToday = calendar.time
        dueDateTv.text = dateFormShow.format(dateToday)
        getDueDateVal = dateFormatServer.format(dateToday)

        priorityTv.text = Utils.getPriorityOnWorkOrder(this)[1]
        getPriorityVal = Utils.getPriorityOnWorkOrder(this)[1].toString()

        statusTv.text = Utils.getValueFromKeyStatus(this)[resources.getString(R.string.new_).lowercase()]
        getStatusVal = Utils.getValueFromKeyStatus(this)[resources.getString(R.string.new_).lowercase()].toString()

        estimateTimeTv.text = "1 " + resources.getString(R.string.hour).lowercase()
        getEstimateTimeVal = "1"

        doneStatusTv.text = "% ${0}"
        subjectEdt.text = if (subItemInspection.workOrderSubjectDefault != null) subItemInspection.workOrderSubjectDefault else ""
        noTv.text = if (subItemInspection.workOrderNoDefault != null)   subItemInspection.workOrderNoDefault else ""

    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.btnSave -> createWorkOrder()
            R.id.btnAddImage -> startActivityForResult(Intent(this@CreateWorkOrderActivity, BaseCameraActivity::class.java), REQUEST_IMAGE)
            R.id.statusTv -> selectStatusPriorityDuring("status_work_order", getStatusVal)
            R.id.priorityTv -> selectStatusPriorityDuring("priority_work_order", getPriorityVal)
            R.id.estimateTimeTv -> {
                val intent = Intent(this@CreateWorkOrderActivity, NationalityActivity::class.java)
                intent.putExtra("action", "estimate_time")
                startActivityForResult(intent, REQUEST_CODE_ESTIMATE_TIME)
            }
            R.id.subjectEdt -> if (assignWorkOrderModel.status != null) {
                Utils.popupMessage(resources.getString(R.string.subject), assignWorkOrderModel.status, this@CreateWorkOrderActivity)
            }
            R.id.dueDateWorkOrderTv -> selectDate("due_date")
            R.id.startDateWorkOrderTv -> selectDate("start_date")
            R.id.departmentTvTv -> selectAssignee(StaticUtilsKey.department_employee_action)
            R.id.assigneeWorkOrderTv ->  {
                if(getDepartmentIdVal != "") {
                    selectAssignee(StaticUtilsKey.assignee_employee_action)
                } else {
                    Utils.customToastMsgError(this, resources.getString(R.string.please_select_department_before_select_assignee), false)
                }
            }
        }
    }

    private fun selectAssignee(action: String){
        val intent = Intent(this@CreateWorkOrderActivity, SelectDepartmentEmployeeActivity::class.java)
        intent.putExtra("action", action)
        intent.putExtra("assignee_id", getAssignIdVal)
        intent.putExtra("department_id", getDepartmentIdVal)
        startActivityForResult(intent, REQUEST_CODE_DEPARTMENT)
    }

    private fun selectDate(action: String) {
        val calendar = Calendar.getInstance()
        val yy = calendar[Calendar.YEAR]
        val mm = calendar[Calendar.MONTH]
        val dd = calendar[Calendar.DAY_OF_MONTH]
        val datePicker = DatePickerDialog(this,
            { _: DatePicker?, year: Int, monthOfYear: Int, dayOfMonth: Int ->
                calendar[Calendar.YEAR] = year
                calendar[Calendar.MONTH] = monthOfYear
                calendar[Calendar.DAY_OF_MONTH] = dayOfMonth

                if (action == "start_date"){
                    if(dateFormatServer.format(calendar.time) > getDueDateVal){
                        Utils.customToastMsgError(this, resources.getString(R.string.start_date_is_before_due_date), false)
                    } else {
                        startDateTv.text = dateFormShow.format(calendar.time)
                        getStartDateVal = dateFormatServer.format(calendar.time)
                    }
                } else {
                    if(dateFormatServer.format(calendar.time) >= getStartDateVal){
                        dueDateTv.text = dateFormShow.format(calendar.time)
                        getDueDateVal = dateFormatServer.format(calendar.time)
                    } else {
                        Utils.customToastMsgError(this, resources.getString(R.string.due_date_is_after_start_date), false)
                    }
                }

            }, yy, mm, dd
        )

        if (action == "due_date" && getStartDateVal != ""){
            datePicker.datePicker.minDate = getMillisFromDate(getStartDateVal)
        }
        //datePicker.datePicker.minDate = System.currentTimeMillis() - 1000    // For define date today to front
        datePicker.show()
    }

    fun getMillisFromDate(dateFormat: String?): Long {
        var date = Date()
        val formatter = SimpleDateFormat("yyyy-MM-dd")
        try {
            date = formatter.parse(dateFormat)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return date.time
    }

    private fun selectStatusPriorityDuring(action: String, value: String) {
        val dateNumPickerFragment = DateNumPickerFragment.newInstance(action, value)
        dateNumPickerFragment.CallBackListener { title: String, values: String, _: String? ->
            if (title == "status_work_order") {
                statusTv.text = values
                getStatusVal = values
            } else if (title == "priority_work_order") {
                priorityTv.text = values
                getPriorityVal = values
            }
        }
        dateNumPickerFragment.show(supportFragmentManager, "Dialog Fragment")
    }

    private fun getPriority(hashMap: HashMap<Int, String>, value: String): Int {
        for ((key, value1) in hashMap) {
            if (value == value1) {
                return key
            }
        }
        return 1
    }

    private fun getStatus(hashMap: HashMap<String, String>, value: String): String {
        for ((key, value1) in hashMap) {
            if (value == value1) {
                return key
            }
        }
        return ""
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK && requestCode == REQUEST_IMAGE) {
            assert(data != null)
            if (data!!.hasExtra("image_path")) {
                val imagePath = data.getStringExtra("image_path").toString()
                addUrlImage("", imagePath)
                setImageRecyclerView(post)
            }
            if (data.hasExtra("select_image")) {
                val imagePath = data.getStringExtra("select_image").toString()
                addUrlImage("", imagePath)
                setImageRecyclerView(post)
            }
        } else if (resultCode == RESULT_OK && requestCode == REQUEST_CODE_ESTIMATE_TIME) {
            assert(data != null)
            val valueEstimateTime = data!!.getStringExtra("estimate_time")
            val estimateTimeTittle = data.getStringExtra("estimate_time_name")
            estimateTimeTv.text = estimateTimeTittle
            getEstimateTimeVal = valueEstimateTime!!
        } else if (resultCode == RESULT_OK && requestCode == REQUEST_CODE_DEPARTMENT){
            if (data != null){
                if (data.getStringExtra("action").equals(StaticUtilsKey.assignee_employee_action)){
                    assigneeWorkOrderTv.text = data.getStringExtra("name")
                    getAssignIdVal = data.getStringExtra("id").toString()
                    assigneeWorkOrderTv.setBackgroundResource(R.drawable.card_border_black)
                } else {
                    departmentTvTv.text = data.getStringExtra("name")
                    getDepartmentIdVal = data.getStringExtra("id").toString()
                    departmentTvTv.setBackgroundResource(R.drawable.card_border_black)

                    // When Select department
                    assigneeWorkOrderTv.text = ""
                    getAssignIdVal = ""
                }
            }
        }
    }

    private fun addUrlImage(posKeyDefine: String, imagePath: String) {
        val loadedBitmap = BaseCameraActivity.convertByteToBitmap(imagePath)
        post += 1
        val imageListModel = ImageListModel()
        imageListModel.keyImage = posKeyDefine
        imageListModel.bitmapImage = loadedBitmap
        bitmapList.add(imageListModel)
    }

    private fun setImageRecyclerView(post: Int) {
        if (bitmapList.isNotEmpty()) {
            txtNoImage.visibility = View.GONE
        } else {
            txtNoImage.visibility = View.VISIBLE
        }
        imageRecyclerView.layoutManager = GridLayoutManager(this, 1, RecyclerView.HORIZONTAL, false)
        val pickUpAdapter = ImageListWorkOrderAdapter(this, post, bitmapList, onClearImage)
        imageRecyclerView.adapter = pickUpAdapter
        btnAddImage.visibility = Utils.visibleView(bitmapList)
    }

    private val onClearImage: ImageListWorkOrderAdapter.OnClearImage =
        object : ImageListWorkOrderAdapter.OnClearImage {
            override fun onClickRemove(bitmap: ImageListModel, post: Int) {
                if (bitmap.keyImage == "") {
                    removeImage(post, bitmap)
                }
            }

            override fun onViewImage(bitmap: ImageListModel) {
                if (bitmap.bitmapImage != null) {
                    Utils.openImageOnDialog(
                        this@CreateWorkOrderActivity,
                        Utils.convert(bitmap.bitmapImage)
                    )
                }
            }
        }

    private fun removeImage(position: Int, bitmap: ImageListModel) {
        bitmapList.remove(bitmap)
        setImageRecyclerView(position)
    }

    private fun hashMapGetData(): HashMap<String, Any> {
        val hashMap = HashMap<String, Any>()
        val listImage: MutableList<String> = ArrayList()
        if (bitmapList.isEmpty()) {
            listImage.add("")
        }
        for (bitmap in bitmapList) {
            if (bitmap.keyImage == "") {
                listImage.add(Utils.convert(bitmap.bitmapImage))
            }
        }


        hashMap["user_id"] = UserSessionManagement(this@CreateWorkOrderActivity).userId
        hashMap["property_id"] = Gson().fromJson(UserSessionManagement(this).userDetail, User::class.java).activePropertyIdFk

        hashMap["room_item_id"] = subItemInspection.item_room_id
        hashMap["work_order_no"] = if (subItemInspection.workOrderNoDefault != null) subItemInspection.workOrderNoDefault else ""
        hashMap["subject"] = if (subItemInspection.workOrderSubjectDefault != null) subItemInspection.workOrderSubjectDefault else ""
        hashMap["status"] = getStatus(Utils.getValueFromKeyStatus(this@CreateWorkOrderActivity), getStatusVal)
        hashMap["priority"] = getPriority(Utils.getPriorityOnWorkOrder(this@CreateWorkOrderActivity), getPriorityVal)
        hashMap["start_date"] = getStartDateVal
        hashMap["end_date"] = getDueDateVal
        hashMap["work_order_type"] = if (subItemInspection.reportType != null) subItemInspection.reportType else ""
        hashMap["component_list"] = totalComponentListEdt.text.toString().trim { it <= ' ' }
        hashMap["description"] = descriptionTv.text.toString().trim { it <= ' ' }
        hashMap["work_order_images"] = listImage
        hashMap["estimate_duration"] = getEstimateTimeVal
        hashMap["done_percent"] = "0"
        hashMap["department_id"] = getDepartmentIdVal
        hashMap["assignee_user_id"] = getAssignIdVal
        return hashMap
    }


    private fun createWorkOrder() {
        when {
            getDepartmentIdVal == "" -> {
                departmentTvTv.setBackgroundResource(R.drawable.shape_error)
                Utils.customToastMsgError(this, resources.getString(R.string.select_department), false)
            }
            getAssignIdVal == "" -> {
                assigneeWorkOrderTv.setBackgroundResource(R.drawable.shape_error)
                Utils.customToastMsgError(this, resources.getString(R.string.assignee), false)
            }
            else -> {
                btnSave.isClickable = false
                progressBar.visibility = View.VISIBLE

                val hashMap: HashMap<String, Any> = hashMapGetData()
                WorkOrderWs()
                    .createWorkOrder(this@CreateWorkOrderActivity, hashMap, object : CallBackWorkOrderListener {
                    override fun onSuccessDeleteImageWorkOrder(msg: String) {
                        btnSave.isClickable = true
                        progressBar.visibility = View.GONE
                        Utils.customToastMsgError(this@CreateWorkOrderActivity, msg, true)
                        setResult(RESULT_OK)
                        finish()
                    }

                    override fun onFailed(msg: String) {
                        btnSave.isClickable = true
                        progressBar.visibility = View.GONE
                        Utils.customToastMsgError(this@CreateWorkOrderActivity, msg, false)
                    }
                })
            }
        }
    }

}