package com.eazy.daikou.ui.home.service_provider;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.eazy.daikou.R;
import com.eazy.daikou.base.BaseActivity;
import com.eazy.daikou.request_data.request.service_provider_ws.SearchProviderWs;
import com.eazy.daikou.model.my_property.service_provider.LatLngServiceProvider;
import com.eazy.daikou.model.law.LawsDetailModel;
import com.eazy.daikou.ui.home.service_provider.adapter.SearchAdapter;

import java.util.ArrayList;
import java.util.List;

public class SearchServiceProviderActivity extends BaseActivity {

    private SearchAdapter searchAdapter;
    private EditText textSearch;
    private RecyclerView recyclerView;
    private ImageView btnClear;
    private TextView emptyTextTv;
    private ProgressBar progressBar;

    private final List<LatLngServiceProvider> providerList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_service_provider);

        findViewById(R.id.backButton).setOnClickListener(view -> finish());
        textSearch = findViewById(R.id.search_store_name);
        recyclerView = findViewById(R.id.list_provider);
        btnClear = findViewById(R.id.clear);
        emptyTextTv = findViewById(R.id.emptyText);
        progressBar = findViewById(R.id.progressItem);
        progressBar.setVisibility(View.GONE);
        emptyTextTv.setVisibility(View.VISIBLE);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);


        textSearch.setOnEditorActionListener((textView, i, keyEvent) -> {
            if(i== EditorInfo.IME_ACTION_SEARCH) {
               getSearch(textSearch.getText().toString());
            }
            return true;
        });

        textSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(!editable.toString().isEmpty()){
                    btnClear.setVisibility(View.VISIBLE);
                    if (providerList.size() > 0)    providerList.clear();
                    getSearch(editable.toString());
                    btnClear.setOnClickListener(view -> {
                       clearText();
                       textSearch.setText("");
                    });
                }else {
                   clearText();
                }

            }
        });
    }

    private void getSearch(String text){
        progressBar.setVisibility(View.VISIBLE);
        searchAdapter = new SearchAdapter(providerList, itemClickServiceProvider);
        recyclerView.setAdapter(searchAdapter);
        new SearchProviderWs().searchServiceProvider(SearchServiceProviderActivity.this, text, 1, 20, new SearchProviderWs.RequestSearchServiceProvider() {
            @SuppressLint("NotifyDataSetChanged")
            @Override
            public void onLoadServiceProvider(List<LatLngServiceProvider> serviceProvider) {
                if(serviceProvider.size() != 0){
                    if (providerList.size() > 0) providerList.clear();
                    providerList.addAll(serviceProvider);
                    searchAdapter.notifyDataSetChanged();
                    emptyTextTv.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                }else {
                    recyclerView.setVisibility(View.GONE);
                    emptyTextTv.setVisibility(View.VISIBLE);
                }
                progressBar.setVisibility(View.GONE);

            }

            @Override
            public void OnLoadFail(String message) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(SearchServiceProviderActivity.this,message,Toast.LENGTH_LONG).show();
            }
        });
    }

    private final  SearchAdapter.ItemClickServiceProvider itemClickServiceProvider = new SearchAdapter.ItemClickServiceProvider() {
        @Override
        public void onClickItem(LawsDetailModel.Category id, String name, LatLngServiceProvider latLongProvider) {
            Intent intent = new Intent();
            intent.putExtra("id",id.getId());
            intent.putExtra("name",id.getName());
            intent.putExtra("service",latLongProvider);
            setResult(RESULT_OK,intent);
            finish();
        }

        @Override
        public void onClickItemCall(String text) {
            Intent callIntent = new Intent(Intent.ACTION_DIAL);
            callIntent.setData(Uri.parse("tel:" + text));
            callIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(callIntent);
        }


    };

    private void clearText(){
        btnClear.setVisibility(View.GONE);
        if (providerList.size() > 0) providerList.clear();
        progressBar.setVisibility(View.GONE);
        emptyTextTv.setVisibility(View.VISIBLE);
        if (searchAdapter != null)  searchAdapter.notifyDataSetChanged();
    }

}