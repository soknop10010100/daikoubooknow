package com.eazy.daikou.ui.home.my_property_v1.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.eazy.daikou.R;

import java.util.List;

public class ImageSecurityAdapter extends RecyclerView.Adapter<ImageSecurityAdapter.ItemViewHolder> {

    private final List<String> filePathList;
    private final ClickCallBackListener clickCallBackListener;

    public ImageSecurityAdapter(List<String> filePathList, ClickCallBackListener clickCallBackListener) {
        this.filePathList = filePathList;
        this.clickCallBackListener = clickCallBackListener;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rowView = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_model, parent, false);
        return new ItemViewHolder(rowView);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        String filePast = filePathList.get(position);
        if (filePast != null){
            holder.cardImageSmall.setVisibility(View.GONE);
            holder.cardImageSecure.setVisibility(View.VISIBLE);
            Glide.with(holder.imageView).load(filePast).into(holder.imageView);
            holder.imageView.setScaleType(ImageView.ScaleType.FIT_XY);
            holder.cardImageSecure.setOnClickListener(view -> clickCallBackListener.imageClick(filePast));
        }
    }

    @Override
    public int getItemCount() {
        return filePathList.size();
    }

    public  static class ItemViewHolder extends RecyclerView.ViewHolder{
        private final ImageView imageView;
        private final LinearLayout cardImageSecure;
        private final CardView  cardImageSmall;
        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageListSecure);
            cardImageSecure = itemView.findViewById(R.id.cardImageSecure);
            cardImageSmall = itemView.findViewById(R.id.cardImage);
        }
    }

    public interface ClickCallBackListener{
        void imageClick(String urlImage);
    }
}
