package com.eazy.daikou.ui.home.facility_booking.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.facility_booking.CardItemByProperty
import com.eazy.daikou.model.facility_booking.ItemCardBookingModel

class ItemCardBookingAdapter(private val listItem : ArrayList<CardItemByProperty>, private val clickCallBack : ClickCallBackListener) : RecyclerView.Adapter<ItemCardBookingAdapter.ItemViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        return ItemViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.add_to_card_booking_model, parent, false))
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val itemCardBookingModel = listItem[position]
        if (itemCardBookingModel != null){
            holder.itemNameTv.text = itemCardBookingModel.space_name
            holder.priceTv.text = "$ " + itemCardBookingModel.total_amount
            holder.number.text = itemCardBookingModel.quantity
            holder.taxPrice.text = "$ " + getTotalTax(itemCardBookingModel)
            itemCardBookingModel.taxTotal = getTotalTax(itemCardBookingModel)
            if (itemCardBookingModel.isClick){
                holder.iconSelect.background = ResourcesCompat.getDrawable(holder.iconSelect.resources, R.drawable.circle_appbar, null)
            } else {
                holder.iconSelect.background = ResourcesCompat.getDrawable(holder.iconSelect.resources, R.drawable.bg_circle_camera, null)
            }

            Glide.with(holder.imgSpace.context).load(if (itemCardBookingModel.space_image != null) itemCardBookingModel.space_image else R.drawable.no_image).into(holder.imgSpace)

            holder.iconAdd.setOnClickListener{clickCallBack.onClickCallBack(itemCardBookingModel, "add", holder.iconAdd)}
            holder.iconRemove.setOnClickListener{clickCallBack.onClickCallBack(itemCardBookingModel, "remove", holder.iconRemove)}
            holder.iconDeleteItem.setOnClickListener{clickCallBack.onClickCallBack(itemCardBookingModel, "delete_item", holder.iconDeleteItem)}
            holder.imgSpace.setOnClickListener{clickCallBack.onClickCallBack(itemCardBookingModel, "select_in_active", holder.iconSelect)}
        }
    }

    override fun getItemCount(): Int {
        return listItem.size
    }


    class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val itemNameTv: TextView = itemView.findViewById(R.id.itemNameTv)
        val number: TextView = itemView.findViewById(R.id.number)
        val iconRemove: ImageView = itemView.findViewById(R.id.iconMinus)
        val iconAdd: ImageView = itemView.findViewById(R.id.iconAdd)
        val iconDeleteItem: ImageView = itemView.findViewById(R.id.iconDeleteItem)
        val priceTv: TextView = itemView.findViewById(R.id.priceTv)
        val taxPrice: TextView = itemView.findViewById(R.id.taxPrice)
        val imgSpace: ImageView = itemView.findViewById(R.id.imgComplaint)
        val iconSelect: ImageView = itemView.findViewById(R.id.iconSelect)
    }

    interface ClickCallBackListener{
        fun onClickCallBack(itemBooking: CardItemByProperty, action : String, iconAction : ImageView)
    }

    fun clear(){
        val size = listItem.size
        if (size > 0) {
            for (i in 0 until size) {
                listItem.removeAt(0)
            }
            notifyItemRangeRemoved(0, size)
        }
    }

    private fun getTotalTax(itemCardBookingModel : CardItemByProperty) : String {
        var totalPrice = 0.0

        var originalPrice = 0.0
        var numberOfPeopleVal = 1
        if (itemCardBookingModel.price_per_one_quantity != null){
            originalPrice = itemCardBookingModel.price_per_one_quantity!!.toDouble()
        }
        if (itemCardBookingModel.quantity != null){
            numberOfPeopleVal = itemCardBookingModel.quantity!!.toInt()
        }
        try {
            totalPrice = originalPrice * numberOfPeopleVal
        } catch (ex : Exception){
            ex.printStackTrace()
        }

        var vatTaxPrice = 0.0
        if (itemCardBookingModel.tax_vat_percent != null){
            vatTaxPrice = (itemCardBookingModel.tax_vat_percent!!.toDouble() / 100) * totalPrice
        }

        var lightingTaxPrice =  0.0
        if (itemCardBookingModel.tax_lighting_percent != null){
            lightingTaxPrice = totalPrice * 0.2 * (itemCardBookingModel.tax_lighting_percent!!.toDouble() / 100)
        }

        var specificTaxPrice = 0.0
        if (itemCardBookingModel.tax_specific_percent != null){
            specificTaxPrice = (itemCardBookingModel.tax_specific_percent!!.toDouble() / 100) * totalPrice
        }

        val totalTaxPrice = (vatTaxPrice + lightingTaxPrice + specificTaxPrice)

        return "${Utils.formatDecimalFormatValue(totalTaxPrice)}"
    }
}