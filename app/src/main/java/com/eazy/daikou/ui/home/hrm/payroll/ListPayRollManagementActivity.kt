package com.eazy.daikou.ui.home.hrm.payroll

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AbsListView
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.base.MockUpData
import com.eazy.daikou.request_data.request.hr_ws.PayrollManagementWs
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.hr.BasicEmployeeInfo
import com.eazy.daikou.model.hr.PayrollList
import com.eazy.daikou.model.hr.PayrollManagement
import com.eazy.daikou.model.hr.PayrollManagementDetailModel
import com.eazy.daikou.ui.home.hrm.payroll.adapter.PayRollAdapter
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*


class ListPayRollManagementActivity : BaseActivity() {

    private lateinit var btnBack : TextView
    private lateinit var recyclerView : RecyclerView
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var basicSalaryTv : TextView
    private lateinit var startWorkingDateTv : TextView
    private lateinit var designationTv : TextView
    private lateinit var payRollAdapter : PayRollAdapter
    private lateinit var progressBar : ProgressBar
    private lateinit var refreshLayout : SwipeRefreshLayout
    private lateinit var notFoundTv : TextView
    private lateinit var btnActionHide : ImageView
    private var basicEmployeeInformation = BasicEmployeeInfo()

    private val listPayrollManagement = ArrayList<PayrollList>()

    private var currentItem = 0
    private var total : Int = 0
    private var scrollDown : Int = 0
    private var currentPage : Int = 1
    private var size : Int = 10
    private var isScrolling = false

    // From Notification
    private var action = ""
    private var payRollId = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_pay_roll_management)

        initView()

        initData()

        initAction()

    }

    private fun initView(){
        btnBack = findViewById(R.id.btn_back)
        recyclerView = findViewById(R.id.list_pay_roll)
        startWorkingDateTv = findViewById(R.id.startWorkingDateTv)
        designationTv = findViewById(R.id.designationTv)
        basicSalaryTv = findViewById(R.id.basic_salary)
        progressBar = findViewById(R.id.progressItem)
        refreshLayout = findViewById(R.id.swipe_layouts)
        btnActionHide = findViewById(R.id.btnActionHide)

        notFoundTv = findViewById(R.id.not_found)
    }

    private fun initData(){
        if (intent != null && intent.hasExtra("action")){
            action = intent.getStringExtra("action").toString()
            if (intent != null && intent.hasExtra("id")) {
                payRollId = intent.getStringExtra("id").toString()
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun initAction(){
        btnBack.setOnClickListener { finish() }

        linearLayoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = linearLayoutManager

        refreshLayout.setColorSchemeResources(R.color.colorPrimary)
        refreshLayout.setOnRefreshListener { this.refreshList() }

        btnActionHide.setOnClickListener{ actionHindBasicSalary() }

        initRecyclerView()

        onScrollComplainSolution()

    }

    private fun refreshList() {
        currentPage = 1
        size = 10
        isScrolling = true
        payRollAdapter.clear()

        requestListPayRole()
    }

    private fun initRecyclerView(){
        payRollAdapter = PayRollAdapter(listPayrollManagement, itemClick)
        recyclerView.adapter = payRollAdapter

        requestListPayRole()
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun actionHindBasicSalary(){
        if (basicSalaryTv.text == "***"){
            basicSalaryTv.text = basicEmployeeInformation.basic_salary
            btnActionHide.setImageResource(R.drawable.ic_remove_red)
            MockUpData.IS_CLICK_HIDE = false
            payRollAdapter.notifyDataSetChanged()
        } else {
            basicSalaryTv.text = "***"
            MockUpData.IS_CLICK_HIDE = true
            btnActionHide.setImageResource(R.drawable.ic_home_hidden)
            payRollAdapter.notifyDataSetChanged()
        }
    }

    private val itemClick = object : PayRollAdapter.ItemClickListener{
        override fun onLoadSuccess(data: PayrollList) {
            startPayrollDetail(data.id)
        }
    }

    private fun startPayrollDetail(payRollId : String){
        val intent = Intent(this@ListPayRollManagementActivity, PayRollDetailActivity::class.java)
        intent.putExtra("payroll_id", payRollId)
        startActivity(intent)
    }

    private fun requestListPayRole(){
        PayrollManagementWs().getListPayRollList(this, currentPage, MockUpData.userBusinessKey(UserSessionManagement(this)), object : PayrollManagementWs.OnCallBackListener{
            @SuppressLint("NotifyDataSetChanged")
            override fun onLoadListSuccessFull(listPayRoll: PayrollManagement) {
                progressBar.visibility = View.GONE
                refreshLayout.isRefreshing = false
                setHeaderValue(listPayRoll.basic_employee_info)
                listPayrollManagement.addAll(listPayRoll.payroll_list)

                if(listPayrollManagement.size == 0){
                    notFoundTv.visibility = View.VISIBLE
                }else{
                    notFoundTv.visibility = View.GONE
                }

                payRollAdapter.notifyDataSetChanged()

                // Start detail from notification after request list already
                if (action == "payroll") {
                    startPayrollDetail(payRollId)
                    action = ""
                }
            }

            // This method not use for here
            override fun onSuccessDataPayrollDetail(listPayRoll: PayrollManagementDetailModel) {}

            override fun onLoadFail(message: String) {
                progressBar.visibility = View.GONE
                refreshLayout.isRefreshing = false
                Utils.customToastMsgError(this@ListPayRollManagementActivity, message, false)
            }

        })
    }

    private fun onScrollComplainSolution() {
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true
                }
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                currentItem = linearLayoutManager.childCount
                total = linearLayoutManager.itemCount
                scrollDown = linearLayoutManager.findFirstVisibleItemPosition()
                if (dy > 0) {
                    if (isScrolling && currentItem + scrollDown == total) {
                        if (total == size) {
                            isScrolling = false
                            currentPage++
                            progressBar.visibility = View.VISIBLE
                            recyclerView.scrollToPosition(listPayrollManagement.size - 1)
                            initRecyclerView()
                            size += 10
                        } else {
                            progressBar.visibility = View.GONE
                        }
                    }
                }
            }
        })
    }

    @SuppressLint("SetTextI18n", "SimpleDateFormat")
    private fun setHeaderValue(basicEmployeeInfo: BasicEmployeeInfo){
        basicEmployeeInformation = basicEmployeeInfo
        if (basicSalaryTv.text == "***"){
            basicSalaryTv.text = "***"
            MockUpData.IS_CLICK_HIDE = true
        } else {
            basicSalaryTv.text = basicEmployeeInfo.basic_salary
            btnActionHide.setImageResource(R.drawable.ic_remove_red)
            MockUpData.IS_CLICK_HIDE = false
        }

        val outputFormat: DateFormat = SimpleDateFormat("dd-MMMM-yyyy", Locale.getDefault())
        val inputFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        val date: Date = inputFormat.parse(basicEmployeeInfo.joining_date) as Date
        val startDateWorking: String = outputFormat.format(date)

        startWorkingDateTv.text = if (basicEmployeeInfo.joining_date != "") startDateWorking else "- - -"
        designationTv.text = basicEmployeeInfo.designation
    }
}