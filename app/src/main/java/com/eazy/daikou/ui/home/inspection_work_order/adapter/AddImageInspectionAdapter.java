package com.eazy.daikou.ui.home.inspection_work_order.adapter;

import android.graphics.BitmapFactory;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.eazy.daikou.R;
import com.eazy.daikou.model.inspection.ImageInspectionModel;

import java.util.List;

public class AddImageInspectionAdapter extends RecyclerView.Adapter<AddImageInspectionAdapter.ItemViewHolder> {

    private final ClickCallBack clickCallBack;
    private final List<ImageInspectionModel> list;

    public  AddImageInspectionAdapter(List<ImageInspectionModel> list, ClickCallBack clickCallBack){
        this.clickCallBack = clickCallBack;
        this.list = list;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.image_model_inspection, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {

        if (list.size() > position) {
            ImageInspectionModel imageInspectionModel = list.get(position);
            if (imageInspectionModel != null) {

                setImage(imageInspectionModel.getFilePath(), holder.iconAdd, holder.iconDelete);

                if (imageInspectionModel.getFilePath() != null && imageInspectionModel.isAlreadySaveImage()){
                    holder.iconAdd.setOnClickListener(v ->
                            clickCallBack.onClickViewImage(imageInspectionModel.getFilePath()));
                    holder.iconDelete.setOnClickListener(v ->
                            clickCallBack.onClickDeleteImage(imageInspectionModel.getDocumentId(), imageInspectionModel.getFileUploadId()));

                } else {    // Maybe not in this condition , just test and prevent
                    if (imageInspectionModel.getFilePath() != null && !imageInspectionModel.getFilePath().equals("")){
                        holder.iconAdd.setOnClickListener(v ->
                                clickCallBack.onClickViewImage(imageInspectionModel.getFilePath()));
                    } else {
                        holder.iconAdd.setOnClickListener(v ->
                                clickCallBack.onClickAddImage(holder.iconAdd, holder.iconDelete,position + 1));

                    }
                     holder.iconDelete.setOnClickListener(v ->
                            clickCallBack.onClickDeleteImage(imageInspectionModel.getDocumentId(), ""));

                }
            }
        } else {
            setImage("", holder.iconAdd, holder.iconDelete);

            holder.iconAdd.setOnClickListener(v ->
                    clickCallBack.onClickAddImage(holder.iconAdd, holder.iconDelete,position + 1));

            holder.iconDelete.setOnClickListener(v ->
                    clickCallBack.onClickDeleteImage("", ""));
        }

    }

    @Override
    public int getItemCount() {
        if (list.size() == 1){
            return list.size() + 3;
        } else if (list.size() == 2){
            return list.size() + 2;
        } else if (list.size() == 3){
            return list.size() + 1;
        } else {
            return 4;
        }
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder{

        private final ImageView iconAdd, iconDelete;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            iconAdd = itemView.findViewById(R.id.iconAdd);
            iconDelete = itemView.findViewById(R.id.iconDelete);
        }
    }

    private void setImage(String value, ImageView imageView, ImageView iconRemove){
        if (value != null && !value.equals("")) {
            if(value.contains(".jpg") || value.contains(".jpeg") || value.contains(".png")) {
                Glide.with(imageView).load(value).into(imageView);
            } else {
                byte[] imageAsBytes = Base64.decode(value, Base64.DEFAULT);
                imageView.setImageBitmap(BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length));
            }
            iconRemove.setVisibility(View.VISIBLE);
        } else {
            Glide.with(imageView).load(R.drawable.ic_add_64).into(imageView);
            iconRemove.setVisibility(View.GONE);
        }
    }

    public interface ClickCallBack{
        void onClickAddImage(ImageView imageView, ImageView iconDelete,int position);
        void onClickViewImage(String url);
        void onClickDeleteImage(String document_id, String fileUploadId);
    }
}
