package com.eazy.daikou.ui.home.data_record.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.helper.DateUtil
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.data_record.SubListDataRecord

class AdapterOpportunityDR (private val context : Context, private val action : String, private val dataRecordList : ArrayList<SubListDataRecord>, private val callBackHoldAndReserved : CallBackHoldAndReservedListener)
    : RecyclerView.Adapter<AdapterOpportunityDR.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
       val view = LayoutInflater.from(context).inflate(R.layout.custom_view_opportunity_data_record, parent, false)
        return ViewHolder(view)
    }

    @SuppressLint("SuspiciousIndentation", "ResourceAsColor")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
         val subDataRecord : SubListDataRecord = dataRecordList[position]
            if (subDataRecord != null){
                when (action) {
                    "opportunity" -> {
                        holder.linearOpportunity.visibility = View.VISIBLE
                        Utils.setValueOnText(holder.opportunityNameTv, subDataRecord.opportunityName)
                        Utils.setValueOnText(holder.projectNameTv, subDataRecord.projectName)
                        Utils.setValueOnText(holder.createByTv,  String.format("%s %s", DateUtil.formatDateComplaint(subDataRecord.createdDate), DateUtil.formatDateToConvertOnlyTimeZoneNoSecond(subDataRecord.createdDate)))

                        // Display Status
                        displayStatus(if (subDataRecord.status != null) subDataRecord.status!! else "", holder.statusTv, holder.style)

                    }

                    "quotation" -> {
                        holder.linearQuotation.visibility = View.VISIBLE
                        Utils.setValueOnText(holder.noQuotationTv, subDataRecord.no)
                        Utils.setValueOnText(holder.projectNameQuotationTv, subDataRecord.projectName)
                        Utils.setValueOnText(holder.unitTypeTv, subDataRecord.unitType)
                        Utils.setValueOnText(holder.unitNoQuotationTv, subDataRecord.unitNo)
                        Utils.setValueOnText(holder.streetFloorTv, subDataRecord.streetFloor)
                        Utils.setValueOnText(holder.priceTv, subDataRecord.price)

                        holder.createByQuotationTv.text = if (subDataRecord.createdDate != null) String.format("%s %s", DateUtil.formatDateComplaint(subDataRecord.createdDate), DateUtil.formatDateToConvertOnlyTimeZoneNoSecond(subDataRecord.createdDate)) else ". . ."
                        holder.expiredDateTv.text = if (subDataRecord.expire_date != null) Utils.formatDateFromString("yyyy-MM-dd", "dd-MMM-yyyy", subDataRecord.expire_date) else ". . ."


                        // Display Status
                        displayStatus(if (subDataRecord.status != null) subDataRecord.status!! else "", holder.statusQuotationTv, holder.style)
                    }

                    "hold_reserved" -> {
                        holder.linearHoldAndReserved.visibility = View.VISIBLE
                        Utils.setValueOnText(holder.noHoldAndReservedTv, subDataRecord.no)
                        Utils.setValueOnText(holder.unitNoTv, subDataRecord.unitNo)
                        Utils.setValueOnText(holder.projectNameHoldTv, subDataRecord.projectName)
                        Utils.setValueOnText(holder.amountTv, subDataRecord.amount)
                        Utils.setValueOnText(holder.typeTv, subDataRecord.type)
                        holder.startDateTv.text = if (subDataRecord.startDate != null)
                            Utils.formatDateFromString("yyyy-MM-dd", "dd-MMM-yyyy", subDataRecord.startDate) else ". . ."

                        if (subDataRecord.endDate != null) {
                            holder.endDateTv.text =
                                Utils.formatDateFromString("yyyy-MM-dd", "dd-MMM-yyyy", subDataRecord.endDate)
                            holder.endDateTv.append(if (subDataRecord.endTime != null)
                                String.format("%s %s", "", Utils.formatDateTime(subDataRecord.endTime, "kk:mm:ss", "hh:mm a"))  else "")

                        } else {
                            holder.endDateTv.text = ". . ."
                        }

                        // Display Status
                        displayStatus(if (subDataRecord.status != null) subDataRecord.status!! else "", holder.statusHoldTv, holder.style)
                    }

                    "booking" -> {
                        holder.linearBooking.visibility = View.VISIBLE
                        Utils.setValueOnText(holder.noBookingTv, subDataRecord.no)
                        Utils.setValueOnText(holder.projectNameBookingTv, subDataRecord.projectName)
                        Utils.setValueOnText(holder.unitNoBookingTv, subDataRecord.unitNo)
                        Utils.setValueOnText(holder.amountBookingTv, subDataRecord.amount)
                        holder.startDateBookingTv.text = if (subDataRecord.startDate != null)
                            Utils.formatDateFromString("yyyy-MM-dd", "dd-MMM-yyyy", subDataRecord.startDate) else ". . ."

                        if (subDataRecord.endDate != null) {
                            holder.endDateBookingTv.text =
                                Utils.formatDateFromString("yyyy-MM-dd", "dd-MMM-yyyy", subDataRecord.endDate)
                            holder.endDateBookingTv.append(if (subDataRecord.endTime != null)
                                String.format("%s %s", "", Utils.formatDateTime(subDataRecord.endTime, "kk:mm:ss", "hh:mm a"))  else "")

                        } else {
                            holder.endDateTv.text = ". . ."
                        }

                        // Display Status
                        displayStatus(if (subDataRecord.status != null) subDataRecord.status!! else "", holder.statusBookingTv, holder.style)
                    }

                    "sale_agreement" -> {
                        holder.linearSpa.visibility = View.VISIBLE
                        Utils.setValueOnText(holder.projectNameSpaTv, subDataRecord.projectName)
                        Utils.setValueOnText(holder.noSpaTv, subDataRecord.no)
                        Utils.setValueOnText(holder.unitNoSpaTv, subDataRecord.unitNo)
                        Utils.setValueOnText(holder.discountSpaTv, subDataRecord.discount)
                        Utils.setValueOnText(holder.paymentTernTypeSpaTv, subDataRecord.payment_tern_type)
                        holder.purchaseDatePsaTv.text = if ( subDataRecord.purchase_date != null) Utils.formatDateFromString("yyyy-MM-dd", "dd-MMM-yyyy", subDataRecord.purchase_date) else ". . ."

                        // Display Status
                        displayStatus(if (subDataRecord.status != null) subDataRecord.status!! else "", holder.statusSpaTv, holder.style)
                    }
                }
                holder.itemView.setOnClickListener{callBackHoldAndReserved.clickItemHoldAndReservedListener(subDataRecord)}
            }
    }

    override fun getItemCount(): Int {
        return dataRecordList.size
    }

    class ViewHolder (view : View) : RecyclerView.ViewHolder(view){
        var linearOpportunity : LinearLayout = view.findViewById(R.id.linearOpportunity)
        var linearQuotation : LinearLayout = view.findViewById(R.id.linearQuotation)
        var linearHoldAndReserved : LinearLayout = view.findViewById(R.id.linearHoldAndReserved)
        var linearBooking : LinearLayout = view.findViewById(R.id.linearBooking)
        var linearSpa : LinearLayout = view.findViewById(R.id.linearSpa)
        var style : View = view.findViewById(R.id.style)

        //recycler opportunity
        var projectNameTv : TextView = view.findViewById(R.id.projectNameTv)
        var createByTv : TextView = view.findViewById(R.id.createByTv)
        var opportunityNameTv : TextView = view.findViewById(R.id.opportunityNameTv)
        var statusTv : TextView = view.findViewById(R.id.statusTv)

        //recycler Quotation
        var noQuotationTv : TextView = view.findViewById(R.id.noQuotationTv)
        var createByQuotationTv : TextView = view.findViewById(R.id.createByQuotationTv)
        var projectNameQuotationTv : TextView = view.findViewById(R.id.projectNameQuotationTv)
        var unitTypeTv : TextView = view.findViewById(R.id.unitTypeTv)
        var unitNoQuotationTv : TextView = view.findViewById(R.id.unitNoQuotationTv)
        var priceTv : TextView = view.findViewById(R.id.priceTv)
        var streetFloorTv : TextView = view.findViewById(R.id.streetFloorTv)
        var expiredDateTv : TextView = view.findViewById(R.id.expiredDateTv)
        var statusQuotationTv : TextView = view.findViewById(R.id.statusQuotationTv)

        //recycler holdAndReserved
        var noHoldAndReservedTv : TextView = view.findViewById(R.id.noHoldAndReservedTv)
        var projectNameHoldTv : TextView = view.findViewById(R.id.projectNameHoldTv)
        var unitNoTv : TextView = view.findViewById(R.id.unitNoTv)
        var amountTv : TextView = view.findViewById(R.id.amountTv)
        var typeTv : TextView = view.findViewById(R.id.typeTv)
        var startDateTv : TextView = view.findViewById(R.id.startDateTv)
        var endDateTv : TextView = view.findViewById(R.id.endDateTv)
        var statusHoldTv : TextView = view.findViewById(R.id.statusHoldTv)

        //Recycler Booking
        var noBookingTv : TextView = view.findViewById(R.id.noBookingTv)
        var projectNameBookingTv : TextView = view.findViewById(R.id.projectNameBookingTv)
        var unitNoBookingTv : TextView = view.findViewById(R.id.unitNoBookingTv)
        var amountBookingTv : TextView = view.findViewById(R.id.amountBookingTv)
        var startDateBookingTv : TextView = view.findViewById(R.id.startDateBookingTv)
        var endDateBookingTv : TextView = view.findViewById(R.id.endDateBookingTv)
        var statusBookingTv : TextView = view.findViewById(R.id.statusBookingTv)

        //Recycler Spa
        var noSpaTv : TextView = view.findViewById(R.id.noSpaTv)
        var projectNameSpaTv : TextView = view.findViewById(R.id.projectNameSpaTv)
        var unitNoSpaTv : TextView = view.findViewById(R.id.unitNoSpaTv)
        var discountSpaTv : TextView = view.findViewById(R.id.discountSpaTv)
        var paymentTernTypeSpaTv : TextView = view.findViewById(R.id.paymentTernTypeSpaTv)
        var purchaseDatePsaTv : TextView = view.findViewById(R.id.purchaseDatePsaTv)
        var statusSpaTv : TextView = view.findViewById(R.id.statusSpaTv)
    }

    private fun displayStatus(status : String, txtStatus : TextView, txtStyle: View){
        var color = R.color.appBarColor
        var valStatus = "- - -"
        when (status) {

            "qualified" -> {
                valStatus = context.getString(R.string.qualified)
                color = R.color.appBarColorOld
            }

            "proposal" -> {
                valStatus = context.getString(R.string.negotiation)
                color = R.color.gray
            }

            "quotation" -> {
                valStatus = context.getString(R.string.quotation)
                color = R.color.blue
            }

            "negotiation" -> {
                valStatus = context.getString(R.string.negotiation)
                color = R.color.green
            }

            "draft" -> {
                valStatus = context.getString(R.string.draft)
                color = R.color.appBarColorOld
            }

            "need_review" -> {
                valStatus = context.getString(R.string.need_review)
                color = R.color.gray
            }

            "in_review" -> {
                valStatus = context.getString(R.string.in_review)
                color = R.color.yellow
            }

            "presented" -> {
                valStatus = context.getString(R.string.presented)
                color = R.color.blue
            }

            "accepted" -> {
                valStatus = context.getString(R.string.accepted)
                color = R.color.green
            }

            "refund" -> {
                valStatus = context.getString(R.string.refund)
                color = R.color.blue
            }

            "changed" -> {
                valStatus = context.getString(R.string.changed)
                color = R.color.yellow
            }

            "booking" -> {
                valStatus = context.getString(R.string.booking)
                color = R.color.blue
            }
            
            "spa" -> {
                valStatus = context.getString(R.string.spa)
                color = R.color.blue
            }
            
            "processing" -> {
                valStatus = context.getString(R.string.processing)
                color = R.color.yellow
            }
            
            "payment" -> {
                valStatus = context.getString(R.string.payment)
                color = R.color.appBarColorOld
            }

            "grr" -> {
                valStatus = context.getString(R.string.grr)
                color = R.color.gray
            }
            
            "termination_cancellation" -> {
                valStatus = context.getString(R.string.termintion_cancellation)
                color = R.color.appBarColorOld
            }

            "approved" ->{
                valStatus = context.getString(R.string.approved)
                color = R.color.green
            }
            "closed_lost" -> {
                valStatus = context.getString(R.string.closed_lost)
                color = R.color.red
            }

            "pending" -> {
                valStatus = context.getString(R.string.pending)
                color = R.color.yellow
            }

            "rejected" -> {
                valStatus = context.getString(R.string.rejected)
                color = R.color.red
            }

            "closed_won" ->{
                valStatus = context.getString(R.string.closed_won)
                color = R.color.green
            }
            "denied" -> {
                valStatus = context.getString(R.string.denied)
                color = R.color.red
            }

            "close" -> {
                valStatus = context.getString(R.string.close)
                color = R.color.red
            }
        }

        txtStatus.text = valStatus
        txtStatus.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(context, color))
        txtStyle.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(context, color))
    }

    interface CallBackHoldAndReservedListener{
        fun clickItemHoldAndReservedListener(subDataRecord: SubListDataRecord)
    }

    fun clear() {
        val size: Int = dataRecordList.size
        if (size > 0) {
            for (i in 0 until size) {
                dataRecordList.removeAt(0)
            }
            notifyItemRangeRemoved(0, size)
        }
    }

}