package com.eazy.daikou.ui.home.development_project.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.model.home_page.home_project.BranchTypes
import com.squareup.picasso.Picasso

class CategoryAdapter(private val listCategory: List<BranchTypes>, private val clickCategory: OnclickCategory) :
    RecyclerView.Adapter<CategoryAdapter.ViewHolder>() {

    class ViewHolder(itemView: View):RecyclerView.ViewHolder(itemView) {
        val textType: TextView = itemView.findViewById(R.id.name_item_hr)
        val imageType: ImageView = itemView.findViewById(R.id.imageIcon)
        val btnType: CardView = itemView.findViewById(R.id.card_hr)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.custom_category_item_property,parent,false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.textType.text = listCategory[position].value
        Picasso.get().load(listCategory[position].icon).into(holder.imageType)

        holder.btnType.setOnClickListener(CustomSetOnClickViewListener {
            clickCategory.clickCategory(listCategory[position])
        })
    }

    override fun getItemCount(): Int {
        return listCategory.size
    }

    interface OnclickCategory{
        fun clickCategory(business: BranchTypes)
    }

}