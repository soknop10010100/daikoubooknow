package com.eazy.daikou.ui.home.my_document

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseFragment
import com.eazy.daikou.helper.AppAlertCusDialog
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.hr.ItemHR
import com.eazy.daikou.ui.home.hrm.adapter.ItemHRAdapter
import java.util.*

class MyDocumentFragment : BaseFragment() {

    private lateinit var itemRecyclerView : RecyclerView
    private lateinit var progressBar: ProgressBar

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rowView = inflater.inflate(R.layout.activity_my_document_acitvity, container, false)

        initView(rowView)

        initAction()

        return rowView
    }

    private fun initView(rowView : View){
        rowView.findViewById<View>(R.id.iconBack).visibility = View.GONE
        val titleToolbar = rowView.findViewById<TextView>(R.id.titleToolbar)
        titleToolbar.text = Utils.getText(mActivity, R.string.my_document).uppercase(Locale.ROOT)

        itemRecyclerView = rowView.findViewById(R.id.list_service_document)
        progressBar = rowView.findViewById(R.id.progressItem)
        progressBar.visibility = View.GONE

    }

    private fun initAction(){

        initItemRecyclerView()
    }

    private fun initItemRecyclerView(){
        itemRecyclerView.layoutManager = Utils.spanGridLayoutManager(mActivity, getListHomeMenu().size)
        val adapter = ItemHRAdapter(mActivity, "sub_home_menu", getListHomeMenu(), onClickCallBack)
        itemRecyclerView.adapter = adapter
    }

    private var onClickCallBack = object : ItemHRAdapter.ItemClickOnListener{

        override fun onItemClickId(id: Int) {}

        override fun onItemClickAction(action: String?) {
            AppAlertCusDialog.underConstructionDialog(mActivity, resources.getString(R.string.coming_soon))
            return
            val intent = Intent(mActivity, ListAllPropertyAndMyDocumentDetailActivity::class.java)
            intent.putExtra("action_type", action)
            startActivity(intent)
        }
    }

    private fun getListHomeMenu(): List<ItemHR> {
        val homeViewModelList: MutableList<ItemHR> = java.util.ArrayList()
        homeViewModelList.add(ItemHR("quotation", R.drawable.ic_home_notebook_v2, mActivity.getString(R.string.quotation),"Your quotation histories", R.color.calendar_active_month_bg))
        homeViewModelList.add(ItemHR("complain", R.drawable.ic_home_notebook_v2, mActivity.getString(R.string.complain), "You can view your complaints", R.color.greenSea))
        homeViewModelList.add(ItemHR("invoice", R.drawable.ic_home_notebook_v2, mActivity.getString(R.string.invoice),"See your invoice", R.color.yellow))
        homeViewModelList.add(ItemHR("sale_agreement", R.drawable.ic_home_notebook_v2, mActivity.getString(R.string.sale_agreement),"See sale agreements you have made", R.color.blue))
        homeViewModelList.add(ItemHR("lease_agreement", R.drawable.ic_home_resignation_v2, mActivity.getString(R.string.lease_agreement), "View your lease agreement", R.color.color_wooden))
        homeViewModelList.add(ItemHR("inspection", R.drawable.ic_home_inspection, mActivity.getString(R.string.inspection),"Unit inspection histories", R.color.appBarColorOld))
        homeViewModelList.add(ItemHR("emergency", R.drawable.ic_emergency_notification, mActivity.getString(R.string.emergency),"Histories of your emergency", R.color.red))
        homeViewModelList.add(ItemHR("parking", R.drawable.home_parking, mActivity.getString(R.string.parking),"You can see your parking record here", R.color.light_blue_900))
        return homeViewModelList
    }

}