package com.eazy.daikou.ui.home.complaint_solution.complaint

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts.StartActivityForResult
import androidx.cardview.widget.CardView
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.base.MockUpData
import com.eazy.daikou.databinding.ActivityComplaintsSolutionBinding
import com.eazy.daikou.helper.*
import com.eazy.daikou.model.complaint.ComplaintListModel
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.request_data.request.complain_ws.ComplaintListWS
import com.eazy.daikou.ui.home.complaint_solution.complaint.adapter.ComplaintSolutionAdapter
import com.eazy.daikou.ui.home.evaluate.EvaluateUnitScanActivity
import com.eazy.daikou.ui.home.front_desk.FrontDeskCreateActivity
import libs.mjn.prettydialog.PrettyDialog

class ComplaintsSolutionActivity : BaseActivity() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var progressBar : ProgressBar

    private var currentItem = 0
    private var total: Int = 0
    private var scrollDown: Int = 0
    private var currentPage: Int = 1
    private var size: Int = 10
    private var isScrolling = false

    private var listType = ""
    private var actionPostType = ""
    private var accountId = ""
    private var accountName = ""

    private lateinit var adapterListComplaint: ComplaintSolutionAdapter
    private lateinit var layoutManager: LinearLayoutManager
    private var listDataListComplaint = ArrayList<ComplaintListModel>()

    // ViewBinding
    private lateinit var viewBinding : ActivityComplaintsSolutionBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewBinding = ActivityComplaintsSolutionBinding.inflate(layoutInflater)
        setContentView(viewBinding.root)

        // Init View
        recyclerView = findViewById(R.id.recyclerView)
        progressBar = findViewById(R.id.progressItem)

        initData()

        initAction()

    }

    private fun initData() {

        actionPostType = GetDataUtils.getDataFromString("action", this)     // list_type is required. Available (by_account, by_user, by_uploader)

        listType = GetDataUtils.getDataFromString("list_type", this)     // list_type is required. Available (by_account, by_user, by_uploader)

        val user = MockUpData.getUserItem(UserSessionManagement(this))
        accountId = Utils.validateNullValue(user.accountId)
        accountName = Utils.validateNullValue(user.accountName)

        Utils.customOnToolbar(this, titleHeader()) { finish() }

    }

    private fun titleHeader() : String {
        if (listType == "by_user")  return resources.getString(R.string.complaint_frontdesk_evaluate)

        return when (actionPostType) {
            "evaluation" -> {
                resources.getString(R.string.evaluation)
            }
            "frontdesk" -> {
                resources.getString(R.string.front_desk)
            }
            else -> {
                resources.getString(R.string.complaints_solutions)
            }
        }
    }

    private fun initAction() {

        layoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = layoutManager

        initItemRecycle()

        initOnScrollItem()

        viewBinding.body.swipeLayouts.setColorSchemeResources(R.color.colorPrimary)
        viewBinding.body.swipeLayouts.setOnRefreshListener { this.onRefreshList() }

        viewBinding.createFrontDesk.setOnClickListener(CustomSetOnClickViewListener {
            if (listType == "by_uploader"){
                startCreateComplaint(actionPostType)
            } else {
                AppAlertCusDialog.optionRequestDialog(this, PrettyDialog(this)) { action ->
                    if (action == "frontdesk" && !Utils.isEmployeeRole(this)){
                        AppAlertCusDialog.underConstructionDialog(this, resources.getString(R.string.you_do_not_permission_to_access))
                    } else {
                        startCreateComplaint(action)
                    }
                }
            }
        })

        initClickMenuCategory()

    }

    private fun initClickMenuCategory() {
        findViewById<CardView>(R.id.cardCategoryLayout).visibility =
            if (listType == "by_uploader")  View.VISIBLE else View.GONE

        val categoryLayout: LinearLayout = findViewById(R.id.categoryLayout)
        for (i in 0 until categoryLayout.childCount) {
            categoryLayout.getChildAt(i).setOnClickListener { v ->
                setBackground(v.id, categoryLayout)
                listType = when (i) {
                    0 -> {
                        "by_uploader"
                    }
                    else -> {
                        "by_account"
                    }
                }
                onRefreshList()
            }
        }
    }

    private fun setBackground(id: Int, categoryLayout: LinearLayout) {
        for (i in 0 until categoryLayout.childCount) {
            val rowView: View = categoryLayout.getChildAt(i)
            if (rowView is TextView) {
                rowView.setTextColor(
                    Utils.getColor(this,
                        if (id == rowView.id) R.color.appBarColorOld else R.color.gray
                    )
                )
            }
        }
    }

    private fun initItemRecycle() {
        adapterListComplaint =
            ComplaintSolutionAdapter(actionPostType, listDataListComplaint, this, onItemComplaintClick)
        recyclerView.adapter = adapterListComplaint

        requestServiceWs()
    }

    private val onItemComplaintClick = object : ComplaintSolutionAdapter.OnItemClick {
        override fun clickItem(complaintModel: ComplaintListModel) {
            val intent =
                Intent(this@ComplaintsSolutionActivity, ComplaintDetailActivity::class.java)
            intent.putExtra("id", complaintModel.id)
            intent.putExtra("action_post", complaintModel.post_type)
            resultLauncher.launch(intent)
        }
    }

    private fun onRefreshList() {
        currentPage = 1
        size = 10
        isScrolling = true
        adapterListComplaint.clear()

        requestServiceWs()
    }

    private fun initOnScrollItem() {
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true
                }
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                currentItem = layoutManager.childCount
                total = layoutManager.itemCount
                scrollDown = layoutManager.findFirstVisibleItemPosition()
                if (dy > 0) {
                    if (isScrolling && currentItem + scrollDown == total) {
                        if (total == size) {
                            isScrolling = false
                            currentPage++
                            progressBar.visibility = View.VISIBLE
                            recyclerView.scrollToPosition(listDataListComplaint.size - 1)
                            initItemRecycle()
                            size += 10
                        } else {
                            progressBar.visibility = View.GONE
                        }
                    }
                }
            }
        })
    }

    private fun requestServiceWs() {
        progressBar.visibility = View.VISIBLE
        ComplaintListWS.getComplaintSolutionWs(
            this,
            currentPage,
            10,
            listType,
            actionPostType,
            accountId,
            callBackListener
        )
    }

    private val callBackListener = object : ComplaintListWS.OnResponseList {
        @SuppressLint("NotifyDataSetChanged")
        override fun onSuccess(list: List<ComplaintListModel>) {
            progressBar.visibility = View.GONE
            viewBinding.body.swipeLayouts.isRefreshing = false
            listDataListComplaint.addAll(list)
            adapterListComplaint.notifyDataSetChanged()

            Utils.validateViewNoItemFound(
                this@ComplaintsSolutionActivity,
                recyclerView,
                listDataListComplaint.size <= 0
            )
        }

        override fun onError(msg: String) {
            progressBar.visibility = View.GONE
            viewBinding.body.swipeLayouts.isRefreshing = false

            Utils.customToastMsgError(this@ComplaintsSolutionActivity, msg, false)
        }
    }

    private val resultLauncher =
        registerForActivityResult(StartActivityForResult()) { result: ActivityResult ->
            if (result.resultCode == RESULT_OK) {
                // After Create
                onRefreshList()

                // Send to main action, request service create
                val intent = Intent(BroadcastTypes.REFRESH_HOME_CREATE.name)
                LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
            }
        }

    private fun startCreateComplaint(action: String){
        val intent : Intent = when (action) {
            "evaluation" -> {
                Intent(this@ComplaintsSolutionActivity, EvaluateUnitScanActivity::class.java)
            }
            "frontdesk" -> {
                Intent(this@ComplaintsSolutionActivity, FrontDeskCreateActivity::class.java)
            }
            else -> {
                Intent(this@ComplaintsSolutionActivity, ComplaintSolutionCreateActivity::class.java)
            }
        }

        intent.putExtra("account_id", accountId)
        intent.putExtra("account_name", accountName)

        intent.putExtra("action", action)
        resultLauncher.launch(intent)
    }

}