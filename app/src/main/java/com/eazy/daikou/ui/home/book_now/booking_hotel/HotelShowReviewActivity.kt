package com.eazy.daikou.ui.home.book_now.booking_hotel

import android.content.res.ColorStateList
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.*
import android.widget.RatingBar.OnRatingBarChangeListener
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.base.MockUpData
import com.eazy.daikou.request_data.request.book_now_ws.BookingHotelWS
import com.eazy.daikou.helper.GetDataUtils
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.booking_hotel.HotelMyRoomOfHotelVendorModel
import com.eazy.daikou.model.booking_hotel.HotelMyVendorModel
import com.eazy.daikou.model.booking_hotel.ReviewHotelModel
import com.eazy.daikou.model.home.MainItemHomeModel
import com.eazy.daikou.ui.home.book_now.booking_hotel.adapter.HotelReviewAdapter
import de.hdodenhof.circleimageview.CircleImageView


class HotelShowReviewActivity : BaseActivity() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var itemRateLayout : RelativeLayout
    private lateinit var doRateLayout : RelativeLayout
    private lateinit var nameTv : TextView
    private lateinit var starRate : RatingBar
    private lateinit var writeReview : EditText
    private lateinit var btnSave : TextView
    private var action = ""
    private var hotelId = ""
    private lateinit var progressBar: ProgressBar
    private lateinit var profileImage : CircleImageView
    private var category = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hotel_show_review_layout)

        initView()

        initAction()

    }

    private fun initView(){
        recyclerView = findViewById(R.id.recyclerView)

        doRateLayout = findViewById(R.id.doRateLayout)
        nameTv = findViewById(R.id.nameTv)
        starRate = findViewById(R.id.starRate)
        writeReview = findViewById(R.id.writeReview)
        btnSave = findViewById(R.id.btnSave)
        itemRateLayout = findViewById(R.id.itemRateLayout)
        progressBar = findViewById(R.id.progressItem)
        progressBar.visibility = View.GONE
        profileImage = findViewById(R.id.profile_image)
    }

    private fun initAction(){
        var reviewList : ArrayList<ReviewHotelModel> = ArrayList()
        if (intent != null && intent.hasExtra("review_list")){
            reviewList = intent.getSerializableExtra("review_list") as ArrayList<ReviewHotelModel>
        }
        action = GetDataUtils.getDataFromString("action", this)
        hotelId = GetDataUtils.getDataFromString("hotel_id", this)
        category = GetDataUtils.getDataFromString("category", this)

        val hotelName = GetDataUtils.getDataFromString("hotel_name", this)

        Utils.customOnToolbar(this, hotelName){finish()}

        val user = MockUpData.getUserItem(UserSessionManagement(this))
        when {
            user.name != null -> {
                nameTv.text = user.name
            }
            user!!.lastName != null &&   user.firstName != null -> {
                nameTv.text = user.lastName + " " + user.firstName
            }
            else -> {
                nameTv.text = "- - -"
            }
        }

        Glide.with(profileImage).load(if (user.photo != null) user.photo else R.drawable.ic_my_profile).into(profileImage)

        if (action == "do_rating"){
            itemRateLayout.visibility = View.GONE
            doRateLayout.visibility = View.VISIBLE

            initDoRation()
        } else {
            recyclerView.layoutManager = LinearLayoutManager(this)
            recyclerView.adapter = HotelReviewAdapter(reviewList)

            Utils.validateViewNoItemFound(this, recyclerView, reviewList.size <= 0)
        }
    }

    private fun initDoRation(){
        checkValidateButtonPay()

        writeReview.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun afterTextChanged(editable: Editable) { checkValidateButtonPay() }
        })

        starRate.progressTintList = ColorStateList.valueOf(ContextCompat.getColor(this, R.color.appBarColorOld))
        starRate.onRatingBarChangeListener = OnRatingBarChangeListener { _, _, _ -> checkValidateButtonPay()}

        btnSave.setOnClickListener(
            CustomSetOnClickViewListener{
                progressBar.visibility = View.VISIBLE
                BookingHotelWS().doReviewHotel(this, hashMapData(), object : BookingHotelWS.OnCallBackHotelVendorListener{
                    override fun onSuccessShowWishlistHotel(locationHotelModel: ArrayList<MainItemHomeModel>) {}

                    override fun onSuccessHotelOfVendor(hotelHistoryList: ArrayList<HotelMyVendorModel>) { }

                    override fun onSuccessRoomHotel(hotelHistory: ArrayList<HotelMyRoomOfHotelVendorModel>) {}

                    override fun onSuccessEditStatus(successMsg: String) {
                        progressBar.visibility = View.GONE
                        Utils.customToastMsgError(this@HotelShowReviewActivity, successMsg, true)
                        setResult(RESULT_OK)
                        finish()
                    }

                    override fun onFailed(message: String) {
                        progressBar.visibility = View.GONE
                        Utils.customToastMsgError(this@HotelShowReviewActivity, message, false)
                    }

                })
            }
        )
    }

    private fun isEnableButtonPay() : Boolean{
         if (writeReview.text.toString().isNotEmpty() || starRate.rating > 0){
            return true
        }
        return false
    }

    private fun checkValidateButtonPay(){
        if (isEnableButtonPay()){
            btnSave.isEnabled = true
            Utils.setBgTint(btnSave, R.color.color_book_now)
        } else {
            btnSave.isEnabled = false
            Utils.setBgTint(btnSave, R.color.gray)
        }
    }

    private fun hashMapData() : HashMap<String, Any>{
        val hashMap : HashMap<String, Any> = HashMap()
        hashMap["user_id"] = MockUpData.getEazyHotelUserId(UserSessionManagement(this))
        hashMap["object_id"] = hotelId
        hashMap["category"] = category
        hashMap["star_rate"] = starRate.rating.toString()
        hashMap["content"] = writeReview.text.toString()
        return hashMap
    }

}