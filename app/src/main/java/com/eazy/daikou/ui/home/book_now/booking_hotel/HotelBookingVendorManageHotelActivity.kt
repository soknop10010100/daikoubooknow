package com.eazy.daikou.ui.home.book_now.booking_hotel

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AbsListView
import android.widget.ProgressBar
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.base.MockUpData
import com.eazy.daikou.request_data.request.book_now_ws.BookingHotelWS
import com.eazy.daikou.request_data.request.book_now_ws.RequestHashMapData
import com.eazy.daikou.helper.GetDataUtils
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.booking_hotel.HotelMyRoomOfHotelVendorModel
import com.eazy.daikou.model.booking_hotel.HotelMyVendorModel
import com.eazy.daikou.model.home.MainItemHomeModel
import com.eazy.daikou.model.home.SubItemHomeModel
import com.eazy.daikou.ui.home.book_now.booking_hotel.adapter.BookingHomeItemAdapter
import com.eazy.daikou.ui.home.book_now.booking_hotel.adapter.HotelCategoryItemAdapter
import com.eazy.daikou.ui.home.book_now.booking_hotel.adapter.MyHotelOfVendorAdapter

class HotelBookingVendorManageHotelActivity : BaseActivity() {

    private lateinit var progressBar: ProgressBar
    private lateinit var recyclerView: RecyclerView
    private lateinit var refreshLayout: SwipeRefreshLayout
    private var currentItem = 0
    private var total : Int = 0
    private var scrollDown : Int = 0
    private var currentPage : Int = 1
    private var size : Int = 10
    private var isScrolling = false
    private lateinit var myHotelOfVendorAdapter : MyHotelOfVendorAdapter
    private var listHotelMyVendorModel : ArrayList<HotelMyVendorModel> = ArrayList()
    private var action : String = ""
    private lateinit var linearLayoutManager : LinearLayoutManager
    private var userEazyId = ""

    private lateinit var bookingHomeItemAdapter: BookingHomeItemAdapter
    private var subItemHomeModelList : ArrayList<SubItemHomeModel> = ArrayList()
    private var category = "hotel"

    // Wishlist
    private lateinit var recyclerViewCategory: RecyclerView
    private var listCategory = ArrayList<SubItemHomeModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hotel_my_control_of_vandor)

        initView()

        initData()

        initAction()

    }

    private fun initView(){
        progressBar = findViewById(R.id.progressItem)
        progressBar.visibility = View.GONE
        recyclerView = findViewById(R.id.recyclerView)
        refreshLayout = findViewById(R.id.swipe_layouts)

        // Wishlist
        recyclerViewCategory = findViewById(R.id.recyclerViewCategory)
    }

    private fun initData(){
        action = GetDataUtils.getDataFromString("action", this)
        userEazyId = MockUpData.getEazyHotelUserId(UserSessionManagement(this))
    }

    private fun initAction(){
        refreshLayout.setColorSchemeResources(R.color.colorPrimary)
        refreshLayout.setOnRefreshListener { refreshList() }

        linearLayoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = linearLayoutManager
        if (action == "hotel_wishlist_hotel"){
            initRecyclerviewCategory()

            initRecyclerViewWishlist()
            Utils.customOnToolbar(this, resources.getString(R.string.my_wishlist)){finish()}
            recyclerViewCategory.visibility = View.VISIBLE
        } else {
            initRecyclerView()
            Utils.customOnToolbar(this, resources.getString(R.string.my_hotel)){finish()}
        }

        onScrollItemRecycle()
    }

    private fun initRecyclerviewCategory() {
        recyclerViewCategory.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        listCategory = RequestHashMapData.addCategoryItemList(resources)
        var bookingHomeItemAdapter: HotelCategoryItemAdapter ?= null
        bookingHomeItemAdapter =  HotelCategoryItemAdapter(false, listCategory,
            object :BookingHomeItemAdapter.PropertyClick{
                @SuppressLint("NotifyDataSetChanged")
                override fun onBookingClick(propertyModel: SubItemHomeModel?) {
                    for (itemSub in listCategory){
                        if (propertyModel != null) {
                            itemSub.isClickCategory = itemSub.id == propertyModel.id
                            category = propertyModel.id.toString()
                        }
                    }
                    bookingHomeItemAdapter!!.notifyDataSetChanged()

                    refreshList()
                }

                override fun onBookingWishlistClick(propertyModel: SubItemHomeModel?) {}

            }
        )
        recyclerViewCategory.adapter = bookingHomeItemAdapter
    }


    private fun initRecyclerView(){
        myHotelOfVendorAdapter = MyHotelOfVendorAdapter(this, action, listHotelMyVendorModel, object : MyHotelOfVendorAdapter.PropertyClick{
            override fun onBookingClick(hotelMyVendorModel: HotelMyVendorModel) {
                val intent = Intent(this@HotelBookingVendorManageHotelActivity, HotelMyRoomOfHotelVendorActivity::class.java)
                intent.putExtra("hotel_id", hotelMyVendorModel.hotel_id)
                intent.putExtra("hotel_name", hotelMyVendorModel.hotel_name)
                startActivity(intent)
            }

            override fun onFavouriteClick(action: String, hotelMyVendorModel: HotelMyVendorModel) {
                editStatusHotelWs(hotelMyVendorModel.hotel_id!!, hotelMyVendorModel.is_wishlist, action)
            }
        })
        recyclerView.adapter = myHotelOfVendorAdapter

        requestDataWs()
    }

    private fun initRecyclerViewWishlist(){
        bookingHomeItemAdapter = BookingHomeItemAdapter(this, "bestseller_listing", subItemHomeModelList, onClickSubItemBookingListener)
        recyclerView.adapter = bookingHomeItemAdapter

        requestDataWs()
    }

    private fun editStatusHotelWs(hotelId : String, isWishList : Boolean, status : String){
        progressBar.visibility = View.VISIBLE
        if (status == "wishlist"){
            BookingHotelWS().doEditStatus(this, "wishlist", RequestHashMapData.editStatusHotelVendor(hotelId, status, userEazyId, if (isWishList) "remove" else "add", category, false), onCallBackServiceListener)
        } else {
            BookingHotelWS().doEditStatus(this, "edit_hotel", RequestHashMapData.editStatusHotelVendor(hotelId, status, "", "", "", false), onCallBackServiceListener)
        }
    }

    private fun refreshList(){
        currentPage = 1
        size = 10
        isScrolling = true
        if (action == "hotel_wishlist_hotel") {
            bookingHomeItemAdapter.clear()
        } else {
            myHotelOfVendorAdapter.clear()
        }
        requestDataWs()
    }

    private fun requestDataWs(){
        progressBar.visibility = View.VISIBLE
        BookingHotelWS().getHotelVendorWs(this, action, category, userEazyId, currentPage, 10, onCallBackServiceListener)
    }

    private fun onScrollItemRecycle() {
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true
                }
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                currentItem = linearLayoutManager.childCount
                total = linearLayoutManager.itemCount
                scrollDown = linearLayoutManager.findFirstVisibleItemPosition()
                if (dy > 0) {
                    if (isScrolling && currentItem + scrollDown == total) {
                        if (total == size) {
                            isScrolling = false
                            currentPage++
                            progressBar.visibility = View.VISIBLE
                            if (action == "wishlist_hotel") {
                                recyclerView.scrollToPosition(subItemHomeModelList.size - 1)
                                requestDataWs()
                            } else {
                                recyclerView.scrollToPosition(listHotelMyVendorModel.size - 1)
                                requestDataWs()
                            }
                            size += 10
                        } else {
                            progressBar.visibility = View.GONE
                        }
                    }
                }
            }
        })
    }

    private val onCallBackServiceListener = object : BookingHotelWS.OnCallBackHotelVendorListener{
        @SuppressLint("NotifyDataSetChanged")
        override fun onSuccessShowWishlistHotel(locationHotelModel: ArrayList<MainItemHomeModel>) {
            progressBar.visibility = View.GONE
            refreshLayout.isRefreshing = false
            for (item in locationHotelModel) {
                subItemHomeModelList.addAll(item.subItemHomeModelList)
            }

            Utils.validateViewNoItemFound(this@HotelBookingVendorManageHotelActivity, recyclerView, subItemHomeModelList.size <= 0)
            bookingHomeItemAdapter.notifyDataSetChanged()
        }

        @SuppressLint("NotifyDataSetChanged")
        override fun onSuccessHotelOfVendor(hotelHistoryList: ArrayList<HotelMyVendorModel>) {
            progressBar.visibility = View.GONE
            refreshLayout.isRefreshing = false
            listHotelMyVendorModel.addAll(hotelHistoryList)

            Utils.validateViewNoItemFound(this@HotelBookingVendorManageHotelActivity, recyclerView, listHotelMyVendorModel.size <= 0)
            myHotelOfVendorAdapter.notifyDataSetChanged()
        }

        override fun onSuccessRoomHotel(hotelHistory: ArrayList<HotelMyRoomOfHotelVendorModel>) {}

        override fun onSuccessEditStatus(successMsg: String) {
            progressBar.visibility = View.GONE
            refreshLayout.isRefreshing = false
            Utils.customToastMsgError(this@HotelBookingVendorManageHotelActivity, successMsg, true)

            refreshList()
        }

        override fun onFailed(message: String) {
            progressBar.visibility = View.GONE
            refreshLayout.isRefreshing = false
            Utils.customToastMsgError(this@HotelBookingVendorManageHotelActivity, message, false)
        }

    }


    private var onClickSubItemBookingListener = object : BookingHomeItemAdapter.PropertyClick {
        override fun onBookingClick(propertyModel: SubItemHomeModel?) {
            if (propertyModel != null) {
                val intent = Intent(this@HotelBookingVendorManageHotelActivity, HotelBookingDetailHotelActivity::class.java)
                intent.putExtra("hotel_name", propertyModel.name)
                intent.putExtra("category", propertyModel.titleCategory)
                intent.putExtra("hotel_image", propertyModel.imageUrl)
                intent.putExtra("hotel_id", propertyModel.id)
                intent.putExtra("price", propertyModel.priceVal)
                intent.putExtra("start_date", "")
                intent.putExtra("end_date", "")
                intent.putExtra("adults", "1")
                intent.putExtra("children", "0")
                resultLauncher.launch(intent)
            }
        }

        override fun onBookingWishlistClick(propertyModel: SubItemHomeModel?) {
            //Add wishlist favourite
        }
    }

    private val resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
        if (result.resultCode == Activity.RESULT_OK) {
            //Call back when click wishlist hotel on detail screen
            refreshList()
        }
    }

}