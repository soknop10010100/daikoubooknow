package com.eazy.daikou.ui.home.service_provider_new.activity

import android.content.Intent
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.base.BaseActivity

class ServiceProviderDetailActivity : BaseActivity() {

    private lateinit var btnAddToCart : TextView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_service_provider_detail)

        initView()

        initData()

        initAction()

    }

    private fun initView(){
        btnAddToCart = findViewById(R.id.btnAddToCart)
    }
    private fun initData(){

    }
    private fun initAction(){
        findViewById<ImageView>(R.id.btnBack).setOnClickListener { finish() }
        btnAddToCart.setOnClickListener(CustomSetOnClickViewListener{
            val intent = Intent(this@ServiceProviderDetailActivity,MyCartProviderActivity::class.java)
            intent.putExtra("action_service", "product")
            startActivity(intent)
        })
    }
}