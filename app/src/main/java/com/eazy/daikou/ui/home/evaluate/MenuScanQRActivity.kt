package com.eazy.daikou.ui.home.evaluate

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import android.widget.ProgressBar
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.base.MockUpData
import com.eazy.daikou.request_data.request.scan_my_unit.ScanQrMyUnitWs
import com.eazy.daikou.request_data.static_data.StaticUtilsKey
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.home.HomeViewModel
import com.eazy.daikou.model.my_unit_info.MyUnitScanInfoModel
import com.eazy.daikou.model.my_unit_info.ServiceProviderInfo
import com.eazy.daikou.ui.home.hrm.adapter.MyRoleMainItemAdapter
import com.eazy.daikou.ui.home.inspection_work_order.ChooseInspectionTypeActivity
import com.eazy.daikou.ui.home.parking.ParkingListActivity
import com.eazy.daikou.ui.home.my_property_v1.property_service_employee.work_service_employee.ServicePropertyHistoryEmployeeActivity

class MenuScanQRActivity : BaseActivity() {

    private lateinit var itemRecyclerView : RecyclerView
    private var resultValQrCode = ""
    private lateinit var progressBar: ProgressBar
    private lateinit var myUnitInfoModel: MyUnitScanInfoModel
    private var action : String = ""
    private var activeUserType = ""
    private var accountId = ""
    private var i = 0
    private var actionService = "" // check in or out for service

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu_scan_qractivity)

        initView()

        initAction()
    }

    private fun initView(){
        Utils.customOnToolbar(this, resources.getString(R.string.my_unit_).uppercase()) { finish() }
        findViewById<LinearLayout>(R.id.layoutMap).visibility = View.INVISIBLE

        itemRecyclerView = findViewById(R.id.list_service_document)
        progressBar = findViewById(R.id.progressItem)
        progressBar.visibility = View.GONE
    }

    private fun initAction(){
        if (intent != null && intent.hasExtra("result_scan_parking")){
            resultValQrCode = intent.getStringExtra("result_scan_parking").toString()
        }
        if (intent != null && intent.hasExtra("action")){
            action = intent.getStringExtra("action").toString()
        }
        val user = MockUpData.getUserItem(UserSessionManagement(this))
        activeUserType = Utils.validateNullValue(user.activeUserType)
        accountId = Utils.validateNullValue(user.accountId)

        requestService()
    }


    private fun initItemRecyclerView(serviceProviderInfo: ServiceProviderInfo, option : ArrayList<String>, unitQrCode : String){
        itemRecyclerView.layoutManager = LinearLayoutManager(this)
        itemRecyclerView.adapter = MyRoleMainItemAdapter(getListHomeMenu(option), object : MyRoleMainItemAdapter.HomeCallBack{
            override fun clickIconListener(item: HomeViewModel) {
                startNewActivity(item.action, serviceProviderInfo, unitQrCode)
            }
        })
    }

    private fun startNewActivity(action : String, serviceProviderInfo: ServiceProviderInfo, unitQrCode : String){
        when(action){
            "unpaid_payment"->{
                // startNewActivity(PaymentDetailActivity::class.java, resultValQrCode, "result_scan_parking")
            }
            "inspection_unit" ->{
                if (!Utils.isEmployeeRole(this)){
                    Utils.customToastMsgError(this,  resources.getString(R.string.you_do_not_permission_to_access), true)
                    return
                }
                val intent = Intent(this@MenuScanQRActivity, ChooseInspectionTypeActivity::class.java)
                intent.putExtra("action", StaticUtilsKey.template_type)
                intent.putExtra("inspection_type_id", myUnitInfoModel.unit?.unit_insp_type_id)
                intent.putExtra("my_unit_info", myUnitInfoModel.unit)
                intent.putExtra("inspection_type_name", "Unit Inspection")
                StaticUtilsKey.isActionMenuHome = StaticUtilsKey.inspection_action
                startActivity(intent)
            }
            "check_in_and_out" ->{
                if (unitQrCode == ""){
                    Utils.customToastMsgError(this, resources.getString(R.string.invalid_qr_code), false)
                    return
                }

                if (serviceProviderInfo.success){
                    val intent = Intent(this@MenuScanQRActivity, ServicePropertyHistoryEmployeeActivity::class.java)
                    intent.putExtra("service_provider_info", serviceProviderInfo)
                    intent.putExtra("action", "from_smart_scan")
                    intent.putExtra("action_service_in_out", actionService)
                    intent.putExtra("unit_qr_code", unitQrCode)
                    resultLauncher.launch(intent)
                } else {
                    var errorMess = ""
                    if (serviceProviderInfo.msg != null) errorMess =serviceProviderInfo.msg.toString()
                    Utils.customToastMsgError(this@MenuScanQRActivity, errorMess, false)
                }
            }
            "evaluate_unit" ->{
                myUnitInfoModel.unit?.unit_id?.let { startNewActivity(EvaluateUnitScanActivity::class.java, it, "unit_id") }
            }
        }
    }

    private fun startNewActivity(toActivityClass: Class<*>, value: String, key : String) {
        val intent = Intent(this, toActivityClass)
        intent.putExtra(key, value)
        startActivity(intent)
    }

    private fun getListHomeMenu(optionList: ArrayList<String>): List<HomeViewModel> {
        val menuItemList : MutableList<HomeViewModel> = ArrayList()
        for (item in optionList){
            if (itemViewModel(item) != null) menuItemList.add(itemViewModel(item)!!)
        }
        return menuItemList
    }

    private fun itemViewModel(action: String) : HomeViewModel?{
        when (action) {
            "unit_info" -> {
                return HomeViewModel(resources.getString(R.string.view_detail_for_unit_inspection),"my_unit",
                    ResourcesCompat.getDrawable(resources, R.drawable.ic_home_notebook_v2, null),
                    resources.getString(R.string.unit_detail), color())
            }
            "check_in_and_out" -> {
                return HomeViewModel("Check in ore check out to do your work","check_in_and_out",
                    ResourcesCompat.getDrawable(resources, R.drawable.ic_home_notebook_v2, null),
                    resources.getString(R.string.check_in_out), color())
            }
            "inspection" -> {
                return HomeViewModel(resources.getString(R.string.see_list_add_new_inspection),"inspection_unit",
                    ResourcesCompat.getDrawable(resources, R.drawable.ic_home_notebook_v2, null),
                    resources.getString(R.string.inspection), color())
            }
            "evaluation" -> {
                return HomeViewModel(resources.getString(R.string.describe_your_feeling),"evaluate_unit",
                    ResourcesCompat.getDrawable(resources, R.drawable.home_evaluate_v2, null),
                    resources.getString(R.string.evaluate), color())
            }
        }
        return null
    }

    private fun requestService(){
        progressBar.visibility = View.VISIBLE
        ScanQrMyUnitWs().getItemMyUnitFromQrCode(this@MenuScanQRActivity, UserSessionManagement(this).userId, resultValQrCode, activeUserType, accountId,
        object : ScanQrMyUnitWs.OnCallBackListener{
            override fun onSuccess(myUnitInfo: MyUnitScanInfoModel) {
                progressBar.visibility = View.GONE
                if (myUnitInfo != null) {
                    myUnitInfoModel = myUnitInfo
                    when (myUnitInfo.part) {
                        "unit" -> {
                            if (myUnitInfo.action != null){
                                if (myUnitInfo.action != "wrong_unit_qr"){
                                    if (myUnitInfo.service_provider_info != null){
                                        var unitQrCode = ""
                                        if (myUnitInfo.unit != null){
                                            if (myUnitInfo.unit!!.unit_qr != null)  unitQrCode = myUnitInfo.unit!!.unit_qr.toString()
                                        }
                                        actionService = if (myUnitInfo.service_provider_info!!.action != null) myUnitInfo.service_provider_info!!.action.toString() else ""

                                        initItemRecyclerView(myUnitInfo.service_provider_info!!, myUnitInfo.options, unitQrCode)
                                    } else {
                                        Utils.customToastMsgError(this@MenuScanQRActivity, resources.getString(R.string.invalid_qr_code), false)
                                        finish()
                                    }
                                } else {
                                    Utils.customToastMsgError(this@MenuScanQRActivity, resources.getString(R.string.invalid_qr_code), false)
                                    finish()
                                }
                            }
                        }
                        "parking" -> {
                            if (myUnitInfo.action != null){
                                when {
                                    myUnitInfo.action != "qr_code_invalid" -> {
                                        val intent = Intent(this@MenuScanQRActivity, ParkingListActivity::class.java)
                                        intent.putExtra("result_scan_parking", resultValQrCode)
                                        intent.putExtra("my_unit_info", myUnitInfo)
                                        intent.putExtra("action", action)
                                        startActivity(intent)
                                        finish()
                                    }
                                    myUnitInfo.action == "not_available" -> {
                                        Utils.customToastMsgError(this@MenuScanQRActivity, resources.getString(R.string.space_not_availabel_parking), false)
                                        finish()
                                    }
                                    else -> {
                                        Utils.customToastMsgError(this@MenuScanQRActivity, resources.getString(R.string.invalid_qr_code), false)
                                        finish()
                                    }
                                }
                            }
                        }
                        else -> {
                            Utils.customToastMsgError(this@MenuScanQRActivity, resources.getString(R.string.not_available), false)
                            finish()
                        }
                    }
                } else {
                    Utils.customToastMsgError(this@MenuScanQRActivity, resources.getString(R.string.not_available), false)
                    finish()
                }
            }

            override fun onLoadFail(message: String) {
                progressBar.visibility = View.GONE
                Utils.customToastMsgError(this@MenuScanQRActivity, message, false)
                finish()
            }
        })
    }

    private fun color() : Int{
        return Utils.setColorBackground(i) { index -> i = index }
    }

    private var resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()){ result ->
        if (result.resultCode == Activity.RESULT_OK) {
            val data: Intent? = result.data
            if (data != null) {
                var action = ""
                if (data.hasExtra("action")) {
                    action = data.getStringExtra("action") as String
                }

                if (action == "check_in_out_success"){ // call back when check in / out success
                    if (actionService == "check_in")    actionService = "check_out"
                }
            }
        }
    }

}