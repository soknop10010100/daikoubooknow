package com.eazy.daikou.ui.home.development_project.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.home_page.home_project.Featured
import com.squareup.picasso.Picasso

class PropertySeeMoreFeatureAdapter(
    private val listPropertyItem: ArrayList<Featured>,
    private val clickListener: OnClickProperty
) : RecyclerView.Adapter<PropertySeeMoreFeatureAdapter.ViewHolder>() {

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val imageBuilding : ImageView = itemView.findViewById(R.id.img_property)
        val textNameBuilding : TextView = itemView.findViewById(R.id.text_name_building)
        val textAddress : TextView = itemView.findViewById(R.id.text_address)
        val btnProperty : CardView = itemView.findViewById(R.id.btn_property)
        val textAddressCity : TextView = itemView.findViewById(R.id.text_address_city)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.custom_item_property_recommend, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = listPropertyItem[position]
        Utils.setValueOnText(holder.textNameBuilding, item.name)

        if (item.image != null) {
            Picasso.get().load(item.image).placeholder(R.drawable.no_image).into(holder.imageBuilding)
        } else {
            holder.imageBuilding.setImageResource(R.drawable.no_image)
        }

        if (item.address != null) {
            holder.textAddress.text = item.address
        } else {
            holder.textAddress.text = ". . ."
        }

        if (item.city != null) {
            holder.textAddressCity.text = item.city
        } else {
            holder.textAddressCity.text = ". . ."
        }

        holder.btnProperty.setOnClickListener {
            clickListener.click(item)
        }

    }

    override fun getItemCount(): Int {
        return listPropertyItem.size
    }

    interface OnClickProperty {
        fun click(property: Featured)
    }

    fun clear() {
        val size: Int = listPropertyItem.size
        if (size > 0) {
            for (i in 0 until size) {
                listPropertyItem.removeAt(0)
            }
            notifyItemRangeRemoved(0, size)
        }
    }
}
