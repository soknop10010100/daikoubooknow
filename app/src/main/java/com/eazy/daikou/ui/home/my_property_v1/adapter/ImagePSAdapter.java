package com.eazy.daikou.ui.home.my_property_v1.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.eazy.daikou.R;

import java.util.List;

public class ImagePSAdapter extends RecyclerView.Adapter<ImagePSAdapter.ItemViewHolder>{

    private final List<String> filePathList;

    public ImagePSAdapter(List<String> filePathList) {
        this.filePathList = filePathList;
    }
    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rowView = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_model, parent, false);
        return new ItemViewHolder(rowView);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        String filePast = filePathList.get(position);
        if (filePast != null){
            Glide.with(holder.imageView.getContext()).load(filePast).into(holder.imageView);
        }

    }

    @Override
    public int getItemCount() {
        return filePathList.size();
    }

    public  static class ItemViewHolder extends RecyclerView.ViewHolder{
        private final ImageView imageView;
        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageList);
        }
    }
}
