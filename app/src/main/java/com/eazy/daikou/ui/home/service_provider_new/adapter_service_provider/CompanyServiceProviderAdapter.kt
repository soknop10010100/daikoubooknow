package com.eazy.daikou.ui.home.service_provider_new.adapter_service_provider

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.model.service_provider.SubCategory

class CompanyServiceProviderAdapter(private val context: Context, private val companyList: ArrayList<SubCategory>, private val callBackListener: ClickItemCallBackListener): RecyclerView.Adapter<CompanyServiceProviderAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.custom_layout_item_company_provider, parent, false)
        return  ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
       val companyData: SubCategory = companyList[position]
        if (companyData != null){
            if (companyData.urlImage != null){
                // Glide.with(context).load(companyData.urlImage).into(holder.imageCompanyTv)
                Glide.with(context).load("https://sandboxeazy.daikou.asia/img/upload/00DBA48D-3563-BC15-5E65-57969A4A5101.jpg").into(holder.imageCompanyTv)
            } else {
                Glide.with(context).load(R.drawable.no_image).into(holder.imageCompanyTv)
            }

            holder.itemView.setOnClickListener {  callBackListener.clickItemListener(companyData)}
        }
    }

    override fun getItemCount(): Int {
       return companyList.size
    }
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view){
        var imageCompanyTv : ImageView = view.findViewById(R.id.imageCompany)
    }

    interface ClickItemCallBackListener{
        fun clickItemListener(companyData: SubCategory)
    }

}