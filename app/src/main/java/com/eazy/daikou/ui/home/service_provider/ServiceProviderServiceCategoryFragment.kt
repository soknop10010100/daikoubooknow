package com.eazy.daikou.ui.home.service_provider

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.eazy.daikou.R
import com.eazy.daikou.ui.home.hrm.leave_request.LeaveBalanceFragment
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class ServiceProviderServiceCategoryFragment : BottomSheetDialogFragment() {

    fun newInstance(): ServiceProviderServiceCategoryFragment {
        val fragment = ServiceProviderServiceCategoryFragment()
        val args = Bundle()
//        args.putString("user_bussiness_key",userBussinessKey)
        fragment.arguments = args

        return fragment

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_service_provider_service_category, container, false)

        initView()
        initAction()
        return view
    }
    private fun initView(){

    }

    private fun initAction(){

    }
}