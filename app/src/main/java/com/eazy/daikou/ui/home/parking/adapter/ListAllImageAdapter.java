package com.eazy.daikou.ui.home.parking.adapter;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.eazy.daikou.R;
import com.eazy.daikou.model.parking.AllImage;

import java.util.List;

public class ListAllImageAdapter extends RecyclerView.Adapter<ListAllImageAdapter.ItemViewHolder> {

    private final List<AllImage> listImage;
    public ListAllImageAdapter(List<AllImage> listImage) {
        this.listImage = listImage;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_image_datpter, parent, false);
        return new ItemViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        AllImage image = listImage.get(position);
        if(image.getFilePath() != null){
            Glide.with(holder.imageView1).load(image.getFilePath()).into(holder.imageView1);
        }

    }
    @Override
    public int getItemCount() {
        return listImage.size();
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder{

        private final ImageView imageView1;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView1 = itemView.findViewById(R.id.photo_1);

        }
    }

    public void clear() {
        int size = listImage.size ();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                listImage.remove ( 0 );
            }
            notifyItemRangeRemoved ( 0, size );
        }
    }
}
