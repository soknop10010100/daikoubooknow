package com.eazy.daikou.ui.home.hrm.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.hr.ItemHR


class ItemHRAdapter(private val context: Context, private val action : String, private val listName: List<ItemHR>, private val itemClick: ItemClickOnListener) : RecyclerView.Adapter<ItemHRAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.layout_item_hr_adapter, parent, false)
        return ViewHolder(v)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data: ItemHR = listName[position]
        holder.nameItemTv.text = data.nameItem
        holder.descriptionItemHrmTv.text = data.descriptionItem
        Glide.with(holder.imageView).load(data.image).into(holder.imageView)

        if (action == "sub_home_menu"){
            Utils.setBgTint(holder.cardIcon, data.color)
            if (data.action == "hotel_booking"){
                holder.imageView.setColorFilter(Utils.getColor(context, R.color.transparent))
            }
        } else {
            holder.cardIcon.backgroundTintList = setColorBackground(context, data.id)
        }

        holder.cardAdapter.setOnClickListener(CustomSetOnClickViewListener {
            itemClick.onItemClickId(data.id)
            itemClick.onItemClickAction(data.action)
        })
    }
    interface ItemClickOnListener{
        fun onItemClickId(id : Int)
        fun onItemClickAction(action: String?)
    }

    override fun getItemCount(): Int {
        return listName.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var nameItemTv : TextView = view.findViewById(R.id.name_item_hr)
        var descriptionItemHrmTv : TextView = view.findViewById(R.id.descriptionItemHrmTv)
        var imageView : ImageView = view.findViewById(R.id.image_hr)
        var cardAdapter : CardView = view.findViewById(R.id.card_hr)
        var cardIcon : CardView = view.findViewById(R.id.cardIcon)
    }

    private fun setColorBackground(context: Context?, id: Int): ColorStateList {
        return when (id) {
            1 -> ColorStateList.valueOf(ContextCompat.getColor(context!!, R.color.greenSea))
            2 -> ColorStateList.valueOf(ContextCompat.getColor(context!!, R.color.yellow))
            3 -> ColorStateList.valueOf(ContextCompat.getColor(context!!, R.color.red))
            4 -> ColorStateList.valueOf(ContextCompat.getColor(context!!, R.color.blue))
            5 -> ColorStateList.valueOf(ContextCompat.getColor(context!!, R.color.color_wooden))
            6 -> ColorStateList.valueOf(ContextCompat.getColor(context!!, R.color.appBarColorOld))
            7 -> ColorStateList.valueOf(ContextCompat.getColor(context!!, R.color.light_blue_600))
            else -> ColorStateList.valueOf(ContextCompat.getColor(context!!, R.color.appBarColorOld))
        }
    }

}