package com.eazy.daikou.ui.home.hrm.payroll

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.base.MockUpData
import com.eazy.daikou.request_data.request.hr_ws.PayrollManagementWs
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.hr.PayrollManagement
import com.eazy.daikou.model.hr.PayrollManagementDetailModel
import com.eazy.daikou.ui.home.complaint_solution.adpater.HistoryRescheduleAdapter
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class PayRollDetailActivity : BaseActivity() {

    private lateinit var btnBack: TextView
    private lateinit var idNoTv: TextView
    private lateinit var designationTv: TextView
    private lateinit var employeeTypeTv: TextView
    private lateinit var basicSalaryTv: TextView
    private lateinit var salaryMonthTv: TextView
    private lateinit var currencyTv: TextView
    private lateinit var btnReceiveSalary: TextView
    private lateinit var paymentMethodTv: TextView
    private lateinit var grossSalaryTv: TextView
    private lateinit var totalDeductionTv: TextView
    private lateinit var netSalaryTv: TextView
    private lateinit var paidDateSalaryTv: TextView
    private lateinit var payRoleDetailLayout: LinearLayout
    private lateinit var progressBar: ProgressBar
    private lateinit var subTotalSalaryLayout: LinearLayout
    private lateinit var subTotalSalaryTv: TextView
    private lateinit var subItemSalaryRecycle: RecyclerView

    private lateinit var data: PayrollManagementDetailModel

    private var payrollId: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pay_roll_detail)

        initView()

        initData()

        initAction()
    }

    private fun initView() {
        btnBack = findViewById(R.id.btn_back)

        //employee_info
        idNoTv = findViewById(R.id.idNoTv)
        salaryMonthTv = findViewById(R.id.salary_monthTv)
        designationTv = findViewById(R.id.designation)
        employeeTypeTv = findViewById(R.id.employee_type)
        basicSalaryTv = findViewById(R.id.basic_salary)
        currencyTv = findViewById(R.id.currencyTv)
        paymentMethodTv = findViewById(R.id.payment_methodTv)
        paidDateSalaryTv = findViewById(R.id.paidDateSalaryTv)

        //total salary detail
        grossSalaryTv = findViewById(R.id.gross_salary)
        totalDeductionTv = findViewById(R.id.total_deduction)
        netSalaryTv = findViewById(R.id.net_salary)
        btnReceiveSalary = findViewById(R.id.btnReceiveSalary)

        payRoleDetailLayout = findViewById(R.id.payRoleDetailLayout)
        payRoleDetailLayout.visibility = View.GONE
        progressBar = findViewById(R.id.progressItem)
        subTotalSalaryLayout = findViewById(R.id.subSalaryDetails)
        subTotalSalaryTv = findViewById(R.id.subTotalSalary)
        subItemSalaryRecycle = findViewById(R.id.subItemSalaryDetail)

    }

    private fun initData() {
        if (intent != null && intent.hasExtra("payroll_id")) {
            payrollId = intent.getStringExtra("payroll_id").toString()
        }
    }

    private fun initAction() {

        btnBack.setOnClickListener { finish() }

        getServiceAPIDetail()

        subTotalSalaryTv.setOnClickListener {
            subTotalSalaryLayout.visibility =
                if (subTotalSalaryLayout.visibility == View.VISIBLE) View.GONE else View.VISIBLE
        }
    }


    private fun setMessageAlertReceiveSalary() {
        if (data.employee_info.confirm_payment_at != null) {
            btnReceiveSalary.visibility = View.GONE
        } else {
            btnReceiveSalary.visibility = View.VISIBLE
        }

        btnReceiveSalary.setOnClickListener {
            val builder = AlertDialog.Builder(this)
            builder.setTitle(getString(R.string.confirm_salary))
            builder.setMessage(getString(R.string.are_u_sure_receive_your_salary))

            builder.setPositiveButton(resources.getString(R.string.yes)) { _, _ ->
                getServiceApiConfirmDate()
                finish()
            }
            builder.setNegativeButton(resources.getString(R.string.no)) { dialog: DialogInterface, _: Int -> dialog.dismiss() }

            val alertDialog: AlertDialog = builder.create()
            alertDialog.setCancelable(false)
            alertDialog.show()
        }
    }

    private fun getServiceAPIDetail() {
        PayrollManagementWs().getListPayRollDetail(
            this,
            payrollId,
            MockUpData.userBusinessKey(UserSessionManagement(this)),
            object : PayrollManagementWs.OnCallBackListener {
                override fun onLoadListSuccessFull(listPayRoll: PayrollManagement) {}

                override fun onSuccessDataPayrollDetail(listPayRoll: PayrollManagementDetailModel) {
                    progressBar.visibility = View.GONE
                    payRoleDetailLayout.visibility = View.VISIBLE
                    data = listPayRoll

                    setDataValue(listPayRoll)

                }

                override fun onLoadFail(message: String) {
                    progressBar.visibility = View.GONE
                    Utils.customToastMsgError(this@PayRollDetailActivity, message, false)
                }
            })
    }

    private fun getServiceApiConfirmDate() {
        val hashMap: HashMap<String, Any> = HashMap()
        hashMap["salary_payment_id"] = data.employee_info.salary_payment_id
        PayrollManagementWs().confirmSalaryPayrollWS(this, hashMap, onConfirmSalaryCallBackListener)
    }

    private val onConfirmSalaryCallBackListener: PayrollManagementWs.OnConfirmSalaryCallBackListener =
        object : PayrollManagementWs.OnConfirmSalaryCallBackListener {
            override fun onLoadListSuccess(msg: String) {
                Utils.customToastMsgError(this@PayRollDetailActivity, msg, true)
                finish()
            }

            override fun onLoadFail(message: String) {
                Utils.customToastMsgError(this@PayRollDetailActivity, message, false)
            }

        }


    private fun setDataValue(data: PayrollManagementDetailModel) {
        //Format date salary month
        val inputFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        val outputFormat: DateFormat = SimpleDateFormat("MMMM-yyyy", Locale.getDefault())
        val date: Date = inputFormat.parse(data.employee_info.salary_month) as Date
        val salaryMonth: String = outputFormat.format(date)

        //employee_info
        setOnTextView(idNoTv, data.employee_info.order_no, "no")
        setOnTextView(salaryMonthTv, salaryMonth, "no")
        setOnTextView(designationTv, data.employee_info.designation, "no")
        setOnTextView(employeeTypeTv, data.employee_info.employee_type, "no")
        setOnTextView(basicSalaryTv, data.employee_info.basic_salary, "yes")
        setOnTextView(paymentMethodTv, data.employee_info.payment_type, "no")
        setOnTextView(paidDateSalaryTv, data.employee_info.paid_date, "no")
        data.currency_code?.let { setOnTextView(currencyTv, it, "no") }

        //total salary detail
        addTextView(grossSalaryTv, data.salary_detail.gross_salary, "yes")
        addTextView(totalDeductionTv, "-" + " " + data.salary_detail.total_deduction, "yes")
        addTextView(netSalaryTv, data.salary_detail.net_salary, "yes")


        subItemSalaryRecycle.isNestedScrollingEnabled = false
        subItemSalaryRecycle.layoutManager = LinearLayoutManager(this)
        val historyRescheduleAdapter =
            HistoryRescheduleAdapter(data.salary_items)
        subItemSalaryRecycle.adapter = historyRescheduleAdapter

        setMessageAlertReceiveSalary()
    }

    @SuppressLint("SetTextI18n")
    private fun addTextView(textView: TextView, value: String, text: String) {
        if (value.isNotEmpty()) {
            if (text == "yes") {
                textView.text = " $value" + " " + data.currency_code
            } else {
                textView.text = value
            }

        } else {
            textView.text = "$ 0"
        }
    }

    private fun setOnTextView(textView: TextView, value: String, text: String) {
        if (value != null) {
            if (text == "yes") {
                textView.text = String.format("%s %s", value, data.currency_code)
            } else {
                textView.text = value
            }
        } else {
            textView.text = "----"
        }
    }
}