package com.eazy.daikou.ui.home.parking.adapter;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.eazy.daikou.R;
import com.eazy.daikou.helper.AbsoluteFitLayoutManager;
import com.eazy.daikou.helper.DateUtil;
import com.eazy.daikou.helper.Utils;
import com.eazy.daikou.model.parking.AllComment;

import java.util.List;

public class ListAllCommentAdapter extends RecyclerView.Adapter<ListAllCommentAdapter.ItemViewHolder> {

    private final List<AllComment> allComments;

    public ListAllCommentAdapter(List<AllComment> allComments) {
        this.allComments = allComments;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_parking_all_comment, parent, false);
        return new ItemViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        AllComment comment = allComments.get(position);
            if(comment!=null){
                if(comment.getName()!=null){
                    holder.nameSecurityTv.setVisibility(View.VISIBLE);
                    holder.nameSecurityTv.setText(comment.getName());
                }else {
                    holder.nameSecurityTv.setVisibility(View.GONE);
                }
                if(comment.getCommenterUserName()!=null){
                    holder.nameSecurityTv.setVisibility(View.VISIBLE);
                    holder.nameSecurityTv.setText(comment.getCommenterUserName());
                }else {
                    holder.nameSecurityTv.setVisibility(View.GONE);
                }
                holder.dateTv.setText(DateUtil.formatTimeLocal(comment.getCreatedDate()));
                if(comment.getComment() != null && !comment.getComment().isEmpty()){
                    holder.commentTv.setText(comment.getComment());
                }else {
                    holder.commentTv.setText(Utils.getText(holder.commentTv.getContext(), R.string.no_reason));
                }
                if(comment.getAllImages().size() != 0 ){
                    AbsoluteFitLayoutManager linearLayoutManager = new AbsoluteFitLayoutManager(holder.itemView.getContext(),1, RecyclerView.HORIZONTAL,false,3);
                    holder.recyclerView.setLayoutManager(linearLayoutManager);
                    ListAllImageAdapter adapter = new ListAllImageAdapter(comment.getAllImages());
                    holder.recyclerView.setAdapter(adapter);
                }
            }
    }
    @Override
    public int getItemCount() {
        return allComments.size();
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder{
        private final TextView commentTv,dateTv,nameSecurityTv;
        private final RecyclerView recyclerView;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            dateTv = itemView.findViewById(R.id.date_time_create);
            commentTv = itemView.findViewById(R.id.title_comment);
            recyclerView = itemView.findViewById(R.id.list_image_com);
            nameSecurityTv = itemView.findViewById(R.id.name_security);
        }
    }

    public void clear() {
        int size = allComments.size ();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                allComments.remove ( 0 );
            }
            notifyItemRangeRemoved ( 0, size );
        }
    }
}
