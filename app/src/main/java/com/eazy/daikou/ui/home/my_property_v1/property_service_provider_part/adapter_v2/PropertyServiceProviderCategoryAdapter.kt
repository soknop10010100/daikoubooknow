package com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.adapter_v2

import android.content.Context
import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.my_property.service_provider_v2.ServiceProviderTypeModel

class PropertyServiceProviderCategoryAdapter(private val context: Context, private val listCategory: ArrayList<ServiceProviderTypeModel>, private val callBackListener: ItemCategoryCallBackListener): RecyclerView.Adapter<PropertyServiceProviderCategoryAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.category_menu_top_model, parent, false)
        return  ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val categoryModel : ServiceProviderTypeModel = listCategory[position]

        holder.nameCategoryTv.text = categoryModel.service_name

        if (categoryModel.isClick) {
            holder.nameCategoryTv.setTextColor(Utils.getColor(context, R.color.white))
            holder.linearLayout.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(context, R.color.appBarColorOld))
        } else {
            holder.linearLayout.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(context, R.color.light_grey))
            holder.nameCategoryTv.setTextColor(Utils.getColor(context, R.color.gray))
        }

        holder.itemView.setOnClickListener { callBackListener.callbackItem(categoryModel) }
    }

    override fun getItemCount(): Int {
        return listCategory.size
    }

    class ViewHolder( view: View): RecyclerView.ViewHolder(view){

        var linearLayout: CardView = view.findViewById(R.id.linear_card)
        var nameCategoryTv: TextView = view.findViewById(R.id.text_category)
    }

    interface ItemCategoryCallBackListener{
        fun callbackItem(categoryModel : ServiceProviderTypeModel)
    }
}