package com.eazy.daikou.ui.home.create_case_sale_and_rent.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.model.buy_sell.ItemSaleAndRentModel
import com.eazy.daikou.model.hr.ItemOvertimeModel

class ItemTypeAdapter(private val context: Context, private val listItem: ArrayList<ItemSaleAndRentModel>, private val callBackItem : CallBackItemClick): RecyclerView.Adapter<ItemTypeAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
       val view = LayoutInflater.from(context).inflate(R.layout.layout_item_select_overtime, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
     val data: ItemSaleAndRentModel = listItem[position]
        if (data != null){
            holder.itemNameTv.text = data.nameItem
         //   holder.imageCheckSelect.visibility = if(data.isClick) View.VISIBLE else View.INVISIBLE
            holder.itemView.setOnClickListener { callBackItem.clickItemCallListener(data) }
        }
    }

    override fun getItemCount(): Int {
        return listItem.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view){
        var itemNameTv:TextView = view.findViewById(R.id.ItemNameTv)
        var imageCheckSelect: ImageView = view.findViewById(R.id.imageCheckSelect)
    }
    interface CallBackItemClick{
        fun clickItemCallListener(data: ItemSaleAndRentModel)
    }
}