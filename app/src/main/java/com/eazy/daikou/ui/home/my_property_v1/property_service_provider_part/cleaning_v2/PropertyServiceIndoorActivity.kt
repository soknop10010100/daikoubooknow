package com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.cleaning_v2

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.request_data.request.service_property_ws_v2.ServiceProviderPropertyWs
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.my_property.service_provider_v2.ServiceProviderCompanyPropertyModel
import com.eazy.daikou.model.my_property.service_provider_v2.ServiceProviderTypeModel
import com.eazy.daikou.model.profile.User
import com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.activity.ListHistoryCleaningServiceActivity
import com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.adapter_v2.PropertyServiceProviderAdapter
import com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.adapter_v2.PropertyServiceProviderCategoryAdapter
import com.google.gson.Gson

class PropertyServiceIndoorActivity : BaseActivity() {

    private lateinit var recyclerViewType: RecyclerView
    private lateinit var adapterTypeService: PropertyServiceProviderCategoryAdapter
    private var listTypeService: ArrayList<ServiceProviderTypeModel> = ArrayList()

    private lateinit var refreshLayout: SwipeRefreshLayout
    private lateinit var recyclerViewListCompany: RecyclerView
    private lateinit var adapterServiceProvider: PropertyServiceProviderAdapter
    private var listCompanyService: ArrayList<ServiceProviderCompanyPropertyModel> = ArrayList()

    private lateinit var progressBar: ProgressBar

    private lateinit var  layoutManagerProvider: LinearLayoutManager
    private lateinit var user: User
    private var currentPage:Int = 1
    private var currentItem = 0
    private var total : Int = 0
    private var scrollDown : Int = 0
    private var size : Int = 10
    private var isScrolling = false
    private var isScrollItem = false

    private var serviceKey: String = ""
    private var serviceName: String = ""
    private var operationPart: String = ""
    private lateinit var switchMenuTop : ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_property_in)

        initView()

        initAction()

    }

    private fun initView(){
        recyclerViewType = findViewById(R.id.list_all_tap)
        recyclerViewListCompany = findViewById(R.id.list_property)
        refreshLayout = findViewById(R.id.swipe_layouts)
        progressBar = findViewById(R.id.progressItem)
        findViewById<LinearLayout>(R.id.layoutMap).visibility = View.VISIBLE
        switchMenuTop = findViewById(R.id.addressMap)
    }

    private fun initAction(){

        Utils.customOnToolbar(this, resources.getString(R.string.property_service)){finish()}

        user = Gson().fromJson(UserSessionManagement(this).userDetail, User::class.java)

        refreshLayout.setColorSchemeResources(R.color.colorPrimary)
        refreshLayout.setOnRefreshListener { this.onRefreshList() }

        val layoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
        recyclerViewType.layoutManager = layoutManager
        adapterTypeService = PropertyServiceProviderCategoryAdapter(this, listTypeService, callbackTypeService)
        recyclerViewType.adapter = adapterTypeService

        requestServiceTypeService()

        initRecyclerListPropertyProvider()

        onScrollItemServiceProvider(recyclerViewListCompany)

        switchMenuTop.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_report_description))
        switchMenuTop.setOnClickListener(
            CustomSetOnClickViewListener{
                startHistoryServiceCleaning("all", true)
            }
        )
    }

    private fun onRefreshList(){
        currentPage = 1
        size = 10
        isScrolling = true
        adapterServiceProvider.clear()

        requestServiceCompanyService()
    }

    private val callbackTypeService: PropertyServiceProviderCategoryAdapter.ItemCategoryCallBackListener = object : PropertyServiceProviderCategoryAdapter.ItemCategoryCallBackListener{
        override fun callbackItem(categoryModel: ServiceProviderTypeModel) {
            serviceKey = categoryModel.service_key!!
            serviceName = categoryModel.service_name!!
            operationPart = categoryModel.operation_part!!
            for (i in listTypeService.indices){
                listTypeService[i].isClick = categoryModel.service_key == listTypeService[i].service_key
            }
            adapterTypeService.notifyDataSetChanged()

            onRefreshList()
        }
    }

    private fun requestServiceTypeService(){
        ServiceProviderPropertyWs().getCategoryTabServiceProviderWs(this, callBackTypeService)
    }

    private val callBackTypeService: ServiceProviderPropertyWs.OnCallBackTypeListener = object : ServiceProviderPropertyWs.OnCallBackTypeListener{
        override fun onLoadSuccessFull(listServiceProvider: ArrayList<ServiceProviderTypeModel>) {
            progressBar.visibility = View.GONE
            listTypeService.addAll(listServiceProvider)
            if (listTypeService.size > 0) {
                listTypeService[0].isClick = true
                serviceKey =  listTypeService[0].service_key!!
                serviceName =  listTypeService[0].service_name!!
                operationPart = listTypeService[0].operation_part!!
                requestServiceCompanyService()
            }
            adapterTypeService.notifyDataSetChanged()
        }

        override fun onLoadFailed(message: String) {
            progressBar.visibility = View.GONE
            Utils.customToastMsgError(this@PropertyServiceIndoorActivity, message, false)
        }
    }

    private fun initRecyclerListPropertyProvider(){
        layoutManagerProvider = LinearLayoutManager(this)
        recyclerViewListCompany.layoutManager = layoutManagerProvider
        adapterServiceProvider = PropertyServiceProviderAdapter(this, listCompanyService, callBackItemProvider)
        recyclerViewListCompany.adapter = adapterServiceProvider
    }

    private val callBackItemProvider: PropertyServiceProviderAdapter.CallBackItemServiceProviderProperty = object : PropertyServiceProviderAdapter.CallBackItemServiceProviderProperty{
        override fun callBackItem(listServiceProvider: ServiceProviderCompanyPropertyModel) {
            val intent = Intent(this@PropertyServiceIndoorActivity, ListHistoryCleaningServiceActivity::class.java)
            intent.putExtra("operation_part", operationPart)
            intent.putExtra("company_account_id",listServiceProvider.id )
            startActivity(intent)
        }
    }

    private fun startHistoryServiceCleaning(operationPart : String, actionHistory : Boolean){
        val intent = Intent(this@PropertyServiceIndoorActivity, ListHistoryCleaningServiceActivity::class.java)
        intent.putExtra("operation_part", operationPart)
        intent.putExtra("action_history", actionHistory)
        startActivity(intent)
    }

    private fun requestServiceCompanyService(){
        progressBar.visibility = View.VISIBLE
        ServiceProviderPropertyWs().getListCompanyServiceProviderWS(this@PropertyServiceIndoorActivity, currentPage,10, user.accountId, serviceKey, serviceName, operationPart, callBackCompanyService)
    }
    private val callBackCompanyService: ServiceProviderPropertyWs.OnCallBackCompanyListener = object : ServiceProviderPropertyWs.OnCallBackCompanyListener{
        override fun onLoadSuccessFull(listServiceProvider: ArrayList<ServiceProviderCompanyPropertyModel>) {
            progressBar.visibility = View.GONE
            refreshLayout.isRefreshing = false
            listCompanyService.clear()

            listCompanyService.addAll(listServiceProvider)
            adapterServiceProvider.notifyDataSetChanged()

            Utils.validateViewNoItemFound(this@PropertyServiceIndoorActivity, recyclerViewListCompany, listCompanyService.size <= 0)
        }

        override fun onLoadFailed(message: String) {
            progressBar.visibility = View.GONE
            refreshLayout.isRefreshing = false
            Utils.customToastMsgError(this@PropertyServiceIndoorActivity, message, false)
        }
    }

    private fun onScrollItemServiceProvider(recyclerViewItem: RecyclerView) {
        recyclerViewItem.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true
                }
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                currentItem = layoutManagerProvider.childCount
                total = layoutManagerProvider.itemCount
                scrollDown = layoutManagerProvider.findFirstVisibleItemPosition()
                if (dy > 0) {
                    if (isScrolling && currentItem + scrollDown == total) {
                        if (total == size) {
                            isScrolling = false
                            currentPage++
                            progressBar.visibility = View.VISIBLE
                            isScrollItem = true
                            recyclerViewItem.scrollToPosition(listCompanyService.size - 1)
                            requestServiceCompanyService()
                            size += 10
                        } else {
                            progressBar.visibility = View.GONE
                        }
                    }
                }
            }
        })
    }

}