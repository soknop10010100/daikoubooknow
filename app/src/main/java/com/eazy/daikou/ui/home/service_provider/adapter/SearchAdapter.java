package com.eazy.daikou.ui.home.service_provider.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.eazy.daikou.R;
import com.eazy.daikou.model.my_property.service_provider.LatLngServiceProvider;
import com.eazy.daikou.model.law.LawsDetailModel;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.ViewHolder> {

    private final List<LatLngServiceProvider> providerList;
    private final ItemClickServiceProvider itemClickServiceProvider;

    public SearchAdapter(List<LatLngServiceProvider> providerList, ItemClickServiceProvider itemClickServiceProvider) {
        this.providerList = providerList;
        this.itemClickServiceProvider = itemClickServiceProvider;
    }

    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
        View view  = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_provider_adater,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull ViewHolder holder, int position) {
        LatLngServiceProvider listService = providerList.get(position);
        if(listService!=null){
            holder.titleTv.setText(listService.getContractorName());
            if(listService.getCompanyContact()!=null){
                holder.phoneTv.setText(listService.getCompanyContact());
            }else {
                holder.phoneTv.setText(R.string.no_phone);
            }
           if(listService.getCompanyLogo()!=null){
               Glide.with(holder.itemView.getContext()).load(listService.getCompanyLogo()).into(holder.imageView);
           }else {
               Glide.with(holder.itemView.getContext()).load(R.drawable.no_image).into(holder.imageView);
           }
           holder.btnCall.setOnClickListener(view -> itemClickServiceProvider.onClickItemCall(listService.getCompanyContact()));
           holder.addressTv.setText(listService.getAddress());
           holder.linearLayout.setOnClickListener(view -> itemClickServiceProvider.onClickItem(listService.getCategory(),listService.getContractorName(),listService));
        }

    }

    @Override
    public int getItemCount() {
        return providerList.size();
    }

    public interface ItemClickServiceProvider{
        void onClickItem(LawsDetailModel.Category id, String name, LatLngServiceProvider latLngServiceProvider);
        void onClickItemCall(String text);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        TextView titleTv,phoneTv,addressTv;
        ImageView imageView;
        RelativeLayout btnCall;
        LinearLayout linearLayout;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            titleTv = itemView.findViewById(R.id.name);
            phoneTv = itemView.findViewById(R.id.phoneTV);
            imageView = itemView.findViewById(R.id.item_img);
            addressTv = itemView.findViewById(R.id.address_tex);
            btnCall = itemView.findViewById(R.id.call);
            linearLayout = itemView.findViewById(R.id.linear);
        }
    }
    public void clear() {
        int size = providerList.size ();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                providerList.remove ( 0 );
            }
            notifyItemRangeRemoved ( 0, size );
        }
    }
}
