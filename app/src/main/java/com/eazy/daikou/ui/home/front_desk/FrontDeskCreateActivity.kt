package com.eazy.daikou.ui.home.front_desk

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.activity.result.contract.ActivityResultContracts
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.base.BaseCameraActivity
import com.eazy.daikou.helper.GetDataUtils
import com.eazy.daikou.request_data.request.complain_ws.ComplaintListWS
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.front_desk.SearchFrontDeskModel
import com.eazy.daikou.model.my_property.my_property.PropertyMyUnitModel
import com.eazy.daikou.model.profile.PropertyItemModel
import com.eazy.daikou.model.profile.UnitNoModel
import com.eazy.daikou.model.work_order.ImageListModel
import com.eazy.daikou.ui.home.complaint_solution.complaint.adapter.AdapterItemListComplaint
import com.eazy.daikou.ui.home.complaint_solution.complaint.fragment.SelectAccountAndUnitNoBottomSheet
import com.eazy.daikou.ui.home.front_desk.fragment.ItemCategoryUnitFragment
import com.eazy.daikou.ui.home.inspection_work_order.adapter.ImageListWorkOrderAdapter
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout

class FrontDeskCreateActivity : BaseActivity() {

    private lateinit var listImageRecyclerView: RecyclerView
    private lateinit var unitTv: TextInputEditText
    private lateinit var accountTv: TextInputEditText
    private lateinit var btnAddImg: LinearLayout
    private lateinit var progressBar: ProgressBar
    private lateinit var phoneEd: TextInputEditText
    private lateinit var nameDeliveryEd: TextInputEditText
    private lateinit var phoneDeliveryEd: TextInputEditText
    private lateinit var descriptionEd: TextInputEditText
    private lateinit var subjectEd: TextInputEditText
    private lateinit var btnCreate: LinearLayout

    private var bitmapList: ArrayList<ImageListModel> = ArrayList()
    private var post = 0
    private var userId = ""
    private var accountId = ""
    private var unitId = ""
    private var isUnitCreated = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_front_desk_create)

        initView()

        initAction()
    }

    private fun initView() {
        btnCreate = findViewById(R.id.btnCreate)
        progressBar = findViewById(R.id.progressItem)
        unitTv = findViewById(R.id.unitTv)
        accountTv = findViewById(R.id.accountTv)
        btnAddImg = findViewById(R.id.btnAddImg)
        phoneEd = findViewById(R.id.phoneEd)
        nameDeliveryEd = findViewById(R.id.nameDeliveryEd)
        phoneDeliveryEd = findViewById(R.id.phoneDeliveryEd)
        listImageRecyclerView = findViewById(R.id.listImageRecyclerView)
        descriptionEd = findViewById(R.id.descriptionEd)
        subjectEd = findViewById(R.id.subjectEd)
        progressBar.visibility = View.GONE

        Utils.customOnToolbar(this, resources.getString(R.string.create_front_desk)) { finish() }
    }

    private fun initAction() {
        accountId = GetDataUtils.getDataFromString("account_id", this)

        val accountName = GetDataUtils.getDataFromString("account_name", this)
        accountTv.setText(accountName)

        findViewById<TextView>(R.id.accountLayout).setOnClickListener (CustomSetOnClickViewListener{
            bottomSheetCategoryService("property")
        })

        findViewById<TextView>(R.id.unitLayout).setOnClickListener (CustomSetOnClickViewListener{
            bottomSheetCategoryService("unit_no")
        })

        btnAddImg.setOnClickListener {
            startForResult.launch(
                Intent(
                    this,
                    BaseCameraActivity::class.java
                )
            )
        }

        btnCreate.setOnClickListener(CustomSetOnClickViewListener { requestServiceWs() })

        findViewById<RadioGroup>(R.id.rGroup).setOnCheckedChangeListener { _, checkedId ->
            if (checkedId == R.id.rUnit) {
                isUnitCreated = true
            } else if (checkedId == R.id.rGeneral) {
                isUnitCreated = false
            }
            findViewById<FrameLayout>(R.id.unitNoLayout).visibility = if (isUnitCreated)   View.VISIBLE else View.GONE
        }

    }


    private fun requestServiceWs() {
        if (accountId == "") {
            Utils.customToastMsgError(this, resources.getString(R.string.please_input_property_name), false)
            return
        } else if (isUnitCreated && unitId == ""){
            Utils.customToastMsgError(this, resources.getString(R.string.please_select_unit_no), false)
            return
        }

        val hashMap = HashMap<String, Any>()

        hashMap["object_type"] = if (isUnitCreated) "unit" else "account"
        hashMap["object_id"] = if (isUnitCreated) unitId else accountId
        hashMap["post_type"] = "frontdesk"
        hashMap["account_id"] = accountId

        hashMap["subject"] = subjectEd.text.toString()
        hashMap["description"] = descriptionEd.text.toString()

        if (bitmapList.size > 0){
            val imageList : java.util.ArrayList<String> = java.util.ArrayList()
            for (item in bitmapList){
                imageList.add(Utils.convert(item.bitmapImage))
            }
            hashMap["file_type"] = "image"
            hashMap["file_items"] = imageList
        }


        hashMap["user_id"] = userId
        hashMap["user_phone"] = phoneEd.text.toString()
        hashMap["delivery_name"] = nameDeliveryEd.text.toString()
        hashMap["delivery_phone"] = phoneDeliveryEd.text.toString()

        progressBar.visibility = View.VISIBLE
        ComplaintListWS.createComplaintWS(this, hashMap, object : ComplaintListWS.CallBackCreateComplaint{
            override fun onSuccessFull(message: String) {
                progressBar.visibility = View.GONE
                Utils.customToastMsgError(this@FrontDeskCreateActivity, message, true)

                setResult(RESULT_OK)
                finish()
            }

            override fun onFailed(mes: String) {
                progressBar.visibility = View.GONE
                Utils.customToastMsgError(this@FrontDeskCreateActivity, mes, false)
            }

        })
    }

    private val startForResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                val data = result.data
                if (data!!.hasExtra("image_path")) {
                    val imagePath = data.getStringExtra("image_path")
                    addUrlImage("", imagePath!!)
                    setImageRecyclerView(post)
                }
                if (data.hasExtra("select_image")) {
                    val imagePath = data.getStringExtra("select_image")
                    addUrlImage("", imagePath!!)
                    setImageRecyclerView(post)
                }
            }
        }

    private fun setImageRecyclerView(post: Int) {
        listImageRecyclerView.layoutManager =
            GridLayoutManager(this, 1, RecyclerView.HORIZONTAL, false)
        val pickUpAdapter = ImageListWorkOrderAdapter(this, post, bitmapList, onClearImage)
        listImageRecyclerView.adapter = pickUpAdapter
        btnAddImg.visibility = Utils.visibleView(bitmapList)
    }

    private fun addUrlImage(posKeyDefine: String, imagePath: String) {
        val loadedBitmap = BaseCameraActivity.convertByteToBitmap(imagePath)
        post += 1
        val imageListModel = ImageListModel()
        imageListModel.keyImage = posKeyDefine
        imageListModel.bitmapImage = loadedBitmap
        bitmapList.add(imageListModel)
    }

    private fun bottomSheetCategoryService(actionType : String){
        val bottomSheet = SelectAccountAndUnitNoBottomSheet.newInstance(actionType, accountId)
        bottomSheet.initDataListener(callBackItemClickFormBottomSheetProperty)
        bottomSheet.show(supportFragmentManager,"action_type")
    }

    private val callBackItemClickFormBottomSheetProperty : AdapterItemListComplaint.CallBackItemClickListener = object :  AdapterItemListComplaint.CallBackItemClickListener {
        override fun onClickAccount(itemComplaint: PropertyItemModel) {
            accountTv.setText(itemComplaint.name)
            accountId = itemComplaint.id.toString()

            if (isUnitCreated){
                unitId = ""
                unitTv.setText("")
            }
        }

        override fun onClickUnit(myUnit: UnitNoModel) {
            unitTv.setText(myUnit.name)
            unitId = myUnit.id.toString()
        }
    }

    private val onClearImage: ImageListWorkOrderAdapter.OnClearImage =
        object : ImageListWorkOrderAdapter.OnClearImage {
            override fun onClickRemove(bitmap: ImageListModel, post: Int) {
                removeImage(post, bitmap)
            }

            override fun onViewImage(bitmap: ImageListModel) {
                if (bitmap.bitmapImage != null) {
                    Utils.openImageOnDialog(
                        this@FrontDeskCreateActivity,
                        Utils.convert(bitmap.bitmapImage)
                    )
                }
            }
        }

    private fun removeImage(position: Int, bitmap: ImageListModel) {
        bitmapList.remove(bitmap)
        setImageRecyclerView(position)
    }
}