package com.eazy.daikou.ui.home.service_provider

import android.content.Intent
import android.graphics.Point
import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.helper.GPSTracker
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import java.io.IOException
import java.util.*

private lateinit var mMap: GoogleMap
private lateinit var ImgCurrentPin: ImageView
private var latNumber: Double = 0.0
private var longNumber: Double = 0.0
private lateinit var geocoder: Geocoder
private var saveCountryCode = ""
private  var fullAddress = ""


class AddressServiceProviderActivity : BaseActivity(), OnMapReadyCallback {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_address_service_provider)

        initView()
        initAction()
    }

    private fun initView(){
        ImgCurrentPin = findViewById(R.id.ImgCurrentPin)
    }
    private fun initAction(){

        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        findViewById<ImageView>(R.id.iconBack).setOnClickListener { finish() }
        findViewById<TextView>(R.id.btnSave).setOnClickListener {
            val address = displayAddressByImageCenter()
            popupInformLocation(address)
        }

        callLocation()
        geocoder = Geocoder(this, Locale.getDefault())
    }
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap.mapType = GoogleMap.MAP_TYPE_NORMAL

        val location = LatLng(latNumber, longNumber)
        mMap.setOnCameraIdleListener { displayAddressByImageCenter() }
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 18.0f))

        mMap.isMyLocationEnabled = true
        mMap.uiSettings.isZoomControlsEnabled = false
        mMap.uiSettings.isMyLocationButtonEnabled = true
        mMap.uiSettings.isCompassEnabled = false //hide compass

    }

    private fun displayAddressByImageCenter(): String {
        try {
            val h = ImgCurrentPin.height
            val w = ImgCurrentPin.width / 2
            val x = ImgCurrentPin.x.toInt() + w
            val y = ImgCurrentPin.y.toInt() + h / 2
            val ll = mMap.projection.fromScreenLocation(Point(x, y + 45))
            val addresses = geocoder.getFromLocation(ll.latitude, ll.longitude, 1)
            val fullAdd: String = getFullAddress(addresses)
            try { saveCountryCode = if (addresses[0].countryName != null) addresses[0].countryCode else country_code
            } catch (e: Exception) {
                e.printStackTrace()
            }
            findViewById<TextView>(R.id.textAddressTv).text = String.format("%s", fullAdd)
            fullAddress = fullAdd
//            saveLat = ll.latitude
//            saveLng = ll.longitude
            return fullAdd
        } catch (e: IOException) {
        }
       return ""
    }
    private fun getFullAddress(addresses: List<Address>): String {
        return if (addresses.isNotEmpty()) {
            addresses[0].getAddressLine(0)
        } else ""
    }

    private fun popupInformLocation(fullAddress: String) {
        val builder = AlertDialog.Builder(this)
        builder.setMessage(fullAddress)
        builder.setNegativeButton(getString(R.string.no), null)
        builder.setPositiveButton(getString(R.string.yes)) { _, _ ->
            val data = Intent()
            data.putExtra("address_location", fullAddress)
            data.putExtra("lat", latNumber)
            data.putExtra("lng", longNumber)
            setResult(RESULT_OK, data)
            finish()
        }
        builder.show()
    }

    private fun callLocation() {
        val gps = GPSTracker(this)
        if (gps.canGetLocation()) {
            latNumber = gps.latitude
            longNumber = gps.longitude
        }
    }

}