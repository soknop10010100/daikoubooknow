package com.eazy.daikou.ui.home.quote_book;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentContainerView;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.eazy.daikou.R;
import com.eazy.daikou.base.BaseActivity;
import com.eazy.daikou.request_data.request.book_quote_ws.QuoteWs;
import com.eazy.daikou.helper.Utils;
import com.eazy.daikou.model.book.BookModelV2;
import com.eazy.daikou.model.book.BookCategoryV2;


import java.util.ArrayList;
import java.util.List;

public class QuoteBookActivity extends BaseActivity {

    private ImageView btnBack;
    private TextView titleToolbar;
    private TextView tvBtnBook;
    private TextView tvBtnQuote;
    private LinearLayout linearLayout;
    private FragmentContainerView fragmentContainerView;
    private ProgressBar progressBar;

    private final List<BookModelV2> bookModelList = new ArrayList<>();
    private final List<BookModelV2> quoteModelList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quote_book);

        initView();

        initAction();

    }

    private void initView(){
        tvBtnQuote = findViewById(R.id.tvBtnQuote);
        titleToolbar = findViewById(R.id.titleToolbar);
        linearLayout = findViewById(R.id.linearLayout);
        btnBack = findViewById(R.id.iconBack);
        tvBtnBook = findViewById(R.id.tvBtnBook);
        tvBtnQuote = findViewById(R.id.tvBtnQuote);
        fragmentContainerView = findViewById(R.id.containerQuoteBook);
        progressBar = findViewById(R.id.progressItem);

    }

    private  void  initAction(){
        titleToolbar.setText(Utils.getText(this, R.string.quotes_books));

        getLinearLayout(linearLayout);

        onRequestListQuoteBook();

    }

    private void getLinearLayout(LinearLayout linearLayout){
        btnBack.setOnClickListener(v->finish());
        for (int i = 0; i < linearLayout.getChildCount(); i++) {
            int finalI = i;
            linearLayout.getChildAt(i).setOnClickListener(view -> {
                setBackgroundMenu(finalI);
                if (finalI == 0){
                    replaceViewFragment(BookFragment.newInstance(bookModelList, "book"));
                } else {
                    replaceViewFragment(BookFragment.newInstance(quoteModelList, "quote"));
                }
            });
        }
    }
    private void replaceViewFragment(Fragment fragment){
        FragmentManager fm = getSupportFragmentManager();
        fragmentContainerView = findViewById(R.id.containerQuoteBook);
        FragmentTransaction transaction = fm.beginTransaction();
        if (fragmentContainerView != null) {
            transaction.replace(R.id.containerQuoteBook, fragment, fragment.getTag());
            transaction.disallowAddToBackStack();
            transaction.commit();
        }
    }

    private void setBackgroundMenu(int position){
        if (position == 0){
            Utils.setBgTint(tvBtnBook, R.color.color_gray_item);
            Utils.setBgTint(tvBtnQuote, R.color.transparent);
            tvBtnBook.setTextColor(Color.WHITE);
            tvBtnQuote.setTextColor(Color.GRAY);
        } else {
            Utils.setBgTint(tvBtnQuote, R.color.color_gray_item);
            Utils.setBgTint(tvBtnBook, R.color.transparent);
            tvBtnBook.setTextColor(Color.GRAY);
            tvBtnQuote.setTextColor(Color.WHITE);
        }
    }

    //Request web service


    private void onRequestListQuoteBook() {
        new QuoteWs().getQuoteBookNew(QuoteBookActivity.this, 10, 1, quoteBookNewCallBackListener);
    }

    private final QuoteWs.QuoteBookNewCallBackListener quoteBookNewCallBackListener = new QuoteWs.QuoteBookNewCallBackListener() {

        @Override
        public void getQuoteBook(BookCategoryV2 quoteAndBookModelV2) {
            progressBar.setVisibility(View.GONE);

            for (BookModelV2 bookQuoteModel : quoteAndBookModelV2.getBookList()) {
                if (bookQuoteModel.getAll().size() > 0){
                    bookModelList.add(bookQuoteModel);
                }
            }

            for (BookModelV2 bookQuoteModel : quoteAndBookModelV2.getQuoteList()) {
                if (bookQuoteModel.getAll().size() > 0){
                    quoteModelList.add(bookQuoteModel);
                }
            }
            replaceViewFragment(BookFragment.newInstance(bookModelList, "book"));
        }

        @Override
        public void onFailed(String error) {
            progressBar.setVisibility(View.GONE);
            Toast.makeText(QuoteBookActivity.this, error, Toast.LENGTH_SHORT).show();
        }
    };
}