package com.eazy.daikou.ui.home.home_menu

import android.app.Activity.RESULT_OK
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.activity.result.contract.ActivityResultContracts
import androidx.cardview.widget.CardView
import androidx.core.content.res.ResourcesCompat
import androidx.core.widget.NestedScrollView
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.denzcoskun.imageslider.ImageSlider
import com.denzcoskun.imageslider.constants.ScaleTypes
import com.denzcoskun.imageslider.models.SlideModel
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.base.BaseFragment
import com.eazy.daikou.base.MockUpData
import com.eazy.daikou.request_data.request.book_now_ws.RequestHashMapData
import com.eazy.daikou.request_data.request.home_ws.HomePageWs
import com.eazy.daikou.request_data.request.home_ws.HomeWs
import com.eazy.daikou.request_data.request.profile_ws.LoginWs
import com.eazy.daikou.helper.AppAlertCusDialog
import com.eazy.daikou.helper.BroadcastTypes
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.home.HomeViewModel
import com.eazy.daikou.model.home.MainItemHomeModel
import com.eazy.daikou.model.home.UserDeactivated
import com.eazy.daikou.model.home_page.DevelopmentProjects
import com.eazy.daikou.model.home_page.PropertyListing
import com.eazy.daikou.model.profile.User
import com.eazy.daikou.model.profile.UserTypeProfileModel
import com.eazy.daikou.ui.ScannerQRCodeAllActivity
import com.eazy.daikou.ui.home.create_case_sale_and_rent.sale_and_rent.DetailListSaleRentActivity
import com.eazy.daikou.ui.home.emergency.EmergencyActivity
import com.eazy.daikou.ui.home.home_menu.adapter.HomeIconMenuAdapter
import com.eazy.daikou.ui.home.home_menu.adapter.ProjectDevelopmentAdapter
import com.eazy.daikou.ui.home.home_menu.adapter.PropertyListBuySellRentAdapter
import com.eazy.daikou.ui.home.hrm.ListHRMActivity
import com.eazy.daikou.ui.home.hrm.MyRoleMainActivity
import com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.cleaning_v2.PropertyServiceIndoorActivity
import com.eazy.daikou.ui.home.development_project.PropertiesItemDetailActivity
import com.eazy.daikou.ui.home.development_project.PropertyDevelopmentListActivity
import com.eazy.daikou.ui.notifications.NotificationActivity
import com.eazy.daikou.ui.profile.SwitchProfileAndPropertyActivity
import com.google.gson.Gson
import de.hdodenhof.circleimageview.CircleImageView
import java.text.SimpleDateFormat
import java.util.*

class HomeFragment : BaseFragment() {

    private lateinit var recyclerView: RecyclerView
    private val homeIconMenuList: ArrayList<HomeViewModel> = ArrayList()
    private lateinit var progressBar: ProgressBar
    private lateinit var btnNotification: ImageView
    private lateinit var numberNotification: TextView

    //Profile
    private var user: User? = null
    private lateinit var btnScan: LinearLayout
    private lateinit var notificationLayout: FrameLayout
    private lateinit var layoutImage: RelativeLayout
    private lateinit var imageLogo: CircleImageView
    private lateinit var imageSlider: ImageSlider

    // Switch Profile
    private lateinit var switchUserLayout: CardView
    private lateinit var switchPropertyLayout: CardView
    private lateinit var userTypeTv: TextView
    private lateinit var propertyTv: TextView
    private lateinit var mainScrollView: NestedScrollView
    private lateinit var toolbarLayout: LinearLayout
    private lateinit var activityProfileLayout: LinearLayout
    private lateinit var activeAccLayout: LinearLayout

    //active account
    private var isDeactivated = false
    private lateinit var cancelDeactivate: TextView

    private lateinit var buttonEmergency: LinearLayout

    private lateinit var btnSeeMorePropertyListing: LinearLayout
    private lateinit var btnSeeMoreProjectDev: LinearLayout

    private lateinit var recyclerViewPropertyListing: RecyclerView
    private lateinit var recyclerViewProjectDevelopment: RecyclerView

    private var currentPage = 1
    private var limit = 10
    private var isAddMoreProject = true
    private var isRespond = true
    private var listProjectDevelopment = ArrayList<DevelopmentProjects>()
    private lateinit var adapterProjectDevelopmentAdapter: ProjectDevelopmentAdapter

    private var userProfileAccountList = ArrayList<UserTypeProfileModel>()

    private lateinit var propertyListingLayout : LinearLayout
    private lateinit var projectDevelopmentLayout : LinearLayout

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val rootView = inflater.inflate(R.layout.fragment_home, container, false)

        initView(rootView)

        initDataValue()

        initAction()

        return rootView
    }

    private fun initView(rootView: View) {
        recyclerView = rootView.findViewById(R.id.recyclerView)
        progressBar = rootView.findViewById(R.id.progressItem)
        btnNotification = rootView.findViewById(R.id.btnNotification)
        numberNotification = rootView.findViewById(R.id.number)
        btnScan = rootView.findViewById(R.id.btnScan)
        notificationLayout = rootView.findViewById(R.id.notificationLayout)
        layoutImage = rootView.findViewById(R.id.layoutImage)
        switchPropertyLayout = rootView.findViewById(R.id.switchPropertyLayout)
        switchUserLayout = rootView.findViewById(R.id.switchUserLayout)
        userTypeTv = rootView.findViewById(R.id.userTypeTv)
        propertyTv = rootView.findViewById(R.id.propertyTv)
        mainScrollView = rootView.findViewById(R.id.mainScroll)
        toolbarLayout = rootView.findViewById(R.id.profileToolBar)
        activityProfileLayout = rootView.findViewById(R.id.activityProfileLayout)

        activeAccLayout = rootView.findViewById(R.id.activeAccLayout)
        cancelDeactivate = rootView.findViewById(R.id.cancelDeactivate)

        buttonEmergency = rootView.findViewById(R.id.btn_emergency)
        btnSeeMorePropertyListing = rootView.findViewById(R.id.layout_see_more_property_listing)
        btnSeeMoreProjectDev = rootView.findViewById(R.id.layout_see_more_project_development)

        recyclerViewProjectDevelopment = rootView.findViewById(R.id.recyclerView_project_development)
        recyclerViewPropertyListing = rootView.findViewById(R.id.recyclerView_property_listing)

        imageLogo = rootView.findViewById(R.id.img_logo)

        imageSlider = rootView.findViewById(R.id.img_slider)

        projectDevelopmentLayout = rootView.findViewById(R.id.projectDevelopmentLayout)
        propertyListingLayout = rootView.findViewById(R.id.propertyListingLayout)
    }

    private fun initDataValue() {
        user = Gson().fromJson(UserSessionManagement(mActivity).userDetail, User::class.java)

        // Check layout user
        if (MockUpData.isUserAsGuest(UserSessionManagement(mActivity))) {
            activityProfileLayout.visibility = View.GONE
        } else {
            activityProfileLayout.visibility = View.VISIBLE
        }

        Utils.setValueOnText(userTypeTv, user!!.activeUserType)
        Utils.setValueOnText(propertyTv, user!!.accountName)

    }

    private fun initAction() {
        // Set height on image slide
        layoutImage.layoutParams.height = Utils.setHeightOfScreen(mActivity, 2.5)

        initCategoryHome()

        recyclerViewPropertyListing.layoutManager = LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false)

        val layoutMangerProjectDevelopment = StaggeredGridLayoutManager(2, LinearLayoutManager.VERTICAL)
        recyclerViewProjectDevelopment.layoutManager = layoutMangerProjectDevelopment
        adapterProjectDevelopmentAdapter = ProjectDevelopmentAdapter(listProjectDevelopment, onClickProjectDevelopment)
        recyclerViewProjectDevelopment.adapter = adapterProjectDevelopmentAdapter

        requestNotification()

        registerActionReceiver()

        requestServicePropertyWs()

        requestServiceProjectWs()

        updateImageSlider()

        //Click btn cancel deactivated account
        cancelDeactivate.setOnClickListener(
            CustomSetOnClickViewListener {
                val hashMap: HashMap<String, Any> = RequestHashMapData.hashMapUserActiveAccount(
                    "activate",
                    UserSessionManagement(mActivity).userId
                )
                progressBar.visibility = View.VISIBLE
                LoginWs().sendVerifyCode(
                    mActivity,
                    "activate",
                    hashMap,
                    object : LoginWs.OnDeleteAccListener {
                        override fun onSuccess(action: String?, msg: String?) {
                            requestItemList()
                        }

                        override fun onFailed(mess: String?) {
                            progressBar.visibility = View.GONE
                            Utils.customToastMsgError(mActivity, mess, false)
                        }

                    })
            }
        )

        // On Scroll Item
        mainScrollView.viewTreeObserver.addOnScrollChangedListener {
            if ((mainScrollView.getChildAt(0).bottom == mainScrollView.scrollY + mainScrollView.height) && isAddMoreProject && isRespond) {
                progressBar.visibility = View.VISIBLE
                currentPage ++
                isRespond = false
                requestServiceProjectWs()
            }
        }

        // Init Click Item

        notificationLayout.setOnClickListener(
            CustomSetOnClickViewListener {
                openSomeActivityForResult()
            }
        )

        btnScan.setOnClickListener(
            CustomSetOnClickViewListener {
                startNewActivity(ScannerQRCodeAllActivity::class.java, "home_screen")
            }
        )

        buttonEmergency.setOnClickListener{ startNewActivity(EmergencyActivity::class.java, "sending")}

        btnSeeMorePropertyListing.setOnClickListener {
            // startActivity(Intent(mActivity, BuyRentSaleActivity::class.java))
        }

        btnSeeMoreProjectDev.setOnClickListener {
            startActivity(Intent(activity, PropertyDevelopmentListActivity::class.java))
        }

        // Active user and account type
        switchUserLayout.setOnClickListener {
            switchProfileActive("switch_profile")
        }

        switchPropertyLayout.setOnClickListener {
            switchProfileActive("switch_property")
        }

    }

    private val onClickPropertyListing = object : PropertyListBuySellRentAdapter.OnClickItem {
        override fun onItemClick(data: PropertyListing) {
            val intent = Intent(mActivity, DetailListSaleRentActivity::class.java)
            intent.putExtra("id_list_detail", data.id)
            // startActivity(intent)
        }

    }

    private val callBackPropertyListing = object : HomePageWs.OnRespondPropertyListing {
        override fun onSuccess(listAccountsAndContacts: List<UserTypeProfileModel>, list: List<PropertyListing>) {
            // Check visible property listing
            if (list.isNotEmpty()){
                recyclerViewPropertyListing.adapter = PropertyListBuySellRentAdapter(list, mActivity, onClickPropertyListing)
                propertyListingLayout.visibility = View.VISIBLE
            } else {
                propertyListingLayout.visibility = View.GONE
            }

            // Account User
            userProfileAccountList.clear()
            userProfileAccountList.addAll(listAccountsAndContacts)

            // Set Account From User
            initDataValue()
        }

        override fun onGetItemFiled(msg: String) {
            Utils.customToastMsgError(mActivity, msg, false)
        }
    }

    private fun requestServicePropertyWs() {
        HomePageWs.getPropertyListing(mActivity, currentPage.toString(), limit.toString(), callBackPropertyListing)
    }

    private val onClickProjectDevelopment = object : ProjectDevelopmentAdapter.OnclickProject {
        override fun onClickProject(project: DevelopmentProjects) {
            val intent = Intent(mActivity, PropertiesItemDetailActivity::class.java)
            intent.putExtra("id", project.id)
            startActivity(intent)
        }

    }

    private val callBackProjectListener = object : HomePageWs.OnRespondProjectDevelopment {
        override fun onSuccess(list: List<DevelopmentProjects>) {
            if (list.isNotEmpty()) {
                isRespond = true
                listProjectDevelopment.addAll(list)
                adapterProjectDevelopmentAdapter.notifyItemChanged(list.size, listProjectDevelopment.size - list.size)
                progressBar.visibility = View.GONE
            } else {
                isAddMoreProject = false
                isRespond = true
            }

            projectDevelopmentLayout.visibility = if (listProjectDevelopment.size > 0) View.VISIBLE else View.GONE

        }

        override fun onGetItemFiled(msg: String) {
            isRespond = true
            progressBar.visibility = View.GONE
            Utils.customToastMsgError(mActivity, msg, false)
        }

    }

    private fun requestServiceProjectWs() {
        HomePageWs.getProjectDevelopment(mActivity, currentPage.toString(), limit.toString(), callBackProjectListener)
    }

    private val onClickCallBackListener = object : HomeIconMenuAdapter.ItemClickOnListener {
        override fun onItemClick(homeViewModel: HomeViewModel) {
            when (homeViewModel.action) {
                "property", "tracking", "inspection_maintenance", "education", "accessibility" -> {
                    val intent = Intent(mActivity, SubHomeMenusActivity::class.java)
                    intent.putExtra("action", homeViewModel.action)
                    startActivity(intent)
                }
                "sub_my_property" -> {
                    if (Utils.isEmployeeRole(mActivity)) {
                        AppAlertCusDialog.underConstructionDialog(mActivity, resources.getString(R.string.you_do_not_permission_to_access))
                        return
                    }
                    val intent = Intent(mActivity, MyRoleMainActivity::class.java)
                    intent.putExtra("action", homeViewModel.action)
                    startActivity(intent)
                }

                "property_listing" -> {
                    // startNewActivity(MyRoleMainActivity::class.java, homeViewModel.action)
                    AppAlertCusDialog.underConstructionDialog(mActivity, resources.getString(R.string.coming_soon))
                }

                "development_project" -> {
                    startNewActivity(PropertyDevelopmentListActivity::class.java, homeViewModel.action)
                }

                "scan_qrcode_all" -> startNewActivity(
                    ScannerQRCodeAllActivity::class.java,
                    "home_screen"
                )

                "facility_book" -> {
                    val intent = Intent(mActivity, SubHomeMenusActivity::class.java)
                    intent.putExtra("action", homeViewModel.action)
                    startActivity(intent)
                }
                "my_role" ->{
                    if (Utils.isEmployeeRole(mActivity)) {
                        val intent = Intent(mActivity, MyRoleMainActivity::class.java)
                        startActivity(intent)
                    } else {
                        AppAlertCusDialog.underConstructionDialog(mActivity, resources.getString(R.string.you_do_not_permission_to_access))
                    }
                }
                "hrm" -> {
                    if (Utils.isEmployeeRole(mActivity)) {
                        val intent = Intent(mActivity, ListHRMActivity::class.java)
                        startActivity(intent)
                    } else {
                        AppAlertCusDialog.underConstructionDialog(mActivity, resources.getString(R.string.you_do_not_permission_to_access))
                    }
                }

                "more" -> {
                    val intentMore = Intent(mActivity, MoreMenuIconActivity::class.java)
                    startActivity(intentMore)
                }

                "service_provider" -> {
                    // startActivity(Intent(mActivity, ServiceProviderHomeActivity::class.java))
                    AppAlertCusDialog.underConstructionDialog(mActivity, resources.getString(R.string.coming_soon))
                }

                "service_in" -> {
                    startActivity(Intent(mActivity, PropertyServiceIndoorActivity::class.java))
                }
                else -> {
                    AppAlertCusDialog.underConstructionDialog(mActivity, resources.getString(R.string.coming_soon))
                }
            }
        }
    }

    private fun startNewActivity(toActivityClass: Class<*>, action: String) {
        Utils.startNewActivity(mActivity, toActivityClass, "action", action)
    }

    private fun updateImageSlider() {
        val urlImages = "https://sandboxeazy.daikou.asia/img/app_home_slide_1.jpg"
        val mutableList = mutableListOf<String>()
        for (index in 1..10) {
            mutableList.add(
                urlImages.replace("1", index.toString())
            )
        }

        val slideModels = ArrayList<SlideModel>()
        for (index in 1..10) {
            slideModels.add(SlideModel(urlImages.replace("1", index.toString())))
        }
        imageSlider.setImageList(slideModels, ScaleTypes.CENTER_CROP)
    }

    private fun initCategoryHome() {
        // recyclerView.layoutManager = GridLayoutManager(mActivity, 4)
        recyclerView.layoutManager = GridLayoutManager(mActivity, 4)
        homeIconMenuList.addAll(getListHomeMenu(mActivity))
        val homeIconMenuAdapter = HomeIconMenuAdapter(homeIconMenuList, onClickCallBackListener)
        recyclerView.adapter = homeIconMenuAdapter
        recyclerView.isNestedScrollingEnabled = false
    }

    private fun registerActionReceiver() {
        LocalBroadcastManager.getInstance(mActivity)
            .registerReceiver(mMessageReceiver, IntentFilter(BroadcastTypes.UPDATE_IMAGE.name))
    }

    private val mMessageReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent?) {
            if (intent != null){
                if (intent.hasExtra("notification")) {
                    requestNotification()
                } else if (intent.hasExtra("user_account_name")){
                    propertyTv.text = intent.getStringExtra("user_account_name").toString()
                }
            }
        }
    }

    private fun openSomeActivityForResult() {
        val intent = Intent(mActivity, NotificationActivity::class.java)
        intent.putExtra("is_deactivated", isDeactivated)
        resultLauncher.launch(intent)
    }

    private var resultLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == RESULT_OK) {
                // data != null from chang profile image
                if (result.data != null) {
                    val action: String = result.data!!.getStringExtra("action").toString()
                    val name: String = result.data!!.getStringExtra("name").toString()
                    val propertyName: String = result.data!!.getStringExtra("property_name").toString()
                    if (action.equals("switch_profile", ignoreCase = true)) {
                        userTypeTv.text = name
                        if (!propertyName.equals("", ignoreCase = true)) {
                            propertyTv.text = propertyName
                        }
                    } else {
                        propertyTv.text = name
                    }
                } else {
                    requestNotification()
                }
            }
        }

    private fun requestItemList() {
        val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        val timeFormat = SimpleDateFormat("k:mm:ss", Locale.getDefault())
        val currentDateTime = simpleDateFormat.format(Date()) + " " + timeFormat.format(Date())
        HomeWs().getItemHomeList(mActivity, currentDateTime, object : HomeWs.HomeItemCallBack {
            override fun onSuccess(
                list: List<MainItemHomeModel>,
                userDeactivated: UserDeactivated
            ) {
                progressBar.visibility = View.GONE
                initItemRecyclerView(userDeactivated)
            }

            override fun onFailure(errorMessage: String?) {
                progressBar.visibility = View.GONE
                Utils.customToastMsgError(mActivity, errorMessage, false)
            }

        })
    }

    private fun initItemRecyclerView(userDeactivated: UserDeactivated) {
        //Check Active Acc Yet
        isDeactivated = userDeactivated.is_deactivated
        activeAccLayout.visibility = if (userDeactivated.is_deactivated) View.VISIBLE else View.GONE
        if (userDeactivated.is_expired) {
            AppAlertCusDialog.requestLogOut(mActivity, progressBar)
        }
    }

    private fun getListHomeMenu(context: Context): List<HomeViewModel> {
        homeIconMenuList.clear()
        val homeViewModels: MutableList<HomeViewModel> = ArrayList()
        homeViewModels.add(
            HomeViewModel(
                "sub_my_property",
                ResourcesCompat.getDrawable(context.resources, R.drawable.home_my_property, null),
                context.resources.getString(R.string.my_property)
            )
        )
        homeViewModels.add(
            HomeViewModel(
                "service_provider",
                ResourcesCompat.getDrawable(context.resources, R.drawable.home_ic_service, null),
                resources.getString(R.string.service)
            )
        )
        homeViewModels.add(
            HomeViewModel(
                "property_listing",
                ResourcesCompat.getDrawable(
                    context.resources,
                    R.drawable.home_project_sale_v2,
                    null
                ),
                context.resources.getString(R.string.sale_rent)
            )
        )
        homeViewModels.add(
            HomeViewModel(
                "my_role",
                ResourcesCompat.getDrawable(context.resources, R.drawable.home_ic_hr, null),
                context.getString(R.string.my_rool)
            )
        )
        homeViewModels.add(
            HomeViewModel(
                "development_project",
                ResourcesCompat.getDrawable(
                    context.resources,
                    R.drawable.home_project_development_v2,
                    null
                ),
                context.resources.getString(R.string.project)
            )
        )
        homeViewModels.add(
            HomeViewModel(
                "education",
                ResourcesCompat.getDrawable(context.resources, R.drawable.home_ic_education, null),
                context.resources.getString(R.string.education)
            )
        )
        homeViewModels.add(
            HomeViewModel(
                "accessibility",
                ResourcesCompat.getDrawable(
                    context.resources,
                    R.drawable.home_ic_accessibility,
                    null
                ),
                context.resources.getString(R.string.accessibility)
            )
        )
        homeViewModels.add(
            HomeViewModel(
                "more",
                ResourcesCompat.getDrawable(context.resources, R.drawable.home_ic_more, null),
                resources.getString(R.string.more)
            )
        )

        return homeViewModels
    }

    private fun requestNotification() {
//        ListAllNotificationWs().readNumberNotification(mActivity,
//            UserSessionManagement(mActivity).userId,
//            object : OnCallBackListNumberNotification {
//                override fun onSuccess(number: String) {
//                    if (number != "0") {
//                        numberNotification.text = number
//                        numberNotification.visibility = View.VISIBLE
//                    } else {
//                        numberNotification.visibility = View.INVISIBLE
//                    }
//                }
//
//                override fun onFailed(message: String) {
//                    Utils.customToastMsgError(mActivity, message, false)
//                }
//            })
    }

    private fun unRegisterActionReceiver(mActionReceiver: BroadcastReceiver) {
        LocalBroadcastManager.getInstance(mActivity).unregisterReceiver(mActionReceiver)
    }

    override fun onDestroy() {
        unRegisterActionReceiver(mMessageReceiver)
        super.onDestroy()
    }

    private fun switchProfileActive(action: String) {
        val intent = Intent(mActivity, SwitchProfileAndPropertyActivity::class.java)
        intent.putExtra("active_profile_list", userProfileAccountList)
        intent.putExtra("action", action)
        resultLauncher.launch(intent)
    }

}