package com.eazy.daikou.ui.home.hrm.report_attendance

import android.content.Intent
import android.content.res.ColorStateList
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.base.MockUpData
import com.eazy.daikou.request_data.request.hr_ws.MyAttendanceWs
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.helper.map.GPSTrackerActivity
import com.eazy.daikou.model.hr.HRAttendanceModel
import com.eazy.daikou.model.hr.HrAttendanceDetailModel
import com.eazy.daikou.model.hr.ItemAttendanceModel
import com.eazy.daikou.ui.home.hrm.report_attendance.adapter.HrAttendanceDetailAdapter
import java.util.*

class AttendanceReportHrDetailActivity : BaseActivity() {

    private lateinit var attendanceModel: ItemAttendanceModel

    private lateinit var breakOutTv : TextView
    private lateinit var breakInTv : TextView
    private lateinit var shiftInTv : TextView
    private lateinit var shiftOutTv : TextView
    private lateinit var totalTimeTv : TextView
    private lateinit var totalBreakTv : TextView

    private lateinit var btnBreak : TextView
    private lateinit var btnCheckOut : TextView
    private lateinit var progressBar: ProgressBar
    private lateinit var itemRecyclerView: RecyclerView
    private lateinit var noItemTv : TextView
    private var isBreakIn : Boolean = true
    private lateinit var itemLayout : RelativeLayout
    private var btnStatus = ""
    private var attendanceType = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_report_attendance)

        initView()

        initData()

        initAction()

    }

    private fun initView(){
        Utils.customOnToolbar(this, resources.getString(R.string.check_in_and_check_out_detail)) { finish() }
        findViewById<LinearLayout>(R.id.layoutMap).visibility = View.INVISIBLE

        shiftInTv = findViewById(R.id.shiftInTv)
        shiftOutTv = findViewById(R.id.shiftOutTv)
        breakOutTv = findViewById(R.id.breakTimeOutTv)
        breakInTv = findViewById(R.id.breakTimeInTv)
        totalTimeTv = findViewById(R.id.totalTimeTv)
        totalBreakTv = findViewById(R.id.totalBreakTv)

        btnBreak = findViewById(R.id.btnBreak)
        btnCheckOut = findViewById(R.id.btnCheckOut)
        progressBar = findViewById(R.id.progressItem)
        itemRecyclerView = findViewById(R.id.itemAttendance)
        noItemTv = findViewById(R.id.noItemTv)
        itemLayout = findViewById(R.id.itemLayout)
    }

    private fun initData(){
        if (intent != null && intent.hasExtra("item_attendance")){
            attendanceModel = intent.getSerializableExtra("item_attendance") as ItemAttendanceModel
        }

        if (intent != null && intent.hasExtra("action_type")){
            attendanceType = intent.getStringExtra("action_type").toString()
        }

        if (attendanceType == ""){
            if (attendanceModel.action_type !=  null) attendanceType = attendanceModel.action_type!!
        }
    }

    private fun initAction(){
        btnBreak.setOnClickListener{
            val message: String = if (isBreakIn) {
                resources.getString(R.string.are_you_sure_to_break_in)
            } else {
                resources.getString(R.string.are_you_sure_to_break_out)
            }
            Utils.customAlertDialogConfirm(this, message) {
                if (isBreakIn){
                    openSomeActivityForResult("break_in")
                } else {
                    openSomeActivityForResult("break_out")
                }
            }
        }

        btnCheckOut.setOnClickListener{
            val message = resources.getString(R.string.are_you_sure_to_check_out)
            Utils.customAlertDialogConfirm(this, message) { openSomeActivityForResult("check_out") }
        }

        itemRecyclerView.layoutManager = LinearLayoutManager(this)

        initListItemAttendanceDetail(attendanceType)

    }

    private fun checkButtonStatus(button_status : String, isBreak : Boolean){
        if(button_status == "check_in" && isBreak){
            btnBreak.visibility = View.VISIBLE
            btnCheckOut.visibility = View.GONE
            btnBreak.text = resources.getString(R.string.break_out)
            btnBreak.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(this, R.color.yellow))
            isBreakIn = false
        } else if(button_status == "check_out" && isBreak){
            btnBreak.visibility = View.VISIBLE
            btnCheckOut.visibility = View.GONE
            btnBreak.text = resources.getString(R.string.break_out)
            btnBreak.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(this, R.color.yellow))
            isBreakIn = false
        } else if(button_status == "check_out" && !isBreak){
            btnBreak.visibility = View.GONE
            btnCheckOut.visibility = View.VISIBLE
        } else if(button_status == "check_in" && !isBreak){
            btnBreak.visibility = View.GONE
            btnCheckOut.visibility = View.VISIBLE
        } else if(button_status == "break_out"){
            btnBreak.visibility = View.VISIBLE
            btnCheckOut.visibility = View.GONE
            btnBreak.text = resources.getString(R.string.break_in)
            btnBreak.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(this, R.color.color_button))
            isBreakIn = true
        } else if(button_status == "break_in"){
            btnBreak.visibility = View.GONE
            btnCheckOut.visibility = View.VISIBLE
        } else if(button_status == "check_out"){
            btnBreak.visibility = View.GONE
            btnCheckOut.visibility = View.VISIBLE
        } else if (button_status == "already_check_out" || button_status == "missed_check_out"){
            btnBreak.visibility = View.GONE
            btnCheckOut.visibility = View.GONE

            val layoutParams = itemRecyclerView.layoutParams as (RelativeLayout.LayoutParams)
            layoutParams.setMargins(0, 0, 0, 0)
            itemRecyclerView.layoutParams = layoutParams
        } else {
            btnBreak.visibility = View.GONE
            btnCheckOut.visibility = View.GONE

            val layoutParams = itemRecyclerView.layoutParams as (RelativeLayout.LayoutParams)
            layoutParams.setMargins(0, 0, 0, 0)
            itemRecyclerView.layoutParams = layoutParams
        }
    }

    private fun initListItemAttendanceDetail(attendanceType : String){
        MyAttendanceWs().getListAttendanceDetail(this, attendanceModel.id!!, attendanceType,object : MyAttendanceWs.OnCallBackListener{
            override fun onLoadListSuccess(hrAttendanceModel: HRAttendanceModel) {}

            override fun onLoadListDetailSuccess(hrAttendanceModel: HrAttendanceDetailModel) {
                progressBar.visibility = View.GONE

                setValue(hrAttendanceModel)

                if(hrAttendanceModel.my_attendance){
                    checkButtonStatus(hrAttendanceModel.button_status!!,hrAttendanceModel.show_break_out_button)
                }

                val hrAttendanceAdapter = HrAttendanceDetailAdapter(this@AttendanceReportHrDetailActivity, hrAttendanceModel.log_list)
                itemRecyclerView.adapter = hrAttendanceAdapter

                itemRecyclerView.visibility = if (hrAttendanceModel.log_list.isNotEmpty()) View.VISIBLE else View.GONE
                noItemTv.visibility = if (hrAttendanceModel.log_list.isNotEmpty()) View.GONE else View.VISIBLE
            }

            override fun onLoadFail(message: String) {
                progressBar.visibility = View.GONE
                Utils.customToastMsgError(this@AttendanceReportHrDetailActivity, message, false)
            }

        })

    }

    private fun setValue(hrAttendanceModel: HrAttendanceDetailModel){
        shiftInTv.text = if (hrAttendanceModel.shift_in != null) Utils.formatDateTime(hrAttendanceModel.shift_in, "kk:mm:ss", "hh:mm a" ) else "- - -"
        shiftOutTv.text = if (hrAttendanceModel.shift_out != null) Utils.formatDateTime(hrAttendanceModel.shift_out,"kk:mm:ss", "hh:mm a") else "- - -"
        breakOutTv.text = if (hrAttendanceModel.break_time_out != null) Utils.formatDateTime(hrAttendanceModel.break_time_out, "kk:mm:ss", "hh:mm a") else "- - -"
        breakInTv.text = if (hrAttendanceModel.break_time_in != null) Utils.formatDateTime(hrAttendanceModel.break_time_in, "kk:mm:ss", "hh:mm a") else "- - -"

        totalTimeTv.text =
            if (hrAttendanceModel.total_working_hour != null)
                if(hrAttendanceModel.button_status == "check_out" || hrAttendanceModel.button_status == "already_check_out")
                    if (hrAttendanceModel.total_working_hour!!.total_hour != null && hrAttendanceModel.total_working_hour!!.total_minute != null)
                        hrAttendanceModel.total_working_hour!!.total_hour + getString(R.string.h) +
                                hrAttendanceModel.total_working_hour!!.total_minute + getString(R.string.min)
                    else "- - -"
                else "- - -"
            else "- - -"
    }

    private fun requestServiceCheckOut(button_status: String){
        val hashMap : HashMap<String, Any?> = HashMap()
        hashMap["user_business_key"] = MockUpData.userBusinessKey(UserSessionManagement(this))
        hashMap["active_account_business_key"] =  MockUpData.activeAccountBusinessKey(UserSessionManagement(this))
        hashMap["device_type"] = "Android"
        hashMap["button_action"] = button_status
        hashMap["user_lat"]  = latitude.toString()
        hashMap["user_long"] = longitude.toString()
        progressBar.visibility = View.VISIBLE
        MyAttendanceWs().checkOutAttendance(this, hashMap, object : MyAttendanceWs.OnAttendanceCallBackListener{
            override fun onLoadListSuccess(msg: String) {
                progressBar.visibility = View.GONE
                Utils.customToastMsgError(this@AttendanceReportHrDetailActivity, msg, true)

                initListItemAttendanceDetail(attendanceType)
                setResult(RESULT_OK)
            }

            override fun onLoadFail(message: String) {
                progressBar.visibility = View.GONE
                Utils.customToastMsgError(this@AttendanceReportHrDetailActivity, message, false)
            }

        })
    }

    private fun openSomeActivityForResult(button_status: String) {
        btnStatus = button_status
        val intent = Intent(this, GPSTrackerActivity::class.java)
        startResult.launch(intent)
    }

    private val startResult = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode == RESULT_OK) {
            if (result.data != null){
                val data = result.data!!.getBooleanExtra("isSuccess", false)
                if (data){
                    requestServiceCheckOut(btnStatus)
                } else {
                    Utils.customToastMsgError(this@AttendanceReportHrDetailActivity, "You can't check in / out without no your current location", false)
                }

            }
        }
    }

}