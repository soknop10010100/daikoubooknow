package com.eazy.daikou.ui.home.my_property.my_unit

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AbsListView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.activity.result.ActivityResult
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.base.MockUpData
import com.eazy.daikou.request_data.request.my_property_ws.my_unit_ws.MyUnitPropertyWS
import com.eazy.daikou.helper.BroadcastTypes
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.CustomCategoryModel
import com.eazy.daikou.model.my_property.my_property.PropertyMyUnitModel
import com.eazy.daikou.ui.CustomMenuHeaderAdapter
import com.eazy.daikou.ui.home.my_property.adapter.PropertyMyUnitAdapter
import com.eazy.daikou.ui.profile.SwitchProfileAndPropertyActivity

class MyUnitPropertyActivity : BaseActivity() {

    private lateinit var recyclerView : RecyclerView
    private lateinit var progressBar: ProgressBar
    private lateinit var adapterMyProperty : PropertyMyUnitAdapter
    private var propertyMyUnitList : ArrayList<PropertyMyUnitModel> = ArrayList()
    private var currentItem = 0
    private var total : Int = 0
    private var scrollDown : Int = 0
    private var currentPage : Int = 1
    private var size : Int = 10
    private var isScrolling = false
    private lateinit var refreshLayout : SwipeRefreshLayout
    private lateinit var linearLayoutManager : LinearLayoutManager
    private var accountUserType = "sale"
    private lateinit var accountNameTv : TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_my_property)

        initView()

        initAction()
    }

    private fun initView() {
        recyclerView = findViewById(R.id.recyclerView)
        progressBar = findViewById(R.id.progressItem)
        refreshLayout = findViewById(R.id.swipe_layouts)
        accountNameTv = findViewById(R.id.nameTv)

        Utils.customOnToolbar(this, resources.getString(R.string.my_unit_)){finish()}

        setMenuStepAdapter(this)
    }

    private fun initAction() {
        linearLayoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = linearLayoutManager

        initRecyclerView()

        onScrollInfoRecycle()

        refreshLayout.setColorSchemeResources(R.color.colorPrimary)
        refreshLayout.setOnRefreshListener { refreshItemList() }

        // Switch Property
        val user = MockUpData.getUserItem(UserSessionManagement(this))
        Utils.setValueOnText(accountNameTv, user.accountName)

        findViewById<View>(R.id.mainLayout).setOnClickListener(CustomSetOnClickViewListener{
            val intent = Intent(this@MyUnitPropertyActivity, SwitchProfileAndPropertyActivity::class.java).apply {
                putExtra("action", "switch_property")
            }

            activityLauncher.launch(intent) { result: ActivityResult ->
                val hashMap = SwitchProfileAndPropertyActivity.checkDisplayAccount(result)

                if (hashMap.isEmpty())  return@launch

                if (hashMap.containsKey("name")){
                    accountNameTv.text = hashMap["name"].toString()

                    // Send to home fragment to set account name
                    val intentBroad = Intent(BroadcastTypes.UPDATE_IMAGE.name).apply {
                        putExtra("user_account_name", hashMap["name"].toString())
                    }
                    LocalBroadcastManager.getInstance(this).sendBroadcast(intentBroad)

                    // Refresh List
                    refreshItemList()
                }
            }
        })
    }

    private fun initRecyclerView(){
        adapterMyProperty= PropertyMyUnitAdapter(this, propertyMyUnitList, callBackOnclickListener)
        recyclerView.adapter = adapterMyProperty

         requestServiceWs()
    }

    private fun onScrollInfoRecycle() {
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true
                }
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                currentItem = linearLayoutManager.childCount
                total = linearLayoutManager.itemCount
                scrollDown = linearLayoutManager.findFirstVisibleItemPosition()
                if (dy > 0) {
                    if (isScrolling && currentItem + scrollDown == total) {
                        if (total == size) {
                            isScrolling = false
                            currentPage++
                            progressBar.visibility = View.VISIBLE
                            recyclerView.scrollToPosition(propertyMyUnitList.size - 1)
                            initRecyclerView()
                            size += 10
                        } else {
                            progressBar.visibility = View.GONE
                        }
                    }
                }
            }
        })
    }

    private fun requestServiceWs(){
        progressBar.visibility = View.VISIBLE
        MyUnitPropertyWS().getListPropertyUnitWS(this, 10, currentPage, accountUserType, callBackListener)
    }

    private val callBackListener : MyUnitPropertyWS.CallBackPropertyMyUnit = object : MyUnitPropertyWS.CallBackPropertyMyUnit{
        @SuppressLint("NotifyDataSetChanged")
        override fun onSuccessFull(listMyUnit: ArrayList<PropertyMyUnitModel>) {
            progressBar.visibility = View.GONE
            refreshLayout.isRefreshing = false
            propertyMyUnitList.addAll(listMyUnit)
            adapterMyProperty.notifyDataSetChanged()

            Utils.validateViewNoItemFound(this@MyUnitPropertyActivity, recyclerView, propertyMyUnitList.size <= 0)
        }

        override fun onFailed(error: String) {
            progressBar.visibility = View.GONE
            refreshLayout.isRefreshing = false
            Utils.customToastMsgError(this@MyUnitPropertyActivity, error, false)
        }
    }

    private val callBackOnclickListener : PropertyMyUnitAdapter.CallBackItemClickListener = object : PropertyMyUnitAdapter.CallBackItemClickListener{
        override fun clickItemCallBack(propertyMyUnit: PropertyMyUnitModel) {
            val intent = Intent(this@MyUnitPropertyActivity, PropertyMyUnitDetailActivity::class.java).apply {
                putExtra("id", propertyMyUnit.id)
            }
            activityLauncher.launch(intent) { result: ActivityResult ->
                if (result.resultCode == RESULT_OK) {
                    // From menu home
                    if (result.data != null){
                        val menu = result.data!!.getStringExtra("menu")
                        if (menu == "my_property" || menu == "home"){
                            val intent2 = Intent()
                            intent2.putExtra("menu", menu)
                            setResult(RESULT_OK, intent2)
                            finish()
                        }
                    } else {
                        // From menu home
                        setResult(RESULT_OK)
                        finish()
                    }
                }
            }
        }
    }

    private fun refreshItemList(){
        currentPage = 1
        size = 10
        isScrolling = true

        adapterMyProperty.clear()
        initRecyclerView()
    }

    // More step
    private fun setMenuStepAdapter(context: Activity){
        val list = ArrayList<CustomCategoryModel>()
        list.addAll(CustomMenuHeaderAdapter.addMenuStepList(context, "", "my_property"))

        CustomMenuHeaderAdapter.setMenuStepAdapter(context,"step_menus", list, object : CustomMenuHeaderAdapter.ClickCallBackListener{
            override fun onClickCallBack(item: CustomCategoryModel) {
                if (item.id == "my_property"){
                    finish()
                } else {    // Back Home
                    setResult(RESULT_OK)
                    finish()
                }
            }
        })
    }

}