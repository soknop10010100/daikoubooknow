package com.eazy.daikou.ui.home.hrm.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.model.hr.Relationship

class ItemRelationshipAdapter(private val listName: List<Relationship>) : RecyclerView.Adapter<ItemRelationshipAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.layout_item_relationship_adapter, parent, false)
        return ViewHolder(v)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data: Relationship = listName[position]

        if ( data.name != null){
            holder.nameTv.text = data.name
        }else{
            holder.nameTv.text = "----"
        }

        if ( data.contact != null){
            holder.contactTv.text = data.contact
        }else{
            holder.contactTv.text = "----"
        }

        if ( data.dob != null){
            holder.dOBRelationshipTv.text = data.dob
        } else {
            holder.dOBRelationshipTv.text = "----"

        }

        if (data.job_title != null){
            holder.jobTitleRelationShipTv.text = data.job_title
        } else{
            holder.jobTitleRelationShipTv.text ="----"
        }

        if (data.company != null){
            holder.CompanyRelationshipTv.text = data.company
        } else {
            holder.CompanyRelationshipTv.text = "----"
        }


        if ( data.relationship != null){
            holder.relationshipTv.text = data.relationship
        }else{
            holder.nameTv.text = "----"
        }

        if (data.address != null){
            holder.AddressRelationshipTv.text = data.address
        } else {
            holder.AddressRelationshipTv.text = "----"
        }
    }

    override fun getItemCount(): Int {
        return listName.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var nameTv : TextView = view.findViewById(R.id.nameTv)
        var contactTv : TextView = view.findViewById(R.id.contactTv)
        var dOBRelationshipTv : TextView = view.findViewById(R.id.dOBRelationshipTv)
        var jobTitleRelationShipTv : TextView = view.findViewById(R.id.jobTitleRelationShipTv)
        var CompanyRelationshipTv : TextView = view.findViewById(R.id.CompanyRelationshipTv)
        var AddressRelationshipTv : TextView = view.findViewById(R.id.AddressRelationshipTv)
        var relationshipTv : TextView = view.findViewById(R.id.relationShipTv)

    }
    interface ItemClickOnListener{
        fun onItemClick(relationship: Relationship)
    }

}