package com.eazy.daikou.ui.home.development_project

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AbsListView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.request_data.request.project_development_ws.ProjectDevelopmentWs
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.home_page.home_project.AllBranches
import com.eazy.daikou.model.home_page.home_project.BranchTypes
import com.eazy.daikou.model.home_page.home_project.Featured
import com.eazy.daikou.model.home_page.home_project.Nearby
import com.eazy.daikou.ui.home.development_project.adapter.RecommendItemPropertyAdapter

class PropertyDevelopmentByCategoryActivity : BaseActivity() {

    private lateinit var recyclerViewProperty: RecyclerView
    private lateinit var progress : View
    private lateinit var layoutRefresh : SwipeRefreshLayout

    private var listProperty = ArrayList<AllBranches>()

    private var currentItem = 0
    private var total : Int = 0
    private var scrollDown : Int = 0
    private var currentPage : Int = 1
    private var size : Int = 10
    private var isScrolling = false
    private lateinit var keyBusiness:String
    private lateinit var keyName:String
    private lateinit var linearLayoutManager : LinearLayoutManager

    private lateinit var adapter: RecommendItemPropertyAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_properties_item_category)

        initView()

        initData()

        initAction()

    }

    private fun initData(){
        if (intent.hasExtra("key")){
            keyBusiness = intent.getStringExtra("key").toString()
        }

        if (intent.hasExtra("value")){
            keyName = intent.getStringExtra("value").toString()
        }

        Utils.customOnToolbar(this, keyName) { finish() }
    }

    private fun initView(){
        recyclerViewProperty = findViewById(R.id.recyclerView)

        progress = findViewById(R.id.progressItem)

        layoutRefresh = findViewById(R.id.swipe_layouts)
    }

    private fun initAction(){
        linearLayoutManager = LinearLayoutManager(this@PropertyDevelopmentByCategoryActivity)
        recyclerViewProperty.layoutManager = linearLayoutManager

        initRecyclerView()

        onScrollItemRecycle()

        layoutRefresh.setColorSchemeResources(R.color.colorPrimary)
        layoutRefresh.setOnRefreshListener { refreshItemList() }
    }

    private fun refreshItemList(){
        currentPage = 1
        size = 10
        isScrolling = true
        adapter.clear()
        requestServiceWs()
    }

    private fun initRecyclerView(){
        adapter = RecommendItemPropertyAdapter(listProperty, clickItemListener)
        recyclerViewProperty.adapter = adapter

        requestServiceWs()

    }

    private fun onScrollItemRecycle() {
        recyclerViewProperty.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true
                }
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                currentItem = linearLayoutManager.childCount
                total = linearLayoutManager.itemCount
                scrollDown = linearLayoutManager.findFirstVisibleItemPosition()
                if (dy > 0) {
                    if (isScrolling && currentItem + scrollDown == total) {
                        if (total == size) {
                            isScrolling = false
                            currentPage++
                            progress.visibility = View.VISIBLE
                            recyclerView.scrollToPosition(listProperty.size - 1)
                            initRecyclerView()
                            size += 10
                        } else {
                            progress.visibility = View.GONE
                        }
                    }
                }
            }
        })
    }

    private val clickItemListener = object :RecommendItemPropertyAdapter.OnClickProperty{
        override fun click(property: AllBranches) {
            val intent = Intent(this@PropertyDevelopmentByCategoryActivity,PropertiesItemDetailActivity::class.java)
            intent.putExtra("id",property.id)
            startActivity(intent)
        }

    }

    //get result
    private val callBackListener = object :ProjectDevelopmentWs.CallBackGetNearBy{
        override fun onResponeListImage(listImageSlider: ArrayList<String>) { }

        override fun onResponeListCategory(listCategory: ArrayList<BranchTypes>) {}

        override fun onResponeListNearBy(listNearBy: ArrayList<Nearby>) {  }

        override fun onResponeListFeatured(listFeatured: ArrayList<Featured>) {}

        @SuppressLint("NotifyDataSetChanged")
        override fun onResponeAllListProperty(listAllProperty: ArrayList<AllBranches>) {
            showProgress(false)
            layoutRefresh.isRefreshing = false
            adapter.notifyDataSetChanged()
            listProperty.addAll(listAllProperty)

            Utils.validateViewNoItemFound(this@PropertyDevelopmentByCategoryActivity, recyclerViewProperty, listProperty.size <= 0)
        }

        override fun onLoadingFailed(msg: String?) {
            showProgress(false)
            layoutRefresh.isRefreshing = false
            Utils.customToastMsgError(this@PropertyDevelopmentByCategoryActivity, msg, false)
        }

        override fun onGettingItemFailed(msg: String?) {
            showProgress(false)
            layoutRefresh.isRefreshing = false
            Utils.customToastMsgError(this@PropertyDevelopmentByCategoryActivity, msg, false)
        }

    }


    private fun requestServiceWs(){
        showProgress(true)
        ProjectDevelopmentWs.getListByCategory(this, keyBusiness, currentPage.toString(), size.toString(), callBackListener)
    }

    private fun showProgress(show:Boolean){
        progress.visibility = if (show) View.VISIBLE else View.GONE
    }
}