package com.eazy.daikou.ui.home.service_provider_new.adapter_service_provider

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.model.service_provider.ImageSliderModel

class SlideImageAdapter(private val listImage: ArrayList<ImageSliderModel>, private val viewPager2: ViewPager2): RecyclerView.Adapter<SlideImageAdapter.SlideViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SlideViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.custom_layout_slide_image, parent, false)
        return SlideViewHolder(view)
    }

    override fun onBindViewHolder(holder: SlideViewHolder, position: Int) {
        holder.setImage(listImage[position])
        if (position == listImage.size - 2){
            viewPager2.post(runnable)
        }
    }

    override fun getItemCount(): Int { return listImage.size }

    class SlideViewHolder(view: View): RecyclerView.ViewHolder(view){
        private var imageViewPager : ImageView = view.findViewById(R.id.imageViewPager)
        fun setImage(sliderItems: ImageSliderModel) {
            Glide.with(imageViewPager).load(sliderItems.image).into(imageViewPager)
        }
    }
    @SuppressLint("NotifyDataSetChanged")
    private val runnable = Runnable {
        listImage.addAll(listImage)
        notifyDataSetChanged()
    }

}