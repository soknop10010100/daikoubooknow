package com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.adapter_v2

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.my_property.cleaning.HistoryLogs

class PropertyServiceProviderRescheduleAdapter(private val context: Context, private val serviceRescheduleList :ArrayList<HistoryLogs>): RecyclerView.Adapter<PropertyServiceProviderRescheduleAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.layout_service_provider_reshedule_view, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val serviceReschedule: HistoryLogs = serviceRescheduleList[position]
        if (serviceReschedule != null){
            if (serviceReschedule.user_profile_image != null){
                Glide.with(context).load(serviceReschedule.user_profile_image).into(holder.imageViewReschedule)
            } else{
                Glide.with(context).load(R.drawable.no_image).into(holder.imageViewReschedule)

            }
            holder.userNameTv.text = if (serviceReschedule.user_name != null) serviceReschedule.user_name else ("- - -")
            holder.startDateTv.text = if (serviceReschedule.reg_start_date != null) serviceReschedule.reg_start_date else ("- - -")
            holder.endDateTv.text = if (serviceReschedule.reg_end_date != null) serviceReschedule.reg_end_date else ("- - -")
            holder.startTimeTv.text = if (serviceReschedule.reg_start_time != null) serviceReschedule.reg_start_time else ("- - -")
            holder.endTimeTv.text = if (serviceReschedule.reg_end_time != null) serviceReschedule.reg_end_time else ("- - -")
            holder.createDateTv.text = if (serviceReschedule.created_at != null) serviceReschedule.created_at else ("- - -")

            Utils.setBgTint(holder.subLayout, if (serviceReschedule.isMine) R.color.transparent_color else R.color.color_gray_item)
            val layoutParams = FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.WRAP_CONTENT)

            if (serviceReschedule.isMine){
                layoutParams.setMargins(0, 0, 40, 0)
            } else {
                layoutParams.setMargins(40, 0, 0, 0)
            }
            holder.mainLayout.layoutParams = layoutParams
        }
    }

    override fun getItemCount(): Int {
       return serviceRescheduleList.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view){
        val mainLayout: LinearLayout = view.findViewById(R.id.mainLayout)
        val subLayout: LinearLayout = view.findViewById(R.id.subLayout)
        val imageViewReschedule: ImageView = view.findViewById(R.id.rescheduleImg)
        val userNameTv: TextView = view.findViewById(R.id.userNameTv)
        val startDateTv: TextView = view.findViewById(R.id.startDateTv)
        val endDateTv: TextView = view.findViewById(R.id.endDateTv)
        val startTimeTv: TextView = view.findViewById(R.id.startTimeTv)
        val endTimeTv: TextView = view.findViewById(R.id.endTimeTv)
        val createDateTv: TextView = view.findViewById(R.id.createDateTv)
    }

}