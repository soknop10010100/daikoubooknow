package com.eazy.daikou.ui.home.book_now.booking_hotel.visit

import android.annotation.SuppressLint
import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.Intent.ACTION_SEND
import android.os.Bundle
import android.view.View
import android.widget.AbsListView
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ProgressBar
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.base.MockUpData
import com.eazy.daikou.request_data.request.book_now_ws.BookingHotelWS
import com.eazy.daikou.request_data.request.book_now_ws.RequestHashMapData
import com.eazy.daikou.helper.AppAlertCusDialog
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.booking_hotel.HotelTravelTalkModel
import com.eazy.daikou.model.booking_hotel.TravelTalkReplyDetail
import com.eazy.daikou.ui.home.book_now.booking_hotel.travel_talk.HotelTravelTalkDetailActivity
import com.eazy.daikou.ui.home.book_now.booking_hotel.travel_talk.HotelTravelTalkViewProfileActivity
import com.eazy.daikou.ui.home.book_now.booking_hotel.adapter.HotelTravelTalkAdapter
import com.eazy.daikou.ui.home.book_now.booking_hotel.travel_talk.HotelAskQuestionTravelTalkActivity

class HotelTravelTalkActivity : BaseActivity() {

    private lateinit var progressBar: ProgressBar
    private lateinit var travelTalkAdapter: HotelTravelTalkAdapter
    private lateinit var recyclerView: RecyclerView
    private var travelTalkList : ArrayList<HotelTravelTalkModel> = ArrayList()
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var refreshLayout : SwipeRefreshLayout

    private var currentItem = 0
    private var total : Int = 0
    private var scrollDown : Int = 0
    private var currentPage : Int = 1
    private var size : Int = 10
    private var isScrolling = false
    private var userId = ""
    private var REQUEST_CODE = 0

    private var backUpHotelArticleModel: HotelTravelTalkModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hotel_travel_talk_activty)

        initView()

        initAction()

    }

    private fun initView(){
        Utils.customOnToolbar(this, resources.getString(R.string.hotel_travel_talk).uppercase()){finish()}

        progressBar = findViewById(R.id.progressItem)
        recyclerView = findViewById(R.id.recyclerView)
        refreshLayout = findViewById(R.id.swipe_layouts)
        findViewById<LinearLayout>(R.id.layoutMap).visibility = View.VISIBLE

        userId = MockUpData.getEazyHotelUserId(UserSessionManagement(this))
    }

    private fun initAction(){
        refreshLayout.setColorSchemeResources(R.color.colorPrimary)
        refreshLayout.setOnRefreshListener { refreshItemList() }

        initRecyclerView()

        onScrollInfoRecycle()

        // On Click Ask Question
        val postImg = findViewById<ImageView>(R.id.addressMap)
        postImg.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_edit_white))
        postImg.setOnClickListener(CustomSetOnClickViewListener{
            if (AppAlertCusDialog.isSuccessLoggedIn(this)) {
                val intent = Intent(this, HotelAskQuestionTravelTalkActivity::class.java)
                REQUEST_CODE = 123
                resultLauncher.launch(intent)
            }
        })
    }

    @SuppressLint("NotifyDataSetChanged")
    private val resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
        if (result.resultCode == Activity.RESULT_OK) {
            if (REQUEST_CODE == 123){
                refreshItemList()
            } else if (REQUEST_CODE == 321){
                Utils.logDebug("jeeeeeeeeeeeeeeeeee", "Shared")
            } else if (REQUEST_CODE == 111) {
                if (result.data != null){
                    if (result.data!!.hasExtra("hotel_travel_item")){
                        backUpHotelArticleModel = result.data!!.getSerializableExtra("hotel_travel_item") as HotelTravelTalkModel
                        for (i in 0 until travelTalkList.size) {
                            if(travelTalkList[i].id == backUpHotelArticleModel!!.id){
                                travelTalkList[i] = backUpHotelArticleModel as HotelTravelTalkModel
                                break
                            }
                        }
                        travelTalkAdapter.notifyDataSetChanged()
                    }
                }
            }
        } else {
            Utils.logDebug("jeeeeeeeeeeeeeeeeee", "Failed")
        }
    }

    private fun initRecyclerView(){
        linearLayoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = linearLayoutManager
        travelTalkAdapter = HotelTravelTalkAdapter(this, travelTalkList, onClickItemListener)
        recyclerView.adapter = travelTalkAdapter

        requestServiceWs()
    }

    private val onClickItemListener = object : HotelTravelTalkAdapter.OnClickItemListener{
        @SuppressLint("NotifyDataSetChanged")
        override fun onClickListener(hotelArticleModel: HotelTravelTalkModel, action: String) {
            backUpHotelArticleModel = hotelArticleModel
            when(action){
                "like_action"->{
                    if (AppAlertCusDialog.isSuccessLoggedIn(this@HotelTravelTalkActivity)) {
                        // On request service
                        val hashMap = RequestHashMapData.requestDataLike(userId, "thread", hotelArticleModel.id!!, if (hotelArticleModel.is_user_liked) "unlike" else "like")
                        RequestHashMapData.requestServiceLikeUnLike(this@HotelTravelTalkActivity,progressBar, "do_like", hashMap, object : RequestHashMapData.Companion.OnSuccessEditStatusListener{
                            override fun onSuccess(msg: String) {}
                        })

                        // On set like / unlike item
                        var numLike = hotelArticleModel.created_by!!.total_likes!!.toInt()  // Profile user
                        var numLikeItem = hotelArticleModel.total_likes!!.toInt()  // Item Like
                        for (item in travelTalkList) {
                            if (item.id == hotelArticleModel.id) {
                                if (item.is_user_liked){
                                    item.is_user_liked = false
                                    numLike -= 1
                                    numLikeItem -= 1
                                } else {
                                    item.is_user_liked = true
                                    numLike += 1
                                    numLikeItem += 1
                                }
                                item.created_by!!.total_likes = numLike.toString()  // Profile user
                                item.total_likes = numLikeItem.toString()  // Item Like
                                break
                            }
                        }
                        travelTalkAdapter.notifyDataSetChanged()
                    }
                }
                "share_action" ->{
                    shareLinkMultipleOption(hotelArticleModel.share_link!!)
                }
                "view_profile_action" ->{
                    val intent = Intent(this@HotelTravelTalkActivity, HotelTravelTalkViewProfileActivity::class.java).apply {
                        putExtra("hotel_travel_talk", hotelArticleModel)
                    }
                    startActivity(intent)
                }
                "comment_action", "detail_action"->{
                    startDetailTravelActivity(hotelArticleModel.id!!, action)
                }
            }
        }

    }

    private fun shareLinkMultipleOption(link: String) {
        val intent = Intent().apply {
            action = ACTION_SEND
            type = "text/plain"
            addCategory(Intent.CATEGORY_DEFAULT)
            putExtra(Intent.EXTRA_TITLE, "")
            putExtra(Intent.EXTRA_TEXT, link)
        }
        val chooser = Intent.createChooser(intent, "Choose a sharing option:")
        try {
            startActivity(chooser)
            val hashmap = RequestHashMapData.requestDataShare("thread", backUpHotelArticleModel!!.id!!, userId)
            RequestHashMapData.requestServiceLikeUnLike(this, progressBar,"do_share", hashmap, object : RequestHashMapData.Companion.OnSuccessEditStatusListener{
                @SuppressLint("NotifyDataSetChanged")
                override fun onSuccess(msg: String) {
                    var numShare = backUpHotelArticleModel!!.total_shares!!.toInt()  // Profile user
                    numShare++
                    backUpHotelArticleModel!!.total_shares = numShare.toString()
                    travelTalkAdapter.notifyDataSetChanged()
                }
            })
        } catch (e: ActivityNotFoundException) {
            Utils.logDebug("ljeeeeeeeeeeeeeeeeeee", "failed")
        }

//        var share = Intent(ACTION_SEND)
//        share.type = "text/plain"
//        share.putExtra(Intent.EXTRA_TITLE, "")
//        share.putExtra(Intent.EXTRA_TEXT, link)
//        val pi = PendingIntent.getBroadcast(
//            this, 321,
//            Intent(this, OnBroadCaseClass::class.java),
//            PendingIntent.FLAG_UPDATE_CURRENT
//        )
//        share = Intent.createChooser(share, "Share file", pi.intentSender)
//        startActivity(share)
    }


    private fun startDetailTravelActivity(id : String, action : String){
        val intent = Intent(this@HotelTravelTalkActivity, HotelTravelTalkDetailActivity::class.java).apply {
            putExtra("id", id)
            putExtra("action", action)
            REQUEST_CODE = 111
        }
        resultLauncher.launch(intent)
    }

    private val callBackListenerWs = object : BookingHotelWS.OnCallBackTravelTalkListener{
        @SuppressLint("NotifyDataSetChanged")
        override fun onSuccessTravelTalkList(travelTalkModelList: ArrayList<HotelTravelTalkModel>) {
            progressBar.visibility = View.GONE
            refreshLayout.isRefreshing = false
            travelTalkList.addAll(travelTalkModelList)
            travelTalkAdapter.notifyDataSetChanged()

            Utils.validateViewNoItemFound(this@HotelTravelTalkActivity, recyclerView, travelTalkList.size <= 0)
        }

        override fun onSuccessTravelTalkReplyDetail(travelTalkModelList: TravelTalkReplyDetail) {}

        override fun onCreatePostSuccess(message: String) {}

        override fun onFailed(message: String) {
            progressBar.visibility = View.GONE
            refreshLayout.isRefreshing = false
            Utils.customToastMsgError(this@HotelTravelTalkActivity, message, false)
        }

    }

    private fun refreshItemList(){
        currentPage = 1
        size = 10
        isScrolling = false

        travelTalkAdapter.clear()

        requestServiceWs()
    }

    private fun requestServiceWs(){
        progressBar.visibility = View.VISIBLE
        BookingHotelWS().getTravelTalkWs(this, currentPage, 10, userId, callBackListenerWs)
    }

    private fun onScrollInfoRecycle() {
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true
                }
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                currentItem = linearLayoutManager.childCount
                total = linearLayoutManager.itemCount
                scrollDown = linearLayoutManager.findFirstVisibleItemPosition()
                if (dy > 0) {
                    if (isScrolling && currentItem + scrollDown == total) {
                        if (total == size) {
                            isScrolling = false
                            currentPage++
                            progressBar.visibility = View.VISIBLE
                            recyclerView.scrollToPosition(travelTalkList.size - 1)

                            requestServiceWs()
                            size += 10
                        } else {
                            progressBar.visibility = View.GONE
                        }
                    }
                }
            }
        })
    }
}