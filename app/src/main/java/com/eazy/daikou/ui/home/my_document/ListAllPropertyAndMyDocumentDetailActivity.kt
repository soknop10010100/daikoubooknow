package com.eazy.daikou.ui.home.my_document

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.request_data.request.my_document.MyDocumentWs
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.helper.web.WebsiteActivity
import com.eazy.daikou.model.my_document.MyDocumentItemModel
import com.eazy.daikou.model.my_document.PropertyDocument
import com.eazy.daikou.ui.home.complaint_solution.complaint.ComplaintDetailActivity
import com.eazy.daikou.ui.home.emergency.fragmentEmergency.HistoryEmergencyDetailActivity
import com.eazy.daikou.ui.home.my_document.adapter.DocumentPropertyAdapter
import com.eazy.daikou.ui.home.my_document.adapter.MyDocumentItemAdapter


class ListAllPropertyAndMyDocumentDetailActivity : BaseActivity() {

    private lateinit var refreshLayout : SwipeRefreshLayout
    private lateinit var recyclerViewMyDocument : RecyclerView
    private lateinit var linearLayoutManagerDocument: LinearLayoutManager
    private lateinit var myDocumentItemAdapter : MyDocumentItemAdapter

    private lateinit var propertyItemAdapter : DocumentPropertyAdapter
    private lateinit var recyclerViewProperty : RecyclerView
    private lateinit var linearLayoutManagerProperty: LinearLayoutManager
    private lateinit var progressBar : ProgressBar
    private lateinit var noRecordTv : LinearLayout

    private var listAllMyDocument : ArrayList<MyDocumentItemModel> = ArrayList()
    private var propertyList : ArrayList<PropertyDocument> = ArrayList()
    private var userId = ""
    private var propertyId = "all"
    private var documentCategory = ""
    private var currentItem = 0
    private var total : Int = 0
    private var scrollDown : Int = 0
    private var currentPage : Int = 1
    private var size : Int = 10
    private var isScrolling = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_document_detail)

        initView()

        initData()

    }

    private fun initView(){
        Utils.customOnToolbar(this, resources.getString(R.string.list_my_document)) { finish() }

        refreshLayout = findViewById(R.id.swipe_layouts)
        recyclerViewMyDocument = findViewById(R.id.list_all_my_document)
        recyclerViewProperty = findViewById(R.id.list_property_document)
        progressBar = findViewById(R.id.progressItem)
        noRecordTv = findViewById(R.id.noDataLayout)
    }


    private fun initData(){
        val userSessionManagement = UserSessionManagement(this)
        userId = userSessionManagement.userId
        if(intent != null && intent.hasExtra("action_type")){
            documentCategory = intent.getStringExtra("action_type").toString()
        }

        initItemCategory()

        linearLayoutManagerDocument = LinearLayoutManager(this)
        recyclerViewMyDocument.layoutManager = linearLayoutManagerDocument

        refreshLayout.setColorSchemeResources(R.color.colorPrimary)
        refreshLayout.setOnRefreshListener { refreshList() }

        onScrollItemList()
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun refreshList(){
        currentPage = 1
        size  = 10
        isScrolling = false

        myDocumentItemAdapter.clear()
        getMyDocument()
    }

    private fun initItemCategory(){
        linearLayoutManagerProperty = GridLayoutManager(this,1, RecyclerView.HORIZONTAL,false)
        recyclerViewProperty.layoutManager = linearLayoutManagerProperty
        propertyItemAdapter = DocumentPropertyAdapter(propertyList,this, itemAdapter)
        recyclerViewProperty.adapter = propertyItemAdapter

        MyDocumentWs().getListProperty(this, userId, documentCategory, myPropertyObject)

    }

    private val itemAdapter = DocumentPropertyAdapter.ClickOnItemDocumentListener { propertyDocument ->
            propertyId = propertyDocument.propertyId
            for (property in propertyList){
                property.isClick = property.propertyId == propertyDocument.propertyId
            }
            refreshList()
        }
    private val myPropertyObject = object : MyDocumentWs.PropertyDocumentOnCallBackListener{
        @SuppressLint("NotifyDataSetChanged")
        override fun onLoadPropertySuccessFull(listProperty: ArrayList<PropertyDocument>) {
            refreshLayout.isRefreshing = false
            propertyList.addAll(listProperty)
            progressBar.visibility = View.GONE

            if(propertyList.size > 0) {
                propertyList[0].isClick = true
                noRecordTv.visibility = View.GONE
                getMyDocument()
            } else {
                noRecordTv.visibility = View.VISIBLE
            }
            propertyItemAdapter.notifyDataSetChanged()
        }

        override fun onLoadPropertyFail(message: String) {
            progressBar.visibility = View.GONE
            refreshLayout.isRefreshing = false
            Toast.makeText(this@ListAllPropertyAndMyDocumentDetailActivity,message,Toast.LENGTH_SHORT).show()
        }

    }

    private fun getMyDocument(){
        progressBar.visibility = View.VISIBLE
        myDocumentItemAdapter = MyDocumentItemAdapter(listAllMyDocument, itemDocumentAdapter)
        recyclerViewMyDocument.adapter = myDocumentItemAdapter

        MyDocumentWs().getListMyDocument(this,currentPage, documentCategory, userId, propertyId, myDocumentObjectListener)
    }
    private val itemDocumentAdapter = object : MyDocumentItemAdapter.ClickOnItemDocumentListener{
        override fun onClickItemDetail(itemDocument: MyDocumentItemModel) {
            when(itemDocument.document_category){
                "complain" ->{
                    val intent = Intent(this@ListAllPropertyAndMyDocumentDetailActivity, ComplaintDetailActivity::class.java)
                    intent.putExtra("id", itemDocument.id)
                    startActivity(intent)
                }
                "emergency" ->{
                    val intent = Intent(this@ListAllPropertyAndMyDocumentDetailActivity, HistoryEmergencyDetailActivity::class.java)
                    intent.putExtra("emergency_id", itemDocument.id)
                    startActivity(intent)
                }
                "inspection" ->{
                    Toast.makeText(this@ListAllPropertyAndMyDocumentDetailActivity, resources.getString(R.string.coming_soon), Toast.LENGTH_SHORT).show()
                }
                else -> {
                    if (itemDocument.url != null) {
                        val intent = Intent(this@ListAllPropertyAndMyDocumentDetailActivity, WebsiteActivity::class.java)
                        intent.putExtra("BuildingName", itemDocument.document_name)
                        intent.putExtra("URL_Property_List", itemDocument.url)
                        startActivity(intent)
                    } else {
                        Toast.makeText(this@ListAllPropertyAndMyDocumentDetailActivity, resources.getString(R.string.not_available_website), Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }
    }

    private val myDocumentObjectListener = object : MyDocumentWs.ListMyDocumentOnCallBackListener{
        @SuppressLint("NotifyDataSetChanged")
        override fun onLoadListMyDocumentSuccessFull(listMyDocument: ArrayList<MyDocumentItemModel>) {
            listAllMyDocument.addAll(listMyDocument)
            progressBar.visibility = View.GONE
            refreshLayout.isRefreshing = false
            noRecordTv.visibility = if(listAllMyDocument.size > 0) View.GONE else View.VISIBLE

            myDocumentItemAdapter.notifyDataSetChanged()

        }

        override fun onLoadPMyDocumentFail(message: String) {
            progressBar.visibility = View.GONE
            refreshLayout.isRefreshing = false
            Toast.makeText(this@ListAllPropertyAndMyDocumentDetailActivity,message,Toast.LENGTH_SHORT).show()
        }

    }

    private fun onScrollItemList() {
        recyclerViewMyDocument.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true
                }
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                currentItem = linearLayoutManagerDocument.childCount
                total = linearLayoutManagerDocument.itemCount
                scrollDown = linearLayoutManagerDocument.findFirstVisibleItemPosition()
                if (dy > 0) {
                    if (isScrolling && currentItem + scrollDown == total) {
                        if (total == size) {
                            isScrolling = false
                            currentPage++
                            progressBar.visibility = View.GONE
                            recyclerView.scrollToPosition(listAllMyDocument.size - 1)
                            size += 10
                            getMyDocument()
                        } else {
                            progressBar.visibility = View.GONE
                        }
                    }
                }
            }
        })
    }

}