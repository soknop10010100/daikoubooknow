package com.eazy.daikou.ui.home.book_now.booking_hotel.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.helper.DateUtil
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.booking_hotel.HotelArticleModel
import java.util.ArrayList

class HotelArticlesRelatedAdapter(private val context : Context, private val list : ArrayList<HotelArticleModel>, private val onClickListener : OnClickItemListener) : RecyclerView.Adapter<HotelArticlesRelatedAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.hotel_articles_related_model, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBindingView(context, list[position], onClickListener)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val typeArticleTv : TextView = itemView.findViewById(R.id.typeArticleTv)
        private val imgArticle : ImageView = itemView.findViewById(R.id.img_article)
        private val title : TextView = itemView.findViewById(R.id.title)
        private val descriptionTv : TextView = itemView.findViewById(R.id.descriptionTv)
        private val mainLayout : CardView = itemView.findViewById(R.id.layoutMain)
        private val dateTv : TextView = itemView.findViewById(R.id.dateTv)

        fun onBindingView(context: Context, item : HotelArticleModel, onClickListener : OnClickItemListener) {

            mainLayout.layoutParams.width = Utils.getScreenWidth(context) - Utils.dpToPx(context, 100)

            Utils.setValueOnText(typeArticleTv, item.category_name)
            Utils.setValueOnText(title, item.title)
            Utils.setTextHtml(descriptionTv, item.content)

            Glide.with(imgArticle).load(if (item.image != null) item.image else R.drawable.no_image).into(imgArticle)

            if (item.created_at != null){
                dateTv.text = DateUtil.formatDateComplaint(item.created_at)
            } else {
                dateTv.text = "- - -"
            }

            mainLayout.setOnClickListener(CustomSetOnClickViewListener{
                onClickListener.onClickItem(item.id!!)
            })
        }
    }

    interface OnClickItemListener {
        fun onClickItem(id : String)
    }
}