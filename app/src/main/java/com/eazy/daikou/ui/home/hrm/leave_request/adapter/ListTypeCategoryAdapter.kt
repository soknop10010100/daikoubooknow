package com.eazy.daikou.ui.home.hrm.leave_request.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.request_data.static_data.StaticUtilsKey
import com.eazy.daikou.model.hr.AssignEmployee
import com.eazy.daikou.model.hr.LeaveCategory

class ListTypeCategoryAdapter : RecyclerView.Adapter<ListTypeCategoryAdapter.ViewHolder> {

    private var listLeaveCategory: ArrayList<LeaveCategory> = ArrayList()
    private val clickBackListener: ItemClickOnLeaveCategory
    private var listAssignToEmployee: ArrayList<AssignEmployee>  = ArrayList()
    private var action : String

    constructor(listLeaveCategory: ArrayList<LeaveCategory>, clickBackListener:ItemClickOnLeaveCategory) : super() {
        action = StaticUtilsKey.leave_category_action   // action for leave
        this.listLeaveCategory = listLeaveCategory
        this.clickBackListener = clickBackListener
    }

    constructor(action : String, listAssignToEmployee: ArrayList<AssignEmployee>, clickBackListener: ItemClickOnLeaveCategory) : super() {
        this.listAssignToEmployee = listAssignToEmployee
        this.action = action // action for approve
        this.clickBackListener = clickBackListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.custom_image_title_layout, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if(action ==  StaticUtilsKey.leave_category_action){
            val leaveCategory : LeaveCategory = listLeaveCategory[position]
            holder.nameTv.text = leaveCategory.leave_category
            holder.checkClick.visibility = if (leaveCategory.isClick) View.VISIBLE  else View.INVISIBLE

            holder.itemView.setOnClickListener{
                clickBackListener.onClickItemLeaveCategory(leaveCategory.leave_category, leaveCategory.id, action)
            }
        } else {
            val employee : AssignEmployee = listAssignToEmployee[position]
            holder.nameTv.text = employee.name
            holder.checkClick.visibility = if (employee.isClick) View.VISIBLE  else View.INVISIBLE

            holder.itemView.setOnClickListener {
                clickBackListener.onClickItemLeaveCategory(employee.name, employee.id, action)
            }

        }

    }

    override fun getItemCount(): Int {
        return if (action == StaticUtilsKey.leave_category_action){
            listLeaveCategory.size
        } else {
            listAssignToEmployee.size
        }
    }
    interface ItemClickOnLeaveCategory {
        fun onClickItemLeaveCategory(name : String, id : String, action: String)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val checkClick: ImageView = view.findViewById(R.id.checkClick)
        val nameTv: TextView = view.findViewById(R.id.item_name)
    }
}