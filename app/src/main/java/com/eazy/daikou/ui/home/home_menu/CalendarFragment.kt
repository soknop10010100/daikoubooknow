package com.eazy.daikou.ui.home.home_menu

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseFragment
import com.eazy.daikou.helper.YearView
import com.eazy.daikou.ui.home.calendar.CalendarDetailActivity
import com.eazy.daikou.ui.home.calendar.adapter.CalendarListAdapter
import com.eazy.daikou.ui.home.calendar.adapter.CalendarListAdapter.CalendarCallback
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import java.util.*

class CalendarFragment : BaseFragment() {

    private val MIN_YEAR = 1945
    private val MAX_YEAR = 3045
    private lateinit var recyclerView: RecyclerView
    private lateinit var calendarListAdapter: CalendarListAdapter
    private var backBtn: ImageView? = null
    private  var addCalBtn: ImageView? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view =  inflater.inflate(R.layout.fragment_calendar, container, false)

        initView(view)

        recyclerView.layoutManager = LinearLayoutManager(mActivity)
        calendarListAdapter = CalendarListAdapter(buildListOfYears(), calendarCallback)
        recyclerView.adapter = calendarListAdapter

        selectFirstDisplayedYear()

        backBtn!!.visibility = View.GONE

        addCalBtn!!.setOnClickListener {
//            startActivity( new Intent(CalendarActivity.this , CalendarAddEventActivity.class));
            Toast.makeText(mActivity, resources.getString(R.string.add_event) + " " + resources.getString(R.string.coming_soon), Toast.LENGTH_SHORT).show()
        }

        return view
    }

    private fun initView(view : View) {
        backBtn = view.findViewById(R.id.iconBack)
        addCalBtn = view.findViewById(R.id.btnAddCalendar)
        recyclerView = view.findViewById(R.id.recyclerViewCal)
    }

    private val calendarCallback = CalendarCallback { timeInMillis: Long ->
        val dateTime = DateTime(timeInMillis)
        val formatter = DateTimeFormat.forPattern("yyyy-MM")
        val time = dateTime.millis
        popupInform(time)
    }

    private fun buildListOfYears(): ArrayList<Int> {
        val listYears = ArrayList<Int>()
        for (i in MIN_YEAR..MAX_YEAR) {
            listYears.add(i)
        }
        return listYears
    }

    private fun selectFirstDisplayedYear() {
        if (calendarListAdapter != null) {
            val defaultYear = DateTime().year
            val desiredYearPosition = calendarListAdapter.getYearPosition(defaultYear)
            if (desiredYearPosition > -1) recyclerView.scrollToPosition(desiredYearPosition)
        }
    }

    private fun popupInform(time: Long) {
        val intent = Intent(mActivity, CalendarDetailActivity::class.java)
        intent.putExtra("time", time)
        startActivity(intent)
    }

}