package com.eazy.daikou.ui.home.inspection_work_order

import android.annotation.SuppressLint
import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.base.BaseCameraActivity
import com.eazy.daikou.request_data.request.inspection_ws.InspectionWS
import com.eazy.daikou.request_data.static_data.StaticUtilsKey
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.inspection.ImageInspectionModel
import com.eazy.daikou.model.inspection.ItemTemplateModel
import com.eazy.daikou.ui.QRCodeAlertDialog
import com.eazy.daikou.ui.home.inspection_work_order.adapter.AddImageInspectionAdapter


class AddImageInspectionActivity : BaseActivity() {

    private lateinit var addImageAdapter: AddImageInspectionAdapter
    private lateinit var recyclerView: RecyclerView
    private lateinit var backUPImage: ImageView
    private lateinit var backUPIconDeleteImg: ImageView
    private lateinit var subInspectionModel: ItemTemplateModel.SubItemTemplateModel
    private lateinit var itemTemplateModel: ItemTemplateModel
    private lateinit var progressBar: ProgressBar
    private lateinit var btnSave: LinearLayout
    private lateinit var noteEdt: EditText
    private lateinit var titleToolbar: TextView

    private lateinit var loadedBitmap: Bitmap
    private lateinit var imageInspectionModel: ImageInspectionModel

    private val imageInspectionModelList: ArrayList<ImageInspectionModel> = ArrayList()
    private val hashMapData = HashMap<String, Any>()

    private var action: String = ""
    private val REQUEST_IMAGE = 869
    private var position: Int = 0
    private lateinit var getDocumentId: String
    private var backUpNoteTv: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_add_image_inspection)

        initView()

        initData()

        initAction()

    }

    private fun initView() {
        recyclerView = findViewById(R.id.recyclerView)
        progressBar = findViewById(R.id.progressItem)
        noteEdt = findViewById(R.id.noteEdt)
        titleToolbar = findViewById(R.id.titleToolbar)

        btnSave = findViewById(R.id.layoutMap)
        btnSave.visibility = View.VISIBLE
        val iconSave = findViewById<ImageView>(R.id.addressMap)
        iconSave.setImageDrawable(
            ResourcesCompat.getDrawable(
                resources,
                R.drawable.icons_save,
                null
            )
        )

        findViewById<ImageView>(R.id.iconBack).setOnClickListener {finish()}
    }

    @SuppressLint("SetTextI18n")
    private fun initData() {

        if (intent != null && intent.hasExtra("item_model")) {
            subInspectionModel =
                intent.getSerializableExtra("item_model") as ItemTemplateModel.SubItemTemplateModel
        }

        if (intent != null && intent.hasExtra("item_model_main")) {
            itemTemplateModel = intent.getSerializableExtra("item_model_main") as ItemTemplateModel
        }
        if (intent != null && intent.hasExtra("action")) {
            action = intent.getStringExtra("action").toString()
        }

        if (action == "note_action") {
            titleToolbar.text = resources?.getString(R.string.edit)!!
                .uppercase() + " " + resources?.getString(R.string.note)!!.uppercase()
        } else {
            titleToolbar.text = resources?.getString(R.string.add_attachment)!!.uppercase()
        }
    }

    private fun initAction() {
        recyclerView.layoutManager = GridLayoutManager(this, 2)
        addImageAdapter = AddImageInspectionAdapter(imageInspectionModelList, clickCallBack)
        recyclerView.adapter = addImageAdapter

        when (action) {
            "main_action" -> {
                requestListImageInspection(
                    StaticUtilsKey.image_key,
                    StaticUtilsKey.main_key,
                    itemTemplateModel.itemTemplateAPIModel.inspectionRoomId,
                    ""
                )
            }
            "note_action" -> {
                noteEdt.visibility = View.VISIBLE
                recyclerView.visibility = View.GONE
//                for (subItem in itemTemplateModel.itemTemplateAPIModel.inspectionSubTitle){
//                    if (subItem.itemNo.equals(subInspectionModel.item_no)){
//                        requestListImageInspection(StaticUtilsKey.note_key, StaticUtilsKey.sub_key, "", subItem.roomItemId)
//                    }
//                }
                requestListImageInspection(
                    StaticUtilsKey.note_key,
                    StaticUtilsKey.sub_key,
                    "",
                    subInspectionModel.item_room_id
                )
            }
            else -> {
//                for (subItem in itemTemplateModel.itemTemplateAPIModel.inspectionSubTitle){
//                    if (subItem.itemNo.equals(subInspectionModel.item_no)){
//                        requestListImageInspection(StaticUtilsKey.image_key, StaticUtilsKey.sub_key, "", subItem.roomItemId)
//                    }
//                }
                requestListImageInspection(
                    StaticUtilsKey.image_key,
                    StaticUtilsKey.sub_key,
                    "",
                    subInspectionModel.item_room_id
                )
            }
        }

        btnSave.setOnClickListener {
            progressBar.visibility = View.VISIBLE
            val hashMap: java.util.HashMap<String, Any>
            when (action) {
                "main_action" -> {
                    hashMap = hashMapUploadImage(
                        StaticUtilsKey.image_key,
                        StaticUtilsKey.main_key,
                        itemTemplateModel.itemTemplateAPIModel.inspectionRoomId,
                        "",
                        StaticUtilsKey.insert_key,
                        "",
                        ""
                    )
                }
                "note_action" -> {
//                    for (subItem in itemTemplateModel.itemTemplateAPIModel.inspectionSubTitle) {
//                        if (subItem.itemNo.equals(subInspectionModel.item_no)) {
//                            hashMap = hashMapUploadImage(StaticUtilsKey.note_key, StaticUtilsKey.sub_key, itemTemplateModel.itemTemplateAPIModel.inspectionRoomId,subItem.roomItemId, StaticUtilsKey.insert_key, "", "")
//                        }
//                    }
                    hashMap = hashMapUploadImage(
                        StaticUtilsKey.note_key,
                        StaticUtilsKey.sub_key,
                        itemTemplateModel.itemTemplateAPIModel.inspectionRoomId,
                        subInspectionModel.item_room_id,
                        StaticUtilsKey.insert_key,
                        "",
                        ""
                    )

                }
                else -> {
//                    for (subItem in itemTemplateModel.itemTemplateAPIModel.inspectionSubTitle) {
//                        if (subItem.itemNo.equals(subInspectionModel.item_no)) {
//                            hashMap = hashMapUploadImage(StaticUtilsKey.image_key, StaticUtilsKey.sub_key, itemTemplateModel.itemTemplateAPIModel.inspectionRoomId,subItem.roomItemId, StaticUtilsKey.insert_key, "", "")
//                        }
//                    }
                    hashMap = hashMapUploadImage(
                        StaticUtilsKey.image_key,
                        StaticUtilsKey.sub_key,
                        itemTemplateModel.itemTemplateAPIModel.inspectionRoomId,
                        subInspectionModel.item_room_id,
                        StaticUtilsKey.insert_key,
                        "",
                        ""
                    )
                }
            }

            InspectionWS().UploadDeleteImageList(
                this@AddImageInspectionActivity,
                hashMap,
                StaticUtilsKey.insert_key,
                callBackListener
            )
        }

        noteEdt.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun afterTextChanged(editable: Editable) {
                setActionOnButtonSave()
            }
        })
    }

    private var clickCallBack = object : AddImageInspectionAdapter.ClickCallBack {
        override fun onClickAddImage(imageView: ImageView, iconDelete: ImageView, pos: Int) {
            backUPImage = imageView
            backUPIconDeleteImg = iconDelete
            startActivityForResult(
                Intent(
                    this@AddImageInspectionActivity,
                    BaseCameraActivity::class.java
                ), REQUEST_IMAGE
            )
        }

        override fun onClickViewImage(image: String?) {
            if (image != null && image != "") {
                val qrCodeAlertDialog = QRCodeAlertDialog(
                    this@AddImageInspectionActivity,
                    image
                )
                qrCodeAlertDialog.show()
            }
        }

        @SuppressLint("NotifyDataSetChanged")
        override fun onClickDeleteImage(documentId: String, fileUploadId: String) {

            if (fileUploadId != "" && documentId != "") {
                AlertDialog.Builder(this@AddImageInspectionActivity)
                    .setTitle(resources.getString(R.string.confirm))
                    .setMessage(resources.getString(R.string.are_you_sure_to_delete))
                    .setPositiveButton(resources.getString(R.string.yes)) { dialog: DialogInterface, _: Int ->
                        getDocumentId = documentId
                        val hashMap: HashMap<String, Any> = hashMapUploadImage(
                            StaticUtilsKey.image_key, StaticUtilsKey.main_key, "", "", StaticUtilsKey.delete_key, fileUploadId, documentId
                        )
                        InspectionWS()
                            .UploadDeleteImageList(this@AddImageInspectionActivity, hashMap, StaticUtilsKey.delete_key, callBackListener
                        )
                        dialog.dismiss()
                    }
                    .setNegativeButton(
                        resources.getString(R.string.no)
                    ) { dialog: DialogInterface, _: Int -> dialog.dismiss() }
                    .setCancelable(false)
                    .setIcon(ResourcesCompat.getDrawable(resources, R.drawable.ic_report_problem, null))
                    .show()
            } else {
                val item: MutableIterator<MutableMap.MutableEntry<String, Any>> =
                    hashMapData.entries.iterator()
                while (item.hasNext()) {
                    val entry = item.next()
                    if (documentId.equals(entry.key, ignoreCase = true)) {
                        item.remove()
                    }
                }

                for (imageInspection in imageInspectionModelList) {
                    if (imageInspection.documentId == documentId) {
                        imageInspectionModelList.remove(imageInspection)
                        break
                    }
                }

                initRecyclerView()

                setActionOnButtonSave()
            }

        }

    }

    private fun initRecyclerView() {
        addImageAdapter = AddImageInspectionAdapter(imageInspectionModelList, clickCallBack)
        recyclerView.adapter = addImageAdapter
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_IMAGE) {
            assert(data != null)
            if (data!!.hasExtra("image_path")) {
                val imagePath = data.getStringExtra("image_path")
                loadedBitmap = BaseCameraActivity.convertByteToBitmap(imagePath)
            }
            if (data.hasExtra("select_image")) {
                val imagePath = data.getStringExtra("select_image")
                loadedBitmap = BaseCameraActivity.convertByteToBitmap(imagePath)
            }

            backUPIconDeleteImg.visibility = View.VISIBLE

            hashMapData["" + position] = loadedBitmap

            //Add Item Image
            imageInspectionModel = ImageInspectionModel()

            imageInspectionModel.documentId = position.toString()
            imageInspectionModel.filePath = Utils.convert(loadedBitmap as Bitmap?)

            imageInspectionModelList.add(imageInspectionModel)

            addImageAdapter.notifyDataSetChanged()
            position++

            setActionOnButtonSave()
        }
    }

    private fun hashMapUploadImage(
        partType: String,
        inspectionLevel: String,
        inspectionId: String,
        inspectionItemId: String,
        usageOption: String,
        fileUploadId: String,
        documentId: String
    ): HashMap<String, Any> {
        val hashMap: HashMap<String, Any> = HashMap()
        hashMap["part_type"] = partType
        hashMap["user_id"] = UserSessionManagement(this).userId
        if (partType == StaticUtilsKey.note_key) {
            hashMap["note"] = noteEdt.text.toString()
            hashMap["inspection_room_item_id"] = inspectionItemId
        } else {
            hashMap["usage_option"] = usageOption
            if (usageOption == StaticUtilsKey.insert_key) {

                hashMap["inspection_level"] = inspectionLevel

                if (inspectionLevel == StaticUtilsKey.main_key) {
                    hashMap["inspection_room_id"] = inspectionId
                } else {
                    hashMap["inspection_room_id"] = inspectionId
                    hashMap["inspection_room_item_id"] = inspectionItemId
                }

                val listImageSub: ArrayList<String> = ArrayList()
                if (hashMapData.size == 0) {
                    listImageSub.add("")
                }

                for ((_, value) in hashMapData.entries) {
                    listImageSub.add(Utils.convert(value as Bitmap?))
                }

                hashMap["images"] = listImageSub
            } else {
                hashMap["file_upload_id"] = fileUploadId
                hashMap["inspection_document_id"] = documentId
            }
        }

        return hashMap
    }

    private fun requestListImageInspection(
        partType: String,
        inspectionLevel: String,
        inspectionRoomId: String,
        inspectionRoomItemId: String
    ) {
        InspectionWS().getListImageInspection(
            this@AddImageInspectionActivity,
            partType,
            inspectionLevel,
            inspectionRoomId,
            inspectionRoomItemId,
            callBackListener
        )
    }

    @SuppressLint("NotifyDataSetChanged")
    private val callBackListener: InspectionWS.CallBackImageInspectionInfoListener =
        object : InspectionWS.CallBackImageInspectionInfoListener {
            override fun onSuccessListImageInspection(itemInspectionModels: MutableList<ImageInspectionModel>?) {
                progressBar.visibility = View.GONE
                itemInspectionModels?.let { imageInspectionModelList.addAll(it) }

                if (action != "note_action") {
                    addImageAdapter.notifyDataSetChanged()
                } else {
                    if (imageInspectionModelList[0].subInspectionNote != null) {
                        noteEdt.setText(imageInspectionModelList[0].subInspectionNote)
                        backUpNoteTv = imageInspectionModelList[0].subInspectionNote
                    }
                }

                setActionOnButtonSave()
            }

            override fun onSuccessDeleteInspectionList(
                listImage: MutableList<ImageInspectionModel>?,
                actionDe_Upload: String,
                msg: String?
            ) {
                progressBar.visibility = View.GONE
                if (actionDe_Upload == StaticUtilsKey.insert_key) {
                    finish()
                } else {
                    for (imageInspection in imageInspectionModelList) {
                        if (imageInspection.documentId == getDocumentId) {
                            imageInspectionModelList.remove(imageInspection)
                            break
                        }
                    }

                    initRecyclerView()

                }
            }

            override fun onFailed(msg: String?) {
                progressBar.visibility = View.GONE
                Utils.customToastMsgError(this@AddImageInspectionActivity, msg, false)
            }
        }

    @SuppressLint("Range")
    private fun setActionOnButtonSave() {
        if (action == "note_action") {
            if (noteEdt.text.toString().trim() == backUpNoteTv) {
                btnSave.isEnabled = false
                btnSave.isClickable = false
                btnSave.alpha = 0.2F
            } else {
                if (noteEdt.text.toString().trim() == "") {
                    btnSave.isEnabled = false
                    btnSave.isClickable = false
                    btnSave.alpha = 0.2F
                } else {
                    btnSave.isEnabled = true
                    btnSave.isClickable = true
                    btnSave.alpha = 100F
                }
            }
        } else {
            if (hashMapData.size > 0) {
                btnSave.isEnabled = true
                btnSave.isClickable = true
                btnSave.alpha = 100F
            } else {
                btnSave.isEnabled = false
                btnSave.isClickable = false
                btnSave.alpha = 0.2F
            }
        }
    }

}