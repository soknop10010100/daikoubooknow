package com.eazy.daikou.ui.profile;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.os.Looper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.eazy.daikou.request_data.request.profile_ws.ChangLanguageWs;
import com.eazy.daikou.helper.CheckIsAppActive;
import com.eazy.daikou.helper.UserSessionManagement;
import com.eazy.daikou.ui.LoginActivity;
import com.eazy.daikou.R;
import com.eazy.daikou.ui.home.book_now.booking_account.LoginBookNowActivity;
import com.eazy.daikou.ui.MainActivity;
import com.eazy.daikou.ui.home.book_now.booking_hotel.HotelBookingMainActivity;
import com.eazy.daikou.ui.profile.adapter.ChangeLangAdapter;
import com.eazy.daikou.model.home.LanguageModel;
import com.eazy.daikou.helper.Utils;

import java.util.HashMap;
import java.util.List;

public class ChangeLanguageFragment extends DialogFragment {

    private String getKey = "";
    private boolean isBookNow = false;
    private ProgressBar progressBar;

    public static ChangeLanguageFragment newInstance(boolean isBookNow, String key) {
        Bundle bundle = new Bundle();
        ChangeLanguageFragment changeLanguageAlert = new ChangeLanguageFragment();
        bundle.putString("getKey",key);
        bundle.putBoolean("book_now_app", isBookNow);
        changeLanguageAlert.setArguments(bundle);
        return changeLanguageAlert;
    }

    public static ChangeLanguageFragment newInstance(String key) {
        Bundle bundle = new Bundle();
        ChangeLanguageFragment changeLanguageAlert = new ChangeLanguageFragment();
        bundle.putString("getKey",key);
        changeLanguageAlert.setArguments(bundle);
        return changeLanguageAlert;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.AlertShape);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.change_language_alert, container, false);
        progressBar = root.findViewById(R.id.progressItem);
        progressBar.setVisibility(View.GONE);

        if (getArguments() != null && getArguments().containsKey("getKey")) {
            getKey = getArguments().getString("getKey");
        }
        if (getArguments() != null && getArguments().containsKey("book_now_app")) {
            isBookNow = getArguments().getBoolean("book_now_app");
            TextView titleToolbar = root.findViewById(R.id.titleToolbar);
            titleToolbar.setTextColor(Utils.getColor(getContext(), R.color.book_now_secondary));
            root.findViewById(R.id.mainLayout).setBackground(Utils.setDrawable(requireContext().getResources(), R.drawable.card_view_shape));
        }

        assert getKey != null;
        if (getKey.equalsIgnoreCase("language") || getKey.equalsIgnoreCase("lang_login")){
            if (getDialog() != null) {
                getDialog().setCanceledOnTouchOutside(false);
            }
            gotoScreenLanguage(root);
        } else if (getKey.equalsIgnoreCase("qr_code")){
           gotoScreenQrCode(root);
           Bitmap qrCodeImageBitmap = Utils.getQRCodeImage512(Utils.leftPadding(new UserSessionManagement(root.getContext()).getUserId()));
           ImageView imageView = root.findViewById(R.id.qr_code_user);
            TextView titleTv = root.findViewById(R.id.titleTv);
            titleTv.setText(requireContext().getString(R.string.scan_qr_code_for_parking));
           imageView.setImageBitmap(qrCodeImageBitmap);
        } else if (getKey.equalsIgnoreCase("qr_code_user")){
            gotoScreenQrCode(root);
            Bitmap qrCodeImageBitmap = Utils.getQRCodeImage512(new UserSessionManagement(root.getContext()).getUserId());
            ImageView imageView = root.findViewById(R.id.qr_code_user);
            TextView titleTv = root.findViewById(R.id.titleTv);
            titleTv.setText(requireContext().getString(R.string.scan_qr_code_me));
            imageView.setImageBitmap(qrCodeImageBitmap);
        }

        return root;
    }

    private void gotoScreenLanguage(View root){
        root.findViewById(R.id.txtCancel).setOnClickListener(view -> dismiss());
        RecyclerView recyclerViewLang = root.findViewById(R.id.recyclerViewLang);
        recyclerViewLang.setLayoutManager(new LinearLayoutManager(getContext()));
        List<LanguageModel> languageModelList = LanguageModel.getLanguage(requireContext());
        ChangeLangAdapter changeLangAdapter = new ChangeLangAdapter(getContext(), languageModelList, clickCallBackListener);
        recyclerViewLang.setAdapter(changeLangAdapter);
    }

    private void gotoScreenQrCode(View root){
        root.findViewById(R.id.changeLangLayout).setVisibility(View.GONE);
        root.findViewById(R.id.fQrCodeLayout).setVisibility(View.VISIBLE);
    }

    private final ChangeLangAdapter.ClickCallBackListener clickCallBackListener = (pos, key,context) -> {
        String lang;
        if (pos == 0) {
            lang = "en";
            Utils.changeLanguage(getActivity(), lang);
        } else if (pos == 1) {
            lang = "kh";
            Utils.changeLanguage(getActivity(), lang);
        } else {
            lang = "cn";
            Utils.changeLanguage(getActivity(), lang);
        }

        if (getKey.equalsIgnoreCase("language_progress")){
            ProgressDialog progressDialog = ProgressDialog.show(
                    context, getResources().getString(R.string.please_wait), getResources().getString(R.string.changing_language), true);
            progressDialog.setCancelable(false);
            progressDialog.getWindow().setGravity(Gravity.CENTER_VERTICAL);
            progressDialog.getWindow().setGravity(Gravity.CENTER_HORIZONTAL);
            progressDialog.show();

            new Handler(Looper.getMainLooper()).postDelayed(() -> {
                context.startActivity(new Intent(context, MainActivity.class));
                progressDialog.dismiss();
                requireActivity().finish();
            }, 1500);
        } else {
            if (getKey.equalsIgnoreCase("language")){
                progressBar.setVisibility(View.VISIBLE);
                if (isBookNow){
                    new Handler(Looper.getMainLooper()).postDelayed(() -> {
                        Intent intent = new Intent(context, HotelBookingMainActivity.class);
                        intent.putExtra("is_hotel_app", true);
                        context.startActivity(intent);
                        progressBar.setVisibility(View.GONE);
                        requireActivity().finish();
                    }, 1500);
                } else {
                    changLanguage(lang, context);
                }
            } else {
                context.startActivity(new Intent(context, CheckIsAppActive.Companion.is_daikou_active() ?  LoginActivity.class : LoginBookNowActivity.class));
                requireActivity().finish();
            }
        }

    };

    private void changLanguage(String key, Context context){
        HashMap<String,Object> hashMap = new HashMap<>();
        hashMap.put("login_user_id", new UserSessionManagement(context).getUserId());
        hashMap.put("lang_code", key);
        new ChangLanguageWs().getLanguage(hashMap, new ChangLanguageWs.OnCallBackLanguages() {
            @Override
            public void onLoadChangSuccessFully(String message) {
                Intent intent = new Intent(context,
                        CheckIsAppActive.Companion.is_daikou_active() ? MainActivity.class : HotelBookingMainActivity.class);
                intent.putExtra("is_hotel_app", true);
                context.startActivity(intent);
                progressBar.setVisibility(View.GONE);
                requireActivity().finish();
            }

            @Override
            public void onLoadChangFail(String message) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(context, message, Toast.LENGTH_LONG).show();
            }
        });
    }

}