package com.eazy.daikou.ui.home.my_property_v1.property_service_employee.service_provider_employee

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.core.widget.NestedScrollView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.base.BaseCameraActivity
import com.eazy.daikou.base.MockUpData
import com.eazy.daikou.request_data.request.service_property_ws.cleaner.ServiceScheduleEmployeeWs
import com.eazy.daikou.request_data.static_data.StaticUtilsKey
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.my_property.cleaning.ImageActivity
import com.eazy.daikou.model.my_property.service_property_employee.EmployeeDetailServiceProviderModel
import com.eazy.daikou.model.profile.User
import com.eazy.daikou.model.work_order.ImageListModel
import com.eazy.daikou.ui.home.inspection_work_order.adapter.ImageListWorkOrderAdapter
import com.eazy.daikou.ui.home.parking.ScanParkActivity
import com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.adapter_v2.ActivityLogInfoAdapter

open class ServicePropertyDetailEmployeeActivity : BaseActivity() {

    private lateinit var progressBarItem: ProgressBar
    private  var employeeDetailServiceModel: EmployeeDetailServiceProviderModel = EmployeeDetailServiceProviderModel()
    private lateinit var user: User

    private lateinit var imageEmployee: ImageView
    private lateinit var nameAssignerTv: TextView
    private lateinit var phoneAssignerTv: TextView
    private lateinit var btnCallPhone: ImageView

    private lateinit var noOrderTv: TextView
    private lateinit var unitNumberTv: TextView
    private lateinit var serviceNameTv: TextView
    private lateinit var serviceTypeTv: TextView
    private lateinit var serviceTermTv: TextView
    private lateinit var startDateTv: TextView
    private lateinit var endDateTv: TextView
    private lateinit var startTimeTv: TextView
    private lateinit var endTimeTv: TextView
    private lateinit var statusTv: TextView
    private lateinit var spaceNameTv: TextView
    private lateinit var btnCheckOut: TextView

    private lateinit var recyclerViewEmployeeLogCheckInOut: RecyclerView
    private var userType = ""
    private var userId = ""
    private var workScheduleId = ""
    private var unitQRCode = ""
    private var actionActivityCheck = ""

    //layout for add image
    private lateinit var btnAddImgBefore: ImageView
    private lateinit var imageBeforeRecyclerView: RecyclerView
    private var isBeforeImage = true

    private lateinit var txtNoImageAddAfter : TextView
    private lateinit var txtNoImageAddBefore : TextView
    private lateinit var btnAddImgAfter : ImageView
    private lateinit var imageAfterRecyclerView : RecyclerView
    private lateinit var linearSpaceName : LinearLayout
    private lateinit var layoutAddImageAfter : LinearLayout
    private lateinit var layoutAddImageBefore : LinearLayout
    private lateinit var lineaServiceProperty : LinearLayout

    private var status = ""
    private val REQUEST_IMAGE = 882
    private val REQUEST_CODE_CHECK_IN = 883
    private var requestCode = 0
    private lateinit var mainLayout : RelativeLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_employee_service_provider_detail)

        initView()

        initData()

        initAction()
    }

    private fun initView(){
        Utils.customOnToolbar(this, resources.getString(R.string.service_detail).uppercase()){finish()}

        //assigner in
        imageEmployee = findViewById(R.id.imageEmployee)
        nameAssignerTv = findViewById(R.id.nameAssignerTv)
        phoneAssignerTv = findViewById(R.id.phoneAssignerTv)
        btnCallPhone = findViewById(R.id.btnCallPhone)

        //detail info
        linearSpaceName = findViewById(R.id.linearSpaceName)
        lineaServiceProperty = findViewById(R.id.lineaServiceProperty)
        noOrderTv = findViewById(R.id.noOrderTv)
        unitNumberTv = findViewById(R.id.unitNumberTv)
        serviceNameTv = findViewById(R.id.serviceNameTv)
        serviceTypeTv = findViewById(R.id.serviceTypeTv)
        serviceTermTv = findViewById(R.id.serviceTermTv)
        startDateTv = findViewById(R.id.startDateTv)
        endDateTv = findViewById(R.id.endDateTv)
        startTimeTv = findViewById(R.id.startTimeTv)
        endTimeTv = findViewById(R.id.endTimeTv)
        spaceNameTv = findViewById(R.id.spaceNameTv)
        statusTv = findViewById(R.id.statusTv)

        //Activity Employee
        recyclerViewEmployeeLogCheckInOut = findViewById(R.id.recyclerViewEmployee)

        btnCheckOut = findViewById(R.id.btnCheckOut)
        progressBarItem = findViewById(R.id.progressItem)

        //Add Image
        btnAddImgBefore = findViewById(R.id.btnAddImg)
        btnAddImgAfter = findViewById(R.id.btnAddImgAfter)
        imageBeforeRecyclerView = findViewById(R.id.list_image)
        imageAfterRecyclerView = findViewById(R.id.list_image_after)
        mainLayout = findViewById(R.id.mainLayout)
        txtNoImageAddAfter = findViewById(R.id.txtNoImageAddAfter)
        txtNoImageAddBefore = findViewById(R.id.txtNoImageAddBefore)
        layoutAddImageAfter = findViewById(R.id.layoutAddImageAfter)
        layoutAddImageBefore = findViewById(R.id.layoutAddImageBefore)
    }

    private fun initData(){
        if (intent != null && intent.hasExtra("key_work_schedule_id")){
            workScheduleId = intent.getStringExtra("key_work_schedule_id") as String
        }
        if (intent != null && intent.hasExtra("qr_code_check")){
            unitQRCode = intent.getStringExtra("qr_code_check") as String
        }
        if (intent != null && intent.hasExtra("action_check")){
            actionActivityCheck = intent.getStringExtra("action_check") as String
        }

        user = MockUpData.getUserItem(UserSessionManagement(this))

        userId = UserSessionManagement(this).userId
        userType = Utils.validateNullValue(user.activeUserType)
    }

    private fun initAction(){
        if (actionActivityCheck == "check_in") { //action put from history to check in employee
            requestServiceApiScanCheckInAndOut(actionActivityCheck, "")
        } else {
             requestServiceApiDetailEmployee()
        }

        btnCallPhone.setOnClickListener(
            CustomSetOnClickViewListener{
                if (employeeDetailServiceModel.assigner_info!!.phone_number != null){
                    val dialIntent = Intent(Intent.ACTION_DIAL)
                    dialIntent.data = Uri.parse("tel:" + employeeDetailServiceModel.assigner_info!!.phone_number)
                    startActivity(dialIntent)
                }
            }
        )

        btnCheckOut.setOnClickListener(
            CustomSetOnClickViewListener{
                val intent = Intent(this@ServicePropertyDetailEmployeeActivity, ScanParkActivity::class.java)
                requestCode = REQUEST_CODE_CHECK_IN
                resultLauncher.launch(intent)
            }
        )

        btnAddImgBefore.setOnClickListener(
            CustomSetOnClickViewListener{
                requestCameraTakePhoto(true)
            }
        )

        btnAddImgAfter.setOnClickListener(
            CustomSetOnClickViewListener{
                requestCameraTakePhoto(false)
            }
        )
    }

    private fun requestCameraTakePhoto(isBefore : Boolean){
        isBeforeImage = isBefore
        val intent = Intent(this@ServicePropertyDetailEmployeeActivity, BaseCameraActivity::class.java)
        intent.putExtra("no_select", "yes")
        requestCode = REQUEST_IMAGE
        resultLauncher.launch(intent)
    }

    private fun initViewDetail(){
        if (employeeDetailServiceModel.assigner_info!!.user_profile_image != null){
            Glide.with(imageEmployee).load(employeeDetailServiceModel.assigner_info!!.user_profile_image).into(imageEmployee)
        } else{
            Glide.with(imageEmployee).load(R.drawable.ic_my_profile).into(imageEmployee)
        }
        Utils.setValueOnText(nameAssignerTv, employeeDetailServiceModel.assigner_info!!.user_name)
        Utils.setValueOnText(phoneAssignerTv, employeeDetailServiceModel.assigner_info!!.phone_number)

        if (employeeDetailServiceModel.detail_info!!.is_common_area){
            lineaServiceProperty.visibility = View.GONE
            linearSpaceName.visibility = View.VISIBLE

            if (employeeDetailServiceModel.detail_info!!.space_name != null){
                spaceNameTv.text = employeeDetailServiceModel.detail_info!!.space_name
            } else{
                spaceNameTv.text = "- - -"

            }
        }

        Utils.setValueOnText(noOrderTv, employeeDetailServiceModel.detail_info!!.work_schedule_no)
        Utils.setValueOnText(unitNumberTv, employeeDetailServiceModel.detail_info!!.unit_no)
        Utils.setValueOnText(serviceNameTv, employeeDetailServiceModel.detail_info!!.sv_name)
        Utils.setValueOnText(serviceTypeTv, employeeDetailServiceModel.detail_info!!.category_name)
        Utils.setValueOnText(serviceTermTv, employeeDetailServiceModel.detail_info!!.sv_term)
        Utils.setValueOnText(startDateTv, Utils.formatDateFromString("yyyy-MM-dd", "dd-MMMM-yyyy", employeeDetailServiceModel.detail_info!!.schedule_start_date))
        Utils.setValueOnText(endDateTv, Utils.formatDateFromString("yyyy-MM-dd", "dd-MMMM-yyyy", employeeDetailServiceModel.detail_info!!.schedule_end_date))
        Utils.setValueOnText(startTimeTv, Utils.formatDateFromString("kk:mm", "hh:mm a",employeeDetailServiceModel.detail_info!!.schedule_start_time ))
        Utils.setValueOnText(endTimeTv,Utils.formatDateFromString("kk:mm", "hh:mm a", employeeDetailServiceModel.detail_info!!.schedule_end_time ))
        if (employeeDetailServiceModel.detail_info!!.schedule_status != null){
            statusTv.backgroundTintList = Utils.setColorStatusService(this, employeeDetailServiceModel.detail_info!!.schedule_status)
            statusTv.text = Utils.getValueFromKeyStatusService(this)[employeeDetailServiceModel.detail_info!!.schedule_status]
            status = employeeDetailServiceModel.detail_info!!.schedule_status.toString()
        }else {
            statusTv.backgroundTintList = Utils.setColorStatusService(this, "")
            statusTv.text = "- - -"
        }

        //image before
        if (employeeDetailServiceModel.image_info!!.before_activity.size > 0) {
            setImageRecyclerView(requestListImage(employeeDetailServiceModel.image_info!!.before_activity), imageBeforeRecyclerView, btnAddImgBefore)
            txtNoImageAddBefore.visibility = View.GONE
            imageBeforeRecyclerView.visibility = View.VISIBLE
            layoutAddImageAfter.visibility = View.VISIBLE       //if image before start have add, layout image after visible ==> if not gone layout after
        } else {
            txtNoImageAddBefore.visibility = View.VISIBLE
            layoutAddImageAfter.visibility = View.GONE
            imageBeforeRecyclerView.visibility = View.GONE
        }

        //image after
        if (employeeDetailServiceModel.image_info!!.after_activity.size > 0) {
            setImageRecyclerView(requestListImage(employeeDetailServiceModel.image_info!!.after_activity), imageAfterRecyclerView, btnAddImgAfter)
            txtNoImageAddAfter.visibility = View.GONE
            imageAfterRecyclerView.visibility = View.VISIBLE
        } else {
            txtNoImageAddAfter.visibility = View.VISIBLE
            imageAfterRecyclerView.visibility = View.GONE
        }

        // activity check in / out
        initRecyclerActivityEmployee()

        if (employeeDetailServiceModel.detail_info!!.schedule_status.equals("completed" ) || employeeDetailServiceModel.detail_info!!.schedule_status.equals("incomplete" )){
            btnCheckOut.visibility = View.GONE
            btnAddImgAfter.visibility = View.GONE
            btnAddImgBefore.visibility = View.GONE

            layoutAddImageBefore.visibility = if (employeeDetailServiceModel.image_info!!.before_activity.size > 0) View.VISIBLE else View.GONE

             Utils.setNoMarginBottomOnButton(findViewById<NestedScrollView>(R.id.mainScroll), 0)
        } else {
            btnCheckOut.visibility = View.VISIBLE
        }

    }

    private fun initRecyclerActivityEmployee(){
        recyclerViewEmployeeLogCheckInOut.layoutManager = LinearLayoutManager(this)
        recyclerViewEmployeeLogCheckInOut.adapter = ActivityLogInfoAdapter(StaticUtilsKey.employee_type, employeeDetailServiceModel.activity_logs)
    }

    private fun requestServiceApiScanCheckInAndOut(actionActivity: String, remark: String){
        val hashMap = HashMap<String, Any>()
        hashMap["work_schedule_id"] = workScheduleId
        hashMap["user_id"] = userId
        hashMap["unit_qr_code"] = unitQRCode
        hashMap["action"] = actionActivity
        if (remark != "") {
            hashMap["remark"] = remark
        }

        ServiceScheduleEmployeeWs().getServiceScanUnitWS(this, hashMap, callBackScanCheckInAndOut)
    }

    private fun requestServiceApiDetailEmployee(){
        progressBarItem.visibility = View.VISIBLE
        ServiceScheduleEmployeeWs().getListDetailEmployeeServiceProviderWs(this, userId, userType, workScheduleId, callBackDetailEmployeeService)
    }

    //CallBack request Service Api
    private val callBackScanCheckInAndOut: ServiceScheduleEmployeeWs.CallBackScanUnitListener = object :ServiceScheduleEmployeeWs.CallBackScanUnitListener{
        override fun onSuccessful(action: String) {
            progressBarItem.visibility = View.GONE
            // validate check in / out
            doEmployeeCheckInOut(action)
        }
        override fun onLoadFail(error: String?) {
            progressBarItem.visibility = View.GONE
            //when check in false, go back list history
            if (actionActivityCheck == "check_in") {
                setResult(RESULT_OK)
                finish()
            }

            Utils.customToastMsgError(this@ServicePropertyDetailEmployeeActivity, error, false)
        }
    }

    private val callBackDetailEmployeeService : ServiceScheduleEmployeeWs.CallBackDetailEmployeeServiceListener = object : ServiceScheduleEmployeeWs.CallBackDetailEmployeeServiceListener{
        override fun onSuccessful(employeeDetailServiceProviderModel: EmployeeDetailServiceProviderModel?) {
            progressBarItem.visibility = View.GONE
            mainLayout.visibility = View.VISIBLE
            if (employeeDetailServiceProviderModel != null) {
                employeeDetailServiceModel = employeeDetailServiceProviderModel
            }
            initViewDetail()
        }
        override fun onLoadFail(error: String?) {
            progressBarItem.visibility = View.GONE
            Utils.customToastMsgError(this@ServicePropertyDetailEmployeeActivity, error, false)
        }
    }
    private var resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()){ result ->
        if (result.resultCode == Activity.RESULT_OK) {
            val data: Intent? = result.data
            if (requestCode == REQUEST_CODE_CHECK_IN){
                if (data != null) {
                    if (data.hasExtra("result_scan")) {
                        unitQRCode = data.getStringExtra("result_scan") as String
                    }
                    requestServiceApiScanCheckInAndOut("check_out","")
                }
            } else if (requestCode == REQUEST_IMAGE){
                if (data!!.hasExtra("image_path")) {
                    val imagePath = data.getStringExtra("image_path")
                    if (imagePath != null) {
                        getBase64Image(imagePath)
                    }
                    requestUploadRemoveImageWS("upload", getBase64Image(imagePath.toString()))
                }
            }
        }
    }

    private fun doEmployeeCheckInOut(action : String){
        when (action) {
            "check_in_success", "check_out_success" -> {
                Utils.customToastMsgError(this@ServicePropertyDetailEmployeeActivity, action , true)
                requestServiceApiDetailEmployee()
                Utils.setResultBack(this, "check_in_out_success", false)
            }
            "check_in_late" -> {
                alertWriteResent(this@ServicePropertyDetailEmployeeActivity, "check_in", "Why are you check in late ?")
            }
            "check_out_late" -> {
                alertWriteResent(this@ServicePropertyDetailEmployeeActivity, "check_out", "Why are you check out late ?")
            }
            "check_out_early" -> {
                alertWriteResent(this@ServicePropertyDetailEmployeeActivity, "check_out", "Why are you check out early ?")
            }
            "You scan wrong Unit of this work schedule."->{
                setResult(RESULT_OK)
                finish()
                Utils.customToastMsgError(this@ServicePropertyDetailEmployeeActivity, action, false)
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun alertWriteResent(activity : Activity, action: String, checkInMessage :String){
        val alertLayout = LayoutInflater.from(activity).inflate(R.layout.custom_view_layout_alert_write_description, null)
        val dialog = AlertDialog.Builder(activity, R.style.AlertShape)
        dialog.setView(alertLayout)
        dialog.setCancelable(false)
        val alertDialog = dialog.show()

        val btnCancel = alertLayout.findViewById<View>(R.id.btn_cancel) as TextView
        val btnOk = alertLayout.findViewById<View>(R.id.btn_ok) as TextView
        val titleTv = alertLayout.findViewById<TextView>(R.id.title)
        val descriptionEdt = alertLayout.findViewById<View>(R.id.description) as EditText
        titleTv.text = checkInMessage

        btnOk.setOnClickListener {
            if(!descriptionEdt.text.isNullOrEmpty()){
                requestServiceApiScanCheckInAndOut(action, descriptionEdt.text.toString().trim())
                alertDialog.dismiss()
            }else{
                Utils.customToastMsgError(this, resources.getString(R.string.please_input_your_reason), false)
            }
        }

        btnCancel.setOnClickListener {
            alertDialog.dismiss()
        }
    }

    private fun getBase64Image(imagePath: String) : String{
        val loadedBitmap = BaseCameraActivity.convertByteToBitmap(imagePath)
        return Utils.convert(loadedBitmap)
    }

    private fun addUrlImage(posKeyDefine: String, imagePath: String) : ArrayList<ImageListModel>{
        val list : ArrayList<ImageListModel> = ArrayList()
        val loadedBitmap = BaseCameraActivity.convertByteToBitmap(imagePath)
        val imageListModel = ImageListModel(posKeyDefine, loadedBitmap)
        if (status == "completed")  imageListModel.actionPart = "image_before_after_service"
        list.add(imageListModel)
        return list
    }

    private fun setImageRecyclerView(bitmapList: ArrayList<ImageListModel>, recyclerView: RecyclerView, btnAddImage : ImageView) {
        recyclerView.layoutManager = GridLayoutManager(this, 1, RecyclerView.HORIZONTAL, false)
        val pickUpAdapter = ImageListWorkOrderAdapter(this, 0, bitmapList, onClearImage)
        recyclerView.adapter = pickUpAdapter
        btnAddImage.visibility = Utils.visibleView(bitmapList)
    }

    private fun requestUploadRemoveImageWS(action : String, base64Image : String){
        progressBarItem.visibility = View.VISIBLE
        val hashMap : HashMap<String, Any> = HashMap()
        hashMap["user_id"] = userId
        hashMap["work_schedule_id"] = workScheduleId
        hashMap["action"] = action

        if (action == "upload") hashMap["base64_image"] = base64Image
        else hashMap["id"] = base64Image    // when remove I put value "id", but I use param "base64Image"

        hashMap["image_type"] = if (isBeforeImage) "before_activity" else "after_activity"
        ServiceScheduleEmployeeWs().uploadActivityPhotoWS(this, hashMap, object : ServiceScheduleEmployeeWs.CallBackActivityPhotoListener{
            override fun onSuccessful(message: String?) {
                progressBarItem.visibility = View.GONE
                Utils.customToastMsgError(this@ServicePropertyDetailEmployeeActivity, message, true)

                requestServiceApiDetailEmployee()
            }

            override fun onLoadFail(error: String?) {
                progressBarItem.visibility = View.GONE
                Utils.customToastMsgError(this@ServicePropertyDetailEmployeeActivity, error, false)
            }

        })
    }

    private val onClearImage: ImageListWorkOrderAdapter.OnClearImage =
        object : ImageListWorkOrderAdapter.OnClearImage {
            override fun onClickRemove(bitmap: ImageListModel, post: Int) {
                removeImage(bitmap)
            }

            override fun onViewImage(bitmap: ImageListModel) {
                if (bitmap.bitmapImage != null) {
                    Utils.openImageOnDialog(this@ServicePropertyDetailEmployeeActivity, Utils.convert(bitmap.bitmapImage))
                }
            }
        }

    private fun removeImage(bitmap: ImageListModel) {
        requestUploadRemoveImageWS("delete", bitmap.keyImage)
    }

    private fun requestListImage(list : ArrayList<ImageActivity>) : ArrayList<ImageListModel>{
        val bipMapList : ArrayList<ImageListModel> = ArrayList()
        for (image in list) {
            val convertUrlToBitmap = Utils.convertUrlToBase64(image.file_path)
            bipMapList.addAll(addUrlImage(image.id.toString(), convertUrlToBitmap))
        }
        return bipMapList
    }

}