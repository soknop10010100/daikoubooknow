package com.eazy.daikou.ui.home.my_property_v1.property_service_employee.work_service_employee

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.ui.home.my_property_v1.property_service_employee.service_adapter_v2.ServicePropertyHistoryAdapter
import android.view.View
import android.widget.*
import androidx.activity.result.contract.ActivityResultContracts
import com.eazy.daikou.request_data.request.service_property_ws.cleaner.ServiceScheduleEmployeeWs
import com.eazy.daikou.request_data.static_data.StaticUtilsKey
import com.eazy.daikou.helper.GetDataUtils
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.my_property.service_property_employee.ServiceDayScheduleModel
import com.eazy.daikou.model.my_property.service_property_employee.ServicePropertyHistoryEmployeeModel
import com.eazy.daikou.model.my_unit_info.ServiceProviderInfo
import com.eazy.daikou.model.profile.User
import com.eazy.daikou.ui.home.parking.ScanParkActivity
import com.eazy.daikou.ui.home.my_property_v1.property_service_employee.service_provider_employee.ServicePropertyDetailEmployeeActivity

import com.google.gson.Gson
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class ServicePropertyHistoryEmployeeActivity : BaseActivity() {

    private lateinit var dateTv : TextView
    private lateinit var recyclerView : RecyclerView
    private lateinit var refreshLayout : SwipeRefreshLayout
    private lateinit var layoutManager : LinearLayoutManager

    private lateinit var scheduleAdapter : ServicePropertyHistoryAdapter
    private var scheduleDateModel : ServiceDayScheduleModel? = null

    private lateinit var progressBar : ProgressBar
    private lateinit var layoutSchedule : LinearLayout

    private var servicePropertyHistoryEmployeeList : ArrayList<ServicePropertyHistoryEmployeeModel>  = ArrayList()

    private var currentItem = 0
    private  var total : Int = 0
    private  var scrollDown : Int = 0
    private  var currentPage : Int = 1
    private  var size : Int = 10
    private var isScrolling = false

    private var dateWork : String = ""
    private var userId = ""
    private var unitId = " "
    private var qrCode =  ""
    private var workScheduleId = " "
    private var accountId = ""
    private var userType = ""

    //Scan from smart scan
    private var action = ""
    private var serviceProviderInfo: ServiceProviderInfo? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_unit_schedule)

        initView()

        initData()

        initAction()
    }

    private fun initView(){
        dateTv = findViewById(R.id.text_date)

        recyclerView = findViewById(R.id.list_schedule)
        progressBar = findViewById(R.id.progressItem)
        refreshLayout = findViewById(R.id.swipe_layouts)
        layoutSchedule = findViewById(R.id.layoutSchedule)
    }

    private fun initData(){

        //Scan from smart scan
        action = GetDataUtils.getDataFromString("action", this)
        qrCode = GetDataUtils.getDataFromString("unit_qr_code", this)
        if (intent != null && intent.hasExtra("service_provider_info")){
            serviceProviderInfo = intent.getSerializableExtra("service_provider_info") as ServiceProviderInfo
        }

        if (action == "from_smart_scan"){
            if (serviceProviderInfo != null){
                if (serviceProviderInfo!!.work_schedule_id != null){
                    workScheduleId = serviceProviderInfo!!.work_schedule_id.toString()
                    val actionServiceInOut = GetDataUtils.getDataFromString("action_service_in_out", this)
                    if (actionServiceInOut != ""){
                        startActivityToDetail(if(actionServiceInOut == "check_in") "check_in" else "click_detail")
                    }
                }
            }
        }

        //From click on working date schedule
        if(intent.hasExtra("schedule")){
            scheduleDateModel = intent.getSerializableExtra("schedule") as ServiceDayScheduleModel
        }
        if (scheduleDateModel != null){
            layoutSchedule.visibility = View.VISIBLE
            dateWork = Utils.validateNullValue(scheduleDateModel!!.working_date)
            dateTv.text =  Utils.formatDateFromString("yyyy-mm-dd", "dd-MMMM-yyyy", dateWork)
        } else {
            dateWork = "all" // show history all
        }

        val userSessionManagement = UserSessionManagement(this)
        val mUser = Gson().fromJson(userSessionManagement.userDetail, User::class.java)

        userId = userSessionManagement.userId
        accountId = Utils.validateNullValue(mUser.accountId)
        userType = Utils.validateNullValue(mUser.activeUserType)
    }

    private fun initAction(){

        if(StaticUtilsKey.operation_part == "maintenance_work_engine"){
            findViewById<TextView>(R.id.titleToolbar).text =  getString(R.string.work_engine_history_service)
        } else {
            Utils.customOnToolbar(this,StaticUtilsKey.operation_part + " " + "History Service"){finish()}
        }

        layoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = layoutManager

        initRecyclerView()

        onScrollItemList()

        //refresh getNew data
        refreshLayout.setColorSchemeResources(R.color.colorPrimary)
        refreshLayout.setOnRefreshListener { refreshListItem() }

    }

    private fun refreshListItem(){
        currentPage = 1
        size = 10
        scheduleAdapter.clear()
        requestServiceWs()
    }

    private fun initRecyclerView(){
        scheduleAdapter = ServicePropertyHistoryAdapter(this, dateWork, servicePropertyHistoryEmployeeList, onClickItemListener)
        recyclerView.adapter = scheduleAdapter

        requestServiceWs()
    }

    private fun requestServiceWs(){
        progressBar.visibility = View.VISIBLE
        ServiceScheduleEmployeeWs().getHistoryWorkSchedule(this, requestHashmap(), unitScheduleListener)
    }

    private fun requestHashmap() : HashMap<String, Any>{
        val hashMap : HashMap<String, Any> = HashMap()
        hashMap["page"] = currentPage
        hashMap["limit"] = 10
        hashMap["user_id"] = userId
        hashMap["operation_part"] = StaticUtilsKey.operation_part
        hashMap["active_account_id"] = accountId
        hashMap["active_user_type"] = userType
        hashMap["working_date"] = dateWork
        return hashMap
    }

    private val unitScheduleListener = object : ServiceScheduleEmployeeWs.UnitScheduleCleanerCallBackListener {
        @SuppressLint("NotifyDataSetChanged")
        override fun onLoadSuccess(listSchedule: ArrayList<ServicePropertyHistoryEmployeeModel>) {
            progressBar.visibility = View.GONE
            refreshLayout.isRefreshing = false
            servicePropertyHistoryEmployeeList.addAll(listSchedule)
            scheduleAdapter.notifyDataSetChanged()
        }

        override fun onLoadFail(message: String) {
            progressBar.visibility = View.GONE
            refreshLayout.isRefreshing = false
            Utils.customToastMsgError(this@ServicePropertyHistoryEmployeeActivity, message, false)
        }

    }

    private fun onScrollItemList() {
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true
                }
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                currentItem = layoutManager.childCount
                total = layoutManager.itemCount
                scrollDown = layoutManager.findFirstVisibleItemPosition()
                if (dy > 0) {
                    if (isScrolling && currentItem + scrollDown == total) {
                        if (total == size) {
                            isScrolling = false
                            currentPage++
                            progressBar.visibility = View.VISIBLE
                            recyclerView.scrollToPosition(servicePropertyHistoryEmployeeList.size - 1)
                            size += 10
                            initRecyclerView()
                        } else {
                            progressBar.visibility = View.GONE
                        }
                    }
                }
            }
        })
    }

    private val onClickItemListener = object : ServicePropertyHistoryAdapter.ItemClickOnService{
        override fun onClickUnitSchedule(cleanerSchedule: ServicePropertyHistoryEmployeeModel) {
            if (cleanerSchedule.work_schedule_id != null){
                workScheduleId = cleanerSchedule.work_schedule_id as String
            }
            if (unitId.isEmpty()){
                Utils.customToastMsgError(this@ServicePropertyHistoryEmployeeActivity, getString(R.string.do_not_have_unit), false)
            }
            if(cleanerSchedule.schedule_status.equals("new")){
                val intent = Intent(this@ServicePropertyHistoryEmployeeActivity, ScanParkActivity::class.java)
                resultLauncher.launch(intent)
            } else {
                startActivityToDetail("click_detail")
            }
        }
    }
    private var resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()){ result ->
        if (result.resultCode == Activity.RESULT_OK) {
            val data: Intent? = result.data
            if (data != null) {
                var action = ""
                if (data.hasExtra("action")) {
                     action = data.getStringExtra("action") as String
                }

                if (action == "check_in_out_success"){ // call back when check in / out success
                    refreshListItem()
                    Utils.setResultBack(this, "", false)

                    // Method call back when check in / out success to smart scan
                    callBackToScanFromSmartScan()
                } else {  // from scan qr code
                    if (data.hasExtra("result_scan")) {
                        qrCode = data.getStringExtra("result_scan") as String
                    }
                    startActivityToDetail("check_in")
                }
            }
        }
    }

    private fun callBackToScanFromSmartScan(){
        if (action == "from_smart_scan"){
            Utils.setResultBack(this, "check_in_out_success", false)
        }
    }

    private fun startActivityToDetail(actionCheck: String){
        val intent = Intent(this@ServicePropertyHistoryEmployeeActivity, ServicePropertyDetailEmployeeActivity::class.java)
        intent.putExtra("key_work_schedule_id", workScheduleId)
        intent.putExtra("qr_code_check", qrCode)
        intent.putExtra("action_check", actionCheck)
        resultLauncher.launch(intent)
    }
}