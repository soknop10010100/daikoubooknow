package com.eazy.daikou.ui.home.inspection_work_order.adapter

import android.content.Context
import android.content.res.ColorStateList
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.request_data.static_data.StaticUtilsKey
import com.eazy.daikou.helper.DateUtil
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.inspection.InfoInspectionModel
import com.eazy.daikou.model.work_order.WorkOrderItemModel
import java.lang.NumberFormatException

class ItemInspectionAdapter : RecyclerView.Adapter<ItemInspectionAdapter.ViewHolder> {

    private var inspectionList: ArrayList<InfoInspectionModel> = ArrayList()
    private val context: Context
    private val clickBackListener: ClickBackListener
    private var workOrderItemModelList: ArrayList<WorkOrderItemModel>  = ArrayList()
    private var action : String = ""

    constructor( inspectionList: ArrayList<InfoInspectionModel>, context: Context, clickBackListener: ClickBackListener) : super() {
                action = StaticUtilsKey.inspection_action
                this.inspectionList = inspectionList
                this.context = context
                this.clickBackListener = clickBackListener
    }

    constructor( action : String, workOrderItemModelList: ArrayList<WorkOrderItemModel>, context: Context, clickBackListener: ClickBackListener) : super() {
        this.workOrderItemModelList = workOrderItemModelList
        this.action = action
        this.context = context
        this.clickBackListener = clickBackListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(context).inflate(R.layout.item_inspection_model, parent, false)
        return ViewHolder(view)
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (action == StaticUtilsKey.work_order_action){
            val workOrderItemModel = workOrderItemModelList[position]
            if (workOrderItemModel != null){

                holder.inspectionLayout.visibility = View.GONE
                holder.workOrderLayout.visibility = View.VISIBLE

                holder.noWorkOrderTv.text = if (workOrderItemModel.workOrderNo != null) workOrderItemModel.workOrderNo else ". . ."
                holder.reportTypeNoTv.text = if (workOrderItemModel.reportTypeNo != null) workOrderItemModel.reportTypeNo else ". . ."
                holder.issueDateTv.text = if (workOrderItemModel.issueDate != null)  DateUtil.formatDateComplaint(workOrderItemModel.issueDate) else ". . ."

                holder.subjectTv.text = if (workOrderItemModel.subject != null) workOrderItemModel.subject else "..."
                if (workOrderItemModel.authorUserInfo != null) {
                    holder.authorWorkOrderTv.text = if (workOrderItemModel.authorUserInfo.fullName != null)  workOrderItemModel.authorUserInfo.fullName else ". . ."
                }
                if (workOrderItemModel.assigneeUserInfo != null) {
                    holder.assigneeTv.text = if (workOrderItemModel.assigneeUserInfo.fullName != null)  workOrderItemModel.assigneeUserInfo.fullName else ". . ."
                }


                // Status : Set Text and Background
                holder.statusWorkOrder.text = if (workOrderItemModel.status != null) Utils.getValueFromKeyStatus(context)[workOrderItemModel.status] else ". . ."
                holder.statusWorkOrder.backgroundTintList = if (workOrderItemModel.status != null)  Utils.setColorStatusOnWorkOrder(context, workOrderItemModel.status) else null

                // Priority : Set Text and Background
                if (workOrderItemModel.priority != null) {

                    holder.priorityTv.text = if (workOrderItemModel.priority != null)
                        Utils.getPriorityOnWorkOrder(context)[workOrderItemModel.priority] else ". . ."

                    holder.priorityTv.backgroundTintList = setColorPriorityOnWorkOrder(context, workOrderItemModel.priority)

                }

                // Done Percent : Set Text and Background
                holder.doneCompleteTv.text = if (workOrderItemModel.donePercent != null) workOrderItemModel.donePercent else ". . ."
                holder.doneCompleteTv.backgroundTintList = if (workOrderItemModel.donePercent != null)
                    Utils.setDoneStatusPercentOnWorkOrder(context, workOrderItemModel.donePercent) else null

                holder.itemView.setOnClickListener{
                    clickBackListener.clickOnItemWorkOrder(workOrderItemModel)
                }

            }
        } else {
            val inspectionModel = inspectionList[position]
            if (inspectionModel != null){
                holder.inspectionType.text = if (inspectionModel.inspectionName != null) inspectionModel.inspectionName else ". . ."

                holder.date.text = if (inspectionModel.createdDt != null)  DateUtil.formatDateComplaint(inspectionModel.createdDt) else ". . ."
                holder.time.text = if (inspectionModel.conductTime != null) inspectionModel.conductTime else ". . ."
                holder.noTv.text = if (inspectionModel.orderNo != null) inspectionModel.orderNo else "..."
                holder.duringTv.text = if (inspectionModel.conductDuration != null)  inspectionModel.conductDuration + " " + Utils.getText(context, R.string.minutes)  else ". . ."
                if (inspectionModel.authorUserInfo != null) {
                    holder.authorTv.text = if (inspectionModel.authorUserInfo.fullName != null) inspectionModel.authorUserInfo.fullName else ". . ."
                }

                holder.status.text = if (inspectionModel.status != null) Utils.getValueFromKeyInspectionStatus(context)[inspectionModel.status] else ". . ."
                if(inspectionModel.status != null) {
                    when {
                        inspectionModel.status.equals("active", ignoreCase = true) -> {
                            holder.status.backgroundTintList =
                                ColorStateList.valueOf(ContextCompat.getColor(context, R.color.blue))
                        }
                        inspectionModel.status.equals("closed", ignoreCase = true) -> {
                            holder.status.backgroundTintList =
                                ColorStateList.valueOf(ContextCompat.getColor(context, R.color.green))
                        }
                        inspectionModel.status.equals("pending", ignoreCase = true) -> {
                            holder.status.backgroundTintList =
                                ColorStateList.valueOf(ContextCompat.getColor(context, R.color.yellow))
                        }
                        inspectionModel.status.equals("completed", ignoreCase = true) -> {
                            holder.status.backgroundTintList =
                                ColorStateList.valueOf(ContextCompat.getColor(context, R.color.green))
                        }
                        inspectionModel.status.equals("assigned", ignoreCase = true) -> {
                            holder.status.backgroundTintList =
                                ColorStateList.valueOf(ContextCompat.getColor(context, R.color.red))
                        }
                        else -> {
                            holder.status.backgroundTintList =
                                ColorStateList.valueOf(ContextCompat.getColor(context, R.color.appBarColor))
                        }
                    }
                }

                holder.itemView.setOnClickListener{
                    clickBackListener.clickOnItemAcceptOpenMap(inspectionModel)
                }

            }
        }
    }

    override fun getItemCount(): Int {
        return if (action == StaticUtilsKey.work_order_action){
            workOrderItemModelList.size
        } else {
            inspectionList.size
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        // =========== Inspection Part =====================
        val inspectionLayout : LinearLayout = itemView.findViewById(R.id.inspectionListLayout)
        val date: TextView = itemView.findViewById(R.id.date)
        val time: TextView = itemView.findViewById(R.id.time)
        val status: TextView = itemView.findViewById(R.id.status)
        val inspectionType: TextView = itemView.findViewById(R.id.inspectionTypeTv)
        val noTv : TextView = itemView.findViewById(R.id.noTv)
        val duringTv : TextView = itemView.findViewById(R.id.duringTv)
        val authorTv : TextView = itemView.findViewById(R.id.authorTv)

        // =========== Work Order Part =====================
        val workOrderLayout : LinearLayout = itemView.findViewById(R.id.workOrderListLayout)
        val issueDateTv: TextView = itemView.findViewById(R.id.issueDateTv)
        val reportTypeNoTv: TextView = itemView.findViewById(R.id.reportWorkOrder)
        val statusWorkOrder: TextView = itemView.findViewById(R.id.statusWorkOrderTv)
        val priorityTv: TextView = itemView.findViewById(R.id.priorityWorkOrderTv)
        val subjectTv: TextView = itemView.findViewById(R.id.subjectWorkOrderTv)
        val assigneeTv: TextView = itemView.findViewById(R.id.assigneeTv)
        val noWorkOrderTv : TextView = itemView.findViewById(R.id.noWorkOrderTv)
        val doneCompleteTv : TextView = itemView.findViewById(R.id.doneTv)
        val authorWorkOrderTv : TextView = itemView.findViewById(R.id.authorWorkOrderTv)

    }

    fun clear() {
        if (action == StaticUtilsKey.work_order_action){
            val size = workOrderItemModelList.size
            if (size > 0) {
                for (i in 0 until size) {
                    workOrderItemModelList.removeAt(0)
                }
                notifyItemRangeRemoved(0, size)
            }
        } else{
            val size = inspectionList.size
            if (size > 0) {
                for (i in 0 until size) {
                    inspectionList.removeAt(0)
                }
                notifyItemRangeRemoved(0, size)
            }
        }

    }

    interface ClickBackListener {
        fun clickOnItemAcceptOpenMap(infoInspectionModel: InfoInspectionModel)
        fun clickOnItemWorkOrder(workOrderItemModel: WorkOrderItemModel)
    }

    private fun setColorPriorityOnWorkOrder(context: Context, value: Int): ColorStateList {
        try {
            return when (value) {
                1 -> {
                    ColorStateList.valueOf(ContextCompat.getColor(context, R.color.yellow))
                }
                2 -> {
                    ColorStateList.valueOf(ContextCompat.getColor(context, R.color.green))
                }
                else -> {
                    ColorStateList.valueOf(ContextCompat.getColor(context, R.color.red))
                }
            }
        } catch (exception: NumberFormatException) {
            exception.printStackTrace()
        }
        return ColorStateList.valueOf(ContextCompat.getColor(context, R.color.yellow))
    }

}