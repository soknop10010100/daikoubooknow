package com.eazy.daikou.ui.home.facility_booking

import android.annotation.SuppressLint
import android.content.Intent
import android.content.res.ColorStateList
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.request_data.request.facility_booking_ws.FacilityBookingWs
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.facility_booking.CardItemByProperty
import com.eazy.daikou.model.facility_booking.ItemCardBookingModel
import com.eazy.daikou.model.facility_booking.my_booking.CouponBookingModel
import com.eazy.daikou.ui.home.facility_booking.adapter.CategoryItemCardBookingAdapter
import com.eazy.daikou.ui.home.facility_booking.adapter.ItemCardBookingAdapter
import com.eazy.daikou.helper.web.WebPayActivity
import libs.mjn.prettydialog.PrettyDialog

class MyCardAddToCardBookingActivity : BaseActivity() {

    private lateinit var itemRecyclerView : RecyclerView
    private lateinit var totalItem : TextView
    private lateinit var totalPrice : TextView
    private lateinit var progressBar : ProgressBar

    private lateinit var itemCardBookingAdapter: CategoryItemCardBookingAdapter
    private lateinit var linearLayoutManager : LinearLayoutManager
    private lateinit var noResultLayout : LinearLayout
    private lateinit var linearCardPay : CardView
    private lateinit var backUpItemBooking : CardItemByProperty
    private lateinit var backUpCategoryBooking : ItemCardBookingModel
    private lateinit var deleteAllIconCard : LinearLayout
    private lateinit var continueToBooking : CardView
    private lateinit var menuItemTotal : LinearLayout
    private lateinit var prettyDialog : PrettyDialog

    private var itemCardBookingList : ArrayList<ItemCardBookingModel> = ArrayList()

    private var isSelectAllDelete = true
    private var getCountItem : Int = 0
    private var backUpPropertyId = ""
    private var backUpPropertyMainId = ""
    private var subTotalAmountNoTax = ""
    private var totalAmountWithTax = ""
    private var action = ""
    private val REQUEST_CODE_PAYMENT = 659
    private var numberCount = 0
    private lateinit var wainingLayout : LinearLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_add_to_card_booking)

        initView()

        initData()

        initAction()
    }

    private fun initView(){
        val iconDeleteAll : ImageView = findViewById(R.id.addressMap)
        itemRecyclerView = findViewById(R.id.recyclerItem)
        progressBar = findViewById(R.id.progressItem)
        totalItem = findViewById(R.id.totalItemTv)
        totalPrice = findViewById(R.id.totalTv)
        noResultLayout = findViewById(R.id.txtNoResult)
        linearCardPay = findViewById(R.id.linear_card)

        deleteAllIconCard = findViewById(R.id.layoutMap)
        deleteAllIconCard.visibility = View.VISIBLE
        continueToBooking = findViewById(R.id.continueBookingLayout)
        menuItemTotal = findViewById(R.id.menuItemTotal)
        wainingLayout = findViewById(R.id.wainingLayout)

        iconDeleteAll.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_delete_white_24))
        Utils.customOnToolbar(this, resources.getString(R.string.my_cart)){finish()}
    }

    private fun initData(){
        if (intent != null && intent.hasExtra("count_item")){
            getCountItem = intent.getIntExtra("count_item", 0)
            totalItem.text = getCountItem.toString()
        }
        if (intent != null && intent.hasExtra("action")){
            action = intent.getStringExtra("action").toString()
        }

    }

    private fun initAction(){
        linearLayoutManager = LinearLayoutManager(this)
        itemRecyclerView.layoutManager = linearLayoutManager
        prettyDialog = PrettyDialog(this)

        initRecyclerView()

        setNoItemSelect()

        linearCardPay.isEnabled = false
        linearCardPay.setOnClickListener{
            if (isEnableButtonPay()){
                val hashMap : HashMap<String, Any> = HashMap()
                hashMap["user_id"] = UserSessionManagement(this).userId
                hashMap["total_amount_without_tax"] = subTotalAmountNoTax
                hashMap["total_amount_with_tax"] = totalAmountWithTax
                val cardItemIdList: ArrayList<String> = ArrayList()
                var propertyId = ""
                for (itemCardBooking in itemCardBookingList){
                    for (subItemCard in itemCardBooking.cart_items){
                        if (subItemCard.isClick){
                            subItemCard.cart_item_id?.let { it1 -> cardItemIdList.add(it1) }
                            propertyId = subItemCard.property_id.toString()
                        }
                    }
                }
                hashMap["property_id"] = propertyId
                hashMap["cart_item_id"] = cardItemIdList

                progressBar.visibility = View.VISIBLE
                linearCardPay.isEnabled = false
                FacilityBookingWs().checkOutPayBooking(this, hashMap, callBackBookingListener)
            }
        }

        deleteAllIconCard.setOnClickListener{
            customPrettyDialog(resources.getString(R.string.are_you_sure_to_clear_all_record), true)
        }

        continueToBooking.setOnClickListener{
            setResult(RESULT_OK)
            finish()
        }

        wainingLayout.setOnClickListener{
            val intent = Intent(this@MyCardAddToCardBookingActivity, MyBookingActivity::class.java)
            startActivity(intent)
        }

    }

    private fun refreshList(){
        itemCardBookingAdapter.clear()
        requestMyCardBookingList()
        setNoItemSelect()
    }

    private fun initRecyclerView(){
        itemCardBookingAdapter = CategoryItemCardBookingAdapter(this, itemCardBookingList, onClickCallBack)
        itemRecyclerView.adapter = itemCardBookingAdapter

        requestMyCardBookingList()
    }

    private fun requestMyCardBookingList(){
        FacilityBookingWs().getMyCardBookingList(this, UserSessionManagement(this).userId, callBackCardItemListener)
    }


    private var callBackCardItemListener = object : FacilityBookingWs.CardBookingCallBackListener{
        @SuppressLint("NotifyDataSetChanged", "Range")
        override fun onLoadSuccessFull(itemCardBookingModelList: MutableList<ItemCardBookingModel>) {
            progressBar.visibility = View.GONE
            itemCardBookingList.addAll(itemCardBookingModelList)

            setViewOnResult()

            itemCardBookingAdapter.notifyDataSetChanged()
            if(itemCardBookingModelList.size > 0) {
                deleteAllIconCard.alpha = 100F
                deleteAllIconCard.isClickable = true
            } else {
                deleteAllIconCard.alpha = 0.2F
                deleteAllIconCard.isClickable = false
            }

            wainingLayout.visibility = View.GONE
        }

        override fun onLoadFailed(error: String?) {
            progressBar.visibility = View.GONE
            Utils.customToastMsgError(this@MyCardAddToCardBookingActivity, error, false)
        }

    }


    private val onClickCallBack = object : CategoryItemCardBookingAdapter.ClickCallBackListener{
        @SuppressLint("NotifyDataSetChanged")
        override fun onClickCallBack(itemMainCategory: ItemCardBookingModel, itemBooking: CardItemByProperty, action: String, subItemCardBookingAdapter: ItemCardBookingAdapter, iconAction : ImageView) {
            backUpItemBooking = itemBooking
            backUpCategoryBooking = itemMainCategory
            if (itemBooking.quantity != null){
                numberCount = itemBooking.quantity!!.toInt()
            }
            when (action) {
                "add" -> {
                    if (numberCount >= 1) {
                        iconAction.isEnabled = false
                        numberCount = numberCount.plus(1)
                        val priceItem = (itemBooking.price_per_one_quantity)!!.toDouble() * numberCount
                        updateCardItem(numberCount, priceItem, itemBooking, action, iconAction)
                    }
                }
                "delete_item" -> {
                    customPrettyDialog(resources.getString(R.string.are_you_sure_to_delete), false)
                }
                "select_in_active" ->{
                    if(isSelectInActiveBooking()){
                       if (backUpPropertyId == itemBooking.property_id.toString()){
                           itemBooking.isClick = !itemBooking.isClick

                           if (!isCheckHaveSelected()){
                               for (itemCard in itemCardBookingList){
                                   if (itemBooking.property_id == backUpPropertyId) {
                                       itemCard.isClick = false
                                   }
                               }
                           }
                           subItemCardBookingAdapter.notifyDataSetChanged()
                           itemCardBookingAdapter.notifyDataSetChanged()

                           checkValidateButtonPay()
                       } else {
                           Utils.customAlertDialogConfirm(this@MyCardAddToCardBookingActivity, resources.getString(R.string.different_property_do_you_want_to_remove_last_property)){
                               for (itemCard in itemCardBookingList){
                                   if (itemCard.property_id.equals(backUpPropertyId)) {
                                       for (subItemCard in itemCard.cart_items) {
                                           subItemCard.isClick = false
                                           itemCard.isClick = false
                                       }
                                   }
                               }
                               itemBooking.isClick = true
                               itemMainCategory.isClick =true
                               subItemCardBookingAdapter.notifyDataSetChanged()
                               itemCardBookingAdapter.notifyDataSetChanged()

                               checkValidateButtonPay()
                           }
                       }
                    } else {
                        itemBooking.isClick = !itemBooking.isClick
                        itemMainCategory.isClick = !isCheckHaveSelected()

                        checkValidateButtonPay()
                    }
                    itemCardBookingAdapter.notifyDataSetChanged()

                }
                else -> {     // Minus People
                    if (numberCount > 1) {
                        iconAction.isEnabled = false
                        numberCount = numberCount.minus(1)
                        val priceItem = (itemBooking.price_per_one_quantity)!!.toDouble() * numberCount
                        updateCardItem(numberCount, priceItem, itemBooking, action, iconAction)
                    }
                }
            }
        }

        @SuppressLint("NotifyDataSetChanged")
        override fun onClickMainCallBack(itemBooking: ItemCardBookingModel) {
            if(isSelectInActiveBookingMain()){
                if (backUpPropertyMainId == itemBooking.property_id.toString()){
                    itemBooking.isClick = !itemBooking.isClick

                    for (itemCard in itemCardBookingList){
                        if (itemCard.property_id.equals(backUpPropertyMainId)) {
                            for (subItemCard in itemCard.cart_items) {
                                subItemCard.isClick = itemBooking.isClick
                            }
                        }
                    }
                    checkValidateButtonPay()
                    itemCardBookingAdapter.notifyDataSetChanged()
                } else {
                    Utils.customAlertDialogConfirm(this@MyCardAddToCardBookingActivity, resources.getString(R.string.different_property_do_you_want_to_remove_last_property)){
                        for (itemCard in itemCardBookingList){
                            if (!itemCard.property_id.equals(backUpPropertyMainId)) {
                                for (subItemCard in itemCard.cart_items) {
                                    subItemCard.isClick = true
                                }
                                itemCard.isClick = true
                            } else {
                                itemCard.isClick = false
                                for (subItemCard in itemCard.cart_items) {
                                    subItemCard.isClick = false
                                }
                            }
                        }

                        checkValidateButtonPay()
                        itemCardBookingAdapter.notifyDataSetChanged()
                    }
                }
            } else {
                itemBooking.isClick = !itemBooking.isClick
                for (itemCard in itemCardBookingList){
                    if (itemCard.property_id.equals(itemBooking.property_id)) {
                        for (subItemCard in itemCard.cart_items) {
                            subItemCard.isClick = itemBooking.isClick
                        }
                    }
                }
                checkValidateButtonPay()
                itemCardBookingAdapter.notifyDataSetChanged()
            }
            itemCardBookingAdapter.notifyDataSetChanged()
        }
    }

    private fun isCheckHaveSelected() : Boolean{
        for (itemCard in itemCardBookingList){
            if (itemCard.property_id.equals(backUpPropertyId)) {
                for (subItemCard in itemCard.cart_items) {
                    if (subItemCard.isClick){
                        return true
                    }
                }
            }
        }
        return false
    }

    private fun isSelectInActiveBookingMain() : Boolean{
        for (itemCard in itemCardBookingList){
            if (itemCard.isClick){
                backUpPropertyMainId = itemCard.property_id.toString()
                return true
            }
        }
        backUpPropertyMainId = ""
        return false
    }

    private fun isSelectInActiveBooking() : Boolean{
        for (itemCard in itemCardBookingList){
            for (subItemCard in itemCard.cart_items){
                if (subItemCard.isClick){
                    backUpPropertyId = itemCard.property_id.toString()
                    return true
                }
            }
        }
        backUpPropertyId = ""
        return false
    }

    private fun checkValidateButtonPay(){
        if (isEnableButtonPay()){
            linearCardPay.isEnabled = true
            linearCardPay.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(this@MyCardAddToCardBookingActivity, R.color.appBarColorOld))
            setValueOnPrice()
        } else {
            linearCardPay.isEnabled = false
            linearCardPay.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(this@MyCardAddToCardBookingActivity, R.color.gray))
            setNoItemSelect()
        }
    }

    private fun isEnableButtonPay() : Boolean{
        for (itemCardBooking in itemCardBookingList){
            for (subItemCard in itemCardBooking.cart_items){
                if (subItemCard.isClick){
                    return true
                }
            }
        }
        return false
    }

    @SuppressLint("SetTextI18n")
    private fun setNoItemSelect(){
        totalItem.text = "0 "
        totalPrice.text = "$ 0.0"
    }

    @SuppressLint("SetTextI18n")
    private fun setValueOnPrice(){
        var subTotalList = 0.00
        var totalTaxList = 0.00
        var itemSelect = 0
        for (categoryItem in itemCardBookingList) {
            for (item in categoryItem.cart_items) {
                if (item.isClick) {
                    var subTotal: Double
                    if (item.total_amount != null) {
                        subTotal = item.total_amount!!.toDouble()
                        subTotalList += subTotal
                    }

                    var taxTotal: Double
                    if (item.taxTotal != null) {
                        taxTotal = item.taxTotal!!.toDouble()
                        totalTaxList += taxTotal
                    }
                    itemSelect++
                }
            }
        }
        val total = totalTaxList + subTotalList
        totalAmountWithTax = "${Utils.formatDecimalFormatValue(total)}"
        subTotalAmountNoTax = "${Utils.formatDecimalFormatValue(subTotalList)}"

        totalItem.text = "$itemSelect "
        totalPrice.text = "$ ${Utils.formatDecimalFormatValue(total)}"
    }

    private var callBackBookingListener = object : FacilityBookingWs.BookingCallBackListener{
        @SuppressLint("NotifyDataSetChanged")
        override fun onLoadSuccessFull(successMsg: String?) {
            progressBar.visibility = View.GONE
            Utils.customToastMsgError(this@MyCardAddToCardBookingActivity, successMsg, true)

            if (isSelectAllDelete){
                refreshList()
            } else {
                if (backUpItemBooking != null) {
                    // Remove Item On Sub Property
                    for (item in itemCardBookingList){
                        item.cart_items.remove(backUpItemBooking)
                    }

                    // Set Select/ Un Select Item On Main Property
                    for (subItem in backUpCategoryBooking.cart_items){
                        if (subItem.isClick){
                            backUpCategoryBooking.isClick = true
                            break
                        } else {
                            backUpCategoryBooking.isClick = false
                        }
                    }

                    // Remove Item On Main Property
                    for (itemCardBooking in itemCardBookingList){
                        if (itemCardBooking.cart_items.isEmpty()){
                            itemCardBookingList.remove(itemCardBooking)
                        }
                    }

                    setViewOnResult()
                }
                itemCardBookingAdapter.notifyDataSetChanged()
            }
            prettyDialog.dismiss()

            checkValidateButtonPay()

            if (action == "my_card_from_add_to_card") {
                val intent = Intent()
                // Just Test Total All Item
                var itemCount = 0
                for (itemBooking in itemCardBookingList){
                    for (item in itemBooking.cart_items){
                        itemCount ++
                    }
                }
                intent.putExtra("count_item", itemCount)
                setResult(RESULT_OK, intent)
            }

            linearCardPay.isEnabled = true
        }

        override fun onLoadCheckOutBookingSuccessFull(successMsg: String?) {
            progressBar.visibility = View.GONE
            if (successMsg != null && successMsg != "") {
                val intent = Intent(this@MyCardAddToCardBookingActivity, WebPayActivity::class.java)
                intent.putExtra("toolBarTitle", "Kess Pay")
                intent.putExtra("linkUrl", successMsg)
                startActivityForResult(intent, REQUEST_CODE_PAYMENT)
            } else {
                Toast.makeText(this@MyCardAddToCardBookingActivity, "Link Payment is empty.", Toast.LENGTH_SHORT).show()
            }
            linearCardPay.isEnabled = true
        }

        override fun onLoadCouponSuccessFull(couponBookingModel: CouponBookingModel?) {
            progressBar.visibility = View.GONE
            linearCardPay.isEnabled = true
        }

        override fun onSuccessUpdateItemCard(numberCount : Int, priceItem : Double,itemBooking: CardItemByProperty, action : String, iconAction : ImageView) {
            progressBar.visibility = View.GONE
            itemBooking.total_amount = priceItem.toString()
            itemBooking.quantity = numberCount.toString()
            itemCardBookingAdapter.notifyDataSetChanged()
            checkValidateButtonPay()

            iconAction.isEnabled = true
        }

        override fun onLoadFailed(error: String?) {
            linearCardPay.isEnabled = true
            progressBar.visibility = View.GONE
            Utils.customToastMsgError(this@MyCardAddToCardBookingActivity, error, false)
        }
    }

    @SuppressLint("NotifyDataSetChanged", "Range")
    private fun customPrettyDialog(message: String, isSelect: Boolean) {
        prettyDialog = PrettyDialog(this)
        prettyDialog
            .setTitle(resources.getString(R.string.delete))
            .setMessage(message)
            .setIcon(R.drawable.ic_close, R.color.red)
            { prettyDialog.dismiss() }
            .addButton(
                resources.getString(R.string.no),
                R.color.pdlg_color_white,
                R.color.greenSea
            )
            {
               prettyDialog.dismiss()
            }

            .addButton(
                resources.getString(R.string.delete),
                R.color.white,
                R.color.red
            )
            {
                isSelectAllDelete = isSelect
                val hashMap: HashMap<String, Any> = HashMap()
                val listId: ArrayList<String> = ArrayList()
                if (isSelect){
                    for (item in itemCardBookingList) {
                        for (subIte in item.cart_items) {
                            subIte.cart_item_id?.let { it1 -> listId.add(it1) }
                        }
                    }
                } else {
                    backUpItemBooking.cart_item_id?.let { listId.add(it) }
                }
                hashMap["cart_item_ids"] = listId
                progressBar.visibility = View.VISIBLE
                FacilityBookingWs().addToCardBooking( this@MyCardAddToCardBookingActivity, hashMap, "delete_card_booking", callBackBookingListener)
            }

            .setAnimationEnabled(true)
            .show()
        prettyDialog.setCancelable(false)
    }

    private fun updateCardItem(numberCount : Int, priceItem : Double, itemBooking: CardItemByProperty, action : String, iconAction : ImageView){
        progressBar.visibility = View.VISIBLE
        val hashMap: HashMap<String, Any> = HashMap()
        hashMap["cart_item_id"] = itemBooking.cart_item_id.toString()
        hashMap["quantity"] = numberCount.toString()
        hashMap["total_tax_amount"] = itemBooking.taxTotal.toString()
        hashMap["amount"] = priceItem.toString()
        FacilityBookingWs().updateItemCard( this@MyCardAddToCardBookingActivity, hashMap,  numberCount, priceItem, itemBooking, action, iconAction, callBackBookingListener)
    }

    private fun setViewOnResult(){
        noResultLayout.visibility = if(itemCardBookingList.isEmpty()) View.VISIBLE else View.GONE
        menuItemTotal.visibility = if(itemCardBookingList.isEmpty()) View.GONE else View.VISIBLE
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == RESULT_OK && requestCode == REQUEST_CODE_PAYMENT){
            if (data != null && data.hasExtra("status")) {
                refreshList()
                if (data.getStringExtra("status") == "success=1") {
                    Utils.popupMessage(getString(R.string.payment), getString(R.string.payment_successful), this@MyCardAddToCardBookingActivity)
                    wainingLayout.visibility = View.GONE
                } else {
                    Utils.popupMessage(getString(R.string.payment), getString(R.string.something_went_wrong), this@MyCardAddToCardBookingActivity)
                    wainingLayout.visibility = View.VISIBLE
                }
            } else {
                wainingLayout.visibility = View.VISIBLE
                refreshList()
            }
        }
    }
}