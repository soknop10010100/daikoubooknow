package com.eazy.daikou.ui.home.inspection_work_order.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.eazy.daikou.R;
import com.eazy.daikou.request_data.static_data.StaticUtilsKey;
import com.eazy.daikou.model.inspection.ItemInspectionModel;

import org.jetbrains.annotations.NotNull;
import java.util.List;

public class SelectInspectionAdapter extends RecyclerView.Adapter<SelectInspectionAdapter.ViewHolder> {

    private final OnClickUtil onClickUtil;
    private final List<ItemInspectionModel> utilNoModelList;
    private final String action;

    public SelectInspectionAdapter(List<ItemInspectionModel> utilNoModels, String action, OnClickUtil onClickUtil) {
        this.onClickUtil = onClickUtil;
        this.utilNoModelList = utilNoModels;
        this.action = action;
    }

    @NotNull
    @Override
    public SelectInspectionAdapter.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view  = LayoutInflater.from(parent.getContext()).inflate(R.layout.select_inspection_model,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull ViewHolder holder, int position) {
        ItemInspectionModel utilNoModel = utilNoModelList.get(position);
        if (action.equals(StaticUtilsKey.inspection_type)) {
            holder.txtUtil.setText(utilNoModel.getInspection_name() != null ? utilNoModel.getInspection_name() : "No Inspection Type");
        } else {
            holder.txtUtil.setText(utilNoModel.getTemplateName() != null ? utilNoModel.getTemplateName() : "No Template Type");
        }

        if (utilNoModel.isClick()){
            holder.imgCheck.setVisibility(View.VISIBLE);
        } else {
            holder.imgCheck.setVisibility(View.GONE);
        }

        holder.allView.setOnClickListener(v -> onClickUtil.onClickUtil(utilNoModel));
    }

    @Override
    public int getItemCount() {
        return utilNoModelList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        private final View allView;
        private final RelativeLayout imgCheck;
        private final TextView txtUtil;

        public ViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            allView = itemView;
            imgCheck = itemView.findViewById(R.id.checkImage);
            txtUtil = itemView.findViewById(R.id.txtUtil);
        }
    }

    public interface OnClickUtil{
        void onClickUtil(ItemInspectionModel utilNoModel);
    }
}
