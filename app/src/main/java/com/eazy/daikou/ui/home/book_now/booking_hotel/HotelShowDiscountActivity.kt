package com.eazy.daikou.ui.home.book_now.booking_hotel

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import android.widget.ProgressBar
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.viewpager2.widget.ViewPager2
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.request_data.request.book_now_ws.RequestHashMapData
import com.eazy.daikou.helper.CustomSliderImageClass
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.home.SubItemHomeModel
import com.eazy.daikou.model.work_order.ImageListModel
import com.eazy.daikou.ui.home.book_now.booking_hotel.adapter.AdapterImageSlider
import com.eazy.daikou.ui.home.book_now.booking_hotel.adapter.BookingHomeItemAdapter
import com.eazy.daikou.ui.home.book_now.booking_hotel.adapter.HotelCategoryItemAdapter
import com.google.android.material.appbar.AppBarLayout
import kotlin.math.abs

class HotelShowDiscountActivity : BaseActivity(), HotelDiscountItemFragment.CallBackListener {

    private lateinit var progressBar: ProgressBar

    private lateinit var recyclerViewCategory: RecyclerView

    private var listCategory = ArrayList<SubItemHomeModel>()
    private var category = "hotel"

    // image slider
    private lateinit var viewPager2: ViewPager2
    private val sliderHandler: Handler = Handler(Looper.getMainLooper())

    private lateinit var swipeRefreshLayout : SwipeRefreshLayout
    private lateinit var appBar : AppBarLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hotel_show_discount)

        initView()

        initAction()

    }

    private fun initView() {
        Utils.customOnToolbar(this, resources.getString(R.string.discount).uppercase()) { finish() }
        progressBar = findViewById(R.id.progressItem)
        progressBar.visibility = View.GONE

        recyclerViewCategory = findViewById(R.id.recycler_view_category_hotel)

        viewPager2 = findViewById(R.id.viewPagerImageSlider)
        swipeRefreshLayout = findViewById(R.id.swipe_layout)
        appBar = findViewById(R.id.appBarLayout)

    }

    private fun initAction() {
        recyclerViewCategory.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)

        initRecyclerviewCategory()

        reLoadFragment(category)

        initRefreshLayout()
    }

    private fun initRecyclerviewCategory() {
        listCategory = RequestHashMapData.addCategoryItemList(resources)
        var bookingHomeItemAdapter: HotelCategoryItemAdapter? = null
        bookingHomeItemAdapter = HotelCategoryItemAdapter(
            false,
            listCategory,
            object : BookingHomeItemAdapter.PropertyClick {
                @SuppressLint("NotifyDataSetChanged")
                override fun onBookingClick(propertyModel: SubItemHomeModel?) {
                    for (itemSub in listCategory) {
                        if (propertyModel != null) {
                            itemSub.isClickCategory = itemSub.id == propertyModel.id
                        }
                    }
                    category = propertyModel!!.id.toString()
                    bookingHomeItemAdapter!!.notifyDataSetChanged()

                    reLoadFragment(category)
                }

                override fun onBookingWishlistClick(propertyModel: SubItemHomeModel?) { }

            }
        )
        recyclerViewCategory.adapter = bookingHomeItemAdapter
    }

    private fun initRefreshLayout(){
        val start = resources.getDimensionPixelSize(R.dimen.indicator_start)
        val end = resources.getDimensionPixelSize(R.dimen.indicator_end)
        swipeRefreshLayout.setColorSchemeResources(R.color.appBarColorOld)
        swipeRefreshLayout.setProgressViewOffset(true, start, end)
        swipeRefreshLayout.setOnRefreshListener {
            swipeRefreshLayout.isRefreshing = false
            reLoadFragment(category)
        }

        //set swipe for only expanded of app bar
        appBar.addOnOffsetChangedListener(AppBarLayout.BaseOnOffsetChangedListener<AppBarLayout> { appBarLayout, verticalOffset ->
            if (abs(verticalOffset) == appBarLayout.totalScrollRange) {
                // Collapsed
                swipeRefreshLayout.isEnabled = false
            } else swipeRefreshLayout.isEnabled = verticalOffset == 0
        })
    }


    private fun initSlideBanner(slideModels : ArrayList<String>){
        if (slideModels.size == 0){
            slideModels.add("https://i.pinimg.com/474x/15/0c/d7/150cd7dec5567c7c759dc0f899d5ac5a.jpg")
            slideModels.add("https://free-psd-templates.com/wp-content/uploads/2018/07/free_psd_preview_last_15-free-hotel-banners-collection-in-psd.jpg")
            slideModels.add("https://indiater.com/wp-content/uploads/2019/03/sea-view-hotel-miami-beach-holiday-package-banner.jpg")
        }

        //Slider Image
        CustomSliderImageClass.initImageSlideViewPager(20, viewPager2, sliderHandler, sliderRunnable, ArrayList(), slideModels, object : AdapterImageSlider.OnClickItemListener{
            override fun onClickCallBack(sliderItems: ImageListModel) {}
        })
    }

    private val sliderRunnable = Runnable { viewPager2.currentItem = viewPager2.currentItem + 1 }

    override fun onPause() {
        super.onPause()
        if (sliderHandler != null)  sliderHandler.removeCallbacks(sliderRunnable)
    }

    override fun onResume() {
        super.onResume()
        if (sliderHandler != null) sliderHandler.postDelayed(sliderRunnable, 3000)
    }

    @SuppressLint("DetachAndAttachSameFragment")
    private fun reLoadFragment(categoryType: String) {
        val ft: FragmentTransaction = supportFragmentManager.beginTransaction()
        ft.replace(R.id.nav_fragment, HotelDiscountItemFragment.newInstance(categoryType))
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        ft.addToBackStack(null)
        ft.commit()
    }

    override fun onBackPressed() {
        finish()
    }

    override fun onCallBack(sliderBanner: ArrayList<String>) {
        initSlideBanner(sliderBanner)
    }

}