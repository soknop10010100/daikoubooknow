package com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.cleaning_v2

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.base.BaseCameraActivity
import com.eazy.daikou.helper.DateUtil
import com.eazy.daikou.helper.GetDataUtils
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.my_property.cleaning.AllScheduleEmployee
import com.eazy.daikou.model.work_order.ImageListModel
import com.eazy.daikou.ui.home.facility_booking.adapter.AvailableDayAdapter
import com.eazy.daikou.ui.home.inspection_work_order.adapter.ImageListWorkOrderAdapter
import com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.adapter_v2.ActivityLogInfoAdapter
import com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.adapter_v2.AllScheduleEmployeeAdapter

class ServicePropertyAssignEmployeeActivity : BaseActivity() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var recyclerViewImage: RecyclerView
    private var allScheduleEmployees : ArrayList<AllScheduleEmployee> = ArrayList()
    private var availableDayList : ArrayList<String> = ArrayList()
    private var allActivityLogModel : AllScheduleEmployee? = null
    private val bitmapList: ArrayList<ImageListModel> = ArrayList()
    private var action : String = ""
    private var userNameEmployee = ""
    private var post = 0
    private lateinit var headImageLayout : LinearLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_service_property_assign_employee)

        initView()

        initData()

        initAction()
    }

    private fun initView(){
        recyclerView = findViewById(R.id.recyclerView)
        recyclerViewImage = findViewById(R.id.recyclerViewImage)
        headImageLayout = findViewById(R.id.headImageLayout)
    }

    private fun initData(){
        if (intent != null && intent.hasExtra("all_schedule_employees")){
            allScheduleEmployees = intent.getSerializableExtra("all_schedule_employees") as ArrayList<AllScheduleEmployee>
        }

        if (intent != null && intent.hasExtra("available_day")){
            availableDayList = intent.getSerializableExtra("available_day") as ArrayList<String>
        }

        if (intent != null && intent.hasExtra("activity_logs")){
            allActivityLogModel = intent.getSerializableExtra("activity_logs") as AllScheduleEmployee
            userNameEmployee = GetDataUtils.getDataFromString("user_full_name", this)

            for (image in allActivityLogModel!!.image_info!!.before_activity) {
                val convertUrlToBitmap = Utils.convertUrlToBase64(image.file_path)
                addUrlImage(post.toString() + "", convertUrlToBitmap)
            }
            for (image in allActivityLogModel!!.image_info!!.after_activity) {
                val convertUrlToBitmap = Utils.convertUrlToBase64(image.file_path)
                addUrlImage(post.toString() + "", convertUrlToBitmap)
            }
        }

        action = GetDataUtils.getDataFromString("action", this)
    }

    private fun addUrlImage(posKeyDefine: String, imagePath: String) {
        val loadedBitmap = BaseCameraActivity.convertByteToBitmap(imagePath)
        post += 1
        val imageListModel = ImageListModel()
        imageListModel.keyImage = posKeyDefine
        imageListModel.bitmapImage = loadedBitmap
        imageListModel.actionPart = "image_before_after_service"
        bitmapList.add(imageListModel)
    }

    private fun initAction(){
        when (action) {
            "available_day" -> {
                Utils.customOnToolbar(this, resources.getString(R.string.working_day_s)){finish()}
                initAvailableDayRecycle()
            }
            "activity_logs" -> {
                Utils.customOnToolbar(this, userNameEmployee){finish()}
                initAllLogActivity()
                headImageLayout.visibility = View.VISIBLE
            }
            else -> {
                Utils.customOnToolbar(this, "Employee"){finish()}
                initAllScheduleEmployeeRecycle()
            }
        }
    }

    private fun initAvailableDayRecycle(){
        recyclerView.layoutManager = GridLayoutManager(this, 4, RecyclerView.VERTICAL, false)
        val availableDayAdapter = AvailableDayAdapter("available_day_service_property", availableDayList, object : AvailableDayAdapter.OnClickCallBackListener{
            override fun onClickBack(data: String) {
                val dateFormat = DateUtil.formatSimpleDateToDayDate( data, "dd EEE MM/yyyy", "yyyy-MM-dd")
                val allScheduleEmployees : ArrayList<AllScheduleEmployee> = ArrayList()
                for (item in allScheduleEmployees){
                    if (item.working_date != null){
                        if (dateFormat == item.working_date){
                            allScheduleEmployees.add(item)
                        }
                    }
                }
                val intent = Intent(this@ServicePropertyAssignEmployeeActivity, ServicePropertyAssignEmployeeActivity::class.java)
                intent.putExtra("all_schedule_employees", allScheduleEmployees)
                intent.putExtra("action", "all_schedule_employees")
                startActivity(intent)
            }
        })
        recyclerView.adapter = availableDayAdapter
    }

    private fun initAllScheduleEmployeeRecycle(){
        recyclerView.layoutManager = LinearLayoutManager(this)
        val allScheduleEmployeeAdapter = AllScheduleEmployeeAdapter(this, allScheduleEmployees, object : AllScheduleEmployeeAdapter.CallBackItemServiceProviderProperty{
            override fun callBackItem(allScheduleEmployee: AllScheduleEmployee) {
                val intent = Intent(this@ServicePropertyAssignEmployeeActivity, ServicePropertyAssignEmployeeActivity::class.java)
                intent.putExtra("activity_logs", allScheduleEmployee)
                intent.putExtra("user_full_name", allScheduleEmployee.user_full_name)
                intent.putExtra("action", "activity_logs")
                startActivity(intent)
            }
        })
        recyclerView.adapter = allScheduleEmployeeAdapter
    }

    private fun initAllLogActivity(){
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = allActivityLogModel?.let { ActivityLogInfoAdapter("", it.activity_logs) }

        //Image
        recyclerViewImage.layoutManager = GridLayoutManager(this, 1, RecyclerView.HORIZONTAL, false)
        recyclerViewImage.adapter = ImageListWorkOrderAdapter(this, post, bitmapList, object : ImageListWorkOrderAdapter.OnClearImage{
            override fun onClickRemove(bitmap: ImageListModel?, post: Int) {}

            override fun onViewImage(bitmap: ImageListModel?) {
                if (bitmap != null) {
                    Utils.openZoomImage(this@ServicePropertyAssignEmployeeActivity, bitmap.valImage, ArrayList<String>())
                }
            }

        })
    }

}