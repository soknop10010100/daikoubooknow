package com.eazy.daikou.ui.home.book_now.booking_hotel.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.daimajia.swipe.SwipeLayout
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.helper.DateUtil
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.booking_hotel.HotelMyVendorModel

class MyHotelOfVendorAdapter(private val context : Context, private val action : String, private val bookingList: ArrayList<HotelMyVendorModel>, private val propertyClick: PropertyClick) :
    RecyclerView.Adapter<MyHotelOfVendorAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.hotel_my_vendor_layout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val hotelMyVendorModel = bookingList[position]
        if (hotelMyVendorModel != null){
            Utils.setValueOnText(holder.hotelNameTv, hotelMyVendorModel.hotel_name)
            Utils.setValueOnText(holder.locationTv, hotelMyVendorModel.location_name)
            Utils.setValueOnText(holder.priceTv, hotelMyVendorModel.price_display)
            Glide.with(holder.imageView).load(if(hotelMyVendorModel.image != null) hotelMyVendorModel.image else R.drawable.no_image).into(holder.imageView)
            if (hotelMyVendorModel.updated_at != null){
                holder.lastUpdateTv.text = DateUtil.formatDateComplaint(hotelMyVendorModel.updated_at)
            } else {
                holder.lastUpdateTv.text = ". . ."
            }

            holder.allHotelVendorLayout.setOnClickListener(
                CustomSetOnClickViewListener{
                    propertyClick.onBookingClick(hotelMyVendorModel)
                }
            )
            if(action == "wishlist_hotel"){
                Glide.with(holder.iconWishlist).load(R.drawable.ic_my_favourite).into(holder.iconWishlist)
            } else {
                Glide.with(holder.iconWishlist).load(if (hotelMyVendorModel.is_wishlist)    R.drawable.ic_my_favourite else R.drawable.ic_favorite_border).into(holder.iconWishlist)
            }
            if (hotelMyVendorModel.status != null) {
                if (hotelMyVendorModel.status == "publish") {
                    holder.statusTv.text = context.resources.getString(R.string.publish)
                    Utils.setBgTint(holder.statusTv, R.color.appBarColorOld)
                } else {
                    holder.statusTv.text = context.resources.getString(R.string.draft)
                    Utils.setBgTint(holder.statusTv, R.color.yellow)
                }
            } else {
                holder.statusTv.text = context.getString(R.string.no_)
                Utils.setBgTint(holder.statusTv, R.color.yellow)
            }

            if (action == "wishlist_hotel"){
                holder.swipelayoutHotelManage.close()
            }
            //Do action click
            holder.iconWishlist.setOnClickListener {
                propertyClick.onFavouriteClick("wishlist", hotelMyVendorModel)
            }
            holder.buttonPublish.setOnClickListener {
                if (hotelMyVendorModel.status != null && hotelMyVendorModel.status != "publish") {
                    propertyClick.onFavouriteClick("publish", hotelMyVendorModel)
                } else {
                    holder.swipelayoutHotelManage.close()
                }
            }
            holder.buttonDraft.setOnClickListener {
                if (hotelMyVendorModel.status != null && hotelMyVendorModel.status != "draft") {
                    propertyClick.onFavouriteClick("draft", hotelMyVendorModel)
                } else {
                    holder.swipelayoutHotelManage.close()
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return bookingList.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val priceTv: TextView = itemView.findViewById(R.id.priceTv)
        val lastUpdateTv: TextView = itemView.findViewById(R.id.lastUpdateTv)
        val statusTv: TextView = itemView.findViewById(R.id.statusTv)
        val hotelNameTv: TextView = itemView.findViewById(R.id.hotelNameTv)
        val locationTv: TextView = itemView.findViewById(R.id.locationTv)
        val imageView : ImageView = itemView.findViewById(R.id.imageView)
        val allHotelVendorLayout : LinearLayout = itemView.findViewById(R.id.allHotelVendorLayout)
        val iconWishlist : ImageView = itemView.findViewById(R.id.iconWishlist)
        val buttonPublish: LinearLayout = itemView.findViewById(R.id.buttonPublishHotel)
        val buttonDraft: LinearLayout = itemView.findViewById(R.id.buttonDraftHotel)
        val swipelayoutHotelManage : SwipeLayout = itemView.findViewById(R.id.swipe_layoutHotelManage)
    }

    interface PropertyClick {
        fun onBookingClick(hotelMyVendorModel : HotelMyVendorModel)
        fun onFavouriteClick(action: String, hotelMyVendorModel : HotelMyVendorModel)
    }

    fun clear() {
        val size: Int = bookingList.size
        if (size > 0) {
            for (i in 0 until size) {
                bookingList.removeAt(0)
            }
            notifyItemRangeRemoved(0, size)
        }
    }
}