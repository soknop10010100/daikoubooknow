package com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.model.my_property.service_property_employee.AllService


class ServiceCleaningAdapter(
    private val serviceList: List<AllService>,
    private val itemClickService: ItemClickOnServiceName
) : RecyclerView.Adapter<ServiceCleaningAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.layout_service_cleaning, parent, false)
        return ViewHolder(v)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val service: AllService = serviceList[position]

        holder.nameTv.text = service.cleaningServiceName
        holder.price.text = service.serviceFee
        holder.itemView.setOnClickListener {
            itemClickService.onClick(service,position)
        }
        if(service.cleaningServiceImage != null){
            for (image in service.cleaningServiceImage){
                Glide.with(holder.productImg).load(image).into(holder.productImg)
            }
        }else{
            Glide.with(holder.productImg).load(R.drawable.no_image).into(holder.productImg)
        }

    }

    override fun getItemCount(): Int {
        return serviceList.size
    }

    interface ItemClickOnServiceName {
        fun onClick(service: AllService, stat : Int)

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var productImg : ImageView = view.findViewById(R.id.productImg)
        var nameTv :TextView = view.findViewById(R.id.txt_name_cleaning)
        var price : TextView = view.findViewById(R.id.txt_price)

    }

}