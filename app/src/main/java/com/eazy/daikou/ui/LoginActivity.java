package com.eazy.daikou.ui;

import static com.eazy.daikou.repository_ws.Constant.LANG_CN;
import static com.eazy.daikou.repository_ws.Constant.LANG_EN;
import static com.eazy.daikou.repository_ws.Constant.LANG_KH;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;

import com.eazy.daikou.BuildConfig;
import com.eazy.daikou.R;
import com.eazy.daikou.repository_ws.Constant;
import com.eazy.daikou.repository_ws.CustomResponseOnClickListener;
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener;
import com.eazy.daikou.base.BaseActivity;
import com.eazy.daikou.request_data.request.profile_ws.ChangLanguageWs;
import com.eazy.daikou.request_data.request.profile_ws.LoginWs;
import com.eazy.daikou.request_data.static_data.StaticUtilsKey;
import com.eazy.daikou.fcm.MyFirebaseMessagingService;
import com.eazy.daikou.helper.AppAlertCusDialog;
import com.eazy.daikou.helper.InternetConnection;
import com.eazy.daikou.helper.UserSessionManagement;
import com.eazy.daikou.helper.Utils;
import com.eazy.daikou.model.profile.User;
import com.eazy.daikou.ui.profile.ChangeLanguageFragment;
import com.eazy.daikou.ui.profile.ForgotPasswordActivity;
import com.eazy.daikou.ui.sign_up.SignUpDaikouActivity;
import com.google.firebase.FirebaseApp;
import com.google.firebase.messaging.FirebaseMessaging;
import com.rilixtech.widget.countrycodepicker.CountryCodePicker;

import java.util.HashMap;
import java.util.Locale;

public class LoginActivity extends BaseActivity {

    private ProgressBar progressBar;
    private EditText edUsernamePh, edPassword;
    private String email_phone, password;
    private UserSessionManagement userSaveData;
    private CountryCodePicker countryCodePicker;
    private TextView emailTv, phoneTv;

    private final HashMap<String,String> dataUser= new HashMap<>();

    private String countryCode, action = "";
    private boolean isSwitchedLang = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        getExistingFirebaseToken();

        initView();

        initData();

        initAction();

    }

    @SuppressLint("ClickableViewAccessibility")
    private void initView(){
        edUsernamePh = findViewById(R.id.ed_mail);
        edPassword = findViewById(R.id.ed_password);
        emailTv = findViewById(R.id.emailTv);
        phoneTv = findViewById(R.id.phoneTv);
        progressBar = findViewById(R.id.progressItem);
        progressBar.setVisibility(View.GONE);
        countryCodePicker = findViewById ( R.id.ccp );

        TextView versionCodeTv = findViewById(R.id.version_code);
        versionCodeTv.setText ( String.format ( Locale.US , "%s %s (%s)",getResources ().getString ( R.string.version ), BuildConfig.VERSION_NAME,BuildConfig.VERSION_CODE ) );

        findViewById(R.id.activity_login).setOnTouchListener((view, motionEvent) -> {
            InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            View f = LoginActivity.this.getCurrentFocus();
            if (f != null){
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                return true;
            }
            else {
                return false;
            }
        });
    }

    private void initData(){
        userSaveData = new UserSessionManagement(this);
        if (getIntent() != null && getIntent().hasExtra("action")){
            action = getIntent().getStringExtra("action");
        }
    }

    private void initAction(){
        edPassword.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
        edPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());

        // Get Country Code
        countryCodePicker.setDefaultCountryUsingNameCodeAndApply(BaseActivity.country_code);
        countryCode = countryCodePicker.getFullNumber();
        countryCodePicker.setOnCountryChangeListener(selectedCountry -> countryCode = countryCodePicker.getFullNumber());

        edUsernamePh.setHint(getResources().getString(R.string.your_email_or_phone_number));
        edPassword.setHint(getResources().getString(R.string.your_password));

        // Btn Login
        findViewById(R.id.card_login).setOnClickListener(new CustomSetOnClickViewListener(view -> {
            if (InternetConnection.checkInternetConnectionActivity(getApplicationContext())){
                email_phone = edUsernamePh.getText().toString();
                password = edPassword.getText().toString();
                if (TextUtils.isEmpty(email_phone) || TextUtils.isEmpty(password)){
                    AppAlertCusDialog app = new AppAlertCusDialog();
                    app.showDialog(LoginActivity.this, getResources().getString(R.string.please_file_the_all_fields));
                } else {
                    if (!email_phone.contains("@")) {
                        if (email_phone.startsWith("0")) {
                            email_phone = countryCode.concat(edUsernamePh.getText().toString().trim().substring(1));
                        } else {
                            email_phone = countryCode.concat(edUsernamePh.getText().toString().trim());
                        }
                    } else {
                        email_phone = edUsernamePh.getText().toString().trim();
                    }

                    dataUser.put("username",email_phone);
                    dataUser.put("password",password);
                    dataUser.put("device_token", BaseActivity.DEVICE_ID);
                    dataUser.put("app_name",  "daikou");

                    progressBar.setVisibility(View.VISIBLE);
                    new LoginWs().loginWs(LoginActivity.this, dataUser, onReceiveCallBack);
                }
            } else {
                AppAlertCusDialog app = new AppAlertCusDialog();
                app.showDialog(LoginActivity.this, getResources().getString(R.string.please_check_your_internet_connection));
            }
        }));

        // Switch Language
        ImageView icon_lang = findViewById(R.id.icon_lang);
        String lang = Utils.getString("language", this);
        switch (lang) {
            case LANG_EN:
            case "":
                icon_lang.setImageResource(R.drawable.flag_united_kingdom);
                isSwitchedLang = false;
                break;
            case LANG_KH:
                icon_lang.setImageResource(R.drawable.cambodia_flag);
                isSwitchedLang = true;
                break;
            case LANG_CN:
                icon_lang.setImageResource(R.drawable.china_flag);
                isSwitchedLang = true;
                break;
        }
        icon_lang.setOnClickListener(view -> selectChangeLanguage());

        // Menu Email / Phone
        emailTv.setOnClickListener(view -> phoneEmailClick("email"));

        phoneTv.setOnClickListener(view -> phoneEmailClick("phone"));

        // Forgot Password
        findViewById(R.id.tv_forgot_password).setOnClickListener(new CustomSetOnClickViewListener(view ->
                startActivity(new Intent(this, ForgotPasswordActivity.class))
        ));

        // Sign Up
        findViewById(R.id.signUpTv).setOnClickListener(new CustomSetOnClickViewListener(view ->
                startActivity(new Intent(LoginActivity.this, SignUpDaikouActivity.class))
        ));
    }

    private void phoneEmailClick(String type){
        String titleHint;
        int startDrawable;
        int inputType;

        if (type.equalsIgnoreCase("email")){
            emailTv.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.appBarColorOld)));
            phoneTv.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.color_description)));
            countryCodePicker.setVisibility(View.GONE);

            titleHint = getResources().getString(R.string.your_email_or_phone_number);
            startDrawable = R.drawable.ic_email_profile;
            inputType = InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS;

        } else {
            emailTv.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.color_description)));
            phoneTv.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.appBarColorOld)));
            countryCodePicker.setVisibility(View.VISIBLE);

            titleHint = getResources().getString(R.string.enter_your_phone_number);
            startDrawable = 0;
            inputType = InputType.TYPE_CLASS_NUMBER;

        }

        phoneTv.setTextColor(Color.WHITE);
        emailTv.setTextColor(Color.WHITE);

        edUsernamePh.getText().clear();
        edUsernamePh.setHint(titleHint);
        edUsernamePh.setCompoundDrawablesWithIntrinsicBounds(startDrawable, 0, 0, 0);
        edUsernamePh.setInputType(inputType);
    }

    private void selectChangeLanguage(){
        ChangeLanguageFragment changeLanguageAlert = ChangeLanguageFragment.newInstance("lang_login");
        changeLanguageAlert.show(getSupportFragmentManager(), changeLanguageAlert.getClass().getSimpleName());
    }


    private final LoginWs.OnRecives onReceiveCallBack = new LoginWs.OnRecives() {
        @Override
        public void onSuccess(User data) {
            String acct = data.getAccessToken();
            String acci = data.getId();

            userSaveData.saveData(data, true);
            userSaveData.saveUserCookie(acct, acci);
            UserSessionManagement.saveString("user_type", data.getActiveUserType(), LoginActivity.this);
            Utils.saveString("token", data.getAccessToken(), LoginActivity.this);
            Utils.saveString(Constant.FIRST_SCAN_QR, "true", getApplicationContext());
            MyFirebaseMessagingService.sendRegistrationToServer(Utils.getString(Constant.FIREBASE_TOKEN, LoginActivity.this),LoginActivity.this, "add");

            if (isSwitchedLang){
                String lang = Utils.getString("language", LoginActivity.this);
                if (lang.equals("")){
                    lang = "en";
                }
                changLanguage(lang, acci,LoginActivity.this);
            } else {
                Intent intent;
                if (action.equals(StaticUtilsKey.action_key_scanner)){
                    intent = new Intent(LoginActivity.this, ScannerQRCodeAllActivity.class);
                    intent.putExtra("action", action);
                } else {
                    intent = new Intent(LoginActivity.this, MainActivity.class);
                }
                startActivity(intent);
                progressBar.setVisibility(View.GONE);
                finish();
            }

        }

        @Override
        public void onWrong(String msg) {
            progressBar.setVisibility(View.GONE);
            AppAlertCusDialog app = new AppAlertCusDialog();
            app.showDialog(LoginActivity.this, msg);
        }

        @Override
        public void onFailed(String mess) {
            progressBar.setVisibility(View.GONE);
            AppAlertCusDialog alert = new AppAlertCusDialog();
            alert.showDialog(LoginActivity.this, mess);
        }
    };

    private void getExistingFirebaseToken(){
        FirebaseApp.initializeApp(this);
        FirebaseMessaging.getInstance().getToken().addOnCompleteListener(LoginActivity.this, task -> {
            if (!task.isSuccessful()){
                return;
            }
            if (task.getResult() != null) {
                String token = task.getResult();
                Utils.logDebug("gettokenstken", token);
                Utils.saveString(Constant.FIREBASE_TOKEN, token, LoginActivity.this);
            }
        });
    }

    @Override
    public void onBackPressed() {
        finishAffinity();
    }

    private void changLanguage(String key, String user_id,Context context){
        HashMap<String,Object> hashMap = new HashMap<>();
        hashMap.put("login_user_id", user_id);
        hashMap.put("lang_code", key);
        new ChangLanguageWs().getLanguage(hashMap, new ChangLanguageWs.OnCallBackLanguages() {
            @Override
            public void onLoadChangSuccessFully(String message) {
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(intent);
                progressBar.setVisibility(View.GONE);
                finish();
            }

            @Override
            public void onLoadChangFail(String message) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(context, message, Toast.LENGTH_LONG).show();
            }
        });
    }

}