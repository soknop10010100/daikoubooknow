package com.eazy.daikou.ui.home.hrm.notice_board
import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.request_data.request.hr_ws.NoticeBoardWs
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.helper.web.WebsiteActivity
import com.eazy.daikou.model.hr.NoticeBoardDetailModel
import com.eazy.daikou.ui.home.hrm.notice_board.adapter.AttachmentNoticeAdapter

class ListNoticeBoardDetailActivity : BaseActivity() {
    private lateinit var btnBack: TextView
    private lateinit var progressItem: ProgressBar
    private lateinit var titleNoticeBoardTv: TextView
    private lateinit var dateTv: TextView
    private lateinit var locationTv: TextView
    private lateinit var createByTv: TextView
    private lateinit var descriptionTv: TextView
    private lateinit var linearAttachment: LinearLayout
    private lateinit var actionAttachment: TextView
    private lateinit var actionNextAttachment: TextView
    private lateinit var noDataDisplayTv: TextView
    private lateinit var recyclerNotice: RecyclerView
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var attachAdapter: AttachmentNoticeAdapter

    private var listAttachment : ArrayList<String> = ArrayList()

    private  var idListNotice : String = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_notice_board_detail)

        initView()

        intiAction()
    }

    private fun initView() {
        btnBack = findViewById(R.id.btn_back)
        progressItem = findViewById(R.id.progressItem)
        titleNoticeBoardTv = findViewById(R.id.titleNoticeBoard)
        dateTv = findViewById(R.id.dateTv)
        locationTv = findViewById(R.id.locationTv)
        createByTv = findViewById(R.id.createByTv)
        descriptionTv = findViewById(R.id.descriptionTv)
        linearAttachment = findViewById(R.id.linearAttachment)
        actionAttachment = findViewById(R.id.actionAttachment)
        noDataDisplayTv = findViewById(R.id.noDataDisplayTv)
        actionNextAttachment = findViewById(R.id.actionNextAttachment)
        recyclerNotice = findViewById(R.id.recyclerNotice)

        btnBack.setOnClickListener { finish() }

        if (intent != null && intent.hasExtra("id_list_notice_board")){
            idListNotice = intent.getStringExtra("id_list_notice_board").toString()
        }

    }

    private fun intiAction() {
        linearLayoutManager = LinearLayoutManager(this)
        recyclerNotice.layoutManager = linearLayoutManager
        attachAdapter = AttachmentNoticeAdapter(this, listAttachment , callBackAttachment)
        recyclerNotice.adapter = attachAdapter

        requestListNoticeBoardDetail()

        linearAttachment.setOnClickListener{
            if (recyclerNotice.visibility == View.VISIBLE){
                recyclerNotice.visibility = View.GONE
                actionAttachment.visibility = View.VISIBLE
                actionNextAttachment.visibility = View.GONE
                noDataDisplayTv.visibility = View.GONE
            } else {
                recyclerNotice.visibility = View.VISIBLE
                actionAttachment.visibility = View.GONE
                actionNextAttachment.visibility = View.VISIBLE
                if (listAttachment.isEmpty()){
                    noDataDisplayTv.visibility = View.VISIBLE
                }
            }
        }
    }

    private fun addDataForDetail(noticeBoardDetailModel: NoticeBoardDetailModel){
        if (noticeBoardDetailModel.notice_title != null) {
            titleNoticeBoardTv.text = noticeBoardDetailModel.notice_title
        } else {
            titleNoticeBoardTv.text = "- - -"
        }

        if (noticeBoardDetailModel.date != null){
            dateTv.text = noticeBoardDetailModel.date
        } else {
            dateTv.text = "- - -"
        }

        if (noticeBoardDetailModel.location != null){
            locationTv.text = noticeBoardDetailModel.location
        } else {
            locationTv.text = "- - -"
        }

        if (noticeBoardDetailModel.created_by != null){
            createByTv.text = noticeBoardDetailModel.created_by
        } else {
            createByTv.text = "- - -"
        }

        if (noticeBoardDetailModel.description != null){
            descriptionTv.text = noticeBoardDetailModel.description
        } else {
            descriptionTv.text = getString(R.string.no_description)
        }

    }

    private fun requestListNoticeBoardDetail(){
        progressItem.visibility = View.VISIBLE
        NoticeBoardWs().getNoticeBoardDetail(this, idListNotice, onCalBackDetailList)
    }

    private val callBackAttachment : AttachmentNoticeAdapter.CallbackItemAttachment = object : AttachmentNoticeAdapter.CallbackItemAttachment{
        override fun onClickBackItem(attachmentModel: String) {
            if (attachmentModel.contains(".pdf")) {
                Utils.openDefaultPdfView(this@ListNoticeBoardDetailActivity, attachmentModel)
            } else {
                if (attachmentModel.contains(".png") || attachmentModel.contains(".jpg") || attachmentModel.contains(".jpeg")) {
                        val intent = Intent(this@ListNoticeBoardDetailActivity, WebsiteActivity::class.java)
                        intent.putExtra("linkUrlNews", attachmentModel)
                        startActivity(intent)
                } else {
                    Utils.customToastMsgError(this@ListNoticeBoardDetailActivity,resources.getText(R.string.not_available_website).toString(), false)

                }
            }
        }

    }

    private val onCalBackDetailList : NoticeBoardWs.OnCallBackNoticeBoardDetail = object : NoticeBoardWs.OnCallBackNoticeBoardDetail{
        @SuppressLint("NotifyDataSetChanged")
        override fun onLoadSuccessFull(noticeBoardDetail: NoticeBoardDetailModel) {
            progressItem.visibility = View.GONE
            addDataForDetail(noticeBoardDetail)
            listAttachment.addAll(noticeBoardDetail.file_upload)
            attachAdapter.notifyDataSetChanged()
        }

        override fun onLoadFailed(message: String) {
            progressItem.visibility = View.GONE
            Utils.customToastMsgError(this@ListNoticeBoardDetailActivity, message, false)
        }

    }

}