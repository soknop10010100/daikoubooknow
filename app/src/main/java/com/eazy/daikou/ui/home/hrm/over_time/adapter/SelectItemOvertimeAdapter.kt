package com.eazy.daikou.ui.home.hrm.over_time.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.model.hr.ItemOvertimeModel

class SelectItemOvertimeAdapter (private val itemOvertimeList: List<ItemOvertimeModel>, private val actionKey: String, private val clickBack:ClickItemOvertimeCallBack): RecyclerView.Adapter<SelectItemOvertimeAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.layout_item_select_overtime,parent,false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
       val itemOvertimeModel : ItemOvertimeModel = itemOvertimeList[position]
        if (itemOvertimeModel != null){
            when (actionKey) {
                "overtime_category" -> {
                    holder.itemNameTv.text = if(itemOvertimeModel.name != null) itemOvertimeModel.name else "- - -"
                }
                "category_shift" -> {
                    holder.itemNameTv.text = if (itemOvertimeModel.name != null) itemOvertimeModel.name else "- - -"
                }
                "approved" -> {
                    holder.itemNameTv.text = if (itemOvertimeModel.user_name != null) itemOvertimeModel.user_name else "- - -"
                }
            }

            holder.itemView.setOnClickListener{
                clickBack.clickItemCallBackListener(itemOvertimeModel)
            }
        }
    }

    override fun getItemCount(): Int {
      return itemOvertimeList.size
    }


    class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {
        val itemNameTv: TextView = itemView.findViewById(R.id.ItemNameTv)
        val imageCheckSelect: ImageView = itemView.findViewById(R.id.imageCheckSelect)
    }

    interface ClickItemOvertimeCallBack{
        fun clickItemCallBackListener(itemOvertimeModel: ItemOvertimeModel)
    }

}