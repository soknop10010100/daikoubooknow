package com.eazy.daikou.ui.home.service_provider_new.activity

import android.os.Bundle
import android.widget.FrameLayout
import androidx.fragment.app.Fragment
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.ui.home.service_provider_new.fragment_service_provider.HomeServiceProviderFragment
import com.eazy.daikou.ui.home.service_provider_new.fragment_service_provider.MapServiceProviderFragment
import com.eazy.daikou.ui.home.service_provider_new.fragment_service_provider.ProfileServiceProviderFragment
import com.eazy.daikou.ui.home.service_provider_new.fragment_service_provider.SearchServiceProviderFragment
import com.google.android.material.bottomnavigation.BottomNavigationView

class ServiceProviderHomeActivity : BaseActivity() {

    private lateinit var fragmentContainer: FrameLayout
    private lateinit var bottomNavigation: BottomNavigationView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_service_provider_home)

        initView()

        initAction()
    }

    private fun initView(){
        bottomNavigation = findViewById(R.id.bottomNavigation)
        fragmentContainer = findViewById(R.id.fragmentContainer)

    }
    private fun initAction(){

        loadFragment(HomeServiceProviderFragment())
        bottomNavigation.setOnItemSelectedListener {

            when(it.itemId){
                R.id.btnHome ->{
                    loadFragment(HomeServiceProviderFragment())
                    return@setOnItemSelectedListener true
                }
                R.id.btnSearch ->{
                    loadFragment(SearchServiceProviderFragment())
                    return@setOnItemSelectedListener true
                }
                R.id.btnMap ->{
                    loadFragment(MapServiceProviderFragment())
                    return@setOnItemSelectedListener true
                }
                R.id.btnProfile ->{
                    loadFragment(ProfileServiceProviderFragment())
                    return@setOnItemSelectedListener true
                }
            }
           false
        }
    }

    private  fun loadFragment(fragment: Fragment){
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.fragmentContainer,fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    override fun onBackPressed() {
        finish()
    }

}