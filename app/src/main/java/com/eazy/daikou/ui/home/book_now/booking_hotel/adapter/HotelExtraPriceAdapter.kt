package com.eazy.daikou.ui.home.book_now.booking_hotel.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.booking_hotel.ExtraPrice
import java.util.ArrayList

class HotelExtraPriceAdapter(private val list : ArrayList<ExtraPrice>, private val onClickListener : OnClickItemListener) : RecyclerView.Adapter<HotelExtraPriceAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.hotel_extra_price_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBindingView(list[position], onClickListener)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val priceTv: TextView = itemView.findViewById(R.id.priceTv)
        private val itemNameTv: TextView = itemView.findViewById(R.id.itemNameTv)
        private val iconSelect: ImageView = itemView.findViewById(R.id.iconSelect)
        private var mainLayout : LinearLayout = itemView.findViewById(R.id.mainLayout)

        fun onBindingView(item : ExtraPrice, onClickListener : OnClickItemListener){
            Utils.setValueOnText(priceTv, item.price_display)
            if(item.name != null) itemNameTv.text = item.name else itemNameTv.text = ""

            iconSelect.background = ResourcesCompat.getDrawable(iconSelect.resources, if (item.isClick) R.drawable.circle_appbar else R.drawable.circle_border_black, null)

            mainLayout.setOnClickListener{ onClickListener.onClickListener(item) }
        }
    }

    interface OnClickItemListener {
        fun onClickListener(item : ExtraPrice)
    }
}