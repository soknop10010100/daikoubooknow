package com.eazy.daikou.ui.home.hrm.over_time

import android.annotation.SuppressLint
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.request_data.request.hr_ws.OvertimeWs
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.hr.OvertimeListModel
import com.eazy.daikou.model.hr.OvertimeMainModel
import com.eazy.daikou.model.profile.User
import com.eazy.daikou.ui.home.hrm.over_time.adapter.ItemOverTimeAdapter
import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog
import com.google.gson.Gson
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class OverTimeHRActivity : BaseActivity() {

    private lateinit var itemOverRecycle : RecyclerView
    private lateinit var progressBar : ProgressBar
    private lateinit var btnCreateOT : ImageView
    private lateinit var btnMyOvertime: TextView
    private lateinit var btnBack: TextView
    private lateinit var linearSelect: LinearLayout
    private lateinit var btnApproveOvertime: TextView
    private lateinit var noDataForDisplay: TextView
    private lateinit var itemOverTimeAdapter: ItemOverTimeAdapter
    private lateinit var refreshLayout : SwipeRefreshLayout
    private lateinit var linearLayoutManager : LinearLayoutManager
    private lateinit var monthTv: TextView
    private lateinit var totalHourTv : TextView
    private var listOverTime : ArrayList<OvertimeListModel> = ArrayList()
    private var actionClick = "my_request"
    private var userBusinessKey = ""
    private var accountBusinessKey = ""
    private var activeUserType = ""
    private var listCategory = "my_list"
    private var currentItem = 0
    private var total : Int = 0
    private var scrollDown : Int = 0
    private var currentPage : Int = 1
    private var size : Int = 10
    private var isScrolling = false
    private var REQUEST_CODE_CREATE = 129
    private var REQUEST_CODE_DETAIL = 127
    private var isScrollItem = false
    private var action = ""
    private var overtTimeId = ""
    private lateinit var user : User
    private var yearService: String = ""
    private var monthService: String = ""
    private var positionClick = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_over_time)

        initView()

        initData()

        initAction()

    }

    private fun initView(){
        btnBack = findViewById(R.id.icon_back)
        btnMyOvertime = findViewById(R.id.btnMyOvertime)
        btnApproveOvertime = findViewById(R.id.btnApproveOvertime)
        linearSelect = findViewById(R.id.linearSelect)
        progressBar = findViewById(R.id.progressItem)
        itemOverRecycle = findViewById(R.id.itemOverRecycle)
        btnCreateOT = findViewById(R.id.btnCreateOT)
        refreshLayout = findViewById(R.id.refreshActivity)
        noDataForDisplay = findViewById(R.id.noDataForDisplay)
        totalHourTv = findViewById(R.id.totalHourTv)
        monthTv = findViewById(R.id.monthTv)
    }

    private fun initData(){
        if (intent != null && intent.hasExtra("action")){
            action = intent.getStringExtra("action").toString()
            if (intent != null && intent.hasExtra("id")) {
                overtTimeId = intent.getStringExtra("id").toString()
            }
        }

        user = Gson().fromJson(UserSessionManagement(this).userDetail, User::class.java)
        val businessKy = user.userBusinessKey
        if (businessKy != null){
            userBusinessKey = businessKy
        }

        val accBusinessKey = user.activeAccountBusinessKey
        if (accBusinessKey != null){
            accountBusinessKey = accBusinessKey
        }

        // Start detail from notification after request list already
        if (action == "overtime_application") {
            startOvertimeDetail(overtTimeId)
            action = ""
        }
    }


    @SuppressLint("SetTextI18n", "SimpleDateFormat", "ResourceAsColor")
    private fun initAction(){
        val yearFormatServer = SimpleDateFormat("MMMM, yyyy", Locale.getDefault())
        val monthFormat = SimpleDateFormat("MM", Locale.getDefault())
        val yearFormat = SimpleDateFormat("yyyy", Locale.getDefault())
        val date = Date()

        monthService = monthFormat.format(date)
        yearService = yearFormat.format(date)
        monthTv.text = yearFormatServer.format(date)
        monthTv.setOnClickListener(CustomSetOnClickViewListener {
            SingleDateAndTimePickerDialog.Builder(this)
                .bottomSheet()
                .curved()
                .backgroundColor(getColor(R.color.appBarColor))
                .mainColor(getColor(R.color.greenSea))
                .displayMinutes(false)
                .displayHours(false)
                .displayDays(false)
                .displayMonth(true)
                .displayYears(true)
                .displayDaysOfMonth(false)
                .title((Utils.getText(this, R.string.date_of_overtime)))
                .titleTextColor(getColor(R.color.white))
                .listener { date->
                    monthService = monthFormat.format(date)
                    yearService = yearFormat.format(date)
                    monthTv.text = yearFormatServer.format(date)
                    onRefreshList()
                }.display()
        })

        getLinearLayout(linearSelect)

        linearLayoutManager = LinearLayoutManager(this)
        itemOverRecycle.layoutManager = linearLayoutManager
        itemOverTimeAdapter = ItemOverTimeAdapter(this, listOverTime,onClickCallBack)
        itemOverRecycle.adapter = itemOverTimeAdapter

        requestOvertimeList()

        initOnScrollItem(itemOverRecycle)

        btnBack.setOnClickListener { finish() }

        btnCreateOT.setOnClickListener{
            val intent = Intent(this,CreateOverTimeHRActivity::class.java)
            btnCreateOT.isEnabled = false
            startActivityForResult(intent,REQUEST_CODE_CREATE)
        }

        refreshLayout.setColorSchemeResources(R.color.colorPrimary)
        refreshLayout.setOnRefreshListener { this.onRefreshList() }

    }

    private fun onRefreshList() {
        currentPage = 1
        size = 10
        isScrolling = true
        itemOverTimeAdapter.clear()
        requestOvertimeList()
    }

    private fun getLinearLayout(linearLayout: LinearLayout){
        for (i in 0 until linearLayout.childCount) {
            linearLayout.getChildAt(i).setOnClickListener {
                if (progressBar.visibility == View.GONE) {
                    isScrollItem = false
                    setBackgroundMenu(i)
                    positionClick = i
                    if (i == 0) {
                        listCategory = "my_list"
                        actionClick = "my_request"
                        btnMyOvertime.isEnabled = false
                        onRefreshList()
                    } else {
                        listCategory = activeUserType
                        actionClick = "approve_overtime"
                        btnApproveOvertime.isEnabled = false
                        onRefreshList()
                    }
                }
            }
        }
    }

    private fun setBackgroundMenu(position: Int) {
        if (position == 0) {
            btnMyOvertime.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(this, R.color.color_dark_gray))
            btnApproveOvertime.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(this, R.color.transparent))
            btnMyOvertime.setTextColor(Color.WHITE)
            btnApproveOvertime.setTextColor(Color.GRAY)
        } else {
            btnMyOvertime.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(this, R.color.transparent))
            btnApproveOvertime.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(this, R.color.color_dark_gray))
            btnMyOvertime.setTextColor(Color.GRAY)
            btnApproveOvertime.setTextColor(Color.WHITE)
        }
    }

    private fun requestOvertimeList(){
        progressBar.visibility = View.VISIBLE
        OvertimeWs().getOvertimeListWs(this, currentPage, 10, userBusinessKey,listCategory, accountBusinessKey, monthService,yearService, onCallBackOvertimeListener)
    }

    private val onCallBackOvertimeListener: OvertimeWs.OnCallBackOvertimeListener = object : OvertimeWs.OnCallBackOvertimeListener {
        @SuppressLint("NotifyDataSetChanged")
        override fun onLoadSuccessFull(overtimeListModel: OvertimeMainModel) {
            progressBar.visibility = View.GONE
            refreshLayout.isRefreshing = false
            btnCreateOT.visibility = if (positionClick == 0) View.VISIBLE else View.GONE

            if (!isScrollItem)  listOverTime.clear()        //Clear item
            listOverTime.addAll(overtimeListModel.list)
            itemOverTimeAdapter.notifyDataSetChanged()
            btnMyOvertime.isEnabled = true
            btnApproveOvertime.isEnabled = true

            if (listOverTime.size > 0) {
                noDataForDisplay.visibility = View.GONE
            } else {
                noDataForDisplay.visibility = View.VISIBLE
            }
            totalHourTv.text = if (overtimeListModel.total_overtime_hour != null) overtimeListModel.total_overtime_hour + " " + resources.getString(R.string.hour_s) else ""
        }

        override fun onLoadFailed(message: String) {
            progressBar.visibility = View.GONE
            refreshLayout.isRefreshing = false
            Utils.customToastMsgError(this@OverTimeHRActivity, message, false)
            btnMyOvertime.isEnabled = true
            btnApproveOvertime.isEnabled = true
        }
    }

    private var onClickCallBack = object : ItemOverTimeAdapter.ClickCallBack{
        override fun onClickCallBack(overtimeListModel: OvertimeListModel) {
            if (progressBar.visibility == View.GONE) {
                overtimeListModel.id?.let { startOvertimeDetail(it) }
            }
        }
    }

    private fun startOvertimeDetail(overtimeId : String){
        val intent = Intent(this@OverTimeHRActivity, OverTimeDetailActivity::class.java)
        intent.putExtra("action_click", action)
        intent.putExtra("overtime_id", overtimeId)
        intent.putExtra("user_business_key", userBusinessKey)
        startActivityForResult(intent,REQUEST_CODE_DETAIL)
    }

    private fun initOnScrollItem(recyclerViewItem: RecyclerView) {
        recyclerViewItem.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true
                }
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                currentItem = linearLayoutManager.childCount
                total = linearLayoutManager.itemCount
                scrollDown = linearLayoutManager.findFirstVisibleItemPosition()
                if (dy > 0) {
                    if (isScrolling && currentItem + scrollDown == total) {
                        if (total == size) {
                            isScrolling = false
                            currentPage++
                            progressBar.visibility = View.VISIBLE
                            isScrollItem = true
                            recyclerViewItem.scrollToPosition(listOverTime.size - 1)
                            requestOvertimeList()
                            size += 10
                        } else {
                            progressBar.visibility = View.GONE
                        }
                    }
                }
            }
        })
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (data != null) {
            if (data.hasExtra("isClickedFirst")) {
                val statusResult = data.getBooleanExtra("isClickedFirst", false)
                if (statusResult) {
                    btnCreateOT.isEnabled = true
                }
            }
        } else {
            if (resultCode == RESULT_OK && requestCode == REQUEST_CODE_CREATE) {
                onRefreshList()
                btnCreateOT.isEnabled = true

            }
            if (resultCode == RESULT_OK && requestCode == REQUEST_CODE_DETAIL) {
                if (listCategory == "my_list") {
                    setBackgroundMenu(0)
                } else {
                    setBackgroundMenu(1)
                }
                btnMyOvertime.isEnabled = false
                listCategory = activeUserType
                onRefreshList()
            }
        }
    }
}