package com.eazy.daikou.ui.home.hrm.leave_request

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AbsListView
import android.widget.ProgressBar
import android.widget.RelativeLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.base.BaseFragment
import com.eazy.daikou.request_data.request.hr_ws.LeaveManagementWs
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.hr.LeaveBalance
import com.eazy.daikou.model.hr.LeaveManagement
import com.eazy.daikou.model.profile.User
import com.eazy.daikou.ui.home.hrm.leave_request.adapter.LeaveAttendanceAdapter
import com.google.gson.Gson

class ManageLeaveFragment : BaseFragment() {

    private lateinit var recyclerView : RecyclerView
    private lateinit var leaveAdapter : LeaveAttendanceAdapter
    private lateinit var layoutManager : LinearLayoutManager
    private lateinit var progressBar : ProgressBar
    private lateinit var refreshLayout : SwipeRefreshLayout
    private lateinit var relateNoItem : RelativeLayout
    private var listLeave = ArrayList<LeaveManagement>()
    private var currentItem = 0
    private var total : Int = 0
    private var scrollDown : Int = 0
    private var currentPage : Int = 1
    private var size : Int = 10
    private var isScrolling = false
    private var userId = ""
    private var action = ""
    private var leaveId = ""
    private var REQUEST_CODE_CREATE_LEAVE = 376
    private lateinit var user : User
    private var activeUserType = ""
    private var userBusinessKey = ""
    private var accountBusinessKey = ""
    private var listType = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_manage_leave, container, false)

        initView(view)

        initData()

        initAction()

        return view;
    }

    private fun initView(view: View) {
        recyclerView = view.findViewById(R.id.recyclerManageLeave)
        progressBar = view.findViewById(R.id.progressItem)
        refreshLayout = view.findViewById(R.id.swipe_layouts)
        relateNoItem = view.findViewById(R.id.image_no_record)
    }

    private fun initData() {

        user = Gson().fromJson(UserSessionManagement(mActivity).userDetail, User::class.java)
        val businessKy = user.userBusinessKey
        if (businessKy != null){
            userBusinessKey = businessKy
        }

//      //  val accBusinessKey = user.activeAccountBusinessKey
//        if (accBusinessKey != null){
//            accountBusinessKey = accBusinessKey
//        }

        if (activity?.intent != null && activity?.intent!!.hasExtra("id")) {
            leaveId = activity?.intent!!.getStringExtra("id").toString()
        }

        getUserType()

        layoutManager = LinearLayoutManager(mActivity)
        recyclerView.layoutManager = layoutManager

        getListLeave()

    }
    private fun initAction() {
        refreshLayout.setColorSchemeResources(R.color.colorPrimary)
        refreshLayout.setOnRefreshListener {refreshItemList()}

        onScrollMyLeave()
    }

    private fun refreshItemList(){
        currentPage = 1
        size = 10
        progressBar.visibility = View.GONE
        leaveAdapter.clear()

        getListLeave()

    }

    private fun getUserType(){
     //   val userType = user.activeUserType
       // val roleUserType = user.hrmRole

//        if (roleUserType != null){
//            activeUserType = if (roleUserType == "superadmin" || roleUserType == "subadmin" || roleUserType == "hr") "all" else "my_assigned_list"
//        }
//        listType = activeUserType
    }


    private val itemClick = object : LeaveAttendanceAdapter.ItemClickLeaveListener{
        override fun onItemClick(leave: LeaveManagement) {
            startLeaveDetail(leave.id)
        }

    }

    private fun startLeaveDetail(leaveId : String){
        val intent = Intent(mActivity,LeaveRequestDetailActivity::class.java)
        intent.putExtra("id", leaveId)
        intent.putExtra("list_type",listType)
//        intent.putExtra("user_business_key", userBusinessKey)
        startActivity(intent)
    }

    private fun onScrollMyLeave() {
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true
                }
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                currentItem = layoutManager.childCount
                total = layoutManager.itemCount
                scrollDown = layoutManager.findFirstVisibleItemPosition()
                if (dy > 0) {
                    if (isScrolling && currentItem + scrollDown == total) {
                        if (total == size) {
                            isScrolling = false
                            currentPage++
                            progressBar.visibility = View.VISIBLE
                            recyclerView.scrollToPosition(listLeave.size - 1)
                            size += 10
                            getListLeave()
                        } else {
                            progressBar.visibility = View.GONE
                        }
                    }
                }
            }
        })
    }

    private fun getListLeave(){
        progressBar.visibility = View.VISIBLE
        LeaveManagementWs().getLeaveManagement(mActivity, userBusinessKey, currentPage, 10, listType, accountBusinessKey, leaveCallBack)

        leaveAdapter = LeaveAttendanceAdapter(listLeave,mActivity,itemClick)
        recyclerView.adapter = leaveAdapter
    }

    private val leaveCallBack = object : LeaveManagementWs.OnCallBackLeaveManagementListener{

        @SuppressLint("NotifyDataSetChanged")
        override fun onLoadListSuccessFull(listLeaveManagement: ArrayList<LeaveManagement>) {
            progressBar.visibility = View.GONE
            refreshLayout.isRefreshing = false
            listLeave.addAll(listLeaveManagement)

            recyclerView.visibility = if(listLeave.size > 0) View.VISIBLE else View.GONE
            relateNoItem.visibility = if(listLeave.size > 0) View.GONE else View.VISIBLE
            leaveAdapter.notifyDataSetChanged()

            // Start detail from notification after request list already
            if (action == "leave_application") {
                startLeaveDetail(leaveId)
                action = ""
            }
        }

        override fun onLoadListCategorySuccessFull(leaveBalance: ArrayList<LeaveBalance>) {}

        override fun onLoadFail(message: String) {
            progressBar.visibility = View.GONE
            refreshLayout.isRefreshing = false

            Utils.customToastMsgError(mActivity, message, false)
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == BaseActivity.RESULT_OK && requestCode == REQUEST_CODE_CREATE_LEAVE){
            refreshItemList()
        }
    }

}