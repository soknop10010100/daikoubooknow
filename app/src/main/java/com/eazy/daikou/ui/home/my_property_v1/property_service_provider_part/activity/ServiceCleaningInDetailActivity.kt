package com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.cardview.widget.CardView
import androidx.viewpager.widget.ViewPager
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.request_data.request.service_property_ws.MyUnitPropertyWs
import com.eazy.daikou.request_data.request.service_property_ws.ServiceAndPropertyWs
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.helper.web.WebsitePropertyActivity
import com.eazy.daikou.model.my_property.service_property_employee.ServicePropertyIn
import com.eazy.daikou.model.profile.User
import com.eazy.daikou.model.utillity_tracking.model.UnitImgModel
import com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.adapter.ListImageAdapter
import com.google.android.material.tabs.TabLayout
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView

class ServiceCleaningInDetailActivity : BaseActivity() {

    private lateinit var companyNameTv : TextView
    private lateinit var addressTv : TextView
    private lateinit var descriptionTv : TextView
    private lateinit var scopeOfWorkTv : TextView
    private lateinit var btnCall : ImageButton
    private lateinit var serviceProperty : ServicePropertyIn
    private lateinit var imageCompany : CircleImageView
    private lateinit var webSiteTv : TextView
    private lateinit var listImageAdapter : ListImageAdapter
    private lateinit var viewPager : ViewPager
    private lateinit var btnCreateService : TextView
    private lateinit var btnHistoryOfService : TextView
    private lateinit var btnBack : TextView
    private lateinit var cardCreate : CardView
    private lateinit var phoneNumberTv : TextView
    private lateinit var emailTv : TextView
    private lateinit var facebookTv : TextView
    private lateinit var wechatTv : TextView
    private lateinit var loading : ProgressBar
    private var sessionManagement : UserSessionManagement? = null
    private var user : User? = null
    private lateinit var linearLayout: LinearLayout
    private var countTv : TextView? = null

    private val myUnitList : ArrayList<UnitImgModel> = ArrayList()
    private var listImage : ArrayList<String> = ArrayList()

    private var type : String = " "
    private var serviceId = " "
    private var phoneNumber = ""
    private var isRefresh = ""
    private val CODE_REFRESH = 289
    private lateinit var tabLayout : TabLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_service_cleaning)

        initView()

        initData()

        initAction()

    }

    private fun initView(){
        linearLayout = findViewById(R.id.linear_view)
        btnBack = findViewById(R.id.btn_back)
        companyNameTv = findViewById(R.id.company_name)
        addressTv = findViewById(R.id.location_text)
        descriptionTv = findViewById(R.id.descriptionTv)
        scopeOfWorkTv = findViewById(R.id.scope_of_work)
        btnCall = findViewById(R.id.btnCall)
        imageCompany = findViewById(R.id.user_pro_image)
        webSiteTv = findViewById(R.id.website_Text)
        viewPager = findViewById(R.id.viewpagers)
        countTv = findViewById(R.id.countItem)
        btnCreateService = findViewById(R.id.creates_serivce)
        btnHistoryOfService = findViewById(R.id.list_service_cleaning)
        cardCreate = findViewById(R.id.card_create)
        phoneNumberTv = findViewById(R.id.phone_number)
        emailTv = findViewById(R.id.email_text)
        facebookTv = findViewById(R.id.face_book)
        wechatTv = findViewById(R.id.we_chat)
        loading = findViewById(R.id.progressItem)
        tabLayout = findViewById(R.id.tab_layout)
    }

    private fun initData(){
        loading.visibility = View.GONE

        if (intent.hasExtra("service_id")) {
            serviceId = intent.getStringExtra("service_id") as String
        }

        if (intent.hasExtra("type")) {
            type = intent.getStringExtra("type") as String
        }

        if (intent.hasExtra("isRefresh")) {
            isRefresh = intent.getStringExtra("isRefresh") as String
        }

        sessionManagement = UserSessionManagement(this)
        val gson = Gson()
        user = gson.fromJson(sessionManagement!!.userDetail, User::class.java)

        if(user!!.activeUserType == "employee"||user!!.activeUserType == "developer"||user!!.activeUserType == "admin"){
            cardCreate.visibility = View.GONE
        }else{
            cardCreate.visibility = View.VISIBLE
        }

        getServiceAndProperty()

        getUnitNo()
    }

    private fun initAction(){
        btnBack.setOnClickListener {
            if (isRefresh == "is") {
                val intent = Intent(this, PropertyServiceOutActivity::class.java)
                intent.putExtra("is_back",isRefresh)
                startActivity(intent)
            } else {
                finish()
            }

        }

        webSiteTv.setOnClickListener {
            if (serviceProperty.serviceWebsite != null) {
                val intent = Intent(applicationContext, WebsitePropertyActivity::class.java)
                intent.putExtra("linkUrlServiceProperty", serviceProperty.serviceWebsite)
                intent.putExtra("contractor_name", serviceProperty.serviceName)
                startActivityForResult(intent,CODE_REFRESH)
            } else {
                Toast.makeText(applicationContext, resources.getString(R.string.not_available_website), Toast.LENGTH_SHORT).show()
            }
        }

        btnCall.setOnClickListener {
            val callIntent = Intent(Intent.ACTION_DIAL)
            callIntent.data = Uri.parse("tel:${phoneNumber}")
            callIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivityForResult(callIntent,CODE_REFRESH)
        }

        btnCreateService.setOnClickListener {
            if (myUnitList.size > 0) {
                // btnCreateService.isEnabled = false
                val intent = Intent(this, SelectServiceNameCleaningActivity::class.java)
                intent.putExtra("image", serviceProperty.serviceImage)
                intent.putExtra("lisUnit", myUnitList)
                startActivityForResult(intent, CODE_REFRESH)
            } else {
                btnCreateService.isEnabled = true
                Toast.makeText(this@ServiceCleaningInDetailActivity, resources.getString(R.string.you_do_not_create_service_cleaning),Toast.LENGTH_SHORT).show()
            }

        }

        btnHistoryOfService.setOnClickListener {
            loading.visibility = View.GONE
            val intent = Intent(this, ListHistoryCleaningServiceActivity::class.java)
            intent.putExtra("type", type)
            startActivityForResult(intent,CODE_REFRESH)
        }

    }
    private fun getServiceAndProperty(){
        loading.visibility = View.VISIBLE
        ServiceAndPropertyWs().getDetailServiceProperty(this,type,serviceId,callBackService)
    }
    private val callBackService = object : ServiceAndPropertyWs.OnCallBackServiceIn{
        @SuppressLint("SetTextI18n")
        override fun onLoadDetailSuccessFull(servicePropertyIns: ServicePropertyIn) {
            loading.visibility = View.GONE
            serviceProperty = servicePropertyIns
            linearLayout.visibility = View.VISIBLE
            phoneNumber = serviceProperty.servicePhoneNumber
            listImage.addAll(serviceProperty.serviceImage)

            loadImage()

            if (serviceProperty.serviceLogo != null) {
                Picasso.get().load(serviceProperty.serviceLogo).into(imageCompany)
            } else {
                Picasso.get().load(R.drawable.no_image).into(imageCompany)
            }
            phoneNumberTv.text = phoneNumber
            emailTv.text = serviceProperty.serviceEmail
            companyNameTv.text = serviceProperty.serviceName
            addressTv.text = serviceProperty.serviceAddress

            if(serviceProperty.serviceWebsite.isNotEmpty()){
                webSiteTv.text = serviceProperty.serviceWebsite
            }else{
                webSiteTv.text = getString(R.string.no_web_site)
            }
            if(serviceProperty.serviceFacebook.isNotEmpty()){
                facebookTv.text = serviceProperty.serviceFacebook
            }else{
                facebookTv.text = getString(R.string.no_account)
            }

            if(serviceProperty.serviceWechat.isNotEmpty()){
                wechatTv.text = serviceProperty.serviceWechat
            }else{
                wechatTv.text = getString(R.string.no_account)
            }

            descriptionTv.text = serviceProperty.serviceDesc

        }

        override fun onLoadFail(message: String?) {
            loading.visibility = View.GONE
            linearLayout.visibility = View.GONE
            Toast.makeText(this@ServiceCleaningInDetailActivity,message,Toast.LENGTH_SHORT).show()
        }

    }

    @SuppressLint("SetTextI18n")
    private fun loadImage() {
        listImageAdapter = ListImageAdapter(supportFragmentManager, listImage, "showImage")
        viewPager.adapter = listImageAdapter
        tabLayout.setupWithViewPager(viewPager, true)
        listImageAdapter.notifyDataSetChanged()
    }


    private fun getUnitNo() {
        MyUnitPropertyWs().getServiceMyUnitV2(this, Utils.validateNullValue(user!!.accountId),
            user!!.accountId, 1, 100, sessionManagement!!.userId, callBackListener)
    }

    private val callBackListener: MyUnitPropertyWs.MyUnitCallBackListener = object :
        MyUnitPropertyWs.MyUnitCallBackListener {
        override fun getServiceUnit(unitImgModels: ArrayList<UnitImgModel>) {
            myUnitList.addAll(unitImgModels)
        }

        override fun failed(error: String) {
            Toast.makeText(this@ServiceCleaningInDetailActivity, error, Toast.LENGTH_SHORT).show()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(data!=null && resultCode == RESULT_OK){
            if(requestCode == CODE_REFRESH){
                loading.visibility = View.GONE
                btnCreateService.isEnabled = true
            }
        }
    }
    

}