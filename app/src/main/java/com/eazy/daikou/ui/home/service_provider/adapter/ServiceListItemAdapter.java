package com.eazy.daikou.ui.home.service_provider.adapter;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.eazy.daikou.R;
import com.eazy.daikou.model.my_property.service_provider.ServiceProvider;

import java.util.List;

public class ServiceListItemAdapter extends RecyclerView.Adapter<ServiceListItemAdapter.ViewHolder> {

    private final Context context;
    private final List<ServiceProvider> serviceProviders;
    private final ItemClickService itemClickService;

    public ServiceListItemAdapter(List<ServiceProvider> serviceProviders,Context context, ItemClickService itemClickService) {
        this.serviceProviders = serviceProviders;
        this.context = context;
        this.itemClickService = itemClickService;
    }
    @NonNull
    @Override
    public ServiceListItemAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view  = LayoutInflater.from(context).inflate(R.layout.contact_header_layout,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ServiceListItemAdapter.ViewHolder holder, int position) {
        ServiceProvider serviceProvider = serviceProviders.get(position);
        if(serviceProvider!=null){
            holder.title.setText(serviceProvider.getCity());
            holder.title.setGravity(Gravity.CENTER);
            holder.recyclerView.setVisibility(View.VISIBLE);
            if(serviceProvider.getCompanyContact()!=null){
                holder.phoneNumber.setText(serviceProvider.getCompanyContact());
            }else {
                holder.phoneNumber.setText(context.getResources().getString(R.string.no_contact));
                holder.phoneNumber.setTextColor(context.getResources().getColor(R.color.red));
            }

            holder.organization.setText(serviceProvider.getContractorName());
            if(serviceProvider.getCompanyLogo()!=null){
                Glide.with(holder.itemView.getContext()).load(serviceProvider.getCompanyLogo()).into(holder.imageView);
            }else {
                Glide.with(holder.itemView.getContext()).load(R.drawable.no_image).into(holder.imageView);
            }
            holder.linearItem.setOnClickListener(view -> itemClickService.itemClick(serviceProvider));

            holder.linearSeeMore.setOnClickListener(view -> itemClickService.itemClickSeeMore(serviceProvider));

        }


//        ItemServicesAdapter itemServicesAdapter = new ItemServicesAdapter(context,serviceProviders,clickBack);
//        holder.recyclerView.setHasFixedSize(true);
//        holder.recyclerView.setLayoutManager(new LinearLayoutManager(context,RecyclerView.HORIZONTAL,false));
//        holder.recyclerView.setAdapter(itemServicesAdapter);
//        holder.recyclerView.setNestedScrollingEnabled(false);
//        itemServicesAdapter.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return serviceProviders.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        TextView title;
        RecyclerView recyclerView;
        ImageView imageView;
        TextView organization,phoneNumber;
        LinearLayout linearSeeMore,linearItem;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.title);
            recyclerView = itemView.findViewById(R.id.recyclerView);
            imageView = itemView.findViewById(R.id.image);
            organization = itemView.findViewById(R.id.servicename);
            phoneNumber = itemView.findViewById(R.id.custmer_phone_number);
            linearSeeMore = itemView.findViewById(R.id.linear_seeMore);
            linearItem = itemView.findViewById(R.id.linear_item);
        }
    }

    public interface ItemClickService{
        void itemClick(ServiceProvider serviceProvider);
        void itemClickSeeMore(ServiceProvider serviceProvider);
    }

    public void clear() {
        int size = serviceProviders.size ();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                serviceProviders.remove ( 0 );
            }
            notifyItemRangeRemoved ( 0, size );
        }
    }
}
