package com.eazy.daikou.ui.sign_up;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.eazy.daikou.R;
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener;
import com.eazy.daikou.base.BaseActivity;
import com.eazy.daikou.base.MockUpData;
import com.eazy.daikou.request_data.request.profile_ws.SignUpWs;
import com.eazy.daikou.helper.Utils;
import com.eazy.daikou.ui.LoginActivity;
import com.eazy.daikou.ui.home.book_now.booking_account.LoginBookNowActivity;
import com.poovam.pinedittextfield.SquarePinField;

import java.util.HashMap;
import java.util.Locale;
import java.util.Objects;

public class VerifySmsCodeActivity extends BaseActivity {

    private SquarePinField pin_Code;
    private  TextView countTimeTv, btnResent;

    private final HashMap<String,Object> hashMapVerifyPin = new HashMap<>();
    private HashMap<String, Object> hashMapPinCode = new HashMap<>();
    private   String phoneNumber = "", verifyPinCode = "", action = "";
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(isBookNowApp ? R.layout.hotel_confirm_sms_code : R.layout.activity_verify_sms_code);

        pin_Code = findViewById(R.id.pin_code);
        countTimeTv = findViewById(R.id.countTime);
        btnResent = findViewById(R.id.tv_resend_code);
        progressBar = findViewById(R.id.progressItem);
        progressBar.setVisibility(View.GONE);

        Utils.customOnToolbar(this, getResources().getString(R.string.verification_code), this::finish);

        //Init Data
        if(getIntent()!=null && getIntent().hasExtra("phone")){
            phoneNumber = getIntent().getStringExtra("phone");
        }

        if(getIntent()!=null && getIntent().hasExtra("pin_code")){
            verifyPinCode = getIntent().getStringExtra("pin_code");
        }

        if(getIntent()!=null && getIntent().hasExtra("action")){
            action = getIntent().getStringExtra("action");
        }

        //Data map for sent to request pin code
        if (getIntent() != null && getIntent().hasExtra("hashmap_request_code")){
            hashMapPinCode = (HashMap<String, Object>) getIntent().getSerializableExtra("hashmap_request_code");
        }

        String star = "";
        if (phoneNumber != null) {
            star = Utils.starDynamic(phoneNumber.length());
        }

        if (!isBookNowApp){
            TextView phoneTv = findViewById(R.id.phone_text);
            if (phoneNumber.length() > 3) {
                phoneTv.setText(String.format("%s %s %s", phoneNumber.substring(0, 3), star.substring(3, phoneNumber.length()), phoneNumber.substring(phoneNumber.length() - 3)));
            } else {
                phoneTv.setText(phoneNumber);
            }
        }

        //Init Action
        countTime();

        pin_Code.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                hashMapVerifyPin.put("verify_code",editable.toString());
                if (editable.length()== 6){
                    Utils.hideSoftKeyboard(pin_Code);
                    if (verifyPinCode.equalsIgnoreCase(editable.toString())){
                        if (action.equalsIgnoreCase("forgot_password")){
                            Intent intent = getIntent();
                            intent.putExtra("pin_code", verifyPinCode);
                            Utils.customToastMsgError(VerifySmsCodeActivity.this, getResources().getString(R.string.verify_code_email_have_sent_to_user_successfully), true);
                            setResult(RESULT_OK, intent);
                            finish();
                        } else {
                            doSignUp();
                        }
                    } else {
                        Objects.requireNonNull(pin_Code.getText()).clear();
                        Utils.customToastMsgError(VerifySmsCodeActivity.this, getResources().getString(R.string.invalid_code_verify), false);
                    }
                }

            }
        });

        findViewById(R.id.btn_submit_verify_code).setOnClickListener(new CustomSetOnClickViewListener(view -> doSignUp()));

    }

    private void countTime(){
        new CountDownTimer(180000, 1000) {
            public void onTick(long millisUntilFinished) {
                long min = (millisUntilFinished / 60000) % 60;
                long sec = (millisUntilFinished / 1000) % 60;
                countTimeTv.setText(String.format(Locale.US,"%s  : %s", min, sec));

                btnResent.setText(String.format(Locale.US, "%s %s : %s", getResources().getString(R.string.available_in), min, sec));
                btnResent.setEnabled(false);
            }

            public void onFinish() {
                btnResent.setText(R.string.resend_code);
                btnResent.setEnabled(true);
                btnResent.setOnClickListener(new CustomSetOnClickViewListener(view -> getNewPin()));
            }
        }.start();
    }

    private void doSignUp(){
        progressBar.setVisibility(View.VISIBLE);
        hashMapVerifyPin.putAll(MockUpData.getHashMapAccount());
        new SignUpWs().signUp(hashMapVerifyPin, isBookNowApp, new SignUpWs.SignUpInterface() {
            @Override
            public void onSuccess(String message, String pin_code, String user_id) {
                progressBar.setVisibility(View.GONE);
                Utils.customToastMsgError(VerifySmsCodeActivity.this, message, true);
                Intent intent = new Intent(VerifySmsCodeActivity.this, isBookNowApp ? LoginBookNowActivity.class : LoginActivity.class);
                startActivity(intent);
            }

            @Override
            public void onFailed(String mess) {
                progressBar.setVisibility(View.GONE);
                Objects.requireNonNull(pin_Code.getText()).clear();
                Utils.customToastMsgError(VerifySmsCodeActivity.this,mess, false);
            }

            @Override
            public void onError(String mess, int code) {
                progressBar.setVisibility(View.GONE);
                Objects.requireNonNull(pin_Code.getText()).clear();
                Utils.customToastMsgError(VerifySmsCodeActivity.this,mess, false);
            }
        });
    }

    private void getNewPin(){
        progressBar.setVisibility(View.VISIBLE);
        HashMap<String, Object> hashMap = isBookNowApp ? hashMapPinCode : MockUpData.getHashMapAccount();
        new SignUpWs().sendVerifyPinCode(hashMap, isBookNowApp, new SignUpWs.SignUpInterface() {
            @Override
            public void onSuccess(String message, String pin_code, String user_id) {
                progressBar.setVisibility(View.GONE);
                verifyPinCode = pin_code;
                countTime();
                Utils.customToastMsgError(VerifySmsCodeActivity.this, message, true);
            }

            @Override
            public void onFailed(String mess) {
                progressBar.setVisibility(View.GONE);
                Objects.requireNonNull(pin_Code.getText()).clear();
                Utils.customToastMsgError(VerifySmsCodeActivity.this,mess, false);
            }

            @Override
            public void onError(String mess, int code) {
                progressBar.setVisibility(View.GONE);
                Objects.requireNonNull(pin_Code.getText()).clear();
                Utils.customToastMsgError(VerifySmsCodeActivity.this,mess, false);
            }
        });
    }
}