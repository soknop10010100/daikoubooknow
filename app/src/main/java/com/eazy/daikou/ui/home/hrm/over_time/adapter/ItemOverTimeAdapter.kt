package com.eazy.daikou.ui.home.hrm.over_time.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.hr.OvertimeListModel
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class ItemOverTimeAdapter(private val context: Context, private val overTimeModelList: ArrayList<OvertimeListModel>, private val onClickCallBack : ClickCallBack) : RecyclerView.Adapter<ItemOverTimeAdapter.ItemViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.over_time_item_layout, parent, false)
        return ItemViewHolder(v)
    }

    @SuppressLint("NewApi")
    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
       val overtimeListModel: OvertimeListModel = overTimeModelList[position]
        if (overtimeListModel != null){

            if (overtimeListModel.id != null){
                holder.idOverTimeTv.text = overtimeListModel.id
            }else{
                holder.idOverTimeTv.text = ("- - - -")
            }
            if (overtimeListModel.created_at != null){
                holder.createDateTv.text =  Utils.formatDateTime(overtimeListModel.created_at, "yyyy-MM-dd kk:mm:ss", "dd-MMM-yyyy hh:mm a")
            }else{
                holder.createDateTv.text = ("- - -")
            }
            if (overtimeListModel.date != null){
                holder.dateTv.text = overtimeListModel.date
            }else{
                holder.dateTv.text =("- - - -")
            }

            if (overtimeListModel.begin_time != null){
                holder.startTimeTv.text = Utils.formatDateTime(overtimeListModel.begin_time, "kk:mm:ss", "hh:mm a")
            }else{
                holder.startTimeTv.text =("- - - -")
            }

            if (overtimeListModel.end_time != null){
                holder.endTimeTv.text = Utils.formatDateTime(overtimeListModel.end_time, "kk:mm:ss", "hh:mm a")
            }else{
                holder.endTimeTv.text =("- - - -")
            }

            if (overtimeListModel.user_name != null){
                holder.employeeNameTv.text = overtimeListModel.user_name
            }else{
                holder.employeeNameTv.text =("- - - -")
            }


            when (overtimeListModel.status) {
                "pending" -> {
                    holder.statusTv.text = Utils.getText(context, R.string.pending)
                    holder.statusTv.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(context, R.color.yellow))
                    holder.style.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(context, R.color.yellow))
                }
                "rejected" -> {
                    holder.statusTv.text = Utils.getText(context, R.string.rejected)
                    holder.statusTv.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(context, R.color.red))
                    holder.style.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(context, R.color.red))
                }
                "approved" ->{
                    holder.statusTv.text = Utils.getText(context, R.string.approved)
                    holder.statusTv.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(context, R.color.green))
                    holder.style.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(context, R.color.green))
                }
                else -> {
                    holder.statusTv.text = "- - -"
                    holder.statusTv.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(context, R.color.appBarColorOld))
                    holder.style.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(context, R.color.appBarColorOld))
                }
            }

            holder.itemView.setOnClickListener {
                onClickCallBack.onClickCallBack(overtimeListModel)
            }
        }

    }

    override fun getItemCount(): Int {
      return overTimeModelList.size
    }

    class ItemViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {
        val idOverTimeTv: TextView = itemView.findViewById(R.id.idOvertimeTv)
        val dateTv: TextView = itemView.findViewById(R.id.dateTv)
        val createDateTv: TextView = itemView.findViewById(R.id.createDateTv)
        val startTimeTv: TextView = itemView.findViewById(R.id.startTimeTv)
        val endTimeTv: TextView = itemView.findViewById(R.id.endTimeTv)
        val employeeNameTv: TextView = itemView.findViewById(R.id.EmployeeNameTv)
        val statusTv: TextView = itemView.findViewById(R.id.statusTv)
        val style: TextView = itemView.findViewById(R.id.style)
    }

    interface ClickCallBack{
        fun onClickCallBack(overtimeListModel: OvertimeListModel)
    }

    fun clear2() {
        val size: Int = overTimeModelList.size
        overTimeModelList.clear()
        notifyItemRangeRemoved(0, size)
    }

    fun clear() {
        val size: Int = overTimeModelList.size
        if (size > 0) {
            for (i in 0 until size) {
                overTimeModelList.removeAt(0)
            }
            notifyItemRangeRemoved(0, size)
        }
    }
}