package com.eazy.daikou.ui.home.my_property.property_service

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import androidx.activity.result.ActivityResult
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.databinding.ActivityPropertyServiceAllCategoryListBinding
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.facility_booking.FeeTypeModel
import com.eazy.daikou.model.my_property.property_service.MainCategoryServiceModel
import com.eazy.daikou.model.my_property.property_service.ServiceByCategoryModel
import com.eazy.daikou.model.profile.UnitNoModel
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.ui.home.my_property.property_service.adapter.MainCategoryServiceAdapter
import com.eazy.daikou.ui.home.my_property.property_service.adapter.PropertyServiceAvailableDayAdapter
import com.eazy.daikou.ui.home.my_property.property_service.adapter.SubServiceByCategoryAdapter
import com.eazy.daikou.ui.profile.SwitchUnitNoActivity

class PropertyServiceAllCategoryActivity : BaseActivity() {

    private lateinit var mBinding : ActivityPropertyServiceAllCategoryListBinding
    private lateinit var recyclerView : RecyclerView
    private var serviceList = ArrayList<MainCategoryServiceModel>()
    private lateinit var progressBar : ProgressBar
    private var serviceType = "time"
    private var unitId = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivityPropertyServiceAllCategoryListBinding.inflate(layoutInflater)
        setContentView(mBinding.root)

        initView()

        initAction()

    }

    private fun initView() {
        recyclerView = mBinding.itemLayout.recyclerView.recyclerView
        progressBar = mBinding.progressBar.progressItem

        Utils.customOnToolbar(this, resources.getString(R.string.service)) { finish() }
    }

    private fun initAction(){
        recyclerView.layoutManager = LinearLayoutManager(this)
        val propertyServiceAdapter = MainCategoryServiceAdapter(this, serviceList, onClickItemListener)
        recyclerView.adapter = propertyServiceAdapter

       requestListTest(propertyServiceAdapter)

        mBinding.itemLayout.swipeLayouts.setColorSchemeResources(R.color.colorPrimary)
        mBinding.itemLayout.swipeLayouts.setOnRefreshListener { requestListTest(propertyServiceAdapter) }

        // Service Type
        val recyclerView = mBinding.recyclerView.recyclerView
        recyclerView.layoutManager = GridLayoutManager(this, 3)
        var adapter: PropertyServiceAvailableDayAdapter? = null
        val list = PropertyServiceAvailableDayAdapter.addListRegService()
        adapter = PropertyServiceAvailableDayAdapter(this,false,
            list, object : PropertyServiceAvailableDayAdapter.ClickCallBackListener {
                @SuppressLint("NotifyDataSetChanged")
                override fun onClickCallBack(feeTypeModel: FeeTypeModel) {
                    for (subItem in list){
                        subItem.isClick = subItem.id == feeTypeModel.id
                    }
                    serviceType = feeTypeModel.id
                    adapter!!.notifyDataSetChanged()
                }
            })

        recyclerView.adapter = adapter

        mBinding.unitLayout.setOnClickListener( CustomSetOnClickViewListener {
            val intent = Intent(this, SwitchUnitNoActivity::class.java).apply {
                putExtra("id", unitId)
            }
            activityLauncher.launch(intent) { result: ActivityResult ->
                if (result.resultCode == RESULT_OK) {
                    if (result.data != null) {
                        if (result.data!!.hasExtra("unit_model")) {
                            val unitModel = result.data!!.getSerializableExtra("unit_model") as UnitNoModel
                            mBinding.unitTv.text = unitModel.name
                            unitId = unitModel.id!!
                        }
                    }
                }
            }
        } )
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun requestListTest(propertyServiceAdapter : MainCategoryServiceAdapter){
        serviceList.clear()
        serviceList.addAll(propertyServiceAdapter.addListRegService())
        propertyServiceAdapter.notifyDataSetChanged()

        mBinding.itemLayout.swipeLayouts.isRefreshing = false
        progressBar.visibility = View.GONE
    }

    private val onClickItemListener = object : SubServiceByCategoryAdapter.OnClickItemListener{
        override fun onClick(propertyService: ServiceByCategoryModel) {
            val intent = Intent(this@PropertyServiceAllCategoryActivity, PropertyServiceScheduleActivity::class.java).apply {
                putExtra("service_type", serviceType)
            }
            startActivity(intent)
        }

    }

}