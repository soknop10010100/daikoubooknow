package com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.cleaning_v2

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.helper.web.WebsitePropertyActivity
import com.eazy.daikou.model.my_property.cleaning.PaymentInfo
import com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.adapter_v2.PropertyServiceProviderInvoiceAdapter

class PropertyServicePaymentInvoiceActivity : BaseActivity() {

    private lateinit var recyclerViewInvoice: RecyclerView
    private lateinit var textNoDataTv: TextView
    private lateinit var adapterInvoiceList : PropertyServiceProviderInvoiceAdapter
    private var listPaymentInfo : ArrayList<PaymentInfo> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_property_service_payment_invoice)

        initView()

        initData()

        initAction()
    }

    private fun initView(){
        Utils.customOnToolbar(this,"Invoice List"){finish()}
        textNoDataTv = findViewById(R.id.textNoDataTv)
        recyclerViewInvoice = findViewById(R.id.recyclerViewInvoice)
    }

    private fun initData(){
        if (intent != null && intent.hasExtra("payment_info")){
            listPaymentInfo = intent.getSerializableExtra("payment_info") as ArrayList<PaymentInfo>
        }
    }
    private fun initAction(){

        textNoDataTv.visibility = if (listPaymentInfo.size > 0) View.GONE else View.VISIBLE

        val layoutManage = LinearLayoutManager(this)
        recyclerViewInvoice.layoutManager = layoutManage
        adapterInvoiceList = PropertyServiceProviderInvoiceAdapter(this, listPaymentInfo, callBackInvoice )
        recyclerViewInvoice.adapter = adapterInvoiceList

    }

    private val callBackInvoice: PropertyServiceProviderInvoiceAdapter.CallBackItemInvoiceListener = object : PropertyServiceProviderInvoiceAdapter.CallBackItemInvoiceListener{
        override fun callBackItem(invoicePayment: PaymentInfo) {
           val intent = Intent(this@PropertyServicePaymentInvoiceActivity, WebsitePropertyActivity::class.java)
            intent.putExtra("status", invoicePayment.payment_status)
            intent.putExtra("link_kess", invoicePayment.kess_payment_url)
            intent.putExtra("url_payment_scheduled", invoicePayment.invoice_url)
            startActivity(intent)

        }

    }
}