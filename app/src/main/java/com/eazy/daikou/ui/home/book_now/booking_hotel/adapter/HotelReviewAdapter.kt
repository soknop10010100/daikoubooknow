package com.eazy.daikou.ui.home.book_now.booking_hotel.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RatingBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.helper.DateUtil
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.booking_hotel.ReviewHotelModel
import de.hdodenhof.circleimageview.CircleImageView

class HotelReviewAdapter(private val historyList : ArrayList<ReviewHotelModel>) : RecyclerView.Adapter<HotelReviewAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.hotel_review_model, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBindingView(historyList[position])
    }

    override fun getItemCount(): Int {
        return historyList.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val imgProfile: CircleImageView = itemView.findViewById(R.id.profile_image)
        val nameTv : TextView = itemView.findViewById(R.id.nameTv)
        val dateTv : TextView = itemView.findViewById(R.id.dateTv)
        private val dateTimeTv : TextView = itemView.findViewById(R.id.dateTimeTv)
        val descriptionTv : TextView = itemView.findViewById(R.id.descriptionTv)
        val ratingStar : RatingBar = itemView.findViewById(R.id.ratingStar)
        private val textReviewTv : TextView = itemView.findViewById(R.id.textReviewTv)
        fun onBindingView(policiesModel: ReviewHotelModel){
            Utils.setValueOnText(descriptionTv, policiesModel.review)
            if (policiesModel.created_at != null){
                dateTv.text = DateUtil.formatDateComplaint(policiesModel.created_at)
                dateTimeTv.text = DateUtil.formatTimeServer(policiesModel.created_at)
            } else {
                dateTimeTv.text = "- - -"
                dateTv.text = "- - -"
            }

            val starRate: Float = if (policiesModel.star_rate != null && policiesModel.star_rate != ""){
                if (policiesModel.star_rate!!.toFloat() >= 0){
                    policiesModel.star_rate!!.toFloat()
                } else {
                    0.0F
                }
            } else {
                0.0F
            }
            if (starRate !=  0.0F){
                textReviewTv.text = starRate.toString()
            } else {
                textReviewTv.visibility = View.GONE
            }
            ratingStar.rating = starRate

            if (policiesModel.user != null){
                Utils.setValueOnText(nameTv, policiesModel.user!!.name)

                Glide.with(imgProfile).load(if (policiesModel.user!!.image != null) policiesModel.user!!.image else R.drawable.ic_my_profile).into(imgProfile)
            } else {
                Utils.setValueOnText(nameTv, "- - -")

                Glide.with(imgProfile).load(R.drawable.ic_my_profile).into(imgProfile)

            }
        }
    }
}