package com.eazy.daikou.ui.contact;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.eazy.daikou.base.BaseFragment;
import com.eazy.daikou.request_data.request.home_ws.ContactWs;
import com.eazy.daikou.helper.AppAlertCusDialog;
import com.eazy.daikou.helper.UserSessionManagement;
import com.eazy.daikou.model.contact.FriendContactModel;
import com.eazy.daikou.R;
import com.eazy.daikou.ui.contact.adapter.DiakouFriendsContactAdapter;
import com.eazy.daikou.ui.home.parking.ScanParkActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DaiKuFragment extends BaseFragment {

    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private static final int REQUEST_SCAN_CODE = 974;
    private TextView noItemTv;
    private SwipeRefreshLayout refreshLayout;
    private LinearLayoutManager linearLayoutManager;
    private int currentItem, total , scrollDown, currentPage = 1 ,size = 10;
    private boolean isScrolling;
    private final List<FriendContactModel> friendContactModelList = new ArrayList<>();
    private DiakouFriendsContactAdapter diakouFriendsContactAdapter;

    public DaiKuFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dai_kufragment,container,false);

        initView(view);

        initData();

        initAction();

        return view;
    }

    private void initView(View view){
        recyclerView = view.findViewById(R.id.recyclerView);
        progressBar = view.findViewById(R.id.progressItem);
        noItemTv = view.findViewById(R.id.txtNoResult);
        refreshLayout = view.findViewById(R.id.refreshScreen);
    }

    private void initData(){
//        ((MainActivity) requireActivity()).btnAddToolbar.setVisibility(View.VISIBLE);
//        ((MainActivity) requireActivity()).btnAddToolbar.setImageDrawable(ContextCompat.getDrawable(mActivity, R.drawable.fab_add));
//        ((MainActivity) requireActivity()).btnAddToolbar.setOnClickListener(v -> startScannerActivity());
//
//        // User Guest
//        if (MockUpData.isUserAsGuest(new UserSessionManagement(mActivity))) {
//            ((MainActivity) requireActivity()).toolbar.setVisibility(View.VISIBLE);
//            ((MainActivity) requireActivity()).imageSlider.setVisibility(View.GONE);
//        }
        ContactFragment.btnAdd.setVisibility(View.VISIBLE);
        ContactFragment.btnAdd.setOnClickListener(view -> startScannerActivity());
    }

    private void initAction(){
        refreshLayout.setColorSchemeResources(R.color.colorPrimary);
        refreshLayout.setOnRefreshListener(this::refreshList);

        linearLayoutManager = new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(linearLayoutManager);

        initRecyclerView();
        onScrollFriendContact();
    }

    private void refreshList(){
        currentPage = 1;
        size = 10;
        isScrolling = true;
        if (diakouFriendsContactAdapter != null)    diakouFriendsContactAdapter.clear();

        requestListService();
    }

    private void startScannerActivity(){
        Intent intent = new Intent(mActivity, ScanParkActivity.class);
        intent.putExtra("action", "scan_add_friend");
        startActivityForResult(intent, REQUEST_SCAN_CODE);
    }

    void initRecyclerView(){
        diakouFriendsContactAdapter = new DiakouFriendsContactAdapter(friendContactModelList, mActivity);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(diakouFriendsContactAdapter);

        requestListService();
    }

    private void onScrollFriendContact(){
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL){
                    isScrolling = true;
                }
            }
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                currentItem = linearLayoutManager.getChildCount();
                total = linearLayoutManager.getItemCount();
                scrollDown = linearLayoutManager.findFirstVisibleItemPosition();
                if(dy > 0) {
                    if (isScrolling && (currentItem + scrollDown == total)) {
                        if(total == size) {
                            isScrolling = false;
                            currentPage++;
                            progressBar.setVisibility(View.VISIBLE);
                            recyclerView.scrollToPosition(friendContactModelList.size() - 1);
                            initRecyclerView();
                            size+=10;
                        }else {
                            progressBar.setVisibility(View.GONE);
                        }
                    }
                }

            }
        });
    }

    private void requestListService(){
        progressBar.setVisibility(View.VISIBLE);
        new ContactWs().getListContactFriend(mActivity, new UserSessionManagement(mActivity).getUserId(), currentPage, 10, callBackListener);
    }


    private void requestScanQrCodeAddFriend(String friendUserId){
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("user_id", new UserSessionManagement(mActivity).getUserId());
        hashMap.put("friend_user_id", friendUserId);
        progressBar.setVisibility(View.VISIBLE);
        new ContactWs().addFriendContact(mActivity, hashMap, callBackListener);
    }

    private final ContactWs.CallBackListener callBackListener = new ContactWs.CallBackListener() {
        @SuppressLint("NotifyDataSetChanged")
        @Override
        public void onListFriendsSuccess(List<FriendContactModel> friendContactModels) {
            progressBar.setVisibility(View.GONE);
            refreshLayout.setRefreshing(false);

            friendContactModelList.addAll(friendContactModels);
            diakouFriendsContactAdapter.notifyDataSetChanged();

            noItemTv.setVisibility(friendContactModelList.size() == 0 ? View.VISIBLE : View.GONE);
            recyclerView.setVisibility(friendContactModelList.size() > 0 ? View.VISIBLE : View.GONE);

        }

        @Override
        public void onSuccess(String msgSuccess) {
            progressBar.setVisibility(View.GONE);
            refreshLayout.setRefreshing(false);
            refreshList();
            Toast.makeText(mActivity, msgSuccess, Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onFailed(String error) {
            progressBar.setVisibility(View.GONE);
            refreshLayout.setRefreshing(false);
            AppAlertCusDialog app = new AppAlertCusDialog();
            app.showDialog(mActivity, error);

        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_SCAN_CODE && data != null){
            requestScanQrCodeAddFriend(data.getStringExtra("result_scan"));
        }
    }
}