package com.eazy.daikou.ui.home.my_property.my_unit

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AbsListView
import android.widget.ProgressBar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseFragment
import com.eazy.daikou.request_data.request.my_property_ws.payment_ws.PaymentWs
import com.eazy.daikou.helper.CustomStartIntentUtilsClass.Companion.onStartPaymentInvoiceActivity
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.my_property.payment_invoice.PaymentInvoiceModel
import com.eazy.daikou.ui.home.my_property.payment.adapter.MyUnitPaymentAdapter

class PropertyPaymentItemFragment : BaseFragment() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var paymentNotificationAdapter: MyUnitPaymentAdapter
    private lateinit var progressBar: ProgressBar
    private lateinit var refreshLayout: SwipeRefreshLayout
    private lateinit var linearLayoutManager: LinearLayoutManager

    private var paymentInvoiceList: ArrayList<PaymentInvoiceModel> = ArrayList()
    private var currentItem = 0
    private var total : Int = 0
    private var scrollDown : Int = 0
    private var currentPage : Int = 1
    private var size : Int = 10
    private var isScrolling = false
    private var propertyId : String = ""
    private var category : String = ""

    companion object {
        @JvmStatic
        fun newInstance(categoryType : String, propertyId: String) =
            PropertyPaymentItemFragment().apply {
                arguments = Bundle().apply {
                    putString("property_id", propertyId)
                    putString("category", categoryType)
                }
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            propertyId = it.getString("property_id").toString()
            category = it.getString("category").toString()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_property_payment_item, container, false)

        initView(view)

        initAction()

        return view

    }

    private fun initView(roof : View){
        progressBar = roof.findViewById(R.id.progressItem)
        refreshLayout = roof.findViewById(R.id.swipe_layouts)
        recyclerView = roof.findViewById(R.id.recyclerView)
    }

    private fun initAction(){
        refreshLayout.setColorSchemeResources(R.color.colorPrimary)
        refreshLayout.setOnRefreshListener { refreshListItem() }

        linearLayoutManager = LinearLayoutManager(mActivity)
        recyclerView.layoutManager = linearLayoutManager

        initRecyclerView()

        initOnScroll()

    }

    private fun refreshListItem(){
        currentPage = 1
        size = 10
        paymentNotificationAdapter.clear()

        requestListPayment()
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun requestListPayment(){
        progressBar.visibility = View.VISIBLE
        PaymentWs().getListPaymentWs(mActivity, currentPage, 10, category, propertyId, onCallBackLister)
    }

    private val onCallBackLister = object : PaymentWs.CallBackListener{
        @SuppressLint("NotifyDataSetChanged")
        override fun onSuccess(paymentList: ArrayList<PaymentInvoiceModel>) {
            progressBar.visibility = View.GONE
            refreshLayout.isRefreshing = false
            paymentInvoiceList.addAll(paymentList)

            paymentNotificationAdapter.notifyDataSetChanged()

            Utils.validateViewNoItemFound(mActivity, recyclerView, paymentInvoiceList.size <= 0)
        }

        override fun onFailed(error: String) {
            progressBar.visibility = View.GONE
            refreshLayout.isRefreshing = false
            Utils.customToastMsgError(mActivity, error, false)
        }

    }

    private fun initRecyclerView(){
        paymentNotificationAdapter = MyUnitPaymentAdapter(onClickPaymentListener, mActivity, paymentInvoiceList)
        recyclerView.adapter = paymentNotificationAdapter

        requestListPayment()
    }

    private val onClickPaymentListener = MyUnitPaymentAdapter.ItemClickNotification { paymentModel, _ -> openWebView(paymentModel) }

    private fun openWebView(paymentModel: PaymentInvoiceModel) {
        onStartPaymentInvoiceActivity(mActivity, paymentModel.paymentStatus, paymentModel.invoiceReceiptUrl, paymentModel.linkPaymentKess)
    }

    private fun initOnScroll() {
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true
                }
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                currentItem = linearLayoutManager.childCount
                total = linearLayoutManager.itemCount
                scrollDown = linearLayoutManager.findFirstVisibleItemPosition()
                if (dy > 0) {
                    if (isScrolling && currentItem + scrollDown == total) {
                        if (total == size) {
                            isScrolling = false
                            currentPage++
                            progressBar.visibility = View.VISIBLE
                            recyclerView.scrollToPosition(paymentInvoiceList.size - 1)
                            initRecyclerView()
                            size += 10
                        }
                    }
                }
            }
        })
    }

}