package com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.ColorStateList;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.eazy.daikou.R;
import com.eazy.daikou.helper.Utils;
import com.eazy.daikou.model.my_property.service_provider.ServiceTypeModel;

import java.util.ArrayList;
import java.util.Locale;

public class CategoryServiceAdapter extends RecyclerView.Adapter<CategoryServiceAdapter.ViewHolder> {

    private final ArrayList<ServiceTypeModel> menuServiceProviders;
    private final ItemClickCategoryServiceProvider itemClick;
    private final Context context;

    public CategoryServiceAdapter(ArrayList<ServiceTypeModel> menuServiceProviders, Context context, ItemClickCategoryServiceProvider itemClick) {
        this.menuServiceProviders = menuServiceProviders;
        this.itemClick = itemClick;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view  = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_menu_top_model,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        ServiceTypeModel listService = menuServiceProviders.get(position);
        if(listService!=null){
            holder.nameCategoryTv.setText(String.format(Locale.US,"%s",listService.getServiceCategory()));
            if (listService.isClick()){
                holder.nameCategoryTv.setTextColor(Utils.getColor(context, R.color.white));
                holder.linearLayout.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(context, R.color.color_gray_item)));
            } else {
                holder.linearLayout.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(context, R.color.color_un_select_item)));
                holder.nameCategoryTv.setTextColor(Utils.getColor(context, R.color.gray));
            }

            holder.itemView.setOnClickListener(view -> itemClick.onClickItem(listService.getServiceCategoryKey()));
        }

    }

    @Override
    public int getItemCount() {
        return menuServiceProviders.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView nameCategoryTv;
        private final CardView linearLayout;
        public ViewHolder(View view) {
            super(view);
            linearLayout = view.findViewById(R.id.linear_card);
            nameCategoryTv = view.findViewById(R.id.text_category);
        }


    }

    public interface ItemClickCategoryServiceProvider{
        void onClickItem(String listString);
    }
}