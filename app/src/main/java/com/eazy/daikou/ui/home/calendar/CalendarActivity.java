package com.eazy.daikou.ui.home.calendar;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.Toast;

import com.eazy.daikou.R;
import com.eazy.daikou.ui.home.calendar.adapter.CalendarListAdapter;
import com.eazy.daikou.base.BaseActivity;
import com.eazy.daikou.helper.YearView;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;

public class CalendarActivity extends BaseActivity {

    private final static int MIN_YEAR = 1945;
    private final static int MAX_YEAR = 3045;
    private RecyclerView recyclerView;
    private CalendarListAdapter calendarListAdapter;
    private ImageView backBtn, addCalBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar_acitivity);

        initView();

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        calendarListAdapter = new CalendarListAdapter(buildListOfYears(),calendarCallback);
        recyclerView.setAdapter(calendarListAdapter);

        selectFirstDisplayedYear();

        backBtn.setOnClickListener(view -> onBackPressed());

        addCalBtn.setOnClickListener(view -> {
//            startActivity( new Intent(CalendarActivity.this , CalendarAddEventActivity.class));
            Toast.makeText(this, getResources().getString(R.string.add_event) + " " +
                          getResources().getString(R.string.coming_soon), Toast.LENGTH_SHORT).show();
        });

    }

    private void initView(){
        backBtn = findViewById(R.id.iconBack);
        addCalBtn = findViewById(R.id.btnAddCalendar);
        YearView yearView = findViewById(R.id.yearView);
        recyclerView = findViewById(R.id.recyclerViewCal);
    }
    private final CalendarListAdapter.CalendarCallback calendarCallback = timeInMillis -> {
        DateTime dateTime = new DateTime(timeInMillis);
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM");
        long time = dateTime.getMillis();
        popupInform(time);
    };

    private ArrayList<Integer> buildListOfYears() {
        ArrayList<Integer> listYears = new ArrayList<>();
        for(int i = MIN_YEAR ; i <= MAX_YEAR;  i++ ){
            listYears.add(i);
        }
        return listYears;
    }

    private void selectFirstDisplayedYear(){
        if(calendarListAdapter != null){
            int defaultYear = new DateTime().getYear();
            int desiredYearPosition = calendarListAdapter.getYearPosition(defaultYear);
            if(desiredYearPosition > -1)
                recyclerView.scrollToPosition(desiredYearPosition);
        }
    }

    private void popupInform(long time) {
        Intent intent = new Intent(CalendarActivity.this, CalendarDetailActivity.class);
        intent.putExtra("time",time);
        startActivity(intent);
    }



}