package com.eazy.daikou.ui.home.service_provider_new.activity

import android.content.Intent
import android.os.Bundle
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.service_provider.TabScrollServiceProviderModel
import com.eazy.daikou.ui.home.hrm.leave_request.adapter.TabsLeaveAdapter
import com.google.android.material.tabs.TabLayout

class ServiceProviderCompanyDetailActivity : BaseActivity() {

    private lateinit var recyclerViewTabText: RecyclerView
    private lateinit var progressBar: ProgressBar
    private lateinit var moreInfoTv: TextView
    private lateinit var tabScrollTB : TabLayout
    private lateinit var viewpager : ViewPager2
    private val isUserScrolling = false
    private var tabList: ArrayList<TabScrollServiceProviderModel> = ArrayList()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_service_provider_company_detail)

        initView()

        initAction()
    }

    private fun initView(){
        tabScrollTB = findViewById(R.id.tabScrollTB)
        moreInfoTv = findViewById(R.id.moreInfoTv)
        //viewpager = findViewById(R.id.viewpager)
    }

    private fun initAction(){

        moreInfoTv.setOnClickListener {
            val intent = Intent(this@ServiceProviderCompanyDetailActivity, MoreInfoCompanyActivity::class.java)
            startActivity(intent)}

        tabList.add(TabScrollServiceProviderModel("001","Service Provider 1"))
        tabList.add(TabScrollServiceProviderModel("002","Service Provider 2"))
        tabList.add(TabScrollServiceProviderModel("003","Service Provider 3"))
        tabList.add(TabScrollServiceProviderModel("004","Service Provider 4"))
        tabList.add(TabScrollServiceProviderModel("005","Service Provider 5"))
        tabList.add(TabScrollServiceProviderModel("006","Service Provider 6"))

        initTabLayout()
    }

    private fun initTabLayout(){
        val adapter = TabsLeaveAdapter(supportFragmentManager)

        for (category in tabList){
            tabScrollTB.addTab(tabScrollTB.newTab().setText(category.nameCategory))
        }

        tabScrollTB.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener{
            override fun onTabSelected(tab: TabLayout.Tab?) {
                Utils.customToastMsgError(this@ServiceProviderCompanyDetailActivity, "service${tab!!.id}", false)
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
            }

        })
    }
}