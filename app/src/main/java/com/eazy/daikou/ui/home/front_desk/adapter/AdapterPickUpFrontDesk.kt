package com.eazy.daikou.ui.home.front_desk.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.model.front_desk.SearchFrontDeskModel
import de.hdodenhof.circleimageview.CircleImageView

class AdapterPickUpFrontDesk (private val context : Context, private val listAssign : ArrayList<SearchFrontDeskModel>, private val callBackListener: CallBackClickItemAssign): RecyclerView.Adapter<AdapterPickUpFrontDesk.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.layout_pick_up_item_front_desk, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val assign = listAssign[position]
        if (assign != null){
            if (assign.image != null){
                Glide.with(holder.iconUser).load(assign.image).into(holder.iconUser)
            } else{
                Glide.with(holder.iconUser).load(R.drawable.no_image).into(holder.iconUser)
            }
            holder.namePickUpTv.text = if ( assign.name != null) assign.name else "- - -"
            holder.phoneTv.text = if (assign.phone != null) assign.phone else " - - -"

            holder.itemView.setOnClickListener { callBackListener.clickItemAssignListener(assign) }
        }
    }

    override fun getItemCount(): Int {
       return listAssign.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view){
        var namePickUpTv: TextView = view.findViewById(R.id.namePickUpTv)
        var phoneTv: TextView = view.findViewById(R.id.phoneTv)
        var iconUser: CircleImageView = view.findViewById(R.id.iconUser)

    }

    interface CallBackClickItemAssign{
        fun clickItemAssignListener(assignModel :SearchFrontDeskModel)
    }
}