package com.eazy.daikou.ui.home.my_property.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.my_property.payment_schedule_my_unit.DeveloperInfo
import de.hdodenhof.circleimageview.CircleImageView

class DeveloperPaymentAdapter (private val listPaymentModel: ArrayList<DeveloperInfo>):
    RecyclerView.Adapter<DeveloperPaymentAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.sub_body_content_developer, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val paymentsModel : DeveloperInfo = listPaymentModel[position]
        if (paymentsModel != null){
            if (paymentsModel.photo != null){
                Glide.with(holder.imageDeveloper).load(paymentsModel.photo).into(holder.imageDeveloper)
            }
            Utils.setValueOnText(holder.phoneTv, paymentsModel.phone)
            Utils.setValueOnText(holder.developerNameTv, paymentsModel.name)
            Utils.setValueOnText(holder.emailTv, paymentsModel.email)
            Utils.setValueOnText(holder.countryTv, paymentsModel.country_name)
            Utils.setValueOnText(holder.positionTv, paymentsModel.contact_type)
        }
    }

    override fun getItemCount(): Int {
        return listPaymentModel.size
    }

    class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView){
        var imageDeveloper : CircleImageView = itemView.findViewById(R.id.imageDeveloper)
        var phoneTv : TextView = itemView.findViewById(R.id.phoneTv)
        var developerNameTv : TextView = itemView.findViewById(R.id.developerNameTv)
        var emailTv : TextView = itemView.findViewById(R.id.emailTv)
        var countryTv : TextView = itemView.findViewById(R.id.countryTv)
        var positionTv : TextView = itemView.findViewById(R.id.positionTv)
    }

}