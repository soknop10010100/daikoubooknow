package com.eazy.daikou.ui.home.utility_tracking;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.anychart.AnyChart;
import com.anychart.AnyChartView;
import com.anychart.chart.common.dataentry.DataEntry;
import com.anychart.chart.common.dataentry.ValueDataEntry;
import com.anychart.charts.Cartesian;
import com.anychart.core.cartesian.series.Line;
import com.anychart.data.Mapping;
import com.anychart.data.Set;
import com.anychart.enums.Anchor;
import com.anychart.enums.MarkerType;
import com.anychart.enums.TooltipPositionMode;
import com.anychart.graphics.vector.Stroke;
import com.eazy.daikou.R;
import com.eazy.daikou.base.BaseActivity;
import com.eazy.daikou.request_data.request.service_property_ws.MyUnitPropertyWs;
import com.eazy.daikou.request_data.request.track_water_ws.TrackWaterElectricityWs;
import com.eazy.daikou.helper.DateNumPickerFragment;
import com.eazy.daikou.helper.UserSessionManagement;
import com.eazy.daikou.helper.Utils;
import com.eazy.daikou.model.utillity_tracking.model.UnitImgModel;
import com.eazy.daikou.model.profile.User;
import com.eazy.daikou.ui.home.utility_tracking.adapter.TableWaterElectricAdapter;
import com.eazy.daikou.model.utillity_tracking.model.ListUsageW_EModel;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class WaterElectricityUsageActivity extends BaseActivity implements View.OnClickListener {

    private TextView txtUnitNo, txtMonth, txtYear, noItem;
    private UserSessionManagement sessionManagement;
    private User user;
    private ProgressBar progressBar;
    private ListUsageW_EModel ownerUsageList;
    private RecyclerView tableRecycleView;
    private ScrollView scrollViewLayout;

    private final List<UnitImgModel> myUnitList = new ArrayList<>();

    private String action, unitNoId = "", month = "01", year = "";
    private boolean isFirstUnit = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_water_electricity_usage);

        initView();
        initData();
        initAction();

    }

    private void initView() {
        txtUnitNo = findViewById(R.id.unitTxt);
        txtMonth = findViewById(R.id.monthTxt);
        txtYear = findViewById(R.id.yearTxt);
        progressBar = findViewById(R.id.progress);
        progressBar.setVisibility(View.GONE);
        tableRecycleView = findViewById(R.id.tableRecycle);
        noItem = findViewById(R.id.noItem);
        scrollViewLayout = findViewById(R.id.scrollViewLayout);
        scrollViewLayout.setVisibility(View.GONE);

        txtUnitNo.setOnClickListener(this);
        txtMonth.setOnClickListener(this);
        txtYear.setOnClickListener(this);
    }

    private void initData() {
        sessionManagement = new UserSessionManagement(this);
        Gson gson = new Gson();
        user = gson.fromJson(sessionManagement.getUserDetail(), User.class);

        if (getIntent().hasExtra("action")) {
            action = getIntent().getStringExtra("action");
            TextView titleToolBar = findViewById(R.id.titleToolbar);
            if (action.equals("water")) {
                titleToolBar.setText(getString(R.string.water_record));
            } else {
                titleToolBar.setText(getString(R.string.electricity_record));
            }
        }
    }

    private void selectUnitNo() {
        progressBar.setVisibility(View.VISIBLE);
        new MyUnitPropertyWs().getServiceMyUnitV2(this, Utils.validateNullValue(user.getAccountId()), user.getActivePropertyIdFk(),
                1, 100, sessionManagement.getUserId(), callBackListener);
    }

    private void selectMonthYear(String key) {
        DateNumPickerFragment monthDialog = DateNumPickerFragment.newInstance(key);
        monthDialog.CallBackListener(callBackDateListener);
        monthDialog.show(getSupportFragmentManager(), "Dialog Fragment");
    }

    private final DateNumPickerFragment.ClickCallBackListener callBackDateListener = new DateNumPickerFragment.ClickCallBackListener() {
        @Override
        public void doneItemClick(String title, String date, String id) {
            if (title.equalsIgnoreCase("Month")) {
                txtMonth.setText(date);
                String numDate;
                if (id.length() == 2) {
                    numDate = id;
                } else {
                    numDate = "0".concat(id);
                }
                month = numDate;
            } else if (title.equalsIgnoreCase("Year")) {
                txtYear.setText(date);
                year = date;
            }
            requestServiceListE_WUsage();
        }
    };

    private final MyUnitPropertyWs.MyUnitCallBackListener callBackListener = new MyUnitPropertyWs.MyUnitCallBackListener() {
        @Override
        public void getServiceUnit(ArrayList<UnitImgModel> unitImgModels) {
            myUnitList.addAll(unitImgModels);
            if (unitImgModels.size() == 0) {
                progressBar.setVisibility(View.GONE);
                noItem.setVisibility(View.VISIBLE);
                scrollViewLayout.setVisibility(View.GONE);

            } else {
                if (isFirstUnit){
                    unitNoId = unitImgModels.get(0).getUnitId();
                    txtUnitNo.setText(unitImgModels.get(0).getUnitNo());
                    isFirstUnit = false;
                    requestServiceListE_WUsage();
                } else {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(WaterElectricityUsageActivity.this, "coming soon . . .", Toast.LENGTH_SHORT).show();
                }
            }
        }

        @Override
        public void failed(String error) {
            progressBar.setVisibility(View.GONE);
            Toast.makeText(WaterElectricityUsageActivity.this, error, Toast.LENGTH_SHORT).show();
        }
    };

    private void initRecycleView(){
        tableRecycleView.setLayoutManager(new LinearLayoutManager(this));
        tableRecycleView.setAdapter(new TableWaterElectricAdapter(ownerUsageList.getTrackingData()));
    }

    @SuppressLint("SetTextI18n")
    private void calculateUsage(){

        TextView totalUsage = findViewById(R.id.totalUsage);
        TextView startDate = findViewById(R.id.fromDate);
        TextView endDate = findViewById(R.id.endDate);
        TextView totalMoney = findViewById(R.id.totalMoney);

        String fromDate = null, toEndDate = null;
        if (ownerUsageList.getTotalUsage() != null) {
            if (ownerUsageList.getTotalUsage().getFromDate() != null || ownerUsageList.getTotalUsage().getToDate() != null) {
                fromDate = ownerUsageList.getTotalUsage().getFromDate();
                toEndDate = ownerUsageList.getTotalUsage().getToDate();
            }
        }

        if (fromDate == null || toEndDate == null){
            fromDate = year + "-" + month + "-" + "01";
            toEndDate = year + "-" + month + "-" + getDaysOfMonth(Integer.parseInt(month), Integer.parseInt(year)) + "";
        }

        startDate.setText(Utils.formatDateFromString("yyyy-MM-dd", "dd MMM yyyy", fromDate));
        endDate.setText(Utils.formatDateFromString("yyyy-MM-dd", "dd MMM yyyy", toEndDate));
        String totalCash;
        if (ownerUsageList.getTotalUsage() != null) {
            if (ownerUsageList.getTotalUsage().getTotalUsage() != null) {
                totalUsage.setText(ownerUsageList.getTotalUsage().getTotalUsage() + " KWH");
                totalCash = ownerUsageList.getTotalUsage().getTotalPrice();
            } else {
                totalUsage.setText("0 KWH");
                totalCash = "0";
            }
        }else {
            totalUsage.setText("0 KWH");
            totalCash = "0";
        }

        //Calculate Total Usage Money
        totalMoney.setText("$ " + totalCash);
    }

    private int getDaysOfMonth(int month, int year) {
        int daysInMonth ;
        if (month == 4 || month == 6 || month == 9 || month == 11) {
            daysInMonth = 30;
        } else {
            if (month == 2) {
                daysInMonth = (year % 4 == 0) && !(year % 100 == 0) || (year % 400 == 0) ? 29 : 28;
            } else {
                daysInMonth = 31;
            }
        }
        return daysInMonth;
    }

    private void initAction() {
        findViewById(R.id.iconBack).setOnClickListener(view -> finish());
        ownerUsageList = new ListUsageW_EModel();

        selectUnitNo();
        Calendar cal = Calendar.getInstance();
        year = cal.get(Calendar.YEAR) + "";
        txtYear.setText(year);
        txtMonth.setText(getResources().getString(R.string.january));
    }

    private void drawChart(){
        AnyChartView anyChartView = findViewById(R.id.any_chart_view);
        anyChartView.setProgressBar(progressBar);

        Cartesian cartesian = AnyChart.line();
        cartesian.animation(true);
        cartesian.padding(10d, 20d, 5d, 20d);

        //Set click graph
        cartesian.crosshair().enabled(true);
        cartesian.crosshair()
                .yLabel(true)
                .yStroke( (Stroke) null, null, null, (String) null, (String) null);
        cartesian.tooltip().positionMode(TooltipPositionMode.POINT);

        //Set title Graph on xAxis and yAxis
        cartesian.xAxis(0).labels().padding(10d, 10d, 10d, 10d);
//        cartesian.xAxis(0).title(getResources().getString(R.string.day));
//        cartesian.yAxis(0).title(getResources().getString(R.string.total_usage));

        List<DataEntry> seriesData = addList(ownerUsageList.getTrackingData());
        Set set = Set.instantiate();
        set.data(seriesData);

        //Draw grape and hover
        Mapping series1Mapping = set.mapAs("{ x: 'x', value: 'value' }");
        Line series1 = cartesian.line(series1Mapping);
        series1.name(getResources().getString(R.string.total_usage));
        series1.hovered().markers().enabled(true);
        series1.hovered().markers()
                .type(MarkerType.CIRCLE)
                .size(4d);
        series1.tooltip()
                .position("right")
                .anchor(Anchor.CENTER);
        cartesian.legend().enabled(true);
        cartesian.legend().fontSize(13d);
        cartesian.legend().padding(0d, 0d, 10d, 0d);

        anyChartView.setChart(cartesian);
    }

    private void requestServiceListE_WUsage(){
        progressBar.setVisibility(View.VISIBLE);
        new TrackWaterElectricityWs().getWaterElectricOwnerUsage(WaterElectricityUsageActivity.this, action, user.getActivePropertyIdFk(),
                unitNoId, month, year, new TrackWaterElectricityWs.OnCallBackUsageElectricWater(){
                    @Override
                    public void onSuccess(ListUsageW_EModel listUsageW_eModels) {
                        ownerUsageList = listUsageW_eModels;
                        if (ownerUsageList.getTrackingData().size() <= 0){
                            noItem.setVisibility(View.VISIBLE);
                            scrollViewLayout.setVisibility(View.GONE);
                        } else {
                            noItem.setVisibility(View.GONE);
                            scrollViewLayout.setVisibility(View.VISIBLE);
                            drawChart();
                            initRecycleView();
                            calculateUsage();
                        }
                    }

                    @Override
                    public void onFail(String message) {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(WaterElectricityUsageActivity.this, message, Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private List<DataEntry> addList(List<ListUsageW_EModel.TrackingDatum> list){
        List<DataEntry> seriesData = new ArrayList<>();
        if (list.size() > 0 ){
            for (int i = 0; i < list.size(); i++){
                String date = list.get(i).getTrackingDate().substring(8);
                seriesData.add(new CustomDataEntry(date, Double.parseDouble(list.get(i).getTotalUsage())));
            }
        }
        return seriesData;
    }


    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.unitTxt:
                 selectUnitNo();
                 break;
            case R.id.monthTxt:
                 selectMonthYear("MONTH");
                 break;
            case R.id.yearTxt:
                 selectMonthYear("YEAR");
                 break;

        }
    }

    //Create new Subclass from ValueDataEntry
    private static class CustomDataEntry extends ValueDataEntry {
        CustomDataEntry(String x, Number value) {
            super(x, value);
        }
    }
}