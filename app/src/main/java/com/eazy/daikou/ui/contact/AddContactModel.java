package com.eazy.daikou.ui.contact;

public class AddContactModel {

    String firstName;
    String lastName;
    String email;
    String number;
    String imageUrl;
    int userId;

    public AddContactModel(String firstName, String lastName, String email, String number, String imageUrl, int userId) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.number = number;
        this.imageUrl = imageUrl;
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getUserId() {
        return userId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public String toString() {
        return firstName;
//        return "AddContactModel{" +
//                "firstName='" + firstName + '\'' +
//                ", lastName='" + lastName + '\'' +
//                ", email='" + email + '\'' +
//                ", number='" + number + '\'' +
//                ", imageUrl='" + imageUrl + '\'' +
//                '}';
    }
}
