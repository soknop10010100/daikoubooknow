package com.eazy.daikou.ui.home.parking

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.request_data.request.parking_ws.ParkingWs
import com.eazy.daikou.model.parking.AllComment
import com.eazy.daikou.model.parking.Parking
import com.eazy.daikou.ui.home.parking.adapter.ListAllCommentAdapter
import java.util.*
import kotlin.collections.HashMap

class ListAllCommentParkingActivity : BaseActivity() {

    private lateinit var parking : Parking
    private lateinit var btnAccept : CardView
    private lateinit var cardText : CardView
    private lateinit var cardList : CardView
    private lateinit var textNotification : TextView
    private lateinit var textNoTv :TextView

    private val listComment: MutableList<AllComment> = ArrayList<AllComment>()
    private var message = ""

    @SuppressLint("SetTextI18n", "NotifyDataSetChanged")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_all_comment_parking)

        val title : TextView = findViewById(R.id.titleToolbar)
        title.text = getString(R.string.list_all_comment)

        val btnBack : ImageView = findViewById(R.id.iconBack)
        btnBack.setOnClickListener { finish() }


        val recyclerView : RecyclerView = findViewById(R.id.list_comment)
        cardText = findViewById(R.id.card_notification)
        textNotification = findViewById(R.id.text_notification)
        btnAccept = findViewById(R.id.card_accept)
        textNoTv = findViewById(R.id.text_no)
        cardList = findViewById(R.id.card_list_comment)


        if(intent.hasExtra("parking")){
            parking = intent.getSerializableExtra("parking") as Parking
        }
        if(intent.hasExtra("message")){
            message = intent.getStringExtra("message") as String
        }

        if(message.isNotEmpty() ){
            btnAccept.visibility = View.VISIBLE
            textNotification.visibility = View.VISIBLE
        } else {
            cardText.visibility = View.GONE
            btnAccept.visibility = View.GONE
            textNotification.visibility = View.GONE
        }


        if(parking.parkingAlertHistory != null && parking.parkingAlertHistory.size!=0){
            cardText.visibility = View.VISIBLE
            btnAccept.visibility = View.VISIBLE
            textNotification.visibility = View.VISIBLE
            for ( message in parking.parkingAlertHistory){
                textNotification.append((( message.message + "\n")))
            }
        } else {
            cardText.visibility = View.GONE
            btnAccept.visibility = View.GONE
            textNotification.visibility = View.GONE
        }

        if(parking.allDocuments.size != 0){
            for (comment in parking.allDocuments) {
                if (comment.comment.isNotEmpty() && comment.allImages.size != 0) {
                    listComment.add(comment)
                }
            }
        }

        //add Comment
        if(listComment.size != 0){
            val linearLayoutManager = LinearLayoutManager(this)
            recyclerView.layoutManager = linearLayoutManager
            val adapter = ListAllCommentAdapter(listComment)
            recyclerView.adapter = adapter
            adapter.notifyDataSetChanged()
        } else {
            textNoTv.visibility = View.VISIBLE
            cardList.visibility = View.GONE
        }

        btnAccept.setOnClickListener {
            acceptNotification(parking.id)
        }
    }

    private fun acceptNotification(parkingId : String){
        val hashMap : HashMap<String,Any> = HashMap()
        hashMap["parking_history_id"] = parkingId
        ParkingWs().acceptNotification(this,hashMap,"report",notification)
    }

    private val notification = object : ParkingWs.OnRequestCreateCallBackListener {
        override fun onLoadSuccess(listParking: String?) {
            startActivity(Intent(this@ListAllCommentParkingActivity, ParkingListActivity::class.java))
            Toast.makeText(this@ListAllCommentParkingActivity, listParking, Toast.LENGTH_LONG).show()
        }

        override fun onLoadSuccessListener(action: String?, parkingId: String?) {}

        override fun onLoadFail(message: String?) {
            Toast.makeText(this@ListAllCommentParkingActivity, message, Toast.LENGTH_LONG).show()
        }

    }
}

