package com.eazy.daikou.ui.home.my_document.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.my_document.MyDocumentItemModel

class MyDocumentItemAdapter(private var list : ArrayList<MyDocumentItemModel>, private var onClickItem : ClickOnItemDocumentListener) : RecyclerView.Adapter<MyDocumentItemAdapter.ItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        return ItemViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.my_document_model, parent, false))
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val myDocumentItemModel = list[position]
        holder.txtCreateDate.text = if (myDocumentItemModel.created_dt != null)  myDocumentItemModel.created_dt else ". . ."
        holder.txtCreateTime.text =  Utils.formatDateTime(myDocumentItemModel.created_dt,"yyyy-MM-dd h:mm:ss","hh:mm")
        holder.readDocument.text = if (myDocumentItemModel.document_name != null) myDocumentItemModel.document_name else ". . ."
        holder.txtCreateReadDocument.setOnClickListener{
            onClickItem.onClickItemDetail(myDocumentItemModel)
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var txtCreateDate: TextView = itemView.findViewById(R.id.created_date)
        var txtCreateTime: TextView = itemView.findViewById(R.id.created_time)
        var txtCreateReadDocument: LinearLayout = itemView.findViewById(R.id.mainLayout)
        var readDocument: TextView = itemView.findViewById(R.id.read_document)
    }

    interface ClickOnItemDocumentListener{
        fun onClickItemDetail(itemDocument : MyDocumentItemModel)
    }

    fun clear() {
        val size: Int = list.size
        if (size > 0) {
            for (i in 0 until size) {
                list.removeAt(0)
            }
            notifyItemRangeRemoved(0, size)
        }
    }
}