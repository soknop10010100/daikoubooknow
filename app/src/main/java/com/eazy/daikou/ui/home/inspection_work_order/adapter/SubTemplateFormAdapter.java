package com.eazy.daikou.ui.home.inspection_work_order.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.daimajia.swipe.SwipeLayout;
import com.eazy.daikou.R;
import com.eazy.daikou.request_data.static_data.StaticUtilsKey;
import com.eazy.daikou.helper.Utils;
import com.eazy.daikou.model.inspection.ItemTemplateModel;

import java.util.List;
import java.util.Locale;

public class SubTemplateFormAdapter extends RecyclerView.Adapter<SubTemplateFormAdapter.ItemViewHolder> {

    private final List<ItemTemplateModel.SubItemTemplateModel> subItemTemplateModelList;
    private final String categoryKey, action_menu;
    private final ClickCallBackSubItemListener clickCallBackSubItemListener;
    private final RecyclerView recyclerView;
    private final Context context;
    private final String main_no;
    private final boolean isAlreadySave;
    private final ItemTemplateModel itemTemplateModel;

    public SubTemplateFormAdapter(Context context, RecyclerView recyclerView, ItemTemplateModel itemTemplateModel,List<ItemTemplateModel.SubItemTemplateModel> subItemTemplateModelList, String main_no, String categoryKey, boolean isAlreadySave, String action_menu, ClickCallBackSubItemListener clickCallBackSubItemListener){
        this.subItemTemplateModelList = subItemTemplateModelList;
        this.categoryKey = categoryKey;
        this.action_menu = action_menu;
        this.clickCallBackSubItemListener = clickCallBackSubItemListener;
        this.recyclerView = recyclerView;
        this.context = context;
        this.main_no = main_no;
        this.isAlreadySave = isAlreadySave;
        this.itemTemplateModel = itemTemplateModel;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_sub_template_form, parent, false));
    }

    @SuppressLint("NotifyDataSetChanged")
    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        ItemTemplateModel.SubItemTemplateModel subItemTemplateModel = subItemTemplateModelList.get(position);
        if (subItemTemplateModel != null){

            if (subItemTemplateModel.getItem_description() != null){
                holder.subItemDescription.setText(subItemTemplateModel.getItem_description());
            }
            if (subItemTemplateModel.getSub_item_category() != null) {
                holder.subItemNameTv.setText(subItemTemplateModel.getSub_item_category());
            }
            if (subItemTemplateModel.getItem_condition() != null) {
                holder.subItemCondition.setText(subItemTemplateModel.getItem_condition());
            }

            if (subItemTemplateModel.getSub_item_category() != null){
                holder.subItemNameTv2.setText(subItemTemplateModel.getSub_item_category());
            }

            if (isAlreadySave){
                holder.subItemNameTv.setEnabled(false);
                holder.subItemNameTv.setBackground(ResourcesCompat.getDrawable(context.getResources(), R.drawable.card_view_shape_dark, null));

                holder.subItemNameTv2.setEnabled(false);
                holder.subItemNameTv2.setBackground(ResourcesCompat.getDrawable(context.getResources(), R.drawable.card_view_shape_dark, null));

                holder.subItemCondition.setEnabled(false);
                holder.subItemCondition.setBackground(ResourcesCompat.getDrawable(context.getResources(), R.drawable.card_view_shape_dark, null));

                holder.subItemDescription.setEnabled(false);
                holder.subItemDescription.setBackground(ResourcesCompat.getDrawable(context.getResources(), R.drawable.card_view_shape_dark, null));

                holder.cleanTv.setEnabled(false);
                holder.unDamageTv.setEnabled(false);
                holder.workingTv.setEnabled(false);
            } else {
                holder.subItemNameTv.setEnabled(true);
                holder.subItemNameTv.setBackground(ResourcesCompat.getDrawable(context.getResources(), R.drawable.shape_transparent_bg, null));

                holder.subItemNameTv2.setEnabled(true);
                holder.subItemNameTv2.setBackground(ResourcesCompat.getDrawable(context.getResources(), R.drawable.shape_transparent_bg, null));

                holder.subItemCondition.setEnabled(true);
                holder.subItemCondition.setBackground(ResourcesCompat.getDrawable(context.getResources(), R.drawable.shape_transparent_bg, null));

                holder.subItemDescription.setEnabled(true);
                holder.subItemDescription.setBackground(ResourcesCompat.getDrawable(context.getResources(), R.drawable.shape_transparent_bg, null));

                holder.cleanTv.setEnabled(true);
                holder.unDamageTv.setEnabled(true);
                holder.workingTv.setEnabled(true);

            }

            subItemTemplateModel.setItem_no(main_no + "." + (position + 1));

            holder.subItemNameTv.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    if(!editable.toString().isEmpty()){
                        subItemTemplateModel.setSub_item_category(editable.toString());
                    } else {
                        subItemTemplateModel.setSub_item_category("");
                    }
                }
            });

            holder.subItemNameTv2.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    if(!editable.toString().isEmpty()){
                        subItemTemplateModel.setSub_item_category(editable.toString());
                    } else {
                        subItemTemplateModel.setSub_item_category("");
                    }
                }
            });

            holder.subItemDescription.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    if(!editable.toString().isEmpty()){
                        subItemTemplateModel.setItem_description(editable.toString());
                    } else {
                        subItemTemplateModel.setItem_description("");
                    }
                }
            });

            holder.subItemCondition.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    if(!editable.toString().isEmpty()){
                        subItemTemplateModel.setItem_condition(editable.toString());
                    } else {
                        subItemTemplateModel.setItem_condition("");
                    }
                }
            });

            //Visible menus swipe layout
            holder.inspectionLayout.setVisibility(View.GONE);
            holder.writeReportLayout.setVisibility(View.GONE);
            holder.addImageLayout.setVisibility(View.GONE);

            holder.inspectionLayout.setOnClickListener(v -> clickCallBackSubItemListener.onClickInspection(subItemTemplateModel));
            holder.writeReportLayout.setOnClickListener(v -> clickCallBackSubItemListener.onClickWriteReport(subItemTemplateModel));
            holder.addImageLayout.setOnClickListener(v -> clickCallBackSubItemListener.onClickAddImage(subItemTemplateModel, categoryKey + subItemTemplateModel.getValue() ));
            holder.deleteLayout.setOnClickListener(v -> clickCallBackSubItemListener.onClickDelete(action_menu, recyclerView, itemTemplateModel, subItemTemplateModel, main_no, categoryKey, isAlreadySave, holder.swipe_layout));

            //Check Action
            if (subItemTemplateModel.getKey_menu().equalsIgnoreCase(StaticUtilsKey.simplify_button_key) && action_menu.equalsIgnoreCase(StaticUtilsKey.simplify_button_key)) {
                holder.simplifyLayout.setVisibility(View.VISIBLE);
                holder.detailLayout.setVisibility(View.GONE);
                holder.titleCleanTv.setVisibility(View.VISIBLE);    holder.titleUnDamageTv.setVisibility(View.VISIBLE);    holder.titleWorkingTv.setVisibility(View.VISIBLE);
            } else if (subItemTemplateModel.getKey_menu().equalsIgnoreCase(StaticUtilsKey.question_button_key) && action_menu.equalsIgnoreCase(StaticUtilsKey.question_button_key)){
                holder.simplifyLayout.setVisibility(View.VISIBLE);
                holder.detailLayout.setVisibility(View.GONE);
                holder.titleCleanTv.setVisibility(View.GONE);    holder.titleUnDamageTv.setVisibility(View.GONE);    holder.titleWorkingTv.setVisibility(View.GONE);
            } else if (subItemTemplateModel.getKey_menu().equalsIgnoreCase(StaticUtilsKey.detail_button_key) && action_menu.equalsIgnoreCase(StaticUtilsKey.detail_button_key)){
                holder.simplifyLayout.setVisibility(View.GONE);
                holder.detailLayout.setVisibility(View.VISIBLE);
            }

            //Set Color Background Item
            if (subItemTemplateModel.getKey_menu().equalsIgnoreCase(StaticUtilsKey.simplify_button_key)) {
                if (subItemTemplateModel.getIsClickNA().equalsIgnoreCase(Utils.getText(context, R.string.yes).toUpperCase(Locale.ROOT))) {
                    setBackgroundTintColor(context, holder.cleanTv, R.color.green);
                    holder.cleanTv.setText(Utils.getText(context, R.string.yes).toUpperCase(Locale.ROOT));
                } else if (subItemTemplateModel.getIsClickNA().equalsIgnoreCase(Utils.getText(context, R.string.no))) {
                    setBackgroundTintColor(context, holder.cleanTv, R.color.red);
                    holder.cleanTv.setText(Utils.getText(context, R.string.no));
                } else {
                    setBackgroundTintColor(context, holder.cleanTv, R.color.gray);
                    holder.cleanTv.setText("N/A");
                }
                subItemTemplateModel.setIsClickNA(subItemTemplateModel.getIsClickNA());
            } else if (subItemTemplateModel.getKey_menu().equalsIgnoreCase(StaticUtilsKey.question_button_key)){
                holder.cleanTv.setText(Utils.getText(context, R.string.yes).toUpperCase(Locale.ROOT));
                holder.unDamageTv.setText(Utils.getText(context, R.string.no));

                if (Utils.getText(context, R.string.yes).toUpperCase(Locale.ROOT).equalsIgnoreCase(subItemTemplateModel.getIsClickNA())) {

                    setBackgroundTintColor(context, holder.cleanTv, R.color.green);
                    setBackgroundTintColor(context, holder.unDamageTv, R.color.gray);
                    setBackgroundTintColor(context, holder.workingTv, R.color.gray);

                } else if (Utils.getText(context, R.string.no).equalsIgnoreCase(subItemTemplateModel.getIsClickNA())) {

                    setBackgroundTintColor(context, holder.cleanTv, R.color.gray);
                    setBackgroundTintColor(context, holder.unDamageTv, R.color.red);
                    setBackgroundTintColor(context, holder.workingTv, R.color.gray);

                } else if ("N/A".equalsIgnoreCase(subItemTemplateModel.getIsClickNA()) || "".equalsIgnoreCase(subItemTemplateModel.getIsClickNA())) {

                    setBackgroundTintColor(context, holder.cleanTv, R.color.gray);
                    setBackgroundTintColor(context, holder.unDamageTv, R.color.gray);
                    setBackgroundTintColor(context, holder.workingTv, R.color.blue);

                } else {

                    setBackgroundTintColor(context, holder.cleanTv, R.color.gray);
                    setBackgroundTintColor(context, holder.unDamageTv, R.color.gray);
                    setBackgroundTintColor(context, holder.workingTv, R.color.blue);
                    
                }
                subItemTemplateModel.setIsClickNA(subItemTemplateModel.getIsClickNA());
            }

            if (subItemTemplateModel.getClickUnDamage() != null && subItemTemplateModel.getKey_menu().equalsIgnoreCase(StaticUtilsKey.simplify_button_key)) {
                if (subItemTemplateModel.getClickUnDamage().equalsIgnoreCase(Utils.getText(context, R.string.yes).toUpperCase(Locale.ROOT))) {
                    setBackgroundTintColor(context, holder.unDamageTv, R.color.green);
                    holder.unDamageTv.setText(Utils.getText(context, R.string.yes).toUpperCase(Locale.ROOT));
                } else if (subItemTemplateModel.getClickUnDamage().equalsIgnoreCase(Utils.getText(context, R.string.no))) {
                    setBackgroundTintColor(context, holder.unDamageTv, R.color.red);
                    holder.unDamageTv.setText(Utils.getText(context, R.string.no));
                } else {
                    setBackgroundTintColor(context, holder.unDamageTv, R.color.gray);
                    holder.unDamageTv.setText("N/A");
                }
                subItemTemplateModel.setClickUnDamage(subItemTemplateModel.getClickUnDamage());
            }

            if (subItemTemplateModel.getKey_menu().equalsIgnoreCase(StaticUtilsKey.simplify_button_key)) {
                if (subItemTemplateModel.getClickWorking().equalsIgnoreCase(Utils.getText(context, R.string.yes))) {
                    setBackgroundTintColor(context, holder.workingTv, R.color.green);
                    holder.workingTv.setText(Utils.getText(context, R.string.yes).toUpperCase(Locale.ROOT));
                } else if (subItemTemplateModel.getClickWorking().equalsIgnoreCase(Utils.getText(context, R.string.no))) {
                    setBackgroundTintColor(context, holder.workingTv, R.color.red);
                    holder.workingTv.setText(Utils.getText(context, R.string.no));
                } else {
                    setBackgroundTintColor(context, holder.workingTv, R.color.gray);
                    holder.workingTv.setText("N/A");
                }
                subItemTemplateModel.setClickWorking(subItemTemplateModel.getClickWorking());
            }

            holder.cleanTv.setOnClickListener(v -> {
                clickCallBackSubItemListener.onClickYesNoNA(action_menu, recyclerView, itemTemplateModel, subItemTemplateModel, main_no, categoryKey, "clean_click");
            });

            holder.unDamageTv.setOnClickListener(v -> {
                clickCallBackSubItemListener.onClickYesNoNA(action_menu, recyclerView, itemTemplateModel, subItemTemplateModel, main_no, categoryKey, "un_damage_click");
            });

            holder.workingTv.setOnClickListener(v -> {
                clickCallBackSubItemListener.onClickYesNoNA(action_menu, recyclerView, itemTemplateModel, subItemTemplateModel, main_no, categoryKey, "working_click");
            });

        }
    }

    @Override
    public int getItemCount() {
        try {
            return subItemTemplateModelList.size();
        } catch(NullPointerException ex) {
            return 0;
        }
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder{

        private final EditText subItemNameTv, subItemNameTv2, subItemDescription, subItemCondition;
        private final LinearLayout inspectionLayout, writeReportLayout, addImageLayout, deleteLayout, simplifyLayout, detailLayout;
        private final TextView cleanTv, unDamageTv, workingTv, titleCleanTv, titleUnDamageTv, titleWorkingTv;
        private final SwipeLayout swipe_layout;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);

            simplifyLayout = itemView.findViewById(R.id.simplifyLayout);
            detailLayout = itemView.findViewById(R.id.linear_item);
            swipe_layout = itemView.findViewById(R.id.swipe_layout);
            subItemCondition = itemView.findViewById(R.id.SubItemCondition);
            subItemDescription = itemView.findViewById(R.id.SubItemDescription);
            subItemNameTv = itemView.findViewById(R.id.SubItemName);
            subItemNameTv2 = itemView.findViewById(R.id.SubItemName2);

            inspectionLayout = itemView.findViewById(R.id.buttonAddInspection);
            writeReportLayout = itemView.findViewById(R.id.buttonReport);
            addImageLayout = itemView.findViewById(R.id.buttonAddImage);
            deleteLayout = itemView.findViewById(R.id.buttonDeleteItem);

            cleanTv = itemView.findViewById(R.id.cleanTv);
            unDamageTv = itemView.findViewById(R.id.unDamageTv);
            workingTv = itemView.findViewById(R.id.workingTv);

            titleCleanTv = itemView.findViewById(R.id.titleCleanTv);
            titleUnDamageTv = itemView.findViewById(R.id.titleUnDamageTv);
            titleWorkingTv = itemView.findViewById(R.id.titleWorkingTv);
        }
    }

    private void setBackgroundTintColor(Context context, View view, int color){
        DrawableCompat.setTint(DrawableCompat.wrap(view.getBackground()).mutate(),
                ContextCompat.getColor(context, color));
    }

    public interface ClickCallBackSubItemListener{
        void onClickInspection(ItemTemplateModel.SubItemTemplateModel subItemTemplateModel);
        void onClickWriteReport(ItemTemplateModel.SubItemTemplateModel subItemTemplateModel);
        void onClickAddImage(ItemTemplateModel.SubItemTemplateModel subItemTemplateModel, String keyDefine);
        void onClickDelete(String action_menu, RecyclerView recyclerView, ItemTemplateModel itemTemplateModel,ItemTemplateModel.SubItemTemplateModel subItemTemplateModel, String main_no, String categoryKey, boolean isAlreadySave,SwipeLayout swipe_layout);
        void onClickYesNoNA(String action_menu, RecyclerView recyclerView, ItemTemplateModel itemTemplateModel,ItemTemplateModel.SubItemTemplateModel subItemTemplateModel, String main_no, String categoryKey, String type);

    }
}
