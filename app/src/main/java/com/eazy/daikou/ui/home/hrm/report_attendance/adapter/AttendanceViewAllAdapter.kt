package com.eazy.daikou.ui.home.hrm.report_attendance.adapter

import android.content.Context
import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.helper.DateUtil
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.hr.ItemAttendanceModel

class AttendanceViewAllAdapter(private val context: Context, private val listAllAttendance : ArrayList<ItemAttendanceModel>,private val callBackList: CallBackAttendanceViewAll): RecyclerView.Adapter<AttendanceViewAllAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view : View = LayoutInflater.from(context).inflate(R.layout.layout_view_all_attendance, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
       val listModel: ItemAttendanceModel = listAllAttendance[position]

        if (listModel != null){

            holder.employeeNameTv.text = if (listModel.user_name != null) listModel.user_name else "- - -"

            holder.dateTv.text = if (listModel.attendance_date != null) Utils.formatDateFromString("yyyy-MM-dd", "dd-MMM-yyyy",listModel.attendance_date) else "- - -"

            holder.checkInTimeTv.text = if(listModel.check_in != null ) DateUtil.formatTimeServer(listModel.check_in.toString()) else "- - -"

            holder.checkOutTimeTv.text = if (listModel.check_out != null) DateUtil.formatTimeServer(listModel.check_out.toString()) else "- - -"

            holder.totalTimeTv.text =
                if (listModel.total_working_hour != null)
                    if (listModel.total_working_hour!!.total_hour != null && listModel.total_working_hour!!.total_minute != null)
                        listModel.total_working_hour!!.total_hour + context.getString(R.string.h) +
                                listModel.total_working_hour!!.total_minute + context.getString(R.string.min)
                    else "- - -"
                else "- - -"

            if (listModel.button_status != null && listModel.button_status == "missed_check_out"){
                holder.style.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(context,R.color.red))
                holder.missedCheckOutTv.visibility = View.VISIBLE
                holder.missedCheckOutTv.text = holder.missedCheckOutTv.context.resources.getString(R.string.missed_check_out)
            } else {
                holder.missedCheckOutTv.visibility = View.GONE
                holder.style.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(context,R.color.greenSea))
            }

            holder.itemView.setOnClickListener{
                callBackList.callBackAttendanceAll(listModel)
            }

        }
    }

    override fun getItemCount(): Int {
        return listAllAttendance.size
    }

    class ViewHolder(view : View) : RecyclerView.ViewHolder(view){
        val employeeNameTv: TextView = view.findViewById(R.id.EmployeeNameTv)
        val dateTv: TextView = view.findViewById(R.id.dateTv)
        val checkInTimeTv: TextView = view.findViewById(R.id.checkInTimeTv)
        val checkOutTimeTv: TextView = view.findViewById(R.id.checkOutTimeTv)
        val totalTimeTv: TextView = view.findViewById(R.id.totalTimeTv)
        val style: TextView = view.findViewById(R.id.style)
        val missedCheckOutTv: TextView = view.findViewById(R.id.missedCheckOutTv)
    }

    interface CallBackAttendanceViewAll{
        fun callBackAttendanceAll(attendanceModel: ItemAttendanceModel)
    }

    fun clear() {
        val size: Int = listAllAttendance.size
        if (size > 0) {
            for (i in 0 until size) {
                listAllAttendance.removeAt(0)
            }
            notifyItemRangeRemoved(0, size)
        }
    }
}