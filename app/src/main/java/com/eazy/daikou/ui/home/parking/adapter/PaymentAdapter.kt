package com.eazy.daikou.ui.home.parking.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.model.notification.Payment
import kotlin.collections.ArrayList


class PaymentAdapter(private val context: Context, private var state: Int, private val serviceList: ArrayList<Payment>,
                     private val itemClickService: ItemClickPaymentMethod) : RecyclerView.Adapter<PaymentAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.layout_item_payment_name, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val pay: Payment = serviceList[position]
        var mSelect :Int = -1

        holder.textView.text = pay.name
        Glide.with(context).load(pay.image).into(holder.image)

        holder.btnRadioButton.setOnClickListener {
            holder.btnRadioButton.isChecked = true
            mSelect = position
            state = mSelect
            itemClickService.onClick(pay.name,pay.image,mSelect)
        }
        holder.linearLayout.setOnClickListener {
            holder.btnRadioButton.isChecked = true
            mSelect = position
            state = mSelect
            itemClickService.onClick(pay.name,pay.image,mSelect)
        }
        if(position== mSelect||position==state){
            holder.btnRadioButton.isChecked  = true
        }

    }

    override fun getItemCount(): Int {
        return serviceList.size
    }

    interface ItemClickPaymentMethod {
        fun onClick(service: String,image:Int,stat : Int)

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val image : ImageView = itemView.findViewById(R.id.image_vie)
        val textView : TextView = itemView.findViewById(R.id.tex_name)
        val linearLayout : LinearLayout = itemView.findViewById(R.id.click_payment)
        val btnRadioButton : RadioButton = itemView.findViewById(R.id.clcik_pay)
    }

    fun clear() {
        val size: Int = serviceList.size
        if (size > 0) {
            for (i in 0 until size) {
                serviceList.removeAt(0)
            }
            notifyItemRangeRemoved(0, size)
        }
    }
}