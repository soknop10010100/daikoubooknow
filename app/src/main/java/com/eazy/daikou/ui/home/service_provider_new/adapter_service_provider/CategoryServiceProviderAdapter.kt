package com.eazy.daikou.ui.home.service_provider_new.adapter_service_provider

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.model.service_provider.SubCategory

class CategoryServiceProviderAdapter(private val context: Context, private val categoryList: ArrayList<SubCategory>, private val callBackItemCategory: CallBackItemCategoryLister): RecyclerView.Adapter<CategoryServiceProviderAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.custom_view_category_provider, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
       val category: SubCategory = categoryList[position]
        if (category != null) {
            if (category.urlImage != null){
                Glide.with(context).load(category.urlImage).into(holder.imageViewCategory)
            } else {
                Glide.with(context).load(R.drawable.no_image).into(holder.imageViewCategory)
            }
            holder.nameCategory.text = if(category.title != null) category.title else "- - -"

            holder.itemView.setOnClickListener { callBackItemCategory.itemClickListener(category)  }
        }
    }
    override fun getItemCount(): Int {
        return categoryList.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view){
        var imageViewCategory : ImageView = view.findViewById(R.id.imageViewCategory)
        var nameCategory : TextView = view.findViewById(R.id.nameCategory)
    }

    interface CallBackItemCategoryLister{
        fun itemClickListener(category: SubCategory)
    }
}