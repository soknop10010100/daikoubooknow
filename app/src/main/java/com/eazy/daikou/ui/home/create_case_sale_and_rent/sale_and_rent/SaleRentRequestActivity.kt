package com.eazy.daikou.ui.home.create_case_sale_and_rent.sale_and_rent

import android.app.Activity
import android.content.Intent
import android.content.res.ColorStateList
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.base.BaseCameraActivity
import com.eazy.daikou.request_data.request.buy_sell_ws.BuySaleRentWs
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.buy_sell.ItemSaleAndRentModel
import com.eazy.daikou.model.work_order.ImageListModel
import com.eazy.daikou.ui.home.inspection_work_order.adapter.ImageListWorkOrderAdapter

class SaleRentRequestActivity : BaseActivity() {

    // add photo
    private lateinit var btnAddImageSecond: RelativeLayout
    private lateinit var btnAddImg: LinearLayout
    private lateinit var listImageRecyclerView: RecyclerView
    private lateinit var txtNoImageAdd: TextView
    //choose list type
    private lateinit var linearTypeHouse: LinearLayout
    private lateinit var linearClickType: LinearLayout
    private lateinit var textSellTv: TextView
    private lateinit var textRentTv: TextView
    private lateinit var propertyTypeTv: TextView
    private lateinit var titleTv: EditText
    private lateinit var priceTv: EditText
    private lateinit var contactTv: EditText
    private lateinit var addressTv: EditText
    private lateinit var widthTv: EditText
    private lateinit var lengthTv: EditText
    private lateinit var roomTv: EditText
    private lateinit var bathRoomTv: EditText
    private lateinit var descriptionTv: EditText
    private lateinit var btnSubmit: TextView

    private lateinit var progressBar: ProgressBar
    private val bitmapList: ArrayList<ImageListModel> = ArrayList()
    private var post = 0
    private var userId = ""
    private var actionType = "sale"
    private var idProperty = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sale_rent)

        initView()

        initData()

        initAction()

    }

    private fun initView(){

        findViewById<ImageView>(R.id.iconBack).setOnClickListener { finish() }
        findViewById<TextView>(R.id.titleToolbar).text = getString(R.string.new_listing)
        progressBar = findViewById(R.id.progressItem)
        // add photo
        btnAddImageSecond = findViewById(R.id.btnAddImageSecond)
        listImageRecyclerView = findViewById(R.id.listImageRecyclerView)
        txtNoImageAdd = findViewById(R.id.txtNoImageAdd)
        btnAddImg = findViewById(R.id.btnAddImg)
        textSellTv = findViewById(R.id.textSellTv)
        textRentTv = findViewById(R.id.textRentTv)
        linearClickType = findViewById(R.id.linearClickType)
        linearTypeHouse = findViewById(R.id.linearTypeHouse)
        propertyTypeTv = findViewById(R.id.propertyTypeTv)

        //List Type
        titleTv = findViewById(R.id.titleTv)
        priceTv = findViewById(R.id.priceTv)
        contactTv = findViewById(R.id.contactTv)
        addressTv = findViewById(R.id.addressTv)
        widthTv = findViewById(R.id.widthTv)
        lengthTv = findViewById(R.id.lengthTv)
        roomTv = findViewById(R.id.roomTv)
        bathRoomTv = findViewById(R.id.bathRoomTv)
        descriptionTv = findViewById(R.id.descriptionTv)
        btnSubmit = findViewById(R.id.btnSubmit)
    }

    private fun initData(){
        val userSessionManagement = UserSessionManagement(this)
        userId = userSessionManagement.userId
    }

    private fun initAction(){
        progressBar.visibility = View.GONE
        propertyTypeTv.setOnClickListener (CustomSetOnClickViewListener{
                bottomSheetCategoryService()
            })

        textSellTv.setOnClickListener {
            actionType ="sale"
            actionClickItem()
        }
        textRentTv.setOnClickListener {
            actionType = "rent"
            actionClickItem()
        }
        btnAddImg.setOnClickListener {
            startForResult.launch(Intent(this, BaseCameraActivity::class.java))
        }
        btnSubmit.setOnClickListener( CustomSetOnClickViewListener{
            createRequestServiceToAPI()
        })
    }

    private fun bottomSheetCategoryService(){
        val bottomSheetFragment = TypePropertyFragment()
        bottomSheetFragment.initListener(callBackData)
        supportFragmentManager.let { bottomSheetFragment.show(it, bottomSheetFragment.tag) }
    }

    private fun actionClickItem(){
        if (actionType == "sale"){
            textSellTv.backgroundTintList =  ColorStateList.valueOf(ContextCompat.getColor(this, R.color.appBarColorOld))
            textRentTv.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(this, R.color.color_gray_item))
        } else {
            textSellTv.backgroundTintList =  ColorStateList.valueOf(ContextCompat.getColor(this, R.color.color_gray_item))
            textRentTv.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(this, R.color.appBarColorOld))
        }
    }

    private fun createRequestServiceToAPI(){
        val hashMap : HashMap<String, Any?> = HashMap()
        hashMap["user_id"] = userId
        hashMap["title"] = titleTv.text.toString()
        hashMap["description"] = descriptionTv.text.toString()
        hashMap["address"] = addressTv.text.toString()
        hashMap["phone_no"] = if (contactTv.text.toString().isNotEmpty()) contactTv.text.toString().replace(" ", "") else ""
        hashMap["width"] = widthTv.text.toString()
        hashMap["length"] = lengthTv.text.toString()
        hashMap["price"] = priceTv.text.toString()
        hashMap["category"] = actionType
        hashMap["total_bedrooms"] = roomTv.text.toString()
        hashMap["total_bathrooms"] = bathRoomTv.text.toString()
        hashMap["property_type"] = idProperty

        val imageList : ArrayList<String> = ArrayList()
        for (item in bitmapList){
            imageList.add(Utils.convert(item.bitmapImage))
        }
        hashMap["images"] = imageList

        progressBar.visibility = View.VISIBLE

        BuySaleRentWs().createListSaleAndRentPropertyWS(this, hashMap, callBackSaleAndRent)

    }

    private val callBackSaleAndRent : BuySaleRentWs.OnCallBackCreateSaleAndRentListener = object : BuySaleRentWs.OnCallBackCreateSaleAndRentListener{
        override fun onSuccess(message: String) {
            progressBar.visibility = View.GONE
            Utils.customToastMsgError(this@SaleRentRequestActivity, message, true)
            finish()
        }
        override fun onFailed(error: String) {
            progressBar.visibility = View.GONE
            Utils.customToastMsgError(this@SaleRentRequestActivity, error, false)
        }
    }

    // Call Back Data from Fragment
    private val callBackData : TypePropertyFragment.CallBackItemDataListener = object : TypePropertyFragment.CallBackItemDataListener{
        override fun callBackDate(itemSaleAndRentModel: ItemSaleAndRentModel) {
            propertyTypeTv.text = itemSaleAndRentModel.nameItem
            idProperty = itemSaleAndRentModel.idItem
            when (itemSaleAndRentModel.idItem){
                "land", "building" -> {
                    linearTypeHouse.visibility = View.GONE
                }
                "house" -> {
                    linearTypeHouse.visibility = View.VISIBLE
                }
            }
        }
    }

    private fun addUrlImage(posKeyDefine: String, imagePath: String) {
        val loadedBitmap = BaseCameraActivity.convertByteToBitmap(imagePath)
        post += 1
        val imageListModel = ImageListModel()
        imageListModel.keyImage = posKeyDefine
        imageListModel.bitmapImage = loadedBitmap
        bitmapList.add(imageListModel)
    }

    private fun setImageRecyclerView(post: Int) {
        listImageRecyclerView.layoutManager = GridLayoutManager(this, 1, RecyclerView.HORIZONTAL, false)
        val pickUpAdapter = ImageListWorkOrderAdapter(this, post, bitmapList, onClearImage)
        listImageRecyclerView.adapter = pickUpAdapter
        btnAddImg.visibility = Utils.visibleView(bitmapList)
    }

    private val startForResult = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode == Activity.RESULT_OK) {
            val data = result.data
            if (data!!.hasExtra("image_path")) {
                val imagePath = data.getStringExtra("image_path")
                addUrlImage("", imagePath!!)
                setImageRecyclerView(post)
            }
            if (data.hasExtra("select_image")) {
                val imagePath = data.getStringExtra("select_image")
                addUrlImage("", imagePath!!)
                setImageRecyclerView(post)
            }
        }
    }
    private val onClearImage: ImageListWorkOrderAdapter.OnClearImage = object : ImageListWorkOrderAdapter.OnClearImage {
            override fun onClickRemove(bitmap: ImageListModel, post: Int) {
                removeImage(post, bitmap)
            }

            override fun onViewImage(bitmap: ImageListModel) {
                if (bitmap.bitmapImage != null) {
                    Utils.openImageOnDialog(this@SaleRentRequestActivity, Utils.convert(bitmap.bitmapImage))
                }
            }
        }

    private fun removeImage(position: Int, bitmap: ImageListModel) {
        bitmapList.remove(bitmap)
        setImageRecyclerView(position)
    }
}