package com.eazy.daikou.ui.home.hrm.report_attendance.adapter

import android.annotation.SuppressLint
import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.helper.DateUtil
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.hr.ItemAttendanceModel
import kotlin.collections.ArrayList

class MyHrAttendanceAdapter(private val attendanceList: ArrayList<ItemAttendanceModel>,private val workStatus : String, private val itemClick: ItemClickListener) :
    RecyclerView.Adapter<MyHrAttendanceAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.layout_adapter_report_attendance, parent, false)
        return ViewHolder(v)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data: ItemAttendanceModel = attendanceList[position]

        if(workStatus == "not_working"){
            holder.messageNotItemTv.text = holder.headerInfoTv.context.resources.getString(R.string.today_is_holiday)
        }
        holder.dateWork.text = if (data.attendance_date != null) Utils.formatDateFromString("yyyy-MM-dd", "dd-MMM-yyyy",data.attendance_date) else "- - -"

        holder.checkInTimeTv.text = if(data.check_in != null ) DateUtil.formatTimeServer(data.check_in.toString()) else "- - -"

        holder.checkOutTimeTv.text = if (data.check_out != null) DateUtil.formatTimeServer(data.check_out.toString()) else "- - -"

            if (data.button_status != null && data.button_status == "missed_check_out"){
                holder.style.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(holder.headerInfoTv.context,R.color.red))
                holder.missedCheckOutTv.visibility = View.VISIBLE
                holder.missedCheckOutTv.text = holder.missedCheckOutTv.context.resources.getString(R.string.missed_check_out)
            } else {
                holder.missedCheckOutTv.visibility = View.GONE
                holder.style.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(holder.headerInfoTv.context,R.color.greenSea))
            }
        holder.totalTimeTv.text =
            if (data.total_working_hour != null)
                if (data.total_working_hour!!.total_hour != null && data.total_working_hour!!.total_minute != null)
                    data.total_working_hour!!.total_hour + " hr " +
                        data.total_working_hour!!.total_minute + " mn"
                else "- - -"
            else "- - -"


        when (data.action_type) {
            "today_info" -> {
                holder.headerInfoTv.text = holder.headerInfoTv.context.resources.getString(R.string.today_attendance)
                holder.noCheckInLayout.visibility = View.GONE
                holder.btnCheckIn.visibility = View.GONE
                holder.iconLabel.setImageDrawable(ContextCompat.getDrawable(holder.headerInfoTv.context, R.drawable.today_attendance))
            }
            "none_today_info" -> {
                holder.headerInfoTv.text = holder.headerInfoTv.context.resources.getString(R.string.today_attendance)
                holder.noCheckInLayout.visibility = View.VISIBLE
                holder.iconLabel.setImageDrawable(ContextCompat.getDrawable(holder.headerInfoTv.context, R.drawable.today_attendance))

                if(workStatus == "not_working"){
                    holder.btnCheckIn.visibility = View.GONE
                }else{
                    holder.btnCheckIn.visibility = if (workStatus != null)
                        if (workStatus == "checked_in") View.GONE else View.VISIBLE
                    else View.GONE
                }

            }
            "history_info" -> {
                holder.headerInfoTv.text = holder.headerInfoTv.context.resources.getString(R.string.attendance_histories)
                holder.noCheckInLayout.visibility = View.GONE
                holder.btnCheckIn.visibility = View.GONE
                holder.iconLabel.setImageDrawable(ContextCompat.getDrawable(holder.headerInfoTv.context, R.drawable.history_attendance))
            }
            "none_history_info" ->{
                holder.headerInfoTv.text = holder.headerInfoTv.context.resources.getString(R.string.attendance_histories)
                holder.noCheckInLayout.visibility = View.VISIBLE
                holder.btnCheckIn.visibility = View.GONE
                holder.messageNotItemTv.text = holder.headerInfoTv.context.resources.getString(R.string.no_attendance_histories)
                holder.iconLabel.setImageDrawable(ContextCompat.getDrawable(holder.headerInfoTv.context, R.drawable.history_attendance))
            }
        }

        if (data.action_type == "today_info"){
            holder.headerTitleLayout.visibility = View.VISIBLE
            holder.infoLayout.visibility = View.VISIBLE
        } else if (data.action_type == "none_today_info"){
            holder.headerTitleLayout.visibility = View.VISIBLE
            holder.infoLayout.visibility = View.GONE
        } else if (data.action_type == "none_history_info"){
            holder.headerTitleLayout.visibility = View.VISIBLE
            holder.infoLayout.visibility = View.GONE
        } else if (data.action_type == "history_info" && position == 1){
            holder.headerTitleLayout.visibility = View.VISIBLE
            holder.infoLayout.visibility = View.VISIBLE
        } else {
            holder.headerTitleLayout.visibility = View.GONE
            holder.infoLayout.visibility = View.VISIBLE
        }

        holder.itemView.setOnClickListener(CustomSetOnClickViewListener{
            itemClick.onLoadSuccess(data)
        })

        holder.btnCheckIn.setOnClickListener(
            CustomSetOnClickViewListener{
                itemClick.onClickedCheckIn(holder.btnCheckIn)
            }
        )

    }

    override fun getItemCount(): Int {
        return attendanceList.size
    }

    interface ItemClickListener{
        fun onLoadSuccess(data: ItemAttendanceModel)
        fun onClickedCheckIn(txtView : TextView)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var missedCheckOutTv : TextView = view.findViewById(R.id.missedCheckOutTv)
        var dateWork : TextView = view.findViewById(R.id.dateTv)
        var checkInTimeTv : TextView = view.findViewById(R.id.checkInTimeTv)
        var checkOutTimeTv : TextView = view.findViewById(R.id.checkOutTimeTv)
        var totalTimeTv : TextView = view.findViewById(R.id.totalTimeTv)
        var headerInfoTv : TextView = view.findViewById(R.id.headerInfoTv)
        var infoLayout : CardView = view.findViewById(R.id.infoLayout)
        var noCheckInLayout : LinearLayout = view.findViewById(R.id.noCheckInLayout)
        var messageNotItemTv : TextView = view.findViewById(R.id.messageNotItemTv)
        var iconLabel : ImageView = view.findViewById(R.id.iconLabel)
        var headerTitleLayout : LinearLayout = view.findViewById(R.id.headerTitleLayout)
        var style : TextView = view.findViewById(R.id.style)
        var btnCheckIn : TextView = view.findViewById(R.id.btnCheckIn)
    }

    fun clear() {
        val size: Int = attendanceList.size
        if (size > 0) {
            for (i in 0 until size) {
                attendanceList.removeAt(0)
            }
            notifyItemRangeRemoved(0, size)
        }
    }

}