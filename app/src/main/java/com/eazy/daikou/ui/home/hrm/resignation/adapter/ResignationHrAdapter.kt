package com.eazy.daikou.ui.home.hrm.resignation.adapter

import android.app.Activity
import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.hr.ResignationHrModel

class ResignationHrAdapter(private val context : Activity, private val listNotice : ArrayList<ResignationHrModel>, private val itemClickCallBack : ItemClickOnListener) : RecyclerView.Adapter<ResignationHrAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(context).inflate(R.layout.resignation_item_model, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val resignationHrModel : ResignationHrModel = listNotice[position]
        if (resignationHrModel != null){
            holder.noTv.text = resignationHrModel.id
            holder.nameTv.text = resignationHrModel.resigner_name
            holder.createDateTv.text = Utils.formatDateTime(resignationHrModel.created_at, "yyyy-MM-dd kk:mm:ss", "dd-MMM-yyyy hh:mm a")
            holder.positionTv.text = resignationHrModel.designation
            holder.lastDayTv.text = resignationHrModel.last_date
            holder.typeTv.text = resignationHrModel.type

            when (resignationHrModel.status) {
                 "approved" -> {
                    holder.statusTv.text = Utils.getText(context, R.string.approved)
                    holder.statusTv.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(context, R.color.green))
                    holder.style.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(context, R.color.green))
                }
                "pending" -> {
                    holder.statusTv.text = Utils.getText(context, R.string.pending)
                    holder.statusTv.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(context, R.color.yellow))
                    holder.style.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(context, R.color.yellow))
                }
                "rejected" -> {
                    holder.statusTv.text = Utils.getText(context, R.string.rejected)
                    holder.statusTv.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(context, R.color.red))
                    holder.style.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(context, R.color.red))
                }
            }

            holder.itemView.setOnClickListener {
                itemClickCallBack.onItemClick(resignationHrModel)
            }
        }
    }

    override fun getItemCount(): Int {
        return listNotice.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var noTv : TextView = view.findViewById(R.id.noTv)
        var createDateTv: TextView = view.findViewById(R.id.createDateTv)
        var nameTv : TextView = view.findViewById(R.id.nameTv)
        var positionTv : TextView = view.findViewById(R.id.positionTv)
        var lastDayTv : TextView = view.findViewById(R.id.lastDayTv)
        var typeTv : TextView = view.findViewById(R.id.typeTv)
        var statusTv : TextView = view.findViewById(R.id.statusTv)
        var style : TextView = view.findViewById(R.id.style)
    }

    interface ItemClickOnListener{
        fun onItemClick(resignationHrModel: ResignationHrModel?)
    }

    fun clear() {
        val size: Int = listNotice.size
        if (size > 0) {
            for (i in 0 until size) {
                listNotice.removeAt(0)
            }
            notifyItemRangeRemoved(0, size)
        }
    }

}