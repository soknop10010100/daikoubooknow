package com.eazy.daikou.ui

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import java.util.*

class ViewPagerItemImagesAdapter(private val context: Context, private val image: ArrayList<String>) : PagerAdapter() {
    private val mLayoutInflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    override fun getCount(): Int {
        return image.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object` as LinearLayout
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val itemView: View = mLayoutInflater.inflate(R.layout.custom_viewpager_layout, container, false)
        val imageView = itemView.findViewById<ImageView>(R.id.imageViewPager)
        Glide.with(context).load(image[position]).into(imageView)
        imageView.setOnClickListener (CustomSetOnClickViewListener{
            // Utils.openZoomImage(context, image[position])
            val intent = Intent(context, ShowMultipleDisplayImagesActivity::class.java).apply {
                putExtra("image_list", image)
            }
            context.startActivity(intent)
        })
        Objects.requireNonNull(container).addView(itemView)
        return itemView
    }

    override fun destroyItem(viewGroup: ViewGroup, position: Int, `object`: Any) {
        viewGroup.removeView(`object` as LinearLayout)
    }

}


