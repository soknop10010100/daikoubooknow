package com.eazy.daikou.ui.home.hrm.notice_board

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AbsListView
import android.widget.ProgressBar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.request_data.request.hr_ws.NoticeBoardWs
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.hr.NoticeBoardHrModel
import com.eazy.daikou.model.profile.User
import com.eazy.daikou.ui.home.hrm.notice_board.adapter.NoticeBoardAdapter
import com.google.gson.Gson

class NoticeBoardHrActivity : BaseActivity() {

    private var currentItem = 0
    private var total : Int = 0
    private var scrollDown : Int = 0
    private var currentPage : Int = 1
    private var size : Int = 10
    private var isScrolling = false
    private var isScrollItem = false
    private var accountBusinessKey = ""
    private lateinit var user : User
    private lateinit var progressBar: ProgressBar
    private lateinit var recyclerView : RecyclerView
    private lateinit var linearLayoutManager : LinearLayoutManager
    private lateinit var refreshLayout: SwipeRefreshLayout
    private var noticeBoardList : ArrayList<NoticeBoardHrModel> = ArrayList()
    private lateinit var noticeBoardAdapter: NoticeBoardAdapter
    private var action = ""
    private var idNoticBoard = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notice_board_hr)

        initView()

        initAction()

    }

    private fun initView(){
        progressBar = findViewById(R.id.progressItem)
        recyclerView = findViewById(R.id.list_attendance)
        refreshLayout = findViewById(R.id.swipe_layouts)

        Utils.customOnToolbar(this, resources.getString(R.string.notice_board)){finish()}

    }

    private fun initAction(){

        user = Gson().fromJson(UserSessionManagement(this).userDetail, User::class.java)
        val accountBusiness = user.activeAccountBusinessKey
        if (accountBusiness != null){
            accountBusinessKey = accountBusiness
        }

        requestListNoticeBoard()
        initOnScrollItem(recyclerView)

        linearLayoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = linearLayoutManager

        noticeBoardAdapter = NoticeBoardAdapter(this, noticeBoardList,callBackNoticeListener)
        recyclerView.adapter = noticeBoardAdapter

        refreshLayout.setColorSchemeResources(R.color.colorPrimary)
        refreshLayout.setOnRefreshListener { this.refreshList() }

        if (intent != null && intent.hasExtra("action")){
            action = intent.getStringExtra("action").toString()
            val id = intent.getStringExtra("id").toString()
            if (intent != null && intent.hasExtra("id")) {
                idNoticBoard = id
            }
        }
        // Start detail from notification after request list already
        if (action == "resignation") {
            setActivityDetail(idNoticBoard)
            action = ""
        }

    }

    private fun refreshList() {
        currentPage = 1
        size = 10
        isScrolling = true
        noticeBoardAdapter.clear()
        requestListNoticeBoard()
    }

    private fun initOnScrollItem(recyclerViewItem: RecyclerView) {
        recyclerViewItem.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true
                }
            }
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                currentItem = linearLayoutManager.childCount
                total = linearLayoutManager.itemCount
                scrollDown = linearLayoutManager.findFirstVisibleItemPosition()
                if (dy > 0) {
                    if (isScrolling && currentItem + scrollDown == total) {
                        if (total == size) {
                            isScrolling = false
                            currentPage++
                            progressBar.visibility = View.VISIBLE
                            isScrollItem = true
                            recyclerViewItem.scrollToPosition(noticeBoardList.size - 1)
                            requestListNoticeBoard()
                            size += 10
                        } else {
                            progressBar.visibility = View.GONE
                        }
                    }
                }
            }
        })
    }

    private fun requestListNoticeBoard(){
        progressBar.visibility = View.VISIBLE
        NoticeBoardWs().getNoticeBoardWs(this,currentPage,10, accountBusinessKey, onCallBackNoticeBoardListener)
    }

    private val onCallBackNoticeBoardListener: NoticeBoardWs.OnCallBackNoticeBoardListener = object : NoticeBoardWs.OnCallBackNoticeBoardListener{
        @SuppressLint("NotifyDataSetChanged")
        override fun onLoadSuccessFull(noticeBoardHrModel: ArrayList<NoticeBoardHrModel>) {
            progressBar.visibility = View.GONE
            refreshLayout.isRefreshing = false

            if (!isScrollItem)  noticeBoardList.clear()
            noticeBoardList.addAll(noticeBoardHrModel)
            noticeBoardAdapter.notifyDataSetChanged()
        }

        override fun onLoadFailed(message: String) {
            progressBar.visibility = View.GONE
            refreshLayout.isRefreshing = false
            Utils.customToastMsgError(this@NoticeBoardHrActivity, message, false)
        }
    }
    private val callBackNoticeListener: NoticeBoardAdapter.CallBackNoticeListener = object: NoticeBoardAdapter.CallBackNoticeListener{
        override fun onItemClick(noticeBoardHrModel: NoticeBoardHrModel) {
            setActivityDetail(noticeBoardHrModel.id!!)
        }
    }

    private fun setActivityDetail(id : String){
        val intent = Intent(this@NoticeBoardHrActivity, ListNoticeBoardDetailActivity::class.java)
        intent.putExtra("id_list_notice_board", id)
        startActivity(intent)
    }
}