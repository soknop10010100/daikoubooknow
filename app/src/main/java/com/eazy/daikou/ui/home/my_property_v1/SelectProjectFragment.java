package com.eazy.daikou.ui.home.my_property_v1;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.eazy.daikou.R;
import com.eazy.daikou.helper.Utils;
import com.eazy.daikou.model.my_property.service_provider.SelectProjectModel;
import com.eazy.daikou.ui.home.my_property_v1.property_service_employee.adapter.SelectProjectAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class SelectProjectFragment extends DialogFragment {

    private SelectProjectAdapter selectProjectAdapter;
    private LinearLayoutManager linearLayoutManager;
    private EditText searchProject;
    private RecyclerView selectProjectRecycle;
    private ProgressBar progressBar;
    private ClickBackListener clickBackListener;
    private LinearLayout searchLayout, layout;
    private ImageView iconBack, refresh;
    private TextView titleToolbar, noItem, txtCancelSearch;
    private CoordinatorLayout coordinatorLayout;


    private final List<SelectProjectModel> searchProjectList = new ArrayList<>();
    private final List<SelectProjectModel> selectProjectList = new ArrayList<>();
    private final List<SelectProjectModel> selectProjectListItem = new ArrayList<>();
    private int currentItem, total, scrollDown, currentPage = 1, size = 15;
    private Boolean isScrolling = true ;
    private int currentItem1, total1, scrollDown1, currentPage1 = 1, size1 = 15;
    private Boolean isScrolling1 = true ;
    private boolean clickOnCancel = false;
    private String key, branch_id, branch_name;

    public SelectProjectFragment() {}

    public static SelectProjectFragment newInstance(String key) {
        SelectProjectFragment fragment = new SelectProjectFragment();
        Bundle args = new Bundle();
        args.putString("key", key);
        fragment.setArguments(args);
        return fragment;
    }
    public static SelectProjectFragment newInstance(String key, String branch_id, String branch_name) {
        SelectProjectFragment fragment = new SelectProjectFragment();
        Bundle args = new Bundle();
        args.putString("key", key);
        args.putString("branch_id", branch_id);
        args.putString("branch_name", branch_name);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.AlertShape);
        if (getArguments() != null && getArguments().containsKey("key")) {
            key = getArguments().getString("key");
        }
        if (getArguments() != null && getArguments().containsKey("branch_id")) {
            branch_id = getArguments().getString("branch_id");
        }
        if (getArguments() != null && getArguments().containsKey("branch_name")) {
            branch_name = getArguments().getString("branch_name");
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_select_project, container, false);

        initView(view);

        //Show Item project
        if (key.equals("ITEM_PROJECT")){
            searchLayout.setVisibility(View.GONE);
            coordinatorLayout.setVisibility(View.VISIBLE);
            layout.setVisibility(View.VISIBLE);
            iconBack.setImageResource(R.drawable.ic_close);
            iconBack.setBackground(ResourcesCompat.getDrawable(requireContext().getResources(), R.drawable.card_view_shape, null));
            iconBack.setColorFilter(ContextCompat.getColor(getContext(), R.color.appBarColor));
            refresh.setImageResource(R.drawable.icons8_refresh_60);
            titleToolbar.setText(branch_name);
            iconBack.setOnClickListener(view1 -> dismiss());

            refresh.setOnClickListener(view1 -> {
                refresh.setRotation(90);
                refresh.setClickable(false);
                selectProjectAdapter.clear();
                currentPage1 = 1;
                size1 = 15;
                progressBar.setVisibility(View.VISIBLE);
                requestDetailService(branch_id);
            });
        }

        linearLayoutManager = new LinearLayoutManager(getContext());
        selectProjectRecycle.setLayoutManager(linearLayoutManager);

        initRecycleSelectPro(key);
        if (key.equals("ALL_PROJECT")){
            initRecycleSelectProScroll(key);
        } else {
            initRecycleSelectProScroll(key, 2);  // identify 2 to avoid the same function
        }

        initSearchProject();

        txtCancelSearch.setOnClickListener(view1 -> {
            clickOnCancel = true;
            searchProject.setText("");
            txtCancelSearch.setVisibility(View.GONE);

            Utils.hideSoftKeyboard(txtCancelSearch);
            clearText();
        });

        //Hide key board when scroll
        selectProjectRecycle.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0){
                    Utils.hideSoftKeyboard(view);
                }
            }
        });

        return view;
    }

    private void initView(View view){
        selectProjectRecycle = view.findViewById(R.id.selectProject);
        progressBar = view.findViewById(R.id.progress);
        searchProject = view.findViewById(R.id.et_searchProject);
        iconBack = view.findViewById(R.id.iconBack);
        titleToolbar = view.findViewById(R.id.titleToolbar);
        layout = view.findViewById(R.id.layoutMap);
        refresh = view.findViewById(R.id.addressMap);
        coordinatorLayout = view.findViewById(R.id.toolbar_layout);
        searchLayout = view.findViewById(R.id.searchLayout);
        noItem = view.findViewById(R.id.noItem);

        //Search item
        txtCancelSearch = view.findViewById(R.id.txtCancel);

    }

    private void initSearchProject(){
        searchProject.setOnClickListener(view -> {
            txtCancelSearch.setVisibility(View.VISIBLE);
        });

        searchProject.setOnEditorActionListener((textView, i, keyEvent) -> {
            if(i== EditorInfo.IME_ACTION_SEARCH) {
                getSearch(searchProject.getText().toString());
            }
            return true;
        });

        searchProject.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(!editable.toString().isEmpty()){
                    size = 15;
                    currentPage = 1;
                    if (searchProjectList.size() > 0)    searchProjectList.clear();
                    getSearch(editable.toString());
                    initRecyclerViewScrollSearch(editable.toString());
                }else {
                    if (!clickOnCancel){
                        clearText();
                    }
                }

            }
        });
    }

    private void getSearch(String key_search){
        progressBar.setVisibility(View.VISIBLE);
        initRecycleSelectPro("SEARCH");
        // new PropertyListWs().SearchProjectItem(getContext(), currentPage, 15, key_search, callBackList);
    }

    private void requestService(){
        // new PropertyListWs().SelectProject(getContext(), currentPage, 15, callBackList);
    }

    private void requestDetailService(String branch_id){
        // new PropertyListWs().SelectProjectItem(getContext(), currentPage1, 15, branch_id,  callBackList);
    }

    private void initRecycleSelectPro(String key){
        if (key.equals("ALL_PROJECT")){
            selectProjectAdapter = new SelectProjectAdapter(getContext(), key, selectProjectList, clickOnListener);
            selectProjectRecycle.setAdapter(selectProjectAdapter);
            requestService();
        } else if (key.equals("SEARCH")){
            selectProjectAdapter = new SelectProjectAdapter(getContext(), key, searchProjectList, clickOnListener);
            selectProjectRecycle.setAdapter(selectProjectAdapter);
        } else {
            selectProjectAdapter = new SelectProjectAdapter(getContext(), key, selectProjectListItem, clickOnListener);
            selectProjectRecycle.setAdapter(selectProjectAdapter);
            requestDetailService(branch_id);
        }


    }


    private void clearText(){
        if (clickOnCancel){
            clickOnCancel = false;
        }
        progressBar.setVisibility(View.VISIBLE);
        size = 15;
        currentPage = 1;
        if (searchProjectList.size() > 0 )  searchProjectList.clear();
        if (selectProjectAdapter != null)   selectProjectAdapter.notifyDataSetChanged();

        if (selectProjectList.size() > 0)   selectProjectList.clear();
        initRecycleSelectPro("ALL_PROJECT");
        initRecycleSelectProScroll("ALL_PROJECT");
    }

    private void initRecyclerViewScrollSearch(String key) {
        selectProjectRecycle.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true;
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                currentItem = linearLayoutManager.getChildCount();
                total = linearLayoutManager.getItemCount();
                scrollDown = linearLayoutManager.findFirstVisibleItemPosition();
                if (dy > 0) {
                    if (isScrolling && (currentItem + scrollDown == total)) {
                        if (total == size) {
                            isScrolling = false;
                            currentPage++;
                            progressBar.setVisibility(View.VISIBLE);
                            recyclerView.scrollToPosition(searchProjectList.size() - 1);
                            getSearch(key);
                            size += 15;
                        }
                    }
                }
            }
        });
    }

    private void initRecycleSelectProScroll(String key) {
        selectProjectRecycle.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true;
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                currentItem = linearLayoutManager.getChildCount();
                total = linearLayoutManager.getItemCount();
                scrollDown = linearLayoutManager.findFirstVisibleItemPosition();
                if (dy > 0) {
                    if (isScrolling && (currentItem + scrollDown == total)) {
                        if (total == size) {
                            isScrolling = false;
                            currentPage++;
                            progressBar.setVisibility(View.VISIBLE);
                            recyclerView.scrollToPosition(selectProjectList.size() - 1);
                            initRecycleSelectPro(key);
                            size += 15;
                        }
                    }
                }
            }
        });
    }

    private void initRecycleSelectProScroll(String key, int identify) {
        selectProjectRecycle.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling1 = true;
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                currentItem1 = linearLayoutManager.getChildCount();
                total1 = linearLayoutManager.getItemCount();
                scrollDown1 = linearLayoutManager.findFirstVisibleItemPosition();
                if (dy > 0) {
                    if (isScrolling1 && (currentItem1 + scrollDown1 == total1)) {
                        if (total1 == size1) {
                            isScrolling1 = false;
                            currentPage1++;
                            progressBar.setVisibility(View.VISIBLE);
                            recyclerView.scrollToPosition(selectProjectListItem.size() - 1);
                            initRecycleSelectPro(key);
                            size1 += 15;
                        }
                    }
                }
            }
        });
    }

    private final SelectProjectAdapter.ClickCallBackListener clickOnListener = new SelectProjectAdapter.ClickCallBackListener() {
        @Override
        public void ClickCallBack(SelectProjectModel selectProjectModel) {
            clickBackListener.clickBackProject(selectProjectModel);
        }
    };

    public void initBackListener(ClickBackListener clickBackListener){
        this.clickBackListener = clickBackListener;
    }

    public interface ClickBackListener {
        void clickBackProject(SelectProjectModel selectProjectModel);
    }

    @Override
    public void onStart() {
        super.onStart();
        int width = getResources().getDisplayMetrics().widthPixels;
        int height = (int) (getResources().getDisplayMetrics().heightPixels * 0.9);
        Objects.requireNonNull(getDialog()).getWindow().setLayout(width, height);
    }
}