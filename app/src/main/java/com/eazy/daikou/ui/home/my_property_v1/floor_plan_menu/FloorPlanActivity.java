package com.eazy.daikou.ui.home.my_property_v1.floor_plan_menu;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.ColorStateList;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.RectF;
import android.graphics.Region;
import android.os.Build;
import android.os.Bundle;
import android.util.Base64;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.eazy.daikou.base.BaseActivity;
import com.eazy.daikou.request_data.static_data.StaticUtilsKey;
import com.eazy.daikou.helper.InternetConnection;
import com.github.ybq.android.spinkit.sprite.Sprite;
import com.github.ybq.android.spinkit.style.MultiplePulse;
import com.google.gson.Gson;
import com.eazy.daikou.R;
import com.eazy.daikou.model.floor_plan.FloorListModel;
import com.eazy.daikou.model.floor_plan.FloorSecureModel;
import com.eazy.daikou.model.floor_plan.FloorSpaceCategoryModel;
import com.eazy.daikou.model.floor_plan.SpacePolyDetailModel;
import com.eazy.daikou.model.utillity_tracking.model.UnitPolyDetailModel;
import com.eazy.daikou.ui.home.my_property_v1.adapter.FloorSpaceCategoryAdapter;
import com.eazy.daikou.model.floor_plan.FloorSpaceItemModel;
import com.eazy.daikou.model.floor_plan.FloorSpacePolyline;
import com.eazy.daikou.model.floor_plan.FloorUnitItemModel;
import com.eazy.daikou.model.profile.User;
import com.eazy.daikou.ui.home.my_property_v1.adapter.FloorNumAdapter;
import com.eazy.daikou.ui.home.my_property_v1.adapter.FloorSecureAdapter;
import com.eazy.daikou.ui.home.my_property_v1.adapter.ImageSecurityAdapter;
import com.eazy.daikou.request_data.request.service_property_ws.FloorListWs;
import com.eazy.daikou.helper.UserSessionManagement;
import com.eazy.daikou.helper.AbsoluteFitLayoutManager;
import com.eazy.daikou.helper.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class FloorPlanActivity extends BaseActivity {

    private View viewCover;
    private TextView tvFloor, tvDetailFloor, noImageLayout, noAvailableAll;
    private ImageView iMore, iDetailMore, imageSetFloor;
    private RecyclerView floorNumRecyclerView, floorSpaceCatRecyclerView, floorSecureRecycle, recycleSecureListImage;
    private FloorSecureAdapter floorSecureAdapter;
    private FloorSpaceCategoryAdapter spaceCategoryAdapter;
    private RelativeLayout frameLayout, frameImageSecure;
    private FloorNumAdapter floorNumAdapter;
    private LinearLayoutManager linearLayoutManager, layoutManagerSecure;
    private UserSessionManagement userSessionManagement;
    private ProgressBar progressBar, progressBarSecure;
    private LinearLayout floorLayout, DetailSecureLayout;
    private TextView titleSecureCat, detailList;
    private ScrollView unit_detail_polyline;
    private TextView roomStyleTv, bedRoomTv, bathRoomTv, directionTv, privateAreaTv;
    private TextView floorNoTv, livingRoomTv, maidRoomTv, totalAreaTv, commonAreaTv, titleItemDetailTv;

    private final List<FloorListModel> floorNumList = new ArrayList<>();
    private final List<FloorSpaceCategoryModel> floorSpaceCatList = new ArrayList<>();
    private final List<FloorSecureModel> floorSecureList = new ArrayList<>();
    private final HashMap<String, List<View>> hashMap = new HashMap<>();
    private final HashMap<String, List<View>> hashMapText = new HashMap<>();
    private final List<Region> regionList = new ArrayList<>();
    private final HashMap<Region,String> keyRegion = new HashMap<>();
    private final HashMap<String, List<Region>> removeKeyRegion = new HashMap<>();
    private List<Region> regionListToRemove = new ArrayList<>();

    private int currentItem, total, scrollDown, currentPage = 1, size = 10, currentItem2, total2, scrollDown2, currentPage2 = 1, size2 = 10;
    private Boolean isScrolling = true, isScrolling2 = true;
    private String getFloorNo, getStorey_id, property_id;
    private String layoutCanvasSize;
    private double W, H;
    private float screenHeight, val;
    private String space_id = "", unit_id = "", noteId = "";
    private boolean isCheckListCategory = true, isCheckClickDetail = true, isFirst = true, checkFirstViewCover = true;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_floor_plan);

        setRequestedOrientation( ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        Utils.hideStatusBar(this);

        initView();

        setColorClick();

        tvFloor.setOnClickListener(view -> setColorClick());

        // Screen Image
        float screenWidth = (float) Utils.getScreenWidth(this);
        val = screenWidth / (float) 1.58;
        frameLayout.getLayoutParams().width = (int) val;
        noImageLayout.getLayoutParams().width = (int) val;
        int dpToPx = Utils.dpToPx(this, 55);
        screenHeight = (float) Utils.getScreenHeight(this) - (float) dpToPx;

        userSessionManagement = new UserSessionManagement(this);
        User user = new Gson().fromJson(userSessionManagement.getUserDetail(), User.class);
        property_id = user.getActivePropertyIdFk();

        TextView tvProperty = findViewById(R.id.tvProperty);
        tvProperty.setText(user.getAccountName() != null ? user.getAccountName() : "- - -");
        tvProperty.setOnClickListener(view -> finish());

        if (checkFirstViewCover){
            viewCover.setVisibility(VISIBLE);
        }

        onProgressLoading();

        linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        floorNumRecyclerView.setLayoutManager(linearLayoutManager);

        initNumFloorRecyclerView(floorNumRecyclerView);
        initFloorListRecyclerViewScroll(floorNumRecyclerView);

        layoutManagerSecure = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        floorSecureRecycle.setLayoutManager(layoutManagerSecure);
        initSecurityRecyclerView();
        initFloorSecurityScroll();

        if (!InternetConnection.checkInternetConnection(viewCover)){
            Utils.customToastMsgError(this, getResources().getString(R.string.please_check_your_internet_connection), false);
        }
    }

    //On loading Floor
    private void onProgressLoading() {
        Sprite doubleBounce = new MultiplePulse();
        progressBar.setIndeterminateDrawable(doubleBounce);
        progressBar.setVisibility(VISIBLE);
    }

    //On loading Security
    private void onLoading2() {
        Sprite doubleBounce = new MultiplePulse();
        progressBarSecure.setIndeterminateDrawable(doubleBounce);
        progressBarSecure.setVisibility(VISIBLE);
    }

    @SuppressLint("NotifyDataSetChanged")
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void setColorClick() {
        noteId = "";
        frameLayout.setVisibility(VISIBLE);
        frameImageSecure.setVisibility(GONE);
        unit_detail_polyline.setVisibility(GONE);
        floorLayout.setVisibility(VISIBLE);
        DetailSecureLayout.setVisibility(GONE);
        tvFloor.setBackgroundTintList(ColorStateList.valueOf(Utils.getColor(this, R.color.white)));
        iDetailMore.setBackgroundTintList(ColorStateList.valueOf(Utils.getColor(this, R.color.appBarColor)));
        tvFloor.setTextColor(Utils.getColor(this, R.color.appBarColor));
        iMore.setBackgroundTintList(ColorStateList.valueOf(Utils.getColor(this, R.color.white)));
        tvDetailFloor.setVisibility(GONE);

        for (FloorSecureModel floorSecure : floorSecureList) {
            floorSecure.setClick(false);
            floorSecureAdapter.notifyDataSetChanged();
        }

        if (!isCheckListCategory) {
            spaceCategoryAdapter.notifyDataSetChanged();
            StaticUtilsKey.is_check_click_category = true;
        }

        if (!isCheckClickDetail){
            frameRemoveAllViewDraw();
            isCheckClickDetail = true;
        }

    }

    private void initView() {
        viewCover = findViewById(R.id.viewCover);
        tvFloor = findViewById(R.id.tvFloor);
        tvDetailFloor = findViewById(R.id.tvDetailFloor);
        iMore = findViewById(R.id.iMore);
        iDetailMore = findViewById(R.id.imgDetailMore);
        progressBar = findViewById(R.id.progress);
        progressBarSecure = findViewById(R.id.progress2);
        noImageLayout = findViewById(R.id.noImageLayout);
        noAvailableAll = findViewById(R.id.noItemAll);
        imageSetFloor = findViewById(R.id.imageSetFloor);

        floorNumRecyclerView = findViewById(R.id.numFloorRecycle);
        floorSpaceCatRecyclerView = findViewById(R.id.commercialRecycle);
        floorSecureRecycle = findViewById(R.id.secureRecycle);

        frameLayout = findViewById(R.id.frameImage);
        DetailSecureLayout = findViewById(R.id.DetailSecureLayout);
        floorLayout = findViewById(R.id.floorLayout);
        frameImageSecure = findViewById(R.id.frameImageSecure);
        recycleSecureListImage = findViewById(R.id.recycleSecureList);
        titleSecureCat = findViewById(R.id.TitleSecureCat);
        detailList = findViewById(R.id.detailList);

        //******************Detail unit floor*****************************
        unit_detail_polyline = findViewById(R.id.unit_detail_poly);
        roomStyleTv = findViewById(R.id.room_style_val);
        bedRoomTv = findViewById(R.id.bed_room_val);
        bathRoomTv = findViewById(R.id.bath_room_val);
        directionTv = findViewById(R.id.direction_val);
        privateAreaTv = findViewById(R.id.private_area_val);
        floorNoTv = findViewById(R.id.floor_no_val);
        livingRoomTv = findViewById(R.id.living_room_val);
        maidRoomTv = findViewById(R.id.maid_room_val);
        totalAreaTv = findViewById(R.id.total_area_val);
        commonAreaTv = findViewById(R.id.common_area_val);
        titleItemDetailTv = findViewById(R.id.titleItemDetail);

    }

    private void initNumFloorRecyclerView(RecyclerView recyclerView) {
        floorNumAdapter = new FloorNumAdapter(FloorPlanActivity.this, floorNumList, clickNumFloorCallback);
        recyclerView.setAdapter(floorNumAdapter);

        onRequestFloorList();
    }

    private void onRequestFloorList() {
        new FloorListWs().getFloorList(FloorPlanActivity.this, currentPage, 10, property_id, callBackList);
    }

    private void onRequestSpaceCategory(String storey_id, String floor_no) {
        onProgressLoading();
        new FloorListWs().getSpaceCategory(FloorPlanActivity.this, storey_id, floor_no, callBackList);
    }

    @SuppressLint("SetTextI18n")
    private final FloorListWs.FloorPlanCallBackListener callBackList = new FloorListWs.FloorPlanCallBackListener() {
        @SuppressLint("NotifyDataSetChanged")
        @Override
        public void getFloorList(List<FloorListModel> floorList) {
            progressBar.setVisibility(GONE);
            checkFirstViewCover = false;
            viewCover.setVisibility(GONE);
            floorNumList.addAll(floorList);
            floorNumAdapter.notifyDataSetChanged();

            if (!floorList.isEmpty()) {
                if (isFirst) {
                    getFloorNo = floorList.get(0).getFloorNo();
                    getStorey_id = floorList.get(0).getStoreyId();
                    layoutCanvasSize = floorList.get(0).getLayoutCanvasSize();
                    if (floorList.get(0).getBackgroundImg() != null) {
                        noImageLayout.setVisibility(GONE);
                        frameLayout.setVisibility(VISIBLE);

                        setImageFloorLayout(floorList.get(0).getBackgroundImg());
                    } else {
                        noImageLayout.setVisibility(VISIBLE);
                        frameLayout.setVisibility(GONE);
                    }

                    tvFloor.setText(getResources().getString(R.string.floor) + " " + floorList.get(0).getFloorNo());

                    if (floorNumList.size() > 0) {
                        floorNumList.get(0).setClick(true);
                    }
                    isFirst = false;
                }
            }

            for (FloorListModel floorListModel : floorList) {
                if (floorListModel.isClick()) {
                    onRequestSpaceCategory(floorListModel.getStoreyId(), floorListModel.getFloorNo());
                }
            }
            getCanvasSizeLayout(layoutCanvasSize);

            if (floorNumList.size() == 0)  {
                noAvailableAll.setVisibility(VISIBLE);
                viewCover.setVisibility(VISIBLE);
            }

        }

        @Override
        public void getSpaceCategory(List<FloorSpaceCategoryModel> spaceCategoryList) {
            progressBar.setVisibility(GONE);
            floorSpaceCatList.clear();
            floorSpaceCatList.addAll(spaceCategoryList);

            initSpaceCategoryFloorRecyclerView();

            if (floorSpaceCatList.size() > 0) {
                floorSpaceCatList.get(0).setClick(true);
            }

        }

        @SuppressLint("NotifyDataSetChanged")
        @Override
        public void getFloorSecurity(List<FloorSecureModel> secureModelList) {
            progressBarSecure.setVisibility(GONE);
            floorSecureList.addAll(secureModelList);
            floorSecureAdapter.notifyDataSetChanged();
        }

        @Override
        public void getSpacePolyline(RelativeLayout layout, List<FloorSpacePolyline> spacePolyline, String category_type, String fullNameSpace) {
            progressBar.setVisibility(GONE);
            layout.setEnabled(true);
            frameAddViewToDraw(spacePolyline, fullNameSpace, category_type, space_id);
        }

        @Override
        public void getUnitPolyline(RelativeLayout layout, List<FloorSpacePolyline> spacePolyline, String category_type, String fullNameSpace) {
            progressBar.setVisibility(GONE);
            layout.setEnabled(true);
            frameAddViewToDraw(spacePolyline, fullNameSpace, category_type, unit_id);
        }

        @Override
        public void getUnitTypePolyline(RelativeLayout layout, List<FloorSpacePolyline> spacePolyline, String category_type, String fullNameSpace) {
            progressBar.setVisibility(GONE);
            layout.setEnabled(true);
            // Before we get unit_id = spacePolyline.getId()
            frameAddViewToDraw(spacePolyline, fullNameSpace, category_type, unit_id);
        }

        @Override
        public void onFailed(String error) {
            // floorSpaceCatList.clear();   floorNumRecyclerView.setVisibility(GONE);
            progressBar.setVisibility(GONE);
            Toast.makeText(FloorPlanActivity.this, error, Toast.LENGTH_SHORT).show();
        }
    };

    private void initFloorListRecyclerViewScroll(RecyclerView recyclerViewScroll) {
        recyclerViewScroll.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true;
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                currentItem = linearLayoutManager.getChildCount();
                total = linearLayoutManager.getItemCount();
                scrollDown = linearLayoutManager.findFirstVisibleItemPosition();
                if (dy > 0) {
                    if (isScrolling && (currentItem + scrollDown == total)) {
                        if (total == size) {
                            isScrolling = false;
                            currentPage++;
                            onProgressLoading();
                            recyclerView.scrollToPosition(floorNumList.size() - 1);
                            initNumFloorRecyclerView(recyclerView);
                            size += 10;
                        }
                    }
                }
            }
        });
    }

    @SuppressLint("SetTextI18n")
    private final FloorNumAdapter.ClickCallBackListener clickNumFloorCallback = (floorNumModel) -> {
        onProgressLoading();
        floorSpaceCatList.clear();
        getFloorNo = floorNumModel.getFloorNo();
        getStorey_id = floorNumModel.getStoreyId();
        layoutCanvasSize = floorNumModel.getLayoutCanvasSize();
        onRequestSpaceCategory(floorNumModel.getStoreyId(), floorNumModel.getFloorNo());

        for (FloorListModel floorNum : floorNumList) {
            floorNum.setClick(floorNum.getFloorNo().equalsIgnoreCase(floorNumModel.getFloorNo()));
            tvFloor.setText(getResources().getString(R.string.floor) + " " + floorNumModel.getFloorNo());
            if (floorNumModel.getBackgroundImg() != null) {
                noImageLayout.setVisibility(GONE);
                frameLayout.setVisibility(VISIBLE);
                setImageFloorLayout(floorNumModel.getBackgroundImg());
            } else {
                noImageLayout.setVisibility(VISIBLE);
                frameLayout.setVisibility(GONE);
            }
        }
        floorNumAdapter.notifyDataSetChanged();
        getCanvasSizeLayout(layoutCanvasSize);

        frameRemoveAllViewDraw();

    };

    private void frameRemoveAllViewDraw(){
        boolean doBreak = false;
        while (!doBreak) {
            int childCount = frameLayout.getChildCount();
            int i;
            for(i = 0; i < childCount; i++) {
                View currentChild = frameLayout.getChildAt(i);
                if (currentChild instanceof SampleCanvasActivity) {
                    frameLayout.removeView(currentChild);
                    break;
                }
                if (currentChild instanceof TextView) {
                    frameLayout.removeView(currentChild);
                    break;
                }
            }

            if (i == childCount) {
                doBreak = true;
            }
        }
    }

    private void setImageFloorLayout(String fileString) {
        if (fileString != null) {
            byte[] imageAsBytes = Base64.decode(fileString, Base64.DEFAULT);
            imageSetFloor.setImageBitmap(
                    BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length)
            );
        }

    }

    private void getCanvasSizeLayout(String layoutCanvasSize) {
        if (layoutCanvasSize != null){
            String[] canvasSize = layoutCanvasSize.split(",");
            String w = canvasSize[0];
            String h = canvasSize[1];

            W = Double.parseDouble(w);
            H = Double.parseDouble(h);
        }
    }

    private void initSpaceCategoryFloorRecyclerView() {
        isCheckListCategory = false;
        floorSpaceCatRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        spaceCategoryAdapter = new FloorSpaceCategoryAdapter(frameLayout, FloorPlanActivity.this, userSessionManagement, getFloorNo, getStorey_id, floorSpaceCatList, clickComFloorCallback);
        floorSpaceCatRecyclerView.setAdapter(spaceCategoryAdapter);
        spaceCategoryAdapter.notifyDataSetChanged();
    }

    private final FloorSpaceCategoryAdapter.ClickBackListener clickComFloorCallback = new FloorSpaceCategoryAdapter.ClickBackListener() {
        @Override
        public void clickBackSpaceFloor(RelativeLayout layout, FloorSpaceItemModel floorSpaceItemModel, String storey_id, String category_type, String fullNameCategory) {
            space_id = floorSpaceItemModel.getId();
            onProgressLoading();

            layout.setEnabled(false);
            new FloorListWs().getSpacePolyline(layout, FloorPlanActivity.this,
                    category_type, property_id, space_id, storey_id,fullNameCategory, callBackList);
            isCheckListCategory = false;
        }

        @Override
        public void clickBackUnitFloor(RelativeLayout layout, FloorUnitItemModel floorSpaceItemModel, String category_type, String fullNameCategory) {
            unit_id = floorSpaceItemModel.getId();
            onProgressLoading();

            layout.setEnabled(false);
            new FloorListWs().getUnitPolyline(layout, FloorPlanActivity.this, category_type, property_id, unit_id, fullNameCategory,callBackList);
            isCheckListCategory = false;
        }

        @Override
        public void clickBackUnitTypeFloor(RelativeLayout layout, String floorUnitTypeId, String category_type, String floor_no, String fullNameCategory) {
            unit_id = floorUnitTypeId;
            onProgressLoading();

            layout.setEnabled(false);
            new FloorListWs().getUnitTypePolyline(layout, FloorPlanActivity.this, category_type, property_id, floorUnitTypeId, floor_no, fullNameCategory,callBackList);
            isCheckListCategory = false;
        }

        @Override
        public void clickBackSpaceFloorClose(LinearLayout layout, List<FloorSpaceItemModel> spaceItemList, String fullNameCategory) {
            layout.setClickable(false);
            frameRemoveViewFromDraw(layout, fullNameCategory);
        }

        @Override
        public void clickBackUnitFloorClose(LinearLayout layout, String fullNameCategory) {
            layout.setClickable(false);
            frameRemoveViewFromDraw(layout, fullNameCategory);
        }

        @Override
        public void clickBackUnitTypeFloorClose(LinearLayout layout, String fullNameCategory) {
            layout.setClickable(false);
            frameRemoveViewFromDraw(layout, fullNameCategory);
        }
    };

    private void initSecurityRecyclerView() {
        floorSecureAdapter = new FloorSecureAdapter(FloorPlanActivity.this, floorSecureList, clickSecFloorCallback);
        floorSecureRecycle.setAdapter(floorSecureAdapter);

        new FloorListWs().getFloorSecurity(FloorPlanActivity.this, property_id, currentPage2, 10, callBackList);
    }

    private void initFloorSecurityScroll() {
        floorSecureRecycle.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling2 = true;
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                currentItem2 = layoutManagerSecure.getChildCount();
                total2 = layoutManagerSecure.getItemCount();
                scrollDown2 = layoutManagerSecure.findFirstVisibleItemPosition();
                if (dy > 0) {
                    if (isScrolling2 && (currentItem2 + scrollDown2 == total2)) {
                        if (total2 == size2) {
                            isScrolling2 = false;
                            currentPage2++;
                            onLoading2();
                            recyclerView.scrollToPosition(floorSecureList.size() - 1);
                            initSecurityRecyclerView();
                            size2 += 10;
                        }
                    }
                }

            }
        });
    }

    private final FloorSecureAdapter.ClickBackListener clickSecFloorCallback = floorSecureModel -> {
        isCheckClickDetail = false;
        //***************Set Title Header*************************
        setHeaderTitle(floorSecureModel.getOptionText());

        //***************Set Title Header*************************
        frameLayout.setVisibility(GONE);
        frameImageSecure.setVisibility(VISIBLE);
        frameImageSecure.getLayoutParams().width = (int) val;

        floorLayout.setVisibility(GONE);
        unit_detail_polyline.setVisibility(GONE);
        DetailSecureLayout.setVisibility(VISIBLE);

        //***************Set Image For Item Secure*************************
        for (FloorSecureModel floorSecure : floorSecureList) {
            floorSecure.setClick(floorSecure.getOptionText().equalsIgnoreCase(floorSecureModel.getOptionText()));
        }

        setImageAdapterDetailFloor(floorSecureModel.getFilePath());

        // Set Description For Item Secure
        titleSecureCat.setText(floorSecureModel.getOptionText());
        detailList.setText(floorSecureModel.getOptionDesc() != null ? floorSecureModel.getOptionDesc() : getResources().getString(R.string.no_description));
    };

    private final FloorListWs.FloorDetailPolyline callBack = new FloorListWs.FloorDetailPolyline() {
        @Override
        public void getSpaceByIdPolyline(SpacePolyDetailModel spacePolylineModels) {
            progressBar.setVisibility(GONE);
            setHeaderTitle(spacePolylineModels.getSpaceName());
            isCheckClickDetail = false;

            frameLayout.setVisibility(GONE);
            frameImageSecure.setVisibility(VISIBLE);
            frameImageSecure.getLayoutParams().width = (int) val;
            floorLayout.setVisibility(GONE);
            DetailSecureLayout.setVisibility(VISIBLE);

            setImageAdapterDetailFloor(spacePolylineModels.getFilePath());
            titleSecureCat.setText(spacePolylineModels.getSpaceName());
            detailList.setText(spacePolylineModels.getDescription() != null ? spacePolylineModels.getDescription() : getResources().getString(R.string.no_description));
        }

        @Override
        public void getUnitByIdPolyline(UnitPolyDetailModel unitPolylineModels) {
            progressBar.setVisibility(GONE);
            setHeaderTitle(unitPolylineModels.getUnitNo());
            isCheckClickDetail = false;

            frameLayout.setVisibility(GONE);
            frameImageSecure.setVisibility(VISIBLE);
            frameImageSecure.getLayoutParams().width = (int) val;
            unit_detail_polyline.setVisibility(VISIBLE);
            floorLayout.setVisibility(GONE);
            DetailSecureLayout.setVisibility(GONE);

            setImageAdapterDetailFloor(unitPolylineModels.getFilePath());
            titleSecureCat.setText(unitPolylineModels.getUnitNo());
            setValue(unitPolylineModels);
        }

        @Override
        public void onFailed(String error) {
            progressBar.setVisibility(GONE);
            Toast.makeText(FloorPlanActivity.this, error, Toast.LENGTH_SHORT).show();
        }
    };

    private void setHeaderTitle(String title){
        tvDetailFloor.setVisibility(VISIBLE);
        tvDetailFloor.setBackgroundTintList(ColorStateList.valueOf(Utils.getColor(this, R.color.white)));
        tvDetailFloor.setTextColor(Utils.getColor(this, R.color.appBarColor));
        iMore.setBackgroundTintList(ColorStateList.valueOf(Utils.getColor(this, R.color.appBarColor)));
        iDetailMore.setBackgroundTintList(ColorStateList.valueOf(Utils.getColor(this, R.color.white)));

        tvFloor.setBackgroundTintList(ColorStateList.valueOf(Utils.getColor(this, R.color.appBarColor)));
        tvFloor.setTextColor(Utils.getColor(this, R.color.white));
        tvDetailFloor.setText(title);
    }

    private void setImageAdapterDetailFloor(List<String> getFilePath){
        if (getFilePath.size() > 0){
            AbsoluteFitLayoutManager absolutefitLayourManager = new AbsoluteFitLayoutManager(FloorPlanActivity.this, 1, RecyclerView.HORIZONTAL, false, 1);
            recycleSecureListImage.setLayoutManager(absolutefitLayourManager);
            ImageSecurityAdapter imageSecurityAdapter = new ImageSecurityAdapter(getFilePath, urlImage -> {
                Intent intent = new Intent(FloorPlanActivity.this, ZoomImageDetailActivity.class);
                intent.putExtra("image", urlImage);
                startActivity(intent);
            });
            recycleSecureListImage.setAdapter(imageSecurityAdapter);
        } else {
            frameImageSecure.setVisibility(GONE);
            noImageLayout.setVisibility(VISIBLE);
            noImageLayout.setText(getResources().getString(R.string.not_available_image));
            frameLayout.setVisibility(GONE);
        }

    }

    private void setValue(UnitPolyDetailModel unitPolylineModels){
        titleItemDetailTv.setText(unitPolylineModels.getUnitNo() != null ? String.format("%s", unitPolylineModels.getUnitNo()) : ". . .");
        roomStyleTv.setText(unitPolylineModels.getStyle() != null ? String.format("%s", unitPolylineModels.getStyle()) : ". . .");
        bedRoomTv.setText(unitPolylineModels.getTotalBedroom() != null ? String.format("%s", unitPolylineModels.getTotalBedroom()) : ". . .");
        bathRoomTv.setText(unitPolylineModels.getTotalBathroom() != null ? String.format("%s", unitPolylineModels.getTotalBathroom()) : ". . .");
        directionTv.setText(unitPolylineModels.getDirection() != null ? String.format("%s", unitPolylineModels.getDirection()) : ". . .");
        privateAreaTv.setText(unitPolylineModels.getTotalPrivateArea() != null ? String.format("%s", unitPolylineModels.getTotalPrivateArea()) : ". . .");
        floorNoTv.setText(unitPolylineModels.getFloorNo() != null ? String.format("%s", unitPolylineModels.getFloorNo()) : ". . .");
        livingRoomTv.setText(unitPolylineModels.getTotalLivingRoom() != null ? String.format("%s", unitPolylineModels.getTotalLivingRoom()) : ". . .");
        maidRoomTv.setText(unitPolylineModels.getTotalMaidRoom() != null ? String.format("%s", unitPolylineModels.getTotalMaidRoom()) : ". . .");
        totalAreaTv.setText(unitPolylineModels.getTotalAreaSqm() != null ? String.format("%s", unitPolylineModels.getTotalAreaSqm()) : ". . .");
        commonAreaTv.setText(unitPolylineModels.getTotalCommonArea() != null ? String.format("%s", unitPolylineModels.getTotalCommonArea()) : ". . .");

    }

    private void drawTextAnimation(Point pointCenter, TextView txt) {
        TranslateAnimation animation = new TranslateAnimation(0, pointCenter.x - 10, 0, pointCenter.y - 10);
        animation.setRepeatMode(0);
        animation.setDuration(1000);
        animation.setFillAfter(true);
        txt.startAnimation(animation);
        spinAnimation(txt);
    }

    private void spinAnimation(TextView txt) {
        ValueAnimator valueAnimator = ValueAnimator.ofFloat(0f, 500f);
        valueAnimator.setRepeatCount(ValueAnimator.INFINITE);
        valueAnimator.setRepeatMode(ValueAnimator.RESTART);
        valueAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
        valueAnimator.setDuration(3000);
        valueAnimator.addUpdateListener(animation1 -> {
            float progress = (float) animation1.getAnimatedValue();
            txt.setRotationY(progress);
        });
        valueAnimator.start();
    }

    public Point polygonCenterOfMass(List<Point> list) {
        int nr = list.size();
        double centerX = 0.0;
        double centerY = 0.0;
        double area = signedPolygonArea(list);
        for (int i = 0; i < nr; i++) {
            int j = (i + 1) % nr;
            double factor1 = list.get(i).x * list.get(j).y - list.get(j).x * list.get(i).y;
            centerX = centerX + (list.get(i).x + list.get(j).x) * factor1;
            centerY = centerY + (list.get(i).y + list.get(j).y) * factor1;
        }
        area = area * 6.0;
        double factor2 = 1.0 / area;
        centerX = centerX * factor2;
        centerY = centerY * factor2;
        return (new Point((int) centerX, (int) centerY));
    }

    public double signedPolygonArea(List<Point> list) {
        int nr = list.size();
        double area = 0;
        for (int i = 0; i < nr; i++) {
            int j = (i + 1) % nr;
            area = area + list.get(i).x * list.get(j).y;
            area = area - list.get(i).y * list.get(j).x;
        }
        area = area / 2.0;
        return area;
    }

    private void frameAddViewToDraw(List<FloorSpacePolyline> spacePolyline, String fullNameCategory, String category_type, String id) {
        double x, y;
        double newX, newY;
        List<Point> list = new ArrayList<>();
        List<Point> allList = new ArrayList<>();
        List<View> listView = new ArrayList<>();
        List<View> listViewText = new ArrayList<>();
        String colorFill;
        Point pointCenter;
        for (FloorSpacePolyline floorSpacePolyline : spacePolyline) {
            colorFill = floorSpacePolyline.getFill();
            for (FloorSpacePolyline.Point point : floorSpacePolyline.getPoints()) {
                x = point.getX();
                y = point.getY();
                newX = (x * val) / W;
                newY = (y * screenHeight) / H;
                list.add(new Point((int) newX, (int) newY));
            }
            allList.addAll(list);

            pointCenter = polygonCenterOfMass(allList);

            TextView txtDK = new TextView(FloorPlanActivity.this);
            txtDK.setText(getResources().getString(R.string.dk));
            txtDK.setTextColor(Utils.getColor(this, R.color.appBarColor));
            txtDK.setTextSize(8);
            txtDK.setLayoutParams(new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT));
            txtDK.setPadding(2,2,2,2);
            txtDK.setBackground(Utils.setDrawable(this, R.drawable.circle_border_black));
            txtDK.setBackgroundTintList(ColorStateList.valueOf(Utils.getColor(this, R.color.white)));
            txtDK.setGravity(Gravity.CENTER);
            SampleCanvasActivity sampleCanvasActivity = new SampleCanvasActivity(FloorPlanActivity.this, fullNameCategory, allList, pointCenter, colorFill, category_type, id);
            RelativeLayout.LayoutParams thisLinParams = new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            sampleCanvasActivity.setLayoutParams(thisLinParams);

            listView.add(sampleCanvasActivity);
            frameLayout.addView(sampleCanvasActivity);

            drawTextAnimation(pointCenter, txtDK);
            listViewText.add(txtDK);
            frameLayout.addView(txtDK);
            frameLayout.invalidate();

            list = new ArrayList<>();
            allList = new ArrayList<>();

        }
        hashMap.put(fullNameCategory, listView);
        hashMapText.put(fullNameCategory, listViewText);
    }

    public class SampleCanvasActivity extends View {
        private final Paint paint = new Paint();
        private final Path path = new Path();
        private final List<Point> list;
        private final String fillColor;
        private Point pointCenter;
        private final String category_type, id, fullNameCategory;

        public SampleCanvasActivity(Context context, String fullNameCategory,List<Point> list, Point pointCenter, String fillColor, String category_type, String id) {
            super(context);
            this.list = list;
            this.fillColor = fillColor;
            this.pointCenter = pointCenter;
            this.category_type = category_type;
            this.id = id;
            this.fullNameCategory = fullNameCategory;
        }


        @SuppressLint("ClickableViewAccessibility")
        @Override
        public boolean onTouchEvent(MotionEvent event) {
            super.onTouchEvent(event);
            Point point = new Point();
            point.x = (int) event.getX();
            point.y = (int) event.getY();
            invalidate();

            switch(event.getAction())
            {
                case MotionEvent.ACTION_UP:
                case MotionEvent.ACTION_MOVE:
                    break;
                case MotionEvent.ACTION_CANCEL:
                case MotionEvent.ACTION_DOWN:
                    for (Region reg : regionList){
                        if(reg.contains(point.x, point.y)) {
                            if (category_type.equalsIgnoreCase("unit")){
                                onProgressLoading();
                                new FloorListWs().getDetailUnitPolyline(FloorPlanActivity.this, keyRegion.get(reg), callBack);
                            } else if (category_type.equalsIgnoreCase("unit_type")){
                                onProgressLoading();
                                new FloorListWs().getDetailUnitPolyline(FloorPlanActivity.this, keyRegion.get(reg), callBack);
                            } else {
                                onProgressLoading();
                                new FloorListWs().getDetailSpacePolyline(FloorPlanActivity.this, keyRegion.get(reg), callBack);
                            }

                            break;
                        }
                    }
                    break;
            }
            return true;
        }

        @SuppressLint("DrawAllocation")
        @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);
            paint.setStrokeWidth(1);
            paint.setColor(Color.parseColor(fillColor));
            paint.setAlpha(99);
            paint.setStyle(Paint.Style.FILL_AND_STROKE);

            path.setFillType(Path.FillType.EVEN_ODD);
            path.moveTo(list.get(0).x, list.get(0).y);
            for (int i = 0; i < list.size(); i++) {
                path.lineTo(list.get(i).x, list.get(i).y);
            }
            path.close();
            canvas.drawPath(path, paint);

            RectF rectF = new RectF();
            path.computeBounds(rectF, true);
            Region r = new Region();
            r.setPath(path, new Region((int) rectF.left, (int) rectF.top, (int) rectF.right, (int) rectF.bottom));
            regionList.add(r);

            if (id.equals(noteId)){
                regionListToRemove.add(r);
            } else if (noteId.equals("")){
                noteId = id;
                regionListToRemove.add(r);
            } else {
                noteId = id;
                regionListToRemove = new ArrayList<>();
                regionListToRemove.add(r);
            }
            removeKeyRegion.put(fullNameCategory, regionListToRemove);
            keyRegion.put(r,id);

        }
    }

    private void frameRemoveViewFromDraw(LinearLayout layout, String fullNameCategory) {
        if (hashMap.get(fullNameCategory) != null && Objects.requireNonNull(hashMap.get(fullNameCategory)).size() > 0 && Objects.requireNonNull(hashMapText.get(fullNameCategory)).size() > 0 && Objects.requireNonNull(removeKeyRegion.get(fullNameCategory)).size() > 0) {
            for (int i = 0; i < Objects.requireNonNull(hashMap.get(fullNameCategory)).size(); i++) {
                frameLayout.removeView(Objects.requireNonNull(hashMap.get(fullNameCategory)).get(i));
                frameLayout.removeView(Objects.requireNonNull(hashMapText.get(fullNameCategory)).get(i));
            }
            for (int j = 0; j < Objects.requireNonNull(removeKeyRegion.get(fullNameCategory)).size() ; j++){
                regionList.remove(Objects.requireNonNull(removeKeyRegion.get(fullNameCategory)).get(j));
            }
        }
        layout.setClickable(true);
    }

}