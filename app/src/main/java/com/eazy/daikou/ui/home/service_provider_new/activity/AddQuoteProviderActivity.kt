package com.eazy.daikou.ui.home.service_provider_new.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import com.eazy.daikou.R

class AddQuoteProviderActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_quote_provider)

        findViewById<ImageView>(R.id.iconClose).setOnClickListener { finish() }
    }

    private fun initView(){

    }
    private fun initAction(){

    }
}