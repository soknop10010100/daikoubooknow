package com.eazy.daikou.ui.home.hrm.over_time

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.request_data.request.hr_ws.OvertimeWs
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.hr.ItemOvertimeModel
import com.eazy.daikou.model.profile.User
import com.eazy.daikou.ui.home.hrm.over_time.adapter.SelectItemOvertimeAdapter
import com.eazy.daikou.ui.home.hrm.over_time.adapter.SelectItemOvertimeAdapter.ClickItemOvertimeCallBack
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.gson.Gson

class ItemOvertimeFragment : BottomSheetDialogFragment() {

    private lateinit var progressBar: ProgressBar
    private lateinit var recyclerView: RecyclerView
    private lateinit var btnCancel: ImageView
    private lateinit var editSearchNameApprove: EditText
    private lateinit var titleNameOvertime: TextView
    private lateinit var noDataLayout: LinearLayout
    private lateinit var btnClearSearch: ImageView
    private var itemOvertimeModelList :  ArrayList<ItemOvertimeModel> = ArrayList()
    private var actionKey = ""
    private var userKey = ""
    private var mContext : Context? = null
    private lateinit var selectEmployeeAdapter: SelectItemOvertimeAdapter
    private lateinit var callBackItem: CallBackItemToCreate

    fun newInstance(actionType: String,itemModelList:  ArrayList<ItemOvertimeModel>): ItemOvertimeFragment {
        val fragment = ItemOvertimeFragment()
        val args = Bundle()
        args.putSerializable("list_item_overtime", itemModelList)
        args.putString("action_type", actionType)
        fragment.arguments = args
        return fragment
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        setupFullHeight()

        val view = inflater.inflate(R.layout.fragment_item_overtime, container, false)

        initView(view)

        initAction()

        return view
    }


    private fun setupFullHeight() {
        val bottomSheetDialog = dialog as BottomSheetDialog
        val behavior = bottomSheetDialog.behavior
        behavior.state = BottomSheetBehavior.STATE_EXPANDED
    }

    private fun initView(view: View){
        progressBar = view.findViewById(R.id.progressItem)
        recyclerView = view.findViewById(R.id.recyclerSelectOvertime)
        btnCancel = view.findViewById(R.id.btnCancel)
        titleNameOvertime = view.findViewById(R.id.titleNameOvertime)
        editSearchNameApprove = view.findViewById(R.id.editSearchNameApprove)
        btnClearSearch = view.findViewById(R.id.btnClearSearch)
        noDataLayout = view.findViewById(R.id.noDataLayout)
    }
    private fun initAction(){

        val user : User = Gson().fromJson(UserSessionManagement(mContext).userDetail, User::class.java)
        if (user.userBusinessKey != null)   userKey = user.userBusinessKey

        setSearchNameApprove(userKey)
        btnCancel.setOnClickListener { dismiss() }
        if (arguments != null) {
            actionKey = requireArguments().getString("action_type").toString()
        }
        if (arguments != null){
            itemOvertimeModelList = requireArguments().getSerializable("list_item_overtime") as ArrayList<ItemOvertimeModel>
        }
        initRecycler()

        noDataLayout.visibility = View.GONE
        progressBar.visibility = View.GONE
    }

    private fun requestServiceApi(userBusinessKey : String, keySearch : String){
        progressBar.visibility = View.VISIBLE
        OvertimeWs().getEmployeeNameListWs(mContext!!, userBusinessKey, keySearch, onCallBackItem)
    }
    private val onCallBackItem : OvertimeWs.OnCallBackEmployeeNameListener = object : OvertimeWs.OnCallBackEmployeeNameListener{
        override fun onLoadSuccessFull(itemOvertimeModel: ArrayList<ItemOvertimeModel>) {
            progressBar.visibility = View.GONE
            itemOvertimeModelList.clear()
            itemOvertimeModelList.addAll(itemOvertimeModel)
            if (itemOvertimeModelList.size > 0) {
                noDataLayout.visibility = View.GONE
                recyclerView.visibility = View.VISIBLE
            } else {
                noDataLayout.visibility = View.VISIBLE
                recyclerView.visibility = View.GONE
            }
            selectEmployeeAdapter.notifyDataSetChanged()
        }

        override fun onLoadFailed(message: String) {
            progressBar.visibility = View.GONE
            Utils.customToastMsgError(mContext, message, false)
        }

    }
    private fun  initRecycler(){
        when (actionKey) {
            "overtime_category" -> {
                titleNameOvertime.text = Utils.getText(mContext, R.string.choose_category_overtime)
            }
            "category_shift" -> {
                titleNameOvertime.text = Utils.getText(mContext, R.string.choose_categoty_shift)
            }
            "approved" -> {
                titleNameOvertime.text = Utils.getText(mContext, R.string.choose_approved_by)
                editSearchNameApprove.visibility = View.VISIBLE
            }
        }
        val layoutManager = LinearLayoutManager(mContext)
        recyclerView.layoutManager = layoutManager
        selectEmployeeAdapter = SelectItemOvertimeAdapter(itemOvertimeModelList, actionKey, clickItemOvertimeCallBack)
        recyclerView.adapter = selectEmployeeAdapter
    }

    private val clickItemOvertimeCallBack: ClickItemOvertimeCallBack = object : ClickItemOvertimeCallBack{
        override fun clickItemCallBackListener(itemOvertimeModel: ItemOvertimeModel) {
            callBackItem.clickItemCallBack(itemOvertimeModel)
            dismiss()
        }
    }

    private fun setSearchNameApprove(userBusinessKey: String) {
        editSearchNameApprove.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(editable: Editable) {
                btnClearSearch.setOnClickListener {
                    editSearchNameApprove.setText("")
                    editable.clear()
                }
                if (editable.toString().isNotEmpty()) {
                    progressBar.visibility = View.VISIBLE
                    btnClearSearch.visibility = View.VISIBLE
                    itemOvertimeModelList.clear()
                    requestServiceApi(userBusinessKey, editable.toString())
                } else {
                    itemOvertimeModelList.clear()
                    btnClearSearch.visibility = View.GONE
                    requestServiceApi(userBusinessKey, "")
                }
            }
        })
    }


    fun initListener(callBackItem: CallBackItemToCreate){
      this.callBackItem = callBackItem
    }
    interface CallBackItemToCreate{
        fun clickItemCallBack(itemOvertimeModel: ItemOvertimeModel)

    }
    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
    }
}