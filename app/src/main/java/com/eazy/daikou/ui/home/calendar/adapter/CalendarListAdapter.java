package com.eazy.daikou.ui.home.calendar.adapter;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.eazy.daikou.R;
import com.eazy.daikou.helper.YearView;

import java.util.List;

public class CalendarListAdapter extends RecyclerView.Adapter<CalendarListAdapter.ViewHolder> implements YearView.MonthGestureListener {

    private final List<Integer> list;
    private final CalendarCallback calendarCallback;

    public CalendarListAdapter(List<Integer> list, CalendarCallback calendarCallback) {
        this.list = list;
        this.calendarCallback = calendarCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.calendar_model,parent,false);
        return new ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (list.get(position) != null){
            holder.textYear.setText("" + this.list.get(position));
            holder.yearView.setYear(this.list.get(position));
            holder.yearView.setMonthGestureListener(this);
        }


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void onMonthClick(long timeInMillis) {
        calendarCallback.callback(timeInMillis);
    }

    @Override
    public void onMonthLongClick(long timeInMillis) {

    }

    @Override
    public void onDayClick(long timeInMillis) {

    }

    @Override
    public void onDayLongClick(long timeInMillis) {

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView textYear;
        private final YearView yearView;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textYear = itemView.findViewById(R.id.txtYear);
            yearView = itemView.findViewById(R.id.yearView);
        }
    }

    public int getYearPosition(int year){
        return list.indexOf(year);
    }
    public interface CalendarCallback{
        void callback(long time);
    }
}
