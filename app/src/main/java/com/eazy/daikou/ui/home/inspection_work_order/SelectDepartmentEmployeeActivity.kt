package com.eazy.daikou.ui.home.inspection_work_order

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.widget.AbsListView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.request_data.request.work_oder_ws.WorkOrderWs
import com.eazy.daikou.request_data.static_data.StaticUtilsKey
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.my_property.service_provider.SelectPropertyModel
import com.eazy.daikou.model.profile.User
import com.eazy.daikou.model.inspection.Author_ClerkUserInfo
import com.eazy.daikou.ui.home.service_provider.adapter.ItemCategoryAdapter
import com.google.gson.Gson

class SelectDepartmentEmployeeActivity : BaseActivity() {

    private lateinit var recyclerView : RecyclerView
    private var action : String = ""
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var itemCategoryAdapter : ItemCategoryAdapter
    private var departmentList : ArrayList<SelectPropertyModel> = ArrayList()
    private var selectEmployeeDepartmentList : ArrayList<Author_ClerkUserInfo> = ArrayList()
    private lateinit var progressBar: ProgressBar
    private var departmentId : String = ""
    private var assigneeId : String = ""

    private var currentItem = 0
    private  var total : Int = 0
    private  var scrollDown : Int = 0
    private  var currentPage : Int = 1
    private  var size : Int = 20
    private var isScrolling = false
    private lateinit var noItemTv : TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_dartment_employee)

        // initView
        recyclerView = findViewById(R.id.list_nationality)
        progressBar = findViewById(R.id.progressItem)
        noItemTv = findViewById(R.id.noItemTv)

        // initData
        if (intent != null && intent.hasExtra("action")) {
            action = intent.getStringExtra("action").toString()
        }

        if (intent != null && intent.hasExtra("assignee_id")) {
            assigneeId = intent.getStringExtra("assignee_id").toString()
        }

        if (intent != null && intent.hasExtra("department_id")) {
            departmentId = intent.getStringExtra("department_id").toString()
        }

        //initAction
        linearLayoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = linearLayoutManager

        initItemRecyclerView()
        if (action.equals(StaticUtilsKey.assignee_employee_action, ignoreCase = true)) {
            title = resources.getString(R.string.assignee)
            onScrollItemRecyclerView()
        } else {
            title = resources.getString(R.string.select_department)
        }

    }

    private fun initItemRecyclerView(){
        if (action.equals(StaticUtilsKey.assignee_employee_action, ignoreCase = true)) {
            itemCategoryAdapter = ItemCategoryAdapter(selectEmployeeDepartmentList, clickDepartmentCallBack)
            recyclerView.adapter = itemCategoryAdapter

            requestServiceList()
        } else {
            itemCategoryAdapter = ItemCategoryAdapter(departmentList, action, onClickAssigneeListener)
            recyclerView.adapter = itemCategoryAdapter

            requestServiceList()
        }
    }

    private fun onScrollItemRecyclerView() {
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true
                }
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                currentItem = linearLayoutManager.childCount
                total = linearLayoutManager.itemCount
                scrollDown = linearLayoutManager.findFirstVisibleItemPosition()
                if (dy > 0) {
                    if (isScrolling && currentItem + scrollDown == total) {
                        if (total == size) {
                            isScrolling = false
                            currentPage++
                            progressBar.visibility = View.VISIBLE
                            recyclerView.scrollToPosition(departmentList.size - 1)
                            initItemRecyclerView()
                            size += 20
                        } else {
                            progressBar.visibility = View.GONE
                        }
                    }
                }
            }
        })
    }

    private fun requestServiceList(){
        if (action.equals(StaticUtilsKey.assignee_employee_action, ignoreCase = true)) {
            WorkOrderWs()
                .selectEmployeeDepartment(this, 1, 10,
                Gson().fromJson(UserSessionManagement(this).userDetail, User::class.java).accountId, departmentId, onCallBackServiceListener )
        } else {
            WorkOrderWs().selectDepartment(this, Gson().fromJson(UserSessionManagement(this).userDetail, User::class.java).accountId, onCallBackServiceListener )
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private var onCallBackServiceListener = object : WorkOrderWs.CallBackDepartmentListener{
        override fun onSuccessAssignee(assigneeEmployee: MutableList<Author_ClerkUserInfo>) {
            progressBar.visibility = View.GONE
            selectEmployeeDepartmentList.addAll(assigneeEmployee)

            for (selectDepartment in selectEmployeeDepartmentList) {
                selectDepartment.isClick = selectDepartment.id == assigneeId
            }

            noItemTv.visibility = if(selectEmployeeDepartmentList.size == 0) View.VISIBLE else View.GONE
            recyclerView.visibility = if(selectEmployeeDepartmentList.size > 0) View.VISIBLE else View.GONE

            itemCategoryAdapter.notifyDataSetChanged()
        }

        override fun onSuccessDepartment(assigneeEmployee: MutableList<SelectPropertyModel>) {
            progressBar.visibility = View.GONE
            departmentList.addAll(assigneeEmployee)

            for (selectAssigneeDepartment in departmentList) {
                selectAssigneeDepartment.isClick = selectAssigneeDepartment.id == departmentId
            }

            noItemTv.visibility = if(departmentList.size == 0) View.VISIBLE else View.GONE
            recyclerView.visibility = if(departmentList.size > 0) View.VISIBLE else View.GONE

            itemCategoryAdapter.notifyDataSetChanged()
        }

        override fun onFailed(msg: String?) {
            progressBar.visibility = View.GONE
            Utils.customToastMsgError(this@SelectDepartmentEmployeeActivity, msg, false)
        }

    }

    private var onClickAssigneeListener = ItemCategoryAdapter.ClickPropertyCallBack { floorSpinnerModel ->
        onFinishResult(action, if (floorSpinnerModel.id != null) floorSpinnerModel.id else "",
            if (floorSpinnerModel.departmentName != null) floorSpinnerModel.departmentName else "")
    }

    private var clickDepartmentCallBack  = ItemCategoryAdapter.ClickEmployeeCallBack { clerkUserInfo ->
        onFinishResult(action, if(clerkUserInfo.employeeId != null) clerkUserInfo.employeeId else "",
            if (clerkUserInfo.employeeFullName != null) clerkUserInfo.employeeFullName else "")
    }

    private fun onFinishResult(action : String, id : String, name : String){
        val intent = intent
        intent.putExtra("action", action)
        intent.putExtra("name", name)
        intent.putExtra("id", id)
        setResult(RESULT_OK, intent)
        finish()
    }

}