package com.eazy.daikou.ui.home.book_now.booking_hotel

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import android.view.MenuItem
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.helper.GetDataUtils
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.ui.home.book_now.booking_hotel.visit.HotelVisitShowFragment
import com.google.android.material.bottomnavigation.BottomNavigationView

class HotelBookingMainActivity : BaseActivity(){

    private var lastPressedTime: Long = 0
    private val PERIOD = 2000
    private var action = ""

    private lateinit var active : Fragment
    private var fragmentManager: FragmentManager = supportFragmentManager
    private lateinit var homeFragment: Fragment
    private lateinit var myDocumentFragment: Fragment
    private lateinit var visitShowFragment: Fragment
    private lateinit var calendarFragment: Fragment
    private lateinit var websiteFragment: Fragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_property)

        initAction()

    }

    @SuppressLint("WrongViewCast")
    private fun initAction(){
        val navView: BottomNavigationView = findViewById(R.id.bottomNavigationBar)

        action = GetDataUtils.getDataFromString("action", this)

        Utils.getLastVersionApp(this)

        setFragment()

        navView.setOnItemSelectedListener { item: MenuItem ->
            when (item.itemId) {
                R.id.mHome -> {
                    replaceFragment(homeFragment as HotelBookingHomePageFragment)
                    return@setOnItemSelectedListener true
                }
                R.id.mSearch -> {
                    replaceFragment(myDocumentFragment as HotelBookingSearchHotelFragment)
                    return@setOnItemSelectedListener true
                }
                R.id.mVisit ->{
                    replaceFragment(visitShowFragment as HotelVisitShowFragment)
                    return@setOnItemSelectedListener true
                }
                R.id.mMap -> {
                    replaceFragment(calendarFragment as HotelMapsFragment)
                    return@setOnItemSelectedListener true
                }
                R.id.mProfile -> {
                    replaceFragment(websiteFragment as HotelBookingProfileFragment)
                    return@setOnItemSelectedListener true
                }
            }
            false
        }

        if (action == "success_payment"){
            val videoUrl = Intent(this, HotelBookingHistoryActivity::class.java)
            videoUrl.putExtra("action", "hotel_history_hotel")
            startActivity(videoUrl)
        }
    }

    private fun setFragment(){
        homeFragment = HotelBookingHomePageFragment()
        myDocumentFragment = HotelBookingSearchHotelFragment()
        visitShowFragment = HotelVisitShowFragment()
        calendarFragment = HotelMapsFragment()
        websiteFragment = HotelBookingProfileFragment()
        active = homeFragment

        fragmentManager.beginTransaction().add(R.id.nav_host_fragment, myDocumentFragment, "2").hide(myDocumentFragment).commit()
        fragmentManager.beginTransaction().add(R.id.nav_host_fragment, visitShowFragment, "3").hide(visitShowFragment).commit()
        fragmentManager.beginTransaction().add(R.id.nav_host_fragment, calendarFragment, "4").hide(calendarFragment).commit()
        fragmentManager.beginTransaction().add(R.id.nav_host_fragment, websiteFragment, "5").hide(websiteFragment).commit()

    }

    private fun replaceFragment(fragment: Fragment) {
        fragmentManager.beginTransaction().hide(active).show(fragment).commit()
        active = fragment
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (event.keyCode == KeyEvent.KEYCODE_BACK) {
            when (event.action) {
                KeyEvent.ACTION_DOWN -> {
                    if (event.downTime - lastPressedTime < PERIOD) {
                        finishAffinity()
                    } else {
                        Toast.makeText(this, resources.getString(R.string.please_press_back_again_to_exit), Toast.LENGTH_SHORT).show()
                        lastPressedTime = event.eventTime
                    }
                    return true
                }
            }
        }
        return false
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        Utils.logDebug("ljekkkkkkkkkkkkkkkkkkkkkkkkkkk", "restore")
        // this.recreate()
    }
}