package com.eazy.daikou.ui.home.inspection_work_order.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.daimajia.swipe.SwipeLayout;
import com.eazy.daikou.R;
import com.eazy.daikou.request_data.static_data.StaticUtilsKey;
import com.eazy.daikou.helper.Utils;
import com.eazy.daikou.model.inspection.SubItemTemplateAPIModel;

import java.util.List;
import java.util.Locale;

public class ParentTaskListAdapter extends RecyclerView.Adapter<ParentTaskListAdapter.ItemViewHolder> {

    private final List<SubItemTemplateAPIModel> parentTaskWorkOrderModelList;
    private final Context context;
    private final String optionType;

    public ParentTaskListAdapter(List<SubItemTemplateAPIModel> parentTaskWorkOrderModelList, String optionType, Context context) {
        this.parentTaskWorkOrderModelList = parentTaskWorkOrderModelList;
        this.context = context;
        this.optionType = optionType;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_sub_template_form, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        SubItemTemplateAPIModel parentTaskWorkOrderModel = parentTaskWorkOrderModelList.get(position);
        if (parentTaskWorkOrderModel != null){

            Utils.setValueOnText(holder.subItemDescription, parentTaskWorkOrderModel.getItemDesc());
            Utils.setValueOnText(holder.subItemNameTv, parentTaskWorkOrderModel.getItemName());
            Utils.setValueOnText(holder.subItemCondition, parentTaskWorkOrderModel.getItemCondition());
            Utils.setValueOnText(holder.subItemNameTv2, parentTaskWorkOrderModel.getItemName());

            holder.subItemNameTv.setEnabled(false);
            holder.subItemNameTv2.setEnabled(false);
            holder.subItemCondition.setEnabled(false);
            holder.subItemDescription.setEnabled(false);

            holder.swipeLayout.setSwipeEnabled(false);

            //Check Action Option Type
            String keyMenu;
            if (optionType != null){
                keyMenu = optionType;
            } else {
                keyMenu = StaticUtilsKey.detail_button_key;
            }

            if (keyMenu.equalsIgnoreCase(StaticUtilsKey.simplify_button_key)) {
                holder.simplifyLayout.setVisibility(View.VISIBLE);
                holder.detailLayout.setVisibility(View.GONE);
                holder.titleCleanTv.setVisibility(View.VISIBLE);    holder.titleUnDamageTv.setVisibility(View.VISIBLE);    holder.titleWorkingTv.setVisibility(View.VISIBLE);
            } else if (keyMenu.equalsIgnoreCase(StaticUtilsKey.question_button_key)){
                holder.simplifyLayout.setVisibility(View.VISIBLE);
                holder.detailLayout.setVisibility(View.GONE);
                holder.titleCleanTv.setVisibility(View.GONE);    holder.titleUnDamageTv.setVisibility(View.GONE);    holder.titleWorkingTv.setVisibility(View.GONE);
            } else if (keyMenu.equalsIgnoreCase(StaticUtilsKey.detail_button_key)){
                holder.simplifyLayout.setVisibility(View.GONE);
                holder.detailLayout.setVisibility(View.VISIBLE);
            }

            //Set Color Background Item
            if (keyMenu.equalsIgnoreCase(StaticUtilsKey.simplify_button_key)) {
                String getCondition;
                if (parentTaskWorkOrderModel.getItemCondition() != null) {
                    getCondition = parentTaskWorkOrderModel.getItemCondition();
                } else {
                    getCondition = "N/A; N/A; N/A";
                }
                String[] conditionArr = getCondition.split(";");

                if (conditionArr[0].replace(" ", "").equalsIgnoreCase(Utils.getText(context, R.string.yes).toUpperCase(Locale.ROOT))) {
                    setBackgroundTintColor(context, holder.cleanTv, R.color.green);
                    holder.cleanTv.setText(Utils.getText(context, R.string.yes).toUpperCase(Locale.ROOT));
                } else if (conditionArr[0].replace(" ", "").equalsIgnoreCase(Utils.getText(context, R.string.no))) {
                    setBackgroundTintColor(context, holder.cleanTv, R.color.red);
                    holder.cleanTv.setText(Utils.getText(context, R.string.no));
                } else {
                    setBackgroundTintColor(context, holder.cleanTv, R.color.gray);
                    holder.cleanTv.setText("N/A");
                }
            } else if (keyMenu.equalsIgnoreCase(StaticUtilsKey.question_button_key)){
                holder.cleanTv.setText(Utils.getText(context, R.string.yes).toUpperCase(Locale.ROOT));
                holder.unDamageTv.setText(Utils.getText(context, R.string.no));

                String getCondition;
                if (parentTaskWorkOrderModel.getItemCondition() != null) {
                    getCondition = parentTaskWorkOrderModel.getItemCondition();
                } else {
                    getCondition = "YES; NO; N/A";
                }
                String[] conditionArr = getCondition.split(";");

                if (Utils.getText(context, R.string.yes).toUpperCase(Locale.ROOT).equalsIgnoreCase(conditionArr[0].replace(" ", ""))) {

                    setBackgroundTintColor(context, holder.cleanTv, R.color.green);
                    setBackgroundTintColor(context, holder.unDamageTv, R.color.gray);
                    setBackgroundTintColor(context, holder.workingTv, R.color.gray);

                } else if (Utils.getText(context, R.string.no).equalsIgnoreCase(conditionArr[0].replace(" ", ""))) {

                    setBackgroundTintColor(context, holder.cleanTv, R.color.gray);
                    setBackgroundTintColor(context, holder.unDamageTv, R.color.red);
                    setBackgroundTintColor(context, holder.workingTv, R.color.gray);

                } else if ("N/A".equalsIgnoreCase(conditionArr[0].replace(" ", "")) || "".equalsIgnoreCase(conditionArr[0].replace(" ", ""))) {

                    setBackgroundTintColor(context, holder.cleanTv, R.color.gray);
                    setBackgroundTintColor(context, holder.unDamageTv, R.color.gray);
                    setBackgroundTintColor(context, holder.workingTv, R.color.blue);

                } else {
                    setBackgroundTintColor(context, holder.cleanTv, R.color.gray);
                    setBackgroundTintColor(context, holder.unDamageTv, R.color.gray);
                    setBackgroundTintColor(context, holder.workingTv, R.color.blue);
                }
            }

            if (keyMenu.equalsIgnoreCase(StaticUtilsKey.simplify_button_key)) {
                String getCondition;
                if (parentTaskWorkOrderModel.getItemCondition() != null) {
                    getCondition = parentTaskWorkOrderModel.getItemCondition();
                } else {
                    getCondition = "N/A; N/A; N/A";
                }
                String[] conditionArr = getCondition.split(";");
                if (conditionArr[1].replace(" ", "").equalsIgnoreCase(Utils.getText(context, R.string.yes).toUpperCase(Locale.ROOT))) {
                    setBackgroundTintColor(context, holder.unDamageTv, R.color.green);
                    holder.unDamageTv.setText(Utils.getText(context, R.string.yes).toUpperCase(Locale.ROOT));
                } else if (conditionArr[1].replace(" ", "").equalsIgnoreCase(Utils.getText(context, R.string.no))) {
                    setBackgroundTintColor(context, holder.unDamageTv, R.color.red);
                    holder.unDamageTv.setText(Utils.getText(context, R.string.no));
                } else {
                    setBackgroundTintColor(context, holder.unDamageTv, R.color.gray);
                    holder.unDamageTv.setText("N/A");
                }
            }

            if (keyMenu.equalsIgnoreCase(StaticUtilsKey.simplify_button_key)) {
                String getCondition;
                if (parentTaskWorkOrderModel.getItemCondition() != null) {
                    getCondition = parentTaskWorkOrderModel.getItemCondition();
                } else {
                    getCondition = "YES; NO; N/A";
                }
                String[] conditionArr = getCondition.split(";");
                if (conditionArr[2].replace(" ", "").equalsIgnoreCase(Utils.getText(context, R.string.yes))) {
                    setBackgroundTintColor(context, holder.workingTv, R.color.green);
                    holder.workingTv.setText(Utils.getText(context, R.string.yes).toUpperCase(Locale.ROOT));
                } else if (conditionArr[2] .replace(" ", "").equalsIgnoreCase(Utils.getText(context, R.string.no))) {
                    setBackgroundTintColor(context, holder.workingTv, R.color.red);
                    holder.workingTv.setText(Utils.getText(context, R.string.no));
                } else {
                    setBackgroundTintColor(context, holder.workingTv, R.color.gray);
                    holder.workingTv.setText("N/A");
                }
            }

        }


    }

    @Override
    public int getItemCount() {
        return parentTaskWorkOrderModelList.size();
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder{

        private final EditText subItemNameTv, subItemNameTv2, subItemDescription, subItemCondition;
        private final LinearLayout simplifyLayout, detailLayout;
        private final TextView cleanTv, unDamageTv, workingTv, titleCleanTv, titleUnDamageTv, titleWorkingTv;
        private final SwipeLayout swipeLayout;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);

            simplifyLayout = itemView.findViewById(R.id.simplifyLayout);
            detailLayout = itemView.findViewById(R.id.linear_item);
            swipeLayout = itemView.findViewById(R.id.swipe_layout);
            subItemCondition = itemView.findViewById(R.id.SubItemCondition);
            subItemDescription = itemView.findViewById(R.id.SubItemDescription);
            subItemNameTv = itemView.findViewById(R.id.SubItemName);
            subItemNameTv2 = itemView.findViewById(R.id.SubItemName2);

            cleanTv = itemView.findViewById(R.id.cleanTv);
            unDamageTv = itemView.findViewById(R.id.unDamageTv);
            workingTv = itemView.findViewById(R.id.workingTv);

            titleCleanTv = itemView.findViewById(R.id.titleCleanTv);
            titleUnDamageTv = itemView.findViewById(R.id.titleUnDamageTv);
            titleWorkingTv = itemView.findViewById(R.id.titleWorkingTv);

        }
    }

    private void setBackgroundTintColor(Context context, View view, int color){
        DrawableCompat.setTint(DrawableCompat.wrap(view.getBackground()).mutate(),
                ContextCompat.getColor(context, color));
    }
}
