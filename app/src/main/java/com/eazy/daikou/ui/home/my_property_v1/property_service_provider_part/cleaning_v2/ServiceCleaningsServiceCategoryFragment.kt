package com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.cleaning_v2

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.model.my_property.cleaning.ServiceCategories
import com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.adapter_v2.ServiceCleaningServiceCategoryAdapter
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class ServiceCleaningsServiceCategoryFragment : BottomSheetDialogFragment() {

    private var mContext : Context? = null
    private lateinit var noItemForDisplayTv: TextView
    private lateinit var recyclerServiceCategory: RecyclerView
    private lateinit var adapterCategory: ServiceCleaningServiceCategoryAdapter
    private var listServiceCategory: ArrayList<ServiceCategories> = ArrayList()
    private lateinit var callBackCategoryName: CallBackItemCategoryName
    private var categoryId = ""

    fun newInstance(categoryId : String, listCategory: ArrayList<ServiceCategories>): ServiceCleaningsServiceCategoryFragment {
        val fragment = ServiceCleaningsServiceCategoryFragment()
        val args = Bundle()
        args.putSerializable("service_category", listCategory)
        args.putSerializable("category_id", categoryId)
        fragment.arguments = args
        return fragment
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_FRAME, R.style.AppBottomSheetDialogTheme)

        if(arguments != null && requireArguments().containsKey("service_category")){
            listServiceCategory = requireArguments().getSerializable("service_category") as ArrayList<ServiceCategories>
        }

        if(arguments != null && requireArguments().containsKey("category_id")){
            categoryId = requireArguments().getString("category_id") as String
        }

        for (item in listServiceCategory){
            item.isClick = item.id == categoryId
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_service_cleanings_service_category, container, false)

        initView(view)

        initAction()

        return view
    }

    private fun initView(view: View){
        recyclerServiceCategory = view.findViewById(R.id.recyclerServiceCategory)
        noItemForDisplayTv = view.findViewById(R.id.noItemForDisplayTv)
    }

    private fun initAction(){
        noItemForDisplayTv.visibility = if (listServiceCategory.size > 0 ) View.GONE else View.VISIBLE

        val layoutManagerCategory = LinearLayoutManager(mContext)
        recyclerServiceCategory.layoutManager = layoutManagerCategory
        adapterCategory = ServiceCleaningServiceCategoryAdapter(mContext!!, listServiceCategory, callBackItem)
        recyclerServiceCategory.adapter = adapterCategory
    }

    private val callBackItem: ServiceCleaningServiceCategoryAdapter.CallBackItemCategoryListener = object : ServiceCleaningServiceCategoryAdapter.CallBackItemCategoryListener{
        override fun callBackItem(serviceCategoryList: ServiceCategories) {
            callBackCategoryName.clickItemCategoryName(serviceCategoryList)

            dismiss()
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
    }

    fun initListener(callBackCategoryName: CallBackItemCategoryName){
        this.callBackCategoryName = callBackCategoryName
    }

    interface CallBackItemCategoryName{
        fun clickItemCategoryName(serviceCategoryList: ServiceCategories)
    }

}