package com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.fragment

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.*
import androidx.fragment.app.DialogFragment
import com.eazy.daikou.R
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment


class RatingAnReViewBottomSheetFragment : BottomSheetDialogFragment() {

    private var onClickAllAction :  OnClickFeedBack?=null
    private lateinit var action : String

    fun newInstance(type : String) : RatingAnReViewBottomSheetFragment {
        val args = Bundle()
        val fragment = RatingAnReViewBottomSheetFragment()
        args.putString("type",type)
        fragment.arguments = args
        return fragment
    }

    @SuppressLint("UseRequireInsteadOfGet")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val root =  inflater.inflate(R.layout.rating_and_review_dialog_fragment, container, false)
        dialog!!.window!!.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
        return root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, R.style.AppBottomSheetDialogTheme)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        if (dialog is BottomSheetDialog) {
            dialog.behavior.skipCollapsed = true
            dialog.behavior.state = BottomSheetBehavior.STATE_EXPANDED
        }
        return dialog
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val rating : RatingBar = view.findViewById(R.id.rating)
        val ratingComment : EditText = view.findViewById(R.id.reviewEt)
        val  btnRate : Button = view.findViewById(R.id.btnRate)
        val closeButton = view.findViewById<LinearLayout>(R.id.close_btn)

        if(ratingComment.text.toString().isNotEmpty() && rating.rating.toString().isNotEmpty()){
            btnRate.setOnClickListener {
                onClickAllAction?.onClickRateAndReview(ratingComment.text.toString(),rating.rating.toString().toFloat(),action)
                dismiss()
            }
        }
        closeButton.setOnClickListener {
            dismiss()
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        val parent = parentFragment
        onClickAllAction = if (parent != null) {
            parent as OnClickFeedBack
        } else {
            context as OnClickFeedBack
        }
    }

    interface OnClickFeedBack {
        fun onClickRateAndReview(txtChoose: String?, position: Float, action : String)
    }
}