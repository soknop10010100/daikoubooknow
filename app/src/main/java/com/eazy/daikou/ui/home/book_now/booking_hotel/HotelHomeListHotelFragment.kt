package com.eazy.daikou.ui.home.book_now.booking_hotel

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.widget.NestedScrollView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.base.BaseFragment
import com.eazy.daikou.base.MockUpData
import com.eazy.daikou.request_data.request.book_now_ws.BookingHotelWS
import com.eazy.daikou.request_data.request.book_now_ws.RequestHashMapData
import com.eazy.daikou.helper.AppAlertCusDialog
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.booking_hotel.LocationHotelModel
import com.eazy.daikou.model.home.MainItemHomeModel
import com.eazy.daikou.model.home.SubItemHomeModel
import com.eazy.daikou.model.home.UserDeactivated
import com.eazy.daikou.ui.home.home_menu.adapter.MainHomeShowItemAdapter

@SuppressLint("ValidFragment")
class HotelHomeListHotelFragment : BaseFragment() {

    private lateinit var recyclerViewItem : RecyclerView
    private lateinit var mainHomeShowItemAdapter : MainHomeShowItemAdapter
    private var locationHotelModelList: ArrayList<LocationHotelModel> = ArrayList()
    private var mainItemAllHotelList: ArrayList<MainItemHomeModel> = ArrayList()
    private lateinit var progressBar: ProgressBar
    private lateinit var progressBarAbove: ProgressBar
    private var eazyhotelUserId = ""
    private lateinit var linearLayoutManager: LinearLayoutManager
    private var currentPage : Int = 1
    private var isScrolling = false
    private var isLastList = false
    private var category: String = ""
    private var callBackListener : CallBackListener ?= null
    private lateinit var nestedScrollView : NestedScrollView
    private var isFirstListItem = true

    companion object{
        fun newInstance(action: String) : HotelHomeListHotelFragment {
            val args = Bundle()
            val fragment = HotelHomeListHotelFragment()
            args.putString("action", action)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        locationHotelModelList = RequestHashMapData.getStoreLocationHotelData()

        arguments?.let {
            category = it.getString("action").toString()
        }

        try {
            callBackListener = parentFragment as CallBackListener?
        } catch (e: java.lang.ClassCastException) {
            throw java.lang.ClassCastException("Calling fragment must implement Callback interface")
        }

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_hotel_home_list_hotel, container, false)

        initView(view)

        initAction()

        return view
    }

    private fun initView(view : View){
        recyclerViewItem = view.findViewById(R.id.recyclerView)
        progressBar = view.findViewById(R.id.progressItem)
        progressBarAbove = view.findViewById(R.id.progressAbove)
        progressBar.visibility = View.VISIBLE
        nestedScrollView = view.findViewById(R.id.scrollView)
    }

    private fun initAction(){
        linearLayoutManager = LinearLayoutManager(mActivity)
        recyclerViewItem.layoutManager = linearLayoutManager
        eazyhotelUserId = Utils.validateNullValue(MockUpData.getEazyHotelUserId(UserSessionManagement(mActivity)))

        initItemRecyclerView()

        // Scroll More Items
        nestedScrollView.setOnScrollChangeListener(NestedScrollView.OnScrollChangeListener { v, scrollX, scrollY, oldScrollX, oldScrollY ->
            val viewI = nestedScrollView.getChildAt(nestedScrollView.childCount - 1)
            val diff = viewI.bottom - (nestedScrollView.height + nestedScrollView.scrollY)
            if (diff == 0 && !isLastList) {

                isScrolling = true
                currentPage++

                progressBar.visibility = View.VISIBLE

                requestItemHotelList()
            }
        })

    }

    @SuppressLint("NotifyDataSetChanged")
    private fun initItemRecyclerView(){
        mainHomeShowItemAdapter = MainHomeShowItemAdapter("booking_menu", mainItemAllHotelList, mActivity, onClickItemLister)
        recyclerViewItem.adapter = mainHomeShowItemAdapter

        requestItemHotelList()
    }

    private val onClickItemLister = object : MainHomeShowItemAdapter.PropertyClick{
        @SuppressLint("NotifyDataSetChanged")
        override fun onClickCallBackListener(actionWishlist: String, subItemHomeModel: SubItemHomeModel) {
            //click to favourite hotel
            if (actionWishlist == "action_wishlist") {
                if (AppAlertCusDialog.isSuccessLoggedIn(mActivity)){
                    progressBar.visibility = View.VISIBLE
                    RequestHashMapData.editStatusHotelWs(mActivity, progressBar, subItemHomeModel.id!!, subItemHomeModel.isWishlist,
                        "wishlist", eazyhotelUserId, category, object : RequestHashMapData.Companion.OnSuccessEditStatusListener{
                            override fun onSuccess(msg: String) {
                                subItemHomeModel.isWishlist = !subItemHomeModel.isWishlist
                                mainHomeShowItemAdapter.notifyDataSetChanged()
                            }
                        })
                }
                return
            }

            //click on hotel item / location
            if(subItemHomeModel.action == "bestseller_listing" || subItemHomeModel.action == "all_nearby_hotels" || subItemHomeModel.action == "featuring_hotel"){
                val intent = Intent(mActivity, HotelBookingDetailHotelActivity::class.java).apply {
                    putExtra("hotel_name", subItemHomeModel.name)
                    putExtra("category", subItemHomeModel.titleCategory)
                    putExtra("hotel_image", subItemHomeModel.imageUrl)
                    putExtra("hotel_id", subItemHomeModel.id)
                    putExtra("price", subItemHomeModel.priceVal)
                }
                resultLauncher.launch(intent)
            } else {
                initStartSearch(false, if (subItemHomeModel.id != null) subItemHomeModel.id!! else "", if (subItemHomeModel.name != null)   subItemHomeModel.name!! else "")
            }
        }

        override fun onClickSeeAllCallBackListener(action: String, actionType : String, subItemHomeModel: ArrayList<*>) {
            val intent = Intent(mActivity, HotelShowAllItemFromHomeActivity::class.java).apply {
                putExtra("header_title", action)
                putExtra("category", category)
                putExtra("action_type", actionType)
                putExtra("list_item", subItemHomeModel)
                putExtra("location_list", locationHotelModelList)
            }
            resultLauncher.launch(intent)
        }
    }

    private fun requestItemHotelList(){
        BookingHotelWS().getHotelBookingByCategoryAll(
            mActivity,
            "hotel_home_page", category,
            RequestHashMapData.hashMapHomePageData(currentPage, category, eazyhotelUserId, BaseActivity.latitude.toString(), BaseActivity.longitude.toString()),
            onCallBackItemListener
        )
    }

    private val onCallBackItemListener = object : BookingHotelWS.OnCallBackHomePageItemListener {
        @SuppressLint("NotifyDataSetChanged")
        override fun onSuccessShowHotel(travelTalkBanner : ArrayList<String>, discountBanner: ArrayList<String>, listImageSlider: MutableList<String>, mainItemHomeModelLIst: ArrayList<MainItemHomeModel>, itemHomeList: ArrayList<SubItemHomeModel>, userDeactivated: UserDeactivated) {
            progressBar.visibility = View.GONE
            progressBarAbove.visibility = View.GONE

            //Call Back Image Slide
            callBackSlide(travelTalkBanner, discountBanner, listImageSlider as ArrayList<String>, userDeactivated)

            // Add Item
            if (isFirstListItem){
                isFirstListItem = false
                mainItemAllHotelList.addAll(mainItemHomeModelLIst)
            } else if (isScrolling){
                isScrolling = false
                for (item in mainItemAllHotelList){
                    if (item.mainAction == "bestseller_listing"){
                        item.subItemHomeModelList.addAll(itemHomeList)
                        break
                    }
                }

            }

            if (itemHomeList.size == 0)    isLastList = true
            mainHomeShowItemAdapter.notifyDataSetChanged()

        }

        override fun onSuccessShowHotelAll(itemHomeList: ArrayList<SubItemHomeModel>) {}

        override fun onFailed(message: String) {
            progressBar.visibility = View.GONE
            progressBarAbove.visibility = View.GONE
            Utils.customToastMsgError(mActivity, message, false)
        }

    }

    private val resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
        if (result.resultCode == Activity.RESULT_OK) {
            //Call back when click wishlist hotel on detail screen
            refreshItemList()
        }
    }

    private fun refreshItemList(){
        currentPage = 1
        isScrolling = false
        isLastList = false
        isFirstListItem = true

        mainItemAllHotelList.clear()

        requestItemHotelList()
    }

    private fun initStartSearch(isClickSearch : Boolean, locationId : String, locationHotel : String){
        val hashMapData : HashMap<String, Any> = HashMap()
        hashMapData["value"] = locationHotel
        hashMapData["location_list"] = locationHotelModelList
        hashMapData["location_id"] = locationId
        hashMapData["location_title"] = locationHotel
        hashMapData["adults"] = ""
        hashMapData["room"] = ""
        hashMapData["children"] = ""
        hashMapData["start_date"] =  ""
        hashMapData["end_date"] = ""
        hashMapData["guest_num"] = ""
        hashMapData["from_action"] =  if (isClickSearch) "search_list" else "hotel_location_detail"
        hashMapData["category"] =  category
        RequestHashMapData.storeHotelItemData(hashMapData)

        val intent = Intent(mActivity, HotelTopDestinationDetailActivity::class.java)
        startActivity(intent)
    }

    private fun callBackSlide(travelTalkBanner : ArrayList<String>, bannerDiscountSlide : ArrayList<String>, listImageSlider : ArrayList<String>, userDeactivated : UserDeactivated){
        if (callBackListener != null){
            callBackListener!!.onCallBack(travelTalkBanner, bannerDiscountSlide, listImageSlider, userDeactivated)
        }
    }

    interface CallBackListener {
        fun onCallBack(travelTalkBanner : ArrayList<String>, bannerDiscountSlide : ArrayList<String>, sliderBanner: ArrayList<String>, userDeactivated : UserDeactivated)
    }

}