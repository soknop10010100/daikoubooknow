package com.eazy.daikou.ui.home.my_document.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.eazy.daikou.R;
import com.eazy.daikou.helper.Utils;
import com.eazy.daikou.model.my_document.PropertyDocument;

import java.util.ArrayList;

public class DocumentPropertyAdapter extends RecyclerView.Adapter<DocumentPropertyAdapter.ViewHolder> {

    private final ArrayList<PropertyDocument> listProperty;
    private final ClickOnItemDocumentListener itemClick;
    private final Context context;

    public DocumentPropertyAdapter(ArrayList<PropertyDocument> listProperty, Context context, ClickOnItemDocumentListener itemClick) {
        this.listProperty = listProperty;
        this.itemClick = itemClick;
        this.context = context;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_tap_document_adapter, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint("NotifyDataSetChanged")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        PropertyDocument propertyDocument = listProperty.get(position);
        if (propertyDocument != null) {
            holder.nameCategoryTv.setText(propertyDocument.getPropertyName());

            if (propertyDocument.getIsClick()) {
                holder.nameCategoryTv.setTextColor(Utils.getColor(context, R.color.white));
                holder.linearLayout.setBackgroundResource(R.drawable.custom_card_view_color_menu);
            } else {
                holder.linearLayout.setBackground(null);
                holder.nameCategoryTv.setTextColor(Utils.getColor(context, R.color.gray));
            }

            holder.itemView.setOnClickListener(view -> itemClick.onClickItem(propertyDocument));

        }


    }

    @Override
    public int getItemCount() {
        return listProperty.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView nameCategoryTv;
        private final LinearLayout linearLayout;

        public ViewHolder(View view) {
            super(view);
            linearLayout = view.findViewById(R.id.linear_card);
            nameCategoryTv = view.findViewById(R.id.text_category);
        }


    }

    public interface ClickOnItemDocumentListener {
        void onClickItem(PropertyDocument propertyDocument);
    }
}