package com.eazy.daikou.ui.home.my_property_v1.adapter;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.eazy.daikou.R;
import com.eazy.daikou.model.floor_plan.FloorSecureModel;

import java.util.List;

public class FloorSecureAdapter extends RecyclerView.Adapter<FloorSecureAdapter.ItemViewModel>{

    private final List<FloorSecureModel> floorSecureList;
    private final ClickBackListener clickBackListener;
    private final Context context;

    public FloorSecureAdapter(Context context, List<FloorSecureModel> floorSecureList, ClickBackListener clickBackListener) {
        this.floorSecureList = floorSecureList;
        this.clickBackListener = clickBackListener;
        this.context = context;
    }

    @NonNull
    @Override
    public FloorSecureAdapter.ItemViewModel onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.floor_security_model, parent, false);
        return new ItemViewModel(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onBindViewHolder(@NonNull FloorSecureAdapter.ItemViewModel holder, final int position) {
        FloorSecureModel item = floorSecureList.get(position);
        if (item != null){
            if (item.getIconFile() != null){
                Glide.with(holder.icon).load(item.getIconFile()).into(holder.icon);
            } else {
                holder.icon.setImageResource(R.drawable.no_image);
            }

            holder.title.setText(item.getOptionText() != null ? item.getOptionText() : context.getResources().getString(R.string.not));

            if (item.isClick()){
                holder.linLayoutMain.setBackgroundColor(context.getResources().getColor(R.color.white));
                holder.title.setTextColor(ContextCompat.getColor(context,R.color.appBarColor));
            } else {
                holder.linLayoutMain.setBackgroundColor(context.getResources().getColor(R.color.appBarColor));
                holder.title.setTextColor(ContextCompat.getColor(context,R.color.white));
            }

            holder.linLayoutMain.setOnClickListener(view -> {
                notifyDataSetChanged();
                clickBackListener.clickBack(item);
                holder.linLayoutMain.setBackgroundColor(context.getResources().getColor(R.color.white));
                holder.title.setTextColor(context.getColor(R.color.appBarColor));
            });
        }
    }

    @Override
    public int getItemCount() {
        return floorSecureList.size();
    }

    public static class ItemViewModel extends RecyclerView.ViewHolder {
        private final TextView title;
        private final ImageView icon;
        private final LinearLayout linLayoutMain;
        public ItemViewModel(@NonNull View itemView) {
            super(itemView);
            linLayoutMain = itemView.findViewById(R.id.linCategorySecure);
            title = itemView.findViewById(R.id.titleSecure);
            icon = itemView.findViewById(R.id.iconSecure);
        }
    }

    public interface ClickBackListener{
        void clickBack(FloorSecureModel floorSecureModel);
    }
}
