package com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseFragment
import com.eazy.daikou.helper.ImageViewTouchView
import com.eazy.daikou.helper.ZoomAllViewImageActivity


class ImageListFragment : BaseFragment() {
    private var position : Int = 0
    private var photo :String?= null
    private var listPhoto = ArrayList<String>()
    private var action : String = ""

    companion object {
        fun newInstance(position: Int, photo: String , listPhoto: ArrayList<String> , action: String) : ImageListFragment {
            val args = Bundle()
            val fragment = ImageListFragment()
            args.putInt("position", position)
            args.putString("photo", photo)
            args.putSerializable("listPhoto",listPhoto)
            args.putString("action", action)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val roof =  inflater.inflate(R.layout.recyclerview_item_image, container, false)
        val imageView = roof.findViewById<ImageView>(R.id.image_view)
        val imageViewTouchViewPager = roof.findViewById<ImageViewTouchView>(R.id.imageView)

        if(arguments!=null&& requireArguments().containsKey("photo")){
            photo = requireArguments().getString("photo")
            position = requireArguments().getInt("position")
            listPhoto = requireArguments().getSerializable("listPhoto") as ArrayList<String>
            action = requireArguments().getString("action") as String
        }

        if (action == "viewImage"){
            imageView.visibility = View.GONE
            Glide.with(imageViewTouchViewPager).load(photo).into(imageViewTouchViewPager)
        } else {
            imageViewTouchViewPager.visibility = View.GONE
            Glide.with(imageView).load(photo).into(imageView)
        }

        roof.setOnClickListener {
            val intent = Intent(mActivity, ZoomAllViewImageActivity::class.java)
            intent.putExtra("image", listPhoto)
            startActivity(intent)
        }
        return roof
    }
}