package com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.adapter_v2

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.my_property.cleaning.ServiceTerms

class PropertyServiceProviderCategoryTermAdapter(private val context: Context, private val listItem : ArrayList<ServiceTerms>, private val callBackTerm: CallBackItemTermListener): RecyclerView.Adapter<PropertyServiceProviderCategoryTermAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.layout_category_item_service, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val listTerm = listItem[position]
        holder.textItemCategoryTv.text = listTerm.term_name
        if (listTerm.isClickTitle) {
            holder.relativeBG.background = ResourcesCompat.getDrawable(context.resources, R.drawable.card_view_shape_color_app, null)
            holder.textItemCategoryTv.setTextColor(Utils.getColor(context, R.color.white))
        } else {
            holder.relativeBG.background = ResourcesCompat.getDrawable(context.resources, R.drawable.shape_transparent_bg, null)
            holder.textItemCategoryTv.setTextColor(Utils.getColor(context, R.color.gray))
        }

        holder.itemView.setOnClickListener {
            callBackTerm.callBackTerm(listTerm)
        }

    }

    override fun getItemCount(): Int {
       return listItem.size
    }
    class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        var textItemCategoryTv : TextView = view.findViewById(R.id.textItemCategoryTv)
        var relativeBG : LinearLayout = view.findViewById(R.id.relativeBG)
    }

    interface CallBackItemTermListener{
        fun callBackTerm( serviceCategoryItemModel: ServiceTerms)
    }
}