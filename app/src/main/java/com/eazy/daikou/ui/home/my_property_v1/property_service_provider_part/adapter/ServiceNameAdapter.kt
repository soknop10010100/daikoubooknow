package com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.model.my_property.service_property_employee.AllService


class ServiceNameAdapter(
    private val context : Context,
    private val serviceList: ArrayList<AllService>,
    private val itemClickService: ItemClickOnService
) : RecyclerView.Adapter<ServiceNameAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.layout_item_service_name, parent, false)
        return ViewHolder(v)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val service: AllService = serviceList[position]

        holder.companyTv.text =  service.cleaningServiceName
        holder.serviceTermTv.text = context.getString(R.string.service_termed) + service.serviceTerm
        holder.serviceTypeTv.text = context.getString(R.string.service_typed) + service.cleaningServiceType
        holder.serviceFeeTv.text =context.getString(R.string.service_feed) + service.serviceFee

        if (service.cleaningServiceImage!= null) {
            for (image in service.cleaningServiceImage){
                Glide.with(holder.imageView).load(image).into(holder.imageView)
            }
        } else {
            Glide.with(holder.imageView).load(R.drawable.no_image).into(holder.imageView)
        }
        holder.itemView.setOnClickListener {
            itemClickService.onClick(service)
        }

    }

    override fun getItemCount(): Int {
        return serviceList.size
    }

    interface ItemClickOnService {
        fun onClick(service: AllService)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var companyTv : TextView = view.findViewById(R.id.name_service)
        var serviceTypeTv : TextView = view.findViewById(R.id.service_type)
        var serviceTermTv : TextView = view.findViewById(R.id.service_term)
        var serviceFeeTv : TextView = view.findViewById(R.id.service_fee)
        var imageView : ImageView = view.findViewById(R.id.imageView)

    }

    fun clear() {
        val size: Int = serviceList.size
        if (size > 0) {
            for (i in 0 until size) {
                serviceList.removeAt(0)
            }
            notifyItemRangeRemoved(0, size)
        }
    }
}