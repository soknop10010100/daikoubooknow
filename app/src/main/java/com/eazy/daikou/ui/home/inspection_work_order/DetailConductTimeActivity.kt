package com.eazy.daikou.ui.home.inspection_work_order

import android.content.Intent
import android.os.Bundle
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.request_data.static_data.StaticUtilsKey
import com.eazy.daikou.model.inspection.Add24HoursModel
import com.eazy.daikou.model.inspection.InfoInspectionModel
import com.eazy.daikou.ui.home.inspection_work_order.adapter.CalendarInspectionAdapter

class DetailConductTimeActivity : BaseActivity() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var calendarInspectionAdapter: CalendarInspectionAdapter
    private lateinit var add24HoursModelList : Add24HoursModel
    private lateinit var titleNewEvent : TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_conducte_time)

        initView()

        initData()

        initAction()
    }

    private fun initView(){
        recyclerView = findViewById(R.id.inspectionRecycle)
        findViewById<TextView>(R.id.titleCancel).setOnClickListener { finish() }
        titleNewEvent = findViewById(R.id.titleNewEvent)
    }

    private fun initData(){
        if (intent != null && intent.hasExtra("add24HoursModelList")){
            add24HoursModelList = intent.getSerializableExtra("add24HoursModelList") as Add24HoursModel
        }
    }

    private fun initAction(){
        linearLayoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = linearLayoutManager

        titleNewEvent.text = add24HoursModelList.hour

        calendarInspectionAdapter = CalendarInspectionAdapter(add24HoursModelList.hourList, true, onClickCallBack)
        recyclerView.adapter = calendarInspectionAdapter
    }

    private var onClickCallBack = object : CalendarInspectionAdapter.OnClickCallBackListener{
        override fun onClickCallBackItemDay(add24HoursModel: Add24HoursModel?) {}

        override fun onClickCallBackItemInMonth(infoInspectionModel: InfoInspectionModel) {
            val intent = Intent(this@DetailConductTimeActivity, ChooseInspectionTypeActivity::class.java)
            intent.putExtra("inspection_template_id", infoInspectionModel.id)
            intent.putExtra("template_id", infoInspectionModel.inspectionTemplateId)
            intent.putExtra("action", StaticUtilsKey.form_template_type)
            intent.putExtra("action_type", "action_detail")
            startActivity(intent)
        }

    }
}