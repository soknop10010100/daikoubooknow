package com.eazy.daikou.ui.home.evaluate

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.activity.result.ActivityResult
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.base.BaseCameraActivity
import com.eazy.daikou.request_data.request.complain_ws.ComplaintListWS
import com.eazy.daikou.databinding.ActivityEvaluateUnitScanBinding
import com.eazy.daikou.helper.GetDataUtils
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.my_property.my_property.PropertyMyUnitModel
import com.eazy.daikou.model.profile.PropertyItemModel
import com.eazy.daikou.model.profile.UnitNoModel
import com.eazy.daikou.model.work_order.ImageListModel
import com.eazy.daikou.ui.home.complaint_solution.complaint.adapter.AdapterItemListComplaint
import com.eazy.daikou.ui.home.complaint_solution.complaint.fragment.SelectAccountAndUnitNoBottomSheet
import com.eazy.daikou.ui.home.inspection_work_order.adapter.ImageListWorkOrderAdapter
import com.google.android.material.textfield.TextInputEditText

class EvaluateUnitScanActivity : BaseActivity() {

    private lateinit var btnAddImg: LinearLayout
    private val bitmapList: ArrayList<ImageListModel> = ArrayList()
    private lateinit var imageRecyclerView: RecyclerView
    private lateinit var satisfactionLayout : LinearLayout
    private lateinit var satisfactionTv : TextView
    private var statusSatisfaction = ""
    private lateinit var btnSave : TextView
    private lateinit var commentEdt : TextInputEditText
    private lateinit var progressBar: ProgressBar
    private var action : String = ""

    private lateinit var bindingView: ActivityEvaluateUnitScanBinding
    private lateinit var propertyTv : TextView
    private var accountId = ""
    private var unitId = ""

    private var isUnitCreated = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bindingView = ActivityEvaluateUnitScanBinding.inflate(layoutInflater)
        setContentView(bindingView.root)

        initView()

        initData()

        initAction()
    }

    private fun initView(){
        Utils.customOnToolbar(this, resources.getString(R.string.evaluation)){finish()}

        btnAddImg = findViewById(R.id.btnAddImg)
        imageRecyclerView = findViewById(R.id.listImageRecyclerView)
        satisfactionLayout = findViewById(R.id.satisfactionLayout)
        satisfactionTv = findViewById(R.id.satisfactionTv)
        commentEdt = findViewById(R.id.commentEdt)
        progressBar = findViewById(R.id.progressItem)
        progressBar.visibility = View.GONE

        btnSave = bindingView.btnSave
        propertyTv = bindingView.propertyTv
    }

    private fun initData(){

        action = GetDataUtils.getDataFromString("action", this)

        accountId = GetDataUtils.getDataFromString("account_id", this)

        val accountName = GetDataUtils.getDataFromString("account_name", this)
        propertyTv.text = accountName

    }

    private fun initAction(){

        btnAddImg.setOnClickListener{
            val intent = Intent(this@EvaluateUnitScanActivity, BaseCameraActivity::class.java)
            activityLauncher.launch(intent) { result: ActivityResult ->
                if (result.resultCode == RESULT_OK) {
                    val data = result.data ?: return@launch

                    if (data.hasExtra("image_path")) {
                        val imagePath = data.getStringExtra("image_path")
                        imagePath?.let { it1 -> setImageRecyclerView("", it1) }
                    }
                    if (data.hasExtra("select_image")) {
                        val imagePath = data.getStringExtra("select_image")
                        imagePath?.let { it1 -> setImageRecyclerView("", it1) }
                    }
                }
            }
        }

        btnSave.setOnClickListener ( CustomSetOnClickViewListener { requestServiceWs() } )

        // Select Property
        propertyTv.setOnClickListener ( CustomSetOnClickViewListener { bottomSheetCategoryService("property") })

        for (i in 0 until satisfactionLayout.childCount) {
            satisfactionLayout.getChildAt(i).setOnClickListener {
                setClickOnMenuHeader(i)
                when (i) {
                    0 -> {
                        statusSatisfaction = "very_dissatisfied"
                    }
                    1 -> {
                        statusSatisfaction = "dissatisfied"
                    }
                    2 -> {
                        statusSatisfaction = "ok"
                    }
                    3 -> {
                        statusSatisfaction = "satisfied"
                    }
                    else -> {
                        statusSatisfaction = "very_satisfied"
                    }
                }
                satisfactionTv.text = Utils.getSatisfaction(this)[statusSatisfaction]
            }
        }

        findViewById<RadioGroup>(R.id.rGroup).setOnCheckedChangeListener { _, checkedId ->
            if (checkedId == R.id.rUnit) {
                isUnitCreated = true
            } else if (checkedId == R.id.rGeneral) {
                isUnitCreated = false
            }
            findViewById<LinearLayout>(R.id.unitNoLayout).visibility = if (isUnitCreated)   View.VISIBLE else View.GONE
        }
    }

    private fun bottomSheetCategoryService(actionType : String){
        val bottomSheet = SelectAccountAndUnitNoBottomSheet.newInstance(actionType, accountId)
        bottomSheet.initDataListener(callBackItemClickFormBottomSheet)
        bottomSheet.show(supportFragmentManager,"action_type")
    }

    private val callBackItemClickFormBottomSheet : AdapterItemListComplaint.CallBackItemClickListener = object : AdapterItemListComplaint.CallBackItemClickListener {
        override fun onClickAccount(itemComplaint: PropertyItemModel) {
            propertyTv.text = itemComplaint.name
            accountId = itemComplaint.id.toString()

            if (isUnitCreated){
                bindingView.unitNoTv.text = ""
                unitId = ""
            }
        }

        override fun onClickUnit(myUnit: UnitNoModel) {
            bindingView.unitNoTv.text = myUnit.name
            unitId = myUnit.id.toString()
        }
    }

    private fun setClickOnMenuHeader(backUpId : Int){
        for (i in 0 until satisfactionLayout.childCount) {
            val rowView: View = satisfactionLayout.getChildAt(i)
            if (rowView is ImageView) {
                if (i == backUpId) {
                    rowView.setColorFilter(Utils.getColor(this@EvaluateUnitScanActivity, R.color.yellow))
                } else {
                    rowView.setColorFilter(Utils.getColor(this@EvaluateUnitScanActivity, R.color.gray))
                }
            }
        }

    }

    private fun setImageRecyclerView(posKeyDefine: String, imagePath: String) {
        val loadedBitmap = BaseCameraActivity.convertByteToBitmap(imagePath)
        bitmapList.add(ImageListModel(posKeyDefine, loadedBitmap))

        // Set image layout
        setImageRecyclerView()
    }

    private fun setImageRecyclerView() {
        ImageListWorkOrderAdapter.setImageRecyclerView(imageRecyclerView, btnAddImg, this, bitmapList, 0, object : ImageListWorkOrderAdapter.OnClearImage {
            override fun onClickRemove(bitmap: ImageListModel, post: Int) {
                bitmapList.remove(bitmap)

                setImageRecyclerView()
            }

            override fun onViewImage(bitmap: ImageListModel) {
                if (bitmap.bitmapImage != null) {
                    Utils.openImageOnDialog(this@EvaluateUnitScanActivity, Utils.convert(bitmap.bitmapImage))
                }
            }

        })
    }

    private fun requestServiceWs() {
        if (accountId == "") {
            Utils.customToastMsgError(this, resources.getString(R.string.please_input_property_name), false)
            return
        } else if (isUnitCreated && unitId == ""){
            Utils.customToastMsgError(this, resources.getString(R.string.please_select_unit_no), false)
            return
        }

        val hashMap = HashMap<String, Any>()

        hashMap["object_type"] = if (isUnitCreated) "unit" else "account"
        hashMap["object_id"] = if (isUnitCreated) unitId else accountId
        hashMap["post_type"] = "evaluation"
        hashMap["account_id"] = accountId

        hashMap["subject"] = bindingView.subjectEdt.text.toString()
        hashMap["description"] = bindingView.commentEdt.text.toString()

        if (bitmapList.size > 0){
            val imageList : java.util.ArrayList<String> = java.util.ArrayList()
            for (item in bitmapList){
                imageList.add(Utils.convert(item.bitmapImage))
            }
            hashMap["file_type"] = "image"
            hashMap["file_items"] = imageList
        }

        hashMap["status"] = statusSatisfaction

        progressBar.visibility = View.VISIBLE
        ComplaintListWS.createComplaintWS(this, hashMap, object : ComplaintListWS.CallBackCreateComplaint{
            override fun onSuccessFull(message: String) {
                progressBar.visibility = View.GONE
                Utils.customToastMsgError(this@EvaluateUnitScanActivity, message, true)

                setResult(RESULT_OK)
                finish()
            }

            override fun onFailed(mes: String) {
                progressBar.visibility = View.GONE
                Utils.customToastMsgError(this@EvaluateUnitScanActivity, mes, false)
            }

        })
    }

}