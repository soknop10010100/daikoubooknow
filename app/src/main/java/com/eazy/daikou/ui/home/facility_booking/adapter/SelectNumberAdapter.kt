package com.eazy.daikou.ui.home.facility_booking.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.model.facility_booking.SelectNumberModel

class SelectNumberAdapter(private val listNumber : List<SelectNumberModel>, private val onClickCallBack : ClickCallBackListener) : RecyclerView.Adapter<SelectNumberAdapter.ItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.select_number_model, parent, false)
        return ItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val stringNumber = listNumber[position]
        if (stringNumber != null){
            holder.titleTv.text = stringNumber.numberString
            holder.linearLayout.setOnClickListener{onClickCallBack.onClickCallBack(stringNumber.numberString)}
            holder.checkImage.visibility = if (stringNumber.isClick) View.VISIBLE else View.GONE
        } else {
            holder.titleTv.text = "-"
        }
    }

    override fun getItemCount(): Int {
        return listNumber.size
    }

    interface ClickCallBackListener{
        fun onClickCallBack(numberString: String)
    }

    class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val linearLayout: RelativeLayout = itemView.findViewById(R.id.layout)
        val checkImage: ImageView = itemView.findViewById(R.id.checkImage)
        val titleTv: TextView = itemView.findViewById(R.id.titleTv)
    }

}