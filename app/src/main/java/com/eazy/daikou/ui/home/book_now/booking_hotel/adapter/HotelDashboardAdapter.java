package com.eazy.daikou.ui.home.book_now.booking_hotel.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.eazy.daikou.R;
import com.eazy.daikou.helper.Utils;
import com.eazy.daikou.model.booking_hotel.DashboardModel;

import java.util.List;

public class HotelDashboardAdapter extends RecyclerView.Adapter<HotelDashboardAdapter.ViewHolder> {

    private final Context context;
    private final List<DashboardModel> dashboardModelList;

    public HotelDashboardAdapter(Context context, List<DashboardModel> dashboardModelList) {
        this.context = context;
        this.dashboardModelList = dashboardModelList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_item_dashboard, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        DashboardModel dashboardModel = dashboardModelList.get(position);
        if (dashboardModel != null) {
            int color = 0;
            switch (dashboardModel.getAction()) {
                case "total_pending":
                    color = R.color.greenSea;
                    break;
                case "total_earning":
                    color = R.color.yellow;
                    break;
                case "total_booking":
                    color = R.color.calendar_active_month_bg;
                    break;
                case "total_bookable_service":
                    color = R.color.purple;
                    break;
            }
            Utils.setValueOnText(holder.titleItemTv, dashboardModel.getName());
            Glide.with(holder.imageView).load(dashboardModel.getDrawable()).into(holder.imageView);
            Utils.setValueOnText(holder.priceTv, dashboardModel.getPrice());

            holder.linearColorBackground.setBackgroundColor(Utils.getColor(context, color));

        }
    }

    @Override
    public int getItemCount() {
        return dashboardModelList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView priceTv;
        private final TextView titleItemTv;
        private final ImageView imageView;
        private final LinearLayout linearColorBackground;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            priceTv = itemView.findViewById(R.id.priceTv);
            titleItemTv = itemView.findViewById(R.id.titleItemTv);
            linearColorBackground = itemView.findViewById(R.id.linearColorBackground);
            imageView = itemView.findViewById(R.id.img);
        }
    }
}
