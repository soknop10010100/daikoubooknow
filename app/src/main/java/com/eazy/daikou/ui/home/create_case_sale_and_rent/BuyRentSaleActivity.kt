package com.eazy.daikou.ui.home.create_case_sale_and_rent


import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.request_data.request.buy_sell_ws.BuySaleRentWs
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.buy_sell.MinMaxPriceModel
import com.eazy.daikou.ui.home.create_case_sale_and_rent.adapter.BuyRentSaleAdapter
import com.eazy.daikou.helper.DateNumPickerFragment
import com.eazy.daikou.model.buy_sell.SaleAndRentProperties
import com.eazy.daikou.ui.home.create_case_sale_and_rent.sale_and_rent.DetailListSaleRentActivity

class BuyRentSaleActivity : BaseActivity() {

    private lateinit var iconSearch : ImageView
    private lateinit var iconFilter : ImageView
    private var action : String = ""
    private lateinit var progressBar: ProgressBar
    private lateinit var recyclerView: RecyclerView
    private var buySaleRentModelList : ArrayList<SaleAndRentProperties> = ArrayList()
    private var currentItem = 0
    private var total : Int = 0
    private var scrollDown : Int = 0
    private var currentPage : Int = 1
    private var size : Int = 10
    private var isScrolling = false
    private lateinit var buyRentSaleAdapter : BuyRentSaleAdapter
    private lateinit var linearLayoutManager: LinearLayoutManager
    private var purposeType = ""
    private lateinit var refreshLayout : SwipeRefreshLayout
    private var maxPriceFilter = ""
    private var minPriceFilter = ""
    private lateinit var filterTv : TextView
    private var isClickFilter = true

    private lateinit var noDataTv: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_buy_rent_sale)

        initView()

        initData()

        initAction()

    }

    private fun initView(){
        iconSearch = findViewById(R.id.iconAdd)
        iconSearch.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_baseline_search_24))
        iconFilter = findViewById(R.id.ic_printer)
        iconFilter.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_baseline_filter_24))
        progressBar = findViewById(R.id.progressItem)
        recyclerView = findViewById(R.id.recyclerView)
        refreshLayout = findViewById(R.id.swipe_layouts)
        filterTv = findViewById(R.id.filterTv)
        noDataTv = findViewById(R.id.noDataTv)
    }

    private fun initData(){
        if (intent != null && intent.hasExtra("action")){
            action = intent.getStringExtra("action").toString()
        }
    }

    private fun initAction(){
        // Search Last Version
        iconSearch.setOnClickListener {
            val intent = Intent(this, SearchBuySaleActivity::class.java)
            intent.putExtra("purpose", purposeType)
            startActivity(intent)
        }
        iconFilter.visibility = View.VISIBLE
        iconFilter.setOnClickListener {
//            if (isClickFilter) {
//                filterTv.visibility = View.VISIBLE
//                isClickFilter = false
//            } else {
//                isClickFilter = true
//                filterTv.visibility = View.GONE
//
//                minPriceFilter = ""
//                maxPriceFilter = ""
//                refreshItemList()
//            }
            selectFilterMaxMinPrice("max_min_price")
        }

   //     filterTv.setOnClickListener { selectFilterMaxMinPrice("max_min_price") }

        val title: String
        when (action) {
            "sale" -> {
                title = resources.getString(R.string.sale).uppercase()
                purposeType =  "sale"
            }
            "buy" -> {
                title = resources.getString(R.string.buy).uppercase()
                purposeType =  "sale"
            }
            else -> {
                title = resources.getString(R.string.rent).uppercase()
                purposeType =  "rent"
            }
        }
        Utils.customOnToolbar(this, title){finish()}

        linearLayoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = linearLayoutManager
        buyRentSaleAdapter = BuyRentSaleAdapter(buySaleRentModelList, callBackItemDetail)
        recyclerView.adapter = buyRentSaleAdapter

        requestListItem()

        onScrollInfoRecycle()

        refreshLayout.setColorSchemeResources(R.color.colorPrimary)
        refreshLayout.setOnRefreshListener { this.refreshItemList() }
    }

    private fun refreshItemList(){
        currentPage = 1
        size = 10
        isScrolling = true
        buyRentSaleAdapter.clear()
        requestListItem()
    }

    private fun requestListItem(){
        progressBar.visibility = View.VISIBLE
        BuySaleRentWs().getListSaleAndRentPropertyWs(this, currentPage, 10, purposeType, minPriceFilter, maxPriceFilter,callBackSaleRent )
    }

    private val callBackSaleRent : BuySaleRentWs.OnCallBackListSaleRentListener = object : BuySaleRentWs.OnCallBackListSaleRentListener{
        @SuppressLint("NotifyDataSetChanged")
        override fun onSuccess(listProperty: ArrayList<SaleAndRentProperties>) {
            progressBar.visibility = View.GONE
            refreshLayout.isRefreshing = false
            buySaleRentModelList.addAll(listProperty)
            noDataTv.visibility = if (buySaleRentModelList.size > 0) View.GONE else View.VISIBLE
            buyRentSaleAdapter.notifyDataSetChanged()
        }

        override fun onFailed(error: String) {
            progressBar.visibility = View.GONE
            refreshLayout.isRefreshing = false
            Utils.customToastMsgError(this@BuyRentSaleActivity, error, false)
        }
    }

    // Detail call Back
    private val callBackItemDetail : BuyRentSaleAdapter.CallBackClickItemListener = object : BuyRentSaleAdapter.CallBackClickItemListener{
        override fun onClickItemProperty(saleAndRentProperties: SaleAndRentProperties) {
           val intent = Intent(this@BuyRentSaleActivity, DetailListSaleRentActivity::class.java)
            intent.putExtra("id_list_detail", saleAndRentProperties.id)
           startActivity(intent)
        }

    }

    private fun onScrollInfoRecycle() {
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true
                }
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                currentItem = linearLayoutManager.childCount
                total = linearLayoutManager.itemCount
                scrollDown = linearLayoutManager.findFirstVisibleItemPosition()
                if (dy > 0) {
                    if (isScrolling && currentItem + scrollDown == total) {
                        if (total == size) {
                            isScrolling = false
                            currentPage++
                            progressBar.visibility = View.VISIBLE
                            recyclerView.scrollToPosition(buySaleRentModelList.size - 1)
                            requestListItem()
                            size += 10
                        } else {
                            progressBar.visibility = View.GONE
                        }
                    }
                }
            }
        })
    }

    private fun selectFilterMaxMinPrice(action : String) {
        val monthDialog = DateNumPickerFragment.newInstance(action)
        monthDialog.CallBackListener { _, value, id ->
            for (item in addListFilterPrice()){
                if (item.id == id){
                    minPriceFilter = item.minPrice
                    maxPriceFilter = item.maxPrice

                    filterTv.text = value
                    break
                }
            }
            refreshItemList()
        }
        monthDialog.show(supportFragmentManager, "dialog_fragment")
    }

    private fun addListFilterPrice() : ArrayList<MinMaxPriceModel>{
        val list : ArrayList<MinMaxPriceModel> = ArrayList()
        list.add(MinMaxPriceModel("1",  "", ""))
        list.add(MinMaxPriceModel("2",  "100", "500"))
        list.add(MinMaxPriceModel("3",  "500", "5000"))
        list.add(MinMaxPriceModel("4", "5000", "50000"))
        list.add(MinMaxPriceModel("5",  "50000", "500000"))
        list.add(MinMaxPriceModel("6", "500000", "1000000"))
        list.add(MinMaxPriceModel("7", "1000000", "10000000"))
        list.add(MinMaxPriceModel("8", "10000000", "0"))
        return list
    }
}