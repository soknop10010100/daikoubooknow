package com.eazy.daikou.ui.home.service_provider.adapter;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.eazy.daikou.R;
import com.eazy.daikou.request_data.static_data.StaticUtilsKey;
import com.eazy.daikou.model.my_property.Child;
import com.eazy.daikou.model.my_property.service_provider.SelectPropertyModel;
import com.eazy.daikou.model.inspection.Author_ClerkUserInfo;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ItemCategoryAdapter extends RecyclerView.Adapter<ItemCategoryAdapter.ViewHolder>{

    private List<Child> stringList;
    private ItemClickListCategory clickListCategory;
    private List<SelectPropertyModel> selectPropertyGuests;
    private List<Author_ClerkUserInfo> clerkUserInfoList;
    private ClickPropertyCallBack clickPropertyCallBack;
    private String action = "";
    private ClickEmployeeCallBack clickEmployeeCallBack;

    public ItemCategoryAdapter(List<Child> stringList, ItemClickListCategory clickListCategory) {
        this.stringList = stringList;
        this.clickListCategory = clickListCategory;
    }

    public ItemCategoryAdapter(List<SelectPropertyModel> selectPropertyGuests, String action, ClickPropertyCallBack clickPropertyCallBack) {
        this.selectPropertyGuests = selectPropertyGuests;
        this.action = action;
        this.clickPropertyCallBack = clickPropertyCallBack;
    }

    public ItemCategoryAdapter(List<Author_ClerkUserInfo> clerkUserInfoList, ClickEmployeeCallBack clickEmployeeCallBack) {
        this.clerkUserInfoList = clerkUserInfoList;
        this.action = StaticUtilsKey.department_action;
        this.clickEmployeeCallBack = clickEmployeeCallBack;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view  = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_image_title_layout,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        if (action.equals("")) {
            Child name = stringList.get(position);
            holder.nameTv.setText(name.getName());
            holder.checkClick.setVisibility(name.isClick() ? View.VISIBLE : View.INVISIBLE);
            holder.itemView.setOnClickListener(view -> {
                clickListCategory.onClickItem(name);
                holder.nameTv.setTextColor(ContextCompat.getColor(holder.nameTv.getContext(), R.color.colorPrimary));

            });
        } else {
            if (action.equalsIgnoreCase(StaticUtilsKey.department_action)){
                Author_ClerkUserInfo author_clerkUserInfo = clerkUserInfoList.get(position);
                if (author_clerkUserInfo != null) {
                    if (author_clerkUserInfo.isClick()) {
                        holder.checkClick.setVisibility(View.VISIBLE);
                    } else {
                        holder.checkClick.setVisibility(View.INVISIBLE);
                    }

                    holder.nameTv.setText(author_clerkUserInfo.getEmployeeFullName() != null ? author_clerkUserInfo.getEmployeeFullName() : ". . .");

                    holder.itemView.setOnClickListener(view -> clickEmployeeCallBack.clickSelectPropertyBack(author_clerkUserInfo));

                    holder.iconProfile.setVisibility(View.VISIBLE);
                    if (author_clerkUserInfo.getEmployeeProfilePhoto() != null){
                        Picasso.get().load(author_clerkUserInfo.getEmployeeProfilePhoto()).into(holder.iconProfile);
                    } else {
                        Picasso.get().load(R.drawable.no_image).into(holder.iconProfile);
                    }

                }
            } else if (action.equalsIgnoreCase(StaticUtilsKey.assignee_employee_action)){
                SelectPropertyModel selectPropertyGuest = selectPropertyGuests.get(position);
                if (selectPropertyGuest != null) {
                    if (selectPropertyGuest.isClick()) {
                        holder.checkClick.setVisibility(View.VISIBLE);
                    } else {
                        holder.checkClick.setVisibility(View.INVISIBLE);
                    }
                    holder.nameTv.setText(selectPropertyGuest.getDepartmentName() != null ? selectPropertyGuest.getDepartmentName() : ". . .");
                    holder.itemView.setOnClickListener(view -> clickPropertyCallBack.clickSelectPropertyBack(selectPropertyGuest));

                }
            } else {        // action = select_property
                SelectPropertyModel selectPropertyGuest = selectPropertyGuests.get(position);
                if (selectPropertyGuest != null) {
                    holder.checkClick.setVisibility(selectPropertyGuest.isClick() ? View.VISIBLE : View.INVISIBLE);

                    if (action.equalsIgnoreCase(StaticUtilsKey.unit_no_action)) {
                        holder.nameTv.setText(selectPropertyGuest.getUnitNo() != null ? selectPropertyGuest.getUnitNo() : ". . .");
                    } else if (action.equalsIgnoreCase(StaticUtilsKey.department_action) || action.equalsIgnoreCase(StaticUtilsKey.department_employee_action)) {
                        holder.nameTv.setText(selectPropertyGuest.getDepartmentName() != null ? selectPropertyGuest.getDepartmentName() : ". . .");
                    } else if (action.equalsIgnoreCase("select_property")){
                        holder.nameTv.setText(selectPropertyGuest.getAccountName() != null ? selectPropertyGuest.getAccountName() : ". . .");
                    } else {
                        holder.nameTv.setText(selectPropertyGuest.getPropertyName() != null ? selectPropertyGuest.getPropertyName() : ". . .");
                    }
                    holder.itemView.setOnClickListener(view -> clickPropertyCallBack.clickSelectPropertyBack(selectPropertyGuest));

                }
            }
        }
    }

    @Override
    public int getItemCount() {
        if (action.equals("")){
            return stringList.size();
        } else {
            if (action.equalsIgnoreCase(StaticUtilsKey.department_action)) {
                return clerkUserInfoList.size();
            } else {
                return selectPropertyGuests.size();
            }
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private final ImageView checkClick;
        private final TextView nameTv;
        private final CircleImageView iconProfile;

        public ViewHolder(View view) {
            super(view);
            iconProfile = view.findViewById(R.id.imgProfile);
            checkClick = view.findViewById(R.id.checkClick);
            nameTv = view.findViewById(R.id.item_name);
        }
    }

    public interface ItemClickListCategory{
        void onClickItem(Child list);
    }

    public interface ClickPropertyCallBack{
        void clickSelectPropertyBack(SelectPropertyModel floorSpinnerModel);
    }

    public interface ClickEmployeeCallBack{
        void clickSelectPropertyBack(Author_ClerkUserInfo clerkUserInfo);
    }
}
