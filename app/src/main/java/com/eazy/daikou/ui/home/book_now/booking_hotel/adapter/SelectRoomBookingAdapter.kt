package com.eazy.daikou.ui.home.book_now.booking_hotel.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.booking_hotel.ItemRoomHotelBookingModel

class SelectRoomBookingAdapter(private val context : Context, private val list : ArrayList<ItemRoomHotelBookingModel>, private val onClickListener : OnClickCallBackLister) : RecyclerView.Adapter<SelectRoomBookingAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.booking_item_list_layout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = list[position]
        if (item != null){
            if (item.size !=  null){
                val ms2: String = String.format(context.resources.getString(R.string.m_square), item.size)
                holder.sizeRoomTv.text = String.format("%s %s", item.size , "m2")
            } else {
                holder.sizeRoomTv.text = "0"
            }
            Glide.with(holder.image).load(if (item.room_image != null) item.room_image else R.drawable.no_image).into(holder.image)
            holder.bathRoomTv.text = if (item.beds != null) "x " + item.beds else ". . ."
            holder.adultsTv.text = if (item.adults != null) "x " + item.adults else ". . ."
            holder.childrenTv.text = if (item.children != null) "x " + item.children else ". . ."

            Utils.setValueOnText(holder.totalRoomTv, item.number.toString())

            //Set price
            Utils.setTextStrikeStyle(holder.priceDiscount)
            if (item.original_price_display != null) {
                holder.priceDiscount.text = String.format("%s", item.original_price_display)
                if (item.price_display != null){
                    holder.priceTv.text = String.format("%s", item.price_display)
                } else {
                    holder.priceTv.text = "- - -"
                }
            } else {
                holder.priceDiscount.visibility = View.GONE
                if (item.price_display != null){
                    holder.priceTv.text = String.format("%s", item.price_display)
                } else {
                    holder.priceTv.text = ". . ."
                }
            }

            Utils.setValueOnText(holder.roomTv, item.room_name)
            holder.iconTechService.visibility = if (item.isClick) View.VISIBLE else View.GONE

            //check full room
            holder.availableLayout.visibility = if (item.full_status == "not_full") View.GONE else View.VISIBLE
            holder.mainLayout.visibility = if (item.full_status == "not_full") View.GONE else View.VISIBLE

            holder.numRoomTv.text = String.format("%s %s", "$", Utils.formatDecimalFormatValue(item.totalAllPrice))
            holder.quantityTv.text = String.format("%s", item.totalRoom)

            holder.iconAdd.isEnabled = item.number > item.quantity
            holder.iconRemove.isEnabled = item.quantity > 0

            if (item.number > 0 && item.number > item.quantity){
                Utils.setBgTint(holder.iconAdd, R.color.yellow)
            } else {
                Utils.setBgTint(holder.iconAdd, R.color.gray)
            }

            if (item.number > 0 && item.quantity > 0){
                Utils.setBgTint(holder.iconRemove, R.color.yellow)
            } else {
                Utils.setBgTint(holder.iconRemove, R.color.gray)
            }

            holder.iconAdd.setOnClickListener(
                CustomSetOnClickViewListener{
                    if (item.full_status == "not_full"){
                        onClickListener.onClickSpinnerCallBack(item, "add", holder.iconAdd)
                    }
                }
            )
            holder.iconRemove.setOnClickListener(
                CustomSetOnClickViewListener{
                    if (item.full_status == "not_full"){
                        onClickListener.onClickSpinnerCallBack(item, "remove", holder.iconRemove)
                    }
                }
            )
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val priceTv : TextView = itemView.findViewById(R.id.price)
        val roomTv : TextView = itemView.findViewById(R.id.roomTv)
        val sizeRoomTv : TextView = itemView.findViewById(R.id.sizeRoomTv)
        val bathRoomTv : TextView = itemView.findViewById(R.id.bathRoomTv)
        val adultsTv : TextView = itemView.findViewById(R.id.adultsTv)
        val childrenTv : TextView = itemView.findViewById(R.id.childrenTv)
        val image : ImageView = itemView.findViewById(R.id.image)
        val numRoomTv : TextView = itemView.findViewById(R.id.numRoomTv)
        val iconTechService : ImageView = itemView.findViewById(R.id.iconTechService)
        val availableLayout : RelativeLayout = itemView.findViewById(R.id.availableLayout)
        val mainLayout : TextView = itemView.findViewById(R.id.mainLayout)
        val iconRemove: ImageView = itemView.findViewById(R.id.iconMinus)
        val iconAdd: ImageView = itemView.findViewById(R.id.iconAdd)
        val totalRoomTv : TextView = itemView.findViewById(R.id.totalRoomTv)
        val quantityTv : TextView = itemView.findViewById(R.id.quantityTv)
        val priceDiscount : TextView = itemView.findViewById(R.id.priceDiscount)
    }

    interface OnClickCallBackLister{
        fun onClickSpinnerCallBack(itemRoomHotelBookingModel: ItemRoomHotelBookingModel, action : String, imageView : ImageView)
    }

    fun clear() {
        val size: Int = list.size
        if (size > 0) {
            for (i in 0 until size) {
                list.removeAt(0)
            }
            notifyItemRangeRemoved(0, size)
        }
    }
}