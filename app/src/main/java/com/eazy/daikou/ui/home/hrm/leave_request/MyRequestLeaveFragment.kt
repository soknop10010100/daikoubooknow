package com.eazy.daikou.ui.home.hrm.leave_request

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.base.BaseFragment
import com.eazy.daikou.request_data.request.hr_ws.LeaveManagementWs
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.hr.LeaveBalance
import com.eazy.daikou.model.hr.LeaveManagement
import com.eazy.daikou.model.profile.User
import com.eazy.daikou.ui.home.hrm.leave_request.adapter.LeaveAttendanceAdapter
import com.eazy.daikou.ui.home.hrm.leave_request.adapter.TypeLeaveCategoryAdapter
import com.google.gson.Gson

class MyRequestLeaveFragment : BaseFragment() {

    private lateinit var recyclerView : RecyclerView
    private lateinit var leaveAdapter : LeaveAttendanceAdapter
    private lateinit var layoutManager : LinearLayoutManager
    private lateinit var progressBar : ProgressBar
    private lateinit var refreshLayout : SwipeRefreshLayout
    private lateinit var relateNoItem : RelativeLayout
    private lateinit var recyclerViewLeaveCategory : RecyclerView
    private lateinit var leaveBalanceCategoryAdapter : TypeLeaveCategoryAdapter
    private var listLeave = ArrayList<LeaveManagement>()
    private val listCategory = ArrayList<LeaveBalance>()
    private var currentItem = 0
    private var total : Int = 0
    private var scrollDown : Int = 0
    private var currentPage : Int = 1
    private var size : Int = 10
    private var isScrolling = false
    private var action = ""
    private var leaveId = ""
    private lateinit var btnCreateLeave : ImageView
    private var REQUEST_CODE_CREATE_LEAVE = 376
    private var userBusinessKey = ""
    private var accountBusinessKey = ""
    private lateinit var user: User


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_my_request_leave, container, false)

        initView(view)

        initData()

        initAction()

        return view

    }

    private fun initView(view : View){

        recyclerView = view.findViewById(R.id.list_attendance_leave)
        progressBar = view.findViewById(R.id.progressItem)
        refreshLayout = view.findViewById(R.id.swipe_layouts)
        relateNoItem = view.findViewById(R.id.image_no_record)
        btnCreateLeave = view.findViewById(R.id.btn_create_leave)
        recyclerViewLeaveCategory = view.findViewById(R.id.list_leave_category)

    }

    @SuppressLint("SetTextI18n")
    private fun initData(){
        user = Gson().fromJson(UserSessionManagement(mActivity).userDetail, User::class.java)
        val businessKy = user.userBusinessKey
        if (businessKy != null){
            userBusinessKey = businessKy
        }

        val accBusinessKey = user.activeAccountBusinessKey
        if (accBusinessKey != null){
            accountBusinessKey = accBusinessKey
        }

        if (activity?.intent != null && activity?.intent!!.hasExtra("id")) {
            leaveId = activity?.intent!!.getStringExtra("id").toString()
        }


        layoutManager = LinearLayoutManager(mActivity)
        recyclerView.layoutManager = layoutManager

        getLeaveCategory()

        getListLeave()

    }

    private fun initAction(){
        refreshLayout.setColorSchemeResources(R.color.colorPrimary)
        refreshLayout.setOnRefreshListener {refreshItemList()}

        btnCreateLeave.setOnClickListener {
            startActivityForResult(Intent(mActivity, CreateLeaveRequestActivity::class.java), REQUEST_CODE_CREATE_LEAVE)
            btnCreateLeave.isEnabled = false
        }

        onScrollMyLeave()
    }

    private fun refreshItemList(){
        currentPage = 1
        size = 10
        progressBar.visibility = View.GONE
        leaveAdapter.clear()
        if(listCategory.size>0){
            listCategory.clear()
            leaveBalanceCategoryAdapter.clear()
            getLeaveCategory()
        }

        getListLeave()
    }

    private val itemClick = object : LeaveAttendanceAdapter.ItemClickLeaveListener{
        override fun onItemClick(leave: LeaveManagement) {
            startLeaveDetail(leave.id)
        }
    }

    private fun startLeaveDetail(leaveId : String){
        val intent = Intent(mActivity,LeaveRequestDetailActivity::class.java)
        intent.putExtra("id", leaveId)
        intent.putExtra("list_type", "my_list")
        startActivity(intent)
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun getLeaveCategory(){
        val layout = GridLayoutManager(mActivity,1,RecyclerView.HORIZONTAL,false)
        recyclerViewLeaveCategory.layoutManager = layout

        LeaveManagementWs().getLeaveCategoryManagement(mActivity, userBusinessKey,leaveCallBack)

    }

    private fun onScrollMyLeave() {
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true
                }
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                currentItem = layoutManager.childCount
                total = layoutManager.itemCount
                scrollDown = layoutManager.findFirstVisibleItemPosition()
                if (dy > 0) {
                    if (isScrolling && currentItem + scrollDown == total) {
                        if (total == size) {
                            isScrolling = false
                            currentPage++
                            progressBar.visibility = View.VISIBLE
                            recyclerView.scrollToPosition(listLeave.size - 1)
                            size += 10
                            getListLeave()
                        } else {
                            progressBar.visibility = View.GONE
                        }
                    }
                }
            }
        })
    }
    private fun getListLeave(){
        progressBar.visibility = View.VISIBLE
        LeaveManagementWs().getLeaveManagement(mActivity, userBusinessKey, currentPage, 10, "my_list", accountBusinessKey, leaveCallBack)

        leaveAdapter = LeaveAttendanceAdapter(listLeave,mActivity,itemClick)
        recyclerView.adapter = leaveAdapter
    }

    private val leaveCallBack = object : LeaveManagementWs.OnCallBackLeaveManagementListener{

        @SuppressLint("NotifyDataSetChanged")
        override fun onLoadListSuccessFull(listLeaveManagement: ArrayList<LeaveManagement>) {
            progressBar.visibility = View.GONE
            refreshLayout.isRefreshing = false
            btnCreateLeave.visibility = View.VISIBLE
            listLeave.addAll(listLeaveManagement)

            recyclerView.visibility = if(listLeave.size > 0) View.VISIBLE else View.GONE
            relateNoItem.visibility = if(listLeave.size > 0) View.GONE else View.VISIBLE
            leaveAdapter.notifyDataSetChanged()

            // Start detail from notification after request list already
            if (action == "leave_application") {
                startLeaveDetail(leaveId)
                action = ""
            }
        }

        @SuppressLint("NotifyDataSetChanged")
        override fun onLoadListCategorySuccessFull(leaveBalance: ArrayList<LeaveBalance>) {

            listCategory.addAll(leaveBalance)
            leaveBalanceCategoryAdapter = TypeLeaveCategoryAdapter(listCategory,mActivity)
            recyclerViewLeaveCategory.adapter = leaveBalanceCategoryAdapter

        }

        override fun onLoadFail(message: String) {
            progressBar.visibility = View.GONE
            Utils.customToastMsgError(mActivity, message, false)
            refreshLayout.isRefreshing = false
        }
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (data != null) {
            if (data.hasExtra("isClickedFirst")) {
                val statusResult = data.getBooleanExtra("isClickedFirst", false)
                if (statusResult) {
                    btnCreateLeave.isEnabled = true
                }
            }
        } else {
            if (resultCode == BaseActivity.RESULT_OK && requestCode == REQUEST_CODE_CREATE_LEAVE) {
                refreshItemList()
                btnCreateLeave.isEnabled = true
            }
        }
    }
}