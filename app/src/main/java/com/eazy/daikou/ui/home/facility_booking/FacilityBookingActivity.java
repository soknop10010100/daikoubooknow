package com.eazy.daikou.ui.home.facility_booking;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.eazy.daikou.R;
import com.eazy.daikou.base.BaseActivity;
import com.eazy.daikou.request_data.request.facility_booking_ws.FacilityBookingWs;
import com.eazy.daikou.helper.Utils;
import com.eazy.daikou.model.facility_booking.AlertPropertyBookingModel;
import com.eazy.daikou.model.facility_booking.FacilityBookingModel;
import com.eazy.daikou.model.facility_booking.VenuePropertyBookingModel;
import com.eazy.daikou.ui.home.facility_booking.adapter.AlertTypePropertyFBAdapter;
import com.eazy.daikou.ui.home.facility_booking.adapter.FacilityBookingAdapter;
import com.eazy.daikou.ui.home.facility_booking.adapter.FacilityBookingGridAdapter;
import com.eazy.daikou.ui.home.facility_booking.adapter.VanueBookingAdapter;
import com.eazy.daikou.ui.home.my_property_v1.EasyDialogClass;
import com.michael.easydialog.EasyDialog;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class FacilityBookingActivity extends BaseActivity {

    private RecyclerView recyclerViewOneCol, recyclerViewTwoCol,  recyclerViewScrollProperty;
    private ImageView btnSwitch, btnSearch;
    private LinearLayout linearTypeProperty;
    private TextView switchProperty;

    private EasyDialog easyDialog;
    private VanueBookingAdapter scrollAdapter;
    private SwipeRefreshLayout swipeRefreshLayout;
    private FacilityBookingAdapter oneColumnAdapter;
    private FacilityBookingGridAdapter twoColumnAdapter;

    private GridLayoutManager gridLayoutManager;
    private LinearLayoutManager linearLayoutManager;
    private ProgressBar progressbar;

    private final ArrayList<FacilityBookingModel> facilityBookingArrayList = new ArrayList<>();
    private final ArrayList<AlertPropertyBookingModel> propertyBookingModelList = new ArrayList<>();
    private final List<AlertPropertyBookingModel> propertySearchBookingList = new ArrayList<>();
    private final ArrayList<VenuePropertyBookingModel> venuePropertyBookingModelList = new ArrayList<>();

    private String propertyId = "", facilityID = "", facilityName = "", dateTimeToday = "";
    private int currentItem, total, scrollDown, currentPage = 1, size = 10;
    private boolean isClickSwitchItemList = true, isScrolling, isClickSelectProperty = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_facility_booking);

       initView();

       initAction();

    }

    private void initView() {
        recyclerViewOneCol = findViewById(R.id.recyclerList_FacilityBooking);
        recyclerViewTwoCol = findViewById(R.id.recyclerGrid_FacilityBooking_Grid);
        recyclerViewScrollProperty = findViewById(R.id.RvScrollPropertyBooking);
        btnSwitch = findViewById(R.id.btn_switchList);
        linearTypeProperty = findViewById(R.id.linearTypeProperty);
        switchProperty = findViewById(R.id.switchProperty);
        btnSearch = findViewById(R.id.ic_printer);

        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        progressbar = findViewById(R.id.progressItem);

        btnSearch.setImageResource(R.drawable.ic_search);
        btnSearch.setVisibility(View.VISIBLE);
        Utils.customOnToolbar(this, getResources().getString(R.string.facility_booking), this::finish);

        findViewById(R.id.layoutMap).setVisibility(View.INVISIBLE);
    }

    private void initAction() {
        SimpleDateFormat dateFormatServer = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        SimpleDateFormat timeFormat = new SimpleDateFormat("k:mm:ss", Locale.getDefault());
        Date date = new Date();
        dateTimeToday = dateFormatServer.format(date) + " " + timeFormat.format(date);

        linearLayoutManager = new LinearLayoutManager(this);
        recyclerViewOneCol.setLayoutManager(linearLayoutManager);
        gridLayoutManager =new GridLayoutManager(this,2);
        recyclerViewTwoCol.setLayoutManager(gridLayoutManager);

        onRequestPropertyBooking("");

        onScrollViewItemList(recyclerViewOneCol);

        onScrollViewItemList(recyclerViewTwoCol);

        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
        swipeRefreshLayout.setOnRefreshListener(this::onRefreshOneLayout);

        // Select Property
        linearTypeProperty.setOnClickListener(v-> openAlertProperty(linearTypeProperty));

        // Select Property
        btnSwitch.setOnClickListener(v->{
            progressbar.setVisibility(View.GONE);
            if (isClickSwitchItemList){
                isClickSwitchItemList = false;
                btnSwitch.setImageResource(R.drawable.ic_file_row);
                recyclerViewOneCol.setVisibility(View.VISIBLE);
                recyclerViewTwoCol.setVisibility(View.GONE);
            } else {
                isClickSwitchItemList = true;
                btnSwitch.setImageResource(R.drawable.ic_baseline_filter_24);
                recyclerViewOneCol.setVisibility(View.GONE);
                recyclerViewTwoCol.setVisibility(View.VISIBLE);
            }
        });

        btnSearch.setOnClickListener(v->{
            Intent intent = new Intent(FacilityBookingActivity.this, SearchFacilityBookingActivity.class);
            intent.putExtra("facility_id", facilityID);
            intent.putExtra("facility_name",facilityName);
            intent.putExtra("property_list", propertyBookingModelList);
            intent.putExtra("vanue_list", venuePropertyBookingModelList);
           startActivity(intent);
        });

    }

    private void onRefreshOneLayout(){
        currentPage = 1;
        size = 10;
        isScrolling = true;

        if (isClickSwitchItemList){
            twoColumnAdapter.clear();
        }else {
            oneColumnAdapter.clear();
        }

        if (!isClickSelectProperty) {
            requestSpaceFacilityList();
        }
    }

    private void setScrollProperty(){
        LinearLayoutManager lmScroll = new LinearLayoutManager(this,RecyclerView.HORIZONTAL,false);
        recyclerViewScrollProperty.setLayoutManager(lmScroll);
        scrollAdapter = new VanueBookingAdapter(this, venuePropertyBookingModelList,clickItemCallBackListener);
        recyclerViewScrollProperty.setAdapter(scrollAdapter);

        onRequestVenuePropertyBooking();
    }

    private void initOneRecyclerView(){
        oneColumnAdapter = new FacilityBookingAdapter(this,facilityBookingArrayList,onClickCallbackList);
        recyclerViewOneCol.setAdapter(oneColumnAdapter);
    }

    private void initGridRecycleView(){
        twoColumnAdapter =new FacilityBookingGridAdapter(this,facilityBookingArrayList,onClickCallBackGrid);
        recyclerViewTwoCol.setAdapter(twoColumnAdapter);

        requestSpaceFacilityList();
    }

    private final FacilityBookingAdapter.onClickCallbackList onClickCallbackList = this::startSpaceBookingDetail;

    private final FacilityBookingGridAdapter.onClickCallBackGrid onClickCallBackGrid = this::startSpaceBookingDetail;

    private void onScrollViewItemList(RecyclerView recyclerViewItem){
        recyclerViewItem.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true;
                }
            }
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                currentItem = isClickSwitchItemList ? gridLayoutManager.getChildCount() : linearLayoutManager.getChildCount();
                total = isClickSwitchItemList ? gridLayoutManager.getItemCount() : linearLayoutManager.getItemCount();
                scrollDown = isClickSwitchItemList ? gridLayoutManager.findFirstVisibleItemPosition() : linearLayoutManager.findFirstVisibleItemPosition();
                if (dy > 0){
                    if (isScrolling && ( currentItem + scrollDown == total)){
                        if ( total == size){
                            isScrolling = false;
                            currentPage ++;
                            progressbar.setVisibility(View.VISIBLE);
                            recyclerViewItem.scrollToPosition(facilityBookingArrayList.size() - 1);
                            requestSpaceFacilityList();
                            size += 10;
                        } else {
                            progressbar.setVisibility(View.GONE);
                        }
                    }
                }
            }
        });
    }

    private void openAlertProperty(LinearLayout linearTypeProperty){
        easyDialog = new EasyDialog(this);
        @SuppressLint("InflateParams") View view = LayoutInflater.from(this).inflate(R.layout.layout_alert_property_facility_booking,null);
        EasyDialogClass.createFilterEasyDialog(easyDialog, this,view,linearTypeProperty, false);
        RecyclerView recyclerView = view.findViewById(R.id.recyclerFB_TypeProperty);
        ProgressBar progressBarProperty = view.findViewById(R.id.progressItem);
        progressBarProperty.setVisibility(View.GONE);

        EditText searchPropertyEt = view.findViewById(R.id.searchEt);
        view.findViewById(R.id.headerTitleLayout).setVisibility(View.GONE);
        searchPropertyEt.setVisibility(View.VISIBLE);
        TextView noItemPropertyTv = view.findViewById(R.id.noItemPropertyTv);

        propertySearchBookingList.clear();
        propertySearchBookingList.addAll(propertyBookingModelList);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        AlertTypePropertyFBAdapter alertTypePropertyFBAdapter = new AlertTypePropertyFBAdapter(this, propertySearchBookingList, itemClickBack);
        recyclerView.setAdapter(alertTypePropertyFBAdapter);

        searchPropertyEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @SuppressLint("NotifyDataSetChanged")
            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().isEmpty()){
                    progressBarProperty.setVisibility(View.VISIBLE);
                    searchProperty(editable.toString(), recyclerView, noItemPropertyTv, progressBarProperty, alertTypePropertyFBAdapter);
                } else {
                    clearTextListProperty(recyclerView, noItemPropertyTv, alertTypePropertyFBAdapter);
                }
            }
        });

    }

    private void searchProperty(String keySearch, RecyclerView recyclerView, TextView noItemTv, ProgressBar progressBar, AlertTypePropertyFBAdapter alertTypePropertyFBAdapter){
        new FacilityBookingWs().getPropertyBooking(this, keySearch, new FacilityBookingWs.BookingPropertyCallBackListener() {
            @SuppressLint("NotifyDataSetChanged")
            @Override
            public void onLoadSuccessFull(List<AlertPropertyBookingModel> alertPropertyBookingModelList) {
                progressBar.setVisibility(View.GONE);
                propertySearchBookingList.clear();
                propertySearchBookingList.addAll(alertPropertyBookingModelList);

                alertTypePropertyFBAdapter.notifyDataSetChanged();
                recyclerView.setVisibility(propertySearchBookingList.size() > 0 ? View.VISIBLE : View.GONE);
                noItemTv.setVisibility(propertySearchBookingList.size() > 0 ? View.GONE : View.VISIBLE);
            }

            @Override
            public void onLoadFailed(String error) {
                progressBar.setVisibility(View.GONE);
                Utils.customToastMsgError(FacilityBookingActivity.this, error, false);
            }
        });
    }

    @SuppressLint("NotifyDataSetChanged")
    private void clearTextListProperty(RecyclerView recyclerView, TextView noItemTv, AlertTypePropertyFBAdapter alertTypePropertyFBAdapter){
        propertySearchBookingList.clear();
        progressbar.setVisibility(View.GONE);
        noItemTv.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);

        propertySearchBookingList.addAll(propertyBookingModelList);
        alertTypePropertyFBAdapter.notifyDataSetChanged();
    }

    private void onRequestPropertyBooking(String keySearch){
        new FacilityBookingWs().getPropertyBooking(this, keySearch, bookingPropertyCallBackListener);
    }

    private  void onRequestVenuePropertyBooking(){
        if (propertyId.equals("all")){
            new FacilityBookingWs().getVenueFacilityBooking(this,"all", propertyId, venuePropertyBookingCallBackListener);
        } else {
            new FacilityBookingWs().getVenueFacilityBooking(this,"property", propertyId, venuePropertyBookingCallBackListener);
        }
    }

    private void startSpaceBookingDetail(FacilityBookingModel facilityBooking){
        String priceVal = "";
        if (facilityBooking.getSpacePricing() != null){
            if (facilityBooking.getSpacePricing().getPrice() != null) {
                priceVal = facilityBooking.getSpacePricing().getPrice();
            }
        }

        Intent intent = new Intent(this, FacilityBookingDetailActivity.class);
        intent.putExtra("data", priceVal);
        intent.putExtra("space_id",facilityBooking.getSpaceId());
        startActivity(intent);
    }

    private final AlertTypePropertyFBAdapter.ItemClickBack itemClickBack = new AlertTypePropertyFBAdapter.ItemClickBack() {
        @SuppressLint("NotifyDataSetChanged")
        @Override
        public void onClickItemBack(AlertPropertyBookingModel alertPropertyBookingModel, VenuePropertyBookingModel venuePropertyBookingModel) {
            switchProperty.setText(alertPropertyBookingModel.getPropertyName());
            propertyId = alertPropertyBookingModel.getPropertyId();

            for (AlertPropertyBookingModel property : propertyBookingModelList) {
                property.setClick(property.getPropertyId().equalsIgnoreCase(propertyId));
            }

            propertySearchBookingList.clear();
            propertySearchBookingList.addAll(propertyBookingModelList);

            isClickSelectProperty = true;
            onRefreshOneLayout();

            onRequestVenuePropertyBooking();
            easyDialog.dismiss();
        }
    };

    private final VanueBookingAdapter.clickItemCallBackListener clickItemCallBackListener = new VanueBookingAdapter.clickItemCallBackListener() {
        @SuppressLint("NotifyDataSetChanged")
        @Override
        public void clickItemCallBack(VenuePropertyBookingModel venuePropertyBookingModel) {
            facilityID = venuePropertyBookingModel.getFacilityBasicId();
            facilityName = venuePropertyBookingModel.getVenueName();
            for (VenuePropertyBookingModel item: venuePropertyBookingModelList) {
                item.setClickTitle(item.getFacilityBasicId().equals(venuePropertyBookingModel.getFacilityBasicId()));
                scrollAdapter.notifyDataSetChanged();
            }
            onRefreshOneLayout();
        }
    };

    private void requestSpaceFacilityList(){
        progressbar.setVisibility(View.VISIBLE);
        new FacilityBookingWs().getSpaceFacilityList(this, currentPage, 10, propertyId, facilityID, dateTimeToday, callBackListFacilityListenerListener);
    }

    private final FacilityBookingWs.CallBackListFacilityListener callBackListFacilityListenerListener = new FacilityBookingWs.CallBackListFacilityListener() {
        @SuppressLint("NotifyDataSetChanged")
        @Override
        public void onLoadSuccessFull(List<FacilityBookingModel> list) {
            progressbar.setVisibility(View.GONE);
            swipeRefreshLayout.setRefreshing(false);
            facilityBookingArrayList.addAll(list);
            oneColumnAdapter.notifyDataSetChanged();
            twoColumnAdapter.notifyDataSetChanged();

            isClickSelectProperty = false;
        }

        @Override
        public void onLoadFailed(String message) {
            progressbar.setVisibility(View.GONE);
            swipeRefreshLayout.setRefreshing(false);
            isClickSelectProperty = false;
            Utils.customToastMsgError(FacilityBookingActivity.this, message, false);
        }
    };

    private final FacilityBookingWs.BookingPropertyCallBackListener bookingPropertyCallBackListener = new FacilityBookingWs.BookingPropertyCallBackListener() {
        @Override
        public void onLoadSuccessFull(List<AlertPropertyBookingModel> alertPropertyBookingModelList) {
            AlertPropertyBookingModel alertPropertyBookingModel = new AlertPropertyBookingModel();
            alertPropertyBookingModel.setPropertyId("all");
            alertPropertyBookingModel.setPropertyName("All");
            propertyBookingModelList.add(alertPropertyBookingModel);

            propertyBookingModelList.addAll(alertPropertyBookingModelList);
            switchProperty.setText(propertyBookingModelList.get(0).getPropertyName());
            propertyId = propertyBookingModelList.get(0).getPropertyId();

            propertyBookingModelList.get(0).setClick(true);

            setScrollProperty();

        }
        @Override
        public void onLoadFailed(String error) {
            Utils.customToastMsgError(FacilityBookingActivity.this, error, false);
        }
    };

    private final FacilityBookingWs.VenuePropertyBookingCallBackListener venuePropertyBookingCallBackListener = new FacilityBookingWs.VenuePropertyBookingCallBackListener() {
        @SuppressLint("NotifyDataSetChanged")
        @Override
        public void onLoadSuccessFull(List<VenuePropertyBookingModel> listVenue) {
            venuePropertyBookingModelList.clear();

            VenuePropertyBookingModel venuePropertyBookingModel = new VenuePropertyBookingModel();
            venuePropertyBookingModel.setFacilityBasicId("all");
            venuePropertyBookingModel.setVenueName("All");
            venuePropertyBookingModelList.add(venuePropertyBookingModel);
            venuePropertyBookingModelList.addAll(listVenue);
            facilityID = venuePropertyBookingModelList.get(0).getFacilityBasicId();
            facilityName = venuePropertyBookingModelList.get(0).getVenueName();

            for (VenuePropertyBookingModel item: venuePropertyBookingModelList) {
                item.setClickTitle(item.getFacilityBasicId().equals("all"));
            }

            scrollAdapter.notifyDataSetChanged();

            initOneRecyclerView();

            initGridRecycleView();

        }

        @Override
        public void onLoadFailed(String error) {
            Utils.customToastMsgError(FacilityBookingActivity.this, error, false);
        }
    };
}