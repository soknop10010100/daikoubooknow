package com.eazy.daikou.ui.home.facility_booking;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.eazy.daikou.R;
import com.eazy.daikou.base.BaseActivity;
import com.eazy.daikou.request_data.request.facility_booking_ws.FacilityBookingWs;
import com.eazy.daikou.helper.Utils;
import com.eazy.daikou.model.facility_booking.AlertPropertyBookingModel;
import com.eazy.daikou.model.facility_booking.FacilityBookingModel;
import com.eazy.daikou.model.facility_booking.VenuePropertyBookingModel;
import com.eazy.daikou.ui.home.facility_booking.adapter.AlertTypePropertyFBAdapter;
import com.eazy.daikou.ui.home.facility_booking.adapter.FacilityBookingAdapter;
import com.eazy.daikou.ui.home.my_property_v1.EasyDialogClass;
import com.michael.easydialog.EasyDialog;

import java.util.ArrayList;
import java.util.List;

public class SearchFacilityBookingActivity extends BaseActivity {

    private EditText searchItemEdt;
    private TextView textPropertySearchTv;
    private TextView textFacilitySearchTv;
    private LinearLayoutManager linearLayoutManager;
    private ProgressBar progressBar;
    private RecyclerView recyclerViewSearch;
    private FacilityBookingAdapter facilityBookingAdapter;
    private ImageView btnClear;
    private LinearLayout txtNoResultLayout;
    private EasyDialog easyDialog;

    private final ArrayList<FacilityBookingModel> facilityBookingArrayList = new ArrayList<>();
    private List<AlertPropertyBookingModel> propertyBookingList = new ArrayList<>();
    private final List<AlertPropertyBookingModel> propertySearchBookingList = new ArrayList<>();
    private List<VenuePropertyBookingModel> venueBookingList = new ArrayList<>();

    private boolean isSelectProperty = true;
    private int currentItem;
    private int total;
    private int scrollDown;
    private int currentPage = 1;
    private int size = 10;
    private boolean isScrolling;
    private String keySearch = "";
    private String propertyId;
    private String facilityId;
    private String facilityName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_facility_booking);

        initView();

        initData();

        initAction();
    }

    private void initView() {
        searchItemEdt = findViewById(R.id.editSearchItem);
        recyclerViewSearch = findViewById(R.id.recyclerView_Search);
        textPropertySearchTv = findViewById(R.id.text_propertySearch);
        textFacilitySearchTv = findViewById(R.id.text_FacilitySearch);
        progressBar = findViewById(R.id.progressItem);
        progressBar.setVisibility(View.GONE);
        btnClear = findViewById(R.id.clear);

        Utils.customOnToolbar(this, getString(R.string.filter_space_for_booking), this::finish);
        findViewById(R.id.layoutMap).setVisibility(View.INVISIBLE);

        // No Item
        TextView noItemTitleTv = findViewById(R.id.noItemTv);
        ImageView emptyBookingImage = findViewById(R.id.imageNoItem);
        findViewById(R.id.continueBookingLayout).setVisibility(View.GONE);
        findViewById(R.id.descriptionNoItemTv).setVisibility(View.INVISIBLE);
        Glide.with(emptyBookingImage).load(R.drawable.no_my_booking_item).into(emptyBookingImage);
        noItemTitleTv.setText(getResources().getString(R.string.no_data_for_display));
        txtNoResultLayout = findViewById(R.id.txtNoResult);
        txtNoResultLayout.setVisibility(View.VISIBLE);
    }

    private void initData(){
        if (getIntent() != null && getIntent().hasExtra("property_list")){
            facilityId = getIntent().getStringExtra("facility_id");
            facilityName = getIntent().getStringExtra("facility_name");
            propertyBookingList = (List<AlertPropertyBookingModel>) getIntent().getSerializableExtra("property_list");
            venueBookingList = (List<VenuePropertyBookingModel>) getIntent().getSerializableExtra("vanue_list");

            propertySearchBookingList.addAll(propertyBookingList);
        }
    }

    private void initAction() {
        for (AlertPropertyBookingModel property : propertySearchBookingList) {
            if (property.isClick()){
                textPropertySearchTv.setText(property.getPropertyName());
                propertyId = property.getPropertyId();
                break;
            }
        }

        textFacilitySearchTv.setText(facilityName);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerViewSearch.setLayoutManager(linearLayoutManager);

        valueSearchList();

        onScrollFacilityList();

        searchItemEdt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @SuppressLint("NotifyDataSetChanged")
            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().isEmpty()){
                    btnClear.setVisibility(View.VISIBLE);
                    facilityBookingAdapter.clear();
                    currentPage = 1;
                    size = 10;
                    keySearch = editable.toString();
                    requestListSearchFacility(editable.toString());

                    btnClear.setOnClickListener(view -> {
                        searchItemEdt.setText("");
                        clearTextList();
                    });
                } else {
                    clearTextList();
                }
            }
        });

        initPropertyVanueBooking();

    }

    private void onScrollFacilityList(){
        recyclerViewSearch.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL){
                    isScrolling = true;
                }
            }
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                currentItem = linearLayoutManager.getChildCount();
                total = linearLayoutManager.getItemCount();
                scrollDown = linearLayoutManager.findFirstVisibleItemPosition();
                if(dy > 0) {
                    Utils.hideKeyboard(SearchFacilityBookingActivity.this);
                    if (isScrolling && (currentItem + scrollDown == total)) {
                        if(total == size) {
                            isScrolling = false;
                            currentPage++;
                            progressBar.setVisibility(View.VISIBLE);
                            recyclerView.scrollToPosition(facilityBookingArrayList.size() - 1);
                            requestListSearchFacility(keySearch);
                            size += 10;
                        } else {
                            progressBar.setVisibility(View.GONE);
                        }
                    }
                }

            }
        });
    }

    private  void requestListSearchFacility(String keySearch){
        progressBar.setVisibility(View.VISIBLE);
        new FacilityBookingWs().getSearchFacility(this, currentPage,10, keySearch, propertyId, facilityId, searchBookingCallback);
    }

    private void valueSearchList(){
        facilityBookingAdapter = new FacilityBookingAdapter(this, facilityBookingArrayList, onClickCallbackList);
        recyclerViewSearch.setAdapter(facilityBookingAdapter);
    }

    private final FacilityBookingAdapter.onClickCallbackList onClickCallbackList = facilityBooking -> {
        Intent intent = new Intent(getBaseContext(),FacilityBookingDetailActivity.class);
        String priceVal = "";
        if (facilityBooking.getSpacePricing().getPrice() != null) {
            priceVal = facilityBooking.getSpacePricing().getPrice();
        }
        intent.putExtra("data", priceVal);
        intent.putExtra("space_id",facilityBooking.getSpaceId());
        startActivity(intent);
    };

    private final FacilityBookingWs.CallBackListFacilityListener searchBookingCallback = new FacilityBookingWs.CallBackListFacilityListener() {
        @SuppressLint("NotifyDataSetChanged")
        @Override
        public void onLoadSuccessFull(List<FacilityBookingModel> listSearch) {
            progressBar.setVisibility(View.GONE);
            facilityBookingArrayList.addAll(listSearch);

            facilityBookingAdapter.notifyDataSetChanged();

            txtNoResultLayout.setVisibility(facilityBookingArrayList.size() > 0 ? View.GONE : View.VISIBLE);
            recyclerViewSearch.setVisibility(facilityBookingArrayList.size() > 0 ? View.VISIBLE : View.GONE);
        }

        @Override
        public void onLoadFailed(String error) {
            progressBar.setVisibility(View.GONE);
            Utils.customToastMsgError(SearchFacilityBookingActivity.this, error, false);
        }
    };

    private void initPropertyVanueBooking(){
        textPropertySearchTv.setOnClickListener(v -> {
            isSelectProperty = true;
            for (AlertPropertyBookingModel item: propertySearchBookingList) {
                item.setClick(item.getPropertyId().equals(propertyId));
            }
            openAlertProperty(textPropertySearchTv, true);
        });

        textFacilitySearchTv.setOnClickListener(v -> {
            isSelectProperty = false;
            for (VenuePropertyBookingModel item: venueBookingList) {
                item.setClickTitle(item.getFacilityBasicId().equals(facilityId));
            }
            openAlertProperty(textFacilitySearchTv, false);
        });
    }

    private void openAlertProperty(TextView txtProperty, boolean isSelectProperty){
        easyDialog = new EasyDialog(this);
        @SuppressLint("InflateParams") View view = LayoutInflater.from(this).inflate(R.layout.layout_alert_property_facility_booking,null);
        EasyDialogClass.createFilterEasyDialog(easyDialog, this, view, txtProperty, false);
        RecyclerView recyclerView = view.findViewById(R.id.recyclerFB_TypeProperty);
        EditText searchPropertyEt = view.findViewById(R.id.searchEt);
        TextView noItemPropertyTv = view.findViewById(R.id.noItemPropertyTv);
        ProgressBar progressBarProperty = view.findViewById(R.id.progressItem);
        progressBarProperty.setVisibility(View.GONE);
        if (isSelectProperty){
            view.findViewById(R.id.headerTitleLayout).setVisibility(View.GONE);
            searchPropertyEt.setVisibility(View.VISIBLE);
        }

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        AlertTypePropertyFBAdapter alertTypePropertyFBAdapter;
        if (isSelectProperty){
            alertTypePropertyFBAdapter  = new AlertTypePropertyFBAdapter(this, propertySearchBookingList, itemClickBackListener);
        } else {
            alertTypePropertyFBAdapter  = new AlertTypePropertyFBAdapter(this, venueBookingList, true, itemClickBackListener);
        }
        recyclerView.setAdapter(alertTypePropertyFBAdapter);

        searchPropertyEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @SuppressLint("NotifyDataSetChanged")
            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().isEmpty()){
                    progressBarProperty.setVisibility(View.VISIBLE);
                    searchProperty(editable.toString(), recyclerView, noItemPropertyTv, progressBarProperty, alertTypePropertyFBAdapter);
                } else {
                    clearTextListProperty(recyclerView, noItemPropertyTv, alertTypePropertyFBAdapter);
                }
            }
        });
    }

    private void searchProperty(String keySearch, RecyclerView recyclerView, TextView noItemTv, ProgressBar progressBar, AlertTypePropertyFBAdapter alertTypePropertyFBAdapter){
        new FacilityBookingWs().getPropertyBooking(this, keySearch, new FacilityBookingWs.BookingPropertyCallBackListener() {
            @SuppressLint("NotifyDataSetChanged")
            @Override
            public void onLoadSuccessFull(List<AlertPropertyBookingModel> alertPropertyBookingModelList) {
                progressBar.setVisibility(View.GONE);
                propertySearchBookingList.clear();
                propertySearchBookingList.addAll(alertPropertyBookingModelList);

                alertTypePropertyFBAdapter.notifyDataSetChanged();
                recyclerView.setVisibility(propertySearchBookingList.size() > 0 ? View.VISIBLE : View.GONE);
                noItemTv.setVisibility(propertySearchBookingList.size() > 0 ? View.GONE : View.VISIBLE);
            }

            @Override
            public void onLoadFailed(String error) {
                progressBar.setVisibility(View.GONE);
                Utils.customToastMsgError(SearchFacilityBookingActivity.this, error, false);
            }
        });
    }

    @SuppressLint("NotifyDataSetChanged")
    private void clearTextListProperty(RecyclerView recyclerView, TextView noItemTv, AlertTypePropertyFBAdapter alertTypePropertyFBAdapter){
        propertySearchBookingList.clear();
        progressBar.setVisibility(View.GONE);
        noItemTv.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);

        propertySearchBookingList.addAll(propertyBookingList);
        alertTypePropertyFBAdapter.notifyDataSetChanged();
    }

    private final AlertTypePropertyFBAdapter.ItemClickBack itemClickBackListener = new AlertTypePropertyFBAdapter.ItemClickBack() {
        @SuppressLint("NotifyDataSetChanged")
        @Override
        public void onClickItemBack(AlertPropertyBookingModel alertPropertyBookingModel, VenuePropertyBookingModel venuePropertyBookingModel) {
            if (isSelectProperty) {
                if (alertPropertyBookingModel != null) {
                    propertyId = alertPropertyBookingModel.getPropertyId();
                    textPropertySearchTv.setText(alertPropertyBookingModel.getPropertyName());

                    progressBar.setVisibility(View.VISIBLE);
                    onRequestVenueBooking();

                    propertySearchBookingList.clear();
                    propertySearchBookingList.addAll(propertyBookingList);
                    for (AlertPropertyBookingModel item : propertySearchBookingList) {
                        item.setClick(item.getPropertyId().equals(alertPropertyBookingModel.getPropertyId()));
                    }
                }
            } else {
                if (venuePropertyBookingModel != null) {
                    facilityId = venuePropertyBookingModel.getFacilityBasicId();
                    textFacilitySearchTv.setText(venuePropertyBookingModel.getVenueName());

                    searchItemEdt.setText("");
                    clearTextList();
                }
            }
            easyDialog.dismiss();
        }
    };

    private  void onRequestVenueBooking(){
        progressBar.setVisibility(View.VISIBLE);
        new FacilityBookingWs().getVenueFacilityBooking(this,propertyId.equals("all") ? "all" : "property", propertyId, venuePropertyBookingCallBackListener);
    }

    private final FacilityBookingWs.VenuePropertyBookingCallBackListener venuePropertyBookingCallBackListener = new FacilityBookingWs.VenuePropertyBookingCallBackListener() {
        @SuppressLint("NotifyDataSetChanged")
        @Override
        public void onLoadSuccessFull(List<VenuePropertyBookingModel> listVenue) {
            progressBar.setVisibility(View.GONE);
            if (!venueBookingList.isEmpty())    venueBookingList.clear();

            VenuePropertyBookingModel venuePropertyBookingModel = new VenuePropertyBookingModel();
            venuePropertyBookingModel.setFacilityBasicId("all");
            venuePropertyBookingModel.setVenueName("All");
            venueBookingList.add(venuePropertyBookingModel);
            venueBookingList.addAll(listVenue);

            facilityId = venueBookingList.get(0).getFacilityBasicId();
            textFacilitySearchTv.setText(venueBookingList.get(0).getVenueName());

            for (VenuePropertyBookingModel item: venueBookingList) {
                item.setClickTitle(item.getFacilityBasicId().equals("all"));
            }

            searchItemEdt.setText("");
            clearTextList();
        }

        @Override
        public void onLoadFailed(String error) {
            progressBar.setVisibility(View.GONE);
            Utils.customToastMsgError(SearchFacilityBookingActivity.this, error, false);
        }
    };

    @SuppressLint("NotifyDataSetChanged")
    private void clearTextList(){
        btnClear.setVisibility(View.GONE);
        if (facilityBookingAdapter != null) facilityBookingAdapter.clear();
        progressBar.setVisibility(View.GONE);
        txtNoResultLayout.setVisibility(View.VISIBLE);
        recyclerViewSearch.setVisibility(View.GONE);
        facilityBookingAdapter.notifyDataSetChanged();
    }
}