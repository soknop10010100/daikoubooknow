package com.eazy.daikou.ui.home.data_record

import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import androidx.viewpager.widget.ViewPager
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.ui.home.book_now.booking_hotel.adapter.HotelHomeListViewPager
import com.eazy.daikou.ui.home.data_record.fragment.QuotationFragment
import com.google.android.material.tabs.TabLayout

class ListDataRecordActivity : BaseActivity() {

    private lateinit var viewPager : ViewPager
    private lateinit var tabLayout : TabLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_data_record)

        initView()

        initAction()
    }

    private fun  initView(){

        findViewById<ImageView>(R.id.iconBack).setOnClickListener( CustomSetOnClickViewListener{ finish()})
        findViewById<TextView>(R.id.titleToolbar).text = getString(R.string.data_record).uppercase()

        viewPager = findViewById(R.id.viewpager)
        tabLayout = findViewById(R.id.slidingTabs)
    }

    private fun  initAction(){

        listFragment()
    }

    private fun listFragment(){
        val adapter = HotelHomeListViewPager(supportFragmentManager).also {
            // add fragment to the list
            it.addFragment(QuotationFragment.newInstance("opportunity"), getString(R.string.opportunity))
            it.addFragment(QuotationFragment.newInstance("quotation"), getString(R.string.quotation))
            it.addFragment(QuotationFragment.newInstance("hold_reserved"), getString(R.string.hold_and_reserved))
            it.addFragment(QuotationFragment.newInstance("booking"), getString(R.string.booking))
            it.addFragment(QuotationFragment.newInstance("sale_agreement"), getString(R.string.sale_agreement))
        }
        viewPager.adapter = adapter
        tabLayout.setupWithViewPager(viewPager)
    }

}