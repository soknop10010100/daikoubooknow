package com.eazy.daikou.ui.home.service_provider_new.adapter_service_provider

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R

class ListServiceProviderAdapter(private val  context: Context, private val callBackItem: ClickItemCallBackListener): RecyclerView.Adapter<ListServiceProviderAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.custom_layout_list_service_provider, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.noTv.text = "0012236"
        holder.typeTv.text = "Product"
        holder.dateTv.text = "19/09/2022"
        holder.companyNameTv.text = "Black Cat"
        holder.createDateTv.text = "19/09/2022"
        holder.timeTv.text = "2:30 pm"
        holder.statusTv.text = "new"

        holder.itemView.setOnClickListener { callBackItem.clickItemListener()  }
    }

    override fun getItemCount(): Int {
       return 20
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        var style : TextView = view.findViewById(R.id.style)
        var statusTv : TextView = view.findViewById(R.id.statusTv)
        var noTv : TextView = view.findViewById(R.id.noTv)
        var companyNameTv : TextView = view.findViewById(R.id.companyNameTv)
        var createDateTv : TextView = view.findViewById(R.id.createDateTv)
        var typeTv : TextView = view.findViewById(R.id.typeTv)
        var dateTv : TextView = view.findViewById(R.id.dateTv)
        var timeTv : TextView = view.findViewById(R.id.timeTv)
    }

    interface ClickItemCallBackListener{
        fun clickItemListener()
    }
}