package com.eazy.daikou.ui.home.parking;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.eazy.daikou.R;
import com.eazy.daikou.repository_ws.Constant;
import com.eazy.daikou.base.BaseActivity;
import com.eazy.daikou.request_data.request.parking_ws.ParkingWs;
import com.eazy.daikou.request_data.request.parking_ws.PrinterWs;
import com.eazy.daikou.request_data.static_data.StaticUtilsKey;
import com.eazy.daikou.helper.UserSessionManagement;
import com.eazy.daikou.helper.Utils;
import com.eazy.daikou.model.my_unit_info.MyUnitScanInfoModel;
import com.eazy.daikou.model.parking.CheckInOutPrinter;
import com.eazy.daikou.model.parking.Parking;
import com.eazy.daikou.model.profile.User;
import com.eazy.daikou.ui.MainActivity;
import com.eazy.daikou.ui.home.parking.adapter.ParkingAdapter;
import com.eazy.daikou.ui.home.my_property_v1.EasyDialogClass;
import com.eazy.daikou.ui.profile.ChangeLanguageFragment;
import com.google.gson.Gson;
import com.michael.easydialog.EasyDialog;

import java.util.ArrayList;
import java.util.HashMap;

public class ParkingListActivity extends BaseActivity {

    private RecyclerView recyclerView;
    private SwipeRefreshLayout refreshLayout;
    private LinearLayoutManager linearLayoutManager;
    private ProgressBar progressBar;
    private User mUser;
    private ParkingAdapter parkingAdapter;
    private TextView titleToolbar, btnAllParking, btnMyParking, noItemTv;
    private ImageView scanBtn, qrCodeParking,btnPrint ;
    private CardView cardMenuTap;
    private EasyDialog easyDialog;

    private final ArrayList<Parking> parkingArrayList = new ArrayList<>();

    private int currentItem, total, scrollDown, currentPage = 1, size = 10;
    private boolean isScrolling;
    private String userType = "", propertyId = "", userId = "", orderId = "", type;
    private String notification = "", action = "", resultScan = "", messageReport = "", message = "";
    private final int CODE_RELOAD_DATA = 348;
    private final int REQUEST_SCANNER = 231;
    private final int REQUEST_SCANNER_PRINT = 232;
    private final int CODE_DETAIL = 321;
    private MyUnitScanInfoModel myUnitScanInfoModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parking);

        initView();

        initData();

        initAction();
    }

    private void initView() {
        btnAllParking = findViewById(R.id.list_all_parking);
        btnMyParking = findViewById(R.id.my_parking);
        cardMenuTap = findViewById(R.id.card_tap);
        btnPrint = findViewById(R.id.ic_printer);

        recyclerView = findViewById(R.id.listParkingRecycle);
        noItemTv = findViewById(R.id.noItemTv);
        progressBar = findViewById(R.id.progressItem);
        refreshLayout = findViewById(R.id.swipe_layouts);
        titleToolbar = findViewById(R.id.titleToolbar);
        scanBtn = findViewById(R.id.rightIcon);
        scanBtn.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.white)));
        qrCodeParking = findViewById(R.id.qr_code_parking);
        progressBar.setVisibility(View.GONE);
    }

    private void initData() {
        UserSessionManagement userSessionManagement = new UserSessionManagement(this);
        mUser = new Gson().fromJson(userSessionManagement.getUserDetail(), User.class);
        userId = userSessionManagement.getUserId();

        if (getIntent() != null && getIntent().hasExtra("action")){
            action = getIntent().getStringExtra("action");
            resultScan =  getIntent().getStringExtra("result_scan_parking");
            myUnitScanInfoModel = (MyUnitScanInfoModel) getIntent().getSerializableExtra("my_unit_info");
        }

        // check user type
        if (mUser.getActiveUserType().equalsIgnoreCase("employee")||mUser.getActiveUserType().equals("developer") || mUser.getActiveUserType().equals("admin")) {
            userType = "employee";
            type = "all";
            cardMenuTap.setVisibility(View.VISIBLE);
            btnPrint.setVisibility(View.GONE);
        } else {
            userType = "car_owner";
            type = "my_list";
            cardMenuTap.setVisibility(View.GONE);
        }

        showScanButton(true);

        if (getIntent().hasExtra("order_id")) {
            orderId = getIntent().getStringExtra("order_id");
        }
        if (getIntent().hasExtra("type")) {
            userType = getIntent().getStringExtra("type");
        }
        if (getIntent().hasExtra("notification")) {
            notification = getIntent().getStringExtra("notification");
        }
        if (getIntent().hasExtra("body")) {
            message = getIntent().getStringExtra("body");
        }
        if(getIntent().hasExtra("parking_report")){
            messageReport = getIntent().getStringExtra("parking_report");
        }

        propertyId = mUser.getActivePropertyIdFk();
        titleToolbar.setText(getResources().getString(R.string.parking).toUpperCase());

        linearLayoutManager = new LinearLayoutManager(ParkingListActivity.this);
        recyclerView.setLayoutManager(linearLayoutManager);

        actionClick(type);

        //refresh get new data
        refreshLayout.setColorSchemeResources(R.color.colorPrimary);
        refreshLayout.setOnRefreshListener(() -> {
            currentPage = 1;
            size = 10;
            parkingAdapter.clear();
            getListParking(userType);
        });

        onScrollListParking();
    }

    private void initAction() {
        findViewById(R.id.iconBack).setOnClickListener(view -> onBackFinish());

        // Action on Tap
        btnAllParking.setOnClickListener(view -> {
            clearData();
            actionClick("all");
        });

        btnMyParking.setOnClickListener(view -> {
            clearData();
            actionClick("my_list");
        });

        // btn scan
        scanBtn.setOnClickListener(view -> {
            Intent intent = new Intent(ParkingListActivity.this, ScanParkActivity.class);
            intent.putExtra("type_scan","have_app");
            startActivityForResult(intent, REQUEST_SCANNER);
        });

        btnPrint.setOnClickListener(view -> openSpinner(btnPrint));

        qrCodeParking.setOnClickListener(v -> {
            String userName = "";
            if (mUser.getName() != null){
                userName = mUser.getName();
            }
            if (Utils.getString(Constant.FIRST_SCAN_QR, ParkingListActivity.this).equalsIgnoreCase("true")) {
                Utils.requestQrCodeToServer(ParkingListActivity.this, userId, userName);
            }
            ChangeLanguageFragment changeLanguageAlert = ChangeLanguageFragment.newInstance("qr_code");
            changeLanguageAlert.show(getSupportFragmentManager(), changeLanguageAlert.getClass().getSimpleName());
        });
    }

    private void actionClick(String types) {
        if (types.equalsIgnoreCase("all")) {
            btnAllParking.setTextColor(ContextCompat.getColor(ParkingListActivity.this, R.color.white));
            btnAllParking.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.custom_card_view_color_menu, null));
            btnMyParking.setTextColor(Color.GRAY);
            btnMyParking.setBackground(null);
            userType = "employee";
            type = "all";

            getListParking(userType);
        } else if (types.equalsIgnoreCase("my_list")) {
            btnMyParking.setTextColor(ContextCompat.getColor(ParkingListActivity.this, R.color.white));
            btnMyParking.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.custom_card_view_color_menu, null));
            btnAllParking.setTextColor(Color.GRAY);
            btnAllParking.setBackground(null);
            userType = "car_owner";
            type = "my_list";

            getListParking(userType);
        }
    }

    private void  clearData(){
        currentPage = 1;
        size = 10;
        if(parkingArrayList.size() > 0){
            parkingAdapter.clear();
        }
        parkingArrayList.clear();
        noItemTv.setVisibility(View.GONE);
    }

    private void onScrollListParking() {
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true;
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                currentItem = linearLayoutManager.getChildCount();
                total = linearLayoutManager.getItemCount();
                scrollDown = linearLayoutManager.findFirstVisibleItemPosition();
                if (dy > 0) {
                    if (isScrolling && (currentItem + scrollDown == total)) {
                        if (total == size) {
                            isScrolling = false;
                            currentPage++;
                            progressBar.setVisibility(View.VISIBLE);
                            recyclerView.scrollToPosition(parkingArrayList.size() - 1);
                            size += 10;

                            getListParking(userType);
                        } else {
                            progressBar.setVisibility(View.GONE);
                        }
                    }
                }

            }
        });
    }

    private void getListParking(String userType) {
        progressBar.setVisibility(View.VISIBLE);
        listParking(userType);

        parkingAdapter = new ParkingAdapter(parkingArrayList, itemClick);
        recyclerView.setAdapter(parkingAdapter);
    }

    private void listParking(String userType){
        new ParkingWs().getListParking(this, currentPage, 10, userId, propertyId, userType, requestCallBack);
    }

    private final ParkingWs.OnRequestCallBackListener requestCallBack = new ParkingWs.OnRequestCallBackListener() {
        @SuppressLint("NotifyDataSetChanged")
        @Override
        public void onLoadSuccess(ArrayList<Parking> listParking) {
            progressBar.setVisibility(View.GONE);
            refreshLayout.setRefreshing(false);

            parkingArrayList.addAll(listParking);
            parkingAdapter.notifyDataSetChanged();

            for (Parking parking : parkingArrayList) {
                if (parking.getId().equals(orderId)) {
                    if (notification.equals("notification_parking")) {
                        Intent intent = new Intent(ParkingListActivity.this, ParkingDetailActivity.class);
                        intent.putExtra("order_id", orderId);
                        intent.putExtra("type", userType);
                        intent.putExtra("userId", userId);
                        intent.putExtra("message",message);
                        intent.putExtra("parking_report", messageReport);
                        intent.putExtra("parking", parking);
                        intent.putExtra("notification", notification);
                        startActivityForResult(intent, CODE_DETAIL);
                        notification = "";
                        message = "";
                    }
                }
            }

            noItemTv.setVisibility(parkingArrayList.isEmpty() ? View.VISIBLE : View.GONE);

            //Scan From Home Menu Scan QR Code
            if (!resultScan.equals("")){
                progressBar.setVisibility(View.VISIBLE);
                String doActionScan = myUnitScanInfoModel.getAction() != null ? myUnitScanInfoModel.getAction() : "";
                String parkingId = null;
                if (myUnitScanInfoModel.getParking() != null)
                    if (myUnitScanInfoModel.getParking().getParking_id() != null){
                        parkingId = myUnitScanInfoModel.getParking().getParking_id();
                    }
                goToDoAction(doActionScan, resultScan, parkingId);
                resultScan = "";
            }

        }

        @Override
        public void onLoadFail(String message) {
            refreshLayout.setRefreshing(false);
            progressBar.setVisibility(View.GONE);
            Utils.customToastMsgError(ParkingListActivity.this, message, false);
        }
    };

    private void goToDoAction(String action, String qrCode, String parkingId){
        switch (action) {
            case "check_in":
                checkInService(qrCode);
                break;
            case "add_info": {
                if (parkingId == null) return;
                startActivityToDetail(qrCode, parkingId, "add_info", "1");
            }
                break;
            case "check_out": {
                if (parkingId == null) return;
                startActivityToDetail(qrCode, parkingId, "check_out", "1");
                break;
            }
            case "go_detail": {
                if (parkingId == null) return;
                startActivityToDetail(qrCode, parkingId, "go_detail", "1");
                break;
            }
            case "qr_code_invalid":
                Utils.customToastMsgError(ParkingListActivity.this, getResources().getString(R.string.not_available), false);
                break;
            default:
                Utils.customToastMsgError(ParkingListActivity.this, getResources().getString(R.string.invalid_qr_code), false);
                break;
        }
    }

    private final ParkingAdapter.OnClickCallBackListener itemClick = parkingModel -> {
        startActivityToDetail("",parkingModel.getId(),"add_info",parkingModel.getParking_with_machine());
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data!=null && resultCode == RESULT_OK) {
            if (requestCode == REQUEST_SCANNER) {
                if (data.hasExtra("result_scan")) {
                    progressBar.setVisibility(View.VISIBLE);
                    String getResultScan = data.getStringExtra("result_scan");
                    // doResultScanned(getResultScan);
                    checkInService(getResultScan);
                }
                progressBar.setVisibility(View.GONE);
            }else if(requestCode == REQUEST_SCANNER_PRINT){
                if (data.hasExtra("result_scan")) {
                    progressBar.setVisibility(View.VISIBLE);
                    String qrScan = data.getStringExtra("result_scan");
                    getParkingPrinter(qrScan);
                    progressBar.setVisibility(View.GONE);
                }
            }
            else if (requestCode == CODE_DETAIL || requestCode == CODE_RELOAD_DATA) {
                currentPage = 1;
                size = 10;
                parkingAdapter.clear();
                getListParking(userType);
                progressBar.setVisibility(View.GONE);
            }
        }
    }

    private void doResultScanned(String getResultScan){
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("scanqr_code_info", getResultScan);
        hashMap.put("user_id", userId);
        parkingAction(hashMap, getResultScan);
    }

    private void showScanButton(boolean isShow) {
        scanBtn.setImageResource(R.drawable.ic_baseline_qr_code_scanner_24);
        scanBtn.setVisibility(isShow ? View.VISIBLE : View.GONE);
        qrCodeParking.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        showScanButton(false);
    }

    private void checkInService(String qrCode){
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("scanqr_code_info", qrCode);
        hashMap.put("user_id", userId);
        createParking(hashMap);
    }

    private void parkingAction(HashMap<String, Object> hashMap, String qrCode) {
        new ParkingWs().createServiceParkingV2(this, hashMap, new ParkingWs.OnRequestCreateCallBackListener() {
            @Override
            public void onLoadSuccess(String listParking) {
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onLoadSuccessListener(String action, String parkingId) {
                progressBar.setVisibility(View.GONE);
                switch (action) {
                    case "scan_check_in":
                    case "check_in":
                        checkInService(qrCode);
                        break;
                    case "scan_add_info":
                        startActivityToDetail(qrCode, parkingId, "add_info","1");
                        break;
                    case "scan_check_out":
                        startActivityToDetail(qrCode, parkingId, "check_out","1");
                        break;
                    case "go_detail":
                        startActivityToDetail(qrCode, parkingId, "go_detail","1");
                        break;
                    default:
                        Toast.makeText(ParkingListActivity.this, action, Toast.LENGTH_SHORT).show();
                        break;
                }

            }

            @Override
            public void onLoadFail(String message) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(ParkingListActivity.this, message, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void startActivityToDetail(String qrCode,String parkingId, String type,String mazin){
        Intent intent = new Intent(ParkingListActivity.this, ParkingDetailActivity.class);
        intent.putExtra("order_id", parkingId);
        intent.putExtra("type", userType);
        intent.putExtra("userId", userId);
        intent.putExtra("parking_with_machine", mazin);
        intent.putExtra("qrcode", qrCode);
        intent.putExtra("action", type);
        startActivityForResult(intent, CODE_DETAIL);
    }

    private void createParking(HashMap<String, Object> hashMap) {
        new ParkingWs().createServiceParking(this, hashMap, new ParkingWs.OnRequestCreateCallBackListener() {
            @Override
            public void onLoadSuccess(String listParking) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(ParkingListActivity.this, listParking, Toast.LENGTH_LONG).show();
                currentPage = 1;
                size = 10;

                if (parkingAdapter != null) parkingAdapter.clear();
                getListParking(userType);
            }

            @Override
            public void onLoadSuccessListener(String action, String parkingId) { }

            @Override
            public void onLoadFail(String message) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(ParkingListActivity.this, message, Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        onBackFinish();
    }

    private void onBackFinish(){
        progressBar.setVisibility(View.GONE);
        if (action.equals(StaticUtilsKey.action_key_scanner)){
            Intent intent = new Intent(ParkingListActivity.this, MainActivity.class);
            startActivity(intent);
            finishAffinity();
        } else{
            finish();
        }
    }

    private void getParkingPrinter(String qrcode){
        HashMap<String,Object> hashMap = new HashMap<>();
        hashMap.put("user_id",userId);
        hashMap.put("print_parking_type","scanqr_code");
        hashMap.put("scanqr_code_info",qrcode);
        new PrinterWs().printerCheckIn(this, hashMap, new PrinterWs.OnRequestPrinterCallBack() {
            @Override
            public void onLoadSuccess(CheckInOutPrinter checkInOutPrinter) {
                progressBar.setVisibility(View.GONE);
                Intent intent;
                if(checkInOutPrinter.getScanAction().equals("check_in")){
                    intent = new Intent(ParkingListActivity.this, PrinterActivity.class);
                    intent.putExtra("type","have_qr");
                    intent.putExtra("parkingPrinter",checkInOutPrinter);
                }else {
                    intent = new Intent(ParkingListActivity.this, PrintLeftParkingActivity.class);
                    intent.putExtra("checkOutPrinter",checkInOutPrinter);
                    intent.putExtra("user_id",userId);
                    intent.putExtra("user_id",userId);
                    intent.putExtra("qr_code",qrcode);
                    intent.putExtra("propertyId",propertyId);
                }
                startActivityForResult(intent,CODE_RELOAD_DATA);

            }

            @Override
            public void onLoadFail(String message) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(ParkingListActivity.this, message, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void openSpinner(ImageView linearLayout){
        easyDialog = new EasyDialog(this);
        View layout = LayoutInflater.from(this).inflate(R.layout.layout_action_print_parking, null);
        EasyDialogClass.createFilterEasyDialog(easyDialog, this,layout,linearLayout, false);
        TextView btnTakePhoto = layout.findViewById(R.id.take_photo_print);
        TextView btnScan = layout.findViewById(R.id.scan_qr_print);
        btnTakePhoto.setOnClickListener(view -> {
            easyDialog.dismiss();
            Intent intent = new Intent(ParkingListActivity.this,PrinterActivity.class);
            intent.putExtra("type","no_qr");
            intent.putExtra("user_id",userId);
            intent.putExtra("propertyId",propertyId);
            startActivity(intent);

        });

        btnScan.setOnClickListener(view -> {
            easyDialog.dismiss();
            Intent intent = new Intent(ParkingListActivity.this, ScanParkActivity.class);
            intent.putExtra("type_scan","no_app");
            startActivityForResult(intent, REQUEST_SCANNER_PRINT);
        });

    }

}