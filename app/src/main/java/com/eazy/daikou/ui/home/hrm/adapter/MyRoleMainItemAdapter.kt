package com.eazy.daikou.ui.home.hrm.adapter

import android.annotation.SuppressLint
import android.content.res.ColorStateList
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.home.HomeViewModel

class MyRoleMainItemAdapter(private val itemList : List<HomeViewModel>, private val onCallBackListener : HomeCallBack) : RecyclerView.Adapter<MyRoleMainItemAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.my_role_main_model, parent, false))
    }

    @SuppressLint("ResourceType")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val homeViewModel = itemList[position]
        if (homeViewModel != null) {
            holder.icon.setImageDrawable(homeViewModel.drawable)
            holder.title.text = homeViewModel.name
            holder.descriptionTv.text = homeViewModel.category_item
            Utils.setBgTint(holder.cardIcon, homeViewModel.color)

            holder.cardLayout.setOnClickListener(
                CustomSetOnClickViewListener{
                    onCallBackListener.clickIconListener(homeViewModel)
                }
            )
            if (homeViewModel.action.contains("hotel_")){
                Utils.setBgTint(holder.cardLayout, R.color.white)
                holder.title.setTextColor(Color.BLACK)
            } else {
                holder.cardLayout.background = Utils.setDrawable(holder.cardLayout.context, R.drawable.custom_card_item_gradient)
                holder.cardLayout.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(holder.cardLayout.context, R.color.white))
            }
        }
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var icon: ImageView = itemView.findViewById(R.id.imgProfile)
        var title: TextView = itemView.findViewById(R.id.name_item_hr)
        var descriptionTv: TextView = itemView.findViewById(R.id.descriptionTv)
        var cardLayout : CardView = itemView.findViewById(R.id.card_hr)
        var cardIcon : CardView = itemView.findViewById(R.id.cardIcon)
    }

    interface HomeCallBack {
        fun clickIconListener(item: HomeViewModel)
    }

}