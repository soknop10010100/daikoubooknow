package com.eazy.daikou.ui.home.complaint_solution.complaint

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.RadioGroup
import android.widget.TextView
import androidx.activity.result.ActivityResult
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.base.BaseCameraActivity
import com.eazy.daikou.request_data.request.complain_ws.ComplaintListWS
import com.eazy.daikou.helper.GetDataUtils
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.profile.PropertyItemModel
import com.eazy.daikou.model.profile.UnitNoModel
import com.eazy.daikou.model.work_order.ImageListModel
import com.eazy.daikou.ui.home.complaint_solution.complaint.adapter.AdapterItemListComplaint
import com.eazy.daikou.ui.home.complaint_solution.complaint.fragment.SelectAccountAndUnitNoBottomSheet
import com.eazy.daikou.ui.home.inspection_work_order.adapter.ImageListWorkOrderAdapter
import com.google.android.material.textfield.TextInputEditText
import java.util.ArrayList
import java.util.HashMap

class ComplaintSolutionCreateActivity : BaseActivity() {

    private lateinit var progressBar: ProgressBar
    private var bitmapList: ArrayList<ImageListModel> = ArrayList()
    private lateinit var subjectEd : TextInputEditText
    private lateinit var descriptionEd : TextInputEditText

    // Add Image
    private lateinit var listImageRecyclerView : RecyclerView
    private lateinit var btnAddImg : LinearLayout
    private var accountId = ""
    private var unitId = ""
    private lateinit var propertyTv : TextView

    private lateinit var unitNoTv : TextView
    private var action : String = ""
    private var isUnitCreated = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_complaint_solution_create)

        initView()

        initData()

        initAction()

    }

    private fun initView() {
        Utils.customOnToolbar(this, resources.getString(R.string.create_complaint)){ finish() }

        progressBar = findViewById(R.id.progressItem)
        progressBar.visibility = View.GONE
        subjectEd = findViewById(R.id.subjectEd)
        descriptionEd = findViewById(R.id.descriptionEd)
        listImageRecyclerView = findViewById(R.id.listImageRecyclerView)
        btnAddImg = findViewById(R.id.btnAddImg)
        propertyTv = findViewById(R.id.propertyTv)
        unitNoTv = findViewById(R.id.unitNoTv)
    }

    private fun initData() {

        action = GetDataUtils.getDataFromString("action", this)

        accountId = GetDataUtils.getDataFromString("account_id", this)

        val accountName = GetDataUtils.getDataFromString("account_name", this)
        propertyTv.text = accountName
    }

    private fun initAction() {

        // Action add images
        btnAddImg.setOnClickListener(CustomSetOnClickViewListener {
            val intent = Intent(this, BaseCameraActivity::class.java)
            activityLauncher.launch(intent) { result: ActivityResult ->
                if (result.resultCode == Activity.RESULT_OK) {
                    val data = result.data ?: return@launch

                    if (data.hasExtra("image_path")) {
                        val imagePath = data.getStringExtra("image_path")
                        setImageRecyclerView("", imagePath!!)
                    }
                    if (data.hasExtra("select_image")) {
                        val imagePath = data.getStringExtra("select_image")
                        setImageRecyclerView("", imagePath!!)
                    }
                }
            }
        })

        // Submit Complaint
        findViewById<LinearLayout>(R.id.btnCreate).setOnClickListener( CustomSetOnClickViewListener { requestServiceWs() })

        // Select Property
        propertyTv.setOnClickListener ( CustomSetOnClickViewListener { bottomSheetCategoryService("property") })

        unitNoTv.setOnClickListener( CustomSetOnClickViewListener { bottomSheetCategoryService("unit_no") } )

        findViewById<RadioGroup>(R.id.rGroup).setOnCheckedChangeListener { _, checkedId ->
            if (checkedId == R.id.rUnit) {
                isUnitCreated = true
            } else if (checkedId == R.id.rGeneral) {
                isUnitCreated = false
            }
            findViewById<LinearLayout>(R.id.unitNoLayout).visibility = if (isUnitCreated)   View.VISIBLE else View.GONE
        }
    }

    private fun bottomSheetCategoryService(actionType : String){
        val bottomSheet = SelectAccountAndUnitNoBottomSheet.newInstance(actionType, accountId)
        bottomSheet.initDataListener(callBackItemClickFormBottomSheet)
        bottomSheet.show(supportFragmentManager,"fragment")
    }

    private val callBackItemClickFormBottomSheet : AdapterItemListComplaint.CallBackItemClickListener = object : AdapterItemListComplaint.CallBackItemClickListener {
        override fun onClickAccount(itemComplaint: PropertyItemModel) {
            propertyTv.text = itemComplaint.name
            accountId = itemComplaint.id.toString()

            if (isUnitCreated){
                unitId = ""
                unitNoTv.text = ""
            }
        }

        override fun onClickUnit(myUnit: UnitNoModel) {
            unitNoTv.text = myUnit.name
            unitId = myUnit.id.toString()
        }
    }

    private fun setImageRecyclerView(posKeyDefine: String, imagePath: String) {
        val loadedBitmap = BaseCameraActivity.convertByteToBitmap(imagePath)
        bitmapList.add(ImageListModel(posKeyDefine, loadedBitmap))

        // Set image layout
        setImageRecyclerView()
    }

    private fun setImageRecyclerView() {
        ImageListWorkOrderAdapter.setImageRecyclerView(listImageRecyclerView, btnAddImg, this, bitmapList, 0, object : ImageListWorkOrderAdapter.OnClearImage {
            override fun onClickRemove(bitmap: ImageListModel, post: Int) {
                bitmapList.remove(bitmap)

                setImageRecyclerView()
            }

            override fun onViewImage(bitmap: ImageListModel) {
                if (bitmap.bitmapImage != null) {
                    Utils.openImageOnDialog(this@ComplaintSolutionCreateActivity, Utils.convert(bitmap.bitmapImage))
                }
            }

        })
    }

    private fun requestServiceWs() {
        if (accountId == "") {
            Utils.customToastMsgError(this, resources.getString(R.string.please_input_property_name), false)
            return
        } else if (isUnitCreated && unitId == ""){
            Utils.customToastMsgError(this, resources.getString(R.string.please_select_unit_no), false)
            return
        }

        val hashMap = HashMap<String, Any>()
        hashMap["object_type"] = if (isUnitCreated) "unit" else "account"
        hashMap["object_id"] = if (isUnitCreated) unitId else accountId
        hashMap["post_type"] = "complaint"
        hashMap["account_id"] = accountId
        if (unitId != "")   hashMap["unit_id"] = unitId

        hashMap["subject"] = subjectEd.text.toString()
        hashMap["description"] = descriptionEd.text.toString()

        if (bitmapList.size > 0){
            val imageList : ArrayList<String> = ArrayList()
            for (item in bitmapList){
                imageList.add(Utils.convert(item.bitmapImage))
            }
            hashMap["file_type"] = "image"
            hashMap["file_items"] = imageList
        }

        progressBar.visibility = View.VISIBLE
        ComplaintListWS.createComplaintWS(this, hashMap, object : ComplaintListWS.CallBackCreateComplaint{
            override fun onSuccessFull(message: String) {
                progressBar.visibility = View.GONE
                Utils.customToastMsgError(this@ComplaintSolutionCreateActivity, message, true)

                setResult(RESULT_OK)
                finish()
            }

            override fun onFailed(mes: String) {
                progressBar.visibility = View.GONE
                Utils.customToastMsgError(this@ComplaintSolutionCreateActivity, mes, false)
            }

        })
    }

}