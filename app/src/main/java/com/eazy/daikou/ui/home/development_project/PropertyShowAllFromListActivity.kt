package com.eazy.daikou.ui.home.development_project

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.request_data.request.project_development_ws.ProjectDevelopmentWs
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.home_page.home_project.AllBranches
import com.eazy.daikou.model.home_page.home_project.BranchTypes
import com.eazy.daikou.model.home_page.home_project.Featured
import com.eazy.daikou.model.home_page.home_project.Nearby
import com.eazy.daikou.ui.home.development_project.adapter.RecommendItemPropertyAdapter
import com.eazy.daikou.ui.home.development_project.adapter.PropertySeeMoreFeatureAdapter
import com.eazy.daikou.ui.home.development_project.adapter.PropertySeeMoreNearbyAdapter

class PropertyShowAllFromListActivity : BaseActivity() {

    private lateinit var progress: View
    private lateinit var recyclerViewProperty: RecyclerView
    private lateinit var swipeRefreshLayout: SwipeRefreshLayout

    private var type = ""
    private var currentItem = 0
    private var total : Int = 0
    private var scrollDown : Int = 0
    private var currentPage : Int = 1
    private var size : Int = 10
    private var isScrolling = false

    private var listNearbyProperty = ArrayList<Nearby>()
    private var listFeaturedProperty = ArrayList<Featured>()
    private var listRecommendProperty = ArrayList<AllBranches>()

    private lateinit var layoutManager: LinearLayoutManager
    private lateinit var adapterNearby: PropertySeeMoreNearbyAdapter
    private lateinit var adapterRecommend: RecommendItemPropertyAdapter
    private lateinit var adapterFeatured: PropertySeeMoreFeatureAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_property_item_recommend)

        initView()

        initDatta()

        initAction()

    }

    //get key from activity
    private fun initDatta() {
        if (intent.hasExtra("type")) {
            type = intent.getStringExtra("type")!!
        }
    }

    // init view
    private fun initView() {
        recyclerViewProperty = findViewById(R.id.recyclerView)

        progress = findViewById(R.id.progress)
        swipeRefreshLayout = findViewById(R.id.swipe_layouts)
    }

    //init action
    private fun initAction() {
        Utils.customOnToolbar(this, type) {finish()}

        swipeRefreshLayout.setColorSchemeResources(R.color.appBarColor)
        swipeRefreshLayout.setOnRefreshListener { refreshList() }

        layoutManager = LinearLayoutManager(this)
        recyclerViewProperty.layoutManager = layoutManager

        when (type) {
            getString(R.string.near_by_property) -> {
                adapterNearby = PropertySeeMoreNearbyAdapter(
                    listNearbyProperty,
                    onClickNearby
                )
                recyclerViewProperty.adapter = adapterNearby
            }
            getString(R.string.featuring) -> {
                adapterFeatured = PropertySeeMoreFeatureAdapter(
                    listFeaturedProperty,
                    clickFeatured
                )
                recyclerViewProperty.adapter = adapterFeatured
            }
            else -> {
                adapterRecommend = RecommendItemPropertyAdapter(
                    listRecommendProperty,
                    clickRecommend
                )
                recyclerViewProperty.adapter = adapterRecommend

            }
        }

        requestServiceWs()

        onScrollItemRecycle()

    }

    private fun refreshList() {
        currentPage = 1
        size = 10
        isScrolling = true

        when (type) {
            getString(R.string.near_by_property) -> {
                adapterNearby.clear()
            }
            getString(R.string.featuring) -> {
                adapterFeatured.clear()
            }
            else -> {
                adapterRecommend.clear()
            }
        }

        requestServiceWs()
    }

    // onclick nearby
    private val onClickNearby = object : PropertySeeMoreNearbyAdapter.OnClickProperty {
        override fun click(property: Nearby) {
            val intent = Intent(this@PropertyShowAllFromListActivity, PropertiesItemDetailActivity::class.java)
            intent.putExtra("id", property.id)
            startActivity(intent)
        }
    }

    // list near by
    private val resultNearbyListener = object : ProjectDevelopmentWs.CallBackGetNearBy {
        override fun onResponeListImage(listImageSlider: ArrayList<String>) {}

        override fun onResponeListCategory(listCategory: ArrayList<BranchTypes>) {}

        @SuppressLint("NotifyDataSetChanged")
        override fun onResponeListNearBy(listNearBy: ArrayList<Nearby>) {
            showProgress(false)
            swipeRefreshLayout.isRefreshing = false
            listNearbyProperty.addAll(listNearBy)
            adapterNearby.notifyDataSetChanged()

            Utils.validateViewNoItemFound(this@PropertyShowAllFromListActivity, recyclerViewProperty, listNearbyProperty.size <= 0)
        }

        override fun onResponeListFeatured(listFeatured: ArrayList<Featured>) {}

        override fun onResponeAllListProperty(listAllProperty: ArrayList<AllBranches>) {}

        override fun onLoadingFailed(msg: String?) {
            showProgress(false)
            Utils.customToastMsgError(this@PropertyShowAllFromListActivity, msg, false)
        }

        override fun onGettingItemFailed(msg: String?) {
            showProgress(false)
            swipeRefreshLayout.isRefreshing = false
            Utils.customToastMsgError(this@PropertyShowAllFromListActivity, msg, false)
        }

    }

    //click on feature
    private val clickFeatured = object : PropertySeeMoreFeatureAdapter.OnClickProperty {
        override fun click(property: Featured) {
            if (property.id == null) return
            startDetailActivity(property.id!!)
        }
    }

    private fun startDetailActivity(id : String){
        val intent = Intent(this@PropertyShowAllFromListActivity, PropertiesItemDetailActivity::class.java).apply {
            putExtra("id", id)
        }
        startActivity(intent)
    }

    private val resultFeaturedListener = object : ProjectDevelopmentWs.CallBackGetNearBy {
        override fun onResponeListImage(listImageSlider: ArrayList<String>) {}

        override fun onResponeListCategory(listCategory: ArrayList<BranchTypes>) {}

        override fun onResponeListNearBy(listNearBy: ArrayList<Nearby>) {}

        @SuppressLint("NotifyDataSetChanged")
        override fun onResponeListFeatured(listFeatured: ArrayList<Featured>) {
            showProgress(false)
            swipeRefreshLayout.isRefreshing = false
            listFeaturedProperty.addAll(listFeatured)
            adapterFeatured.notifyDataSetChanged()

            Utils.validateViewNoItemFound(this@PropertyShowAllFromListActivity, recyclerViewProperty, listFeaturedProperty.size <= 0)
        }

        override fun onResponeAllListProperty(listAllProperty: ArrayList<AllBranches>) {}

        override fun onLoadingFailed(msg: String?) {
            showProgress(false)
            swipeRefreshLayout.isRefreshing = false
            Utils.customToastMsgError(this@PropertyShowAllFromListActivity, msg, false)
        }

        override fun onGettingItemFailed(msg: String?) {
            showProgress(false)
            swipeRefreshLayout.isRefreshing = false
            Utils.customToastMsgError(this@PropertyShowAllFromListActivity, msg, false)
        }

    }


    //click on recommend
    private val clickRecommend = object : RecommendItemPropertyAdapter.OnClickProperty {
        override fun click(property: AllBranches) {
            if (property.id == null) return
            startDetailActivity(property.id!!)
        }

    }

    //list recommend
    private val resultRecommendListener = object : ProjectDevelopmentWs.CallBackGetNearBy {
        override fun onResponeListImage(listImageSlider: ArrayList<String>) {}

        override fun onResponeListCategory(listCategory: ArrayList<BranchTypes>) {}

        override fun onResponeListNearBy(listNearBy: ArrayList<Nearby>) {}

        override fun onResponeListFeatured(listFeatured: ArrayList<Featured>) {}

        @SuppressLint("NotifyDataSetChanged")
        override fun onResponeAllListProperty(listAllProperty: ArrayList<AllBranches>) {
            showProgress(false)
            swipeRefreshLayout.isRefreshing = false
            listRecommendProperty.addAll(listAllProperty)
            adapterRecommend.notifyDataSetChanged()

            Utils.validateViewNoItemFound(this@PropertyShowAllFromListActivity, recyclerViewProperty,listRecommendProperty.size <= 0)
        }

        override fun onLoadingFailed(msg: String?) {
            showProgress(false)
            swipeRefreshLayout.isRefreshing = false
            Utils.customToastMsgError(this@PropertyShowAllFromListActivity, msg, false)
        }

        override fun onGettingItemFailed(msg: String?) {
            showProgress(false)
            swipeRefreshLayout.isRefreshing = false
            Utils.customToastMsgError(this@PropertyShowAllFromListActivity, msg, false)
        }

    }

    private fun requestServiceWs() {
        showProgress(true)
        when (type) {
            getString(R.string.near_by_property) -> {
                ProjectDevelopmentWs.getListNearBy(
                    this,
                    currentPage.toString(),
                    size.toString(),
                    latitude.toString(),
                    longitude.toString(),
                    resultNearbyListener
                )
            }
            getString(R.string.featuring) -> {
                ProjectDevelopmentWs.getListFeature(
                    this,
                    currentPage.toString(),
                    size.toString(),
                    resultFeaturedListener
                )
            }
            else -> {
                ProjectDevelopmentWs.getMoreRecommend(
                    this,
                    currentPage.toString(),
                    size.toString(),
                    latitude.toString(),
                    longitude.toString(),
                    resultRecommendListener
                )
            }
        }

    }

    private fun onScrollItemRecycle() {
        recyclerViewProperty.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true
                }
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                currentItem = layoutManager.childCount
                total = layoutManager.itemCount
                scrollDown = layoutManager.findFirstVisibleItemPosition()
                if (dy > 0) {
                    if (isScrolling && currentItem + scrollDown == total) {
                        if (total == size) {
                            isScrolling = false
                            currentPage++
                            progress.visibility = View.VISIBLE
                            when (type) {
                                resources.getString(R.string.near_by_property) -> {
                                    recyclerView.scrollToPosition(listNearbyProperty.size - 1)
                                    requestServiceWs()
                                }
                                resources.getString(R.string.featuring) -> {
                                    recyclerView.scrollToPosition(listFeaturedProperty.size - 1)
                                    requestServiceWs()
                                }
                                else -> {
                                    recyclerView.scrollToPosition(listRecommendProperty.size - 1)
                                    requestServiceWs()
                                }
                            }

                            size += 10
                        } else {
                            progress.visibility = View.GONE
                        }
                    }
                }
            }
        })
    }

    //show progress
    private fun showProgress(show: Boolean) {
        if (show) {
            progress.visibility = View.VISIBLE
        } else {
            progress.visibility = View.GONE
        }

    }

}