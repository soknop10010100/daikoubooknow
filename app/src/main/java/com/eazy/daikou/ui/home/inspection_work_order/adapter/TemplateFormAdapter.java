package com.eazy.daikou.ui.home.inspection_work_order.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.ColorStateList;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.daimajia.swipe.SwipeLayout;
import com.eazy.daikou.R;
import com.eazy.daikou.base.MockUpData;
import com.eazy.daikou.request_data.request.inspection_ws.InspectionWS;
import com.eazy.daikou.request_data.static_data.StaticUtilsKey;
import com.eazy.daikou.helper.Utils;
import com.eazy.daikou.model.inspection.ItemTemplateAPIModel;
import com.eazy.daikou.model.inspection.ItemTemplateModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

public class TemplateFormAdapter extends RecyclerView.Adapter<TemplateFormAdapter.ItemViewHolder> {

    private final List<ItemTemplateModel> itemModelList;
    private List<ItemTemplateModel.SubItemTemplateModel> listSub = new ArrayList<>();
    private int count = 1;
    private final Context context;
    private SubTemplateFormAdapter subTemplateFormAdapter;
    private final ClickCallBackListener callBackListener;
    private final HashMap<String, List<ItemTemplateModel.SubItemTemplateModel>> hashMap = new HashMap<>();

    public TemplateFormAdapter(Context context, List<ItemTemplateModel> integerList, ClickCallBackListener callBackListener){
        this.itemModelList = integerList;
        this.context = context;
        this.callBackListener = callBackListener;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_form_template_inspection, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, @SuppressLint("RecyclerView") int position) {
        ItemTemplateModel itemTemplateModel = itemModelList.get(position);
        if (itemTemplateModel != null){
            if (itemTemplateModel.getType_menu() != null){
                if (itemTemplateModel.getType_menu().equals(StaticUtilsKey.detail_button_key)){
                    holder.typeMenu.setText(context.getResources().getString(R.string.detailed));
                    holder.typeMenu.setBackgroundTintList(ColorStateList.valueOf(Utils.getColor(context, R.color.blue)));
                } else if (itemTemplateModel.getType_menu().equals(StaticUtilsKey.simplify_button_key)){
                    holder.typeMenu.setText(context.getResources().getString(R.string.simplified));
                    holder.typeMenu.setBackgroundTintList(ColorStateList.valueOf(Utils.getColor(context, R.color.yellow)));
                } else {
                    holder.typeMenu.setText(context.getResources().getString(R.string.question));
                    holder.typeMenu.setBackgroundTintList(ColorStateList.valueOf(Utils.getColor(context, R.color.pdlg_color_green)));
                }
            }

            if (itemTemplateModel.isAlreadySave()){
                holder.buttonEditItem.setVisibility(View.VISIBLE);
                holder.iconAdd.setVisibility(View.GONE);
                holder.headItemEdt.setEnabled(false);
                holder.headItemEdt.setBackground(ResourcesCompat.getDrawable(context.getResources(), R.drawable.card_view_shape_dark, null));
            } else {
                holder.buttonEditItem.setVisibility(View.GONE);
                holder.iconAdd.setVisibility(View.VISIBLE);
                holder.headItemEdt.setEnabled(true);
                holder.headItemEdt.setBackground(ResourcesCompat.getDrawable(context.getResources(), R.drawable.shape_transparent_bg, null));
            }

            if (!itemTemplateModel.isAlreadySave()){
                holder.headItemEdt.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        if(!editable.toString().isEmpty()){
                            callBackListener.onWriteCategoryItem(itemTemplateModel, editable.toString(), position);
                        } else {
                            callBackListener.onWriteCategoryItem(itemTemplateModel, "", position);
                        }
                    }
                });
            }

            if (itemTemplateModel.getMain_category_inspection() != null){
                holder.headItemEdt.setText(itemTemplateModel.getMain_category_inspection());
            } else {
                holder.headItemEdt.setText("");
            }

            if (position == itemModelList.size() - 1 && !MockUpData.isRemove && !itemTemplateModel.isAlreadySave() && StaticUtilsKey.noteId.equalsIgnoreCase("")) {
                listSub = new ArrayList<>();
                count = 1;
                listSub = getListSub(itemModelList.get(itemModelList.size() - 1).getValue());
                hashMap.put(itemTemplateModel.getValue(), listSub);
                MockUpData.setListSubInspection(hashMap);
            } else {
                listSub = MockUpData.getListSubInspection().get(itemTemplateModel.getValue());
                hashMap.put(itemTemplateModel.getValue(), listSub);
            }

            holder.recyclerView.setLayoutManager(new LinearLayoutManager(context));
            holder.setIsRecyclable(false);
            subTemplateFormAdapter = new SubTemplateFormAdapter(context, holder.recyclerView, itemTemplateModel, hashMap.get(itemTemplateModel.getValue()), (position + 1) + "", itemTemplateModel.getValue(), itemTemplateModel.isAlreadySave(), itemTemplateModel.getType_menu(),callBackSubItemListener);
            holder.recyclerView.setAdapter(subTemplateFormAdapter);

            holder.iconAdd.setOnClickListener(v -> {
                count++;
                if (Objects.equals(itemTemplateModel.getValue(), StaticUtilsKey.noteId)){
                    listSub = new ArrayList<>();
                    if (hashMap.get(itemTemplateModel.getValue()) != null)  listSub = hashMap.get(itemTemplateModel.getValue());
                    assert listSub != null;
                    listSub.add(new ItemTemplateModel.SubItemTemplateModel(count + itemTemplateModel.getValue(), "BB" + count, itemTemplateModel.getType_menu(),"", "", "", false));
                } else if (StaticUtilsKey.noteId.equals("")){
                    StaticUtilsKey.noteId = itemTemplateModel.getValue();
                    listSub = new ArrayList<>();
                    if (hashMap.get(itemTemplateModel.getValue()) != null)  listSub = hashMap.get(itemTemplateModel.getValue());
                    assert listSub != null;
                    listSub.add(new ItemTemplateModel.SubItemTemplateModel(count + itemTemplateModel.getValue(), "BB" + count, itemTemplateModel.getType_menu(),"", "", "", false));
                }
                else {
                    StaticUtilsKey.noteId = itemTemplateModel.getValue();
                    listSub = new ArrayList<>();
                    if (hashMap.get(itemTemplateModel.getValue()) != null)  listSub = hashMap.get(itemTemplateModel.getValue());
                    assert listSub != null;
                    listSub.add(new ItemTemplateModel.SubItemTemplateModel(count + itemTemplateModel.getValue(), "BB" + count, itemTemplateModel.getType_menu(), "", "", "", false));
                }

                hashMap.put(itemTemplateModel.getValue(), listSub);
                MockUpData.setListSubInspection(hashMap);

                subTemplateFormAdapter = new SubTemplateFormAdapter(context, holder.recyclerView, itemTemplateModel, hashMap.get(itemTemplateModel.getValue()), (position + 1) + "", itemTemplateModel.getValue(), itemTemplateModel.isAlreadySave(), itemTemplateModel.getType_menu(), callBackSubItemListener);
                holder.recyclerView.setAdapter(subTemplateFormAdapter);
            });

            holder.buttonDeleteItem.setOnClickListener(v -> callBackListener.onClickDeleteCallBack(itemTemplateModel));

            holder.buttonAddImage.setVisibility(View.GONE);
            holder.buttonAddImage.setOnClickListener(v -> callBackListener.onClickCameraCallBack(itemTemplateModel));

            holder.iconDropDown.setOnClickListener(v -> {
                if (holder.recyclerView.getVisibility() == View.GONE){
                    holder.iconDropDown.setImageResource(R.drawable.ic_drop_down);
                    holder.recyclerView.setVisibility(View.VISIBLE);
                    if (itemTemplateModel.isAlreadySave()){
                        holder.iconAdd.setVisibility(View.GONE);
                    } else {
                        holder.iconAdd.setVisibility(View.VISIBLE);
                    }

                } else {
                    holder.iconDropDown.setImageResource(R.drawable.right_arrow_back);
                    holder.recyclerView.setVisibility(View.GONE);
                    holder.iconAdd.setVisibility(View.GONE);
                }
            });

            if (itemModelList.size() > 1){
                if (position == itemModelList.size() - 1){
                    holder.recyclerView.setVisibility(View.VISIBLE);
                    holder.iconDropDown.setImageResource(R.drawable.ic_drop_down);
                    if (itemTemplateModel.isAlreadySave()){
                        holder.iconAdd.setVisibility(View.GONE);
                    } else {
                        holder.iconAdd.setVisibility(View.VISIBLE);
                    }
                } else {
                    holder.iconDropDown.setImageResource(R.drawable.right_arrow_back);
                    holder.recyclerView.setVisibility(View.GONE);
                    holder.iconAdd.setVisibility(View.GONE);
                }
            }

            holder.buttonEditItem.setOnClickListener(v -> {
                holder.swipe_layout.close();
                callBackListener.onEditItemInspectionItem(itemTemplateModel);
            });

        }
    }

    @Override
    public int getItemCount() {
        return itemModelList.size();
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder{

        private final ImageView iconAdd, iconDropDown;
        private final LinearLayout buttonDeleteItem, buttonAddImage, buttonEditItem;
        private final RecyclerView recyclerView;
        private final TextView typeMenu;
        private final EditText headItemEdt;
        private final SwipeLayout swipe_layout;
        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            swipe_layout = itemView.findViewById(R.id.swipe_layout);
            buttonEditItem = itemView.findViewById(R.id.buttonEdit);
            typeMenu = itemView.findViewById(R.id.num);
            iconDropDown = itemView.findViewById(R.id.iconDropDown);
            iconAdd = itemView.findViewById(R.id.iconAdd);
            recyclerView = itemView.findViewById(R.id.subInspection);
            buttonDeleteItem = itemView.findViewById(R.id.buttonDeleteItem);
            buttonAddImage = itemView.findViewById(R.id.buttonAddImage);
            headItemEdt = itemView.findViewById(R.id.headItemEdt);
        }
    }

    private List<ItemTemplateModel.SubItemTemplateModel> getListSub(String value){
        for (int i = 1; i <= count; i++) {
            ItemTemplateModel.SubItemTemplateModel itemTemplateModel = new ItemTemplateModel.SubItemTemplateModel(i + value, "BB" + i,StaticUtilsKey.is_check_key,"", "", "", false);
            listSub.add(itemTemplateModel);
        }
        return listSub;
    }

    public interface ClickCallBackListener{
        void onClickDeleteCallBack(ItemTemplateModel itemTemplateModel);
        void onClickCameraCallBack(ItemTemplateModel itemTemplateModel);
        void onClickSubCameraCallBack(ItemTemplateModel.SubItemTemplateModel itemTemplateModel, String uniqueKeySub);
        void onWriteCategoryItem(ItemTemplateModel itemTemplateModel, String write_text, int position);
        void onEditItemInspectionItem(ItemTemplateModel itemTemplateModel);
    }

    private final SubTemplateFormAdapter.ClickCallBackSubItemListener callBackSubItemListener = new SubTemplateFormAdapter.ClickCallBackSubItemListener() {
        @Override
        public void onClickInspection(ItemTemplateModel.SubItemTemplateModel subItemTemplateModel) { }

        @Override
        public void onClickWriteReport(ItemTemplateModel.SubItemTemplateModel subItemTemplateModel) { }

        @Override
        public void onClickAddImage(ItemTemplateModel.SubItemTemplateModel subItemTemplateModel, String uniqueKey) {
            callBackListener.onClickSubCameraCallBack(subItemTemplateModel, uniqueKey);
        }

        @Override
        public void onClickDelete(String action_menu, RecyclerView recyclerView, ItemTemplateModel itemTemplateModel, ItemTemplateModel.SubItemTemplateModel subItemTemplateModel, String main_no, String categoryKey, boolean isAlreadySave, SwipeLayout swipeLayout) {
            if (isAlreadySave){
                AlertDialog alertDialog = new AlertDialog.Builder(context)
                        .setTitle(context.getResources().getString(R.string.confirm))
                        .setMessage(context.getResources().getString(R.string.are_you_sure_to_delete))
                        .setPositiveButton(context.getResources().getString(R.string.yes), (dialog, which) -> {
                            deleteInspectionList(context, StaticUtilsKey.sub_key, subItemTemplateModel.getItem_room_id(),
                                    recyclerView, itemTemplateModel, subItemTemplateModel, main_no, categoryKey, true, action_menu);
                            dialog.dismiss();
                        })
                        .setNegativeButton(context.getResources().getString(R.string.no), (dialog, which) -> dialog.dismiss())
                        .setCancelable(false)
                        .setIcon(ResourcesCompat.getDrawable(context.getResources(), R.drawable.ic_report_problem, null))
                        .show();
            } else {
                if (Objects.requireNonNull(hashMap.get(categoryKey)).size() > 0) {
                    for (int i = 0; i < Objects.requireNonNull(hashMap.get(categoryKey)).size(); i++) {
                        if (Objects.requireNonNull(hashMap.get(categoryKey)).get(i).getKey().equals(subItemTemplateModel.getKey())) {
                            Objects.requireNonNull(hashMap.get(categoryKey)).remove(subItemTemplateModel);
                        }
                    }

                    MockUpData.setListSubInspection(hashMap);

                    subTemplateFormAdapter = new SubTemplateFormAdapter(context, recyclerView, itemTemplateModel, hashMap.get(categoryKey), main_no, categoryKey, false,action_menu, callBackSubItemListener);
                    recyclerView.setAdapter(subTemplateFormAdapter);

                }
            }

            swipeLayout.close();
        }

        @Override
        public void onClickYesNoNA(String action_menu, RecyclerView recyclerView, ItemTemplateModel itemTemplateModel, ItemTemplateModel.SubItemTemplateModel subItemTemplateModel, String main_no, String categoryKey, String action_click) {
            switch (action_click) {
                case "clean_click":
                    if (subItemTemplateModel.getKey_menu().equalsIgnoreCase(StaticUtilsKey.simplify_button_key)) {
                        if (subItemTemplateModel.getIsClickNA().equalsIgnoreCase(Utils.getText(context, R.string.yes).toUpperCase(Locale.ROOT))) {
                            subItemTemplateModel.setIsClickNA(Utils.getText(context, R.string.no));
                        } else if (subItemTemplateModel.getIsClickNA().equalsIgnoreCase(Utils.getText(context, R.string.no))) {
                            subItemTemplateModel.setIsClickNA("N/A");
                        } else {
                            subItemTemplateModel.setIsClickNA(Utils.getText(context, R.string.yes).toUpperCase(Locale.ROOT));
                        }
                    } else {
                        subItemTemplateModel.setIsClickNA(Utils.getText(context, R.string.yes).toUpperCase(Locale.ROOT));
                    }
                    break;
                case "un_damage_click":
                    if (subItemTemplateModel.getKey_menu().equalsIgnoreCase(StaticUtilsKey.simplify_button_key)) {
                        if (subItemTemplateModel.getClickUnDamage().equalsIgnoreCase(Utils.getText(context, R.string.yes))) {
                            subItemTemplateModel.setClickUnDamage(Utils.getText(context, R.string.no));
                        } else if (subItemTemplateModel.getClickUnDamage().equalsIgnoreCase(Utils.getText(context, R.string.no))) {
                            subItemTemplateModel.setClickUnDamage("N/A");
                        } else {
                            subItemTemplateModel.setClickUnDamage(Utils.getText(context, R.string.yes));
                        }
                    } else {
                        subItemTemplateModel.setIsClickNA(Utils.getText(context, R.string.no));
                    }
                    break;
                case "working_click":
                    if (subItemTemplateModel.getKey_menu().equals(StaticUtilsKey.simplify_button_key)) {
                        if (subItemTemplateModel.getClickWorking().equalsIgnoreCase(Utils.getText(context, R.string.yes))) {
                            subItemTemplateModel.setClickWorking(Utils.getText(context, R.string.no));
                        } else if (subItemTemplateModel.getClickWorking().equalsIgnoreCase(Utils.getText(context, R.string.no))) {
                            subItemTemplateModel.setClickWorking("N/A");
                        } else {
                            subItemTemplateModel.setClickWorking(Utils.getText(context, R.string.yes));
                        }
                    } else {
                        subItemTemplateModel.setIsClickNA("N/A");
                    }
                    break;
            }

            subTemplateFormAdapter = new SubTemplateFormAdapter(context, recyclerView, itemTemplateModel, hashMap.get(categoryKey), main_no, categoryKey, false,action_menu, callBackSubItemListener);
            recyclerView.setAdapter(subTemplateFormAdapter);
        }
    };


    private void deleteInspectionList(Context context, String inspectionLevel, String inspectionId,
                                      RecyclerView recyclerView, ItemTemplateModel itemTemplateModel, ItemTemplateModel.SubItemTemplateModel subItemTemplateModel, String main_no, String categoryKey, boolean isAlreadySave, String action_menu){
        HashMap<String, Object> hashMapRequest = new HashMap<>();
        hashMapRequest.put("inspection_level", inspectionLevel);
        if (inspectionLevel.equalsIgnoreCase(StaticUtilsKey.main_key)) {
            hashMapRequest.put("inspection_room_id", inspectionId);
        } else {
            hashMapRequest.put("inspection_room_item_id", inspectionId);
        }
        new InspectionWS().DeleteInspectionList(context, hashMapRequest, new InspectionWS.CallBackUpdateInspectionListener() {
            @Override
            public void onSuccessSaveInspectionList(String msg, ItemTemplateAPIModel modelList) { }

            @Override
            public void onSuccessDeleteInspectionList(String msg) {
                Utils.customToastMsgError(context, msg, true);
                if (Objects.requireNonNull(hashMap.get(categoryKey)).size() > 0) {
                    for (int i = 0; i < Objects.requireNonNull(hashMap.get(categoryKey)).size(); i++) {
                        if (Objects.requireNonNull(hashMap.get(categoryKey)).get(i).getKey().equals(subItemTemplateModel.getKey())) {
                            Objects.requireNonNull(hashMap.get(categoryKey)).remove(subItemTemplateModel);
                        }
                    }

                    itemTemplateModel.getItemTemplateAPIModel().getInspectionSubTitle().removeIf(subItemTemplateAPIModel -> subItemTemplateModel.getItem_room_id().equalsIgnoreCase(subItemTemplateAPIModel.getRoomItemId()));

                    MockUpData.setListSubInspection(hashMap);

                    subTemplateFormAdapter = new SubTemplateFormAdapter(context, recyclerView, itemTemplateModel, hashMap.get(categoryKey), main_no, categoryKey, isAlreadySave,action_menu, callBackSubItemListener);
                    recyclerView.setAdapter(subTemplateFormAdapter);
                }
            }

            @Override
            public void onFailed(String msg) {
                Utils.customToastMsgError(context, msg, false);
            }
        });
    }

}
