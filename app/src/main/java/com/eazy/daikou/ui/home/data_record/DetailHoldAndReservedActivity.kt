package com.eazy.daikou.ui.home.data_record

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.graphics.Paint
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.request_data.request.data_record.DataRecordWS
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.helper.web.WebsitePropertyActivity
import com.eazy.daikou.model.CustomCategoryModel
import com.eazy.daikou.model.data_record.AllStatus
import com.eazy.daikou.model.data_record.DetailHoldAndReservedModel
import com.eazy.daikou.model.data_record.PaymentSchedule
import com.eazy.daikou.model.profile.User
import com.eazy.daikou.ui.CustomMenuHeaderAdapter
import com.eazy.daikou.ui.home.data_record.adapter.AdapterPaymentScheduleDR
import com.eazy.daikou.ui.home.my_property.my_unit.PropertyMyUnitDetailActivity
import com.google.gson.Gson

class DetailHoldAndReservedActivity : BaseActivity() {
    private lateinit var progressBar : ProgressBar
    private lateinit var holdAndReservedIdTv : TextView
    private lateinit var linearHoldAndReservedNo : LinearLayout
    private lateinit var lineaTotalPayment : LinearLayout
    private lateinit var amountTotalTv : TextView
    private lateinit var intersAmountTv : TextView
    private lateinit var paymentsTv : TextView
    private lateinit var customerPhoneTv : TextView
    private lateinit var customerNameTv : TextView
    private lateinit var discountQuotationTv : TextView
    private lateinit var optionTv : TextView
    private lateinit var paymentTernTypeTv : TextView
    private lateinit var supportTeamTv : TextView
    private lateinit var channelTv : TextView
    private lateinit var linearPaymentSchedule : LinearLayout
    private lateinit var iconDownPaymentSchedule : ImageView
    private lateinit var linearDownPaymentSchedule : LinearLayout
    private lateinit var projectNameTv : TextView
    private lateinit var noDataPaymentSchedule : TextView
    private lateinit var sellingPriceHoldTv : TextView
    private lateinit var holdIdTv : TextView
    private lateinit var unitTypeTv : TextView
    private lateinit var floorTv : TextView
    private lateinit var unitTv : TextView
    private lateinit var typeTv : TextView
    private lateinit var startDateTv : TextView
    private lateinit var endDateTv : TextView
    private lateinit var discountTv : TextView
    private lateinit var voucherAmountTv : TextView
    private lateinit var sellingPriceTv : TextView
    private lateinit var amountTv : TextView
    private lateinit var quotationNoTv : TextView
    private lateinit var priorityTv : TextView
    private lateinit var voucherQuotationTv : TextView
    private lateinit var sellingPriceQuotationTv : TextView
    private lateinit var expiredDateTv : TextView
    private lateinit var linearHold : LinearLayout
    private lateinit var titleDetailTv : TextView
    private lateinit var linearTypeHold : LinearLayout
    private lateinit var titleAmountTv : TextView

    private lateinit var menuStepRecycle : RecyclerView
    private lateinit var adapterPaymentSchedule : AdapterPaymentScheduleDR
    private lateinit var recyclerPaymentReschedule : RecyclerView
    private lateinit var iconViewInvoice : LinearLayout
    private var listPaymentSchedule : ArrayList<PaymentSchedule> = ArrayList()
    private var idList = ""
    private var actionList = ""

    private lateinit var user : User
    private var userName = ""
    private var userPhone = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_hold_and_reserved)

        initView()

        initData()

        initAction()
    }

    private fun initView(){
        titleDetailTv = findViewById(R.id.titleDetailTv)
        progressBar = findViewById(R.id.progressItem)
        lineaTotalPayment = findViewById(R.id.lineaTotalPayment)
        amountTotalTv = findViewById(R.id.amountTotalTv)
        intersAmountTv = findViewById(R.id.intersAmountTv)
        paymentsTv = findViewById(R.id.paymentsTv)
        menuStepRecycle = findViewById(R.id.menuStepRecycle)
        customerNameTv = findViewById(R.id.customerNameTv)
        customerPhoneTv = findViewById(R.id.customerPhoneTv)
        channelTv = findViewById(R.id.channelTv)
        supportTeamTv = findViewById(R.id.supportTeamTv)
        paymentTernTypeTv = findViewById(R.id.paymentTernTypeTv)
        optionTv = findViewById(R.id.optionTv)
        discountQuotationTv = findViewById(R.id.discountQuotationTv)
        linearPaymentSchedule = findViewById(R.id.linearPaymentSchedule)
        iconDownPaymentSchedule = findViewById(R.id.iconDownPaymentSchedule)
        linearDownPaymentSchedule = findViewById(R.id.linearDownPaymentSchedule)
        projectNameTv = findViewById(R.id.projectNameTv)
        noDataPaymentSchedule = findViewById(R.id.noDataPaymentSchedule)
        linearHold = findViewById(R.id.linearHold)
        sellingPriceHoldTv = findViewById(R.id.sellingPriceHoldTv)
        holdIdTv = findViewById(R.id.holdIdTv)
        unitTypeTv = findViewById(R.id.unitTypeTv)
        floorTv = findViewById(R.id.floorTv)
        unitTv = findViewById(R.id.unitTv)
        typeTv = findViewById(R.id.TypeTv)
        startDateTv = findViewById(R.id.startDateTv)
        endDateTv = findViewById(R.id.endDateTv)
        discountTv = findViewById(R.id.discountTv)
        voucherAmountTv = findViewById(R.id.voucherAmountTv)
        sellingPriceTv = findViewById(R.id.sellingPriceTv)
        quotationNoTv = findViewById(R.id.quotationNoTv)
        amountTv = findViewById(R.id.amountTv)
        priorityTv = findViewById(R.id.priorityTv)
        voucherQuotationTv = findViewById(R.id.voucherQuotationTv)
        sellingPriceQuotationTv = findViewById(R.id.sellingPriceQuotationTv)
        expiredDateTv = findViewById(R.id.expiredDateTv)
        recyclerPaymentReschedule = findViewById(R.id.recyclerPaymentReschedule)
        iconViewInvoice = findViewById(R.id.iconViewInvoice)
        linearTypeHold = findViewById(R.id.linearTypeHold)
        titleAmountTv = findViewById(R.id.titleAmountTv)
        linearHoldAndReservedNo = findViewById(R.id.linearHoldAndReservedNo)
        holdAndReservedIdTv = findViewById(R.id.holdAndReservedIdTv)
    }

    private fun initData(){

        if (intent != null && intent.hasExtra("id_list")){
            idList = intent.getStringExtra("id_list") as String
        }

        if (intent != null && intent.hasExtra("action")){
            actionList = intent.getStringExtra("action") as String
        }

        user = Gson().fromJson(UserSessionManagement(this).userDetail, User::class.java)
        val name = user.name
        val phone = user.phoneNumber

        if (name != null) { userName = name }
        if (phone != null) { userPhone = phone }
    }

    private fun initAction(){

        if (actionList == "hold_reserved"){
            Utils.customOnToolbar(this, getString(R.string.hold_and_reserved_detail)){ finish() }
            titleDetailTv.text = getString(R.string.hold_and_reserved_detail)
        } else {
            Utils.customOnToolbar(this, getString(R.string.booking_detail)){ finish() }
            linearHoldAndReservedNo.visibility = View.VISIBLE
            titleDetailTv.text = getString(R.string.booking_detail)
            linearTypeHold.visibility = View.GONE
            titleAmountTv.text =  getString(R.string.booking_amount)
        }

        requestServiceAPI()

        linearDownPaymentSchedule.setOnClickListener( CustomSetOnClickViewListener{
            loadViewDropList(recyclerPaymentReschedule, iconDownPaymentSchedule, linearPaymentSchedule)
        })

        val linearLayoutManager = LinearLayoutManager(this)
        recyclerPaymentReschedule.layoutManager = linearLayoutManager
        adapterPaymentSchedule = AdapterPaymentScheduleDR(this, "", listPaymentSchedule, callBackClickInvoice)
        recyclerPaymentReschedule.adapter = adapterPaymentSchedule
    }

    private val callBackClickInvoice = object : AdapterPaymentScheduleDR.CallBackItemClickListener {
        override fun clickItemCallback(paymentSchedule: PaymentSchedule) {}
    }

    private fun setClickUnit(idUnit : String){
        unitTypeTv.setOnClickListener(CustomSetOnClickViewListener{
            val intent = Intent(this@DetailHoldAndReservedActivity, PropertyMyUnitDetailActivity::class.java)
            intent.putExtra("id", idUnit)
            intent.putExtra("action_type", "data_record_unit")
            startActivity(intent)
        })
    }

    private fun setClickSelectDetailHoldAndReserved(detailHoldAndReserve: DetailHoldAndReservedModel){
        holdAndReservedIdTv.setOnClickListener(CustomSetOnClickViewListener {
            if (detailHoldAndReserve.hold_and_reserve != null){
                val intent = Intent(this@DetailHoldAndReservedActivity, DetailHoldAndReservedActivity::class.java)
                intent.putExtra("id_list", detailHoldAndReserve.hold_and_reserve!!.id.toString())
                intent.putExtra("action", "hold_reserved")
                startActivity(intent)
            } else {
                Utils.customToastMsgError(this@DetailHoldAndReservedActivity, getString(R.string.something_went_wrong), false)
            }

        })
    }

    private fun buttonPayment(detailHoldAndReserve: DetailHoldAndReservedModel) {
        iconViewInvoice.setOnClickListener {
            val intent = Intent(this, WebsitePropertyActivity::class.java)
            intent.putExtra("url_web", detailHoldAndReserve.invoice_receipt_url)
                intent.putExtra("link_kess",detailHoldAndReserve.payment_kess_url)
            startActivity(intent)
        }
    }

    private fun setMenuStepAdapter(context: Activity, listStepMenu : ArrayList<AllStatus>){
        val list = ArrayList<CustomCategoryModel>()
        for (item in listStepMenu){
            list.add(CustomCategoryModel(item.status, item.status_display, item.is_active))
        }

        CustomMenuHeaderAdapter.setMenuStepAdapter(context, "status", list, object : CustomMenuHeaderAdapter.ClickCallBackListener{
            override fun onClickCallBack(item: CustomCategoryModel) {}
        })
    }

    private fun loadViewDropList(recyclerViewDrop: RecyclerView, iconDrop : ImageView, linearData : LinearLayout){
        if(recyclerViewDrop.visibility == View.GONE){
            recyclerViewDrop.visibility = View.VISIBLE
            linearData.visibility = View.VISIBLE
            iconDrop.setImageResource(R.drawable.ic_arrow_next)

        } else {
            recyclerViewDrop.visibility = View.GONE
            iconDrop.setImageResource(R.drawable.ic_drop_down)
            linearData.visibility = View.GONE
        }
    }

    private fun requestServiceAPI(){
        progressBar.visibility = View.VISIBLE
        if (actionList == "hold_reserved"){
            DataRecordWS().getHoldAndReservedDetailWS(this, idList, callBackDetailHoldAndReserved)
        } else {
            DataRecordWS().getBookingDetailWS(this, idList, callBackDetailHoldAndReserved)
        }
    }

    private val callBackDetailHoldAndReserved = object : DataRecordWS.OnCallBackDetailHoldAndReservedListener{
        @SuppressLint("NotifyDataSetChanged")
        override fun onSuccess(detailOpportunity: DetailHoldAndReservedModel) {
            progressBar.visibility = View.GONE
            linearHold.visibility = View.VISIBLE

            setClickSelectDetailHoldAndReserved(detailOpportunity)

            setDataDetail(detailOpportunity)

            buttonPayment(detailOpportunity)

            setMenuStepAdapter(this@DetailHoldAndReservedActivity, detailOpportunity.all_status )

            if (detailOpportunity.payment_schedules.size > 0){
                lineaTotalPayment.visibility = View.VISIBLE
                listPaymentSchedule.addAll(detailOpportunity.payment_schedules)
            } else{
                noDataPaymentSchedule.visibility = View.VISIBLE
            }
            adapterPaymentSchedule.notifyDataSetChanged()
        }

        override fun onFailed(error: String) {
            progressBar.visibility = View.GONE
            Utils.customToastMsgError(this@DetailHoldAndReservedActivity, error, false)
        }
    }

    private fun setDataDetail( detail: DetailHoldAndReservedModel){
        holdAndReservedIdTv.paintFlags = holdAndReservedIdTv.paintFlags or Paint.UNDERLINE_TEXT_FLAG

        setClickUnit(detail.unit!!.id.toString())
        Utils.setValueOnText(holdAndReservedIdTv, if (detail.hold_and_reserve != null)  detail.hold_and_reserve!!.code else ". . .")
        Utils.setValueOnText(projectNameTv, if (detail.account != null) detail.account!!.name else ". . .")
        Utils.setValueOnText(customerNameTv, userName)
        Utils.setValueOnText(customerPhoneTv, userPhone)

        Utils.setValueOnText(amountTotalTv, detail.total_amount)
        Utils.setValueOnText(intersAmountTv, detail.total_interest_amount)
        Utils.setValueOnText(paymentsTv, detail.total_payment_amount)

        if (detail.unit != null){
            unitTv.text = if (detail.unit!!.name != null) detail.unit!!.name else ". . ."
            unitTypeTv.text = if (detail.unit!!.type!!.name != null) detail.unit!!.type!!.name else ". . ."
            floorTv.text = if (detail.unit!!.floor_no != null) detail.unit!!.floor_no else ". . ."
        }

        holdIdTv.text = if (detail.code != null) detail.code else ". . ."
        typeTv.text = if (detail.type != null) detail.type else ". . ."
        startDateTv.text = if (detail.start_date != null) Utils.formatDateFromString("yyyy-MM-dd", "dd-MMM-yyyy", detail.start_date) else ". . ."
        endDateTv.text = if (detail.end_date != null ) Utils.formatDateFromString("yyyy-MM-dd", "dd-MMM-yyyy", detail.end_date) +" "+
                Utils.formatDateTime(detail.end_time,"kk:mm", "hh:mm a") else ". . ."
        discountTv.text =  if (detail.discount != null) detail.discount else ". . ."
        amountTv.text =  if (detail.amount != null) detail.amount else ". . ."
        voucherAmountTv.text = if(detail.voucherAmountTv != null) detail.voucherAmountTv else ". . ."

        if(detail.quotation != null){
            quotationNoTv.text = if (detail.quotation!!.no != null) detail.quotation!!.no else ". . ."
            priorityTv.text = if (detail.quotation!!.priority != null) detail.quotation!!.priority else ". . ."
            sellingPriceHoldTv.text = if (detail.quotation!!.sale_price != null) detail.quotation!!.sale_price else ". . ."
            voucherQuotationTv.text = if (detail.quotation!!.voucher_amount != null) detail.quotation!!.voucher_amount else ". . ."
            sellingPriceQuotationTv.text = if (detail.quotation!!.sale_price != null) detail.quotation!!.sale_price else ". . ."
            expiredDateTv.text = if (detail.quotation!!.expire_date != null) Utils.formatDateFromString("yyyy-MM-dd", "dd-MMM-yyyy", detail.quotation!!.expire_date) else ". . ."
            voucherAmountTv.text = if (detail.quotation!!.voucher_amount != null) detail.quotation!!.voucher_amount else ". . ."
            sellingPriceTv.text = if (detail.quotation!!.sale_price != null) detail.quotation!!.sale_price else ". . ."

            Utils.setValueOnText(channelTv, detail.quotation!!.channel_name)
            Utils.setValueOnText(supportTeamTv, detail.quotation!!.supporting_team)
            Utils.setValueOnText(paymentTernTypeTv, detail.quotation!!.payment_term_type)
            Utils.setValueOnText(optionTv, detail.quotation!!.option)
            Utils.setValueOnText(discountQuotationTv, detail.quotation!!.discount)
        } else{
            quotationNoTv.text = ". . ."
            priorityTv.text = ". . ."
            sellingPriceHoldTv.text = ". . ."
            voucherQuotationTv.text = ". . ."
            sellingPriceQuotationTv.text = ". . ."
            expiredDateTv.text = ". . ."
            voucherAmountTv.text = ". . ."
            sellingPriceTv.text = ". . ."
            channelTv.text = ". . ."
            supportTeamTv.text = ". . ."
            paymentTernTypeTv.text = ". . ."
            optionTv.text = ". . ."
            discountQuotationTv.text = ". . ."
        }
    }
}