package com.eazy.daikou.ui.home.parking;

import static com.eazy.daikou.helper.Utils.convert;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.eazy.daikou.R;
import com.eazy.daikou.base.BaseActivity;
import com.eazy.daikou.base.BaseCameraActivity;
import com.eazy.daikou.request_data.request.parking_ws.PrinterWs;
import com.eazy.daikou.model.parking.CheckInOutPrinter;
import com.eazy.daikou.ui.home.parking.printer.SunmiPrintHelper;

import java.util.HashMap;

public class PrinterActivity extends BaseActivity {

    private ImageView btnBack;
    private TextView btnPrint;
    private RelativeLayout btnTakePhoto;
    private ImageView imageView;

    private CheckInOutPrinter parkingPrinter;
    private String fileImage = "";
    private final int CODE_CAMERA = 723;
    private String type ="";
    private String propertyId = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_printer);

        TextView title = findViewById(R.id.titleToolbar);
        btnBack = findViewById(R.id.iconBack);
        btnPrint = findViewById(R.id.btn_print);
        imageView = findViewById(R.id.image_car);
        ImageView imageCamera = findViewById(R.id.camera);
        btnTakePhoto = findViewById(R.id.btn_take_photo);
        title.setText(R.string.check_in);
        title.setAllCaps(true);

        if (getIntent().hasExtra("parkingPrinter")) {
            parkingPrinter = (CheckInOutPrinter) getIntent().getSerializableExtra("parkingPrinter");
        }

        if(getIntent().hasExtra("type")){
            type = getIntent().getStringExtra("type");
        }

        if(getIntent().hasExtra("propertyId")){
            propertyId = getIntent().getStringExtra("propertyId");
        }

        if(type.equals("have_qr")){
            btnTakePhoto.setEnabled(false);
            imageCamera.setVisibility(View.GONE);
        }

        initAction();


    }

    private void initAction(){
        btnBack.setOnClickListener(view -> finish());

        btnTakePhoto.setOnClickListener(view -> {
            Intent intent =  new Intent(this,BaseCameraActivity.class);
            intent.putExtra("no_select","yes");
            startActivityForResult(intent,CODE_CAMERA);
        });
        btnPrint.setOnClickListener(view -> {
            btnPrint.setEnabled(true);
            if(type.equals("have_qr")){
                SunmiPrintHelper.getInstance().printCheckInParking(PrinterActivity.this,parkingPrinter);
                Intent intent = new Intent();
                setResult(RESULT_OK, intent);
                finish();
            }else {
                if(fileImage.isEmpty()){
                    btnPrint.setEnabled(false);
                    Toast.makeText(PrinterActivity.this, "Please take photo.", Toast.LENGTH_SHORT).show();
                }else {
                    printNoQrCode();
                }

            }

        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data!=null && requestCode == CODE_CAMERA && resultCode == RESULT_OK) {
            if(data.hasExtra("image_path")){
                String imagePath = data.getStringExtra("image_path");
                Bitmap loadedBitmap = BaseCameraActivity.convertByteToBitmap(imagePath);
                fileImage = convert(loadedBitmap);
                Glide.with(imageView).load(loadedBitmap).into(imageView);
            }

        }

    }

    private void printNoQrCode(){
        HashMap<String, Object> hashMap = new HashMap<>();
        //hashMap.put("user_id",userId);
        hashMap.put("property_id",propertyId);
        hashMap.put("print_parking_type","take_an_picture");
        hashMap.put("car_image",fileImage);
        new PrinterWs().printerCheckIn(this, hashMap, new PrinterWs.OnRequestPrinterCallBack() {
            @Override
            public void onLoadSuccess(CheckInOutPrinter listParking) {
                btnPrint.setEnabled(false);
                SunmiPrintHelper.getInstance().printCheckInParking(PrinterActivity.this,listParking);
                Intent intent = new Intent();
                setResult(RESULT_OK, intent);
                finish();
            }

            @Override
            public void onLoadFail(String message) {
                btnPrint.setEnabled(false);
                Toast.makeText(PrinterActivity.this, message, Toast.LENGTH_SHORT).show();

            }
        });

    }
}