package com.eazy.daikou.ui.home.hrm.leave_request.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.model.hr.LeaveBalance


class TypeLeaveCategoryAdapter(private val listLeaveBalance: ArrayList<LeaveBalance>, private val context : Context) : RecyclerView.Adapter<TypeLeaveCategoryAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.layout_adapter_leave_category, parent, false)
        return ViewHolder(v)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val leave: LeaveBalance = listLeaveBalance[position]
        holder.categoryNameTv.text = leave.leave_category

        holder.leaveBalanceTv.text = context.getString(R.string.leave_balances) + leave.total_user_leave_left + " " + context.resources.getString(R.string.day_s)

        holder.totalLeaveTv.text = context.getString(R.string.totals) + leave.leave_per_year + " " + context.resources.getString(R.string.day_s)

    }

    override fun getItemCount(): Int {
        return listLeaveBalance.size
    }


    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val categoryNameTv : TextView = view.findViewById(R.id.category_name)
        val leaveBalanceTv : TextView = view.findViewById(R.id.leave_balance)
        val totalLeaveTv : TextView = view.findViewById(R.id.total_leave)

    }

    fun clear() {
        val size: Int = listLeaveBalance.size
        if (size > 0) {
            for (i in 0 until size) {
                listLeaveBalance.removeAt(0)
            }
            notifyItemRangeRemoved(0, size)
        }
    }

}