package com.eazy.daikou.ui.home.hrm.report_attendance

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.base.MockUpData
import com.eazy.daikou.request_data.request.hr_ws.MyAttendanceWs
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.hr.HRAttendanceModel
import com.eazy.daikou.model.hr.HrAttendanceDetailModel
import com.eazy.daikou.model.hr.ItemAttendanceModel
import com.eazy.daikou.model.profile.User
import com.eazy.daikou.ui.home.hrm.report_attendance.adapter.MyHrAttendanceAdapter
import com.google.gson.Gson
import java.util.*
import kotlin.collections.ArrayList

class AttendanceHrReportActivity : BaseActivity(){

    private lateinit var recyclerView : RecyclerView
    private lateinit var btnSeeAllAttendance : ImageView
    private lateinit var linearLayoutManager : LinearLayoutManager
    private var myAttendanceAdapter: MyHrAttendanceAdapter? = null
    private lateinit var refreshLayout: SwipeRefreshLayout
    private lateinit var btnGoCheckIn : ImageView
    private lateinit var progressBar: ProgressBar

    private var listMyAttendance : ArrayList<ItemAttendanceModel> = ArrayList()

    private var REQUEST_CODE_CHECK_IN = 545
    private var REQUEST_CODE_DETAIL = 575
    private var currentItem = 0
    private  var total : Int = 0
    private  var scrollDown : Int = 0
    private  var currentPage : Int = 1
    private  var size : Int = 10
    private var isScrolling = false
    private lateinit var backUpTxtView : TextView
    private lateinit var user : User
    private var isLivePhoto : Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_report_attendance)

        initView()

        initData()

        initAction()
    }

    private fun initView(){
        recyclerView = findViewById(R.id.list_attendance)
        btnGoCheckIn = findViewById(R.id.btnAdd)
        progressBar = findViewById(R.id.progressItem)
        refreshLayout = findViewById(R.id.swipe_layouts)
        findViewById<LinearLayout>(R.id.layoutMap).visibility = View.INVISIBLE
        btnSeeAllAttendance = findViewById(R.id.addressMap)
        findViewById<LinearLayout>(R.id.layoutMap).visibility = View.VISIBLE

        btnSeeAllAttendance.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_home_purchase_order))

    }

    private fun initData(){

        user = Gson().fromJson(UserSessionManagement(this).userDetail, User::class.java)

        linearLayoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = linearLayoutManager

        requestMyAttendanceList()

        onScrollComplainSolution()

        refreshLayout.setColorSchemeResources(R.color.colorPrimary)
        refreshLayout.setOnRefreshListener { this.refreshList() }

    }

    private fun refreshList() {
        currentPage = 1
        size = 10
        isScrolling = true
        myAttendanceAdapter!!.clear()

        requestMyAttendanceList()
    }

    private fun requestMyAttendanceList(){
        progressBar.visibility = View.VISIBLE
        MyAttendanceWs().getListMyAttendance(this, currentPage, MockUpData.userBusinessKey(UserSessionManagement(this)), object : MyAttendanceWs.OnCallBackListener{
            @SuppressLint("NotifyDataSetChanged")
            override fun onLoadListSuccess(hrAttendanceModel: HRAttendanceModel) {
                progressBar.visibility = View.GONE
                refreshLayout.isRefreshing = false
                isLivePhoto = hrAttendanceModel.is_live_photo_required

                if (currentPage == 1) {
                    if (hrAttendanceModel.today_list != null){
                        hrAttendanceModel.today_list!!.action_type = ("today_info")
                        listMyAttendance.add(hrAttendanceModel.today_list!!)
                    } else {
                        val hr = ItemAttendanceModel()
                        hr.action_type = ("none_today_info")
                        listMyAttendance.add(hr)
                    }
                }

                for (itemAttendanceModel in Objects.requireNonNull(hrAttendanceModel.list)) {
                    itemAttendanceModel.action_type=("history_info")
                    listMyAttendance.add(itemAttendanceModel)
                }
                if (hrAttendanceModel.list.isEmpty() && currentPage == 1){
                    val hr = ItemAttendanceModel()
                    hr.action_type = "none_history_info"
                    listMyAttendance.add(hr)
                }

                if (myAttendanceAdapter == null){
                    myAttendanceAdapter = MyHrAttendanceAdapter(listMyAttendance, hrAttendanceModel.today_work_status.toString(), attendanceObject)
                    recyclerView.adapter = myAttendanceAdapter
                }
                myAttendanceAdapter!!.notifyDataSetChanged()

            }

            override fun onLoadListDetailSuccess(hrAttendanceModel: HrAttendanceDetailModel) {}

            override fun onLoadFail(message: String) {
                progressBar.visibility = View.GONE
                refreshLayout.isRefreshing = false
                Utils.customToastMsgError(this@AttendanceHrReportActivity, message, false)
            }

        })
    }

    private fun  initAction() {
        Utils.customOnToolbar(this, resources.getString(R.string.my_attendance).uppercase()){finish()}

        checkRoleListViewAll()

        btnSeeAllAttendance.setOnClickListener {
           startActivity(Intent(this, AttendanceViewAllListActivity::class.java))
        }
    }
    private fun toCheckIn(isSelfiePhoto: Boolean){
        btnGoCheckIn.setOnClickListener{
            val intent = Intent(this, CheckInOutAttendanceActivity::class.java)
            intent.putExtra("selfie_photo", isSelfiePhoto)
            startActivityForResult(intent, REQUEST_CODE_CHECK_IN)
            btnGoCheckIn.isEnabled = false
        }
    }

    private fun checkRoleListViewAll() {
//        val roleUserType = user.hrmRole
//        if (roleUserType != null) {
//            btnSeeAllAttendance.visibility =
//                if (roleUserType == "superadmin" || roleUserType == "subadmin" || roleUserType == "hr") View.VISIBLE else View.GONE
//        } else {
//            btnSeeAllAttendance.visibility = View.GONE
//        }
    }

    private fun onScrollComplainSolution() {
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true
                }
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                currentItem = linearLayoutManager.childCount
                total = linearLayoutManager.itemCount
                scrollDown = linearLayoutManager.findFirstVisibleItemPosition()
                if (dy > 0) {
                    if (isScrolling && currentItem + scrollDown == total) {
                        if (total == (size + 1)) {
                            isScrolling = false
                            currentPage++
                            progressBar.visibility = View.VISIBLE
                            recyclerView.scrollToPosition(listMyAttendance.size - 1)
                            requestMyAttendanceList()
                            size += 10
                        } else {
                            progressBar.visibility = View.GONE
                        }
                    }
                }
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK && requestCode ==REQUEST_CODE_CHECK_IN){
            if (data != null){
                if (data.hasExtra("isClickedFirst")){
                    val statusResult = data.getBooleanExtra("isClickedFirst", false)
                    if (statusResult) {
                        backUpTxtView.isEnabled = true
                    }
                } else {
                    backUpTxtView.isEnabled = true
                    val statusResult = data.getBooleanExtra("status", false)
                    if (statusResult) {
                        refreshList()
                    }
                }
            }
        } else if (resultCode == RESULT_OK && requestCode == REQUEST_CODE_DETAIL){
            refreshList()
        }
    }

    private val  attendanceObject = object : MyHrAttendanceAdapter.ItemClickListener{
        override fun onLoadSuccess(data: ItemAttendanceModel) {
            if (data.action_type == "today_info" || data.action_type == "history_info") {
                val intent = Intent(this@AttendanceHrReportActivity, AttendanceReportHrDetailActivity::class.java)
                intent.putExtra("item_attendance", data)
                startActivityForResult(intent, REQUEST_CODE_DETAIL)
            }
        }

        override fun onClickedCheckIn(txtView: TextView) {
            backUpTxtView = txtView
            txtView.isEnabled = false
            val intent = Intent(this@AttendanceHrReportActivity, CheckInOutAttendanceActivity::class.java)
            intent.putExtra("selfie_photo", isLivePhoto)
            startActivityForResult(intent, REQUEST_CODE_CHECK_IN)
        }

    }

}