package com.eazy.daikou.ui.home.book_now.booking_hotel.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.helper.DateUtil
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.booking_hotel.HotelArticleModel

class HotelArticlesAdapter(private val hotelArticlesList : ArrayList<HotelArticleModel>, private val onClickListener : OnClickItemListener) : RecyclerView.Adapter<HotelArticlesAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.hotel_articles_item_model, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBindingView(hotelArticlesList[position], onClickListener)
    }

    override fun getItemCount(): Int {
        return hotelArticlesList.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val durationTv : TextView = itemView.findViewById(R.id.DurationTv)
        private val titleTv : TextView = itemView.findViewById(R.id.titleTv)
        private val descriptionTv : TextView = itemView.findViewById(R.id.descriptionTv)
        private val typeArticleTv : TextView = itemView.findViewById(R.id.typeArticleTv)
        private val dateTv : TextView = itemView.findViewById(R.id.dateTv)
        private val numViewerTv : TextView = itemView.findViewById(R.id.numViewerTv)
        private val imgArticle : ImageView = itemView.findViewById(R.id.img_article)
        private val mainLayout : CardView = itemView.findViewById(R.id.mainLayout)
        private val btnShare : ImageView = itemView.findViewById(R.id.btnShare)

        fun onBindingView(hotelArticleModel: HotelArticleModel, onClickListener : OnClickItemListener){
            Utils.setValueOnText(titleTv, hotelArticleModel.title)
            Utils.setTextHtml(descriptionTv, hotelArticleModel.content)
            Utils.setValueOnText(typeArticleTv, hotelArticleModel.category_name)

            if (hotelArticleModel.created_at != null){
                dateTv.text = DateUtil.formatDateComplaint(hotelArticleModel.created_at)
            } else {
                dateTv.text = "- - -"
            }

            Utils.setValueOnText(numViewerTv, hotelArticleModel.view)
            Glide.with(imgArticle).load(if (hotelArticleModel.image != null) hotelArticleModel.image else R.drawable.no_image).into(imgArticle)

            Utils.setValueOnText(durationTv, DateUtil.getDurationDate(durationTv.context, hotelArticleModel.created_at))

            mainLayout.setOnClickListener(CustomSetOnClickViewListener{
                onClickListener.onClickListener(hotelArticleModel, "detail")
            })

            btnShare.setOnClickListener(CustomSetOnClickViewListener{
                onClickListener.onClickListener(hotelArticleModel, "share_article")
            })
        }
    }

    fun clear() {
        val size: Int = hotelArticlesList.size
        if (size > 0) {
            for (i in 0 until size) {
                hotelArticlesList.removeAt(0)
            }
            notifyItemRangeRemoved(0, size)
        }
    }

    interface OnClickItemListener {
        fun onClickListener(hotelArticleModel: HotelArticleModel, action: String)
    }
}