package com.eazy.daikou.ui.home.my_property_v1.property_service_employee.work_service_employee

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.base.MockUpData
import com.eazy.daikou.request_data.request.service_property_ws.cleaner.ServiceScheduleEmployeeWs
import com.eazy.daikou.request_data.static_data.StaticUtilsKey
import com.eazy.daikou.helper.CustomStartIntentUtilsClass.Companion.onStartSplashScreenActivity
import com.eazy.daikou.helper.GetDataUtils
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.my_property.service_property_employee.ServiceDayScheduleModel
import com.eazy.daikou.model.profile.User
import com.eazy.daikou.ui.home.my_property_v1.property_service_employee.service_adapter_v2.DayScheduleAdapter
import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class ServicePropertyMainEmployeeListActivity : BaseActivity() {

    private lateinit var recyclerView : RecyclerView
    private lateinit var levelAndStarAdapter : DayScheduleAdapter
    private lateinit var progressBar : ProgressBar
    private lateinit var switchMenuTop : ImageView
    private lateinit var mUser: User
    private var userId = ""
    private var accountId = ""
    private var userType = ""
    private var dateToServer = ""

    private var notificationType = ""
    private lateinit var currentDateTv : TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_service_cleaner)

        initView()

        initData()

        initAction()
    }

    private fun initView(){
        progressBar = findViewById(R.id.progressItem)
        recyclerView = findViewById(R.id.recyclerView)
        currentDateTv = findViewById(R.id.currentDateTv)
        findViewById<LinearLayout>(R.id.layoutMap).visibility = View.VISIBLE
        switchMenuTop = findViewById(R.id.addressMap)

    }

    @SuppressLint("NotifyDataSetChanged", "SetTextI18n")
    private fun initData(){
        val userSessionManagement = UserSessionManagement(this)
        mUser = MockUpData.getUserItem(userSessionManagement)

        accountId = Utils.validateNullValue(mUser.accountId)
        userType = Utils.validateNullValue(mUser.activeUserType)
        userId = userSessionManagement.userId

        StaticUtilsKey.operation_part = GetDataUtils.getDataFromString("action", this)

        if(intent.hasExtra("action_notification")){
            notificationType = intent.getStringExtra("action_notification") as String
        }

        if (StaticUtilsKey.operation_part == "maintenance_work_engine"){
            findViewById<TextView>(R.id.titleToolbar).text = getString(R.string.work_engine_schedule)
            findViewById<ImageView>(R.id.iconBack).setOnClickListener { finish() }
        } else{
            Utils.customOnToolbar(this, StaticUtilsKey.operation_part + " " + "schedule"){ finish()}
        }
    }

    private fun initAction(){
        val monthFormatForServer = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        val monthFormatForDisplay = SimpleDateFormat("MMMM, yyyy", Locale.getDefault())
        val date = Date()
        dateToServer = monthFormatForServer.format(date)
        currentDateTv.text = monthFormatForDisplay.format(date)

        switchMenuTop.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_report_description))
        switchMenuTop.setOnClickListener(
            CustomSetOnClickViewListener{
                startHistoryService(null)
            }
        )
        currentDateTv.setOnClickListener(
            CustomSetOnClickViewListener{
                SingleDateAndTimePickerDialog.Builder(this)
                    .bottomSheet()
                    .curved()
                    .backgroundColor(getColor(R.color.color_blue_gray))
                    .mainColor(getColor(R.color.blue))
                    .displayMinutes(false)
                    .displayHours(false)
                    .displayDays(false)
                    .displayMonth(true)
                    .displayYears(true)
                    .displayDaysOfMonth(false)
                    .title((Utils.getText(this, R.string.filter_month)))
                    .titleTextColor(getColor(R.color.white))
                    .listener { date->
                        dateToServer = monthFormatForServer.format(date)
                        currentDateTv.text = monthFormatForDisplay.format(date)
                        getListScheduleWs()
                    }.display()
            }
        )

        // Start From Notification
        if(notificationType == "notification_work_schedule_employee"){
            val intent = Intent(this@ServicePropertyMainEmployeeListActivity, ServicePropertyHistoryEmployeeActivity::class.java)
            resultLauncher.launch(intent)
        }

        // *************** Before screen have this class **********************
        // val intent = Intent(this, MyStarActivity::class.java)
        // val intent = Intent(this, MyRewardsActivity::class.java)
        // val intent = Intent(this, StarAndLevelsOfCleanerActivity::class.java)

        recyclerView.layoutManager = GridLayoutManager(this,4,RecyclerView.VERTICAL,false)

        getListScheduleWs()
    }

    private val onClickCallBackLister = object : DayScheduleAdapter.ItemClickDaySchedule{
        override fun onItemClick(schedule: ServiceDayScheduleModel) {
            startHistoryService(schedule)
        }
    }

    private fun startHistoryService(schedule: ServiceDayScheduleModel?){
        val intent = Intent(this@ServicePropertyMainEmployeeListActivity, ServicePropertyHistoryEmployeeActivity::class.java)
        if (schedule != null)   intent.putExtra("schedule",schedule)
        resultLauncher.launch(intent)
    }

    private fun getListScheduleWs(){
        progressBar.visibility = View.VISIBLE
        ServiceScheduleEmployeeWs().getListScheduleCleaner(this, requestHashMap(), requestServiceWorkingStatusListener)
    }

    private fun requestHashMap() : HashMap<String, Any>{
        val hashMap : HashMap<String, Any> = HashMap()
        hashMap["user_id"] = userId
        hashMap["active_account_id"] = accountId
        hashMap["active_user_type"] = userType
        hashMap["operation_part"] = StaticUtilsKey.operation_part
        hashMap["date"] = dateToServer
        return hashMap
    }

    private val requestServiceWorkingStatusListener = object : ServiceScheduleEmployeeWs.ScheduleCleanerCallBackListener {
        @SuppressLint("NotifyDataSetChanged")
        override fun onLoadSuccessFull(listSchedule: ArrayList<ServiceDayScheduleModel>) {
            progressBar.visibility = View.GONE
            levelAndStarAdapter = DayScheduleAdapter(listSchedule, onClickCallBackLister)
            recyclerView.adapter = levelAndStarAdapter

            Utils.validateViewNoItemFound(this@ServicePropertyMainEmployeeListActivity, recyclerView, listSchedule.size <= 0)
        }

        override fun onLoadFail(message: String?) {
            progressBar.visibility = View.GONE
            Utils.customToastMsgError(this@ServicePropertyMainEmployeeListActivity, message, false)
        }

    }

    private var resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode == Activity.RESULT_OK) {
            val data: Intent? = result.data
            if (data != null) {
                val isBackSuccess = data.getBooleanExtra("is_result_success", false)
                if (isBackSuccess)  getListScheduleWs()
            }
        }
    }

    override fun onBackPressed() {
        onBackFish()
    }

    private fun onBackFish(){
        if (notificationType != ""){
            onStartSplashScreenActivity(this@ServicePropertyMainEmployeeListActivity, "already_notification")
            finishAffinity()
        } else {
            finish()
        }
    }
}