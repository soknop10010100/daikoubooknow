package com.eazy.daikou.ui.home.my_property.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.model.my_property.my_property.ImagesModel
import java.util.*

class ImageViewpagerAdapter(private val context: Context, private val imageList: ArrayList<ImagesModel>, private val typeItem: String, private val type: String, private val onCallBackListener : OnClickCallBackListener) : PagerAdapter(){

    private val mLayoutInflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun getCount(): Int {
        return imageList.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object` as LinearLayout
    }


    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val view: View =  mLayoutInflater.inflate(R.layout.upload_img_unit_model, container, false)
        val ivImages = view.findViewById<ImageView>(R.id.unitUploadImg)
        val iconDelete = view.findViewById<ImageView>(R.id.iconDelete)
        val playVideo = view.findViewById<ImageView>(R.id.playVideo)
        val item = imageList[position]

        Glide.with(context).load(item.file_path).into(ivImages)
        playVideo.visibility = if (type == "video") View.VISIBLE else View.GONE
        iconDelete.visibility = if (typeItem == "gallery") View.GONE else View.VISIBLE

        ivImages.setOnClickListener(CustomSetOnClickViewListener {
            onCallBackListener.onCallBackListener(item, type, "view_item")
        })

        iconDelete.setOnClickListener(CustomSetOnClickViewListener{
            onCallBackListener.onCallBackListener(item, type, "delete_item")
        })

        Objects.requireNonNull(container).addView(view)
        return view

    }

    override fun destroyItem(viewGroup: ViewGroup, position: Int, `object`: Any) {
        viewGroup.removeView(`object` as LinearLayout)
    }

    interface OnClickCallBackListener {
        fun onCallBackListener(urlSt : ImagesModel, type : String, action : String)
    }

}