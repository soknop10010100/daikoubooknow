package com.eazy.daikou.ui.home.book_now.booking_hotel.travel_talk

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.*
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.cardview.widget.CardView
import androidx.fragment.app.FragmentTransaction
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.base.BaseCameraActivity
import com.eazy.daikou.base.MockUpData
import com.eazy.daikou.request_data.request.book_now_ws.RequestHashMapData
import com.eazy.daikou.helper.AppAlertCusDialog
import com.eazy.daikou.helper.DateUtil
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.booking_hotel.HotelTravelTalkModel
import com.eazy.daikou.model.booking_hotel.TravelTalkReplyDetail
import com.eazy.daikou.ui.ViewPagerItemImagesAdapter
import com.google.android.material.tabs.TabLayout

class HotelTravelTalkDetailActivity : BaseActivity() {

    private lateinit var progressBar: ProgressBar
    private var threadId = ""
    private lateinit var descriptionReplyTv : EditText

    private var userId = ""
    private var urlImage = ""
    private lateinit var imageLayout : CardView

    private lateinit var replyLayout : LinearLayout
    private lateinit var bottomReplyLayout : LinearLayout
    private var actionFrom = ""

    //Reply Sub
    private var typeReply = ""
    private var replyId = ""
    private lateinit var hotelTravelTalkModel : HotelTravelTalkModel
    private lateinit var btnLike : ImageView
    private lateinit var totalShare : TextView
    private lateinit var btnSendComment : ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hotel_travel_talk_detail)

        initView()

        initAction()

    }

    private fun initView(){
        progressBar = findViewById(R.id.progressItem)
        descriptionReplyTv = findViewById(R.id.descriptionReplyTv)
        imageLayout = findViewById(R.id.imageLayout)
        replyLayout = findViewById(R.id.replyLayout)
        bottomReplyLayout = findViewById(R.id.bottomReplyLayout)
        btnLike = findViewById(R.id.btnLike)
        totalShare = findViewById(R.id.totalShare)

        Utils.customOnToolbar(this, resources.getString(R.string.post_detail)){finish()}

    }

    private fun initAction(){
        // Set Value Profile Item
        if (intent != null && intent.hasExtra("id")){
            threadId = intent.getStringExtra("id") as String

            reLoadFragment()
        }

        if (intent != null && intent.hasExtra("action")){
            actionFrom = intent.getStringExtra("action") as String

            if (actionFrom == "comment_action"){
                showKeyBoardReply()
            }
        }

        userId = MockUpData.getEazyHotelUserId(UserSessionManagement(this))

        // Do action click
        findViewById<ImageView>(R.id.cameraImg).setOnClickListener(CustomSetOnClickViewListener{
            resultLauncher.launch(Intent(this@HotelTravelTalkDetailActivity, BaseCameraActivity::class.java))
        })

        // Clear image upload
        validateImage(false)    // First Screen
        findViewById<ImageView>(R.id.btn_clear).setOnClickListener(CustomSetOnClickViewListener{ validateImage(false) })

        btnSendComment = findViewById(R.id.sendImg)
        btnSendComment.setOnClickListener(CustomSetOnClickViewListener{
            if (AppAlertCusDialog.isSuccessLoggedIn(this@HotelTravelTalkDetailActivity)) {
                if (typeReply != "" && replyId != "") {
                    onReplyComment(typeReply, replyId)
                } else {
                    onReplyComment("thread", "")
                }
            }
        })

        isEnableButton(false)
        descriptionReplyTv.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun afterTextChanged(editable: Editable) {
                if (editable.toString().isNotEmpty()){
                    isEnableButton(true)
                } else {
                    isEnableButton(false)
                }
            }
        })
    }

    private fun isEnableButton(isEnable : Boolean){
        btnSendComment.isEnabled = isEnable
        btnSendComment.setColorFilter(if (isEnable) Utils.getColor(this, R.color.book_now_secondary) else Utils.getColor(this, R.color.gray))
    }

    private fun onReplyComment(type : String, replySubId : String){
        val hashMap = RequestHashMapData.requestDataReply(userId, type, threadId, replySubId, descriptionReplyTv.text.trim().toString(), urlImage)
        RequestHashMapData.requestServiceLikeUnLike(this@HotelTravelTalkDetailActivity, progressBar,"do_reply_comment", hashMap, object : RequestHashMapData.Companion.OnSuccessEditStatusListener{
            override fun onSuccess(msg: String) {
                progressBar.visibility = View.GONE
                Utils.customToastMsgError(this@HotelTravelTalkDetailActivity, msg, true)

                // Reload Screen
                descriptionReplyTv.clearFocus()
                descriptionReplyTv.setText("")
                Utils.hideKeyboard(this@HotelTravelTalkDetailActivity)
                validateImage(false)

                replyId = ""
                typeReply = ""

                reLoadFragment()
            }
        })
    }

    private fun validateImage(isHaveImage : Boolean){
        if (isHaveImage){
            imageLayout.visibility = View.VISIBLE
            Glide.with(this).load(BaseCameraActivity.convertByteToBitmap(urlImage)).into(findViewById(R.id.image))
        } else {
            urlImage = ""
            imageLayout.visibility = View.GONE
        }

        // Set Bottom Layout
        Utils.widthHeightLayout(bottomReplyLayout) { _, height ->
             Utils.setMarginBottomOnCoordinatorLayout(replyLayout, height)
        }
    }

    private val resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
        if (result.resultCode == Activity.RESULT_OK) {
            if (result.data != null){
                val data = result.data
                if (data!!.hasExtra("image_path")) {
                    val imagePath = data.getStringExtra("image_path")
                    if (imagePath != null) {
                        urlImage = imagePath
                    }
                }
                if (data.hasExtra("select_image")) {
                    val imagePath = data.getStringExtra("select_image")
                    if (imagePath != null) {
                        urlImage = imagePath
                    }
                }
                if (urlImage != ""){
                    Utils.logDebug("jeeeeeeeeeeeeeeee", urlImage)
                    validateImage(true)
                }
            }
        }
    }

    @SuppressLint("DetachAndAttachSameFragment")
    private fun reLoadFragment() {
        val hotelHomeListHotelFragment = HotelReplyCommentItemFragment(threadId, onCallBackListener)
        val ft: FragmentTransaction = supportFragmentManager.beginTransaction()
        ft.replace(R.id.nav_fragment, hotelHomeListHotelFragment)
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        ft.addToBackStack(null)
        ft.commit()
    }

    private val onCallBackListener = object : HotelReplyCommentItemFragment.InitCallBackListener {
        override fun onCallBack(item: HotelTravelTalkModel) {
            hotelTravelTalkModel = item
            setValue(item)
        }

        override fun onDoReplyCallBack(item: TravelTalkReplyDetail) {
            showKeyBoardReply()
            typeReply = "reply"
            replyId = item.id!!
        }
    }

    private fun setValue(hotelTravelTalkModel : HotelTravelTalkModel){
        progressBar.visibility = View.GONE

        val subjectTv : TextView = findViewById(R.id.subjectTv)
        val descriptionTv : TextView = findViewById(R.id.descriptionTv)
        val btnLikeLayout : LinearLayout = findViewById(R.id.btnLikeLayout)
        val btnShareLayout : LinearLayout = findViewById(R.id.btnShareLayout)
        val commentLayout : LinearLayout = findViewById(R.id.commentLayout)

        //User Profile
        if (hotelTravelTalkModel.created_by != null){
            setValueTxt(findViewById(R.id.nameTv), hotelTravelTalkModel.created_by!!.name)
            Glide.with(this).load(if (hotelTravelTalkModel.created_by!!.image != null) hotelTravelTalkModel.created_by!!.image else R.drawable.ic_my_profile).into(findViewById(R.id.profile_image))

            setValueTxt(findViewById(R.id.numPost), String.format("%s %s", hotelTravelTalkModel.created_by!!.total_posts, resources.getString(R.string.post)))
            setValueTxt(findViewById(R.id.numLike), String.format("%s %s", hotelTravelTalkModel.created_by!!.total_likes, resources.getString(R.string.like)))
            setValueTxt(findViewById(R.id.numComment), String.format("%s %s", hotelTravelTalkModel.created_by!!.total_comments, resources.getString(R.string.comment)))
        } else {
            Utils.setValueOnText(findViewById(R.id.nameTv), "- - -")
            Glide.with(this).load(if (hotelTravelTalkModel.created_by!!.image != null) hotelTravelTalkModel.created_by!!.image else R.drawable.ic_my_profile).into(findViewById(R.id.profile_image))

            setValueTxt(findViewById(R.id.numPost), "")
            setValueTxt(findViewById(R.id.numLike), "")
            setValueTxt(findViewById(R.id.numComment), "")
        }

        // Content Body
        Utils.setTextHtml(subjectTv, hotelTravelTalkModel.title)

        setValueTxt(descriptionTv, hotelTravelTalkModel.description)
        subjectTv.setOnClickListener {
            if (subjectTv.maxLines == 2) {
                subjectTv.maxLines = 50
            } else {
                subjectTv.maxLines = 2
            }
        }
        descriptionTv.setOnClickListener {
            if (descriptionTv.maxLines == 3) {
                descriptionTv.maxLines = 50
            } else {
                descriptionTv.maxLines = 3
            }
        }

        if (hotelTravelTalkModel.category != null){
            setValueTxt(findViewById(R.id.typeTv), hotelTravelTalkModel.category!!.name)
        } else {
            setValueTxt(findViewById(R.id.typeTv), "")
        }

        val durationTv = findViewById<TextView>(R.id.durationTv)
        if (hotelTravelTalkModel.created_at != null){
            durationTv.text = DateUtil.getDurationDate(durationTv.context, hotelTravelTalkModel.created_at)
        } else {
            durationTv.text = "- - -"
        }


        setValueTxt(totalShare, String.format("%s %s", hotelTravelTalkModel.total_shares, resources.getString(R.string.share)))
        setValueTxt(findViewById(R.id.totalComment), String.format("%s %s", hotelTravelTalkModel.total_replies, resources.getString(R.string.comment)))
        setValueTxt(findViewById(R.id.totalLike), String.format("%s %s", hotelTravelTalkModel.total_likes, resources.getString(R.string.like)))

        Glide.with(btnLike).load(if (hotelTravelTalkModel.is_user_liked)    R.drawable.ic_like_fb else R.drawable.ic_unlike_fb).into(btnLike)

        btnLikeLayout.setOnClickListener {
            if (AppAlertCusDialog.isSuccessLoggedIn(this@HotelTravelTalkDetailActivity)) {
                // On request service
                val hashMap = RequestHashMapData.requestDataLike(userId, "thread", hotelTravelTalkModel.id!!, if (hotelTravelTalkModel.is_user_liked) "unlike" else "like")
                RequestHashMapData.requestServiceLikeUnLike(this@HotelTravelTalkDetailActivity, progressBar,"do_like", hashMap, onCallBackSuccess)
            }
        }

        btnShareLayout.setOnClickListener(CustomSetOnClickViewListener{
            Utils.shareLinkMultipleOption(this@HotelTravelTalkDetailActivity, hotelTravelTalkModel.share_link, "")

            val hashmap = RequestHashMapData.requestDataShare("thread", threadId, userId)
            RequestHashMapData.requestServiceLikeUnLike(this, progressBar,"do_share", hashmap, onCallBackSuccess)
        })

        commentLayout.setOnClickListener(CustomSetOnClickViewListener{ showKeyBoardReply() })

        // images
        setViewPagerImage(hotelTravelTalkModel.images)

    }

    private val onCallBackSuccess = object : RequestHashMapData.Companion.OnSuccessEditStatusListener{
        override fun onSuccess(msg: String) {
            if (msg.equals("Shared.", false)){
                var numShare = hotelTravelTalkModel.total_shares!!.toInt()  // Profile user
                numShare++
                hotelTravelTalkModel.total_shares = numShare.toString()
                setValueTxt(totalShare, String.format("%s %s", hotelTravelTalkModel.total_shares, resources.getString(R.string.share)))
            }
            // Do Like
            else {
                // On set like / unlike item
                var numLike = hotelTravelTalkModel.created_by!!.total_likes!!.toInt()   // Profile
                var numLikeItem = hotelTravelTalkModel.total_likes!!.toInt()  // Item Like
                if (hotelTravelTalkModel.is_user_liked){
                    hotelTravelTalkModel.is_user_liked = false
                    numLike -= 1
                    numLikeItem -= 1
                } else {
                    hotelTravelTalkModel.is_user_liked = true
                    numLike += 1
                    numLikeItem += 1
                }
                hotelTravelTalkModel.created_by!!.total_likes = numLike.toString()
                hotelTravelTalkModel.total_likes = numLikeItem.toString()

                // Update
                Glide.with(btnLike).load(if (hotelTravelTalkModel.is_user_liked)    R.drawable.ic_like_fb else R.drawable.ic_unlike_fb).into(btnLike)
                setValueTxt(findViewById(R.id.numLike), String.format("%s %s", hotelTravelTalkModel.created_by!!.total_likes, resources.getString(R.string.like)))
                setValueTxt(findViewById(R.id.totalLike), String.format("%s %s", hotelTravelTalkModel.total_likes, resources.getString(R.string.like)))

                // Back List Post
                val intent = Intent()
                intent.putExtra("hotel_travel_item", hotelTravelTalkModel)
                setResult(RESULT_OK, intent)
            }
        }

    }

    private fun showKeyBoardReply(){
        Utils.showKeyboardV2(this)
        descriptionReplyTv.isFocusableInTouchMode = true
        descriptionReplyTv.isFocusable = true
        descriptionReplyTv.requestFocus()
    }

    private fun setValueTxt(textView: TextView, value: String?) {
        textView.text = value ?: " "
    }

    private fun setViewPagerImage(image : ArrayList<String>){
        val viewPager = findViewById<ViewPager>(R.id.viewPagers)
        val tabLayout = findViewById<TabLayout>(R.id.tab_layout)
        if (image.size > 0 ) {
            findViewById<CardView>(R.id.linearPhoto).visibility = View.VISIBLE
            val viewpagerAdapter = ViewPagerItemImagesAdapter(this, image)
            viewPager.adapter = viewpagerAdapter
            tabLayout.setupWithViewPager(viewPager, true)
        } else {
            findViewById<CardView>(R.id.linearPhoto).visibility = View.GONE
        }
    }

    override fun onBackPressed() {
        finish()
    }
}