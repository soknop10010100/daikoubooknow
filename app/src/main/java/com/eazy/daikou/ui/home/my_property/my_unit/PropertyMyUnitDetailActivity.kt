package com.eazy.daikou.ui.home.my_property.my_unit

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.base.BaseCameraActivity
import com.eazy.daikou.request_data.request.data_record.DataRecordWS
import com.eazy.daikou.request_data.request.my_property_ws.my_unit_ws.MyUnitPropertyWS
import com.eazy.daikou.helper.AppAlertCusDialog
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.helper.Utils.setValueOnText
import com.eazy.daikou.model.my_property.my_property.*
import com.eazy.daikou.model.my_property.payment_schedule_my_unit.DeveloperInfo
import com.eazy.daikou.ui.QRCodeAlertDialog
import com.eazy.daikou.ui.home.my_property.VideoPlayerActivity
import com.eazy.daikou.ui.home.my_property.adapter.ImageViewpagerAdapter
import java.io.File
import java.util.*

class PropertyMyUnitDetailActivity : BaseActivity() {

    private lateinit var linearPayment : LinearLayout
    private lateinit var progressBar: ProgressBar
    private lateinit var viewPagerPhoto: ViewPager
    private lateinit var viewPagerVideo: ViewPager
    private lateinit var iconAddPhoto : LinearLayout
    private lateinit var iconPlusVideo : LinearLayout
    private lateinit var iconLayout : LinearLayout
    private lateinit var iconDocument : LinearLayout
    private lateinit var iconDeveloperInfo : LinearLayout
    private lateinit var iconQR : LinearLayout
    private lateinit var paymentUnitLayout : LinearLayout
    private lateinit var operationPaymentLayout : LinearLayout
    private lateinit var leaseAgreementLayout : LinearLayout
    private lateinit var textPrivateArea : TextView
    private lateinit var textTotalArea : TextView

    private var actionType = ""
    private var unitId = ""
    private var qrCode = ""
    private var layoutUrl = ""
    private var unitNo = ""

    // Payment
    private var developerInfoList : ArrayList<DeveloperInfo> = ArrayList()
    private val listDocument : ArrayList<DocumentModel> = ArrayList()
    private var alertPopupDialog : androidx.appcompat.app.AlertDialog ?= null
    private var progressBarDialog : ProgressBar ?= null

    // Unit Detail
    private var unitDetailModel : Type?= null
    private var floorNo : String?= null
    private var unitDetailItemModel : PropertyMyUnitModel?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_property_list_detail)

        initView()

        initData()

        initAction()

    }

    private fun initView(){
        findViewById<ImageView>(R.id.btnBack).setOnClickListener { finish() }
        linearPayment = findViewById(R.id.linearPayment)
        progressBar = findViewById(R.id.progressItem)
        viewPagerPhoto = findViewById(R.id.viewPagerPhoto)
        viewPagerVideo = findViewById(R.id.viewPagerVideo)
        iconAddPhoto = findViewById(R.id.iconAddPhoto)
        iconPlusVideo = findViewById(R.id.iconPlusVideo)
        iconLayout = findViewById(R.id.iconLayout)
        iconDocument = findViewById(R.id.iconDocument)
        iconDeveloperInfo = findViewById(R.id.iconDeveloperInfo)
        iconQR = findViewById(R.id.iconQR)
        paymentUnitLayout = findViewById(R.id.paymentUnitLayout)
        operationPaymentLayout = findViewById(R.id.operationPaymentLayout)
        leaseAgreementLayout = findViewById(R.id.leaseAgreementLayout)
        textPrivateArea = findViewById(R.id.textPrivateArea)
        textTotalArea = findViewById(R.id.textTotalArea)
    }

    private fun initData(){
        if (intent != null && intent.hasExtra("id")){
            unitId = intent.getStringExtra("id").toString()
        }

        if (intent != null && intent.hasExtra("action_type")){
            actionType = intent.getStringExtra("action_type") as String
        }

        if (actionType == "data_record_unit"){
            linearPayment.visibility = View.GONE
            iconAddPhoto.visibility = View.GONE
            iconPlusVideo.visibility = View.GONE
        }
    }

    private fun initAction(){

        requestServiceDetailWs()

        iconLayout.setOnClickListener(CustomSetOnClickViewListener {
            if (layoutUrl != "") {
                Utils.openZoomImage(this@PropertyMyUnitDetailActivity, layoutUrl, ArrayList<String>())
            } else {
                AppAlertCusDialog.underConstructionDialog(this, resources.getString(R.string.not_available))
            }
        })

        iconDocument.setOnClickListener(CustomSetOnClickViewListener{
            val intent = Intent(this@PropertyMyUnitDetailActivity, MyUnitDocumentDeveloperActivity::class.java).apply {
                putExtra("action", "document_info")
                putExtra("document_list", listDocument)
            }
            startActivity(intent)
        })

        iconDeveloperInfo.setOnClickListener(CustomSetOnClickViewListener{
            val intent = Intent(this@PropertyMyUnitDetailActivity, MyUnitDocumentDeveloperActivity::class.java).apply {
                putExtra("action", "developer_info")
                putExtra("developer_list", developerInfoList)
            }
            startActivity(intent)
        })

        iconQR.setOnClickListener(CustomSetOnClickViewListener{
            if (qrCode != ""){
                val qrCodeImageBitmap = Utils.getQRCodeImage512(qrCode)
                val qrCodeAlertDialog = QRCodeAlertDialog(this, qrCodeImageBitmap)
                qrCodeAlertDialog.show()
            } else {
                AppAlertCusDialog.underConstructionDialog(this, resources.getString(R.string.not_available))
            }
        })

        paymentUnitLayout.setOnClickListener(CustomSetOnClickViewListener{
            if (unitDetailItemModel == null)    return@CustomSetOnClickViewListener
            val intent = Intent(this@PropertyMyUnitDetailActivity, PropertyPaymentMyUnitActivity::class.java).apply {
                putExtra("action", "payment_schedule")
                putExtra("unit_no", unitNo)
                putExtra("unit_detail_Item_model", unitDetailItemModel)
            }

            activityLauncher.launch(intent) { result: ActivityResult ->
                if (result.resultCode == RESULT_OK) {
                    // From menu home
                    if (result.data != null){
                        val menu = result.data!!.getStringExtra("menu")
                        val intent2 = Intent()
                        intent2.putExtra("menu", menu)
                        setResult(RESULT_OK, intent2)
                        finish()
                    }
                }
            }
        })

        operationPaymentLayout.setOnClickListener(CustomSetOnClickViewListener{
            val intent = Intent(this@PropertyMyUnitDetailActivity, PropertyOperationPaymentActivity::class.java)
            intent.putExtra("action", "operation_schedule")
            startActivity(intent)
        })

        leaseAgreementLayout.setOnClickListener(CustomSetOnClickViewListener{

        })

        iconPlusVideo.setOnClickListener(CustomSetOnClickViewListener{
            val intent = Intent(this, BaseCameraActivity::class.java).apply {
                putExtra("action", "video")
            }
            resultLauncher.launch(intent)
        })

        iconAddPhoto.setOnClickListener( CustomSetOnClickViewListener{
            resultLauncher.launch(Intent(this, BaseCameraActivity::class.java))
        })

        findViewById<LinearLayout>(R.id.seeMoreUnitLayout).setOnClickListener(CustomSetOnClickViewListener{
            val detailUnitLayout = DetailUnitBottomSheetFragment.newInstance(floorNo, unitDetailModel)
            detailUnitLayout.show(supportFragmentManager, "fragment")
        })

    }

    private fun requestServiceDetailWs(){
        progressBar.visibility = View.VISIBLE
        if (actionType == "data_record_unit"){
            DataRecordWS().getDetailUnitInterestingAndRecommendedWS(this, unitId, callBackUnitDataRecordListener)
        } else{
            MyUnitPropertyWS().getDetailPropertyMyUnitWS(this, unitId, callBackListener)
        }
    }

    private val callBackUnitDataRecordListener : DataRecordWS.OnClickBackDetailUnitListener = object : DataRecordWS.OnClickBackDetailUnitListener{
        override fun onSuccess(detailPropertyMyUnit: PropertyMyUnitModel) {
            progressBar.visibility = View.GONE

            initDataLayout(detailPropertyMyUnit)
        }

        override fun onFailed(error: String) {
            progressBar.visibility = View.GONE
            Utils.customToastMsgError(this@PropertyMyUnitDetailActivity, error, false)
        }

    }

    private val callBackListener : MyUnitPropertyWS.CallBackDetailPropertyMyUnit = object : MyUnitPropertyWS.CallBackDetailPropertyMyUnit{
        override fun onSuccessFull(detailPropertyModel: PropertyMyUnitModel) {
            progressBar.visibility = View.GONE

            unitDetailItemModel = detailPropertyModel

            initDataLayout(detailPropertyModel)
        }

        override fun onSuccessUploadImgVdo(msg: String) {
            progressBar.visibility = View.GONE
            Utils.customToastMsgError(this@PropertyMyUnitDetailActivity, msg, true)

            if (alertPopupDialog != null)   alertPopupDialog!!.dismiss()  // Delete image / video
            if (progressBarDialog != null)   progressBarDialog!!.visibility = View.GONE  // Delete image / video

            requestServiceDetailWs()
        }

        override fun onFailed(error: String) {
          progressBar.visibility = View.GONE
           Utils.customToastMsgError(this@PropertyMyUnitDetailActivity, error, false)

            if (progressBarDialog != null)   progressBarDialog!!.visibility = View.GONE  // Delete image / video
        }
    }

    private fun initDataLayout(detailProperty : PropertyMyUnitModel){

        if (detailProperty.account_category == "is_borey"){
            textPrivateArea.text = getText(R.string.building_size)
            textTotalArea.text = getText(R.string.land_size)
        }

        // Unit Detail
        if (detailProperty.type != null){
            setValueOnText(findViewById(R.id.unitTypeTv), detailProperty.type!!.name)
            setValueOnText(findViewById(R.id.roomStyleTv), detailProperty.type!!.style)
            setValueOnText(findViewById(R.id.bedRoomTv), detailProperty.type!!.total_bedroom)
            setValueOnText(findViewById(R.id.livingRoomTv), detailProperty.type!!.total_living_room)
            setValueOnText(findViewById(R.id.bathroomTv), detailProperty.type!!.total_bathroom)
            setValueOnText(findViewById(R.id.maidRoomTv), detailProperty.type!!.total_maid_room)
            setValueOnText(findViewById(R.id.totalAreaTv), detailProperty.type!!.total_area_sqm)
            setValueOnText(findViewById(R.id.privateAreaTv), detailProperty.type!!.total_private_area)
            setValueOnText(findViewById(R.id.commentAreaTv), detailProperty.type!!.total_common_area)

            // Unit detail
            val directionTv = findViewById<TextView>(R.id.directionTv)
            directionTv.text = ""
            for (i in detailProperty.type!!.direction) {
                directionTv.append(
                    String.format(
                        "%s %s",
                        detailProperty.type!!.direction[0].uppercase(),
                        ", "
                    )
                )
            }

            unitDetailModel = detailProperty.type

            if (detailProperty.type!!.layout_photo != null) layoutUrl = detailProperty.type!!.layout_photo!!
        }

        setValueOnText(findViewById(R.id.floorTv), detailProperty.floor_no)

        // Unit
        setValueOnText(findViewById(R.id.unitNameTv), detailProperty.name)

        // Owner Info
        setValueOnText(findViewById(R.id.customerNameTv), detailProperty.user_name)
        setValueOnText(findViewById(R.id.customerPhoneTv), detailProperty.user_phone)
        setValueOnText(findViewById(R.id.channelNameTv), detailProperty.channel_name)
        setValueOnText(findViewById(R.id.brokerNameTv), detailProperty.broker_name)
        setValueOnText(findViewById(R.id.agencyNameTv), detailProperty.agency_name)
        setValueOnText(findViewById(R.id.supportingTeamTv), detailProperty.supporting_team_name)

        // Payment
       // setValueOnText(findViewById(R.id.buyDateTv), Utils.formatDateFromString("yyyy-MM-dd", "dd-MMM-yyyy",detailProperty.purchase_date))
        setValueOnText(findViewById(R.id.buyDateTv), if(detailProperty.purchase_date != null) Utils.formatDateFromString("yyyy-MM-dd", "dd-MMM-yyyy", detailProperty.purchase_date) else ". . .")
        setValueOnText(findViewById(R.id.originalPriceTv), detailProperty.price)
        setValueOnText(findViewById(R.id.voucherAmountTv), detailProperty.voucher_amount)
        setValueOnText(findViewById(R.id.discountTv), detailProperty.discount)
        setValueOnText(findViewById(R.id.paymentOptionTv), detailProperty.payment_option)
        setValueOnText(findViewById(R.id.paymentTermTypeTv), detailProperty.payment_term_type)
        setValueOnText(findViewById(R.id.sellingPriceTv), detailProperty.sale_price)

        // Image Unit
        val coverImageUnit = findViewById<ImageView>(R.id.coverImageUnit)
        when {
            detailProperty.my_images.size > 0 -> {
                imageViewpagerMyUnit(viewPagerPhoto,"image", "my_image", detailProperty.my_images)
                Glide.with(this).load(detailProperty.my_images[0].file_path).into(coverImageUnit)
                findViewById<TextView>(R.id.noImage).visibility = View.GONE
            }
            detailProperty.type != null -> {
                imageViewpagerMyUnit(viewPagerPhoto,"image", "gallery", detailProperty.type!!.images)
                if (detailProperty.type!!.images.size > 0){
                    Glide.with(this).load(detailProperty.type!!.images[0].file_path).into(coverImageUnit)
                } else {
                    Glide.with(this).load(R.drawable.no_image).into(coverImageUnit)
                }
                findViewById<TextView>(R.id.noImage).visibility = if (detailProperty.type!!.images.size > 0) View.GONE else View.VISIBLE
            }
            else -> {
                findViewById<TextView>(R.id.noImage).visibility = View.VISIBLE
                Glide.with(this).load(R.drawable.no_image).into(coverImageUnit)
            }
        }

        // Video Unit
        imageViewpagerMyUnit(viewPagerVideo, "video", "my_video",detailProperty.my_videos)
        findViewById<TextView>(R.id.noVideo).visibility = if (detailProperty.my_videos.size > 0)  View.GONE else View.VISIBLE

        // List Development Info
        developerInfoList.clear()
        developerInfoList.addAll(detailProperty.developers)

        // List Document Info
        listDocument.addAll(detailProperty.file_uploads)

        qrCode = Utils.validateNullValue(detailProperty.qr_code)
        unitNo = Utils.validateNullValue(detailProperty.name)
        floorNo = Utils.validateNullValue(detailProperty.floor_no)
    }

    private fun imageViewpagerMyUnit(viewPager: ViewPager, type : String, typeOfItem : String, imageList : ArrayList<ImagesModel>){
        val adapterViewpagerPhoto = ImageViewpagerAdapter(this, imageList, typeOfItem, type, object : ImageViewpagerAdapter.OnClickCallBackListener{
            override fun onCallBackListener(urlSt: ImagesModel, type: String, action: String) {
                if (type == "image"){
                    if (action == "view_item"){
                        Utils.openZoomImage(this@PropertyMyUnitDetailActivity, "", imageList)
                    } else if (action == "delete_item"){
                        deleteImgVdo(urlSt.id!!)
                    }
                } else {
                    if (action == "view_item"){
                        val intent = Intent(this@PropertyMyUnitDetailActivity, VideoPlayerActivity::class.java)
                        intent.putExtra("video", urlSt.file_path)
                        startActivity(intent)
                    } else if (action == "delete_item"){
                        deleteImgVdo(urlSt.id!!)
                    }
                }
            }

        })
        viewPager.adapter = adapterViewpagerPhoto

        if (type == "image"){
            findViewById<TextView>(R.id.photoViewTv).text = if (imageList.size > 0) String.format("%s %s %s %s", resources.getString(R.string.photo_view), "(", imageList.size, ")" ) else resources.getString(R.string.photo_view)
        } else {
            findViewById<TextView>(R.id.videoViewTv).text = if (imageList.size > 0) String.format("%s %s %s %s", resources.getString(R.string.video_view), "(", imageList.size, ")" ) else resources.getString(R.string.photo_view)
        }
    }

    private fun deleteImgVdo(id : String){
        AppAlertCusDialog.customAlertDialogYesNo(
            this,
            resources.getString(R.string.confirm),
            resources.getString(R.string.are_you_sure_to_delete),
            resources.getString(R.string.ok),
            resources.getString(R.string.cancel)
        ) { alertDialog, progressBar, _, _ ->
            progressBar.visibility = View.VISIBLE
            alertPopupDialog = alertDialog
            progressBarDialog = progressBar

            deleteImgVideo( id, "delete")
        }
    }

    private val resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode == Activity.RESULT_OK) {
            val data = result.data
            if (data != null){
                var imagePath = ""
                when {
                    data.hasExtra("image_path_uri") -> {
                        val imagePathUri = data.getStringExtra("image_path_uri").toString()
                        val actualFile =  File(imagePathUri)
                        updateVideo("image", actualFile)
                    }
                    data.hasExtra("select_image_uri") -> {
                        val imagePathSelect = data.getStringExtra("select_image_uri").toString()
                        val actualImage = Utils.from(this, Uri.parse(imagePathSelect))
                        updateVideo("image", actualImage)
                    }
                    // Video Take
                    data.hasExtra("video_path") -> {
                        val mCameraFileName = data.getStringExtra("video_path").toString()
                        val actualFile =  File(mCameraFileName)
                        updateVideo("video", actualFile)
                    }
                    data.hasExtra("select_video") -> {
                        val uriString = data.getStringExtra("select_video").toString()

                        val actualImage = Utils.from(this, Uri.parse(uriString))

                        updateVideo("video", actualImage)
                    }
                }
                Utils.logDebug("jeeeeeeeeeeeeeeeeeeeeeeeeee", imagePath)
            }
        }
    }


    private fun deleteImgVideo( idFile : String, action : String){
        val hashMap = HashMap<String, Any>()
            hashMap["file_id"] = idFile

        hashMap["unit_id"] = unitId
        progressBar.visibility = View.VISIBLE
        MyUnitPropertyWS().uploadImgVideoUnitWS(this, action, hashMap, callBackListener)
    }

    private fun updateVideo(fileType : String, fileSt : File){
        val hashMap = HashMap<String, Any>()
        hashMap["file_type"] = fileType
        hashMap["file"] = fileSt.absoluteFile
        hashMap["unit_id"] = unitId
        progressBar.visibility = View.VISIBLE
        MyUnitPropertyWS().uploadVideoUnitWS(this, hashMap, callBackListener)
    }

}