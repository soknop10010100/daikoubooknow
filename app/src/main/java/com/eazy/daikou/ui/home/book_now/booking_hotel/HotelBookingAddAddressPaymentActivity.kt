package com.eazy.daikou.ui.home.book_now.booking_hotel

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.cardview.widget.CardView
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.base.MockUpData
import com.eazy.daikou.request_data.request.book_now_ws.BookingHotelWS
import com.eazy.daikou.request_data.request.book_now_ws.RequestHashMapData.Companion.getListOnlinePayment
import com.eazy.daikou.helper.*
import com.eazy.daikou.model.booking_hotel.ExtraPrice
import com.eazy.daikou.model.booking_hotel.PaymentSuccessModel
import com.eazy.daikou.model.booking_hotel.TicketType
import com.eazy.daikou.ui.home.book_now.booking_hotel.adapter.PaymentMethodOnlineAdapter
import com.eazy.daikou.helper.web.WebPayActivity
import libs.mjn.prettydialog.PrettyDialog
import java.text.SimpleDateFormat

class HotelBookingAddAddressPaymentActivity : BaseActivity() {

    private lateinit var firstName : EditText
    private lateinit var lastName : EditText
    private lateinit var emailTv : EditText
    private lateinit var phoneTv : EditText
    private lateinit var specialRequirement : EditText
    private lateinit var countryEdt : TextView
    private lateinit var cityEd : EditText
    private lateinit var progressBar: ProgressBar
    private lateinit var imgHotelTv : ImageView
    private lateinit var hotelNameTv : TextView
    private lateinit var totalPriceTv : TextView
    private var bookingId = ""
    private var adults = ""
    private var children = ""
    private var totalPrice = ""
    private var startDate : String = ""
    private var endDate : String = ""
    private lateinit var adultsTv : TextView
    private lateinit var childrenTv : TextView
    private lateinit var btnSubmit : TextView
    private lateinit var rangDateTv : TextView

    private lateinit var applyCouponCode : LinearLayout
    private lateinit var iconAppliedCoupon : ImageView
    private lateinit var couponTv : TextView
    private var isAlreadyAddCoupon = false
    private var alertDialogBackUp: AlertDialog? = null
    private var couponCode = ""
    private lateinit var totalAllPriceTv : TextView
    private lateinit var couponPriceTv : TextView
    private lateinit var subTotalTv : TextView

    //Event
    private lateinit var durationTv : TextView
    private var category : String = ""
    private var duration : String = ""
    private var dateFromToTour: String = ""
    private var itemEventActivityModelList : ArrayList<TicketType> = ArrayList()
    private var extraPriceList = ArrayList<ExtraPrice>()
    private var serviceFeeList = ArrayList<ExtraPrice>()
    private lateinit var recyclerViewPayment : RecyclerView

    private var hashMap: HashMap<String, Any> = HashMap()
    private var kessPayment = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_address_before_booking_hetel)

        initView()

        initData()

        initAction()

    }

    private fun initView(){
        Utils.customOnToolbar(this, resources.getString(R.string.payment).uppercase()){finish()}

        firstName = findViewById(R.id.firstNameTv)
        lastName = findViewById(R.id.lastNameTv)
        emailTv = findViewById(R.id.emailTv)
        phoneTv = findViewById(R.id.phoneTv)
        specialRequirement = findViewById(R.id.specialRequirement)
        countryEdt = findViewById(R.id.countryEdt)
        cityEd = findViewById(R.id.cityEd)
        progressBar = findViewById(R.id.progressItem)
        progressBar.visibility = View.GONE
        imgHotelTv = findViewById(R.id.imgHotelTv)
        hotelNameTv = findViewById(R.id.hotelNameTv)
        totalPriceTv = findViewById(R.id.totalPriceTv)
        adultsTv = findViewById(R.id.adultsTv)
        childrenTv = findViewById(R.id.childrenTv)
        btnSubmit = findViewById(R.id.text_category)
        rangDateTv = findViewById(R.id.rangDateTv)

        applyCouponCode = findViewById(R.id.applyCouponCode)
        iconAppliedCoupon = findViewById(R.id.iconAppliedCoupon)
        couponTv = findViewById(R.id.couponTv)

        totalAllPriceTv = findViewById(R.id.totalAllPriceTv)
        couponPriceTv = findViewById(R.id.couponPriceTv)
        subTotalTv = findViewById(R.id.subTotalTv)

        //Event
        durationTv = findViewById(R.id.durationTv)
        recyclerViewPayment = findViewById(R.id.recyclerView)
    }

    private fun initData(){
        bookingId = GetDataUtils.getDataFromString("booking_id", this)
        adults = GetDataUtils.getDataFromString("adults", this)
        children = GetDataUtils.getDataFromString("children", this)
        totalPrice = GetDataUtils.getDataFromString("price", this)
        startDate = GetDataUtils.getDataFromString("start_date", this)
        endDate = GetDataUtils.getDataFromString("end_date", this)
        category = GetDataUtils.getDataFromString("category", this)
        duration = GetDataUtils.getDataFromString("duration", this)
        dateFromToTour = GetDataUtils.getDataFromString("date_form_to", this)
        kessPayment = GetDataUtils.getDataFromString("kess_payment", this)

        val name = GetDataUtils.getDataFromString("hotel_name", this)
        val image = GetDataUtils.getDataFromString("hotel_image", this)
        Glide.with(imgHotelTv).load(image).into(imgHotelTv)
        hotelNameTv.text = name

        if(intent != null && intent.hasExtra("hashmap_draft_data")){
            hashMap = intent.getSerializableExtra("hashmap_draft_data") as HashMap<String, Any>
        }

        if(intent != null && intent.hasExtra("extra_price_list")){
            extraPriceList = intent.getSerializableExtra("extra_price_list") as ArrayList<ExtraPrice>
        }

        if(intent != null && intent.hasExtra("event_list")){
            itemEventActivityModelList = intent.getSerializableExtra("event_list") as ArrayList<TicketType>
        }

        if(intent != null && intent.hasExtra("service_fee_list")){
            serviceFeeList = intent.getSerializableExtra("service_fee_list") as ArrayList<ExtraPrice>
        }

        // Validate View
        when (category) {
            "hotel" -> {
                findViewById<LinearLayout>(R.id.adultsChildrenLayout).visibility = View.VISIBLE
                rangDateTv.text = String.format("%s - %s", startDate, endDate)
            }
            "space" ->{
                findViewById<LinearLayout>(R.id.adultsChildrenLayout).visibility = View.VISIBLE
                findViewById<LinearLayout>(R.id.dayLayout).visibility = View.VISIBLE

                rangDateTv.text = String.format("%s - %s", startDate, endDate)

                // Set duration day
                val day = DateUtil.calculateDuring(startDate, endDate, SimpleDateFormat("yyyy-MM-dd")).toInt() + 1 // StartDate / EndDate format "yyyy-MM-dd"
                findViewById<TextView>(R.id.dayTv).text = day.toString()
            }
            "tour" ->{
                val eventLayout = findViewById<LinearLayout>(R.id.EventLayout)
                eventLayout.visibility = View.VISIBLE

                val eventActivityLayout = findViewById<LinearLayout>(R.id.eventActivityLayout)
                eventActivityLayout.visibility = View.VISIBLE

                rangDateTv.text = dateFromToTour

                addViewItem(eventLayout)

                addViewItemExtraPrice(findViewById(R.id.itemExtraPriceLayout))

                addViewItemServiceFee(findViewById(R.id.itemServiceFeeLayout))
            }
            "event", "activity" -> {
                val eventLayout = findViewById<LinearLayout>(R.id.EventLayout)
                eventLayout.visibility = View.VISIBLE
                rangDateTv.text = String.format("%s", ". . .")  // No yet date

                val eventActivityLayout = findViewById<LinearLayout>(R.id.eventActivityLayout)
                eventActivityLayout.visibility = View.VISIBLE

                addViewItem(eventLayout)

                addViewItemExtraPrice(findViewById(R.id.itemExtraPriceLayout))

                addViewItemServiceFee(findViewById(R.id.itemServiceFeeLayout))

                durationTv.text = duration

            }
        }
    }

    private fun addViewItem(eventLayout : LinearLayout){
        for (item in itemEventActivityModelList) {
            if (item.isClick){
                val view: View = LayoutInflater.from(this).inflate(R.layout.custom_textview_left_right, null)
                val keyTv = view.findViewById(R.id.keyTv) as TextView
                val valueTv = view.findViewById(R.id.valueTv) as TextView
                keyTv.text = item.name
                valueTv.text = item.quantity.toString()
                eventLayout.addView(view)
            }
        }
    }

    private fun addViewItemExtraPrice(eventLayout : LinearLayout){
        var isHaveItemClick = false
        for (item in extraPriceList) {
            if (item.isClick){
                val view: View = LayoutInflater.from(this).inflate(R.layout.custom_textview_left_right, null)
                val keyTv = view.findViewById(R.id.keyTv) as TextView
                val valueTv = view.findViewById(R.id.valueTv) as TextView
                keyTv.text = item.name
                valueTv.text = item.price_display
                eventLayout.addView(view)
                isHaveItemClick = true
            }
        }
        if (!isHaveItemClick)   findViewById<CardView>(R.id.extraPriceLayout).visibility = View.GONE
    }

    private fun addViewItemServiceFee(eventLayout : LinearLayout){
        for (item in serviceFeeList) {
            val view: View = LayoutInflater.from(this).inflate(R.layout.custom_textview_left_right, null)
            val keyTv = view.findViewById(R.id.keyTv) as TextView
            val valueTv = view.findViewById(R.id.valueTv) as TextView
            keyTv.text = item.name
            valueTv.text = item.price_display
            eventLayout.addView(view)
        }
        if (serviceFeeList.size == 0)   findViewById<CardView>(R.id.serviceFeeLayout).visibility = View.GONE
    }

    private fun initAction(){
        val user = MockUpData.getUserItem(UserSessionManagement(this))
        if (user.firstName != null) firstName.setText(user.firstName)
        if (user.lastName != null) lastName.setText(user.lastName)
        if (user.phoneNumber != null) phoneTv.setText(user.phoneNumber)
        if (user.email != null) emailTv.setText(user.email)

        adultsTv.text = adults
        childrenTv.text = children

        Utils.logDebug("Jjekkkkkkkkkkkk", Utils.getStringGson(requestDataPayment()))

        btnSubmit.setOnClickListener(CustomSetOnClickViewListener{
            if (kessPayment != "") {
                val intent = Intent(this@HotelBookingAddAddressPaymentActivity, WebPayActivity::class.java)
                intent.putExtra("toolBarTitle", "Kess Pay")
                intent.putExtra("linkUrl", kessPayment)
                resultLauncher.launch(intent)
            } else {
                Utils.customToastMsgError(this@HotelBookingAddAddressPaymentActivity, "Link Payment is empty.", false)
            }
        })

        applyCouponCode.setOnClickListener(CustomSetOnClickViewListener{
            if (isAlreadyAddCoupon){
                AppAlertCusDialog.deleteUserAccount(this,  resources.getString(R.string.confirm), "You applied and got promoted coupon already.\n Are you sure to remove this coupon ?", PrettyDialog(this)) {
                    progressBar.visibility = View.VISIBLE
                    BookingHotelWS().applyCouponCode(this, progressBar, requestCodeCoupon(couponCode, "remove"), applyCouponCodeListener)
                }
            } else {
                alertCouponCodeDialog()
            }
        })

        subTotalTv.text = totalPrice
        setSummaryPrice(totalPrice, "$ 0.0")

        onLinePayment()
    }

    private fun alertCouponCodeDialog(){
        val alertLayout = LayoutInflater.from(this).inflate(R.layout.hotel_apply_coupon_code, null)
        val dialog = AlertDialog.Builder(this)
        dialog.setView(alertLayout)
        dialog.setCancelable(false)
        val alertDialog = dialog.show()
        val cancel = alertLayout.findViewById<TextView>(R.id.btnCancel)
        val ok = alertLayout.findViewById<TextView>(R.id.btnApply)
        val couponEdt = alertLayout.findViewById<EditText>(R.id.couponEdt)
        val progressBar = alertLayout.findViewById<ProgressBar>(R.id.progressItem)
        progressBar.visibility = View.GONE

        cancel.setOnClickListener { alertDialog.dismiss() }
        ok.setOnClickListener(CustomSetOnClickViewListener{
            if (couponEdt.text.toString().isEmpty()){
                couponEdt.setBackgroundResource(R.drawable.shape_error)
                Utils.customToastMsgError(this, resources.getString(R.string.enter_code), false)
            } else {
                couponEdt.setBackgroundResource(R.drawable.shape_transparent_hotel_book_bg)
                progressBar.visibility = View.VISIBLE
                couponCode = couponEdt.text.toString().trim()
                alertDialogBackUp = alertDialog
                BookingHotelWS().applyCouponCode(this, progressBar, requestCodeCoupon(couponCode, "add"), applyCouponCodeListener)
            }
        })
    }

    private val applyCouponCodeListener = object : BookingHotelWS.OnRequestCouponCallBackListener{

        override fun onLoadSuccess(message: String, action: String, couponCode: String, couponPrice: String, price: String, progressBar: ProgressBar) {
            progressBar.visibility = View.GONE
            isAlreadyAddCoupon = !isAlreadyAddCoupon

            if (action == "add" && alertDialogBackUp != null) {
                alertDialogBackUp!!.dismiss()
            }

            if (action == "add") {   // Applied coupon code
                iconAppliedCoupon.visibility = View.VISIBLE
                applyCouponCode.setBackgroundResource(R.drawable.card_view_shape_light_gray)
                couponTv.text = couponCode
                couponTv.setCompoundDrawablesWithIntrinsicBounds(null, null,  ResourcesCompat.getDrawable(resources, R.drawable.ic_delete_white_24, null), null);

                // Set Price
                setSummaryPrice(price, String.format("%s %s" , "-",couponPrice))
            } else if (action == "remove"){       // Removed coupon code
                iconAppliedCoupon.visibility = View.GONE
                applyCouponCode.setBackgroundResource(R.drawable.background_card_border)
                couponTv.text = ""
                couponTv.setCompoundDrawablesWithIntrinsicBounds(null, null,  ResourcesCompat.getDrawable(resources, R.drawable.fab_add, null), null);

                // Set Price
                setSummaryPrice(price, "$ 0.0")
            }

            Utils.customToastMsgError(this@HotelBookingAddAddressPaymentActivity, message, true)

        }

        override fun onLoadFail(message: String, progressBar: ProgressBar) {
            progressBar.visibility = View.GONE
            Utils.customToastMsgError(this@HotelBookingAddAddressPaymentActivity, message, false)
        }

    }

    private fun setSummaryPrice(totalPrice : String, couponPrice : String){
        totalAllPriceTv.text = totalPrice
        couponPriceTv.text = couponPrice
        totalPriceTv.text = totalPrice
    }

    private fun requestCodeCoupon(couponCode : String, action : String) : HashMap<String, Any>{
        val hashMap : HashMap<String, Any> = HashMap()
        hashMap["coupon_code"] = couponCode
        hashMap["booking_id"] = bookingId
        hashMap["action"] = action
        return hashMap
    }

    private fun requestPayment(){
        progressBar.visibility = View.VISIBLE
        Utils.logDebug("Jjekkkkkkkkkkkk", Utils.getStringGson(requestDataPayment()))
        BookingHotelWS().getPayment(this, requestDataPayment(), object : BookingHotelWS.OnRequestPaymentCallBack{
            override fun onLoadSuccess(paymentSuccessModel: PaymentSuccessModel) {
                progressBar.visibility = View.GONE
                // Show Dialog Success
                val intent = Intent(this@HotelBookingAddAddressPaymentActivity, HotelBookingSuccessDialogActivity::class.java)
                intent.putExtra("payment_info", paymentSuccessModel)
                startActivity(intent)

                Utils.customToastMsgError(this@HotelBookingAddAddressPaymentActivity, getString(R.string.payment_successful), true)

            }

            override fun onLoadFail(message: String) {
                progressBar.visibility = View.GONE
                Utils.customToastMsgError(this@HotelBookingAddAddressPaymentActivity, message, false)
            }

        })
    }

    private var resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode == Activity.RESULT_OK) {
            val data: Intent? = result.data
            if (data != null) {
                if (data.hasExtra("status")) {
                    if (data.getStringExtra("status") == "success=1") {
                        requestPayment()
                    } else {
                        Utils.customToastMsgError(this@HotelBookingAddAddressPaymentActivity,  getString(R.string.something_went_wrong), false)
                    }
                }
            }
        }
    }

    private fun requestDataPayment() : HashMap<String, Any>{
        val hashMap : HashMap<String, Any> = HashMap()
        hashMap["booking_id"] = bookingId
        hashMap["phone"] = phoneTv.text.toString()
        hashMap["first_name"] = firstName.text.toString()
        hashMap["last_name"] = lastName.text.toString()
        hashMap["email"] = emailTv.text.toString()
        hashMap["gateway_payment"] = "kess_payment"
        hashMap["city"] = countryEdt.text.toString()
        hashMap["country"] = resources.getString(R.string.cambodia)

        return hashMap
    }

    private fun onLinePayment(){
        recyclerViewPayment.isNestedScrollingEnabled = false
        recyclerViewPayment.layoutManager = GridLayoutManager(this, 2)
        recyclerViewPayment.adapter = PaymentMethodOnlineAdapter(getListOnlinePayment(this))
    }

}