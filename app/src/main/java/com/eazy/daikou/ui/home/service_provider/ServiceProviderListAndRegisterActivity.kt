package com.eazy.daikou.ui.home.service_provider

import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import androidx.viewpager.widget.ViewPager
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.ui.home.hrm.leave_request.adapter.TabsLeaveAdapter
import com.google.android.material.tabs.TabLayout

class ServiceProviderListAndRegisterActivity : BaseActivity() {
    private lateinit var viewPager: ViewPager
    private lateinit var tabLayout: TabLayout
    private lateinit var btnBack: ImageView
    private lateinit var titleTab: TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register_service_provider)

        initView()
        initAction()
    }

    private fun initView(){
        btnBack = findViewById(R.id.iconBack)
        titleTab = findViewById(R.id.titleToolbar)
        viewPager = findViewById(R.id.viewPagerTab)
        tabLayout = findViewById(R.id.tabLayouts)

    }

    private fun initAction(){

        btnBack.setOnClickListener { finish() }
        titleTab.text = getString(R.string.register_service_provider)

        val adapter = TabsLeaveAdapter(supportFragmentManager)
        // add fragment to the list
        adapter.addFragment(ServiceProviderMyListFragment(), getString(R.string.my_list_service_provider))
        adapter.addFragment(ServiceProviderRegisterFragment(), getString(R.string.register_service_provider))
        viewPager.adapter = adapter
        tabLayout.setupWithViewPager(viewPager)
    }
}