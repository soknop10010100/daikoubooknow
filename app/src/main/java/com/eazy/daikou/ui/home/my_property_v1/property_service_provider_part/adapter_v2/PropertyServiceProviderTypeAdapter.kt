package com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.adapter_v2

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.my_property.cleaning.PropertyServiceTypeModel
import de.hdodenhof.circleimageview.CircleImageView

class PropertyServiceProviderTypeAdapter(private val context: Context, private  val listPopular: ArrayList<PropertyServiceTypeModel>, private val callBackItem : CallbackPopularItem): RecyclerView.Adapter<PropertyServiceProviderTypeAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
       val view = LayoutInflater.from(context).inflate(R.layout.layout_service_item_popular_service, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val servicePopular : PropertyServiceTypeModel = listPopular[position]
        if (servicePopular != null) {
            Glide.with(context).load(if (servicePopular.sv_photo != null) servicePopular.sv_photo else R.drawable.no_image).into(holder.imageItemPopular)
            Utils.setValueOnText(holder.nameServiceTv, servicePopular.sv_name)
            Utils.setValueOnText(holder.priceServiceTv, servicePopular.price)
            if (servicePopular.sv_period != null){
                holder.descriptionServiceTv.text = String.format("%s %s %s", context.resources.getString(R.string.duration), servicePopular.sv_period, context.resources.getString(R.string.minutes))
            } else {
                holder.descriptionServiceTv.text = "- - -"
            }

            holder.itemView.setOnClickListener(
                CustomSetOnClickViewListener {
                    callBackItem.popularCallBackListener(servicePopular)
                }
            )
        }
    }

    override fun getItemCount(): Int {
       return listPopular.size
    }

    class ViewHolder( view: View) : RecyclerView.ViewHolder(view){
        val imageItemPopular: CircleImageView = view.findViewById(R.id.imageItemPopular)
        val nameServiceTv: TextView = view.findViewById(R.id.nameServiceTv)
        val descriptionServiceTv: TextView = view.findViewById(R.id.descriptionServiceTv)
        val priceServiceTv: TextView = view.findViewById(R.id.priceServiceTv)
    }

    interface CallbackPopularItem{
        fun popularCallBackListener(servicePopular: PropertyServiceTypeModel)
    }

    fun clear() {
        val size: Int = listPopular.size
        if (size > 0) {
            for (i in 0 until size) {
                listPopular.removeAt(0)
            }
            notifyItemRangeRemoved(0, size)
        }
    }


}