package com.eazy.daikou.ui.home.my_property_v1.property_service_employee.service_adapter_v2

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.my_property.service_property_employee.ServiceDayScheduleModel


class DayScheduleAdapter(private val serviceList: ArrayList<ServiceDayScheduleModel>, private val itemClick : ItemClickDaySchedule): RecyclerView.Adapter<DayScheduleAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.layout_item_day_schedule, parent, false)
        return ViewHolder(v)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
       val service : ServiceDayScheduleModel = serviceList[position]
        if (service.working_date != null){
            holder.dayNumberTv.text = Utils.formatDateFromString("yyyy-MM-dd", "dd", service.working_date)
        } else {
            holder.dayNumberTv.text = "- - -"
        }
        Utils.setValueOnText(holder.dayTv, service.day_name)
        holder.numberService.text = "+" + if(service.total_works != null) service.total_works else "0"

        val color: Int
        when (if (service.schedule_status != null) service.schedule_status else "") {
            "completed" -> {
                color = R.color.blue
            }
            "new" -> {
                color = R.color.appBarColorOld
            }
            "progressing" -> {
                color = R.color.yellow
            }
            "arriving" -> {
                color = R.color.green
            }
            "incomplete" ->{
                color = R.color.red

            }
            else -> {
                color = R.color.red
            }
        }

        holder.linearLayout.setBackgroundResource(color)

        holder.linearLayout.setOnClickListener(
            CustomSetOnClickViewListener{
                itemClick.onItemClick(service)
            }
        )


    }

    override fun getItemCount(): Int {
        return serviceList.size
    }
    interface ItemClickDaySchedule{
        fun onItemClick(schedule: ServiceDayScheduleModel)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var dayTv : TextView = view.findViewById(R.id.day_text)
        var dayNumberTv : TextView = view.findViewById(R.id.day_number)
        var numberService : TextView = view.findViewById(R.id.text_count)
        var linearLayout :LinearLayout = view.findViewById(R.id.layout_day)
    }
    fun clear() {
        val size: Int = serviceList.size
        if (size > 0) {
            for (i in 0 until size) {
                serviceList.removeAt(0)
            }
            notifyItemRangeRemoved(0, size)
        }
    }
}