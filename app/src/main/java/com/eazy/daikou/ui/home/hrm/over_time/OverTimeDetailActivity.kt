package com.eazy.daikou.ui.home.hrm.over_time
import android.annotation.SuppressLint
import android.app.Dialog
import android.content.res.ColorStateList
import android.os.Bundle
import android.view.View
import android.view.Window
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.core.widget.NestedScrollView
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.request_data.request.hr_ws.OvertimeWs
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.hr.OvertimeListModel

class OverTimeDetailActivity : BaseActivity() {

    private lateinit var employeeNameTv : TextView
    private lateinit var overtimeCategoryTv : TextView
    private lateinit var OverTimeNoTv : TextView
    private lateinit var createDateTv: TextView
    private lateinit var hourTv : TextView
    private lateinit var dateTv : TextView
    private lateinit var startTimeTv : TextView
    private lateinit var endTimeTv : TextView
    private lateinit var shiftTv : TextView
    private lateinit var assignToTv: TextView
    private lateinit var approvedByTv : TextView
    private lateinit var reasonTv : TextView
    private lateinit var statusTv : TextView
    private lateinit var btnApproveOvertime: TextView
    private lateinit var btnRejectOvertime: TextView
    private lateinit var overtimeDescriptionTv : TextView
    private lateinit var linearLayoutAction: LinearLayout
    private lateinit var imgCoverOvertime: ImageView
    private lateinit var linearDataProgress : LinearLayout
    private lateinit var progressItem : ProgressBar

    private var overtimeListId: String = ""
    private var userBusinessKey: String = ""
    private var actionClick: String = ""
    private var listCategory: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_over_time_detail)

        initView()

        initAction()

    }

    private fun initView(){
        employeeNameTv = findViewById(R.id.employeeNameTv)
        overtimeCategoryTv = findViewById(R.id.overtimeCategoryTv)
        OverTimeNoTv = findViewById(R.id.OverTimeNoTv)
        createDateTv = findViewById(R.id.createDateTv)
        hourTv = findViewById(R.id.hourTv)
        dateTv = findViewById(R.id.dateTv)
        startTimeTv = findViewById(R.id.startTimeTv)
        endTimeTv = findViewById(R.id.endTimeTv)
        shiftTv = findViewById(R.id.shiftTv)
        assignToTv = findViewById(R.id.assignToTv)
        approvedByTv = findViewById(R.id.approvedByTv)
        reasonTv = findViewById(R.id.reasonTv)
        statusTv = findViewById(R.id.statusTv)
        btnApproveOvertime = findViewById(R.id.btnApproveOvertime)
        btnRejectOvertime = findViewById(R.id.btnRejectOvertime)
        overtimeDescriptionTv = findViewById(R.id.overtime_descriptionTv)
        linearLayoutAction = findViewById(R.id.linearLayoutAction)
        imgCoverOvertime = findViewById(R.id.imgCoverOvertime)
        linearDataProgress = findViewById(R.id.linearDataProgress)
        imgCoverOvertime.setImageResource(R.drawable.cv_home_overtime)
        progressItem = findViewById(R.id.progressItem)

    }

    private fun initAction(){

        findViewById<TextView>(R.id.btnBack).setOnClickListener{
            onBackPressed()
        }

        if (intent != null && intent.hasExtra("overtime_id") || intent.hasExtra("action_click")) {
            overtimeListId = intent.getStringExtra("overtime_id").toString()
            userBusinessKey = intent.getStringExtra("user_business_key").toString()
            actionClick = intent.getStringExtra("action_click").toString()
        }

        requestListDetail()

        btnApproveOvertime.setOnClickListener {
            showDialogApproveReject("approve")
        }
        btnRejectOvertime.setOnClickListener {
          showDialogApproveReject("reject")
        }
    }

    override fun onBackPressed() {
        finish()
    }

    private fun showDialogApproveReject(status : String){
        val dialog = Dialog (this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.layout_alert_come_here)
        val btnCancel = dialog.findViewById<View>(R.id.btn_cancel) as TextView
        val btnOk = dialog.findViewById<View>(R.id.btn_ok) as TextView
        val titleTv = dialog.findViewById<TextView>(R.id.title)
        val  linearDescription = dialog.findViewById<LinearLayout>(R.id.layout)
        val  layoutEmpty = dialog.findViewById<LinearLayout>(R.id.layoutEmpty)
        val descriptionEdt : EditText = dialog.findViewById(R.id.description)

        if (status == "reject"){
            titleTv.text = getString(R.string.are_you_sure_to_reject)
        } else {
            titleTv.text = resources.getString(R.string.are_u_sure_to_approve)
            linearDescription .visibility = View.GONE
            layoutEmpty.visibility = View.VISIBLE
        }

        btnOk.setOnClickListener {
            if (status == "reject"){
                if(descriptionEdt.text.isNotEmpty()){
                    sendListActionButton(status,  descriptionEdt.text.toString())
                    dialog.dismiss()
                } else {
                    Toast.makeText(this,getString(R.string.please_input_your_reason), Toast.LENGTH_SHORT).show()
                }
            } else {
                sendListActionButton(status,"")
                dialog.dismiss()
            }
        }

        btnCancel.setOnClickListener { dialog.cancel() }
        dialog.show()
    }

    @SuppressLint("SetTextI18n")
    private fun   addDataForDetail(overtimeListModel: OvertimeListModel){
        if (overtimeListModel != null){

            if (overtimeListModel.id != null) OverTimeNoTv.text = overtimeListModel.id else OverTimeNoTv.text = ("----")

            if (overtimeListModel.created_at != null) createDateTv.text =  Utils.formatDateTime(overtimeListModel.created_at, "yyyy-MM-dd kk:mm:ss", "dd-MMM-yyyy hh:mm a") else createDateTv.text = "- - -"

            if (overtimeListModel.user_name != null) employeeNameTv.text = overtimeListModel.user_name else employeeNameTv.text = ("- - - -")

            if (overtimeListModel.overtime_category != null) overtimeCategoryTv.text = overtimeListModel.overtime_category else overtimeCategoryTv.text = ("- - -")

            if (overtimeListModel.hour != null) hourTv.text = overtimeListModel.hour + " " + resources.getString(R.string.hour).lowercase() else hourTv.text = ("- - -")

            if (overtimeListModel.date != null) dateTv.text = overtimeListModel.date else dateTv.text = ("- - -")

            if (overtimeListModel.begin_time != null) startTimeTv.text = Utils.formatDateTime(overtimeListModel.begin_time, "kk:mm:ss", "hh:mm a")  else startTimeTv.text = ("- - -")

            if (overtimeListModel.end_time != null) endTimeTv.text = Utils.formatDateTime(overtimeListModel.end_time ,"kk:mm:ss", "hh:mm a") else endTimeTv.text = ("- - -")

            if (overtimeListModel.shift_name != null) shiftTv.text = overtimeListModel.shift_name else shiftTv.text = ("- - -")

            if (overtimeListModel.approver_name != null) assignToTv.text = overtimeListModel.approver_name else assignToTv.text = ("- - -")

            if (overtimeListModel.approved_by_name != null) approvedByTv.text = overtimeListModel.approved_by_name else approvedByTv.text = ("- - -")

            if (overtimeListModel.remark != null) reasonTv.text = overtimeListModel.remark else reasonTv.text = ("- - -")

            if (overtimeListModel.description != null) overtimeDescriptionTv.text = overtimeListModel.description else overtimeDescriptionTv.text = resources.getString(R.string.no_description)

            when (overtimeListModel.status) {
                "pending" -> {
                    statusTv.text = Utils.getText(this, R.string.pending)
                    statusTv.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(this, R.color.yellow))
                }
                "rejected" -> {
                    statusTv.text = Utils.getText(this, R.string.rejected)
                    statusTv.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(this, R.color.red))
                }
                "approved" ->{
                    statusTv.text = Utils.getText(this, R.string.approved)
                    statusTv.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(this, R.color.green))
                }
                else -> {
                    statusTv.text = "- - -"
                    statusTv.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(this, R.color.appBarColor))
                }
            }
        }
    }

    private fun requestListDetail(){
        progressItem.visibility = View.VISIBLE
        OvertimeWs().getDetailOvertimeWs(this, overtimeListId, onCallBackDetail)
    }

    private fun sendListActionButton(status: String, reason: String){
        val hashMap: HashMap<String,Any> = HashMap()
        hashMap["over_time_id"] = overtimeListId
        hashMap["status"] = status
        hashMap["user_business_key"] = userBusinessKey
        if (status == "reject"){
            hashMap["reason"] = reason
        }

        OvertimeWs().getActionApproveRejectWS(this,hashMap,onCallBackActionOvertime)
    }

    private val onCallBackDetail: OvertimeWs.OnCallBackDetailOvertimeListener = object : OvertimeWs.OnCallBackDetailOvertimeListener{
        override fun onLoadSuccessFull(overtimeListModel: OvertimeListModel) {
            addDataForDetail(overtimeListModel)

            progressItem.visibility = View.GONE
            linearDataProgress.visibility = View.VISIBLE

            if (overtimeListModel.status == "pending" && overtimeListModel.user_business_key != userBusinessKey) {
                linearLayoutAction.visibility = View.VISIBLE
                listCategory = "approve_overtime"
            } else {
                linearLayoutAction.visibility = View.GONE
                listCategory = "my_list"
            }

            if (actionClick != "") {
                setBackResult(listCategory)
            }

            // set margin bottom == 0
            if (linearLayoutAction.visibility == View.GONE) {
                Utils.setNoMarginBottomOnButton(findViewById<NestedScrollView>(R.id.mainScroll), 0)
            }
        }

        override fun onLoadFailed(message: String) {
            progressItem.visibility = View.GONE
            Utils.customToastMsgError(this@OverTimeDetailActivity, message, false)
        }
    }

    private fun setBackResult(listCategory : String){
        val intent = intent
        intent.putExtra("list_category", listCategory)
        setResult(RESULT_OK, intent)
    }

    private val onCallBackActionOvertime: OvertimeWs.OnCallBackActionOvertime = object : OvertimeWs.OnCallBackActionOvertime{
        override fun onLoadSuccessFull(message: String) {
            Utils.customToastMsgError(this@OverTimeDetailActivity,message,true)
            setResult(RESULT_OK)
            finish()
        }

        override fun onLoadFailed(message: String) {
            Utils.customToastMsgError(this@OverTimeDetailActivity,message,false)
        }
    }
}
