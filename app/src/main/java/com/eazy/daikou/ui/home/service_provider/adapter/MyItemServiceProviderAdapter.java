package com.eazy.daikou.ui.home.service_provider.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.eazy.daikou.R;
import com.eazy.daikou.helper.Utils;
import com.eazy.daikou.model.my_property.service_provider.MenuServiceProvider;

import java.util.List;
import java.util.Locale;

public class MyItemServiceProviderAdapter extends RecyclerView.Adapter<MyItemServiceProviderAdapter.ViewHolder> {

    private final List<MenuServiceProvider> menuServiceProviders;
    private final ItemClickCategoryServiceProvider itemClick;
    private final Context context;

    public MyItemServiceProviderAdapter(List<MenuServiceProvider> menuServiceProviders, Context context, ItemClickCategoryServiceProvider itemClick) {
        this.menuServiceProviders = menuServiceProviders;
        this.itemClick = itemClick;
        this.context = context;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view  = LayoutInflater.from(parent.getContext()).inflate(R.layout.service_provider_adapter,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        MenuServiceProvider listService = menuServiceProviders.get(position);
        if(listService!=null){
            holder.nameCategoryTv.setText(String.format(Locale.US,"%s",listService.getName()));
            if (listService.isClick()){
                holder.nameCategoryTv.setTextColor(Color.WHITE);
                holder.linearLayout.setBackgroundResource(R.drawable.bg_shap_color_appbar);
            } else {
                holder.nameCategoryTv.setTextColor(Color.GRAY);
                holder.linearLayout.setBackgroundResource(R.drawable.custom_card_view_color_menu);
            }

            holder.itemView.setOnClickListener(view -> {
                itemClick.onClickItem(listService,position);
            });

        }

    }

    @Override
    public int getItemCount() {
        return menuServiceProviders.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView nameCategoryTv;
        LinearLayout linearLayout;
        public ViewHolder(View view) {
            super(view);
            linearLayout = view.findViewById(R.id.linear_card);
            imageView = view.findViewById(R.id.image_category);
            nameCategoryTv = view.findViewById(R.id.text_category);
        }


    }
    public interface ItemClickCategoryServiceProvider{
        void onClickItem(MenuServiceProvider listString,int position);
    }
}