package com.eazy.daikou.ui

import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import androidx.viewpager2.widget.ViewPager2
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.my_property.my_property.ImagesModel
import com.eazy.daikou.ui.home.development_project.adapter.ImageDetailAdapter

class ShowMultipleDisplayImagesActivity : BaseActivity() {

    private lateinit var viewPager: ViewPager2
    private lateinit var textCount:TextView
    private var listImageUrl = ArrayList<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        
        setContentView(R.layout.activity_properties_image_detail)

        initView()
        
        initAction()
    }


    private fun initView() {
        viewPager = findViewById(R.id.view_pager)
        textCount = findViewById(R.id.text_count_image)

        Utils.customOnToolbar(this, resources.getString(R.string.show_all_images)){finish()}
    }

    private fun initAction(){
        // init data
        if (intent != null && intent.hasExtra("image_list")) {
            listImageUrl = intent.getStringArrayListExtra("image_list") as ArrayList<String>
        }

        // Image list model
        if (intent != null && intent.hasExtra("image_model_list")) {
            val listImageUrl = intent.getSerializableExtra("image_model_list") as ArrayList<ImagesModel>
            convertListModelToListString(listImageUrl)
        }

        // init action
        viewPager.adapter = ImageDetailAdapter(listImageUrl)
        viewPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                textCount.text = "${position+1}/${listImageUrl.size}"
            }
        })
    }

    private fun convertListModelToListString(imageList : ArrayList<ImagesModel>) {
        for (item in imageList){
            listImageUrl.add(item.file_path!!)
        }
    }

}