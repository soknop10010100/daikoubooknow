package com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.adapter

import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.fragment.ImageListFragment


@Suppress("DEPRECATION")
class ListImageAdapter(fragmentManager: FragmentManager, private val listPhoto:ArrayList<String>, private val action: String) : FragmentPagerAdapter(fragmentManager) {

    override fun getCount(): Int {
       return  listPhoto.size
    }

    override fun getItem(position: Int): ImageListFragment {
        val listPhotos = listPhoto[position]
        return ImageListFragment.newInstance(listPhoto.size, listPhotos, listPhoto, action)
    }

}