package com.eazy.daikou.ui.home.development_project.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.github.chrisbanes.photoview.PhotoView
import com.squareup.picasso.Picasso

class ImageDetailAdapter(
    private val listImageUrl:List<String>
) :RecyclerView.Adapter<ImageDetailAdapter.ViewHolder>()
{
    inner class ViewHolder(itemView:View):RecyclerView.ViewHolder(itemView)
    {
        val image = itemView.findViewById<PhotoView>(R.id.image)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.custom_image_item_property_detail,parent,false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val imgUrl = listImageUrl[position].replace("\"","")
        Picasso.get().load(imgUrl).placeholder(R.drawable.no_image).into(holder.image)
        Log.d("haha",imgUrl)
    }

    override fun getItemCount(): Int {
        return listImageUrl.size
    }
}