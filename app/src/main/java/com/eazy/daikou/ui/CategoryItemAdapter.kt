package com.eazy.daikou.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.CustomCategoryModel

class CategoryItemAdapter (private val list: ArrayList<CustomCategoryModel>, private val itemClickListener : ClickCallBackListener):
    RecyclerView.Adapter<CategoryItemAdapter.ItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.custom_category_text_layout, parent, false)
        return ItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.setImage(list[position], itemClickListener)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private var nameItemTv : TextView = view.findViewById(R.id.text_category)
        private var categoryLayout : LinearLayout = view.findViewById(R.id.categoryLayout)
        fun setImage(item : CustomCategoryModel, itemClickListener: ClickCallBackListener) {
            nameItemTv.text = if (item.name != null) item.name else ". . ."
            categoryLayout.background = Utils.setDrawable(categoryLayout.context.resources, if (item.isClick) R.drawable.on_swipe_shape_color_app else R.drawable.bg_swap_app_appbar_strok)
            nameItemTv.setTextColor(if (item.isClick) Utils.getColor(nameItemTv.context, R.color.white) else Utils.getColor(nameItemTv.context, R.color.black))

            nameItemTv.setOnClickListener(CustomSetOnClickViewListener{
                itemClickListener.onClickCallBack(item)
            })
        }
    }

    interface ClickCallBackListener {
        fun onClickCallBack(item: CustomCategoryModel)
    }

}