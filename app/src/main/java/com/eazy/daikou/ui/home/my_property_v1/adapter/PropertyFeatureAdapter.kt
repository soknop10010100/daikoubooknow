package com.eazy.daikou.ui.home.my_property_v1.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.hr.ItemHR

class PropertyFeatureAdapter(private val action : String, private val itemList: List<ItemHR>) : RecyclerView.Adapter<PropertyFeatureAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_project_highlight, parent, false))
    }

    @SuppressLint("ResourceType")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val homeViewModel = itemList[position]
        if (homeViewModel != null) {
            Glide.with(holder.icon).load(homeViewModel.image).into(holder.icon)
            if (action == "Property Overview"){
                holder.icon.setColorFilter(Utils.getColor(holder.icon.context, R.color.gray))
                holder.title.text = String.format("%s : %s", homeViewModel.nameItem, homeViewModel.descriptionItem)
            } else {
                holder.icon.setColorFilter(Utils.getColor(holder.icon.context, R.color.greenSea))
                holder.title.text = homeViewModel.nameItem
            }
        }
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var icon: ImageView = itemView.findViewById(R.id.img_changeItem)
        var title: TextView = itemView.findViewById(R.id.txtTitle)
    }
}