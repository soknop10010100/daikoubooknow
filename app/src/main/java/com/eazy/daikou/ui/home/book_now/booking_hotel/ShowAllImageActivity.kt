package com.eazy.daikou.ui.home.book_now.booking_hotel

import android.content.Context
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.helper.GetDataUtils
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.ui.home.book_now.booking_hotel.adapter.CustomImageGridLayoutAdapter

class ShowAllImageActivity : BaseActivity() {

    private lateinit var recyclerView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show_all_image)

        initAction()

    }

    private fun initAction(){
        recyclerView = findViewById(R.id.recyclerView)

        var slideModels: ArrayList<String> = ArrayList()
        if (intent != null && intent.hasExtra("image_list")){
            slideModels = intent.getSerializableExtra("image_list") as ArrayList<String>
        }
        val hotelName = GetDataUtils.getDataFromString("hotel_name", this)
        Utils.customOnToolbar(this, hotelName){finish()}

        recyclerView.layoutManager = spanGridLayoutManager(this)
        recyclerView.adapter = CustomImageGridLayoutAdapter("show_all_image", this, slideModels, object : CustomImageGridLayoutAdapter.OnClickCallBackLister{
            override fun onClickCallBack(value: String) {
                if (value != null)  Utils.openZoomImage(this@ShowAllImageActivity, value, ArrayList<String>())
            }
        })
    }

    private fun spanGridLayoutManager(context: Context?): RecyclerView.LayoutManager {
        val gridLayoutManager = GridLayoutManager(context, 2)
        gridLayoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                return if (position % 3 == 0)   2
                       else                     1
            }
        }
        return gridLayoutManager
    }
}