package com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.cleaning_v2

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.CalendarView
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity

class ServiceCleaningCreateNextActivity : BaseActivity() {

    private lateinit var startDateLinear: LinearLayout
    private lateinit var endDateLinear: LinearLayout
    private lateinit var dateStart: TextView
    private lateinit var dateEnd: TextView
    private lateinit var btnReviewPayment: TextView
    private lateinit var recyclerChooseDay: RecyclerView
    private lateinit var selectStartDate : CalendarView
    private lateinit var selectEndDate : CalendarView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_service_cleaning_create_next)

        initView()
        initAction()
    }

    private fun initView(){
        startDateLinear = findViewById(R.id.startDateLinear)
        endDateLinear = findViewById(R.id.endDateLinear)
        dateStart = findViewById(R.id.dateStart)
        dateEnd = findViewById(R.id.dateEnd)
        btnReviewPayment = findViewById(R.id.btnReviewPayment)
        recyclerChooseDay = findViewById(R.id.recyclerChooseDay)
        selectStartDate = findViewById(R.id.selectStartDate)
        selectEndDate = findViewById(R.id.selectEndDate)
    }

    private fun initAction(){

        onClickDate("start")

        findViewById<ImageView>(R.id.iconBack).setOnClickListener { finish() }
        findViewById<TextView>(R.id.titleToolbar).text = getString(R.string.register_service_provider)

        startDateLinear.setOnClickListener {
            onClickDate("start")
        }

        endDateLinear.setOnClickListener {
            onClickDate("end")
        }

        btnReviewPayment.setOnClickListener {
            startActivity(Intent(this@ServiceCleaningCreateNextActivity, ServiceCleaningPaymentAndBookingActivity::class.java))
        }
        dateMultiple()
    }

    private fun dateMultiple(){

    }
    private fun onClickDate(types:String){
        if (types == "start") {
            dateStart.setTextColor(ContextCompat.getColor(this, R.color.greenSea))
            startDateLinear.background = ResourcesCompat.getDrawable(resources, R.drawable.custom_card_view_color_menu, null)
            dateEnd.setTextColor(Color.GRAY)
            endDateLinear.background = null
            selectStartDate.visibility = View.VISIBLE
            selectEndDate.visibility = View.GONE

        } else {
            dateEnd.setTextColor(ContextCompat.getColor(this, R.color.greenSea))
            endDateLinear.background = ResourcesCompat.getDrawable(resources, R.drawable.custom_card_view_color_menu, null)
            dateStart.setTextColor(Color.GRAY)
            startDateLinear.background = null
            selectStartDate.visibility = View.GONE
            selectEndDate.visibility = View.VISIBLE
        }
    }
}