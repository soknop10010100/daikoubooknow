package com.eazy.daikou.ui.notifications.apdater;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.eazy.daikou.R;
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener;
import com.eazy.daikou.helper.DateUtil;
import com.eazy.daikou.helper.Utils;
import com.eazy.daikou.model.notification.ListNotification;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

public class ListAllNotificationAdapter extends RecyclerView.Adapter<ListAllNotificationAdapter.ViewHolder> {

    private final List<ListNotification> listNotifications;
    private final OnClickCallBack onClickCallBack;
    private final Context context;

    public ListAllNotificationAdapter(Context context, List<ListNotification> listNotifications, OnClickCallBack onClickCallBack) {
        this.listNotifications = listNotifications;
        this.onClickCallBack = onClickCallBack;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ListAllNotificationAdapter.ViewHolder(LayoutInflater.from(context).inflate(R.layout.layout_list_all_notification, parent, false));
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ListNotification listNotification = listNotifications.get(position);
        if (listNotification != null){
            if (listNotification.getCreatedDt() != null){
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                String createdDate = Utils.formatDateFromString("MM/dd/yyyy", "yyyy-MM-dd", DateUtil.formatDate(listNotification.getCreatedDt()));
                String dateToday = simpleDateFormat.format(new Date());

                if (createdDate.equals(dateToday)){
                    holder.txtDate.setText(context.getResources().getString(R.string.today).toLowerCase(Locale.ROOT));
                } else if (createdDate.compareTo(dateToday) < 0){
                    if (Integer.parseInt(calculateDuring(createdDate, dateToday)) >= 30){
                        holder.txtDate.setText(convertDayToMonth(createdDate, dateToday) + " " + context.getResources().getString(R.string.month_s).toLowerCase(Locale.ROOT) + " " + context.getResources().getString(R.string.ago));
                    } else  {
                        holder.txtDate.setText(convertDayToMonth(createdDate, dateToday) + " " + context.getResources().getString(R.string.day_s).toLowerCase(Locale.ROOT) + " " + context.getResources().getString(R.string.ago));
                    }
                } else {
                    holder.txtDate.setText(". . .");
                }
            } else {
                holder.txtDate.setText(". . .");
            }

            holder.subjectTv.setText(listNotification.getTitle() != null ? listNotification.getTitle() : ". . .");

            Utils.setValueOnText(holder.descriptionTv, listNotification.getDescription());

            if (listNotification.getImage() != null){
                Glide.with(context).load(listNotification.getImage()).into(holder.circleImageView);
            } else {
                Glide.with(context).load(R.drawable.no_image).into(holder.circleImageView);
            }


            holder.linearLayout.setOnClickListener(new CustomSetOnClickViewListener(view -> onClickCallBack.onClick(listNotification, "detail_action")));

            if (listNotification.getReadStatus()){
                holder.dotRead.setVisibility(View.GONE);
            } else {
                holder.dotRead.setVisibility(View.VISIBLE);
            }

            holder.buttonDeleteLayout.setOnClickListener(new CustomSetOnClickViewListener(view -> onClickCallBack.onClick(listNotification, "delete_action")));

        }
    }

    @Override
    public int getItemCount() {
        return listNotifications.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView txtDate, subjectTv, descriptionTv;
        private final CircleImageView circleImageView;
        private final CardView linearLayout;
        private final LinearLayout buttonDeleteLayout;
        private final ImageView dotRead;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            circleImageView = itemView.findViewById(R.id.imageNotification);
            txtDate = itemView.findViewById(R.id.dateTv);
            subjectTv = itemView.findViewById(R.id.subjectTv);
            descriptionTv = itemView.findViewById(R.id.descriptionTv);
            linearLayout = itemView.findViewById(R.id.mainCard);
            buttonDeleteLayout = itemView.findViewById(R.id.buttonDeleteItem);
            dotRead = itemView.findViewById(R.id.dotRead);
        }
    }

    public interface OnClickCallBack{
        void onClick(ListNotification listNotifications, String action);
    }

    public void clear() {
        int size = listNotifications.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                listNotifications.remove(0);
            }
            notifyItemRangeRemoved(0, size);
        }
    }

    private String calculateDuring(String CurrentDate, String FinalDate){
        String dayDifference = "";
        try {
            Date date1;
            Date date2;
            SimpleDateFormat dates = new SimpleDateFormat("yyyy-MM-dd");
            date1 = dates.parse(CurrentDate);
            date2 = dates.parse(FinalDate);
            assert date1 != null;
            assert date2 != null;
            long difference = Math.abs(date1.getTime() - date2.getTime());
            long differenceDates = difference / (24 * 60 * 60 * 1000);
            dayDifference = Long.toString(differenceDates);
        } catch (Exception exception) {
            Utils.logDebug("hejklehjkjkl", exception.getMessage());
        }
        return dayDifference;
    }

    private int convertDayToMonth(String CurrentDate, String FinalDate){
        String day = calculateDuring(CurrentDate, FinalDate);
        int months = 0;
        try {
            if (Integer.parseInt(day) >= 30){
                months = Integer.parseInt(day) / 30;
            } else {
                months = Integer.parseInt(day);
            }
        } catch (Exception ignored){ }

        return months;
    }
}