package com.eazy.daikou.ui.home.hrm.resignation

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.request_data.request.hr_ws.ResignationWs
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.hr.*
import com.eazy.daikou.model.profile.User
import com.eazy.daikou.ui.home.hrm.resignation.adapter.ItemCategoryResignAdapter
import com.google.gson.Gson

class ItemCategoryResignActivity : BaseActivity() {
    private lateinit var recyclerView : RecyclerView
    private lateinit var btnCancel : ImageView
    private lateinit var noDataLayout : LinearLayout
    private lateinit var editSearchNameApprove : EditText
    private lateinit var btnClearSearch : ImageView
    private lateinit var titleEmployeeName : TextView
    private lateinit var progressBar: ProgressBar
    private var action = ""
    private var userBusinessKey = ""
    private lateinit var user : User
    private var itemCategoryResignList = ArrayList<ApproveResignType>()
    private lateinit var categoryItemAdapter : ItemCategoryResignAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_item_category_resign)

        initView()

        initData()

        initAction()
    }

    private fun initView(){
        recyclerView = findViewById(R.id.recyclerSelectItem)
        btnCancel = findViewById(R.id.btnCancel)
        progressBar = findViewById(R.id.progressItem)
        editSearchNameApprove = findViewById(R.id.editSearchNameApprove)
        btnClearSearch = findViewById(R.id.btnClearSearch)
        noDataLayout = findViewById(R.id.noDataLayout)
        titleEmployeeName = findViewById(R.id.titleEmployeeName)

    }

    private fun initData(){
        if (intent != null && intent.hasExtra("list_item_resign")){
            itemCategoryResignList = intent.getSerializableExtra("list_item_resign") as ArrayList<ApproveResignType>
            action = intent.getStringExtra("action").toString()
        }
        user = Gson().fromJson(UserSessionManagement(this).userDetail, User::class.java)
        val businessKy = user.userBusinessKey
        if (businessKy != null){
            userBusinessKey = businessKy
        }
    }

    private fun initAction(){
        btnCancel.setOnClickListener { finish() }

        progressBar.visibility = View.GONE

        if (action == "approveList"){
            editSearchNameApprove.visibility = View.VISIBLE
        } else {
            titleEmployeeName.text = getString(R.string.type_of_resignation)
            editSearchNameApprove.visibility = View.GONE
        }

        recyclerView.layoutManager = LinearLayoutManager(this)
        categoryItemAdapter = ItemCategoryResignAdapter(itemCategoryResignList, object : ItemCategoryResignAdapter.OnClickCallBackListener{
            override fun onClickBack(item: ApproveResignType) {
                val intent = Intent()
                intent.putExtra("id", item.id)
                intent.putExtra("name", item.name)
                intent.putExtra("approver_user_business_key", item.user_business_key)
                setResult(RESULT_OK, intent)
                finish()
            }

        })
        recyclerView.adapter = categoryItemAdapter

        onSearchApproveName()

    }

    private fun onCloneServiceAPI(searchApprove : String){
        progressBar.visibility = View.VISIBLE
        ResignationWs().getCategoryItemResignWs(this, userBusinessKey, searchApprove,callBackListItem)
    }

    private val callBackListItem : ResignationWs.OnListItemResignCallBackListener = object : ResignationWs.OnListItemResignCallBackListener{
        @SuppressLint("NotifyDataSetChanged")
        override fun onSuccessful(itemResignationModel: ItemResignationModel) {
            progressBar.visibility = View.GONE
            itemCategoryResignList.clear()
            itemCategoryResignList.addAll(itemResignationModel.approver)

            if (itemCategoryResignList.size > 0) {
                noDataLayout.visibility = View.GONE
                recyclerView.visibility = View.VISIBLE
            } else {
                noDataLayout.visibility = View.VISIBLE
                recyclerView.visibility = View.GONE
            }

            categoryItemAdapter.notifyDataSetChanged()
        }

        override fun onFailed(error: String) {
            progressBar.visibility = View.GONE
            Utils.customToastMsgError(this@ItemCategoryResignActivity, error, false)
        }

    }

    private fun onSearchApproveName(){
        editSearchNameApprove.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun afterTextChanged(editable: Editable) {
                btnClearSearch.setOnClickListener {
                    editSearchNameApprove.setText("")
                    editable.clear()
                }
                if (editable.toString().isNotEmpty()){
                    btnClearSearch.visibility = View.VISIBLE
                    onCloneServiceAPI(editable.toString())
                } else {
                    btnClearSearch.visibility = View.GONE
                    onCloneServiceAPI("")
                }
            }
        })
    }

}