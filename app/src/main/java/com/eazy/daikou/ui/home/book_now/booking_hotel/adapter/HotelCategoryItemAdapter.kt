package com.eazy.daikou.ui.home.book_now.booking_hotel.adapter

import android.content.res.ColorStateList
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.home.SubItemHomeModel
import com.squareup.picasso.Picasso


class HotelCategoryItemAdapter (private val isHomePageHotel : Boolean, private val listImage: ArrayList<SubItemHomeModel>, private val itemClickListener : BookingHomeItemAdapter.PropertyClick): RecyclerView.Adapter<HotelCategoryItemAdapter.ItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.hotel_category_booking_item_model, parent, false)
        return ItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.setImage(isHomePageHotel, listImage[position], itemClickListener)
    }

    override fun getItemCount(): Int {
        return listImage.size
    }


    class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        //Is home page hotel
        private var nameItemTv : TextView = view.findViewById(R.id.name_item_hr)
        private var imageView : ImageView = view.findViewById(R.id.image_hr)
        private var cardAdapter : CardView = view.findViewById(R.id.card_hr)
        private var imageLayout : CardView = view.findViewById(R.id.imageLayout)
        private var menuHomePageLayout : LinearLayout = view.findViewById(R.id.menuHomePageLayout)

        //Not home page hotel
        private var textCategory : TextView = view.findViewById(R.id.text_category)
        private var imageCategory : ImageView = view.findViewById(R.id.imageCategory)
        private var categoryLayout : LinearLayout = view.findViewById(R.id.categoryLayout)

        private var imageVisitLayout : CardView = view.findViewById(R.id.imageVisitLayout)
        private var imageIcon : ImageView = view.findViewById(R.id.imageIcon)

        fun setImage(isHomePageHotel : Boolean, subItemModel: SubItemHomeModel,  itemClickListener : BookingHomeItemAdapter.PropertyClick) {
            //Is home page hotel
            nameItemTv.text = if (subItemModel.name != null) subItemModel.name else "- - -"
            Glide.with(imageView).load(if (subItemModel.drawable != null) subItemModel.drawable else R.drawable.no_image).into(imageView)

            if (subItemModel.isClickCategory){
                imageLayout.layoutParams.height = Utils.dpToPx(imageLayout.context, 55)
                imageLayout.layoutParams.width = Utils.dpToPx(imageLayout.context, 55)
                nameItemTv.textSize = 14f
                nameItemTv.setTextColor(Utils.getColor(imageLayout.context, R.color.appBarColorOld))
            } else {
                imageLayout.layoutParams.height = Utils.dpToPx(imageLayout.context, 45)
                imageLayout.layoutParams.width = Utils.dpToPx(imageLayout.context, 45)
                nameItemTv.textSize = 12f
                nameItemTv.setTextColor(Utils.getColor(imageLayout.context, R.color.book_now_secondary))
            }

            imageLayout.backgroundTintList = setColorBackground(Utils.validateNullValue(subItemModel.id!!))

            //Not home page hotel
            textCategory.text = if (subItemModel.name != null) subItemModel.name else "- - -"
            Glide.with(imageCategory).load(if (subItemModel.drawable != null) subItemModel.drawable else R.drawable.no_image).into(imageCategory)

            categoryLayout.background = Utils.setDrawable(categoryLayout.context.resources, if (subItemModel.isClickCategory) R.drawable.on_swipe_shape_color_app else R.drawable.bg_swap_app_appbar_strok)
            textCategory.setTextColor(if (subItemModel.isClickCategory) Utils.getColor(textCategory.context, R.color.white) else Utils.getColor(textCategory.context, R.color.black))
            imageCategory.setColorFilter(if (subItemModel.isClickCategory) Utils.getColor(textCategory.context, R.color.white) else Utils.getColor(textCategory.context, R.color.book_now_secondary))

            cardAdapter.setOnClickListener(CustomSetOnClickViewListener{
                itemClickListener.onBookingClick(subItemModel)
            })

            menuHomePageLayout.visibility = if (isHomePageHotel) View.VISIBLE else View.GONE
            categoryLayout.visibility = if (isHomePageHotel) View.GONE else View.VISIBLE

            if (subItemModel.action == "menus_visit_category"){
                imageVisitLayout.visibility = View.VISIBLE
                imageLayout.visibility = View.GONE
                Picasso.get().load(subItemModel.imageUrl).into(imageIcon)
            }

            // Set width layout on Card Layout Main
            val size = Utils.dpToPx(cardAdapter.context, 5)
            val itemViewParam = LinearLayout.LayoutParams(if (isHomePageHotel) LinearLayout.LayoutParams.MATCH_PARENT else LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            itemViewParam.setMargins(size, size, size, size)
            cardAdapter.layoutParams = itemViewParam
        }

        private fun setColorBackground(id: String): ColorStateList {
            return when (id) {
                "hotel" -> ColorStateList.valueOf(Color.parseColor("#2086a7"))
                "space" -> ColorStateList.valueOf(Color.parseColor("#fdb52e"))
                "activity" -> ColorStateList.valueOf(Color.parseColor("#fd7522"))
                "tour" -> ColorStateList.valueOf(Color.parseColor("#0f7f3a"))
                "event" -> ColorStateList.valueOf(Color.parseColor("#1476fb"))
                else -> ColorStateList.valueOf(Color.parseColor("#2086a7"))
            }
        }
    }

}