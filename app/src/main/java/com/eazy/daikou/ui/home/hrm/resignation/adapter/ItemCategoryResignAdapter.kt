package com.eazy.daikou.ui.home.hrm.resignation.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.model.hr.ApproveResignType
import com.eazy.daikou.model.hr.ItemResignationModel

class ItemCategoryResignAdapter(private val listCategory : ArrayList<ApproveResignType>, private val onClickCallBack : OnClickCallBackListener): RecyclerView.Adapter<ItemCategoryResignAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.layout_item_select_overtime,parent,false)
        return ViewHolder(view)

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val itemResignationModel = listCategory[position]
        if (itemResignationModel != null){
            holder.itemName.text = if (itemResignationModel.name != null) itemResignationModel.name else "- - -"
            holder.itemView.setOnClickListener { onClickCallBack.onClickBack(itemResignationModel) }
            if (itemResignationModel.isClick){
                holder.imageCheckSelect.visibility = View.VISIBLE
            } else {
                holder.imageCheckSelect.visibility = View.GONE
            }
        }
    }

    override fun getItemCount(): Int {
       return listCategory.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val itemName: TextView = itemView.findViewById(R.id.ItemNameTv)
        val imageCheckSelect: ImageView = itemView.findViewById(R.id.imageCheckSelect)
    }

    interface OnClickCallBackListener{
        fun onClickBack(item : ApproveResignType)
    }

}