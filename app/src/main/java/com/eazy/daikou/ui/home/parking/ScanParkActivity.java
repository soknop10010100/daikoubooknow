package com.eazy.daikou.ui.home.parking;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.eazy.daikou.R;
import com.eazy.daikou.base.BaseActivity;
import com.eazy.daikou.request_data.static_data.StaticUtilsKey;
import com.eazy.daikou.ui.MainActivity;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.LuminanceSource;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.RGBLuminanceSource;
import com.google.zxing.Reader;
import com.google.zxing.Result;
import com.google.zxing.common.HybridBinarizer;

import java.io.FileNotFoundException;
import java.io.InputStream;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class ScanParkActivity extends BaseActivity implements  ZXingScannerView.ResultHandler, View.OnClickListener {

    private final String TAG = ScanParkActivity.this.getClass().getName();
    private ZXingScannerView mScannerView;
    private final int CAMERA = 123;
    private final int PHOTO = 124 ;
    private String action = "",typeScan = " " ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_scan_main);

        ImageView imageView = findViewById(R.id.btnUpload);
        imageView.setOnClickListener(this);
        mScannerView = findViewById(R.id.scan_view);

        if (getIntent() != null && getIntent().hasExtra("action")){
            action = getIntent().getStringExtra("action");
        }
        if(getIntent()!=null && getIntent().hasExtra("type_scan")){
            typeScan = getIntent().getStringExtra("type_scan");
        }

        findViewById(R.id.backButton).setOnClickListener(v -> onBackFinish());
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.CAMERA},CAMERA );
        } else {
            mScannerView.startCamera();          // Start camera on resume
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == CAMERA) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this,getString(R.string.camera_was_granted), Toast.LENGTH_SHORT).show();
                mScannerView.startCamera();          // Start camera on resume
            } else {
                Toast.makeText(this, getString(R.string.camera_was_not_granted), Toast.LENGTH_SHORT).show();
            }
        }else  if(requestCode == PHOTO){
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, getString(R.string.gallery_was_granted), Toast.LENGTH_SHORT).show();
                choosePhotoFromGallery();
            } else {
                Toast.makeText(this, getString(R.string.gallery_was_not_granted), Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();           // Stop camera on pause
    }

    @Override
    public void handleResult(Result rawResult) {
        // Do something with the result here
        Log.v("asdfasdfasdfads", rawResult.getText()); // Prints scan results
        Log.v(TAG, rawResult.getBarcodeFormat().toString()); // Prints the scan format (qrcode, pdf417 etc.)
//        Toast.makeText(ScannerActivity.this, rawResult.getText() , Toast.LENGTH_LONG).show();

        // If you would like to resume scanning, call this method below:
        doScan(rawResult.getText());
        mScannerView.resumeCameraPreview(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(data!=null){
            int REQUEST_TRANSFER = 2342;
            if(resultCode == RESULT_OK && requestCode == REQUEST_TRANSFER){
                setResult(RESULT_OK);
                finish();
            }else if(requestCode == PHOTO){
                Uri selectedImage = data.getData();
                InputStream imageStream = null;
                try {
                    //getting the image
                    imageStream = getContentResolver().openInputStream(selectedImage);
                } catch (FileNotFoundException e) {
                    Toast.makeText(getApplicationContext(), "File not found", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
                //decoding bitmap
                Bitmap bMap = BitmapFactory.decodeStream(imageStream);
                doScan(scanQRImage(bMap));
            }
        }

    }

    private void doScan(String text){
        try {
            Intent intent = getIntent();
            intent.putExtra("result_scan", text);
            intent.putExtra("action", action);
            intent.putExtra("type_scan",typeScan);
            setResult(RESULT_OK, intent);
            finish();
        } catch (Exception e){
            Toast.makeText(this, getString(R.string.scanner_not_working), Toast.LENGTH_SHORT).show();
        }
    }

    public static String scanQRImage(Bitmap bMap) {
        String contents = null;

        int[] intArray = new int[bMap.getWidth()*bMap.getHeight()];
        //copy pixel data from the Bitmap into the 'intArray' array
        bMap.getPixels(intArray, 0, bMap.getWidth(), 0, 0, bMap.getWidth(), bMap.getHeight());

        LuminanceSource source = new RGBLuminanceSource(bMap.getWidth(), bMap.getHeight(), intArray);
        BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));

        Reader reader = new MultiFormatReader();
        try {
            Result result = reader.decode(bitmap);
            contents = result.getText();
        }
        catch (Exception e) {
            Log.e("QrTest", "Error decoding barcode", e);
        }
        return contents;
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.btnUpload){
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, PHOTO);
                }
            }else {
                choosePhotoFromGallery();
            }
        }
    }

    private void choosePhotoFromGallery() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, PHOTO);
    }

    @Override
    public void onBackPressed() {
        onBackFinish();
    }

    private void onBackFinish(){
        if (action.equals(StaticUtilsKey.action_key_scanner)){
            Intent intent = new Intent(ScanParkActivity.this, MainActivity.class);
            startActivity(intent);
            finishAffinity();
        } else if (action.equals("home_screen")){
            Intent intent = getIntent();
            intent.putExtra("click_back", action);
            setResult(RESULT_OK, intent);
            finish();
        } else {
            finish();
        }
    }


}
