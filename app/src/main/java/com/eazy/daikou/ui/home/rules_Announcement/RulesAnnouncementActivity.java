package com.eazy.daikou.ui.home.rules_Announcement;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentContainerView;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.eazy.daikou.R;
import com.eazy.daikou.base.BaseActivity;
import com.eazy.daikou.helper.Utils;
import com.eazy.daikou.ui.home.quote_book.BookFragment;

public class RulesAnnouncementActivity extends BaseActivity {

    private ImageView btnBack;
    private TextView titleToolbar;
    private TextView tvBtnRules;
    private TextView tvBtnAnnouncement;
    private LinearLayout linearLayout;
    private FragmentContainerView fragmentContainerView;
    private ProgressBar progressBar;
    private boolean isFirstClickRule = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rules_annonement);

        initView();

        replaceViewFragment(new RulesFragment());

        getLinearLayout(linearLayout);
    }

    private void initView(){

        tvBtnRules = findViewById(R.id.tvBtnBook);
        tvBtnAnnouncement = findViewById(R.id.tvBtnQuote);
        titleToolbar = findViewById(R.id.titleToolbar);
        linearLayout = findViewById(R.id.linearLayout);
        btnBack = findViewById(R.id.iconBack);
        fragmentContainerView = findViewById(R.id.containerQuoteBook);
        progressBar = findViewById(R.id.progressItem);
        progressBar.setVisibility(View.GONE);

        titleToolbar.setText(Utils.getText(this, R.string.rules_and_announcement));
        btnBack.setOnClickListener(v->finish());

        // Utils.customOnToolbar(this, Utils.getText(this, R.string.rules_and_announcement), this::finish);

        tvBtnRules.setText(Utils.getText(this,R.string.rule));
        tvBtnAnnouncement.setText(Utils.getText(this,R.string.announcement));

    }

    private void getLinearLayout(LinearLayout linearLayout){
        for (int i = 0; i < linearLayout.getChildCount(); i++) {
            int finalI = i;
            linearLayout.getChildAt(i).setOnClickListener(view -> {
                setBackgroundMenu(finalI);
                if (finalI == 0){
                    linearLayout.getChildAt(0).setClickable(false);
                    linearLayout.getChildAt(1).setClickable(true);
                    if (!isFirstClickRule) {
                        replaceViewFragment(new RulesFragment());
                    }
                } else {
                    isFirstClickRule = false;
                    linearLayout.getChildAt(0).setClickable(true);
                    linearLayout.getChildAt(1).setClickable(false);
                    replaceViewFragment(new AnnouncementFragment());
                }
            });
        }
    }

    private void replaceViewFragment(Fragment fragment){
        FragmentManager fm = getSupportFragmentManager();
        fragmentContainerView = findViewById(R.id.containerQuoteBook);
        FragmentTransaction transaction = fm.beginTransaction();
        if (fragmentContainerView != null) {
            transaction.replace(R.id.containerQuoteBook, fragment, fragment.getTag());
            transaction.disallowAddToBackStack();
            transaction.commit();
        }
    }

    private void setBackgroundMenu(int position){
        if (position == 0){
            tvBtnRules.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.card_view_shape, null));
            tvBtnAnnouncement.setBackground(null);
        } else {
            tvBtnRules.setBackground(null);
            tvBtnAnnouncement.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.card_view_shape, null));
        }
    }
}