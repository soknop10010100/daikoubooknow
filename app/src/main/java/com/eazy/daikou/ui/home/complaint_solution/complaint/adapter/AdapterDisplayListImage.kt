package com.eazy.daikou.ui.home.complaint_solution.complaint.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.ui.ShowMultipleDisplayImagesActivity

class AdapterDisplayListImage(private var context : Context, private var listImages: ArrayList<String>) :RecyclerView.Adapter<AdapterDisplayListImage.ViewHolder>() {

    inner class ViewHolder(itemView:View):RecyclerView.ViewHolder(itemView){
        val imageView: ImageView = itemView.findViewById(R.id.imageViewPager)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.custom_image_item_detail_compliant_solution, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val image = listImages[position]
        Glide.with(context).load(image).into(holder.imageView)

        holder.imageView.setOnClickListener (CustomSetOnClickViewListener{
            val intent = Intent(context, ShowMultipleDisplayImagesActivity::class.java).apply {
                putExtra("image_list", listImages)
            }
            context.startActivity(intent)
        })
    }

    override fun getItemCount(): Int {
        return listImages.size
    }
}