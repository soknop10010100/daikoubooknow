package com.eazy.daikou.ui.profile.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.eazy.daikou.R;
import com.eazy.daikou.helper.CheckIsAppActive;
import com.eazy.daikou.helper.Utils;
import com.eazy.daikou.model.home.LanguageModel;

import java.util.List;

public class ChangeLangAdapter extends RecyclerView.Adapter<ChangeLangAdapter.ItemViewHolder> {

    private final List<LanguageModel> modelList;
    private final ClickCallBackListener clickCallBackListener;
    private final Context context;

    public ChangeLangAdapter(Context context, List<LanguageModel> modelList, ClickCallBackListener clickCallBackListener) {
        this.modelList = modelList;
        this.clickCallBackListener = clickCallBackListener;
        this.context = context;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rowView = LayoutInflater.from(parent.getContext()).inflate(R.layout.language_model, parent, false);
        return new ItemViewHolder(rowView);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        LanguageModel languageModel = modelList.get(position);
        if (languageModel != null) {
            holder.iconFlag.setImageResource(languageModel.getIconFlag());
            holder.nameLanguage.setText(languageModel.getNameLanguage());

            char getLangSaved = Utils.getString("language", context).charAt(0);
            char getNameFirstChLang = languageModel.getKey().charAt(0);
            if (getLangSaved == getNameFirstChLang) {
                holder.iconTech.setVisibility(View.VISIBLE);
            } else {
                holder.iconTech.setVisibility(View.INVISIBLE);
            }

            holder.nameLanguage.setTextColor(Utils.getColor(context, R.color.black));

            holder.linParentLayout.setOnClickListener(view -> clickCallBackListener.ClickCallBack(position, languageModel.getKey(), context));
        }
    }

    @Override
    public int getItemCount() {
        return modelList.size();
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder {
        private final TextView nameLanguage;
        private final ImageView iconFlag, iconTech;
        private final LinearLayout linParentLayout;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            nameLanguage = itemView.findViewById(R.id.name_language);
            iconFlag = itemView.findViewById(R.id.icon_flag);
            iconTech = itemView.findViewById(R.id.icon_tech_default);
            linParentLayout = itemView.findViewById(R.id.lin_parent);
        }
    }

    public interface ClickCallBackListener {
        void ClickCallBack(int pos, String lang, Context context);
    }
}
