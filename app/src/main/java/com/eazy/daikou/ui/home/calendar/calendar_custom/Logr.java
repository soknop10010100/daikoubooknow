package com.eazy.daikou.ui.home.calendar.calendar_custom;

import android.util.Log;

import com.eazy.daikou.BuildConfig;
import com.eazy.daikou.helper.Utils;

/** Log utility class to handle the log tag and DEBUG-only logging. */
final class Logr {
  public static void d(String message) {
    if (BuildConfig.DEBUG) {
      Utils.logDebug("DateTimePicker", message);
    }
  }

  public static void d(String message, Object... args) {
    if (BuildConfig.DEBUG) {
      d(String.format(message, args));
    }
  }
}
