package com.eazy.daikou.ui.home.book_now.booking_hotel.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.work_order.ImageListModel

class AdapterImageSlider (private val listImage: ArrayList<ImageListModel>, private val viewPager2: ViewPager2, private val onClickCallBack: OnClickItemListener): RecyclerView.Adapter<AdapterImageSlider.SliderViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SliderViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.custom_image_slide_model, parent, false)
        return SliderViewHolder(view)
    }

    override fun onBindViewHolder(holder: SliderViewHolder, position: Int) {
        holder.setImage(listImage[position], onClickCallBack)
        if (position == listImage.size - 2) {
            viewPager2.post(runnable)
        }
    }

    override fun getItemCount(): Int {
        return listImage.size
    }


    class SliderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imageViewPager: ImageView = itemView.findViewById(R.id.imageViewPager)
        var cardViewLayout : CardView = itemView.findViewById(R.id.cardViewLayout)
        fun setImage(sliderItems: ImageListModel, onClickCallBack: OnClickItemListener) {
            cardViewLayout.layoutParams.height =  Utils.setHeightOfScreen(cardViewLayout.context, 4.0)
            Glide.with(imageViewPager).load(sliderItems.valImage).into(imageViewPager)

            itemView.setOnClickListener(CustomSetOnClickViewListener{
                onClickCallBack.onClickCallBack(sliderItems)
            })
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private val runnable = Runnable {
        listImage.addAll(listImage)
        notifyDataSetChanged()
    }

    interface OnClickItemListener {
        fun onClickCallBack(sliderItems : ImageListModel)
    }
}
