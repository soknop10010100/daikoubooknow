package com.eazy.daikou.ui.home.my_property_v1

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.helper.map.MapsPropertyActivity
import com.eazy.daikou.model.home.MainItemHomeModel
import com.eazy.daikou.model.home.SubItemHomeModel
import com.eazy.daikou.model.hr.ItemHR
import com.eazy.daikou.ui.home.home_menu.adapter.MainHomeShowItemAdapter
import com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.adapter.ListImageAdapter
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.tabs.TabLayout

class PropertyListItemDetailActivity : BaseActivity(), OnMapReadyCallback {

    private lateinit var viewPager : ViewPager
    private lateinit var tabLayout : TabLayout
    private lateinit var listImageAdapter : ListImageAdapter
    private lateinit var progressBar: ProgressBar
    private var mMap: GoogleMap? = null
    private lateinit var recyclerViewItemFeature : RecyclerView
    private var list : ArrayList<MainItemHomeModel> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_property_list_item_detail)

        initView()

        initAction()

    }

    private fun initView(){
        tabLayout = findViewById(R.id.tab_layout)
        viewPager = findViewById(R.id.viewpagers)
        findViewById<ImageView>(R.id.btnBack).setOnClickListener { finish() }
        progressBar = findViewById(R.id.progressItem)
        progressBar.visibility = View.GONE
        recyclerViewItemFeature = findViewById(R.id.recyclerViewItemHighlight)
//        recyclerViewItemHighlight = findViewById(R.id.recyclerViewItemHighlight)
    }

    private fun initAction(){
        loadImage()

        val mapFragment = (supportFragmentManager.findFragmentById(R.id.map_google) as SupportMapFragment?)!!
        mapFragment.getMapAsync(this)

        findViewById<ImageView>(R.id.iconZoomMap).setOnClickListener {
            val intent = Intent(this@PropertyListItemDetailActivity, MapsPropertyActivity::class.java)
            intent.putExtra("title", "Maps")
            startActivity(intent)
        }

        getItemList()
        recyclerViewItemFeature.isNestedScrollingEnabled = false
        recyclerViewItemFeature.layoutManager = LinearLayoutManager(this)
        recyclerViewItemFeature.adapter = MainHomeShowItemAdapter("property_detail_item", list, this, object : MainHomeShowItemAdapter.PropertyClick{
            override fun onClickCallBackListener(actionWishlist: String, subItemHomeModel: SubItemHomeModel) {}

            override fun onClickSeeAllCallBackListener(action: String , actionType : String, subItemHomeModel: ArrayList<*>) {}

        })
    }

    //Static Menu More
    private fun getItemList(){
        var mainItemHomeModel = MainItemHomeModel()
        var homeViewModelList: ArrayList<ItemHR> = ArrayList()
        mainItemHomeModel.headerTitle = "Project Highlight"
        homeViewModelList.add(ItemHR("my_property", R.drawable.ic_tech_white, "Project information","You can view your own property here", R.color.greenSea))
        homeViewModelList.add(ItemHR("property_service", R.drawable.ic_tech_white, "Previous period","All the projects in cambodia", R.color.calendar_active_month_bg))
        homeViewModelList.add(ItemHR("property_listing", R.drawable.ic_tech_white, "Next Period","You can search property buy sale and rent", R.color.yellow))
        homeViewModelList.add(ItemHR("property_listing", R.drawable.ic_tech_white, "Issues Status","You can search property buy sale and rent", R.color.yellow))
        homeViewModelList.add(ItemHR("property_listing", R.drawable.ic_tech_white, "Risks Status","You can search property buy sale and rent", R.color.yellow))
        mainItemHomeModel.subItemMoreModelList = homeViewModelList
        list.add(mainItemHomeModel)

        mainItemHomeModel = MainItemHomeModel()
        mainItemHomeModel.headerTitle = "Project Overview"
        homeViewModelList = ArrayList()
        homeViewModelList.add(ItemHR("facility", R.drawable.icons8_bed_size_90, "Total Unit","This is for your facility service booking", R.color.greenSea))
        homeViewModelList.add(ItemHR("my_card", R.drawable.floor_plan, "Bed room in unit","You can view your own booking", R.color.calendar_active_month_bg))
        homeViewModelList.add(ItemHR("facility", R.drawable.icons8_bed_size_90, "Total Unit","This is for your facility service booking", R.color.greenSea))
        homeViewModelList.add(ItemHR("my_card", R.drawable.floor_plan, "Bed room in unit","You can view your own booking", R.color.calendar_active_month_bg))
        homeViewModelList.add(ItemHR("facility", R.drawable.icons8_bed_size_90, "Total Unit","This is for your facility service booking", R.color.greenSea))
        homeViewModelList.add(ItemHR("my_card", R.drawable.floor_plan, "Bed room in unit","You can view your own booking", R.color.calendar_active_month_bg))
        homeViewModelList.add(ItemHR("facility", R.drawable.icons8_bed_size_90, "Total Unit","This is for your facility service booking", R.color.greenSea))
        homeViewModelList.add(ItemHR("my_card", R.drawable.floor_plan, "Bed room in unit","You can view your own booking", R.color.calendar_active_month_bg))
        homeViewModelList.add(ItemHR("facility", R.drawable.icons8_bed_size_90, "Total Unit","This is for your facility service booking", R.color.greenSea))
        homeViewModelList.add(ItemHR("my_card", R.drawable.floor_plan, "Bed room in unit","You can view your own booking", R.color.calendar_active_month_bg))
        mainItemHomeModel.subItemMoreModelList = homeViewModelList
        list.add(mainItemHomeModel)

        mainItemHomeModel = MainItemHomeModel()
        mainItemHomeModel.headerTitle = "Property Feature"
        homeViewModelList = ArrayList()
        homeViewModelList.add(ItemHR("attendance", R.drawable.ic_home_hotel_booking_v2, "Good Service","223", R.color.greenSea))
        homeViewModelList.add(ItemHR("attendance", R.drawable.ic_home_hotel_booking_v2,"Garden","2", R.color.greenSea))
        homeViewModelList.add(ItemHR("attendance", R.drawable.ic_home_hotel_booking_v2, "Good Service","223", R.color.greenSea))
        homeViewModelList.add(ItemHR("attendance", R.drawable.ic_home_hotel_booking_v2,"Garden","2", R.color.greenSea))
        homeViewModelList.add(ItemHR("attendance", R.drawable.ic_home_hotel_booking_v2, "Good Service","223", R.color.greenSea))
        homeViewModelList.add(ItemHR("attendance", R.drawable.ic_home_hotel_booking_v2,"Garden","2", R.color.greenSea))
        homeViewModelList.add(ItemHR("attendance", R.drawable.ic_home_hotel_booking_v2, "Good Service","223", R.color.greenSea))
        homeViewModelList.add(ItemHR("attendance", R.drawable.ic_home_hotel_booking_v2,"Garden","2", R.color.greenSea))
        homeViewModelList.add(ItemHR("attendance", R.drawable.ic_home_hotel_booking_v2, "Good Service","223", R.color.greenSea))
        homeViewModelList.add(ItemHR("attendance", R.drawable.ic_home_hotel_booking_v2,"Garden","2", R.color.greenSea))
        homeViewModelList.add(ItemHR("attendance", R.drawable.ic_home_hotel_booking_v2, "Good Service","223", R.color.greenSea))
        homeViewModelList.add(ItemHR("attendance", R.drawable.ic_home_hotel_booking_v2,"Garden","2", R.color.greenSea))
        homeViewModelList.add(ItemHR("attendance", R.drawable.ic_home_hotel_booking_v2, "Good Service","223", R.color.greenSea))
        homeViewModelList.add(ItemHR("attendance", R.drawable.ic_home_hotel_booking_v2,"Garden","2", R.color.greenSea))
        homeViewModelList.add(ItemHR("attendance", R.drawable.ic_home_hotel_booking_v2, "Good Service","223", R.color.greenSea))
        homeViewModelList.add(ItemHR("attendance", R.drawable.ic_home_hotel_booking_v2,"Garden","2", R.color.greenSea))
        mainItemHomeModel.subItemMoreModelList = homeViewModelList
        list.add(mainItemHomeModel)

    }

    private fun zoomMap(lat: Double, lng: Double) {
        mMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(lat, lng), 15f))
        val latLng = LatLng(lat, lng)
        val marker = mMap!!.addMarker(MarkerOptions().position(latLng)
                .title("Daikou Mall")
        )
        marker?.showInfoWindow()
    }

    private fun loadImage() {
        val listImage : ArrayList<String> = ArrayList()
        val urlImage = "https://eazy.daikou.asia/img/app_home_slide_1.jpg"
        for (i in 1..10) {
            listImage.add(urlImage.replace("1", i.toString()))
        }
        listImageAdapter = ListImageAdapter(supportFragmentManager, listImage, "showImage")
        viewPager.adapter = listImageAdapter
        tabLayout.setupWithViewPager(viewPager, true)
        listImageAdapter.notifyDataSetChanged()
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }

        zoomMap(latitude, longitude)
        mMap!!.isMyLocationEnabled = true
        mMap!!.uiSettings.isZoomControlsEnabled = false
        mMap!!.uiSettings.isMyLocationButtonEnabled = false
        mMap!!.uiSettings.isCompassEnabled = false
    }

}