package com.eazy.daikou.ui.home.inspection_work_order.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.daimajia.swipe.SwipeLayout;
import com.eazy.daikou.R;
import com.eazy.daikou.request_data.static_data.StaticUtilsKey;
import com.eazy.daikou.helper.Utils;
import com.eazy.daikou.model.inspection.ItemTemplateModel;
import java.util.List;
import java.util.Locale;

public class EditItemInspectionAdapter extends RecyclerView.Adapter<EditItemInspectionAdapter.ItemViewHolder> {

    private final List<ItemTemplateModel.SubItemTemplateModel> subItemTemplateModelList;
    private final ClickCallBackSubItemListener clickCallBackSubItemListener;
    private final ItemTemplateModel itemTemplateModel;
    private final Context context;

    public EditItemInspectionAdapter(Context context, ItemTemplateModel itemTemplateModel, List<ItemTemplateModel.SubItemTemplateModel> subItemTemplateModelList, ClickCallBackSubItemListener clickCallBackSubItemListener) {
        this.context = context;
        this.itemTemplateModel = itemTemplateModel;
        this.subItemTemplateModelList = subItemTemplateModelList;
        this.clickCallBackSubItemListener = clickCallBackSubItemListener;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_sub_template_form, parent, false));
    }

    @SuppressLint("NotifyDataSetChanged")
    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, @SuppressLint("RecyclerView") int position) {
        ItemTemplateModel.SubItemTemplateModel subItemTemplateModel = subItemTemplateModelList.get(position);
        if (subItemTemplateModel != null){

            if (subItemTemplateModel.getItem_description() != null){
                holder.subItemDescription.setText(subItemTemplateModel.getItem_description());
            }
            if (subItemTemplateModel.getSub_item_category() != null) {
                holder.subItemNameTv.setText(subItemTemplateModel.getSub_item_category());
            }
            if (subItemTemplateModel.getItem_condition() != null) {
                holder.subItemCondition.setText(subItemTemplateModel.getItem_condition());
            }

            if (subItemTemplateModel.getSub_item_category() != null){
                holder.subItemNameTv2.setText(subItemTemplateModel.getSub_item_category());
            }

            subItemTemplateModel.setItem_no(itemTemplateModel.getKey() + "." + (position + 1));

            holder.subItemNameTv.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    if(!editable.toString().isEmpty()){
                        subItemTemplateModel.setSub_item_category(editable.toString());
                        clickCallBackSubItemListener.onWriteSubItem(subItemTemplateModel, editable.toString(), position);
                    } else {
                        subItemTemplateModel.setSub_item_category("");
                        clickCallBackSubItemListener.onWriteSubItem(subItemTemplateModel, "", position);
                    }
                }
            });

            holder.subItemNameTv2.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    if(!editable.toString().isEmpty()){
                        subItemTemplateModel.setSub_item_category(editable.toString());
                        clickCallBackSubItemListener.onWriteSubItem(subItemTemplateModel, editable.toString(), position);

                    } else {
                        subItemTemplateModel.setSub_item_category("");
                        clickCallBackSubItemListener.onWriteSubItem(subItemTemplateModel, "", position);
                    }
                }
            });

            holder.subItemDescription.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    if(!editable.toString().isEmpty()){
                        subItemTemplateModel.setItem_description(editable.toString());
                    } else {
                        subItemTemplateModel.setItem_description("");
                    }
                }
            });

            holder.subItemCondition.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    if(!editable.toString().isEmpty()){
                        subItemTemplateModel.setItem_condition(editable.toString());
                    } else {
                        subItemTemplateModel.setItem_condition("");
                    }
                }
            });

            holder.inspectionLayout.setOnClickListener(v -> {
                clickCallBackSubItemListener.onClickInspection(subItemTemplateModel);
                holder.swipe_layout.close();
            });

            holder.writeReportLayout.setOnClickListener(v -> {
                clickCallBackSubItemListener.onClickWriteReport(subItemTemplateModel);
                holder.swipe_layout.close();
            });

            holder.addImageLayout.setOnClickListener(v -> {
                clickCallBackSubItemListener.onClickAddImage(subItemTemplateModel, itemTemplateModel.getValue() + subItemTemplateModel.getValue() );
                holder.swipe_layout.close();
            });

            holder.deleteLayout.setOnClickListener(v -> {
                clickCallBackSubItemListener.onClickDelete( subItemTemplateModel, itemTemplateModel.getValue(), holder.swipe_layout);
                holder.swipe_layout.close();
            });

            //Check Action Option Type
            String keyMenu;
            if (subItemTemplateModel.getKey_menu() != null){
                keyMenu = subItemTemplateModel.getKey_menu();
            } else {
                keyMenu = StaticUtilsKey.detail_button_key;
            }

            if (keyMenu.equalsIgnoreCase(StaticUtilsKey.simplify_button_key)) {
                holder.simplifyLayout.setVisibility(View.VISIBLE);
                holder.detailLayout.setVisibility(View.GONE);
                holder.titleCleanTv.setVisibility(View.VISIBLE);    holder.titleUnDamageTv.setVisibility(View.VISIBLE);    holder.titleWorkingTv.setVisibility(View.VISIBLE);
            } else if (keyMenu.equalsIgnoreCase(StaticUtilsKey.question_button_key)){
                holder.simplifyLayout.setVisibility(View.VISIBLE);
                holder.detailLayout.setVisibility(View.GONE);
                holder.titleCleanTv.setVisibility(View.GONE);    holder.titleUnDamageTv.setVisibility(View.GONE);    holder.titleWorkingTv.setVisibility(View.GONE);
            } else if (keyMenu.equalsIgnoreCase(StaticUtilsKey.detail_button_key)){
                holder.simplifyLayout.setVisibility(View.GONE);
                holder.detailLayout.setVisibility(View.VISIBLE);
            }

            //Set Color Background Item
            if (keyMenu.equalsIgnoreCase(StaticUtilsKey.simplify_button_key)) {
                if (subItemTemplateModel.getIsClickNA().equalsIgnoreCase(Utils.getText(context, R.string.yes).toUpperCase(Locale.ROOT))) {
                    setBackgroundTintColor(context, holder.cleanTv, R.color.green);
                    holder.cleanTv.setText(Utils.getText(context, R.string.yes).toUpperCase(Locale.ROOT));
                } else if (subItemTemplateModel.getIsClickNA().equalsIgnoreCase(Utils.getText(context, R.string.no))) {
                    setBackgroundTintColor(context, holder.cleanTv, R.color.red);
                    holder.cleanTv.setText(Utils.getText(context, R.string.no));
                } else {
                    setBackgroundTintColor(context, holder.cleanTv, R.color.gray);
                    holder.cleanTv.setText("N/A");
                }
                subItemTemplateModel.setIsClickNA(subItemTemplateModel.getIsClickNA());
            } else if (keyMenu.equalsIgnoreCase(StaticUtilsKey.question_button_key)){
                holder.cleanTv.setText(Utils.getText(context, R.string.yes).toUpperCase(Locale.ROOT));
                holder.unDamageTv.setText(Utils.getText(context, R.string.no));

                if (Utils.getText(context, R.string.yes).toUpperCase(Locale.ROOT).equalsIgnoreCase(subItemTemplateModel.getIsClickNA())) {

                    setBackgroundTintColor(context, holder.cleanTv, R.color.green);
                    setBackgroundTintColor(context, holder.unDamageTv, R.color.gray);
                    setBackgroundTintColor(context, holder.workingTv, R.color.gray);

                } else if (Utils.getText(context, R.string.no).equalsIgnoreCase(subItemTemplateModel.getIsClickNA())) {

                    setBackgroundTintColor(context, holder.cleanTv, R.color.gray);
                    setBackgroundTintColor(context, holder.unDamageTv, R.color.red);
                    setBackgroundTintColor(context, holder.workingTv, R.color.gray);

                } else if ("N/A".equalsIgnoreCase(subItemTemplateModel.getIsClickNA()) || "".equalsIgnoreCase(subItemTemplateModel.getIsClickNA())) {

                    setBackgroundTintColor(context, holder.cleanTv, R.color.gray);
                    setBackgroundTintColor(context, holder.unDamageTv, R.color.gray);
                    setBackgroundTintColor(context, holder.workingTv, R.color.blue);

                } else {
                    setBackgroundTintColor(context, holder.cleanTv, R.color.gray);
                    setBackgroundTintColor(context, holder.unDamageTv, R.color.gray);
                    setBackgroundTintColor(context, holder.workingTv, R.color.blue);
                }
                subItemTemplateModel.setIsClickNA(subItemTemplateModel.getIsClickNA());
            }

            if (subItemTemplateModel.getClickUnDamage() != null && keyMenu.equalsIgnoreCase(StaticUtilsKey.simplify_button_key)) {
                if (subItemTemplateModel.getClickUnDamage().equalsIgnoreCase(Utils.getText(context, R.string.yes).toUpperCase(Locale.ROOT))) {
                    setBackgroundTintColor(context, holder.unDamageTv, R.color.green);
                    holder.unDamageTv.setText(Utils.getText(context, R.string.yes).toUpperCase(Locale.ROOT));
                } else if (subItemTemplateModel.getClickUnDamage().equalsIgnoreCase(Utils.getText(context, R.string.no))) {
                    setBackgroundTintColor(context, holder.unDamageTv, R.color.red);
                    holder.unDamageTv.setText(Utils.getText(context, R.string.no));
                } else {
                    setBackgroundTintColor(context, holder.unDamageTv, R.color.gray);
                    holder.unDamageTv.setText("N/A");
                }
                subItemTemplateModel.setClickUnDamage(subItemTemplateModel.getClickUnDamage());
            }

            if (keyMenu.equalsIgnoreCase(StaticUtilsKey.simplify_button_key)) {
                if (subItemTemplateModel.getClickWorking().equalsIgnoreCase(Utils.getText(context, R.string.yes))) {
                    setBackgroundTintColor(context, holder.workingTv, R.color.green);
                    holder.workingTv.setText(Utils.getText(context, R.string.yes).toUpperCase(Locale.ROOT));
                } else if (subItemTemplateModel.getClickWorking().equalsIgnoreCase(Utils.getText(context, R.string.no))) {
                    setBackgroundTintColor(context, holder.workingTv, R.color.red);
                    holder.workingTv.setText(Utils.getText(context, R.string.no));
                } else {
                    setBackgroundTintColor(context, holder.workingTv, R.color.gray);
                    holder.workingTv.setText("N/A");
                }
                subItemTemplateModel.setClickWorking(subItemTemplateModel.getClickWorking());
            }

            holder.cleanTv.setOnClickListener(v -> {
                clickCallBackSubItemListener.onClickYesNoNA(context, subItemTemplateModel, itemTemplateModel.getValue(), holder.swipe_layout, "clean_click");
            });

            holder.unDamageTv.setOnClickListener(v -> {
                clickCallBackSubItemListener.onClickYesNoNA(context, subItemTemplateModel, itemTemplateModel.getValue(), holder.swipe_layout, "un_damage_click");
            });

            holder.workingTv.setOnClickListener(v -> {
                clickCallBackSubItemListener.onClickYesNoNA(context, subItemTemplateModel, itemTemplateModel.getValue(), holder.swipe_layout, "working_click");
            });

        }

    }

    @Override
    public int getItemCount() {
        return subItemTemplateModelList.size();
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder{

        private final EditText subItemNameTv, subItemNameTv2, subItemDescription, subItemCondition;
        private final LinearLayout inspectionLayout, writeReportLayout, addImageLayout, deleteLayout, simplifyLayout, detailLayout;
        private final TextView cleanTv, unDamageTv, workingTv, titleCleanTv, titleUnDamageTv, titleWorkingTv;
        private final SwipeLayout swipe_layout;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);

            simplifyLayout = itemView.findViewById(R.id.simplifyLayout);
            detailLayout = itemView.findViewById(R.id.linear_item);
            swipe_layout = itemView.findViewById(R.id.swipe_layout);
            subItemCondition = itemView.findViewById(R.id.SubItemCondition);
            subItemDescription = itemView.findViewById(R.id.SubItemDescription);
            subItemNameTv = itemView.findViewById(R.id.SubItemName);
            subItemNameTv2 = itemView.findViewById(R.id.SubItemName2);

            inspectionLayout = itemView.findViewById(R.id.buttonAddInspection);
            writeReportLayout = itemView.findViewById(R.id.buttonReport);
            addImageLayout = itemView.findViewById(R.id.buttonAddImage);
            deleteLayout = itemView.findViewById(R.id.buttonDeleteItem);

            cleanTv = itemView.findViewById(R.id.cleanTv);
            unDamageTv = itemView.findViewById(R.id.unDamageTv);
            workingTv = itemView.findViewById(R.id.workingTv);

            titleCleanTv = itemView.findViewById(R.id.titleCleanTv);
            titleUnDamageTv = itemView.findViewById(R.id.titleUnDamageTv);
            titleWorkingTv = itemView.findViewById(R.id.titleWorkingTv);

        }
    }

    private void setBackgroundTintColor(Context context, View view, int color){
        DrawableCompat.setTint(DrawableCompat.wrap(view.getBackground()).mutate(),
                ContextCompat.getColor(context, color));
    }

    public interface ClickCallBackSubItemListener{
        void onClickInspection(ItemTemplateModel.SubItemTemplateModel subItemTemplateModel);
        void onClickWriteReport(ItemTemplateModel.SubItemTemplateModel subItemTemplateModel);
        void onClickAddImage(ItemTemplateModel.SubItemTemplateModel subItemTemplateModel, String keyDefine);
        void onClickDelete(ItemTemplateModel.SubItemTemplateModel subItemTemplateModel, String categoryKey, SwipeLayout swipe_layout);
        void onWriteSubItem(ItemTemplateModel.SubItemTemplateModel itemTemplateModel, String write_text, int position);
        void onClickYesNoNA(Context context, ItemTemplateModel.SubItemTemplateModel subItemTemplateModel, String categoryKey, SwipeLayout swipe_layout, String action_click);
    }
}
