package com.eazy.daikou.ui.home.complaint_solution.complaint.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.helper.DateUtil
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.complaint.ComplaintMainReplyModel
import java.util.ArrayList

class ComplaintSubReplyAdapter(private val action: String, private val list : ArrayList<ComplaintMainReplyModel>, private val onClickListener : OnClickItemListener) :
    RecyclerView.Adapter<ComplaintSubReplyAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.hotel_reply_comment_item_model, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBindingView(action, list[position], onClickListener)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val descriptionTv: TextView = itemView.findViewById(R.id.descriptionTv)
        private val nameTv: TextView = itemView.findViewById(R.id.nameTv)
        private val profileImage: ImageView = itemView.findViewById(R.id.profile_image)
        private val imageComment: ImageView = itemView.findViewById(R.id.imageComment)
        private val durationTv: TextView = itemView.findViewById(R.id.durationTv)
        private val commentTV: TextView = itemView.findViewById(R.id.commentTV)

        private val recyclerView : RecyclerView = itemView.findViewById(R.id.recyclerView)

        fun onBindingView(action: String, item : ComplaintMainReplyModel, onClickListener : OnClickItemListener){
            if (item.user != null){
                Utils.setValueOnText(nameTv, item.user!!.name)
                Glide.with(profileImage).load(if (item.user!!.image != null) item.user!!.image else R.drawable.ic_my_profile).into(profileImage)
            }

            Utils.setTextHtml(descriptionTv, item.description)

            if (item.created_at != null){
                durationTv.text = DateUtil.getDurationDate(durationTv.context, item.created_at)
            } else {
                durationTv.visibility = View.GONE
            }

            itemView.findViewById<CardView>(R.id.imageCardLayout).visibility = if (item.image != null) View.VISIBLE else View.GONE
            Glide.with(imageComment).load(if (item.image != null) item.image else R.drawable.no_image).into(imageComment)

            commentTV.setOnClickListener(CustomSetOnClickViewListener{
                onClickListener.onClickListener(item, "do_reply_comment")
            })

            // Sub reply comment
            commentTV.visibility = if (action == "sub_reply") View.GONE else View.VISIBLE

            recyclerView.layoutManager = LinearLayoutManager(recyclerView.context)
            recyclerView.isNestedScrollingEnabled = false
            val hotelTalkReplyCommentSubAdapter = ComplaintSubReplyAdapter("sub_reply", item.replies, object : OnClickItemListener{
                override fun onClickListener(item: ComplaintMainReplyModel, action: String) { }

                override fun onClickSubListener(item: ComplaintMainReplyModel, hotelTalkReplyCommentSubAdapter: ComplaintSubReplyAdapter) {
                    // onClickListener.onClickSubListener(item, hotelTalkReplyCommentSubAdapter)
                }
            })
            recyclerView.adapter = hotelTalkReplyCommentSubAdapter

            // Like
            itemView.findViewById<TextView>(R.id.txtLike).visibility = View.GONE
            itemView.findViewById<TextView>(R.id.txtTotalLike).visibility = View.GONE
        }

    }

    interface OnClickItemListener {
        fun onClickListener(item : ComplaintMainReplyModel, action : String)
        fun onClickSubListener(item : ComplaintMainReplyModel, hotelTalkReplyCommentSubAdapter : ComplaintSubReplyAdapter)
    }

    fun clear() {
        val size: Int = list.size
        if (size > 0) {
            for (i in 0 until size) {
                list.removeAt(0)
            }
            notifyItemRangeRemoved(0, size)
        }
    }
}