package com.eazy.daikou.ui.home.front_desk.fragment

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AbsListView
import android.widget.EditText
import android.widget.ImageView
import android.widget.ProgressBar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.request_data.request.front_desk_ws.FrontDeskWS
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.front_desk.SearchFrontDeskModel
import com.eazy.daikou.model.profile.User
import com.eazy.daikou.ui.home.front_desk.adapter.AdapterPickUpFrontDesk
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.gson.Gson

class PickerItemFragment : BottomSheetDialogFragment() {

    private lateinit var recyclerViewSignTo : RecyclerView
    private lateinit var progressBar : ProgressBar
    private var mContext : Context? = null
    private lateinit var searchEt : EditText
    private lateinit var iconClearText : ImageView
    private var listItemSearchFrontDesk : ArrayList<SearchFrontDeskModel> = ArrayList()
    private var user: User? = null
    private var accountID = ""
    private lateinit var adapterPickUp : AdapterPickUpFrontDesk
    private lateinit var callBackItemClick : CallBackItemAfterClick
    private lateinit var linearLayoutManager :  LinearLayoutManager
    private var page = 1
    private var currentItem = 0
    private var total : Int = 0
    private var scrollDown : Int = 0
    private var currentPage : Int = 1
    private var size : Int = 10
    private var isScrolling = false
    private var isScrollItem = false

    fun newInstance() : PickerItemFragment{
        val fragment = PickerItemFragment()
        val args = Bundle()
        //args.putString("action",action)
        fragment.arguments = args
        return fragment
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view =  inflater.inflate(R.layout.fragment_picker_item, container, false)

        initView(view)

        initData()

        initAction()

        return view
    }

    private fun initView(view : View){
        progressBar = view.findViewById(R.id.progressItem)
        recyclerViewSignTo = view.findViewById(R.id.recyclerViewSignTo)
        searchEt = view.findViewById(R.id.searchEt)
        iconClearText = view.findViewById(R.id.iconClearText)

    }
    private fun initData(){
        val sessionManagement = UserSessionManagement(mContext)
        user = Gson().fromJson(sessionManagement.userDetail, User::class.java)
        accountID = user!!.accountId
    }

    private fun initAction(){
        initOnScrollItem(recyclerViewSignTo)

        requestServiceAPI("")

        searchItemComplaint()


        linearLayoutManager = LinearLayoutManager(context)
        recyclerViewSignTo.layoutManager = linearLayoutManager
        adapterPickUp = AdapterPickUpFrontDesk(context!!, listItemSearchFrontDesk, callBackAdapter)
        recyclerViewSignTo.adapter = adapterPickUp
    }

    private val callBackAdapter = object : AdapterPickUpFrontDesk.CallBackClickItemAssign{
        override fun clickItemAssignListener(assignModel: SearchFrontDeskModel) {
            callBackItemClick.clickItemListener(assignModel)
            dismiss()
        }
    }

    private fun searchItemComplaint(){
        searchEt.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

            override fun afterTextChanged(editable: Editable) {
                iconClearText.setOnClickListener {
                    searchEt.setText("")
                    editable.clear()
                }
                if (editable.toString().isNotEmpty()) {
                    progressBar.visibility = View.VISIBLE
                    iconClearText.visibility = View.VISIBLE
                    listItemSearchFrontDesk.clear()
                    requestServiceAPI(editable.toString())
                } else {
                    listItemSearchFrontDesk.clear()
                    iconClearText.visibility = View.GONE
                    requestServiceAPI( "")
                }
            }
        })
    }

    private fun initOnScrollItem(recyclerViewItem: RecyclerView) {
        recyclerViewItem.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true
                }
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                currentItem = linearLayoutManager.childCount
                total = linearLayoutManager.itemCount
                scrollDown = linearLayoutManager.findFirstVisibleItemPosition()
                if (dy > 0) {
                    if (isScrolling && currentItem + scrollDown == total) {
                        if (total == size) {
                            isScrolling = false
                            currentPage++
                            progressBar.visibility = View.VISIBLE
                            isScrollItem = true
                            recyclerViewItem.scrollToPosition(listItemSearchFrontDesk.size - 1)
                            requestServiceAPI("")
                            size += 10
                        } else {
                            progressBar.visibility = View.GONE
                        }
                    }
                }
            }
        })
    }


    private fun requestServiceAPI(  keySearch : String){
        progressBar.visibility = View.VISIBLE
        FrontDeskWS().searchFrontDeskWS( mContext!!, page, 10, accountID, keySearch, callBackSearchFrontDesk)
    }

    private val callBackSearchFrontDesk = object : FrontDeskWS.CallBackSearchFrontDeskListener{
        override fun onSuccessFul(listSearch: ArrayList<SearchFrontDeskModel>) {
            progressBar.visibility = View.GONE
            listItemSearchFrontDesk.addAll(listSearch)
            adapterPickUp.notifyDataSetChanged()

        }

        override fun onFailed(mess: String) {
            progressBar.visibility = View.GONE
            Utils.customToastMsgError(mContext, mess, false)
        }
    }

    fun initDataListener(callBackItemClick : CallBackItemAfterClick){
        this.callBackItemClick = callBackItemClick
    }

    interface CallBackItemAfterClick{
        fun clickItemListener(searchFrontDesk: SearchFrontDeskModel)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
    }
}