package com.eazy.daikou.ui.home.my_property.my_unit

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AbsListView
import android.widget.ProgressBar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.CustomCategoryModel
import com.eazy.daikou.model.my_property.payment_schedule_my_unit.OperationPaymentInfoModel
import com.eazy.daikou.ui.CategoryItemAdapter
import com.eazy.daikou.ui.home.my_property.adapter.OperationPaymentUnitAdapter

class PropertyOperationPaymentActivity : BaseActivity() {

    private lateinit var progressBar: ProgressBar
    private lateinit var recyclerView: RecyclerView
    private lateinit var categoryRecycler : RecyclerView
    private lateinit var operationPaymentUnitAdapter: OperationPaymentUnitAdapter
    private var operationPaymentSchedulesList: ArrayList<OperationPaymentInfoModel> = ArrayList()
    private lateinit var refreshLayout: SwipeRefreshLayout
    private var currentItem = 0
    private var total : Int = 0
    private var scrollDown : Int = 0
    private var currentPage : Int = 1
    private var size : Int = 10
    private var isScrolling = false
    private lateinit var linearLayoutManager : LinearLayoutManager
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_property_operation_payment)

        initView()

        initAction()

    }

    private fun initView(){
        Utils.customOnToolbar(this, resources.getString(R.string.operation_payment)){finish()}

        progressBar = findViewById(R.id.progressItem)
        progressBar.visibility = View.GONE
        refreshLayout = findViewById(R.id.swipe_layouts)

        recyclerView  = findViewById(R.id.recyclerView)
        categoryRecycler = findViewById(R.id.categoryRecycler)
    }

    private fun initAction(){
        linearLayoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = linearLayoutManager

        initCategoryRecyclerView()

        initRecyclerView()

        refreshLayout.setColorSchemeResources(R.color.colorPrimary)
        refreshLayout.setOnRefreshListener { refreshList() }
        
        initOnScroll()
    }

    private fun initCategoryRecyclerView(){
        categoryRecycler.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        val categoryList : ArrayList<CustomCategoryModel> = ArrayList()

        var item = CustomCategoryModel("water", "Water", true)
        categoryList.add(item)
        item = CustomCategoryModel("electric", "Electric", false)
        categoryList.add(item)
        item = CustomCategoryModel("cleaning", "Cleaning", false)
        categoryList.add(item)
        item = CustomCategoryModel("internet", "Internet", false)
        categoryList.add(item)
        item = CustomCategoryModel("security", "Security", false)
        categoryList.add(item)

        val categoryAdapter = CategoryItemAdapter(categoryList, object : CategoryItemAdapter.ClickCallBackListener{
            @SuppressLint("NotifyDataSetChanged")
            override fun onClickCallBack(item: CustomCategoryModel) {
                for (category in categoryList){
                    category.isClick = category.id == item.id
                }
                categoryRecycler.adapter!!.notifyDataSetChanged()

                refreshList()
            }
        })
        categoryRecycler.adapter = categoryAdapter
    }

    private fun refreshList(){
        operationPaymentUnitAdapter.clear()

        requestListOperationList()
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun requestListOperationList(){
        for (i in 1..10){
            val item = OperationPaymentInfoModel()
            operationPaymentSchedulesList.add(item)

        }
        progressBar.visibility = View.GONE
        operationPaymentUnitAdapter.notifyDataSetChanged()
    }

    private fun initRecyclerView(){
        operationPaymentUnitAdapter = OperationPaymentUnitAdapter(this, operationPaymentSchedulesList, object : OperationPaymentUnitAdapter.OnCallBackListenerPayment{
            override fun onClickBackItemPayment(paymentsModel: OperationPaymentInfoModel) {

            }

        })
        recyclerView.adapter = operationPaymentUnitAdapter

        requestListOperationList()
    }

    private fun initOnScroll() {
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true
                }
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                currentItem = linearLayoutManager.childCount
                total = linearLayoutManager.itemCount
                scrollDown = linearLayoutManager.findFirstVisibleItemPosition()
                if (dy > 0) {
                    if (isScrolling && currentItem + scrollDown == total) {
                        if (total == size) {
                            isScrolling = false
                            currentPage++
                            progressBar.visibility = View.VISIBLE
                            recyclerView.scrollToPosition(operationPaymentSchedulesList.size - 1)
                            initRecyclerView()
                            size += 10
                        }
                    }
                }
            }
        })
    }

}