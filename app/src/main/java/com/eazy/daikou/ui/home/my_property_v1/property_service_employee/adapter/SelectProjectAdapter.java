package com.eazy.daikou.ui.home.my_property_v1.property_service_employee.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.eazy.daikou.R;
import com.eazy.daikou.model.my_property.service_provider.SelectProjectModel;
import com.squareup.picasso.Picasso;

import java.util.List;

public class SelectProjectAdapter extends RecyclerView.Adapter<SelectProjectAdapter.ItemViewHolder> {
    private final ClickCallBackListener clickCallBackListener;
    private final Context context;
    private final List<SelectProjectModel> projectModelList;
    private final String key;

    public SelectProjectAdapter(Context context, String key, List<SelectProjectModel> projectModelList, ClickCallBackListener clickCallBackListener) {
        this.clickCallBackListener = clickCallBackListener;
        this.context = context;
        this.projectModelList = projectModelList;
        this.key = key;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rowView = LayoutInflater.from(parent.getContext()).inflate(R.layout.select_project_model,parent,false);
        return new ItemViewHolder(rowView);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        SelectProjectModel selectProjectModel = projectModelList.get(position);
        if (selectProjectModel != null){
            if (key.equals("ALL_PROJECT") || key.equals("SEARCH")){
                holder.nameProject.setText(selectProjectModel.getBranchName() != null ? selectProjectModel.getBranchName() :
                        String.format("%s", context.getResources().getString(R.string.not)));
            } else {
                holder.nameProject.setText(selectProjectModel.getBuildingName() != null ? selectProjectModel.getBuildingName() :
                        String.format("%s", context.getResources().getString(R.string.not)));
            }

            if (selectProjectModel.getFilePath() != null){
                Picasso.get().load(selectProjectModel.getFilePath()).into(holder.iconProject);
            } else {
                Picasso.get().load(R.drawable.no_image).into(holder.iconProject);
            }

            holder.itemView.setOnClickListener(view -> clickCallBackListener.ClickCallBack(selectProjectModel));

        }
    }

    @Override
    public int getItemCount() {
        return projectModelList.size();
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder{
        private final TextView nameProject;
        private final ImageView iconProject;
        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            nameProject = itemView.findViewById(R.id.titleProject);
            iconProject = itemView.findViewById(R.id.img_project);
        }
    }

    public interface ClickCallBackListener{
        void ClickCallBack(SelectProjectModel selectProjectModel);
    }

    public void clear() {
        int size = projectModelList.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                projectModelList.remove(0);
            }
            notifyItemRangeRemoved(0, size);
        }
    }
}
