package com.eazy.daikou.ui.home.book_now.booking_hotel

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.base.MockUpData
import com.eazy.daikou.request_data.request.book_now_ws.BookingHotelWS
import com.eazy.daikou.helper.*
import com.eazy.daikou.model.booking_hotel.ExtraPrice
import com.eazy.daikou.model.booking_hotel.HotelBookingDetailModel
import com.eazy.daikou.model.booking_hotel.ItemRoomHotelBookingModel
import com.eazy.daikou.model.booking_hotel.LocationHotelModel
import com.eazy.daikou.ui.home.book_now.booking_hotel.adapter.SelectRoomBookingAdapter
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class HotelBookingSelectRoomActivity : BaseActivity(), View.OnClickListener {

    private lateinit var progressBar: ProgressBar
    private lateinit var recyclerView: RecyclerView
    private lateinit var totalItemTv : TextView
    private lateinit var totalTv : TextView
    private var itemRoomHotelBookingModelList : ArrayList<ItemRoomHotelBookingModel> = ArrayList()
    private lateinit var selectRoomBookingAdapter: SelectRoomBookingAdapter
    private var totalPrice : Double = 0.0
    private lateinit var btnBooking : TextView
    private var hotelId = ""
    private var adults = "1"
    private var children = "0"
    private var startDate : String = ""
    private var endDate : String = ""
    private lateinit var dateFormatServer : SimpleDateFormat
    private lateinit var adultsTv : TextView
    private lateinit var childrenTv : TextView
    private lateinit var startDateEndDateTv : LinearLayout
    private lateinit var startDateTv : TextView
    private lateinit var endDateTv : TextView
    private lateinit var guestLayout : LinearLayout
    private var category = ""

    private lateinit var paymentFeeLayout : LinearLayout
    private var extraPriceList = ArrayList<ExtraPrice>()
    private var totalPriceFeeService = 0.0
    private var getHotelDetailItemModel: HotelBookingDetailModel? = null
    private val serviceFeeList = ArrayList<ExtraPrice>()
    private lateinit var dateStartEndTv : TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_go_to_booking_hotel)

        initView()

        initAction()

    }

    private fun initView(){
        Utils.customOnToolbar(this, resources.getString(R.string.select_room).uppercase()){finish()}
        progressBar = findViewById(R.id.progressItem)
        recyclerView = findViewById(R.id.recyclerView)
        totalItemTv = findViewById(R.id.totalItemTv)
        totalTv = findViewById(R.id.totalTv)
        btnBooking = findViewById(R.id.linear_card)
        adultsTv = findViewById(R.id.adultsTv)
        childrenTv = findViewById(R.id.childrenTv)
        startDateEndDateTv = findViewById(R.id.startDateEndDateTv)
        startDateTv = findViewById(R.id.startDateTv)
        endDateTv = findViewById(R.id.endDateTv)
        guestLayout = findViewById(R.id.guestLayout)
        paymentFeeLayout = findViewById(R.id.paymentFeeLayout)
        dateStartEndTv = findViewById(R.id.dateTv)
    }

    private fun initAction(){
        hotelId = GetDataUtils.getDataFromString("hotel_id", this)
        startDate = GetDataUtils.getDataFromString("start_date", this)
        endDate = GetDataUtils.getDataFromString("end_date", this)
        children = GetDataUtils.getDataFromString("children", this)
        adults = GetDataUtils.getDataFromString("adults", this)
        category = GetDataUtils.getDataFromString("category", this)
        dateFormatServer = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        if (intent != null && intent.hasExtra("item_detail_model")) {
            getHotelDetailItemModel = intent.getSerializableExtra("item_detail_model") as HotelBookingDetailModel
            //Service Fee Payment
            serviceFeeList.addAll(getHotelDetailItemModel!!.buyer_fees)
            serviceFeeList.addAll(getHotelDetailItemModel!!.service_fees)

            //Extra Price
            extraPriceList.addAll(getHotelDetailItemModel!!.extra_price)

            addViewItem(paymentFeeLayout, serviceFeeList)
        }

        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.isNestedScrollingEnabled = false
        selectRoomBookingAdapter = SelectRoomBookingAdapter(this, itemRoomHotelBookingModelList, onClickItemCallBackListener)
        recyclerView.adapter = selectRoomBookingAdapter

        if(adults == "")    adults = "1"
        if(children == "")    children = "0"
        adultsTv.text = String.format("%s %s", adults, resources.getString(R.string.adults))
        childrenTv.text = String.format("%s %s", children, resources.getString(R.string.children))

        getStartEndDate()

        requestData()

        checkValidateButtonPay()

        startDateEndDateTv.setOnClickListener(this)
        guestLayout.setOnClickListener(this)
        btnBooking.setOnClickListener(this)
    }

    private fun addViewItem(eventLayout : LinearLayout, list : ArrayList<ExtraPrice>){
        totalPriceFeeService = 0.0
        for (item in list) {
            val view: View = LayoutInflater.from(this).inflate(R.layout.custom_textview_left_right, null)
            val keyTv = view.findViewById(R.id.keyTv) as TextView
            val valueTv = view.findViewById(R.id.valueTv) as TextView
            keyTv.text = item.name
            valueTv.text = item.price_display.toString()

            totalPriceFeeService += item.price

            eventLayout.addView(view)
        }
        if (list.size == 0) findViewById<CardView>(R.id.feeServiceLayout).visibility = View.GONE
    }

    private fun getStartEndDate(){
        // Start Date
        var dateToday = Date()
        if (startDate == "")    startDate = dateFormatServer.format(dateToday)
        startDateTv.text = startDate

        // Due Date
        val calendar = Calendar.getInstance()
        calendar.time = dateToday
        calendar.add(Calendar.DATE, 1)
        dateToday = calendar.time
        if (endDate == "")    endDate = dateFormatServer.format(dateToday)
        endDateTv.text = endDate
    }

    private val onClickItemCallBackListener = object : SelectRoomBookingAdapter.OnClickCallBackLister{
        @SuppressLint("NotifyDataSetChanged")
        override fun onClickSpinnerCallBack(itemRoomHotelBookingModel: ItemRoomHotelBookingModel, action : String, imageView : ImageView) {
            when(action){
                "add" ->{
                    var numberCount = itemRoomHotelBookingModel.quantity
                    if (itemRoomHotelBookingModel.number > 0 && itemRoomHotelBookingModel.number > itemRoomHotelBookingModel.quantity) {
                        numberCount = numberCount.plus(1)
                        itemRoomHotelBookingModel.totalAllPrice = (itemRoomHotelBookingModel.price * numberCount)

                        itemRoomHotelBookingModel.quantity = numberCount

                        itemRoomHotelBookingModel.totalRoom = numberCount
                        itemRoomHotelBookingModel.isClick = numberCount > 0
                        itemRoomHotelBookingModel.numberRoomSelect = numberCount
                        selectRoomBookingAdapter.notifyDataSetChanged()

                        checkValidateButtonPay()
                    }
                }
                "remove" ->{
                    var numberCount = itemRoomHotelBookingModel.quantity
                    if (itemRoomHotelBookingModel.number > 0 && numberCount > 0) {
                        numberCount = numberCount.minus(1)
                        itemRoomHotelBookingModel.totalAllPrice = (itemRoomHotelBookingModel.price * numberCount)

                        itemRoomHotelBookingModel.quantity = numberCount

                        itemRoomHotelBookingModel.totalRoom = numberCount
                        itemRoomHotelBookingModel.isClick = numberCount > 0
                        itemRoomHotelBookingModel.numberRoomSelect = numberCount
                        selectRoomBookingAdapter.notifyDataSetChanged()

                        checkValidateButtonPay()
                    }
                }
            }
        }
    }

    private fun checkValidateButtonPay(){
        if (isEnableButtonPay()){
            btnBooking.isEnabled = true
            Utils.setBgTint(btnBooking, R.color.color_book_now)
        } else {
            btnBooking.isEnabled = false
            Utils.setBgTint(btnBooking, R.color.gray)
        }

        setValueOnPrice()

        // duration Night
        durationDay()
    }

    private fun setValueOnPrice() {
        var itemSelect = 0
        totalPrice = 0.0
        for (itemCardBooking in itemRoomHotelBookingModelList) {
            if (itemCardBooking.isClick) {
                val subTotal: Double = itemCardBooking.totalAllPrice
                totalPrice += subTotal
                itemSelect += itemCardBooking.numberRoomSelect
            }
        }

        // Price total can book by date
        val getTotalPriceFeeService = totalPriceFeeService
        totalPrice += (totalPrice * getTotalPriceFeeService) / 100 // totalAllPriceItemSelect + ( totalAllPriceItemSelect * totalPriceFeeService ) / 100

        totalTv.text = String.format("%s %s", "$", Utils.formatDecimalFormatValue(totalPrice))
        totalItemTv.text = String.format("%s ", itemSelect)
    }

    private fun isEnableButtonPay() : Boolean{
        for (itemCardBooking in itemRoomHotelBookingModelList){
            if (itemCardBooking.isClick){
                return true
            }
        }
        return false
    }

    private fun refreshList(){
        selectRoomBookingAdapter.clear()
        requestData()
        checkValidateButtonPay()
    }

    private fun requestData(){
        progressBar.visibility = View.VISIBLE
        BookingHotelWS().getRoomAll(this, hotelId, adults, children, startDate , endDate, onCallBackRequestListener)
    }

    private val onCallBackRequestListener = object : BookingHotelWS.OnCallBackRoomHotelListener{
        @SuppressLint("NotifyDataSetChanged")
        override fun onSuccessRoomHotel(locationHotelModel: ArrayList<ItemRoomHotelBookingModel>) {
            progressBar.visibility = View.GONE
            itemRoomHotelBookingModelList.addAll(locationHotelModel)

            selectRoomBookingAdapter.notifyDataSetChanged()

            checkValidateButtonPay()

            Utils.validateViewNoItemFound(this@HotelBookingSelectRoomActivity, recyclerView, itemRoomHotelBookingModelList.size <= 0)
        }

        override fun onSuccessBookingRoom(bookingId: String, bookingCode: String, kessPayment : String) {
            progressBar.visibility = View.GONE
            val intent = Intent(this@HotelBookingSelectRoomActivity, HotelBookingAddAddressPaymentActivity::class.java)
            intent.putExtra("booking_id", bookingId)
            intent.putExtra("booking_code", bookingCode)
            intent.putExtra("kess_payment", kessPayment)
            intent.putExtra("adults", adults)
            intent.putExtra("children", children)
            intent.putExtra("price", String.format("%s %s", "$", Utils.formatDecimalFormatValue(totalPrice)))
            intent.putExtra("start_date", startDate)
            intent.putExtra("end_date", endDate)
            intent.putExtra("category", category)
            if (getHotelDetailItemModel != null){
                if (getHotelDetailItemModel!!.name != null){
                    intent.putExtra("hotel_name", getHotelDetailItemModel!!.name)
                }
                if (getHotelDetailItemModel!!.image != null){
                    intent.putExtra("hotel_image", getHotelDetailItemModel!!.image)
                }
            }

            intent.putExtra("hashmap_draft_data", bookingRoomData())
            startActivity(intent)
        }

        override fun onFailed(message: String) {
            progressBar.visibility = View.GONE
            Utils.customToastMsgError(this@HotelBookingSelectRoomActivity, message, false)
        }
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.guestLayout -> {
                Utils.checkEnableView(v)
                startSelectCategory("select_room")
            }
            R.id.startDateEndDateTv -> {
                Utils.checkEnableView(v)
                startEndDateRang()
            }
            R.id.linear_card ->{
                Utils.checkEnableView(v)
                if (AppAlertCusDialog.isSuccessLoggedIn(this)) {
                    progressBar.visibility = View.VISIBLE
                    BookingHotelWS().bookingRoomDraft(this, bookingRoomData(), onCallBackRequestListener)
                }
            }
        }
    }

    private fun startSelectCategory(action: String){
        val selectLocationBookingFragment = SelectLocationBookingFragment().newInstance("no_room_check", adults, children, action)
        selectLocationBookingFragment.initListener(onCallBackListener)
        selectLocationBookingFragment.show(supportFragmentManager, "fragment")
    }

    private val onCallBackListener = object : SelectLocationBookingFragment.OnClickCallBackListener{
        override fun onClickSelectLocation(titleHeader: LocationHotelModel) {}

        override fun onClickSelect(titleHeader: String, r : String, ad : String, ch : String) {
            adults = ad
            children = ch
            adultsTv.text = String.format("%s %s", adults, resources.getString(R.string.adults))
            childrenTv.text = String.format("%s %s", children, resources.getString(R.string.children))
            refreshList()
        }
    }

    private fun startEndDateRang(){
        val selectLocationBookingFragment = SelectStartEndDateRangBottomsheet.newInstance(startDate, endDate)
        selectLocationBookingFragment.initListener(onCallBackDateListener)
        selectLocationBookingFragment.show(supportFragmentManager, "fragment")
    }

    private val onCallBackDateListener = object : SelectStartEndDateRangBottomsheet.OnClickCallBackListener{
        override fun onClickSelect(getStartDate: String, getEndDate : String) {
            startDate = getStartDate
            endDate = getEndDate
            startDateTv.text = startDate
            endDateTv.text = endDate
            refreshList()
        }
    }

    private fun bookingRoomData() : HashMap<String, Any>{
        val hashMap : HashMap<String, Any> = HashMap()
        val totalGuest = adults.toInt() + children.toInt()
        hashMap["hotel_id"] = hotelId
        hashMap["start_date"] = startDate
        hashMap["end_date"] = endDate
        hashMap["total"] = totalPrice
        hashMap["total_guests"] = totalGuest.toString()
        hashMap["hotel_user_id"] = Utils.validateNullValue(MockUpData.getEazyHotelUserId(UserSessionManagement(this)))
        val listHashmap: ArrayList<HashMap<String, Any?>> = ArrayList()
        for (item in itemRoomHotelBookingModelList){
            if(item.isClick) {
                val hashMapSub = HashMap<String, Any?>()
                hashMapSub["room_id"] = item.room_id
                hashMapSub["total_room"] = item.totalRoom
                hashMapSub["total_price"] = item.totalAllPrice
                listHashmap.add(hashMapSub)
            }
        }
        hashMap["booking_rooms"] = listHashmap
        hashMap["category"] = category
        return hashMap
    }

    private fun durationDay(){
        val day = DateUtil.calculateDuring(startDate, endDate, SimpleDateFormat("yyyy-MM-dd")).toInt()// StartDate / EndDate format "yyyy-MM-dd"
        dateStartEndTv.text = String.format("%s %s", day, resources.getString(R.string.night).lowercase())
    }
}