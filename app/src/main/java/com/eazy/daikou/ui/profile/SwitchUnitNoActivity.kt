package com.eazy.daikou.ui.profile

import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import androidx.activity.result.ActivityResult
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.helper.GetDataUtils
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.profile.UnitNoModel
import com.eazy.daikou.ui.profile.adapter.SwitchUnitNoAdapter

class SwitchUnitNoActivity : BaseActivity() {

    private lateinit var recyclerView : RecyclerView
    private lateinit var progressLayout : ProgressBar
    private lateinit var switchProfilePropertyAdapter : SwitchUnitNoAdapter
    private var unitNoList : ArrayList<UnitNoModel> = ArrayList()
    private var unitIdSelected : String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_unit_no2)

        initView()

        initData()

        initAction()

    }

    private fun initView(){
        recyclerView = findViewById(R.id.recyclerView)
        progressLayout = findViewById(R.id.progressItem)
        progressLayout.visibility = View.GONE

        Utils.customOnToolbar(this, resources.getString(R.string.select_unit)) { finish() }
    }

    private fun initData(){
        unitIdSelected = GetDataUtils.getDataFromString("id", this)
    }

    private fun initAction(){
        Utils.customOnToolbar(this, resources.getString(R.string.select_unit)){finish()}

        initRecyclerView()
    }

    private fun initRecyclerView(){
        for (i in 1..10){
            val item = UnitNoModel()
            item.id = i.toString()
            item.name = "A $i"
            item.property = "Eaon Mall Phnom Penh $i"
            unitNoList.add(item)
        }
        for (item in unitNoList) {
            item.isClick = item.id == unitIdSelected
        }

        recyclerView.layoutManager = LinearLayoutManager(this)
        switchProfilePropertyAdapter = SwitchUnitNoAdapter(this, unitNoList, object : SwitchUnitNoAdapter.OnClickCallBackListener{
            override fun onClickCallBack(unitNoModel: UnitNoModel) {
                setBackValue(unitNoModel)
            }
        })
        recyclerView.adapter = switchProfilePropertyAdapter

        Utils.validateViewNoItemFound(this, recyclerView, unitNoList.size <= 0)
    }

    private fun setBackValue(unitNoModel: UnitNoModel){
        intent.putExtra("unit_model", unitNoModel)
        setResult(RESULT_OK, intent)
        finish()
    }

}