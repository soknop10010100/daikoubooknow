package com.eazy.daikou.ui.home.hrm.leave_request.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.hr.LeaveManagement
import org.w3c.dom.Text


class LeaveAttendanceAdapter(private val listName: ArrayList<LeaveManagement>, private val context : Context, private val itemClick: ItemClickLeaveListener) : RecyclerView.Adapter<LeaveAttendanceAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.layout_adapter_leave_attendance, parent, false)
        return ViewHolder(v)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val leave: LeaveManagement = listName[position]

        holder.ticketNoTv.text = leave.id ?: "- - -"
        holder.employeeNameTv.text = leave.name ?: "- - -"
        holder.createDateTv.text = Utils.formatDateTime(leave.created_at, "yyyy-MM-dd kk:mm:ss", "dd-MMM-yyyy hh:mm a") ?: "- - -"
        holder.assignToTv.text = leave.leave_category_id ?: "- - -"
        holder.leaveTypeTv.text = leave.leave_category ?: "- - -"
        holder.startDateTv.text = leave.start_date ?: "- - -"
        holder.endDateTv.text = leave.end_date ?: "- - -"
        holder.periodTv.text = if(leave.period != null) String.format("%s %s", leave.period, context.resources.getString(R.string.day_s).lowercase()) else "- - -"

        when (leave.status) {
            "accepted" -> {
                holder.statusTv.text = context.resources.getString(R.string.accept)
                holder.statusTv.backgroundTintList =
                    ColorStateList.valueOf(ContextCompat.getColor(context, R.color.green))
                holder.view.backgroundTintList =
                    ColorStateList.valueOf(ContextCompat.getColor(context, R.color.green))
            }
            "pending" -> {
                holder.statusTv.backgroundTintList =
                    ColorStateList.valueOf(ContextCompat.getColor(context, R.color.yellow))
                holder.statusTv.text = context.resources.getString(R.string.pending)
                holder.view.backgroundTintList =
                    ColorStateList.valueOf(ContextCompat.getColor(context, R.color.yellow))
            }
            "rejected" -> {
                holder.statusTv.backgroundTintList =
                    ColorStateList.valueOf(ContextCompat.getColor(context, R.color.red))
                holder.statusTv.text = context.resources.getString(R.string.rejected)
                holder.view.backgroundTintList =
                    ColorStateList.valueOf(ContextCompat.getColor(context, R.color.red))
            }
            else -> {
                holder.statusTv.backgroundTintList =
                    ColorStateList.valueOf(ContextCompat.getColor(context, R.color.red))
                holder.statusTv.text = context.resources.getString(R.string.rejected)
                holder.view.backgroundTintList =
                    ColorStateList.valueOf(ContextCompat.getColor(context, R.color.red))
            }
        }

        holder.itemView.setOnClickListener {
            itemClick.onItemClick(leave)
        }

    }

    override fun getItemCount(): Int {
        return listName.size
    }

    interface ItemClickLeaveListener{
        fun onItemClick(leave: LeaveManagement)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val ticketNoTv : TextView = view.findViewById(R.id.ticket_no)
        val employeeNameTv : TextView = view.findViewById(R.id.employeeNameTv)
        val createDateTv : TextView = view.findViewById(R.id.createDateTv)
        val assignToTv : TextView = view.findViewById(R.id.assign_to)
        val leaveTypeTv : TextView = view.findViewById(R.id.leave_type)
        val startDateTv : TextView = view.findViewById(R.id.leave_start_date)
        val endDateTv : TextView = view.findViewById(R.id.leave_end_date)
        val periodTv : TextView = view.findViewById(R.id.periodTv)
        val statusTv : TextView = view.findViewById(R.id.status)
        val view : View = view.findViewById(R.id.view)

    }

    fun clear() {
        val size: Int = listName.size
        if (size > 0) {
            for (i in 0 until size) {
                listName.removeAt(0)
            }
            notifyItemRangeRemoved(0, size)
        }
    }

}