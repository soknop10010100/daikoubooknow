package com.eazy.daikou.ui.sign_up;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Window;

import com.eazy.daikou.R;
import com.eazy.daikou.base.BaseActivity;
import com.eazy.daikou.base.MockUpData;
import com.eazy.daikou.helper.Utils;
import com.eazy.daikou.model.profile.NationalityList;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class NationalityActivity extends BaseActivity {

    private List<NationalityList> listNationality = new ArrayList<>();
    private int stat = -1;
    private String action = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nationality);
        RecyclerView recyclerView = findViewById(R.id.list_nationality);

        if (getIntent() != null && getIntent().hasExtra("action")){
            action = getIntent().getStringExtra("action");
        }

        if (action.equalsIgnoreCase("nationality")){
            setTitle(getResources().getString(R.string.nationality));
            getNationality();
        } else {
            setTitle(getResources().getString(R.string.estimate_time));
            listNationality = getListEstimateTime();
        }


        if (MockUpData.getStat()!=null){
            stat = MockUpData.getStat();
        }

        recyclerView.setLayoutManager(new LinearLayoutManager(NationalityActivity.this));

        NationalityNotifyAdapter adapter = new NationalityNotifyAdapter(NationalityActivity.this, listNationality, stat, (id, name, post, sat) -> {
            stat = sat;
            MockUpData.setStat(stat);
            Intent intent = getIntent();
            if (action.equalsIgnoreCase("nationality")) {
                intent.putExtra("name_national", name != null ? name : "");
                intent.putExtra("id_national", id != null ? id : "");
            } else {
                intent.putExtra("estimate_time_name", name != null ? name : "");
                intent.putExtra("estimate_time", id != null ? id : "");
            }
            setResult(RESULT_OK, intent);
            finish();
        });

        recyclerView.setAdapter(adapter);
    }

    private List<NationalityList> getListEstimateTime(){
        List<NationalityList> list = new ArrayList<>();
        for (int i = 1; i <= 500; i++) {
            NationalityList nationalityActivity = new NationalityList();
            nationalityActivity.setNationality(i + " ".concat(getResources().getString(R.string.hour_s).toLowerCase(Locale.ROOT)));
            nationalityActivity.setNaum_code(i + "");
            list.add(nationalityActivity);
        }
        return list;
    }

    private void getNationality(){
        String jsonFileString = Utils.getJsonFromAssets(NationalityActivity.this, "NationalityList.json");
        Utils.logDebug("data", jsonFileString);

        Gson gson = new Gson();
        Type listUserType = new TypeToken<List<NationalityList>>() { }.getType();

        if (jsonFileString != null) {
            listNationality = gson.fromJson(jsonFileString, listUserType);
        }
        for (int i = 0; i < (listNationality != null ? listNationality.size() : 0); i++) {
            Utils.logDebug("data", "> Item " + i + "\n" + listNationality.get(i));
        }

    }
}