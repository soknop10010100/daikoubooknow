package com.eazy.daikou.ui.home.service_provider

import android.app.Activity
import android.app.ProgressDialog.show
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseCameraActivity
import com.eazy.daikou.base.BaseFragment
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.work_order.ImageListModel
import com.eazy.daikou.ui.home.inspection_work_order.adapter.ImageListWorkOrderAdapter
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.coroutines.supervisorScope


class ServiceProviderRegisterFragment : BaseFragment() {

    private lateinit var addressProviderTv: TextView
    private lateinit var serviceCategoryTv: TextView
    private lateinit var btnAddImg: RelativeLayout
    private lateinit var list_image: RecyclerView
    private lateinit var txtNoImageAdd: TextView
    private lateinit var btnSave: TextView
    private var post = 0
    private var addressLocation = ""

    private val bitmapList: ArrayList<ImageListModel> = ArrayList()
    private val REQUEST_IMAGE = 882
    private val REQUEST_ADDRESS_CODE = 899

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, avedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_register_service_provider, container, false)

        initView(view)
        initData()
        initAction()

        return view
    }
    private fun initView(view: View){
        addressProviderTv = view.findViewById(R.id.addressProviderTv)
        serviceCategoryTv = view.findViewById(R.id.serviceCategoryTv)
        btnAddImg = view.findViewById(R.id.btnAddImg)
        list_image = view.findViewById(R.id.list_image)
        txtNoImageAdd = view.findViewById(R.id.txtNoImageAdd)
        btnSave = view.findViewById(R.id.btnSave)
    }
    private fun initData(){
    }
    private fun initAction(){
        btnAddImg.setOnClickListener {
            startActivityForResult(Intent(mActivity, BaseCameraActivity::class.java), REQUEST_IMAGE)
        }

        addressProviderTv.setOnClickListener {
            startActivityForResult(Intent(mActivity, AddressServiceProviderActivity::class.java), REQUEST_ADDRESS_CODE)
        }
        serviceCategoryTv.setOnClickListener {
            bottomSheetCategoryService()
        }
    }

    private fun bottomSheetCategoryService(){
        val bottomSheetFragment = ServiceProviderServiceCategoryFragment()
        activity?.supportFragmentManager?.let { bottomSheetFragment.show(it, bottomSheetFragment.tag) }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_IMAGE) {
            assert(data != null)
            if (data!!.hasExtra("image_path")) {
                val imagePath = data.getStringExtra("image_path")
                addUrlImage("", imagePath!!)
                setImageRecyclerView(post)
            }
            if (data.hasExtra("select_image")) {
                val imagePath = data.getStringExtra("select_image")
                addUrlImage("", imagePath!!)
                setImageRecyclerView(post)
            }
        }
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_ADDRESS_CODE){
            if (data!!.hasExtra("address_location")|| data!!.hasExtra("lat")|| data!!.hasExtra("lng")){
                val address = data.getStringExtra("address_location")
                val latLocation = data.getStringExtra("lat")
                val longLocation = data.getStringExtra("lng")
                addressLocation = address.toString()
                addressProviderTv.text = addressLocation
            }
        }
    }

    private fun addUrlImage(posKeyDefine: String, imagePath: String) {
        val loadedBitmap = BaseCameraActivity.convertByteToBitmap(imagePath)
        post += 1
        val imageListModel = ImageListModel()
        imageListModel.keyImage = posKeyDefine
        imageListModel.bitmapImage = loadedBitmap
        bitmapList.add(imageListModel)
    }

    private fun setImageRecyclerView(post: Int) {
        list_image.layoutManager = GridLayoutManager(mActivity, 1, RecyclerView.HORIZONTAL, false)
        val pickUpAdapter = ImageListWorkOrderAdapter(mActivity, post, bitmapList, onClearImage)
        list_image.adapter = pickUpAdapter
        btnAddImg.visibility = Utils.visibleView(bitmapList)
    }

    private val onClearImage: ImageListWorkOrderAdapter.OnClearImage =
        object : ImageListWorkOrderAdapter.OnClearImage {
            override fun onClickRemove(bitmap: ImageListModel, post: Int) {
                removeImage(post, bitmap)
            }

            override fun onViewImage(bitmap: ImageListModel) {
                if (bitmap.bitmapImage != null) {
                    Utils.openImageOnDialog(mActivity, Utils.convert(bitmap.bitmapImage))
                }
            }
        }

    private fun removeImage(position: Int, bitmap: ImageListModel) {
        bitmapList.remove(bitmap)
        setImageRecyclerView(position)
    }

}