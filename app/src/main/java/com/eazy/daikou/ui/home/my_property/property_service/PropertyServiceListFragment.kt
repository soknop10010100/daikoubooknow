package com.eazy.daikou.ui.home.my_property.property_service

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.activity.result.ActivityResult
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.base.BaseFragment
import com.eazy.daikou.databinding.FragmentPropertyServiceListBinding
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.my_property.property_service.PropertyServiceListModel
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.ui.home.my_property.property_service.adapter.PropertyServiceListAdapter

class PropertyServiceListFragment : BaseFragment() {

    private lateinit var mBinding : FragmentPropertyServiceListBinding
    private lateinit var progressBar : ProgressBar
    private lateinit var recyclerView : RecyclerView
    private lateinit var propertyServiceAdapter : PropertyServiceListAdapter
    private var propertyServiceList = ArrayList<PropertyServiceListModel>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {

        mBinding = FragmentPropertyServiceListBinding.inflate(inflater, container, false)

        initView()

        initAction()

        return mBinding.root
    }

    private fun initView() {
        progressBar = mBinding.progressBar.progressItem
        recyclerView = mBinding.itemLayout.recyclerView.recyclerView
    }

    private fun initAction() {
        recyclerView.layoutManager = LinearLayoutManager(mActivity)

        propertyServiceAdapter = PropertyServiceListAdapter(mActivity, propertyServiceList, onClickItemListener)
        recyclerView.adapter = propertyServiceAdapter

        getListTest()

        mBinding.itemLayout.swipeLayouts.setColorSchemeResources(R.color.colorPrimary)
        mBinding.itemLayout.swipeLayouts.setOnRefreshListener { getListTest() }

        mBinding.btnCreate.setOnClickListener(CustomSetOnClickViewListener {
            val intent = Intent(mActivity, PropertyServiceItemCategoryActivity::class.java)
            activityLauncher.launch(intent) { result: ActivityResult ->
                if (result.resultCode == BaseActivity.RESULT_OK) {
                    getListTest()
                }
            }
        })

    }

    private val onClickItemListener = object : PropertyServiceListAdapter.OnClickItemListener {
        override fun onClick(propertyService: PropertyServiceListModel) {
            val intent = Intent(mActivity, PropertyServiceListDetailActivity::class.java).apply {
                putExtra("service_model", propertyService)
                putExtra("action", "list_detail")
            }
            startActivity(intent)
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun getListTest() {
        propertyServiceList.clear()
        propertyServiceList.addAll(PropertyServiceListAdapter.addListRegService())
        propertyServiceAdapter.notifyDataSetChanged()

        progressBar.visibility = View.GONE
        mBinding.itemLayout.swipeLayouts.isRefreshing = false
    }

    companion object {
        @JvmStatic
        fun newInstance( serviceType : String) =
            PropertyServiceListFragment().apply {
                arguments = Bundle().apply {
                    putString("service_type", serviceType)
                }
            }
    }
}