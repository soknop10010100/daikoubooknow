package com.eazy.daikou.ui.home.development_project.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.model.home_page.home_project.Featured
import com.squareup.picasso.Picasso

class PropertyFeatureItemAdapter(
    private val listPropertyFeature: ArrayList<Featured>,
    private val clickListener: OnClickProperty
):RecyclerView.Adapter<PropertyFeatureItemAdapter.ViewHolder>()
{

    class ViewHolder(itemView: View):RecyclerView.ViewHolder(itemView){
        val imageBuilding: ImageView = itemView.findViewById(R.id.img_property)
        val textNameBuilding: TextView = itemView.findViewById(R.id.text_name_building)
        val textAddress: TextView = itemView.findViewById(R.id.text_address)
        val textAddressCity: TextView = itemView.findViewById(R.id.text_address_city)
        val btnProperty: LinearLayout = itemView.findViewById(R.id.btn_property)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.custom_item_property_featuring,parent,false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.textNameBuilding.text = listPropertyFeature[position].name

        // set address
        if (listPropertyFeature[position].address!=null && listPropertyFeature[position].address!="")
        {
            holder.textAddress.text = listPropertyFeature[position].address
        }
        else
        {
            holder.textAddress.text = ". . ."
        }

        //set city
        if (listPropertyFeature[position].city!=null && listPropertyFeature[position].city!=""){
            holder.textAddressCity.text = listPropertyFeature[position].city
        }
        else
        {
            holder.textAddressCity.text = ". . ."
        }

        //set image
        if (listPropertyFeature[position].image!=null && listPropertyFeature[position].image!="")
        {
            Picasso.get().load(listPropertyFeature[position].image).placeholder(R.drawable.no_image).into(holder.imageBuilding)
        }
        else
        {
            holder.imageBuilding.setImageResource(R.drawable.no_image)
        }

        holder.btnProperty.setOnClickListener {
            clickListener.click(listPropertyFeature[position])
        }

    }

    override fun getItemCount(): Int {
        return listPropertyFeature.size
    }

    interface OnClickProperty{
        fun click(FeatureProperty :Featured)
    }

    fun clear() {
        val size: Int = listPropertyFeature.size
        if (size > 0) {
            for (i in 0 until size) {
                listPropertyFeature.removeAt(0)
            }
            notifyItemRangeRemoved(0, size)
        }
    }
}