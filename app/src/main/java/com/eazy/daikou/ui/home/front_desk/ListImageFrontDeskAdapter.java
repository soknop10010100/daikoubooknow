package com.eazy.daikou.ui.home.front_desk;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;


import java.util.List;

public class ListImageFrontDeskAdapter extends FragmentPagerAdapter {

    private final List<String> lists;

    public ListImageFrontDeskAdapter(FragmentManager fm, List<String> lists) {
        super(fm, lists.size());
        this.lists = lists;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        String item = lists.get(position);
        if (item != null) {
            return FrontDeskImageListFragment.newInstance(position,item,lists);
        } else {
            return null;
        }
    }

    @Override
    public int getCount() {
        return lists.size();
    }


}