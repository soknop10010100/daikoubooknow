package com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.cleaning_v2

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.model.my_property.cleaning.ServiceProviderDetailModel
import com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.adapter_v2.PropertyServiceProviderRescheduleAdapter
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class PropertyServiceProviderRescheduleFragment : BottomSheetDialogFragment() {

    private lateinit var adapterReschedule: PropertyServiceProviderRescheduleAdapter
    private lateinit var recyclerViewReschedule: RecyclerView
    private var serviceRescheduleModel: ServiceProviderDetailModel = ServiceProviderDetailModel()

    private var mContext : Context? = null

    private lateinit var textNoDataTv: TextView

    fun newInstance(serviceDetailModel: ServiceProviderDetailModel): PropertyServiceProviderRescheduleFragment{
        val fragment = PropertyServiceProviderRescheduleFragment()
        val args = Bundle()
        args.putSerializable("history_schedule", serviceDetailModel)
        fragment.arguments = args
        return fragment
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_property_service_provider_reschedule, container, false)

        setupFullHeight()

        initView(view)

        initAction()

        return view
    }

    private fun setupFullHeight() {
        val bottomSheetDialog = dialog as BottomSheetDialog
        val behavior = bottomSheetDialog.behavior
        behavior.state = BottomSheetBehavior.STATE_EXPANDED
    }
    
   private fun initView(view: View){

       recyclerViewReschedule = view.findViewById(R.id.recyclerViewReschedule)
       textNoDataTv = view.findViewById(R.id.textNoDataTv)
   }

   private fun initAction(){

       if(arguments != null && requireArguments().containsKey("history_schedule")){
           serviceRescheduleModel = requireArguments().getSerializable("history_schedule") as ServiceProviderDetailModel
       }

       val layoutManager = LinearLayoutManager(mContext)
       recyclerViewReschedule.layoutManager = layoutManager

       initRecyclerView()
   }
    private fun initRecyclerView(){

        textNoDataTv.visibility = if (serviceRescheduleModel.history_logs.size > 0) View.GONE else View.VISIBLE

         adapterReschedule = PropertyServiceProviderRescheduleAdapter(mContext!!, serviceRescheduleModel.history_logs)
        //adapterReschedule = PropertyServiceProviderRescheduleAdapter(mContext!!, addStaticLogHistory())
        recyclerViewReschedule.adapter = adapterReschedule

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
    }

}