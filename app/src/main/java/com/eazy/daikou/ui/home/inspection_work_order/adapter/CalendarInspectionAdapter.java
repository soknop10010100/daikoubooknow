package com.eazy.daikou.ui.home.inspection_work_order.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.eazy.daikou.R;
import com.eazy.daikou.model.inspection.Add24HoursModel;
import com.eazy.daikou.model.inspection.InfoInspectionModel;

import java.util.List;

public class CalendarInspectionAdapter extends RecyclerView.Adapter<CalendarInspectionAdapter.ItemViewHolder> {

    private final List<InfoInspectionModel> inspectionModelList;
    private List<Add24HoursModel> inspectionHourList;
    private final boolean isActionMonth;
    private final OnClickCallBackListener onClickCallBackListener;

    public CalendarInspectionAdapter(List<InfoInspectionModel> inspectionModelList, boolean isActionMonth, OnClickCallBackListener onClickCallBackListener) {
        this.inspectionModelList = inspectionModelList;
        this.isActionMonth = isActionMonth;
        this.onClickCallBackListener = onClickCallBackListener;
    }

    public CalendarInspectionAdapter(List<InfoInspectionModel> inspectionModelList, List<Add24HoursModel> inspectionHourList,boolean isActionMonth, OnClickCallBackListener onClickCallBackListener) {
        this.inspectionModelList = inspectionModelList;
        this.isActionMonth = isActionMonth;
        this.inspectionHourList = inspectionHourList;
        this.onClickCallBackListener = onClickCallBackListener;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view  = LayoutInflater.from(parent.getContext()).inflate(R.layout.calendar_inspection_model,parent,false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        if (isActionMonth) {
            InfoInspectionModel infoInspectionModel = inspectionModelList.get(position);
            if (infoInspectionModel != null){
                if (infoInspectionModel.getUnitNo() != null) {
                    holder.nameTv.setText(infoInspectionModel.getUnitNo());
                } else if (infoInspectionModel.getInspectionName() != null) {
                    holder.nameTv.setText(infoInspectionModel.getInspectionName());
                } else {
                    holder.nameTv.setText(". . .");
                }
                holder.mainLayout.setOnClickListener(v -> onClickCallBackListener.onClickCallBackItemInMonth(infoInspectionModel));
            }

        } else {
            Add24HoursModel add24HoursModel = inspectionHourList.get(position);
            if (add24HoursModel != null) {
                if (add24HoursModel.getHour() != null) {
                    holder.nameTv.setText(add24HoursModel.getHour());
                } else {
                    holder.nameTv.setText(". . .");
                }

                if (add24HoursModel.getHourList().size() > 0) {
                    holder.numberInspectionTv.setText(add24HoursModel.getHourList().size() + "");
                    holder.numberInspectionTv.setVisibility(View.VISIBLE);
                } else {
                    holder.numberInspectionTv.setVisibility(View.INVISIBLE);
                }

                holder.mainLayout.setOnClickListener(v -> onClickCallBackListener.onClickCallBackItemDay(add24HoursModel));
            }
        }

    }

    @Override
    public int getItemCount() {
        if (isActionMonth) {
            return inspectionModelList.size();
        } else {
            return inspectionHourList.size();
        }
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder{

        private final TextView nameTv, numberInspectionTv;
        private final LinearLayout mainLayout;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            mainLayout = itemView.findViewById(R.id.ln_parent);
            nameTv = itemView.findViewById(R.id.tittleInspectionTv);
            numberInspectionTv = itemView.findViewById(R.id.numberInspectionTv);
        }
    }

    public interface OnClickCallBackListener {
        void onClickCallBackItemDay(Add24HoursModel add24HoursModel);
        void onClickCallBackItemInMonth(InfoInspectionModel infoInspectionModel);
    }
}
