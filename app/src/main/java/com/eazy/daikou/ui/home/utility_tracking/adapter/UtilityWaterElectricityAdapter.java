package com.eazy.daikou.ui.home.utility_tracking.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.eazy.daikou.R;
import com.eazy.daikou.request_data.sql_database.TrackWaterElectricityDatabase;
import com.eazy.daikou.helper.InternetConnection;
import com.eazy.daikou.helper.Utils;
import com.eazy.daikou.model.utillity_tracking.model.UtilNoCommonAreaModel;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class UtilityWaterElectricityAdapter extends RecyclerView.Adapter<UtilityWaterElectricityAdapter.ViewHolder> {

    private final OnClickUtil onClickUtil;
    private final List<UtilNoCommonAreaModel> utilNoModelList;
    private final Activity activity;
    private final Boolean isWater;
    private final String tracking_date, click_action, actionType;

    public UtilityWaterElectricityAdapter(Activity activity,List<UtilNoCommonAreaModel> utilNoModels, Boolean isWater , String tracking_date, String click_action,String actionType, OnClickUtil onClickUtil) {
        this.onClickUtil = onClickUtil;
        this.utilNoModelList = utilNoModels;
        this.activity = activity;
        this.isWater = isWater;
        this.tracking_date = tracking_date;
        this.click_action = click_action;
        this.actionType = actionType;
    }

    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view  = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_track_water_electricity,parent,false);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams((Utils.getWidth(activity) / 2) - 50 , LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(20,0,0,20);
        view.setLayoutParams(layoutParams);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull ViewHolder holder, int position) {
        UtilNoCommonAreaModel utilNoModel = utilNoModelList.get(position);
        if (utilNoModel != null) {
            holder.txtUtil.setText(utilNoModel.getName());
            if (InternetConnection.checkInternetConnection(holder.imgCheck)) {          //Have Internet Connection
                if (isWater) {
                    if (utilNoModel.getLastTrackDataWater() != null) {
                        if (utilNoModel.getLastTrackDataWater().getIsAlreadyHaveTrack()) {
                            holder.imgCheck.setVisibility(View.VISIBLE);
                        } else {
                            holder.imgCheck.setVisibility(View.GONE);
                        }
                    } else {
                        holder.imgCheck.setVisibility(View.GONE);
                    }
                } else {
                    if (utilNoModel.getLastTrackDataElectricity() != null) {
                        if (utilNoModel.getLastTrackDataElectricity().getIsAlreadyHaveTrack()) {
                            holder.imgCheck.setVisibility(View.VISIBLE);
                        } else {
                            holder.imgCheck.setVisibility(View.GONE);
                        }
                    } else {
                        holder.imgCheck.setVisibility(View.GONE);
                    }
                }
            } else {                                                      //No Internet Connection
                @SuppressLint("SimpleDateFormat") SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                String dateToday = formatter.format(new Date());
                List<HashMap<String, String>> list = new TrackWaterElectricityDatabase(holder.imgCheck.getContext()).getFloorDetailList(actionType);
                if (click_action.equals("warning")) {                    //Click on warning
                    if (isWater) {
                        if (utilNoModel.getLastTrackDataWater().getCurrent_date().equals(tracking_date)) {
                            if (utilNoModel.getLastTrackDataWater() != null) {
                                if (utilNoModel.getLastTrackDataWater().getIsAlreadyHaveTrack()) {
                                    holder.imgCheck.setVisibility(View.VISIBLE);
                                } else {
                                    holder.imgCheck.setVisibility(View.GONE);
                                }
                            } else {
                                holder.imgCheck.setVisibility(View.GONE);
                            }
                        }
                        if (dateToday.equals(tracking_date)) {
                            if (utilNoModel.getLastTrackDataWater() != null) {
                                if (utilNoModel.getLastTrackDataWater().getIsAlreadyHaveTrack()) {
                                    holder.imgCheck.setVisibility(View.VISIBLE);
                                } else {
                                    holder.imgCheck.setVisibility(View.GONE);
                                }
                            } else {
                                holder.imgCheck.setVisibility(View.GONE);
                            }
                        } else {
                            for (int i = 0; i < list.size(); i++) {
                                if (utilNoModel.getId().equals(list.get(i).get("location_id"))) {
                                    holder.imgCheck.setVisibility(View.VISIBLE);
                                    break;
                                } else {
                                    holder.imgCheck.setVisibility(View.GONE);
                                }
                            }
                        }
                    } else {
                        if (utilNoModel.getLastTrackDataElectricity().getCurrent_date().equals(tracking_date)) {
                            if (utilNoModel.getLastTrackDataElectricity() != null) {
                                if (utilNoModel.getLastTrackDataElectricity().getIsAlreadyHaveTrack()) {
                                    holder.imgCheck.setVisibility(View.VISIBLE);
                                } else {
                                    holder.imgCheck.setVisibility(View.GONE);
                                }
                            } else {
                                holder.imgCheck.setVisibility(View.GONE);
                            }
                        } else {
                            if (dateToday.equals(tracking_date)) {
                                if (utilNoModel.getLastTrackDataElectricity() != null) {
                                    if (utilNoModel.getLastTrackDataElectricity().getIsAlreadyHaveTrack()) {
                                        holder.imgCheck.setVisibility(View.VISIBLE);
                                    } else {
                                        holder.imgCheck.setVisibility(View.GONE);
                                    }
                                } else {
                                    holder.imgCheck.setVisibility(View.GONE);
                                }
                            } else {
                                for (int i = 0; i < list.size(); i++) {
                                    if (utilNoModel.getId().equals(list.get(i).get("location_id"))) {
                                        holder.imgCheck.setVisibility(View.VISIBLE);
                                        break;
                                    } else {
                                        holder.imgCheck.setVisibility(View.GONE);
                                    }
                                }
                            }
                        }
                    }
                } else {                                                      //Click Create Not warning
                    if (dateToday.equals(tracking_date)) {
                        if (isWater) {
                            if (utilNoModel.getLastTrackDataWater() != null) {
                                if (utilNoModel.getLastTrackDataWater().getIsAlreadyHaveTrack()) {
                                    holder.imgCheck.setVisibility(View.VISIBLE);
                                } else {
                                    holder.imgCheck.setVisibility(View.GONE);
                                }
                            } else {
                                holder.imgCheck.setVisibility(View.GONE);
                            }
                        } else {
                            if (utilNoModel.getLastTrackDataElectricity() != null) {
                                if (utilNoModel.getLastTrackDataElectricity().getIsAlreadyHaveTrack()) {
                                    holder.imgCheck.setVisibility(View.VISIBLE);
                                } else {
                                    holder.imgCheck.setVisibility(View.GONE);
                                }
                            } else {
                                holder.imgCheck.setVisibility(View.GONE);
                            }
                        }
                    } else {
                        if (isWater) {
                            if (tracking_date.equals(utilNoModel.getLastTrackDataWater().getCurrent_date())) {
                                if (utilNoModel.getLastTrackDataWater() != null) {
                                    if (utilNoModel.getLastTrackDataWater().getIsAlreadyHaveTrack()) {
                                        holder.imgCheck.setVisibility(View.VISIBLE);
                                    } else {
                                        holder.imgCheck.setVisibility(View.GONE);
                                    }
                                }
                            } else {
                                holder.imgCheck.setVisibility(View.GONE);
                            }
                        } else {
                            if (tracking_date.equals(utilNoModel.getLastTrackDataElectricity().getCurrent_date())) {
                                if (utilNoModel.getLastTrackDataElectricity() != null) {
                                    if (utilNoModel.getLastTrackDataElectricity().getIsAlreadyHaveTrack()) {
                                        holder.imgCheck.setVisibility(View.VISIBLE);
                                    } else {
                                        holder.imgCheck.setVisibility(View.GONE);
                                    }
                                }
                            } else {
                                holder.imgCheck.setVisibility(View.GONE);
                            }
                        }
                    }
                }
            }

            holder.allView.setOnClickListener(v -> onClickUtil.onClickUtil(utilNoModel));
        }
    }

    @Override
    public int getItemCount() {
        return utilNoModelList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        private final View allView;
        private final RelativeLayout imgCheck;
        private final TextView txtUtil;

        public ViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            allView = itemView;
            imgCheck = itemView.findViewById(R.id.checkImage);
            txtUtil = itemView.findViewById(R.id.txtUtil);
        }
    }

    public interface OnClickUtil{
        void onClickUtil(UtilNoCommonAreaModel utilNoModel);
    }

}
