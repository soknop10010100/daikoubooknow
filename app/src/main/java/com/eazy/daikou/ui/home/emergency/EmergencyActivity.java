package com.eazy.daikou.ui.home.emergency;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener;
import com.eazy.daikou.helper.GetDataUtils;
import com.eazy.daikou.helper.Utils;
import com.eazy.daikou.helper.map.MapsActivity;
import com.eazy.daikou.ui.home.emergency.fragmentEmergency.EmergencyHistoryActivity;
import com.eazy.daikou.R;
import com.eazy.daikou.base.BaseActivity;

import java.util.Locale;

public class EmergencyActivity extends BaseActivity implements View.OnClickListener {

    private ImageView iconHistory;
    private String actionType = "";

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_alert_emergency);

        // Change Status Bar To Red
        Utils.changeStatusBarColor(ContextCompat.getColor(this, R.color.red), getWindow());

        findViewById(R.id.appBarLayout).setBackgroundColor(ContextCompat.getColor(this, R.color.red));

        initView();

        initAction();

    }

    private void initView(){
        findViewById(R.id.layoutMap).setVisibility(View.VISIBLE);
        iconHistory = findViewById(R.id.addressMap);

        Utils.customOnToolbar(this, getResources().getString(R.string.emergency).toUpperCase(Locale.ROOT), this::finish);

        LinearLayout policeBtn = findViewById(R.id.btnPolice);
        LinearLayout touristPoliceBtn = findViewById(R.id.btnTouristPolice);
        LinearLayout securityGuardBtn = findViewById(R.id.btnSecurityGuard);
        LinearLayout hospitalBtn = findViewById(R.id.btnHospital);
        LinearLayout fireStationBtn = findViewById(R.id.btnFireStation);

        policeBtn.setOnClickListener(this);
        touristPoliceBtn.setOnClickListener(this);
        securityGuardBtn.setOnClickListener(this);
        hospitalBtn.setOnClickListener(this);
        fireStationBtn.setOnClickListener(this);
    }

    private void initAction(){
        actionType = GetDataUtils.Companion.getDataFromString("action", this);

        iconHistory.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_report_description));
        iconHistory.setOnClickListener(new CustomSetOnClickViewListener(view -> {
            Intent intent = new Intent(EmergencyActivity.this, EmergencyHistoryActivity.class);
            intent.putExtra("list_type", actionType);
            startActivity(intent);
        }));

        checkPermissionOnMaps();
    }

    private void checkPermissionOnMaps(){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, getString(R.string.permission_granted), Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(this, getString(R.string.permission_granted), Toast.LENGTH_SHORT).show();
            }
        }
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnPolice:
                toastAlert(getResources().getString(R.string.police));
                break;
            case R.id.btnTouristPolice:
                toastAlert(getResources().getString(R.string.tourist_police));
                break;
            case R.id.btnHospital:
                toastAlert(getResources().getString(R.string.hospital));
                break;
            case R.id.btnFireStation:
                toastAlert(getResources().getString(R.string.fire_station));
                break;
            case R.id.btnSecurityGuard:
                startActivity(new Intent(this, MapsActivity.class));
                break;
        }
    }

    private void toastAlert(String title){
        Toast.makeText(this, title + " " + getResources().getString(R.string.coming_soon), Toast.LENGTH_SHORT).show();
    }
}