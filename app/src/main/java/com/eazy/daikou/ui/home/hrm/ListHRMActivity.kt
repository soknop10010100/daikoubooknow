package com.eazy.daikou.ui.home.hrm

import android.content.Intent
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.base.MockUpData
import com.eazy.daikou.helper.CustomStartIntentUtilsClass.Companion.onStartSplashScreenActivity
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.home.PermissionModel
import com.eazy.daikou.ui.SplashScreenActivity
import com.eazy.daikou.ui.home.hrm.adapter.ItemHRAdapter
import com.eazy.daikou.ui.home.hrm.leave_request.ListLeaveManagementActivity
import com.eazy.daikou.ui.home.hrm.notice_board.NoticeBoardHrActivity
import com.eazy.daikou.ui.home.hrm.report_attendance.AttendanceHrReportActivity
import com.eazy.daikou.ui.home.hrm.over_time.OverTimeHRActivity
import com.eazy.daikou.ui.home.hrm.payroll.ListPayRollManagementActivity
import com.eazy.daikou.ui.home.hrm.profile.ProfileEmployeeActivity
import com.eazy.daikou.ui.home.hrm.resignation.ResignationHrActivity
import java.util.HashMap

class ListHRMActivity : BaseActivity() {

    private var action = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_hrmactivity)

        val btnBack = findViewById<TextView>(R.id.btn_back)
        btnBack.setOnClickListener { onBackFish() }

        val recyclerView = findViewById<RecyclerView>(R.id.list_item)

        recyclerView.layoutManager = Utils.spanGridLayoutManager(this, MockUpData.listItem(this).size)
        val adapter = ItemHRAdapter(this, "hr_menu", MockUpData.listItem(this), itemClick)
        recyclerView.adapter = adapter

        findViewById<ImageView>(R.id.noticeBoard).setOnClickListener {
            startActivity(Intent(this@ListHRMActivity, NoticeBoardHrActivity::class.java))
        }

        initActionFromNotification()

    }
    private val itemClick = object : ItemHRAdapter.ItemClickOnListener{
        override fun onItemClickId(id: Int) {
            when (id) {
                1 -> {
                    startActivity(Intent(this@ListHRMActivity, AttendanceHrReportActivity::class.java))
                }
                2 -> {
                    startActivity(Intent(this@ListHRMActivity, ListLeaveManagementActivity::class.java))
                }
                3 -> {
                    startActivity(Intent(this@ListHRMActivity, ListPayRollManagementActivity::class.java))
                }
                4 -> {
                    startActivity(Intent(this@ListHRMActivity, ProfileEmployeeActivity::class.java))
                }
                5 -> {
                    startActivity(Intent(this@ListHRMActivity, OverTimeHRActivity::class.java))
                }
                6 -> {
                    startActivity(Intent(this@ListHRMActivity, ResignationHrActivity::class.java))
                }
                7 ->{
                    val intent = Intent(this@ListHRMActivity, MyRoleMainActivity::class.java)
                    startActivity(intent)
                }
            }
        }

        override fun onItemClickAction(action: String?) {}

    }

    private fun initActionFromNotification(){
        if (intent != null && intent.hasExtra("action")){
            action = intent.getStringExtra("action").toString()
            if (intent != null && intent.hasExtra("id")) {
                val id = intent.getStringExtra("id").toString()
                val intent = Intent(this@ListHRMActivity, intentActivity(action))
                intent.putExtra("action", action)
                intent.putExtra("id", id)
                startActivity(intent)
            }
        }
    }

    private fun intentActivity(action : String) : Class<*>{
        return when (action) {
            "payroll" -> {
                ListPayRollManagementActivity::class.java
            }
            "leave_application" -> {
                ListLeaveManagementActivity::class.java
            }
            "overtime_application" -> {
                OverTimeHRActivity::class.java
            }
            "noticeboard" -> {
                NoticeBoardHrActivity::class.java
            }
            "resignation" -> {
                ResignationHrActivity::class.java
            }
            else -> {
                SplashScreenActivity::class.java
            }
        }
    }

    override fun onBackPressed() {
        onBackFish()
    }

    private fun onBackFish(){
        if (action != ""){
            onStartSplashScreenActivity(this@ListHRMActivity, "already_notification")
            finishAffinity()
        } else {
            finish()
        }
    }
}