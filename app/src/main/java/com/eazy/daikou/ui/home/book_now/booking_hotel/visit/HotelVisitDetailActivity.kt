package com.eazy.daikou.ui.home.book_now.booking_hotel.visit

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.request_data.request.book_now_ws.BookingVisitWS
import com.eazy.daikou.helper.AbsoluteFitLayoutManager
import com.eazy.daikou.helper.GetDataUtils
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.helper.map.MapsGenerateClass
import com.eazy.daikou.helper.web.WebsiteActivity
import com.eazy.daikou.model.booking_hotel.HotelVisitDetailModel
import com.eazy.daikou.model.booking_hotel.LocationModel
import com.eazy.daikou.model.booking_hotel.Point
import com.eazy.daikou.model.home.SubItemHomeModel
import com.eazy.daikou.ui.home.book_now.booking_hotel.ShowAllImageActivity
import com.eazy.daikou.ui.home.book_now.booking_hotel.adapter.BookingHomeItemAdapter
import com.eazy.daikou.ui.home.book_now.booking_hotel.adapter.CustomImageGridLayoutAdapter
import com.eazy.daikou.ui.home.book_now.booking_hotel.adapter.HotelVisitItemAdapter
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.material.appbar.AppBarLayout
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.abs

class HotelVisitDetailActivity : BaseActivity() {

    private var id = ""
    private var action = ""
    private lateinit var titleToolbar : TextView
    private lateinit var mapFragment : SupportMapFragment
    private lateinit var progressBar: ProgressBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hotel_visit_detail)

        initView()

        initData()

        initAction()

    }

    private fun initView(){
        Utils.customOnToolbar(this, ""){finish()}
        titleToolbar = findViewById(R.id.titleToolbar)
        progressBar = findViewById(R.id.progressItem)
    }

    private fun initData(){
        id = GetDataUtils.getDataFromString("id", this)
        action = GetDataUtils.getDataFromString("action", this)
    }

    private fun initAction(){
        //Custom app bar scroll to top
        val appBarLayout = findViewById<AppBarLayout>(R.id.appBarLayout)
        appBarLayout.addOnOffsetChangedListener { appBarLayout, verticalOffset ->
            if (abs(verticalOffset) - appBarLayout.totalScrollRange == 0) {
                // Collapsed
                titleToolbar.visibility = View.VISIBLE
            } else {
                // Expanded
                titleToolbar.visibility = View.INVISIBLE

            }
        }

        requestVisitDetail()
    }

    private fun requestVisitDetail(){
        BookingVisitWS().getVisitShowDetail(this, action, id, object : BookingVisitWS.OnCallBackDetailLocationListener{
            override fun onSuccess(subHomeModelCategoryList: ArrayList<SubItemHomeModel>, locationModel: ArrayList<LocationModel>) {}

            override fun onSuccessDetail(hotelVisitDetailModel: HotelVisitDetailModel, nearByList: ArrayList<SubItemHomeModel>) {
                progressBar.visibility = View.GONE
                setValueData(hotelVisitDetailModel, nearByList)
            }

            override fun onFailed(message: String) {
                progressBar.visibility = View.GONE
                Utils.customToastMsgError(this@HotelVisitDetailActivity, message, false)
            }

        })
    }

    private fun setValueData(hotelVisitDetailModel: HotelVisitDetailModel, nearByList: ArrayList<SubItemHomeModel>){
        // profile title
        Utils.setValueOnText(findViewById(R.id.hotelNameTv), hotelVisitDetailModel.title)
        Utils.setValueOnText(findViewById(R.id.titleTv), hotelVisitDetailModel.title)

        if (hotelVisitDetailModel.profile_photo != null){
            Glide.with(this).load(hotelVisitDetailModel.profile_photo).into(findViewById(R.id.profile_image))
        }
        Utils.setValueOnText(findViewById(R.id.locationTv), hotelVisitDetailModel.location)
        Utils.setValueOnText(titleToolbar, hotelVisitDetailModel.title)

        Utils.setValueOnText(findViewById(R.id.viewTv), hotelVisitDetailModel.views)
        if (hotelVisitDetailModel.created_at != null){
            findViewById<TextView>(R.id.dateTv).text = getFormattedDate(hotelVisitDetailModel.created_at!!)
        }

        // description
        Utils.setTextHtml(findViewById(R.id.descriptionTv), hotelVisitDetailModel.body)

        val sourceTv = findViewById<TextView>(R.id.sourceTv)
        Utils.setValueOnText(sourceTv, hotelVisitDetailModel.name_resource)
        if (hotelVisitDetailModel.name_resource != null) {
            sourceTv.setOnClickListener(CustomSetOnClickViewListener {
                if (hotelVisitDetailModel.link != null){
                    val propertyWebsite = Intent(this, WebsiteActivity::class.java)
                    propertyWebsite.putExtra("linkUrlVideo", hotelVisitDetailModel.link)
                    startActivity(propertyWebsite)
                } else {
                    Utils.customToastMsgError(this, resources.getString(R.string.no_web_site), true)
                }
            })
        }

        if (action == "news"){
            // init Image item top
            if (hotelVisitDetailModel.image != null){
                val slideModels : ArrayList<String> = ArrayList()
                slideModels.add(hotelVisitDetailModel.image!!)
                initRecyclerImage(slideModels, hotelVisitDetailModel.title!!)
            }
            findViewById<LinearLayout>(R.id.mapsLayout).visibility = View.GONE
            findViewById<LinearLayout>(R.id.moreInfoLayout).visibility = View.GONE

            findViewById<TextView>(R.id.titleTv).visibility = View.VISIBLE
        } else {
            // init Image item top
            val slideModels : ArrayList<String> = ArrayList()
            slideModels.addAll(hotelVisitDetailModel.gallery)
            initRecyclerImage(slideModels, hotelVisitDetailModel.title!!)
            // init maps
            mapFragment = (supportFragmentManager.findFragmentById(R.id.google_map) as SupportMapFragment?)!!
            if (hotelVisitDetailModel.lat != null && hotelVisitDetailModel.long != null){
                MapsGenerateClass.initMapsActivity(this, hotelVisitDetailModel.lat!!.toDouble(), hotelVisitDetailModel.long!!.toDouble(), "", mapFragment,  object : MapsGenerateClass.CallBackOnMapReadyListener{
                    override fun onCallBackLister(map: GoogleMap) {}
                })

                findViewById<TextView>(R.id.btnGetDirection).setOnClickListener( CustomSetOnClickViewListener{
                    if (hotelVisitDetailModel.lat != null && hotelVisitDetailModel.long != null) {
                        Utils.startOpenLinkMaps(this, hotelVisitDetailModel.lat, hotelVisitDetailModel.long)
                    }
                })
            } else {
                findViewById<TextView>(R.id.btnGetDirection).isEnabled = false
            }

            findViewById<LinearLayout>(R.id.profileLayout).visibility = View.VISIBLE
        }

        // Near by item
        initNearbyRecyclerView(nearByList)

        // Contact
        if (hotelVisitDetailModel.contact != null){
            if (hotelVisitDetailModel.contact!!.phoneNumber != null){
                findViewById<TextView>(R.id.phoneTv).append(String.format(" %s %s", ":", hotelVisitDetailModel.contact!!.phoneNumber))
            } else {
                findViewById<TextView>(R.id.phoneTv).append(String.format(" %s %s", ":", ". . ."))
            }
            if (hotelVisitDetailModel.contact!!.email != null){
                findViewById<TextView>(R.id.emailTv).append(String.format(" %s %s", ":", hotelVisitDetailModel.contact!!.email))
            } else {
                findViewById<TextView>(R.id.emailTv).append(String.format(" %s %s", ":", ". . ."))
            }
            if (hotelVisitDetailModel.contact!!.facebook != null){
                findViewById<TextView>(R.id.facebookTv).append(String.format(" %s %s", ":", hotelVisitDetailModel.contact!!.facebook))
            } else {
                findViewById<TextView>(R.id.facebookTv).append(String.format(" %s %s", ":", ". . ."))
            }
            if (hotelVisitDetailModel.contact!!.telegram != null){
                findViewById<TextView>(R.id.telegramTv).append(String.format(" %s %s", ":", hotelVisitDetailModel.contact!!.telegram))
            } else {
                findViewById<TextView>(R.id.telegramTv).append(String.format(" %s %s", ":", ". . ."))
            }
        } else {
            findViewById<LinearLayout>(R.id.contactLayout).visibility = View.GONE
        }

        // More option
        addViewOptionMoreInfo(hotelVisitDetailModel.points, findViewById(R.id.optionsMoreLayout))
    }

    private fun addViewOptionMoreInfo(list : ArrayList<Point>, eventLayout : LinearLayout){
        for (item in list) {
            val view: View = LayoutInflater.from(this).inflate(R.layout.payment_method_online_option, null)
            val iconBank : ImageView = view.findViewById(R.id.icon_bank)
            view.findViewById<LinearLayout>(R.id.mainLayout).background = null
            view.findViewById<TextView>(R.id.nameTv).text = if (item.title != null) item.title else ". . ."
            Glide.with(iconBank).load(if (item.image != null) item.image else R.drawable.no_image).into(iconBank)
            eventLayout.addView(view)
        }
        if (list.size == 0) findViewById<LinearLayout>(R.id.moreInfoOptionLayout).visibility = View.GONE
    }


    private fun initRecyclerImage(slideModels: ArrayList<String>, name : String){
        val recyclerView: RecyclerView = findViewById(R.id.recyclerView)
        recyclerView.layoutManager = Utils.spanGridImageLayoutManager(slideModels, this)
        recyclerView.adapter = CustomImageGridLayoutAdapter("show_5_items", this, slideModels, object : CustomImageGridLayoutAdapter.OnClickCallBackLister{
            override fun onClickCallBack(value: String) {
                val intent = Intent(this@HotelVisitDetailActivity, ShowAllImageActivity::class.java)
                intent.putExtra("image_list", slideModels)
                intent.putExtra("hotel_name", name)
                startActivity(intent)
            }

        })
        recyclerView.isNestedScrollingEnabled = false
        recyclerView.visibility = if (slideModels.size > 0) View.VISIBLE else View.GONE
    }

    private fun initNearbyRecyclerView(list : ArrayList<SubItemHomeModel>){
         val recyclerView : RecyclerView = findViewById(R.id.hotelRelatedRecycler)
        recyclerView.layoutManager = AbsoluteFitLayoutManager(this, 1, RecyclerView.HORIZONTAL, false, 2)

        val bookingHomeItemAdapter = HotelVisitItemAdapter(this, "nearby_item_detail_visit", list, object : BookingHomeItemAdapter.PropertyClick{
            override fun onBookingClick(propertyModel: SubItemHomeModel?) {
                val intent = Intent(this@HotelVisitDetailActivity, HotelVisitDetailActivity::class.java).apply {
                    putExtra("id", propertyModel!!.id)
                    putExtra("list", list)
                }
                startActivity(intent)
            }

            override fun onBookingWishlistClick(propertyModel: SubItemHomeModel?) {}

        })
        recyclerView.adapter = bookingHomeItemAdapter
        if (list.size == 0) findViewById<LinearLayout>(R.id.relatedHotelLayout).visibility = View.GONE
    }

    @SuppressLint("SimpleDateFormat")
    private fun getFormattedDate(OurDate: String): String? {
        var OurDate: String? = OurDate
        OurDate = try {
            val formatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
            formatter.timeZone = TimeZone.getTimeZone("UTC")
            val value: Date = formatter.parse(OurDate) as Date
            val dateFormatter = SimpleDateFormat("MM-dd-yyyy")
            dateFormatter.timeZone = TimeZone.getDefault()
            dateFormatter.format(value)
        } catch (e: Exception) {
            "00-00-0000 00:00"
        }
        return OurDate
    }

}