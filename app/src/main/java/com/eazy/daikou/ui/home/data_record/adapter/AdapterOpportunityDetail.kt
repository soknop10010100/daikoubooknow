package com.eazy.daikou.ui.home.data_record.adapter

import android.content.Context
import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.model.data_record.Interesting

class AdapterOpportunityDetail(private val context: Context, private val listDetailInteresting: ArrayList<Interesting>, private val callBackClickItem : CallBackClickItemListener): RecyclerView.Adapter<AdapterOpportunityDetail.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.custom_data_record_interesting, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
       val interesting : Interesting = listDetailInteresting[position]
        if (interesting != null){

            holder.unitTypeTv.text = if(interesting.unit!!.type!!.name != null) interesting.unit!!.type!!.name else "- - -"
            holder.streetFloorTv.text = if (interesting.unit!!.floor_no != null) interesting.unit!!.floor_no else " - - -"
            holder.unitNoTv.text = if (interesting.unit!!.name != null) interesting.unit!!.name else "- - -"
            holder.sellingPriceTv.text = if(interesting.unit!!.price != null) interesting.unit!!.price else "- - -"
            if (interesting.status != null){
                if(interesting.status == "available"){
                        holder.statusTv.text = context.getText(R.string.available)
                        holder.statusTv.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(context, R.color.green))
                } else {
                    holder.statusTv.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(context, R.color.appBarColorOld))
                    holder.statusTv.text = interesting.status
                }
            }
            holder.itemView.setOnClickListener{ callBackClickItem.clickItemListener(interesting)}
        }
    }

    override fun getItemCount(): Int {
        return listDetailInteresting.size
    }

    class ViewHolder(view : View) : RecyclerView.ViewHolder(view){
        var unitTypeTv : TextView = view.findViewById(R.id.unitTypeTv)
        var streetFloorTv : TextView = view.findViewById(R.id.streetFloorTv)
        var unitNoTv : TextView = view.findViewById(R.id.unitNoTv)
        var sellingPriceTv : TextView = view.findViewById(R.id.sellingPriceTv)
        var statusTv : TextView = view.findViewById(R.id.statusTv)
    }

    interface CallBackClickItemListener{
        fun clickItemListener(interesting : Interesting)
    }
}