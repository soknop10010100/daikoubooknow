package com.eazy.daikou.ui.home.my_property_v1.adapter;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;


import com.eazy.daikou.R;
import com.eazy.daikou.model.floor_plan.FloorSpaceItemModel;
import com.eazy.daikou.model.floor_plan.FloorUnitItemModel;
import com.eazy.daikou.model.floor_plan.FloorUnitTypeItemModel;
import com.eazy.daikou.helper.Utils;

import java.util.List;

public class FloorSpaceListAdapter extends RecyclerView.Adapter<FloorSpaceListAdapter.ItemViewHolder> {

    private List<FloorSpaceItemModel> spaceItemList;
    private List<FloorUnitItemModel> unitItemList;
    private List<FloorUnitTypeItemModel> unitTypeItemList;

    private final ClickBackListener backListener;
    private final String category_type;
    private String storey_id;
    private String floor_no;

    public FloorSpaceListAdapter(List<FloorSpaceItemModel> spaceItemList, String category_type, String space_id, String storey_id, ClickBackListener backListener) {
        this.spaceItemList = spaceItemList;
        this.backListener = backListener;
        this.category_type = category_type;
        this.storey_id = storey_id;
    }
    public FloorSpaceListAdapter(List<FloorUnitItemModel> unitItemList, String category_type, ClickBackListener backListener) {
        this.unitItemList = unitItemList;
        this.backListener = backListener;
        this.category_type = category_type;
    }
    public FloorSpaceListAdapter(List<FloorUnitTypeItemModel> unitTypeItemList, String category_type, String floor_no, ClickBackListener backListener) {
        this.unitTypeItemList = unitTypeItemList;
        this.backListener = backListener;
        this.floor_no = floor_no;
        this.category_type = category_type;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rowView = LayoutInflater.from(parent.getContext()).inflate(R.layout.commercail_floor_switch, parent, false);
        return new ItemViewHolder(rowView);
    }

    @SuppressLint("ResourceType")
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        if (category_type.equalsIgnoreCase("unit")){
            FloorUnitItemModel floorUnitItemModel = unitItemList.get(position);
            if (floorUnitItemModel != null) {
                holder.fbSwitchOpen.setText(floorUnitItemModel.getTotalSpace() + "");
                holder.nameCommercialOpen.setText(floorUnitItemModel.getSpaceName());
                holder.nameCommercialClose.setText(floorUnitItemModel.getSpaceName());
                String fullNameCategory = Utils.convertFirstCapital(category_type) + floorUnitItemModel.getSpaceName();

                holder.switchCloseLayout.setOnClickListener(view -> {
                    holder.switchCloseLayout.setVisibility(View.GONE);
                    holder.switchBtnOpenLayout.setVisibility(View.VISIBLE);
                    backListener.clickBackUnitFloor(holder.switchBtnOpenLayout, floorUnitItemModel, category_type, fullNameCategory);

                });
                holder.switchBtnOpenLayout.setOnClickListener(view -> {
                    holder.switchCloseLayout.setVisibility(View.VISIBLE);
                    holder.switchBtnOpenLayout.setVisibility(View.GONE);

                    backListener.clickBackUnitFloorClose(holder.switchCloseLayout, fullNameCategory);

                });
                if (floorUnitItemModel.getFillColor() != null){
                    holder.squareBox.getBackground().setTint(Color.parseColor(floorUnitItemModel.getFillColor()));
                    holder.nameCommercialOpen.getBackground().setTint(Color.parseColor(floorUnitItemModel.getFillColor()));
                }

            }
        } else if (category_type.equalsIgnoreCase("unit_type")){
            FloorUnitTypeItemModel floorUnitTypeItemModel = unitTypeItemList.get(position);
            if (floorUnitTypeItemModel != null){
                holder.fbSwitchOpen.setText(floorUnitTypeItemModel.getTotalSpace() + "");
                holder.nameCommercialOpen.setText(floorUnitTypeItemModel.getSpaceName());
                holder.nameCommercialClose.setText(floorUnitTypeItemModel.getSpaceName());
                String fullNameCategory = Utils.convertFirstCapital(category_type) + floorUnitTypeItemModel.getSpaceName();

                holder.switchCloseLayout.setOnClickListener(view -> {
                    holder.switchCloseLayout.setVisibility(View.GONE);
                    holder.switchBtnOpenLayout.setVisibility(View.VISIBLE);

                    backListener.clickBackUnitTypeFloor(holder.switchBtnOpenLayout, floorUnitTypeItemModel.getId(), category_type, floor_no, fullNameCategory);

                });
                holder.switchBtnOpenLayout.setOnClickListener(view -> {
                    holder.switchCloseLayout.setVisibility(View.VISIBLE);
                    holder.switchBtnOpenLayout.setVisibility(View.GONE);

                    backListener.clickBackUnitTypeFloorClose(holder.switchCloseLayout, fullNameCategory);

                });
                if (floorUnitTypeItemModel.getFillColor() != null){
                    holder.squareBox.getBackground().setTint(Color.parseColor(floorUnitTypeItemModel.getFillColor()));
                    holder.nameCommercialOpen.getBackground().setTint(Color.parseColor(floorUnitTypeItemModel.getFillColor()));
                }

            }
        } else {
            FloorSpaceItemModel floorSpaceItemModel = spaceItemList.get(position);
            if (floorSpaceItemModel != null){
                holder.fbSwitchOpen.setText(floorSpaceItemModel.getTotalSpace() + "");
                holder.nameCommercialOpen.setText(floorSpaceItemModel.getSpaceName());
                holder.nameCommercialClose.setText(floorSpaceItemModel.getSpaceName());
                String fullNameCategory = Utils.convertFirstCapital(category_type) + floorSpaceItemModel.getSpaceName();

                holder.switchCloseLayout.setOnClickListener(view -> {
                    holder.switchCloseLayout.setVisibility(View.GONE);
                    holder.switchBtnOpenLayout.setVisibility(View.VISIBLE);

                    backListener.clickBackSpaceFloor(holder.switchBtnOpenLayout, floorSpaceItemModel, storey_id, "space", fullNameCategory);

                });
                holder.switchBtnOpenLayout.setOnClickListener(view -> {
                    holder.switchCloseLayout.setVisibility(View.VISIBLE);
                    holder.switchBtnOpenLayout.setVisibility(View.GONE);

                    backListener.clickBackSpaceFloorClose( holder.switchCloseLayout, spaceItemList, fullNameCategory);

                });
                if (floorSpaceItemModel.getFillColor() != null){
                    holder.squareBox.getBackground().setTint(Color.parseColor(floorSpaceItemModel.getFillColor()));
                    holder.nameCommercialOpen.getBackground().setTint(Color.parseColor(floorSpaceItemModel.getFillColor()));
                }

            }
        }
    }
    @Override
    public int getItemCount() {
        if (category_type.equalsIgnoreCase("unit")){
            return unitItemList.size();
        } else if (category_type.equalsIgnoreCase("unit_type")){
            return unitTypeItemList.size();
        } else {
            return spaceItemList.size();
        }

    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder{
        private final TextView nameCommercialOpen, nameCommercialClose, fbSwitchOpen, squareBox;
        private final LinearLayout switchCloseLayout;
        private final RelativeLayout switchBtnOpenLayout;
        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            fbSwitchOpen = itemView.findViewById(R.id.fbSwitchOpen);
            switchBtnOpenLayout = itemView.findViewById(R.id.switchBtnOpenLayout);
            switchCloseLayout = itemView.findViewById(R.id.switchCloseLayout);
            squareBox = itemView.findViewById(R.id.clickItemCom);
            nameCommercialOpen = itemView.findViewById(R.id.commercialNameOpen);
            nameCommercialClose = itemView.findViewById(R.id.commercialNameClose);
        }
    }
    public interface ClickBackListener {
        void clickBackSpaceFloor(RelativeLayout layout, FloorSpaceItemModel floorSpaceItemModel, String storey_id, String category_type, String fullNameCategory);
        void clickBackUnitFloor(RelativeLayout layout, FloorUnitItemModel floorSpaceItemModel, String category_type, String fullNameCategory);
        void clickBackUnitTypeFloor(RelativeLayout layout, String floorUnitTypeId, String category_type, String floor_no, String fullNameCategory);

        void clickBackSpaceFloorClose(LinearLayout layout, List<FloorSpaceItemModel> spaceItemList, String fullNameCategory);
        void clickBackUnitFloorClose(LinearLayout layout, String fullNameCategory);
        void clickBackUnitTypeFloorClose(LinearLayout layout, String fullNameCategory);
    }
}
