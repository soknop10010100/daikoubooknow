package com.eazy.daikou.ui.profile;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;

import com.eazy.daikou.R;
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener;
import com.eazy.daikou.base.BaseActivity;
import com.eazy.daikou.request_data.request.profile_ws.ProfileWs;
import com.eazy.daikou.request_data.request.profile_ws.SignUpWs;
import com.eazy.daikou.request_data.request.sign_up_v2.CheckAccountExitsWs;
import com.eazy.daikou.helper.AppAlertCusDialog;
import com.eazy.daikou.helper.CheckIsAppActive;
import com.eazy.daikou.helper.Utils;
import com.eazy.daikou.ui.SampleCanvasClassActivity;
import com.eazy.daikou.ui.sign_up.VerifySmsCodeActivity;
import com.eazy.daikou.ui.sign_up.VerifySmsCodeV2Activity;
import com.google.android.material.textfield.TextInputLayout;
import com.rilixtech.widget.countrycodepicker.CountryCodePicker;
import java.util.HashMap;

public class ForgotPasswordActivity extends BaseActivity {

    private LinearLayout btnSubmit;
    private EditText emailEdt, phoneEdt;
    private TextInputLayout emailBorder, phoneBorder;
    private ProgressBar progressBar;
    private CountryCodePicker countryCodePicker;

    private String countryCode = "", eazyhotelUserId = ""; //User Id will get after sent pin code
    private boolean x = true;
    private AppAlertCusDialog appAlertCusDialog;
    private TextView titleEmailPhone;

    private String emailAndPhone = "";

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(isBookNowApp ? R.layout.hotel_forgot_password : R.layout.activity_forgot_password);

        initView();

        initAction();

    }

    private void initView() {
        countryCodePicker = findViewById(R.id.ccp);
        emailEdt = findViewById(R.id.ed_forgot_p_mail);
        phoneEdt = findViewById(R.id.ed_forgot_phone);
        emailBorder = findViewById(R.id.et_email_border);
        phoneBorder = findViewById(R.id.et_phone_border);
        btnSubmit = findViewById(R.id.btn_summit_forgot_p);
        progressBar = findViewById(R.id.progressItem);
        progressBar.setVisibility(View.GONE);

        if (CheckIsAppActive.Companion.is_daikou_active()) {
            findViewById(R.id.btnCancel).setOnClickListener(view -> finish());
        } else {
            titleEmailPhone = findViewById(R.id.titleHeaderTv);
            RelativeLayout fragmentLayout = findViewById(R.id.fragmentLayout);
            fragmentLayout.addView(new SampleCanvasClassActivity(false, this, "#0b4891"));
            Utils.customOnToolbar(this, getResources().getString(R.string.forgot_password), this::finish);
        }
    }

    private void initAction() {
        // country code
        countryCodePicker.setDefaultCountryUsingNameCodeAndApply(BaseActivity.country_code);
        countryCode = countryCodePicker.getFullNumber();
        countryCodePicker.setOnCountryChangeListener(selectedCountry -> countryCode = countryCodePicker.getFullNumber());

        emailEdt.setHint(getResources().getString(R.string.input_your_email_add));
        phoneEdt.setHint(getResources().getString(R.string.enter_your_phone_number));

        emailBorder.setVisibility(View.VISIBLE);
        phoneBorder.setVisibility(View.GONE);

        appAlertCusDialog = new AppAlertCusDialog();
        btnSubmit.setOnClickListener(new CustomSetOnClickViewListener(view -> {
            String mail = emailEdt.getText().toString().trim();
            String phone = phoneEdt.getText().toString().trim();
            if (!phone.isEmpty() && !x) {
                Utils.hideSoftKeyboard(btnSubmit);
                if (phone.startsWith("0")) {
                    phone = phoneEdt.getText().toString().trim().substring(1);
                } else {
                    phone = phoneEdt.getText().toString().trim();
                }
                if (isBookNowApp) {
                    requestPinCodeWs(false, phone);
                } else {
                    requestServiceCheck(false, phone);
                }
            } else if (!mail.isEmpty() && x) {
                Utils.hideSoftKeyboard(btnSubmit);
                if (isBookNowApp) {
                    requestPinCodeWs(true, mail);
                } else {
                    requestServiceCheck(true ,mail);
                }
            } else {
                if (!x) {
                    appAlertCusDialog.showDialog(ForgotPasswordActivity.this, getResources().getString(R.string.input_your_phone_number));
                } else {
                    appAlertCusDialog.showDialog(ForgotPasswordActivity.this, getResources().getString(R.string.input_your_email_add));
                }
            }
        }));
    }

    private void requestServiceCheck(  boolean isEmail,String userName) {
        new CheckAccountExitsWs().checkUserNameExist(this, userName, new CheckAccountExitsWs.OnResponseUserNameExist() {
            @Override
            public void onSuccessFull(@NonNull String message) {
                Utils.customToastMsgError(ForgotPasswordActivity.this, "Couldn't find your username.", false);
            }

            @Override
            public void onFailed(@NonNull String msg) {
                sendVerifyPinCode(isEmail, userName);
            }

            @Override
            public void onInternetConnection(@NonNull String mess) {
                Utils.customToastMsgError(ForgotPasswordActivity.this, mess, false);
            }
        });
    }

    @SuppressLint("NonConstantResourceId")
    public void onRadioButtonClicked(View view) {
        boolean checked = ((RadioButton) view).isChecked();
        switch (view.getId()) {
            case R.id.rEmail:
                if (checked){
                    emailBorder.setVisibility(View.VISIBLE);
                    phoneBorder.setVisibility(View.GONE);
                    phoneEdt.getText().clear();
                    emailEdt.setInputType(InputType.TYPE_CLASS_TEXT);
                    x = true;
                    if (isBookNowApp){
                        titleEmailPhone.setText(getResources().getString(R.string.we_just_need_your_registered_email_to_send_your_password_reset_code));
                    }
                }
                break;
            case R.id.rSms:
                if (checked){
                    emailBorder.setVisibility(View.GONE);
                    phoneBorder.setVisibility(View.VISIBLE);
                    emailEdt.getText().clear();
                    emailEdt.setInputType(InputType.TYPE_CLASS_NUMBER);
                    x = false;
                    if (isBookNowApp){
                        titleEmailPhone.setText(getResources().getString(R.string.we_just_need_your_registered_phone_to_send_your_password_reset_code));
                    }
                }
                break;
        }
    }

    public void sendVerifyPinCode(boolean isEmail, String phone_email){
        HashMap<String, Object> hashMap = new HashMap<>() ;
        if (isEmail) {
            hashMap.put("username", phone_email);
            emailAndPhone = phone_email;
        } else {
            hashMap.put("username", countryCode + "" + phone_email);
            emailAndPhone = countryCode + "" + phone_email;
        }
        progressBar.setVisibility(View.VISIBLE);
        new ProfileWs().sendAlertVerifyPinCodeEmailPhone(this, hashMap, callBackSendVerify);
    }

    private final  ProfileWs.CallBackUpdateEmailPhoneListener callBackSendVerify = new ProfileWs.CallBackUpdateEmailPhoneListener() {
        @Override
        public void onSuccessVerifyCode(String pinCode) {
            progressBar.setVisibility(View.GONE);
            Intent intent = new Intent(getBaseContext(), VerifySmsCodeV2Activity.class);
            intent.putExtra("pin_code", pinCode);
            intent.putExtra("phone", emailAndPhone);
            intent.putExtra("action", "forget_password");
            intent.putExtra("email_and_phone", emailAndPhone);
            resultLauncher.launch(intent);
        }
        @Override
        public void onFailed(String error) {
            progressBar.setVisibility(View.GONE);

        }
    };

    //Book Now
    private void requestPinCodeWs(boolean isEmail, String phone_email){
        HashMap<String,Object> hashMap = new HashMap<>();
        if (isEmail) {
            hashMap.put("username", phone_email);
        } else {
            String phoneNumber = countryCodePicker.getDefaultCountryCode().concat(phone_email);
            hashMap.put("username", phoneNumber);
        }
        hashMap.put("action", "send");
        progressBar.setVisibility(View.VISIBLE);
        new SignUpWs().sendVerifyPinCode(hashMap, isBookNowApp, new SignUpWs.SignUpInterface() {
            @Override
            public void onSuccess(String message, String pin_code, String user_id) {
                eazyhotelUserId = user_id;
                Intent intent = new Intent(ForgotPasswordActivity.this, VerifySmsCodeActivity.class);
                intent.putExtra("hashmap_request_code", hashMap);
                intent.putExtra("pin_code", pin_code);
                intent.putExtra("action", "forgot_password");
                resultLauncher.launch(intent);
            }

            @Override
            public void onFailed(String mess) {
                progressBar.setVisibility(View.GONE);
                appAlertCusDialog.showDialog(ForgotPasswordActivity.this, mess);
            }

            @Override
            public void onError(String mess, int code) {
                progressBar.setVisibility(View.GONE);
                appAlertCusDialog.showDialog(ForgotPasswordActivity.this, mess);
            }
        });
    }

    private final ActivityResultLauncher<Intent> resultLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(), result -> {
                if (result.getResultCode() == Activity.RESULT_OK) {
                    if (result.getData() != null) {
                        Intent data = result.getData();
                        Intent intent = new Intent(ForgotPasswordActivity.this, NewPasswordActivity.class);
                        String getPinCode = data.getStringExtra("pin_code");
                        intent.putExtra("pin_code", getPinCode);
                        intent.putExtra("eazyhotel_user_id", eazyhotelUserId);
                        intent.putExtra("email_and_phone", emailAndPhone);
                        startActivity(intent);
                    }
                }
            });
}

