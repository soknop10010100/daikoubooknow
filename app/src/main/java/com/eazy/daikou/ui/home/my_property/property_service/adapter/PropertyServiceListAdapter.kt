package com.eazy.daikou.ui.home.my_property.property_service.adapter

import android.content.Context
import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.databinding.PropertyServiceListModelBinding
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.my_property.property_service.PropertyServiceListModel
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener

class PropertyServiceListAdapter(private val context: Context, private val list: ArrayList<PropertyServiceListModel>, private val itemClickService: OnClickItemListener):
    RecyclerView.Adapter<PropertyServiceListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = PropertyServiceListModelBinding.inflate(LayoutInflater.from(context), parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val service : PropertyServiceListModel = list[position]
        holder.onBindView(context, service, itemClickService)
    }

    override fun getItemCount(): Int {
        return list.size
    }
    interface OnClickItemListener {
        fun onClick(propertyService: PropertyServiceListModel)
    }

    class ViewHolder(private val mBind: PropertyServiceListModelBinding) : RecyclerView.ViewHolder(mBind.root) {

        fun onBindView(context: Context, service : PropertyServiceListModel, itemClickService: OnClickItemListener){
            Utils.setValueOnText(mBind.orderNoTv, service.reg_no)
            Utils.setValueOnText(mBind.unitNoTv, service.unit_no)
            Utils.setValueOnText(mBind.serviceNameTv, service.service_name)
            Utils.setValueOnText(mBind.serviceTypeTv, service.service_type)
            Utils.setValueOnText(mBind.startDateTv, service.start_date)
            Utils.setValueOnText(mBind.endDateTv, service.end_date)
            Utils.setValueOnText(mBind.taxTv, service.total_tax_amount)
            Utils.setValueOnText(mBind.totalPriceTv, service.total_price_amount)

            if (service.status != null) {
                mBind.statusTv.text = getValueFromKeyStatus(context)[service.status]
            } else {
                mBind.statusTv.text = ". . ."
            }

            mBind.infoLayout.setOnClickListener ( CustomSetOnClickViewListener {
                itemClickService.onClick(service)
            })
        }

        private fun getValueFromKeyStatus(context: Context?): HashMap<String, String> {
            val hashMap = HashMap<String, String>()
            hashMap["new"] = Utils.getText(context, R.string.new_)
            hashMap["pending"] = Utils.getText(context, R.string.pending)
            hashMap["accepted"] = Utils.getText(context, R.string.accepted)
            hashMap["cancelled"] = Utils.getText(context, R.string.cancel)
            hashMap["confirmed"] = Utils.getText(context, R.string.confirm)
            return hashMap
        }

    }
    fun clear() {
        val size: Int = list.size
        if (size > 0) {
            for (i in 0 until size) {
                list.removeAt(0)
            }
            notifyItemRangeRemoved(0, size)
        }
    }

    companion object {
        fun addListRegService() : ArrayList<PropertyServiceListModel> {
            val list = ArrayList<PropertyServiceListModel>()
            for (i in 1..10){
                val item = PropertyServiceListModel(
                    "11$i",
                    "Cleaning",
                    "Mr Lika",
                    "A00$i",
                    "Cleaning Home",
                    "01-10-2020 07 : 00",
                    "01-10-2020 08 : 00",
                    "10 $",
                    "10 %",
                    if (i % 2 == 0)  "Time" else "Week",
                    "1 $",
                    "11 $",
                    "new",
                    "Sok Nim",
                    "012345678",
                    "Phnom Penh"
                )
                item.id = "$i"
                list.add(item)
            }
            return list
        }
    }
}