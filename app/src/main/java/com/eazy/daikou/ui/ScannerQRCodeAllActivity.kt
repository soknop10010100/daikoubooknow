package com.eazy.daikou.ui

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.ui.home.evaluate.EvaluateUnitScanActivity
import com.eazy.daikou.ui.home.parking.ScanParkActivity

class ScannerQRCodeAllActivity : BaseActivity() {

    private val REQUEST_CODE_SCANNER = 123
    private var action : String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scanner_qrcode_all)

        if (intent != null && intent.hasExtra("action")) {
            action = intent.getStringExtra("action")!!
        }

        val intentScanQr = Intent(this@ScannerQRCodeAllActivity, ScanParkActivity::class.java)
        intentScanQr.putExtra("action", action)
        startActivityForResult(intentScanQr, REQUEST_CODE_SCANNER)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK){
            if(requestCode == REQUEST_CODE_SCANNER){
                if (data != null){
                    val backButtonVal : String? = data.getStringExtra("click_back")
                    if(backButtonVal != null){
                        finish()
                    } else {
                        val resultScanQr : String? = data.getStringExtra("result_scan")
                        val actionResult : String? = data.getStringExtra("action")
                        if (resultScanQr != null && actionResult != null) {
                            getResultScanner(resultScanQr, actionResult)
                        } else{
                            Toast.makeText(this, getString(R.string.scanner_not_working), Toast.LENGTH_SHORT).show()

                        }
                    }
                }
            }
        }
    }
    private fun getResultScanner(resultVal : String, action : String){
        val intent = Intent(this@ScannerQRCodeAllActivity, EvaluateUnitScanActivity::class.java)
        intent.putExtra("result_scan_parking", resultVal)
        intent.putExtra("action", action)
        startActivity(intent)
        finish()
    }

}