package com.eazy.daikou.ui.home.service_provider_new.adapter_service_provider

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.ui.home.service_provider_new.test_model.TestModel

class PopularProviderAdapter (private val context: Activity) : RecyclerView.Adapter<PopularProviderAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.custom_popular_layout_view, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
      // val popular: TestModel = listPopular[position]

       Glide.with(context).load("https://dev.booknow.asia/uploads/mytravel/location/location-3.jpg").into(holder.imagePopular)
        //"https://i.pinimg.com/474x/15/0c/d7/150cd7dec5567c7c759dc0f899d5ac5a.jpg"

      // holder.itemView.setOnClickListener { callBackPopular.clickItemListener(popular) }
    }

    override fun getItemCount(): Int {
        return 20
    }


    class  ViewHolder(view: View) : RecyclerView.ViewHolder(view){
        var imagePopular: ImageView = view.findViewById(R.id.imagePopular)
    }

//    interface CallBackItemClickListener{
//        fun clickItemListener(popular : TestModel)
//    }
}