package com.eazy.daikou.ui.home.utility_tracking.adapter;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.eazy.daikou.R;
import com.eazy.daikou.request_data.static_data.TypeTrackingWaterElectricity;
import com.eazy.daikou.helper.Utils;
import com.eazy.daikou.model.utillity_tracking.model.W_ETrackingModel;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class ListWaterElectricityAdapter  extends RecyclerView.Adapter<ListWaterElectricityAdapter.ViewHolder> {

    private final String action;
    private final List<W_ETrackingModel> listSubRecord;
    private final OnClickItemTracking onClickItemTracking;

    public ListWaterElectricityAdapter(List<W_ETrackingModel> listSubRecord, String action , OnClickItemTracking onClickItemTracking) {
        this.listSubRecord = listSubRecord;
        this.action = action;
        this.onClickItemTracking = onClickItemTracking;
    }

    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_list_water_electricity, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull @NotNull ViewHolder holder, int position) {
        W_ETrackingModel eTrackingModel = listSubRecord.get(position);
        if (eTrackingModel.getTrackingDate() != null) {
            holder.dateTv.setText( Utils.formatDateFromString("yyyy-MM-dd", "dd, MMM yyyy", eTrackingModel.getTrackingDate()));
        }
        if (eTrackingModel.getCreatedDate() != null){
            holder.valTrackDate.setText(Utils.formatDateFromString("yyyy-MM-dd", "dd, MMM yyyy", eTrackingModel.getCreatedDate()));
        } else {
            holder.valTrackDate.setText("---");
        }

        if (eTrackingModel.getTotalUsage() != null) {
            if (action.equals(TypeTrackingWaterElectricity.WATER_TRACK)){
                holder.valTotalUnit.setText(eTrackingModel.getTotalUsage() +" "+"M" + '\u00B3');
            } else if (action.equals(TypeTrackingWaterElectricity.WATER_COMMON)){
                holder.valTotalUnit.setText(eTrackingModel.getTotalUsage() +" "+"M" + '\u00B3');
            } else if (action.equals(TypeTrackingWaterElectricity.WATER_PPWSA)){
                holder.valTotalUnit.setText(eTrackingModel.getTotalUsage() +" "+ "M" + '\u00B3');
            } else if (action.equals(TypeTrackingWaterElectricity.ELECTRICITY_TRACK)){
                holder.valTotalUnit.setText(eTrackingModel.getTotalUsage() +" "+ "KWH");
            } else if (action.equals(TypeTrackingWaterElectricity.ELECTRICITY_COMMON)){
                holder.valTotalUnit.setText(eTrackingModel.getTotalUsage() +" "+ "KWH");
            } else if (action.equals(TypeTrackingWaterElectricity.ELECTRICITY_EDC)){
                holder.valTotalUnit.setText(eTrackingModel.getTotalUsage() +" "+ "KWH");
            }

        } else {
            holder.valTotalUnit.setText("0");
        }

        if (eTrackingModel.getRecordedNumber() != null){
            holder.txtCount.setVisibility(View.VISIBLE);
            holder.txtCount.setText(eTrackingModel.getRecordedNumber());
        } else {
            holder.txtCount.setVisibility(View.GONE);
        }

        holder.itemView.setOnClickListener(v -> onClickItemTracking.onClickItemTrack(eTrackingModel));
    }

    @Override
    public int getItemCount() {
        return listSubRecord.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        private final TextView dateTv, valTrackDate, valTotalUnit, txtCount;

        public ViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            dateTv = itemView.findViewById(R.id.dateTv);
            valTrackDate = itemView.findViewById(R.id.valTrackDate);
            valTotalUnit = itemView.findViewById(R.id.valTotalUnit);
            txtCount = itemView.findViewById(R.id.count);
        }
    }

    public interface OnClickItemTracking{
        void onClickItemTrack(W_ETrackingModel eTrackingModel);
    }

    public void clear() {
        int size = listSubRecord.size ();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                listSubRecord.remove ( 0 );
            }
            notifyItemRangeRemoved ( 0, size );
        }
    }

}
