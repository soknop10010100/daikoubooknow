package com.eazy.daikou.ui.home.development_project

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.denzcoskun.imageslider.ImageSlider
import com.denzcoskun.imageslider.constants.ScaleTypes
import com.denzcoskun.imageslider.models.SlideModel
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.request_data.request.project_development_ws.ProjectDevelopmentWs
import com.eazy.daikou.model.home_page.home_project.DataDetail
import com.eazy.daikou.model.home_page.home_project.RelatedBranches
import com.eazy.daikou.ui.ShowMultipleDisplayImagesActivity
import com.eazy.daikou.ui.home.development_project.adapter.PropertyHighlightAdapter
import com.eazy.daikou.ui.home.development_project.adapter.PropertyProgressItemAdapter
import com.eazy.daikou.ui.home.development_project.adapter.PropertyRelatedItemAdapter
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions


class PropertiesItemDetailActivity : BaseActivity(), OnMapReadyCallback {

    private lateinit var textNameBuilding: TextView
    private lateinit var textAddress: TextView
    private lateinit var textAddressCity: TextView
    private lateinit var textPhoneProperty: TextView
    private lateinit var textDirection: TextView
    private lateinit var textCountry: TextView
    private lateinit var imageSlider: ImageSlider
    private lateinit var progress: View
    private lateinit var buttonSeeMoreImage: ImageView
    private lateinit var recyclerViewSimilar: RecyclerView
    private lateinit var scrollView: ScrollView

    private lateinit var textDescription: TextView

    private lateinit var buttonRefresh: ImageView
    private lateinit var layoutMap: View
    private lateinit var layoutSimilar: View
    private lateinit var mMap: GoogleMap

    private var listImages = ArrayList<String>()

    private lateinit var layoutNoInternet: LinearLayout
    private lateinit var layoutBody: RelativeLayout
    private lateinit var layoutHeader: View
    private lateinit var layoutConstruction: View
    private lateinit var layoutFeature: View
    private lateinit var layoutName: View
    private var id = 0

    private var x: Double = 0.0
    private var y: Double = 0.0
    private var urlWebsite = ""

    private lateinit var recyclerViewHighlight: RecyclerView
    private lateinit var recyclerViewOverView: RecyclerView
    private lateinit var recyclerViewFeaturing: RecyclerView
    private lateinit var recyclerViewAmenities: RecyclerView
    private lateinit var recyclerViewProgress: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_properties_item_detail)

        initView()

        initData()

        initAction()

    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
    }

    private fun initData() {
        if (intent.hasExtra("id")) {
            id = intent.getStringExtra("id")!!.toInt()
        }
    }

    //static
    private fun setRecyclerViewHighlight() {
        recyclerViewHighlight.layoutManager = GridLayoutManager(this, 2)
        recyclerViewOverView.layoutManager = GridLayoutManager(this, 2)
        recyclerViewFeaturing.layoutManager = GridLayoutManager(this, 2)

        val listTitleHighlight = arrayListOf(
            "Project Information",
            "Previous Period",
            "Next Period",
            "Risk Status",
            "Issues Status"
        )

        val listIconHighlight = arrayListOf(
            R.drawable.default_dot,
            R.drawable.default_dot,
            R.drawable.default_dot,
            R.drawable.default_dot,
            R.drawable.default_dot,
        )

        recyclerViewHighlight.adapter =
            PropertyHighlightAdapter(listTitleHighlight, listIconHighlight)

        val listTitleOverView = arrayListOf(
            "Total Unit",
            "Bed room unit",
            "Floor unit",
            "Security"
        )

        val listIconOverview = arrayListOf(
            R.drawable.icons8_double_bed_90,
            R.drawable.icons8_double_bed_90,
            R.drawable.floor_plan,
            R.drawable.security_guard
        )

        recyclerViewOverView.adapter = PropertyHighlightAdapter(listTitleOverView, listIconOverview)

        val listTitleFeaturing = arrayListOf(
            "Air Conditioning",
            "Pay TV",
            "Pet Friendly",
            "Internet / Wifi",
            "Balcony"
        )

        recyclerViewFeaturing.adapter =
            PropertyHighlightAdapter(listTitleFeaturing, listIconHighlight)

        val listTitleAmenities = arrayListOf(
            "Car Parking",
            "Non-Flooding",
            "Commercial area",
            "Playground",
            "Common Area",
            "Sports Facilities",
            "Garden",
            "Swimming Pool",
            "Gym/Fitness Center",
            "Tennis Court",
            "Lift / Elevator",
            "Backup Electricity / Generator"
        )

        val listIconAmenities = arrayListOf(
            R.drawable.default_dot,
            R.drawable.default_dot,
            R.drawable.default_dot,
            R.drawable.default_dot,
            R.drawable.default_dot,
            R.drawable.default_dot,
            R.drawable.default_dot,
            R.drawable.default_dot,
            R.drawable.default_dot,
            R.drawable.default_dot,
            R.drawable.default_dot,
            R.drawable.default_dot,
        )

        recyclerViewAmenities.layoutManager =
            GridLayoutManager(this@PropertiesItemDetailActivity, 2)
        recyclerViewAmenities.adapter =
            PropertyHighlightAdapter(listTitleAmenities, listIconAmenities)


        val listTextHeader = arrayListOf(
            "10%",
            "40%",
            "2030"
        )

        val listTextBody = arrayListOf(
            "Downpayment Ratio",
            "Construction Progress",
            "Completion Year"
        )

        val listIcon = arrayListOf(
            R.drawable.ic_percent,
            R.drawable.ic_percent,
            R.drawable.ic_calendar
        )

        recyclerViewProgress.layoutManager = LinearLayoutManager(
            this@PropertiesItemDetailActivity,
            LinearLayoutManager.HORIZONTAL,
            false
        )
        recyclerViewProgress.adapter = PropertyProgressItemAdapter(
            listTextHeader,
            listTextBody,
            listIcon
        )
    }

    private fun initView() {

        textNameBuilding = findViewById(R.id.text_name_building)
        textAddress = findViewById(R.id.text_address)
        textAddressCity = findViewById(R.id.text_address_city)
        textPhoneProperty = findViewById(R.id.text_phone)
        textDirection = findViewById(R.id.text_direction)
        textCountry = findViewById(R.id.text_address_country)

        imageSlider = findViewById(R.id.img_slider)
        progress = findViewById(R.id.progress)
        buttonSeeMoreImage = findViewById(R.id.btn_see_more_image)
        recyclerViewSimilar = findViewById(R.id.recyclerView_similar)
        scrollView = findViewById(R.id.scroll_view)

        layoutBody = findViewById(R.id.body)
        layoutNoInternet = findViewById(R.id.layout_no_internet)
        buttonRefresh = findViewById(R.id.btn_refresh)
        layoutSimilar = findViewById(R.id.layout_similar)

        layoutMap = findViewById(R.id.layout_map)

        textDescription = findViewById(R.id.text_description)

        val mapFragment =
            supportFragmentManager.findFragmentById(R.id.map_google) as SupportMapFragment
        mapFragment.getMapAsync(this)

        recyclerViewHighlight = findViewById(R.id.recyclerView_highlight)
        recyclerViewOverView = findViewById(R.id.recyclerView_over_view)
        recyclerViewFeaturing = findViewById(R.id.recyclerView_featuring)
        recyclerViewAmenities = findViewById(R.id.recyclerView_amenities)
        recyclerViewProgress = findViewById(R.id.recyclerView_progress)

        layoutConstruction = findViewById(R.id.layout_construction)
        layoutHeader = findViewById(R.id.layout_header)
        layoutFeature = findViewById(R.id.layout_feature)
        layoutName = findViewById(R.id.layout_name)
    }

    private fun initAction() {
        findViewById<ImageView>(R.id.img_back).setOnClickListener { finish() }

        buttonSeeMoreImage.setOnClickListener {
            if (listImages.isNotEmpty()) {
                val intent = Intent(this, ShowMultipleDisplayImagesActivity::class.java)
                intent.putExtra("image_list", listImages)
                startActivity(intent)
            }
        }

        textDirection.setOnClickListener {
            val gmmIntentUri = Uri.parse("google.navigation:q=$x,$y&mode=b")
            val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
            mapIntent.setPackage("com.google.android.apps.maps")
            startActivity(mapIntent)
        }

        buttonRefresh.setOnClickListener {
            hideView(false)
            getDetailProperty()
        }

        getDetailProperty()

        setRecyclerViewHighlight()

    }

    //set view
    private fun setView(propertyDetail: DataDetail) {
        textNameBuilding.text = propertyDetail.name

        //set city
        if (propertyDetail.city != null) {
            textAddressCity.text = propertyDetail.city
        } else {
            textAddressCity.text = ". . ."
        }

        //set address
        if (propertyDetail.address != null) {
            textAddress.text = propertyDetail.address
        } else {
            textAddress.text = ". . ."
        }

        //set phone number
        if (propertyDetail.phone != null) {
            textPhoneProperty.text = "+${propertyDetail.phone}"
        } else {
            textPhoneProperty.text = ". . ."
        }

        //set country
        if (propertyDetail.country != null) {
            textCountry.visibility = View.VISIBLE
            textCountry.text = ", ${propertyDetail.country}"
        } else {
            textCountry.visibility = View.GONE
        }

        //set website
        if (propertyDetail.website != "" && propertyDetail.website != null) {
            urlWebsite = propertyDetail.website!!
            findViewById<LinearLayout>(R.id.layout_website).visibility = View.VISIBLE
        } else {
            findViewById<LinearLayout>(R.id.layout_website).visibility = View.GONE
        }

    }

    //set image slider
    private fun setImageSlider(listImages: ArrayList<String>) {
        val slideModels = ArrayList<SlideModel>()
        for (index in listImages.indices) {
            slideModels.add(SlideModel(listImages[index].replace("\"", "")))
        }
        imageSlider.setImageList(slideModels, ScaleTypes.CENTER_CROP)
    }

    //click related
    private val clickRelated = object : PropertyRelatedItemAdapter.OnClickProperty {
        override fun click(relatedBranche: RelatedBranches) {
            val intent = Intent(this@PropertiesItemDetailActivity,PropertiesItemDetailActivity::class.java)
            intent.putExtra("id",relatedBranche.id)
            startActivity(intent)
        }

    }

    //result detail
    private val resultDetailListener = object : ProjectDevelopmentWs.OnRespondListener {
        override fun onSuccess(propertyDetailModel: DataDetail) {
            hideView(false)
            //set map
            if (propertyDetailModel.coordLat != null && propertyDetailModel.coordLong != null) {
                initMap(
                    LatLng(
                        propertyDetailModel.coordLat!!.toDouble(),
                        propertyDetailModel.coordLong!!.toDouble()
                    ),
                    propertyDetailModel.name!!
                )

                x = propertyDetailModel.coordLat!!.toDouble()
                y = propertyDetailModel.coordLong!!.toDouble()
                layoutMap.visibility = View.VISIBLE
            } else {
                layoutMap.visibility = View.GONE
            }


            //set detail
            setView(propertyDetailModel)
            layoutBody.visibility = View.VISIBLE
        }

        override fun onRelatedProperty(listRelatedBranches: ArrayList<RelatedBranches>) {
            hideView(false)
            showProgress(false)
            if (listRelatedBranches.isNotEmpty()) {
                for (index in listRelatedBranches.indices) {
                    if (listRelatedBranches[index].id!!.toInt() == this@PropertiesItemDetailActivity.id) {
                        listRelatedBranches.removeAt(index)
                        break
                    }
                }
                recyclerViewSimilar.layoutManager = LinearLayoutManager(
                    this@PropertiesItemDetailActivity,
                    LinearLayoutManager.HORIZONTAL,
                    false
                )
                recyclerViewSimilar.adapter = PropertyRelatedItemAdapter(
                    listRelatedBranches,
                    clickRelated
                )
                layoutSimilar.visibility = View.VISIBLE
            } else {
                layoutSimilar.visibility = View.GONE
            }
        }

        override fun onImage(listImage: List<String>) {
            showProgress(false)
            listImages.clear()
            listImages.addAll(listImage)
            setImageSlider(listImages)
        }

        override fun onError(msg: String) {
            showProgress(false)
            Toast.makeText(this@PropertiesItemDetailActivity, msg, Toast.LENGTH_LONG).show()
            layoutBody.visibility = View.GONE
        }

        override fun onInternet(msg: String) {
            Toast.makeText(this@PropertiesItemDetailActivity, msg, Toast.LENGTH_LONG).show()
            showProgress(false)
            hideView(true)
        }

    }

    //get detail
    private fun getDetailProperty() {
        showProgress(true)
        ProjectDevelopmentWs.getDetail(this, id, resultDetailListener)
    }


    //init map
    private fun initMap(latLng: LatLng, name: String) {
        // move camera to location property
        mMap.moveCamera(
            CameraUpdateFactory.newLatLngZoom(latLng, 15f)
        )

        // add maker
        mMap.addMarker(
            MarkerOptions().title(name).position(latLng)
        )
    }


    //show progress
    private fun showProgress(show: Boolean) {
        if (show) {
            progress.visibility = View.VISIBLE
        } else {
            progress.visibility = View.GONE
        }

    }


    // hide view
    private fun hideView(show: Boolean) {
        if (show) {
            layoutBody.visibility = View.GONE
            layoutNoInternet.visibility = View.VISIBLE
        } else {
            layoutBody.visibility = View.VISIBLE
            layoutNoInternet.visibility = View.GONE
        }

    }
}