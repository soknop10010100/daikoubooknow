package com.eazy.daikou.ui.home.service_provider_new.adapter_service_provider

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.AppCompatRatingBar
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.model.service_provider.SubCategory
import org.w3c.dom.Text

class CompanyListAdapter(private val  context: Context): RecyclerView.Adapter<CompanyListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
       val view = LayoutInflater.from(context).inflate(R.layout.custom_list_company_provider, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

//        val companyModel : SubCategory = companyList[position]
      //  if (companyModel != null){
            Glide.with(context).load(R.drawable.cv_home_overtime).into(holder.imageCoverCompany)
      //  }

    }

    override fun getItemCount(): Int {
        return 20
    }

    class  ViewHolder(view: View): RecyclerView.ViewHolder(view){
        var imageCoverCompany : ImageView = view.findViewById(R.id.imageCoverCompany)
        var numberRateTv : TextView = view.findViewById(R.id.numberRateTv)
        var rateStarRB : AppCompatRatingBar = view.findViewById(R.id.rateStarRB)
        var companyNameTv : TextView = view.findViewById(R.id.companyNameTv)
       // var addressTv : TextView = view.findViewById(R.id.addressTv)
        //var phoneNumberTv : TextView = view.findViewById(R.id.phoneNumberTv)
        var directionBtn : TextView = view.findViewById(R.id.directionBtn)
        var contactBtn : TextView = view.findViewById(R.id.contactBtn)
        var rateAndReviewBtn : TextView = view.findViewById(R.id.rateAndReviewBtn)
    }

    interface CallBackListCompany{
        fun clickCompanyListener(company: SubCategory)
    }
}