package com.eazy.daikou.ui.home.calendar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.eazy.daikou.R;
import com.eazy.daikou.base.BaseActivity;

public class RepeatActivity extends BaseActivity implements View.OnClickListener {

    private TextView titleToolbar;
    private ImageView iconBack;
    private LinearLayout neverLayout, everyDayLayout, everyWeekLayout, everyMonthLayout, everyYearLayout ,alert30mnLayout, alert1hrLayout, alertBorder;
    private TextView neverTxt, everyDayTxt, everyWeekTxt, everyMonthTxt, everyYearTxt, alert30mnTxt, alert1hrTxt;
    private int requestCode;
    private View lin1_event , lin2_event ,lin3_event ,lin4_event ,lin_event_ShowAs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repeat);

        initView();
        initData();
        initAction();
    }

    private void initData(){
        if (getIntent() !=null && getIntent().hasExtra("repeat_alert")){
            requestCode = getIntent().getIntExtra("repeat_alert",-1);
        }
    }
    private void initView(){
        titleToolbar = findViewById(R.id.titleToolbar);
        iconBack = findViewById(R.id.iconBack);

        neverLayout = findViewById(R.id.neverLayout);
        everyDayLayout = findViewById(R.id.everydayLayout);
        everyWeekLayout = findViewById(R.id.everyWeekLayout);
        everyMonthLayout = findViewById(R.id.everyMonthLayout);
        everyYearLayout = findViewById(R.id.everyYearLayout);
        alert30mnLayout = findViewById(R.id.alert30mn_Layout);
        alert1hrLayout = findViewById(R.id.alert1hr_Layout);
        alertBorder = findViewById(R.id.alert_border);

        neverTxt = findViewById(R.id.neverTxt);
        everyDayTxt = findViewById(R.id.everyDayTxt);
        everyWeekTxt = findViewById(R.id.everyWeekTxt);
        everyMonthTxt = findViewById(R.id.everyMonthTxt);
        everyYearTxt = findViewById(R.id.everyYearTxt);
        alert30mnTxt = findViewById(R.id.alert30mn_Txt);
        alert1hrTxt = findViewById(R.id.alert1hr_Txt);

        lin1_event = findViewById(R.id.lin1_event);
        lin2_event = findViewById(R.id.lin2_event);
        lin3_event = findViewById(R.id.lin3_event);
        lin4_event = findViewById(R.id.lin4_event);
        lin_event_ShowAs = findViewById(R.id.lin_event_ShowAs);
    }

    private void initAction(){
        iconBack.setOnClickListener(view -> onBackPressed());

        if (requestCode == 510 ){
            titleToolbar.setText(R.string.repeat);
        } else if (requestCode == 515){
            titleToolbar.setText(R.string.alert);
            setDataTitleAlert();
        } else if (requestCode == 520){
            titleToolbar.setText(R.string.show_as);
            setDataTitleShowAs();
        }

        neverLayout.setOnClickListener(this);
        everyDayLayout.setOnClickListener(this);
        everyWeekLayout.setOnClickListener(this);
        everyMonthLayout.setOnClickListener(this);
        everyYearLayout.setOnClickListener(this);

        //alert
        alert30mnLayout.setOnClickListener(this);
        alert1hrLayout.setOnClickListener(this);

    }

    private void setDataTitleAlert(){
        neverTxt.setText(R.string.none);
        everyDayTxt.setText(R.string.at_time_of_event);
        everyWeekTxt.setText(R.string.five_minutes_before);
        everyMonthTxt.setText(R.string.ten_minutes_before);
        everyYearTxt.setText(R.string.fifteen_minutes_before);
        alert30mnTxt.setText(R.string.thirdty_minutes_before);
        alert1hrTxt.setText(R.string.one_hore_before);
        alertBorder.setVisibility(View.VISIBLE);
    }
    private void setDataTitleShowAs(){
        neverTxt.setText(R.string.busy);
        everyDayTxt.setText(R.string.free);
        lin_event_ShowAs.setVisibility(View.VISIBLE);
        lin1_event.setVisibility(View.GONE);
        lin2_event.setVisibility(View.GONE);
        lin3_event.setVisibility(View.GONE);
        lin4_event.setVisibility(View.GONE);
        everyWeekLayout.setVisibility(View.GONE);
        everyMonthLayout.setVisibility(View.GONE);
        everyYearLayout.setVisibility(View.GONE);
        alertBorder.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View view) {
        int idView = view.getId();

        if (idView == R.id.neverLayout){
            String neverStringBack = neverTxt.getText().toString();
            getIntent("never_key" , neverStringBack);
        } else if (idView == R.id.everydayLayout){
            String everyDayStringBack = everyDayTxt.getText().toString();
            getIntent("everyday_key" , everyDayStringBack);
        } else if (idView == R.id.everyWeekLayout){
            String everyWeekStringBack = everyWeekTxt.getText().toString();
            getIntent("everyWeek_key", everyWeekStringBack);
        } else if (idView == R.id.everyMonthLayout){
            String everyMonthStringBack = everyMonthTxt.getText().toString();
            getIntent("everyMonth_key", everyMonthStringBack);
        } else if (idView == R.id.everyYearLayout){
            String everyYearStringBack = everyYearTxt.getText().toString();
            getIntent("everyYear_key", everyYearStringBack);
        } else if (idView == R.id.alert30mn_Layout){
            String everyYearStringBack = alert30mnTxt.getText().toString();
            getIntent("alert30mn_key", everyYearStringBack);
        } else if (idView == R.id.alert1hr_Layout){
            String everyYearStringBack = alert1hrTxt.getText().toString();
            getIntent("alert1hr_key", everyYearStringBack);
        }
    }

    private void getIntent(String key, String value){
        Intent intent = getIntent();
        intent.putExtra(key, value);
        setResult(RESULT_OK, intent);
        finish();
    }
}