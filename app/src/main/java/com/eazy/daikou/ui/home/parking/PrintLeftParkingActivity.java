package com.eazy.daikou.ui.home.parking;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.eazy.daikou.R;
import com.eazy.daikou.base.BaseActivity;
import com.eazy.daikou.request_data.request.parking_ws.PaymentParkingWs;
import com.eazy.daikou.helper.DateUtil;
import com.eazy.daikou.helper.Utils;
import com.eazy.daikou.model.parking.CheckInOutPrinter;
import com.eazy.daikou.helper.web.WebPayActivity;
import com.eazy.daikou.ui.home.parking.printer.SunmiPrintHelper;

public class PrintLeftParkingActivity extends BaseActivity implements SelectPaymentMethodBottomSheetFragment.OnClickFeedBack{

    private TextView ticketTv,orderNoTv,
            timeInTv,timeOutTv,timeUsedTv,parkingFeeTv,totalPriceTv,titleTv;
    private ImageView btnBack;
    private TextView btnPaid;
    private TextView dateTv,parkingLotTypeTv;
    private CheckInOutPrinter checkOutPrinter;
    private ImageView imageCar;
    private String userId;
    private LinearLayout relLayoutLot ;

    private final int CODE_REQUEST_PRINTER = 8970;
    private final int CODE_REQUEST_BACK = 8745;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_print_left_parking);

        initView();
        initData();
        initAction();
    }

    private void initView(){
         titleTv = findViewById(R.id.titleToolbar);
         btnBack = findViewById(R.id.iconBack);
         ticketTv = findViewById(R.id.ticket_no);
         orderNoTv = findViewById(R.id.order_no);
         totalPriceTv = findViewById(R.id.total_price);
         timeInTv = findViewById(R.id.time_in);
         timeOutTv = findViewById(R.id.time_out);
         timeUsedTv = findViewById(R.id.time_used);
         parkingFeeTv = findViewById(R.id.parking_fee);
         btnPaid = findViewById(R.id.btn_paid);
         imageCar = findViewById(R.id.image_car);
         dateTv = findViewById(R.id.date_text);
         parkingLotTypeTv  = findViewById(R.id.parking_lot_type);
         relLayoutLot = findViewById(R.id.relLayout_lot);
    }

    @SuppressLint("SetTextI18n")
    private  void initData(){
        titleTv.setText(R.string.checkout_detail);
        titleTv.setAllCaps(true);
        if(getIntent().hasExtra("checkOutPrinter")){
            checkOutPrinter = (CheckInOutPrinter) getIntent().getSerializableExtra("checkOutPrinter");
        }
        if(getIntent().hasExtra("user_id")){
            userId = getIntent().getStringExtra("user_id");
        }

        ticketTv.setText(checkOutPrinter.getTicketNo().toString());
        if(checkOutPrinter.getTimeIn()!=null){
            timeInTv.setText(DateUtil.formatTimeLocal(checkOutPrinter.getTimeIn()));
        }
        if(checkOutPrinter.getTimeOut()!=null){
            timeOutTv.setText(DateUtil.formatTimeLocal(checkOutPrinter.getTimeOut().toString()));
            dateTv.setText(DateUtil.formatTimeZoneLocalDay(checkOutPrinter.getTimeOut().toString()));
        }
        if (checkOutPrinter.getTimeDurationInMinute()!=null){
            timeUsedTv.setText(Utils.covertTime(checkOutPrinter.getTimeDurationInMinute(),"time"));
        }
        if (checkOutPrinter.getParkingFee()!=null){
            parkingFeeTv.setText(checkOutPrinter.getCurrency()+ " " + checkOutPrinter.getParkingFee());
            totalPriceTv.setText(checkOutPrinter.getCurrency()+ " " + checkOutPrinter.getParkingFee());
        }
        if(checkOutPrinter.getOrderNo()!=null){
            orderNoTv.setText(checkOutPrinter.getOrderNo());
        }

        if(checkOutPrinter.getParkingLotType() != null){
            parkingLotTypeTv.setText(checkOutPrinter.getParkingLotType().toString());
        }else {
            relLayoutLot.setVisibility(View.GONE);
        }

        if(checkOutPrinter.getCarImage()!=null){
            Glide.with(imageCar).load(checkOutPrinter.getCarImage()).into(imageCar);
        }else {
            Glide.with(imageCar).load(R.drawable.home_parking).into(imageCar);

        }
    }

    private void initAction(){
        btnBack.setOnClickListener(view -> finish());

        btnPaid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SelectPaymentMethodBottomSheetFragment bottom = new SelectPaymentMethodBottomSheetFragment().newInstance("action", 0);
                bottom.show(getSupportFragmentManager(), "SelectPaymentMethodBottomSheetFragment");

            }
        });
    }


    @Override
    public void onClickSubmit(@NonNull String txtChoose, int image, int position) {

        if (txtChoose.equals("PAY BY CASH")){
            Intent intent = new Intent(PrintLeftParkingActivity.this,PaidDialogActivity.class);
            intent.putExtra("check_out",checkOutPrinter);
            intent.putExtra("parking_fee",checkOutPrinter.getParkingFee() + "");
            intent.putExtra("parking_id",checkOutPrinter.getId());
            intent.putExtra("user_id",userId);
            intent.putExtra("type","printer");
            startActivityForResult(intent,CODE_REQUEST_BACK);
        }else {
            getLinkPayment(checkOutPrinter.getId());
        }

    }

    //Payment method Kess
    private void getLinkPayment(String parkingId) {
        new PaymentParkingWs().getPaymentKessPrinter(this,parkingId, new PaymentParkingWs.OnRequestPaymentCallBack() {
            @Override
            public void onLoadSuccess(String linkKESSPay) {
                Intent intent = new Intent(PrintLeftParkingActivity.this, WebPayActivity.class);
                intent.putExtra("linkUrl", linkKESSPay);
                intent.putExtra("toolBarTitle", "Kess Pay");
                startActivityForResult(intent,CODE_REQUEST_PRINTER);
            }

            @Override
            public void onLoadFail(String message) {
                Toast.makeText(PrintLeftParkingActivity.this, message, Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(data!=null && resultCode== RESULT_OK){
            if(requestCode == CODE_REQUEST_BACK){
                Intent intent = new Intent();
                setResult(RESULT_OK,intent);
                finish();
            }else if(requestCode == CODE_REQUEST_PRINTER){
                SunmiPrintHelper.getInstance().printReceipt(this,checkOutPrinter,checkOutPrinter.getCurrency(),checkOutPrinter.getParkingFee().toString(),checkOutPrinter.getParkingFee().toString(),"0","online");
                Intent intent = new Intent();
                setResult(RESULT_OK,intent);
                finish();
            }

        }
    }
}