package com.eazy.daikou.ui.home.book_now.booking_account;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;

import androidx.cardview.widget.CardView;

import com.eazy.daikou.R;
import com.eazy.daikou.base.BaseActivity;
import com.eazy.daikou.base.MockUpData;
import com.eazy.daikou.request_data.request.profile_ws.SignUpWs;
import com.eazy.daikou.helper.AppAlertCusDialog;
import com.eazy.daikou.helper.Utils;
import com.eazy.daikou.ui.sign_up.VerifySmsCodeActivity;
import com.google.android.material.textfield.TextInputEditText;
import com.rilixtech.widget.countrycodepicker.CountryCodePicker;

import java.util.HashMap;
import java.util.Objects;

public class SignUpBookNowActivity extends BaseActivity implements View.OnClickListener {

    private EditText firstNameEd, lastNameEd, emailEd, passwordEd, conPasswordEd;
    private CardView submitBtn;
    private CountryCodePicker countryCodePicker;
    private TextInputEditText  phoneEd;

    private ProgressBar progressBar;
    private AppAlertCusDialog app;
    private boolean isCheckPhoneFirst = true;
    private String getPhoneNumber = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hotel_sign_up_activity);

        initView();

        initAction();
    }

    private void initView() {
        Utils.customOnToolbar(this, getString(R.string.sign_up).toUpperCase(), this::finish);
        findViewById(R.id.layoutMap).setVisibility(View.INVISIBLE);

        submitBtn = findViewById(R.id.submitBtn);
        firstNameEd = findViewById(R.id.firstNameTv);
        lastNameEd = findViewById(R.id.lastNameTv);
        emailEd = findViewById(R.id.emailTv);
        phoneEd = findViewById(R.id.txtPhoneNumber);
        passwordEd = findViewById(R.id.passwordTv);
        conPasswordEd = findViewById(R.id.conPasswordTv);
        countryCodePicker = findViewById(R.id.ccp);
        countryCodePicker.setDefaultCountryUsingNameCodeAndApply(BaseActivity.country_code);

        progressBar = findViewById(R.id.progressItem);
        progressBar.setVisibility(View.GONE);

        //Set Value
        phoneEd.setHint(getResources().getString(R.string.phone_number_));
        passwordEd.setHint(getResources().getString(R.string.password_6_digits));
        conPasswordEd.setHint(getResources().getString(R.string.confirm_password));
    }

    private void initAction() {
        app = new AppAlertCusDialog();
        submitBtn.setOnClickListener(this);
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.submitBtn) {
            Utils.checkEnableView(view);
            checkValidateInfo();
        }
    }

    private void checkValidateInfo(){
        if(firstNameEd.getText().toString().isEmpty()){
            app.showDialog(SignUpBookNowActivity.this, getResources().getString(R.string.first_name_is_require));
        }
        else if(lastNameEd.getText().toString().isEmpty()){
            app.showDialog(SignUpBookNowActivity.this, getResources().getString(R.string.last_name_is_require));
        }
        else if (Objects.requireNonNull(phoneEd.getText()).toString().isEmpty()) {
            app.showDialog(SignUpBookNowActivity.this, getResources().getString(R.string.phone_number_is_require));
        } else if (passwordEd.getText().toString().isEmpty()) {
            app.showDialog(SignUpBookNowActivity.this, getResources().getString(R.string.password_is_require));
        } else if(conPasswordEd.getText().toString().isEmpty()){
            app.showDialog(SignUpBookNowActivity.this, getResources().getString(R.string.confirm_password_is_require));
        } else if(!passwordEd.getText().toString().equals(conPasswordEd.getText().toString())){
            app.showDialog(SignUpBookNowActivity.this, getResources().getString(R.string.password_and_confirm_password_must_the_same));
        } else {
            validateEmail();
        }
    }

    private void validateEmail(){
        progressBar.setVisibility(View.VISIBLE);
        new SignUpWs().validateEmail(checkEmailPhoneExist(), isBookNowApp, new SignUpWs.SignUpInterface() {
            @Override
            public void onSuccess(String message, String pin_code, String user_id) {
                progressBar.setVisibility(View.GONE);
                if (isCheckPhoneFirst && !emailEd.getText().toString().isEmpty()){
                    isCheckPhoneFirst = false;
                    validateEmail();
                } else {
                    requestServiceDate();
                }
            }

            @Override
            public void onFailed(String mess) {
                progressBar.setVisibility(View.GONE);
                app.showDialog(SignUpBookNowActivity.this, mess);
            }

            @Override
            public void onError(String mess, int code) {
                progressBar.setVisibility(View.GONE);
                app.showDialog(SignUpBookNowActivity.this, mess);
            }
        });
    }

    private HashMap<String, Object> checkEmailPhoneExist(){
        HashMap<String, Object> hashMap = new HashMap<>();
        if (isCheckPhoneFirst){
            String phone = getPhoneNumber = Objects.requireNonNull(phoneEd.getText()).toString().trim();
            if (phone.startsWith("0")) {
                getPhoneNumber = phone.substring(1);
            } else {
                getPhoneNumber = phone;
            }
            String phoneNumber = countryCodePicker.getDefaultCountryCode().concat(getPhoneNumber);
            hashMap.put("username", phoneNumber);
        } else {
            hashMap.put("username", emailEd.getText().toString().trim());
        }
        return hashMap;
    }

    private void requestServiceDate() {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("first_name", firstNameEd.getText().toString());
        hashMap.put("last_name", lastNameEd.getText().toString());
        hashMap.put("email", emailEd.getText().toString());
        hashMap.put("phone_code", countryCodePicker.getDefaultCountryCode());
        hashMap.put("phone_no", getPhoneNumber);
        hashMap.put("password", passwordEd.getText().toString());
        MockUpData.setHashMapAccount(hashMap);

        requestPinCodeWs();
    }

    private void requestPinCodeWs(){
        HashMap<String,Object> hashMap = new HashMap<>();
        String phoneNumber = countryCodePicker.getDefaultCountryCode().concat(Objects.requireNonNull(phoneEd.getText()).toString().trim());
        hashMap.put("username", phoneNumber);
        hashMap.put("action", "send");
        progressBar.setVisibility(View.VISIBLE);
        new SignUpWs().sendVerifyPinCode(hashMap, isBookNowApp, new SignUpWs.SignUpInterface() {
            @Override
            public void onSuccess(String message, String pin_code, String user_id) {
                progressBar.setVisibility(View.GONE);
                Intent intent = new Intent(SignUpBookNowActivity.this, VerifySmsCodeActivity.class);
                intent.putExtra("phone", countryCodePicker.getDefaultCountryCodeWithPlus() + " " + phoneEd.getText().toString());
                intent.putExtra("hashmap_request_code", hashMap);
                intent.putExtra("pin_code", pin_code);
                startActivity(intent);
            }

            @Override
            public void onFailed(String mess) {
                progressBar.setVisibility(View.GONE);
                app.showDialog(SignUpBookNowActivity.this, mess);
            }

            @Override
            public void onError(String mess, int code) {
                progressBar.setVisibility(View.GONE);
                app.showDialog(SignUpBookNowActivity.this, mess);
            }
        });
    }

}