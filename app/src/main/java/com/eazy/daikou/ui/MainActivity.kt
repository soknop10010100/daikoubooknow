package com.eazy.daikou.ui

import android.content.Intent
import android.content.IntentSender.SendIntentException
import android.graphics.Color
import android.os.Bundle
import android.provider.Settings
import android.view.KeyEvent
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.Constant
import com.eazy.daikou.repository_ws.Constant.TAG
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.base.MockUpData
import com.eazy.daikou.helper.AppAlertCusDialog
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.helper.web.WebsiteFragment
import com.eazy.daikou.ui.home.home_menu.CommentHomeActionFragment
import com.eazy.daikou.ui.home.home_menu.HomeFragment
import com.eazy.daikou.ui.profile.ProfileFragment
import com.eazy.daikou.ui.home.my_document.MyDocumentFragment
import com.google.android.material.snackbar.Snackbar
import com.google.android.play.core.appupdate.AppUpdateInfo
import com.google.android.play.core.appupdate.AppUpdateManager
import com.google.android.play.core.appupdate.AppUpdateManagerFactory
import com.google.android.play.core.install.InstallStateUpdatedListener
import com.google.android.play.core.install.model.AppUpdateType.FLEXIBLE
import com.google.android.play.core.install.model.AppUpdateType.IMMEDIATE
import com.google.android.play.core.install.model.InstallStatus
import com.google.android.play.core.install.model.UpdateAvailability

class MainActivity : BaseActivity() {

    private lateinit var btnHome: LinearLayout
    private lateinit var dashBoard: LinearLayout
    private lateinit var reminder:LinearLayout
    private lateinit var profile:LinearLayout
    private lateinit var commentDoAction:LinearLayout
    private lateinit var mainMenuLayout : LinearLayout
    private var lastPressedTime: Long = 0
    private val PERIOD = 2000
    private var MY_REQUEST_CODE = 190

    private lateinit var appUpdateManager : AppUpdateManager
    private var appUpdateInfoUp : AppUpdateInfo? = null

    private lateinit var active : Fragment
    private lateinit var fragmentManager: FragmentManager
    private lateinit var homeFragment: Fragment
    private lateinit var commentActionFragment: Fragment
    private lateinit var myDocumentFragment: Fragment
    private lateinit var websiteFragment: Fragment
    private lateinit var profileFragment: Fragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initView()

        initAction()

    }

    private fun initView(){
        dashBoard = findViewById(R.id.navigation_dashboard)
        reminder = findViewById(R.id.navigation_notification)
        profile = findViewById(R.id.profile)
        btnHome = findViewById(R.id.navigation_home)
        commentDoAction = findViewById(R.id.navigation_create)
        mainMenuLayout = findViewById(R.id.nav_view)

    }

    private fun initAction(){

        // Set Bottom Layout
        val menuBottomLayout = findViewById<LinearLayout>(R.id.menuBottomLayout)
        val layoutFragment = findViewById<LinearLayout>(R.id.layoutFragment)

        Utils.widthHeightLayout(menuBottomLayout) { _, height ->
            Utils.setNoMarginBottomOnButton(layoutFragment, height)
        }

        // GetLatestVersion(this@MainActivity).execute()

        getLatestVersion()

        // Device Token
        if (DEVICE_ID == null)  DEVICE_ID = Settings.Secure.getString(baseContext.contentResolver, Settings.Secure.ANDROID_ID)

        homeFragment = HomeFragment()
        commentActionFragment = CommentHomeActionFragment()
        myDocumentFragment = MyDocumentFragment()
        profileFragment = ProfileFragment()
        websiteFragment = WebsiteFragment.newInstance(Constant.url_beetube_news, "news")
        fragmentManager = supportFragmentManager
        active = homeFragment

        fragmentManager.beginTransaction().add(R.id.nav_fragment, profileFragment, "5").hide(profileFragment).commit()
        fragmentManager.beginTransaction().add(R.id.nav_fragment, websiteFragment, "4").hide(websiteFragment).commit()
        fragmentManager.beginTransaction().add(R.id.nav_fragment, commentActionFragment, "3").hide(commentActionFragment).commit()
        fragmentManager.beginTransaction().add(R.id.nav_fragment, myDocumentFragment, "2").hide(myDocumentFragment).commit()
        fragmentManager.beginTransaction().add(R.id.nav_fragment, homeFragment, "1").commit()

        onClickMenuNavigation()

    }

    private fun onClickMenuNavigation() {
        val categoryLayout: LinearLayout = findViewById(R.id.nav_view)
        for (i in 0 until categoryLayout.childCount) {
            Utils.logDebug("jeeeeeeeeeeeee", i.toString())
            categoryLayout.getChildAt(i).setOnClickListener { v ->
                setBackground(v.id, categoryLayout)
                val fragment : Fragment = when (i) {
                    0 -> {
                        homeFragment
                    }
                    1 -> {
                        myDocumentFragment
                    }
                    2 -> {
                        commentActionFragment
                    }
                    3 -> {
                        websiteFragment
                    }
                    4 -> {
                        profileFragment
                    }
                    else -> {
                        homeFragment
                    }
                }
                replaceFragment(fragment)
            }
        }
    }

    private fun setBackground(id: Int, categoryLayout: LinearLayout) {
        for (i in 0 until categoryLayout.childCount) {
            val rowView: View = categoryLayout.getChildAt(i)
            if (rowView is LinearLayout) {
                if (id == rowView.id) {
                    val icon : View = rowView.getChildAt(0)
                    if (icon is ImageView)  icon.setColorFilter(color(R.color.yellow))
                    rowView.getChildAt(1).visibility = View.VISIBLE
                } else {
                    val icon : View = rowView.getChildAt(0)
                    if (icon is ImageView)  icon.setColorFilter(color(R.color.white))
                    rowView.getChildAt(1).visibility = View.INVISIBLE
                }
            }
        }
    }

    private fun color(cr: Int): Int {
        return ContextCompat.getColor(this@MainActivity, cr)
    }

    private fun replaceFragment(fragment: Fragment) {
        fragmentManager.beginTransaction().hide(active).show(fragment).commit()
        active = fragment
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (event.keyCode == KeyEvent.KEYCODE_BACK) {
            if (event.action == KeyEvent.ACTION_DOWN) {
                if (event.downTime - lastPressedTime < PERIOD) {
                    finishAffinity()
                } else {
                    Toast.makeText(this, resources.getString(R.string.please_press_back_again_to_exit), Toast.LENGTH_SHORT).show()
                    lastPressedTime = event.eventTime
                }
                return true
            }
        }
        return false
    }

    override fun onDestroy() {
        MockUpData.isFirstHomeScreen = true
        super.onDestroy()
        if (appUpdateManager != null) {
            appUpdateManager.unregisterListener(listener)
        }
    }

    // Check Version App With Store
    private fun getLatestVersion(){
        appUpdateManager = AppUpdateManagerFactory.create(this)
        appUpdateManager.registerListener(listener)
        val appUpdateInfoTask = appUpdateManager.appUpdateInfo

        // Checks that the platform will allow the specified type of update.
        appUpdateInfoTask.addOnSuccessListener { appUpdateInfo ->
            appUpdateInfoUp = appUpdateInfo
            Utils.logDebug(TAG, appUpdateInfo.availableVersionCode().toString() + " KKK " + appUpdateInfo.updateAvailability()
                    + " KKK " + UpdateAvailability.UPDATE_AVAILABLE+ " KKK " + appUpdateInfo.isUpdateTypeAllowed(IMMEDIATE))
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE){
                // This example applies an immediate update. To apply a flexible update
                // instead, pass in AppUpdateType.FLEXIBLE
//                if(appUpdateInfo.isUpdateTypeAllowed(FLEXIBLE)) {
//                    val updateType = FLEXIBLE
//                     requestUpdate(appUpdateInfo, updateType)
//                    AppAlertCusDialog.checkUpdatePopUp(this)
//                } else if (appUpdateInfo.isUpdateTypeAllowed(IMMEDIATE)) {
//                    val updateType = IMMEDIATE
//                    requestUpdate(appUpdateInfo, updateType)
//                    AppAlertCusDialog.checkUpdatePopUp(this)
//                }

                if(appUpdateInfo.isUpdateTypeAllowed(FLEXIBLE) || appUpdateInfo.isUpdateTypeAllowed(IMMEDIATE)) {
                    AppAlertCusDialog.checkUpdatePopUp(this)
                }
            }
        }
    }

    private fun requestUpdate(appUpdateInfo: AppUpdateInfo, updateType: Int) {
        try {
            appUpdateManager.startUpdateFlowForResult(appUpdateInfo, updateType, this, MY_REQUEST_CODE)
        } catch (e: SendIntentException) {
            e.printStackTrace()
        }
    }

    private var listener = InstallStateUpdatedListener { state -> if (state.installStatus() == InstallStatus.DOWNLOADED) {
                // After the update is downloaded, show a notification
                // and request user confirmation to restart the app.
                // SnackBarManager.getSnackBarManagerInstance().showSnackBar(GaanaActivity.this, "An update has just been downloaded.", true);
        popupSnackBarForCompleteUpdate()
            }
        Utils.logDebug(TAG, "~Error code is: " + state.installErrorCode());
        Utils.logDebug(TAG, "~Package name is: " + state.packageName())
    }

    private fun popupSnackBarForCompleteUpdate() {
        val snackbar = Snackbar.make(findViewById(android.R.id.content), "An update has just been downloaded.", Snackbar.LENGTH_INDEFINITE)
        snackbar.setAction("Restart") {
            appUpdateManager.completeUpdate()
            appUpdateManager.unregisterListener(listener)
        }
        snackbar.setActionTextColor(Color.WHITE)
        snackbar.show()
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == MY_REQUEST_CODE) {
            println("App Update = $resultCode")
            if (resultCode != RESULT_OK) {
                println("Update flow failed! Result code: $resultCode")
                // If the update is cancelled or fails,
                // you can request to start the update again.
            }
        }
    }

}