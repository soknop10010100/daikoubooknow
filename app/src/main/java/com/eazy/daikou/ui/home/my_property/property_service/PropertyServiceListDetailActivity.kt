package com.eazy.daikou.ui.home.my_property.property_service

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.databinding.ActivityPropertyServiceListDetailBinding
import com.eazy.daikou.helper.AbsoluteFitLayoutManager
import com.eazy.daikou.helper.AppAlertCusDialog.alertPhoneCall
import com.eazy.daikou.helper.GetDataUtils
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.helper.Utils.startNewActivity
import com.eazy.daikou.model.my_property.property_service.PropertyServiceListModel
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.ui.home.my_property.property_service.adapter.PropertyServiceListAdapter
import com.eazy.daikou.ui.home.my_property.property_service.adapter.ServiceWorkingDayAdapter
import de.hdodenhof.circleimageview.CircleImageView

class PropertyServiceListDetailActivity : BaseActivity() {

    private lateinit var mBind : ActivityPropertyServiceListDetailBinding
    private lateinit var progressBar : ProgressBar
    private var serviceModel : PropertyServiceListModel ?= null
    private var action = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBind = ActivityPropertyServiceListDetailBinding.inflate(layoutInflater)
        setContentView(mBind.root)

        initAction()

    }

    private fun initAction() {
        progressBar = mBind.progressBar.progressItem
        progressBar.visibility = View.GONE

        Utils.customOnToolbar(this, resources.getString(R.string.service_detail)) { finish() }

        addAssignEmployeeWork()

        serviceModel = GetDataUtils.getDataFromModelClass("service_model", this)

        action = GetDataUtils.getDataFromString("action", this)


        setValueData()

        mBind.btn.setOnClickListener {
            if (action == "request_detail"){
                startNewActivity(this, PropertyServiceListActivity::class.java, "action", "property_service")
            }  else {
                Utils.customAlertDialogConfirm(this, "Do you want to cancel this request ?") { finish() }
            }
        }

    }

    private fun addAssignEmployeeWork(){
        val hashMap = HashMap<Int, Any>()
        for (i in 1..3) {
            val view: View = LayoutInflater.from(this).inflate(R.layout.assign_employee_service_model, null)
            val nameTv : TextView = view.findViewById(R.id.nameTv)
            val phoneTv : TextView = view.findViewById(R.id.phoneTv)
            val profileImage : CircleImageView = view.findViewById(R.id.profile_image)
            nameTv.text = "Mr Dara"
            phoneTv.text = "097 33 55 335"
            Glide.with(this).load("data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMTEhUTExMWEBUVGBUXFhUVFRUVFxUVFRcWFxUVFRUYHiggGBolGxUVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OGhAQGi0dHR8rLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAJ8BPgMBIgACEQEDEQH/xAAcAAACAwEBAQEAAAAAAAAAAAAEBQIDBgcBAAj/xABAEAABAwIEBAMFBgMIAQUAAAABAAIDBBEFEiExBkFRcSJhkRMygaGxBxQjUsHRJELwFTM0YnKSsuGCFkNTY6L/xAAaAQABBQEAAAAAAAAAAAAAAAAEAAECAwUG/8QANhEAAQMCAwYDBgUFAQAAAAAAAQACEQMhBBIxBSJBUWFxEzLBUoGRobHhIzRC8PEVJDNy0Qb/2gAMAwEAAhEDEQA/AOncOuuzXmNV5GR7Zw89ENgE4sB1VjjlqHK7iolLvtFqzFSF1rgOH/IBcWq+I7k+Eeq7F9rsZOGvsbe7/wAmr86PgcoODYupsc4WC1ZqHPaDpZWtab35IWkb+E1HN5LqKIGUR0XLV3kuJPMo7FpZqZjJMoyP2J80r/8AWbxyb6ra/aQGvoImi1zkt8AuTMwUlc9jHtfVLiLn+F0eCY+nRDAbD+VqI+MZB+VWDjWX/Ksy3B3IumwJxKFlnJGfic05dxnKebV4zjCXqPRUU/CErzoE8ofs0kd7zsqW57KW9zSyfiF8uUOdexGg29F2XCsTDY2+IbDdc5xP7Pm00RlEhJbqQba2TjGuFpKkROikMVgdr63tZTsG6KsglwW/biw7/FECNjxnIXJRgNfA9t5XSMuL2PLzBXVqF34Q7JmkHQKLgRxVEkbXnfZJOI8Le+MhlibJVXzzipcG7aahMH4s9jfxNdFExmsVYM0XXKuIYy15a4WIGoWbph4lqeLakvmc46XCzNL7xUG+dWO8i6FwA+0luoW7xx9o7rm3CTyJmW87rpeKNvDqrn6yqG6KWAkFgTYpLw37idkoUm6ICirnjRU3VxOiduhUX6hU2Xjivbrx6rU1X7RWMcqWwq2MWTpKYXtl8FIFQeHWgqTSBqvANUJXGyODUHiWysUBdY7HyXPapjYL7FRdwXx2VRMq8CAqnoCdtyiZpUG6VJPZfQssUW2VC51DOVF7A/VOxxbotjw+XEAgaBNjIDL56XuvsP4dMYGWUm3vapDxXRGBwc2VxLjrqtQuDRKzWjO6E344jElLlJuDa/qFybEMEjB6LUvq3uFnPLh0JSTG9wg3vzGyMp0sjYN0jqiGsDANjuq2PNwLK6t2uos5Lr8KD4Tb8AuPxpHjPtxKdOi9oGhxJAGgKez4FEIGkN6JNBJqB5BbCLxUvYLD2h+YeOgW7s38rTPX1WcjwyPoio6No2C9Y8dUQFkStiAvY3Fu2ivFXJsHkeiHUm7hNmPNLKEFxhO8U+rib2v6rb4VM0xxNA3A+iyHG5aaYW11C12Exn2cLvJv0RTfIUG/zBOzStO4SHGGuje0McQCbW5LSSHZIsblaZIxfW6cWKgEudQPD8+90BxL/dk22C10lsoWY4qj/DPYqsCHAq0GQuTcQyXeLfl/UpJSDxJtjukg7JXRnx9VP9af9K1nDb8s0Z87LrM8eaJcmwqL8SIjUFwI8txYrX8R8ZMgaIoxnlOn+UHmfO3RW1DoqabSbJtDXxUzC6V7Yx5nfsNz8ElqftGiLssLb8s8hDW98o1PxsuT4xXzTvL5C53n+luXZL6a+cDxEX1AKoDQrnGNF2ifj3KWMLGucfeNyADvoOluacYZxjDL4Tdh+BHqNfkuTYpTuMQeNswAN7kCw3XorXRgWN3i1ra36kkgKcDgo66ruDSSLg3B2PVetusPwbxjE/8ADlcWE/5bMv3uflotxl8736KjLCslXlVhpUA09VdEop1ZGCrAvFIJ1FSagsV2RrUFiuyc6JDVZKu95Rfsvap3iXkp0VRV4S+XdUFiueVS9ySdeheKLXL3MkkulNqH59DYO3Hmk3HdO3Kwg63/AEKdT0ZY0PB35d1nuLxlia4nmPmj6nlKDpiXAjRZlkRtuleMUxNtUzjqglmL1+1kGAUWYSqoaAzU63/VVseNAjJ4A6Eyc7oVjBoea7DBkmi3sFxuPAFZ3dPmRXy9gtjgsd6dze6yL3ZSweQWs4WmzMeP62WNtD813AW1s8AYQdCfqkopkW0IeWUhxFuZXzaryWOQVsghE2XqrY8nkpxuuop0NxMP4UrZ4BVZ44h5D6LIcRj+EctRwuPwoidLtbb/AGoun5EHV861jxosTxHHlqGOTSpxJ4qmw8i3Nf10+SD4nb441ZTuVS4QE4hbmYOwWe4t0jKbOkc1oIWexir9o5rTzOqhlOqsBGi5XxTTPDw62llnsNmvIQuz8Y4az7s91hcN0XFaQWmSbdyd1hZb3DLMAkJ8LQSe+wHe5Cz1ZXB0znNIc91xvq1vIN/dPLZqeTm0AO+YNj53+izFJBZ97b+p/ZJ9zdTp6WTOhwZ0hGYk35c/ULXYdwZCNbG/cFEYFSjKDz5rU0jdEMXGUWGgCUthwGIAty3ad2nUFCT8MQkWyWHTcfNalkauMYslfmmJHJc7qMEjhOZjdR0JafUfsey2+CVrHxMF8rnbNIO9uRNt7HkNtkFjNN4cwGo17+RSV9SamNskYDHxPsw7ZZAbhjugcNjteymwyCCqajbghbn2amyNfQSB7GvtbMAbdCdweyuAUVFfBeheL4FJJTagsUPhRrSgMWPhSKYarIVJ8ZX0pQVRVjOe6hNWKtEAKyQKh7VD7yqpJ0oSVhavC1Vslup5kkiuj01dLKwAtAHlySbj2CRtIToQC0/MLRtljijsNzrZKuLaoPoZCWnUH4WWqwA1BItIWYXvFN2XgCuaUT2u1zfBD40BYaoJ7batUKus0F23IVmL2c6mc1O4+iqwW02VhlfZ31TBrv4Zwvz/AGQcewUoKdz4DIDYA6hVtZsbrZwR/Bb2WLtEfjOWmqIgXMN/5VpuE2gOcOoCzkzLZOyf8NPIkt1CxdoH+5HYLc2e3+2PcoOvjaJHA9Sq2saicZpz7ZyphiIWW/UrUZoFNoCtYyyhkKuCgpqjH/8ACv7FaLhh94IOzfos9jf+Ff2KecHm9PAfIfRFUvIUJW84TyspR7dj+YFvqg+IoblpTmYXcEs4hBsABdWt83uQ5uF5Oy0Y7BZWoYPvDOxWrqJQIwDvoFmqikd7Vr+SWrbc1IeZXcWgfd3jqw/RcEjH4q/QPEMV4D/pK/P+a0xHn+qqpneKseN1b3BmB1LUZnZBlFjrq4G4Atz29UjwihdLJvoNynPD9UWxyaZvDtYHfS+qc0VIPuzHNaGvPtQ7wtBJa5+ptz935KbgC4jpKmyQwO4Ewi8OxCBgymRtxoba272T+gxKBxs2RpPS9j6HVZqesexrGRsA6vLbgHlYAKyzjB7aRgBuWgtaW6G+Vx111DR5ZjvZU5AiXO0lbpobvyQNTjkIORueR3SNjn27kaIfB5hJAAQT5O1NrXseR5IaZkzZQWMDg0+5s0t35G4N07RdVO4ol9aH6GOQA8yw2162vZZNsZo6t2hkgkALiASGG/hDnDQG+19d7LW0xluc3u8r+953GyFpaTxT5m5nSOhve9iwDYctLHS3cpgGyeiTiYHUhaDD5mPja6Nwc03sRr8ETZZHg1xFTVNbYQhzGho2Egb4yOmuh7LahoUQC66g6GGFQQvgFc5oVJKYghIGVIJXjj7MKZF6CxCIObZMdE4XI6up/EPdfOqU/mwlvtD3VpwJqjZWwVnGTKTpCtB/Y7QoOw1qjITwUmjcVYZE0/s9QdQJSEoK3FPGZWhxIBBIPbZNcZgYaR8Ys78Mj5JdT4c6KJ7gcwOtunZfS6QFxPI3F1o5rzyQbqYf5dFxh4LDY7KD2grQYtTMtcELNPdY2C2cDjxXGVwuFh4/Z3gHOw7v0TmgZ/CSdz+iXs2R2DOzU8ocbAX+iXgmwsjaBEnugMUJDTPBbLEoSDEf8p/RMMEdaVqBxqYNbCSbf0FHD8UjD2nMN1ze0CfGaenqV0+BA8JwHM/QJ7jbfxO4QTVbjuIx5gcw2S0YpF+cIGqN8o6kdwI9ehL/AO1ovzBenF4+RuqjZWIvFv8ADvHkfom3ArL00XlZI3ye1icG3Oi0HA8mWmYLHS49CUVRO4UJXG8Foa2oLJGDk7T4qGJ1jWgXG9kYWCWxI22SniOKwbb8zfqpEyqmwF5UMLi3TTdUVzLWTWFl2jskvEEZ015qTXRZItleYtrF8CvzxWttUO/1H6r9C14/B+C4BjTbVT/9R+qqZ51a/wAi1/CWsgb+YEa+Y0TvCI3sY5jxdweT66XHxas9wtMGyscSAAdSTYDTclaKSqidO8xTMlBB0DgTc3dYdbZXeqsrjQqWFcLtPu7rR0LRbVuvqoYy0uYMwsxrgdeZ1AH/AErsDqGuaOqlxGxsgjaHkFrswAsQTYjX1KGmEWWgmCvuG3n2ZdqMxJ7XKbxX5WJSvBI5Wg5iyztstxb4G/1TIsva2hHz8k4Kg9olWllxruoPhGTfLrcnmdrD1UxU3HmND5JBjOPexqI4yQR7POWAeIkyWzXO4DWO0GqkLqp1kdgVCY2vv7znZ3d3kuI9SU4jJ6quCRrhnabhwaQfLX916x3jKTSQYVb73Xz8wO69AKun5KoJnTKduikyO6qqmEBFRKFWNE3BLisPiOkinHOoY376ojcqCilbUVCFkkKuIuV7M0JG6QV0R0Xj1CJ2ikSmSWtwHFS/R7QR0QnHkbRTkt/DDre7pzGiG4f0sjuP4L0MhHLX5ha4ZmIHOyys/hku5Li1Sy7rBxPxVjYA0Xcvo2gbqFUHuGgsCtxtOlhqcaDmsE1a2LqyBPom+FgGlmI8/olsWynh1UYoHxkXLr691WySwHmrMK8ObnHFV46nlcGcQtDi9pmwtOwv+ijT4FF0VrHAsj7fsmEBCwtq7tVoHL1K39lHNRLjxPoFQ3CIyNdVYzBIvyo1rgr2LKJWtAQIweL8oVzMLjH8oRQUwmTq6BgjY4NA2KZ8ANvBY/mf/wAilbDoexTP7Pn3jeP/ALJPmboml5ShK2q0Va4tbcclRUsEsQPxR1THdpS7DA5sZzDQE+ilCqlW0p8ISXiQHLdOopBa6VYxKHjKNSlCkEHO68XwXCOJI/4twHN31Xd5ISIvguWiiH3x8zxo12Vv+o+874D6+SHdU8KXEafPoiqVE1iGN4/LqgqPh6abQ/hRi1iRcvPW3TpdaCCkp4HRgzBsjAWhugzZ9LOAG+nXunMFRmIDbH9EXNSMcPG1rxv4mh3osr+pVXvl0hvIR66rV/p1JjYaAXczPygiFdgMrPaM2IeP/wBBL8Yp3NqQ8Pd7M5g+MWuCB4SwnbXcHTVI5sXp6Z7BFJ4S8jKc3gdYu/m1DdxblcLSwzNn8QPn+602mRMR3shCIdMz2KcYY2CwImk5i1hfy0svnRe1eAx0jWNN3OJILzyb5C979gpUNINCAR2JTENDR0AVk2VT3AE3J7/wvJLBfn/jbiAz4hJJG67GFsbPMR7n4uLiCOoXVPtDxSSOkL4zlBewOI3LCfEB0B2+JXCqmQOke5rNCSQL6gJ2FDvGi/QXAmKNqae4N3D3haxB3II76/FaHN4guE8EYtLTyB8ZOtszdw4dCF25soka2Rul7Ejv+qYESmc0/FMJtgoNKm73VBhVWIa5x3TCei4AXREQUaoKyFVVhVg0UDqsJxCLPQGdMeIx4ksKoOqLGgU/aqbSSotarQ6ySS9a0qQU2C6+eFX4gzZVLIYlPsHjsAmfGB/gpRzLTbvZLMFkJIB5J7jjGmneTqA0/RbYdlIPJY725pHNcUpMNLxc6dArcToSGCxsnMJsNj6IXFZbs90+hQ+IxNSu6XacuSJw+GpUGZWfHiUtoWARSZhc2/QoSEeFENqQGOB0uP3QkLvCuj2f/gaua2peuQnED7tY1OKaHzSakIDWnmnFPU+Syts/5W9vVamwjNB0+0UU2m80bGLBBNqfJWMq/JY11uBGKYQ7JSeStjfdMpKU77McfIq/7Pqo5HAc3uQ1b/du7KH2b+47XZx/RE0fKUJX1C1mL1crBaMZiVZh2In2ZY4eLmrsSqWsGYi6QcO4g2aomB0y5bN563RLQ2NEIZTeJ+h+KxuKY/7KcstyBWuOhcBsCbLCYzhjn1ea2hsPmk078dFONxazBpnTRlztByWFr/FO+1gxrnDysNz/AF0W9H4UJtyaT6BYakoyQTsTqTvre6ydq1RAaFt7Fpxneeg9fQKcURBuy4cRewOlhyB23H9bFpSVGYG+hGhHQ90LTxODum97e6b7W6FXVEwYLm2pAAJtcnQC6xBNRwY0STotao4NBLtAs7x/hcTmNe5pa/MBnaNDobZvPofgguD4Ki/4c0TurJHFru7dPpdVcUTuBkZ7f2xJGZgaQAW8gb6gW6LPUswcLB3/AI9FvCk+i0NcZ/f7/wCIfC+DjGGN10mDa/u0P1XbKComFmuY0Ecw4n9EbO/S8jg1o1Oth8Sd1xnDsVmiILZXNttZ5t8W7WVVS+WR5dLK+a/uhxO3bZPnsk7Y9UuGUgg8dI7/AGldF44qYX0b2tkY+9rAOB2N9B8FyWmw17Sbi7TceYB6pvC/XQmw19FYPkE7alkfT2BRg53k21sAOPWVRw7LlLRo17HAC9rOtcG3LkNCu34BXCeMNdZkoGw0DvMDquLtpM3iO1idCCOVrEaFOeEMfc2YRSyWbf8ADkI2sdA8bjoSPTVRbM205LnK7GiWzMaH7LshbYWKoYvJsUa33xuL3Gx8wf6uh6fFoH6teOx3+SvddCspVIkNJHS6bRKirUqaZrvdcHdiCq6tMVCLwsbxINUoTniPdKGKg6opui8Ll4XqwtUfZKKmAOKkJ7Lz25K+9mvgwKMCZTzwWmwp/iHzWpqyPYkkX0WYwqEAtK09ULwOHkVsHRZBjMsozFWf/GArhXQuGVzRZZeQOvYAquGoBf7MG7unNKZsnygcUxq+EqaXMY3ZXHbXS/ZYnFcGkp3ZJB2cNitnHNY9CmdVgja2C2bK9ux8+3MIzCYvwzkd5UFjMJ4jc7fN9VgqAiwTmGIdULPgc0Fs7dAdxqEZBfoq9sPDqjC0zb1VmxGObSeHCN70CvEbeqtZG3qhxASiIInBYsrcAV7bdVdGyyryEq8Jk8L2Vt2kIXgSE5pG7Wd+l0fALkBW8HRhtRO3o5vzaiaGhQmIMQmuLVoL2wcyLnssrS0RhqpJAdHWH0WpmpW/e855sy/O6C4ngyjM3qETT8x7IV+nvTOKxAPVLeICGtzAahMKVh9mDtosVxjxfC0eyiImkNwS0+BnncbnyCqc8B8omhQfV3Wi6jV8SsMZjMjQSLHxDS5sb9OalRVsGgEsZ/8ANv7rl9NfNblp8tUya2yza9LxHSSuxwOzW+HlBPyXR3kNc3m19xf/ADWuPUA+ix3GGIZpBC0nTcjk46/t80JT1T2bOLedgdL9bbIGSTLUtkuXe0Lr3Ol+f1SwWEDK+aeFu/H5IbbGCq0sNnaQ4AiecTa3eJQ1NSkMBLi4k2DjuNxr15qElK1x8Xgfye3RHEkSSMP8puPMKBaiK8ioUZsmnSqYGmQ0de4Nz36oeAuBySeP8rtsw/dESRlzcu1t+3P915brqmnD9MX1EcYNs5DSbXsCHAnzsLn4KoXPdaDh4dNxdcAE31gCYn15IFrcota3X9lGb3T2TLHmWnlAIcLkhw2IcbgjpoRolsu1kjZENcHUsw0In4qxlc8gQRtu6waDyAOunUoqopI6Z7GyOOZ98xPMkWN+lxp6K3g6Nv3ll9dXEebt0BxbUGare9tiAQ1ttQRYX7+In0UQbjkuLxdJ1OqQRfUdl0jBcZa2JsUpvc+BxOluQcel+fIlIuKMrPHE8h4PjDcu22Yt5HkSND62yEFS4xhhBAaTbc2B5A72Xhmdci5N9wdR37q0OMQtDZ+DlrazTqbhPqfi6pblOZt28y33ul/+rbroWBcTR1JEZ0kyNNu4Fx3FwuQAIrD618MgkYbH+tE4qE+ZaeM2XSrsgCHcD/1dE4l3SaMojHa0lsbnWzOa1xttcgE28tUqjq1EtK46Mpg8EzBX10E2pUzOVCE8oklRuhfvBUTUFMpLY4TK0m1yD0WrMgER56LEtp8urC66PopajJZ7x6WWyWwN4gLInOdwE9gl9RLK2/4W1+YWJjhqTWCobGWtza9tjouklg3JJXrMrRYABDB9GlcEko4UMRVs5oA+ay+KzlkrGBuYyfLurZKqVmjSWEb25ojEgPbxuOtjop4vFchzRfrZVsxTXviIlX1MA5lOZk9lVR1r5HZZDmBB3QQbZxHQkKymFng+ahXWbIfVTxAkAoahAKJYrWlBsnHn6K4S+R9EGUaEQCpqgS+RUva+RTJJhhou9v8AXIr3CGEV8wG1mE/7VTg7yZBfQaorBJQK+fW/hj+QKLoHdKBxFnLQYlhgeQQSCOYQddh5czKTchM6mrsNEjrsXA56q7Jxchsx0CyX2n4m9lMyFpLQ8kPtpma0Dw9rkei5jFDcX2cW6eV1rvtMrczo282sv/vP7NWLpqv3euxQ9QyZC6rZoZTosD/1Cfmr8OcWvLD0CaEIGnAu53NyOYqDBK6XBty08vUx2UY3KdPRRyVMXtC7Lq0NBA8W/ToPkvMq+klygOG7XNf/ALTr8rqykS0iVXtKh42EezpI7i4Wz4vwCKKFssTMpsLm7iSCCNST1ssM7QrrFQz7xQu5nKQO5FwfVcqkbe/cqeLbvA/uyx//ADVXNhn0uLXT7j/BULoimeQQRuCDfpbzQ4U4yhgV0Yur53HMb3JBO+p11Fz8UNbcoqs96/5mtPqP+kK86JO1TC7B09EOM2XwktPUGx81ZiMHszFlOpBN+uW1++pUaTYIump3VBGYbXDQP5epHQqEwFzm1aU5KnMR8L+qvkF4fds422GhABsR+yXQ9U8wzHWPjkgkAa5gcGSbZsuo+OnzSZo8Tk7bWKK2Q4OZA4H63Vl145SVZOqcrcOi0RcX5QT/AHdmHsWgt+Yf8kxpqVvNI8HmvMWn/wBzN6+835gD4rQtapOcZlcZtXDilX/2APv0Kt9i1Qc1q8LSgqu4USs5HBjV4Y2qiBpIVhYkktN98039FZBMSNAShoPZNGtz8Ea2tbbwhPPtFHkAWY30U4qd530U5Kdo53VDZ3OO9uyYU8LQLnVMC02aPioPLm3cfcFncVIMsYa089U2w9vi2urKp7MzbDnui4XhpvZMG3kp3VdyISbE8KN87dNdljMbxKUVBZHHnsBqukYnU3abLDxYpGyQuIueyKpvBkG6z8QwwHCxKTS4tUs0dEB3KkeIai39231TDGMbilcLC1vJRpXxnf6KRAmwCpBMalK3cQ1H5WhTbi9U7ZrU8MUBF7fJRZOwaAKJMcApDuUthrqo6GzfMI7gyVzJ5C8lxNtTuj6aYHkhcAqQKyQEaWCspE8lXVAIWjqa179GggdbIT7jzIJKdf2lGP5VdR1bXu93QaqJBJkmU0gCAFw7iqq9pUSno8tHZnhH0SCGPU26FFYjIMznH+ZxPqbq6jitrzKrc6BK61tHO9rPZA+iGw+r1yn4J5AUmxCj/nbp1H6ozCKnO3XcaFQkeYI7AvfTqeBU9x5hMWqiqlDGuc7Zu/PTbZXFREzWBz3tzta25bpqBy1SIOgWpWdFNxHAE/ALc8BY7E+D2T5GtfltlcQ03G26yGMwhlRKxpvlJdbyddw+GvyWmw8h8TCWCMPAIZobA6i5Ate3RW1OBwTEuMYaQAA9tmutys4a9dCh6u0Q/dc2I5EHp0XI7OYcHVNVpzNfqIjjaPvbVYNwUgnWL4CY2l7Xe0YNDewcCN+4v0SMJ2VA8S0rqqdRrxmbojKt4LWW3yWPwJ/dBybWVxHhHkSPUX/RDSmwU3J4ytI7qFMdE1wOpka9wjaLgHV2ttDcgBKoRoi6XEhTvbJa4N2H08J9fqoxNll7QpF2Fn2b+hRWBYdFI50ZNnXNr6HQa692j5oRzbEj/UPQqWCuEkwbs97iWnoC48+oUquEseWncfrr+qQmUJsRwzvb09fuqQVXB1Xk77Dvp6qyNtgnGq6CZdHJWQylr2uG4II7g3C22cHUbEBw7OFx8isG5avCajNA082ks+G7fk63wThYu3KWakKg/SfkfumPtEDWtzbK26lGFLS65Y3XkFwF6XlFRsFlRKBdVioC4hTLCGyv/9k=")
                .into(profileImage)
            val params = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            params.setMargins(0, 5, 0, 5)
            view.layoutParams = params
            view.id = i
            hashMap[i] = if (i == 1)    "097 33 55 335" else "012 34 56 78"
            view.setOnClickListener {
                alertPhoneCall(this, hashMap[view.id].toString())
            }
            mBind.assignEmployee.addView(view)
        }
    }

    private fun initWorkingRecyclerView(){
        val recyclerView = mBind.workingRyclerView.recyclerView
        val list = ArrayList<ServiceWorkingDayAdapter.WorkingDayModel>()
        for (i in 1..20){
            list.add(ServiceWorkingDayAdapter.WorkingDayModel("Monday", "11:00 - 12:00"))
        }

        recyclerView.layoutManager = AbsoluteFitLayoutManager(this, 3, RecyclerView.HORIZONTAL, false, 4)
        recyclerView.adapter = ServiceWorkingDayAdapter(this, list, object : ServiceWorkingDayAdapter.OnClickCallBackListener {
            override fun onClickBack(data: ServiceWorkingDayAdapter.WorkingDayModel) {

            }
        })
    }

    private fun setValueData() {
        if (action == "request_detail"){
            mBind.btn.text = resources.getString(R.string.submit)
            mBind.btn.setBackgroundColor(Utils.getColor(this, R.color.appBarColorOld))
        }  else {
            mBind.btn.text = resources.getString(R.string.cancel)
            mBind.btn.setBackgroundColor(Utils.getColor(this, R.color.red))
        }

        if (serviceModel == null) {
            serviceModel = PropertyServiceListAdapter.addListRegService()[0]
        }
        mBind.unitNoTv.text = serviceModel?.unit_no
        mBind.nameTv.text = serviceModel?.username
        mBind.phoneTv.text = serviceModel?.phone
        mBind.serviceNameTv.text = serviceModel?.service_name
        mBind.categoryServiceTv.text = serviceModel?.service_type
        mBind.taxTv.text = serviceModel?.tax
        mBind.priceTv.text = serviceModel?.price
        mBind.statusTv.text = serviceModel?.status

        mBind.qtyTv.text = "1"
        mBind.totalTaxTv.text = serviceModel?.total_tax_amount
        mBind.totalPriceTv.text = serviceModel?.total_price_amount

        if (serviceModel != null) {
            if (!serviceModel!!.service_type.equals("time", true)){
                mBind.workingDateLayout.visibility = View.GONE
                initWorkingRecyclerView()
            }
        }
    }

}