package com.eazy.daikou.ui.home.parking

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.*
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.base.MockUpData
import com.eazy.daikou.ui.home.parking.adapter.PaymentAdapter
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment


class SelectPaymentMethodBottomSheetFragment : BottomSheetDialogFragment() {

    private var onClickAllAction :  OnClickFeedBack?=null
    private var state : Int = -1

    fun newInstance(type : String,state:Int) : SelectPaymentMethodBottomSheetFragment {
        val args = Bundle()
        val fragment = SelectPaymentMethodBottomSheetFragment()
        args.putString("type",type)
        args.putInt("stat",state)
        fragment.arguments = args
        return fragment
    }

    @SuppressLint("UseRequireInsteadOfGet")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root =  inflater.inflate(R.layout.layout_payment_dialog_fragment, container, false)
        dialog!!.window!!.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
        return root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, R.style.AppBottomSheetDialogTheme)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        if (dialog is BottomSheetDialog) {
            dialog.behavior.skipCollapsed = true
            dialog.behavior.state = BottomSheetBehavior.STATE_EXPANDED
        }
        return dialog
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if(requireArguments().containsKey("stat")){
            state = requireArguments().getInt("stat")
        }
        val btnBack : TextView = view.findViewById(R.id.btn_back)
        btnBack.setOnClickListener {
            dismiss()
        }

        val recyclerView : RecyclerView = view.findViewById(R.id.list_payment)
        val linearLayoutManager = LinearLayoutManager(requireContext())
        recyclerView.layoutManager = linearLayoutManager
        val adapter = PaymentAdapter(requireContext(),state, MockUpData.listPaymentMethod(), itemClick)
        recyclerView.adapter = adapter
    }
    private val itemClick  = object : PaymentAdapter.ItemClickPaymentMethod{
        override fun onClick(service: String,image:Int, stat: Int) {
            onClickAllAction!!.onClickSubmit(service,image,stat)
            dismiss()
        }

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        val parent = parentFragment
        onClickAllAction = if (parent != null) {
            parent as OnClickFeedBack
        } else {
            context as OnClickFeedBack
        }
    }

    interface OnClickFeedBack {
        fun onClickSubmit(txtChoose: String,image:Int, position: Int)
    }
}