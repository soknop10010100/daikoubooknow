package com.eazy.daikou.ui.home.book_now.booking_hotel.visit

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.request_data.request.book_now_ws.BookingVisitWS
import com.eazy.daikou.helper.GetDataUtils
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.booking_hotel.HotelVisitDetailModel
import com.eazy.daikou.model.booking_hotel.LocationModel
import com.eazy.daikou.model.home.SubItemHomeModel
import com.eazy.daikou.ui.home.book_now.booking_hotel.adapter.BookingHomeItemAdapter
import com.eazy.daikou.ui.home.book_now.booking_hotel.adapter.HotelVisitItemAdapter
import com.eazy.daikou.ui.home.book_now.booking_hotel.adapter.VisitSelectLocationAdapter

class HotelVisitByLocationActivity : BaseActivity() {

    private var subItemHomeModelList : ArrayList<SubItemHomeModel> = ArrayList()
    private lateinit var progressBar: ProgressBar
    private lateinit var recyclerView: RecyclerView
    private lateinit var gridLayoutManager: GridLayoutManager
    private lateinit var refreshLayout: SwipeRefreshLayout
    private var id = ""
    private var name = ""
    private var action = ""
    private var locationModelList: ArrayList<LocationModel> = ArrayList()
    private lateinit var bookingHomeItemAdapter : HotelVisitItemAdapter
    private lateinit var searchLocation : ImageView
    private var isSearchLocation = false
    private var locationId = ""
    private lateinit var titleToolbar: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hotel_visit_by_location)

        initView()

        initData()

        initAction()
    }

    private fun initView(){
        progressBar = findViewById(R.id.progressItem)
        recyclerView = findViewById(R.id.recyclerView)
        refreshLayout = findViewById(R.id.swipe_layouts)
        titleToolbar = findViewById(R.id.titleToolbar)
        findViewById<LinearLayout>(R.id.layoutMap).visibility = View.VISIBLE
        searchLocation = findViewById(R.id.addressMap)
        searchLocation.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_baseline_location_on_24))
    }

    private fun initData(){
        id = GetDataUtils.getDataFromString("id", this)
        name = GetDataUtils.getDataFromString("name", this)
        action = GetDataUtils.getDataFromString("action", this)

        Utils.customOnToolbar(this, name){finish()}
    }

    private fun initAction(){

        refreshLayout.setColorSchemeResources(R.color.appBarColorOld)
        refreshLayout.setOnRefreshListener { refreshList() }

        initItemRecycle()

        // Search item by location
        searchLocation.setOnClickListener(CustomSetOnClickViewListener{ alertDialogLocation() })

    }

    private fun alertDialogLocation(){
        val alertLayout = LayoutInflater.from(this).inflate(R.layout.fragment_select_location_booking, null)
        val dialog = AlertDialog.Builder(this)
        dialog.setView(alertLayout)
        val alertDialog = dialog.show()

        recyclerView = alertLayout.findViewById(R.id.recyclerView)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = VisitSelectLocationAdapter(locationId, locationModelList, object :
            VisitSelectLocationAdapter.OnClickCallBackLister {
                override fun onClickCallBack(value: LocationModel) {

                    locationId = value.id!!
                    if (action == "menu_category"){
                        titleToolbar.text = String.format("%s %s %s %s", name, "(", value.name, ")")
                    } else {
                        titleToolbar.text = value.name
                    }

                    isSearchLocation = true
                    alertDialog.dismiss()

                    refreshList()
                }
            })
    }

    private fun refreshList(){
        bookingHomeItemAdapter.clear()

        requestDataWs()
    }


    private fun initItemRecycle(){
        gridLayoutManager = GridLayoutManager(this, 2, RecyclerView.VERTICAL, false)
        recyclerView.layoutManager = gridLayoutManager

        bookingHomeItemAdapter = HotelVisitItemAdapter(this, "visit_by_location", subItemHomeModelList, onClickSubItemBookingListener)
        recyclerView.adapter = bookingHomeItemAdapter

        requestDataWs()

    }

    private fun requestDataWs(){
        progressBar.visibility = View.VISIBLE
        if (action == "menu_category"){
            BookingVisitWS().getLocationDetail(this, if (isSearchLocation) "search_item_by_menu" else action, locationId, id, onCallBackListener)
        } else {
            BookingVisitWS().getLocationDetail(this, action, locationId, if (isSearchLocation) locationId else id, onCallBackListener)
        }
    }

    private val onCallBackListener = object : BookingVisitWS.OnCallBackDetailLocationListener{
        @SuppressLint("NotifyDataSetChanged")
        override fun onSuccess(subHomeModelCategoryList: ArrayList<SubItemHomeModel>, locationModel: ArrayList<LocationModel>) {
            progressBar.visibility = View.GONE
            refreshLayout.isRefreshing = false
            if (locationModelList.size == 0)    locationModelList.addAll(locationModel)
            subItemHomeModelList.addAll(subHomeModelCategoryList)

            bookingHomeItemAdapter.notifyDataSetChanged()

            Utils.validateViewNoItemFound(this@HotelVisitByLocationActivity, recyclerView, subHomeModelCategoryList.size <= 0)
        }

        override fun onSuccessDetail(hotelVisitDetailModel: HotelVisitDetailModel, nearByList: ArrayList<SubItemHomeModel>) {}

        override fun onFailed(message: String) {
            progressBar.visibility = View.GONE
            refreshLayout.isRefreshing = false
            Utils.customToastMsgError(this@HotelVisitByLocationActivity, message, false)
        }

    }

    private var onClickSubItemBookingListener = object : BookingHomeItemAdapter.PropertyClick {
        override fun onBookingClick(propertyModel: SubItemHomeModel?) {
            val intent = Intent(this@HotelVisitByLocationActivity, HotelVisitDetailActivity::class.java).apply {
                putExtra("id", propertyModel!!.id)
                putExtra("action", action)
            }
            startActivity(intent)
        }

        override fun onBookingWishlistClick(propertyModel: SubItemHomeModel?) {}
    }
}