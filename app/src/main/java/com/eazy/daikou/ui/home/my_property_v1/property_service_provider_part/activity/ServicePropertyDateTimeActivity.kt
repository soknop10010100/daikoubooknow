package com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.activity

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.*
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RequiresApi
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.request_data.request.service_property_ws_v2.ServiceProviderPropertyWs
import com.eazy.daikou.helper.*
import com.eazy.daikou.helper.DateUtil.milliseconds
import com.eazy.daikou.model.facility_booking.FeeTypeModel
import com.eazy.daikou.model.my_property.cleaning.PropertyServiceTypeModel
import com.eazy.daikou.model.my_property.cleaning.ServiceProviderDetailModel
import com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.adapter.AvailableDayCleaningAdapter
import com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.cleaning_v2.ServiceCleaningPaymentAndBookingActivity
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class ServicePropertyDateTimeActivity : BaseActivity() {

    private lateinit var recycleViewDay : RecyclerView
    private lateinit var propertyServiceModel : PropertyServiceTypeModel
    private lateinit var btnNext : TextView
    private lateinit var serviceTermAdapter : AvailableDayCleaningAdapter
    private lateinit var startDateCalendar : CalendarView
    private lateinit var dateStartTv : TextView
    private var listDay : ArrayList<FeeTypeModel> = ArrayList()
    private lateinit var progressBar: ProgressBar
    private lateinit var dateFormatForDay : SimpleDateFormat
    private lateinit var simpleFormat : SimpleDateFormat
    private lateinit var lastSelectedCalendar: Calendar
    private var isSelectAvailableDay : Boolean = false
    private lateinit var quantityWeekLayout : LinearLayout
    private lateinit var numberWeek : EditText
    private lateinit var availableTimeTv : TextView
    private lateinit var startEndTimeTv : TextView
    private lateinit var numberRoomLayout : LinearLayout
    private lateinit var labelQty : TextView

    private var dayVal : String = ""
    private var serviceCategory = ""
    private var unitId = ""
    private var serviceTerm = ""
    private var startDate = ""
    private var dayWork = ""
    private var availableTime = ""

    //From detail reschedule
    private  var serviceDetailModel: ServiceProviderDetailModel = ServiceProviderDetailModel()
    private var action = ""
    private var periodDuration = 0
    private val availableDay :  ArrayList<String> = ArrayList()
    private val availableTimeList :  ArrayList<String> = ArrayList()
    private var idRegister = ""

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_service_cleaning_booking)

        initView()

        initData()

        initAction()

    }

    private fun initView() {
        startDateCalendar = findViewById(R.id.select_start_date)
        recycleViewDay = findViewById(R.id.list_day)
        btnNext = findViewById(R.id.btn_next)
        dateStartTv = findViewById(R.id.date_start)
        progressBar = findViewById(R.id.progressItem)
        progressBar.visibility = View.GONE
        quantityWeekLayout = findViewById(R.id.quantityWeekLayout)
        numberWeek = findViewById(R.id.numberWeek)
        availableTimeTv = findViewById(R.id.availableTimeTv)
        startEndTimeTv = findViewById(R.id.startEndTimeTv)
        numberRoomLayout = findViewById(R.id.numberRoomLayout)
        labelQty = findViewById(R.id.labelQty)
    }

    @SuppressLint("SimpleDateFormat", "SetTextI18n")
    private fun initData() {
        if (intent != null && intent.hasExtra("service_type_model")) {
            propertyServiceModel = intent.getSerializableExtra("service_type_model") as PropertyServiceTypeModel
            action = "create_service"
        }
        serviceTerm = GetDataUtils.getDataFromString("service_term", this)
        serviceCategory = GetDataUtils.getDataFromString("service_category", this)
        unitId = GetDataUtils.getDataFromString("unit_id", this)

        //from reschedule service
        if (intent != null && intent.hasExtra("service_detail_model")) {
            serviceDetailModel = intent.getSerializableExtra("service_detail_model") as ServiceProviderDetailModel
            action = "reschedule_service"
        }

        // Calendar
        dateFormatForDay = SimpleDateFormat("EEE", Locale.getDefault())
        simpleFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        lastSelectedCalendar = Calendar.getInstance()

        var titleToolbar = ""
        if (action == "create_service"){
            if (propertyServiceModel.sv_name != null) {
                titleToolbar = propertyServiceModel.sv_name!!
            }
            // set available time
            if (propertyServiceModel.sv_start_time != null && propertyServiceModel.sv_end_time != null){
                startEndTimeTv.text = String.format("%s - %s",
                    Utils.formatDateTime(propertyServiceModel.sv_start_time,"hh:mm:ss","h : mm"),
                    Utils.formatDateTime(propertyServiceModel.sv_end_time,"hh:mm:ss", "h : mm"))
            }
            periodDuration = if (propertyServiceModel.sv_period != null) propertyServiceModel.sv_period!!.toInt() else 0

            availableDay.addAll(propertyServiceModel.available_days)
            availableTimeList.addAll(propertyServiceModel.available_start_times)

            //Generate Calendar
            val calendar = Calendar.getInstance()
            startDate = simpleFormat.format(Date())
            dayWork = dateFormatForDay.format(Date())
            dayVal = dateFormatForDay.format(calendar.time)
            dateStartTv.text = startDate
        }
        // action == "reschedule_service"
        else {
            if (serviceDetailModel.registration_info != null){
                if (serviceDetailModel.registration_info!!.id != null) idRegister = serviceDetailModel.registration_info!!.id!!
            }

            if (serviceDetailModel.service_detail != null) {
                titleToolbar = if (serviceDetailModel.service_detail!!.service_name != null) serviceDetailModel.service_detail!!.service_name!! else "- - -"
                availableDay.addAll(serviceDetailModel.service_detail!!.available_days)
                availableTimeList.addAll(serviceDetailModel.service_detail!!.available_start_times)

                serviceTerm = Utils.validateNullValue(serviceDetailModel.service_detail!!.service_term!!)
                serviceCategory = Utils.validateNullValue(serviceDetailModel.service_detail!!.service_category!!)
            }
            // set value
            if (serviceDetailModel.date_and_time != null){
                if (serviceDetailModel.date_and_time!!.start_time != null && serviceDetailModel.date_and_time!!.end_time != null){
                    availableTimeTv.text = String.format("%s - %s",
                        Utils.formatDateTime(serviceDetailModel.date_and_time!!.start_time,"hh:mm:ss","h : mm"),
                        Utils.formatDateTime(serviceDetailModel.date_and_time!!.end_time,"hh:mm:ss", "h : mm"))
                }
                if(serviceDetailModel.date_and_time!!.start_time != null) availableTime = serviceDetailModel.date_and_time!!.start_time!!

                //Generate Calendar
                if (serviceDetailModel.date_and_time!!.start_time != null) {
                    startDate = serviceDetailModel.date_and_time!!.start_date!!
                    dayWork = DateUtil.formatSimpleDateToDayDate(serviceDetailModel.date_and_time!!.start_date, "yyyy-MM-dd", "EEE")
                    dayVal = DateUtil.formatSimpleDateToDayDate(serviceDetailModel.date_and_time!!.start_date, "yyyy-MM-dd", "EEE")
                    dateStartTv.text = startDate
                } else {
                    dateStartTv.text = "- - -"
                }

            }
            periodDuration = if (serviceDetailModel.date_and_time?.sv_period != null) serviceDetailModel.date_and_time!!.sv_period!!.toInt() else 0

            startEndTimeTv.text = String.format("%s - %s", availableTimeList[0], availableTimeList[availableTimeList.size - 1])

            btnNext.text = resources.getString(R.string.reschedule)
            numberWeek.isEnabled = false
            if (serviceDetailModel.booking_summary != null)
            numberWeek.setText(if (serviceDetailModel.booking_summary!!.qty != null) serviceDetailModel.booking_summary!!.qty else "- - -")
            numberWeek.setTextColor(Utils.getColor(this, R.color.gray))
        }

        Utils.customOnToolbar(this, titleToolbar){finish()}
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun initAction() {
        // Available day
        recycleViewDay.layoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
        initListAvailableDay()
        if (serviceTerm == "time") {
            initRecycleDay()
            recycleViewDay.visibility = View.VISIBLE
            quantityWeekLayout.visibility = View.GONE
        } else {
            if (serviceTerm != "weekly") {
                labelQty.text = resources.getString(R.string.month)
                numberWeek.hint = "Enter number of month"
            }
            initRecycleDay()
            quantityWeekLayout.visibility = View.VISIBLE
        }

        startDateCalendar.setOnDateChangeListener { _, year, month, day ->
            val date = year.toString() + "-" + (month + 1).toString() + "-" + day.toString()
            val checkCalendar = Calendar.getInstance()
            checkCalendar[year, month] = day

            val cal = Calendar.getInstance()
            cal[year, month] = day

            if (serviceTerm != "time") {
                if (numberWeek.text.isEmpty()) {
                    if (serviceTerm != "weekly") {
                        Utils.customToastMsgError(this, "Please Enter number of month", false)
                    } else {
                        Utils.customToastMsgError(this, "Please Enter number of week", false)
                    }
                    startDateCalendar.date = lastSelectedCalendar.timeInMillis
                    return@setOnDateChangeListener
                }
            }
            val backUpDayVal = dayVal
            dayVal = dateFormatForDay.format(cal.time)

            if (checkCalendar == lastSelectedCalendar)
                return@setOnDateChangeListener

            if (validateAvailableDate()){
                lastSelectedCalendar = checkCalendar
                startDate = date
                dateStartTv.text = simpleFormat.format(cal.time)
                dayVal = dateFormatForDay.format(cal.time)
                isSelectAvailableDay = true
            } else {
                startDateCalendar.date = lastSelectedCalendar.timeInMillis
                Utils.customToastMsgError(this@ServicePropertyDateTimeActivity, dayVal + " " + resources.getString(R.string.is_not_available_day), false)
                dayVal = backUpDayVal
            }
        }

        startDateCalendar.minDate = System.currentTimeMillis() - 1000
        if (action != "create_service"){
            if (serviceDetailModel.date_and_time != null)
                if (serviceDetailModel.date_and_time!!.start_time != null)
                    startDateCalendar.setDate (milliseconds(serviceDetailModel.date_and_time!!.start_date), true, true)
        }

        numberWeek.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun afterTextChanged(editable: Editable) {
                if (editable.toString().isNotEmpty()) {
                    numberRoomLayout.background = ResourcesCompat.getDrawable(resources, R.drawable.shape_transparent_bg, null)
                } else {
                   numberRoomLayout.background = ResourcesCompat.getDrawable(resources, R.drawable.shape_error, null)
                }
            }
        })

        initActionClick()
    }

    private fun initAvailableTime(){
        val dateNumPickerFragment = DateNumPickerFragment.newInstance("select_available_time",  availableTimeList)
        dateNumPickerFragment.CallBackListener { _: String, values: String, _: String? ->
            availableTime = values
            calculateEndTime()
        }
        dateNumPickerFragment.show(supportFragmentManager, "Dialog Fragment")
    }

    private fun calculateEndTime(){
        val startTime = availableTime.split(":").toTypedArray()
        val hours = startTime[0].replace(" ", "")
        val min = startTime[1].replace(" ", "")

        val time = calculateHour(hours.toInt(), min.toInt())
        val endVal = time + periodDuration
        availableTimeTv.text = String.format("%s - %s", availableTime, DateUtil.convertMinutesToHours(endVal.toString()))

        availableTimeTv.setBackgroundResource(R.drawable.shape_transparent_bg)
    }

    private fun calculateHour(hour : Int, min : Int) : Int {
        return (hour * 60) + min
    }

    private fun validateAvailableDate() : Boolean{
        if (serviceTerm == "time") {
            if (dayVal == dayWork){
                return true
            }
        } else {
            for (day in listDay){
                if (dayVal == day.freeTypeName && day.isClick){
                    return true
                }
            }
        }
        return false
    }

    private fun initRecycleDay() {
        serviceTermAdapter = AvailableDayCleaningAdapter(this, listDay, serviceTerm, itemClickDayListener)
        recycleViewDay.adapter = serviceTermAdapter
    }

    private val itemClickDayListener = object : AvailableDayCleaningAdapter.ItemClickOnService{
        @SuppressLint("NotifyDataSetChanged")
        override fun onClickItem(listString: FeeTypeModel) {
            if (serviceTerm == "time") {
                dayWork = listString.freeTypeName
                isSelectAvailableDay = true

                for (item in listDay){
                    item.isClick = item.id == listString.id
                }
                serviceTermAdapter.notifyDataSetChanged()
            } else {
                dayWork = listString.freeTypeName
                isSelectAvailableDay = true

                listString.isClick = !listString.isClick
                serviceTermAdapter.notifyDataSetChanged()
            }
        }

    }

    private fun initListAvailableDay(){
        listDay = ArrayList()
        val dayList :  ArrayList<String> = ArrayList()
        dayList.addAll(availableDay)

        if (serviceTerm == "time") {
            var i =0
            for(item in dayList){
                i++
                listDay.add(FeeTypeModel(i.toString(), Utils.convertFirstCapital(item)))
            }

            for(item in listDay){
                if (dayVal == item.freeTypeName){
                    item.isClick = if (serviceDetailModel.date_and_time != null) isClickDay(item.freeTypeName) else true
                    isSelectAvailableDay = true
                }
            }
            if (!isSelectAvailableDay){
                if (listDay.size > 0) {
                    dayWork = listDay[0].freeTypeName
                    listDay[0].isClick = true
                }
            }
        } else {
            var i =0
            for(item in dayList){
                i++
                val day = FeeTypeModel(i.toString(), Utils.convertFirstCapital(item))
                day.isClick = if (serviceDetailModel.date_and_time != null) isClickDay(item) else true
                listDay.add(day)
            }
        }
    }

    private fun isClickDay(day : String) : Boolean{
        if (serviceDetailModel.date_and_time != null) {
            for (item in serviceDetailModel.date_and_time!!.reg_days) {
                if (item.equals(day.lowercase(), false)) {
                    return true
                }
            }
        }
        return false
    }

    private fun initActionClick(){
        btnNext.setOnClickListener(
            CustomSetOnClickViewListener {
                if (serviceTerm == "time") {
                    when {
                        dayVal == dayWork && isSelectAvailableDay && availableTime != ""-> {
                            if (action == "create_service"){
                                startNextDetailPayment()
                            } else {
                                requestServiceApiReschedule()
                            }
                        }
                        else -> {
                            if (availableTime == ""){
                                Utils.customToastMsgError(
                                    this@ServicePropertyDateTimeActivity,
                                    "Please select available time !",
                                    false
                                )
                                availableTimeTv.setBackgroundResource(R.drawable.shape_error)
                            } else if (!isSelectAvailableDay) {
                                Utils.customToastMsgError(
                                    this@ServicePropertyDateTimeActivity,
                                    "Please select available day before go to next !",
                                    false
                                )
                            } else {
                                Utils.customToastMsgError(
                                    this@ServicePropertyDateTimeActivity,
                                    "Available day is not match with date selected !",
                                    false
                                )
                            }
                        }
                    }
                }

                else {
                    if (numberWeek.text.isEmpty()) {
                        if (serviceTerm != "weekly") {
                            Utils.customToastMsgError(this, "Please Enter number of month", false)
                        } else {
                            Utils.customToastMsgError(this, "Please Enter number of week", false)
                        }
                        startDateCalendar.date = lastSelectedCalendar.timeInMillis
                        numberRoomLayout.setBackgroundResource(R.drawable.shape_error)
                        return@CustomSetOnClickViewListener
                    }

                    for (item in listDay) {
                        if (dayVal == item.freeTypeName && item.isClick && availableTime != "") {
                            if (action == "create_service"){
                                startNextDetailPayment()
                                return@CustomSetOnClickViewListener
                            } else {
                                requestServiceApiReschedule()
                                return@CustomSetOnClickViewListener
                            }
                        }
                    }

                    if (availableTime == ""){
                        Utils.customToastMsgError(
                            this@ServicePropertyDateTimeActivity,
                            "Please select available time !",
                            false
                        )
                        availableTimeTv.setBackgroundResource(R.drawable.shape_error)
                    } else if (!isSelectAvailableDay) {
                        Utils.customToastMsgError(
                            this@ServicePropertyDateTimeActivity,
                            "Please select available day before go to next !",
                            false
                        )
                    } else {
                        Utils.customToastMsgError(
                            this@ServicePropertyDateTimeActivity,
                            "Available day is not match with date selected !",
                            false
                        )
                    }
                }
            }
        )

        availableTimeTv.setOnClickListener(
            CustomSetOnClickViewListener {
                initAvailableTime()
            }
        )
    }

    private fun startNextDetailPayment(){
        val intent = Intent(this, ServiceCleaningPaymentAndBookingActivity::class.java)
        intent.putExtra("unit_id", unitId)
        intent.putExtra("service_id", propertyServiceModel.id)
        intent.putExtra("service_term", serviceTerm)
        intent.putExtra("start_date", startDate)
        intent.putExtra("start_time", availableTime)
        intent.putExtra("quantity", if(numberWeek.text.isEmpty()) "1" else numberWeek.text.toString())
        val listDays : ArrayList<String> = ArrayList()
        for (item in listDay){
            if (item.isClick){
                listDays.add(item.freeTypeName.lowercase())
            }
        }
        intent.putExtra("available_days", listDays)
        resultLauncher.launch(intent)
    }


    private var resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode == Activity.RESULT_OK) {
            val data: Intent? = result.data
            if (data != null) {
                //Set Result CallBack
                val isBackSuccess = data.getBooleanExtra("is_result_success", false)
                if (isBackSuccess)  Utils.setResultBack(this@ServicePropertyDateTimeActivity, "", true)
            }
        }
    }

    private fun requestServiceApiReschedule() {
        progressBar.visibility = View.VISIBLE
        val hashMap : HashMap<String, Any> = HashMap()
        hashMap["service_registration_id"] = idRegister
        hashMap["user_id"] = UserSessionManagement(this).userId
        hashMap["action"] = "reschedule"
        hashMap["start_date"] = startDate
        hashMap["start_time"] = availableTime
        val listDays : ArrayList<String> = ArrayList()
        for (item in listDay){
            if (item.isClick){
                listDays.add(item.freeTypeName.lowercase())
            }
        }
        hashMap["days"] = listDays

        ServiceProviderPropertyWs().rescheduleAndAcceptServiceProviderWs(this, hashMap, callBackRescheduleAndAccept)
    }

    private val callBackRescheduleAndAccept : ServiceProviderPropertyWs.OnCallBackRescheduleAndAccept = object : ServiceProviderPropertyWs.OnCallBackRescheduleAndAccept{
        override fun onLoadSuccessFull(message: String) {
            progressBar.visibility = View.GONE
            Utils.customToastMsgError(this@ServicePropertyDateTimeActivity, message, true)

            Utils.setResultBack(this@ServicePropertyDateTimeActivity, "rescheduled_service", true)
        }

        override fun onLoadFailed(error: String) {
            progressBar.visibility = View.GONE
            Utils.customToastMsgError(this@ServicePropertyDateTimeActivity, error, false)
        }
    }
}