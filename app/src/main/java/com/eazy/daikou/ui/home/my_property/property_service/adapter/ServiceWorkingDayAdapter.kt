package com.eazy.daikou.ui.home.my_property.property_service.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.databinding.ServiceWorkingDayModelBinding
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import kotlin.collections.ArrayList

class ServiceWorkingDayAdapter(private val activity : Activity, private val list : ArrayList<WorkingDayModel>, private val onClickCallBackListener : OnClickCallBackListener) : RecyclerView.Adapter<ServiceWorkingDayAdapter.ItemViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val view = ServiceWorkingDayModelBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        val itemViewParam = LinearLayout.LayoutParams(Utils.getWidth(activity) / 4, LinearLayout.LayoutParams.WRAP_CONTENT)
        view.mainLayout.layoutParams = itemViewParam
        return ItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val item : WorkingDayModel = list[position]
        holder.onBindView(item, onClickCallBackListener)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ItemViewHolder(private val mBind: ServiceWorkingDayModelBinding) : RecyclerView.ViewHolder(mBind.root) {
        fun onBindView(item : WorkingDayModel, onClickCallBackListener : OnClickCallBackListener) {
            mBind.dayTv.text = item.day
            mBind.timeTv.text = item.time

            itemView.setOnClickListener( CustomSetOnClickViewListener {
                onClickCallBackListener.onClickBack(item)
            } )
        }
    }

    interface OnClickCallBackListener{
        fun onClickBack(data : WorkingDayModel)
    }

    data class WorkingDayModel(val day : String, val time : String) : java.io.Serializable {

    }
}