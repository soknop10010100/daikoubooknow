package com.eazy.daikou.ui.home.book_now.booking_hotel.travel_talk

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AbsListView
import android.widget.ProgressBar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseFragment
import com.eazy.daikou.base.MockUpData
import com.eazy.daikou.request_data.request.book_now_ws.BookingHotelWS
import com.eazy.daikou.request_data.request.book_now_ws.RequestHashMapData
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.booking_hotel.HotelTravelTalkModel
import com.eazy.daikou.model.booking_hotel.TravelTalkReplyDetail
import com.eazy.daikou.ui.home.book_now.booking_hotel.adapter.HotelTalkReplyCommentAdapter

class HotelReplyCommentItemFragment(private val mainId : String, private val callBackListener : InitCallBackListener) : BaseFragment() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var replyCommentAdapter : HotelTalkReplyCommentAdapter
    private val travelTalkReplyDetailList : ArrayList<TravelTalkReplyDetail> = ArrayList()

    private var currentItem = 0
    private var total : Int = 0
    private var scrollDown : Int = 0
    private var currentPage : Int = 1
    private var size : Int = 10
    private var isScrolling = false
    private var userId = ""

    private lateinit var progressBar : ProgressBar

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_hotel_reply_comment_item, container, false)

        initView(view)

        initData()

        initAction()

        return view

    }

    private fun initView(view : View){
        recyclerView = view.findViewById(R.id.recyclerView)
        progressBar = view.findViewById(R.id.progressItem)
    }

    private fun initData(){
        userId = MockUpData.getEazyHotelUserId(UserSessionManagement(mActivity))
    }

    private fun initAction(){
        linearLayoutManager = LinearLayoutManager(mActivity)

        recyclerView.layoutManager = linearLayoutManager
        // recyclerView.isNestedScrollingEnabled = false
        replyCommentAdapter = HotelTalkReplyCommentAdapter("main_reply", travelTalkReplyDetailList, onClickItemListener)
        recyclerView.adapter = replyCommentAdapter

        requestServiceWs()

        onScrollInfoRecycle()
    }

    private fun requestServiceWs(){
        progressBar.visibility = View.VISIBLE
        BookingHotelWS().getDetailReplyTravelTalkWs(mActivity, currentPage, 10, userId, mainId, object : BookingHotelWS.OnCallBackTravelTalkListener{
            override fun onSuccessTravelTalkList(travelTalkModelList: ArrayList<HotelTravelTalkModel>) {}

            @SuppressLint("NotifyDataSetChanged")
            override fun onSuccessTravelTalkReplyDetail(travelTalkModelList: TravelTalkReplyDetail) {
                progressBar.visibility = View.GONE
                travelTalkReplyDetailList.addAll(travelTalkModelList.replies)
                replyCommentAdapter.notifyDataSetChanged()

                // Call Back Item
                if (travelTalkModelList.thread != null) {
                    callBackListener.onCallBack(travelTalkModelList.thread!!)
                }
            }

            override fun onCreatePostSuccess(message: String) {}

            override fun onFailed(message: String) {
                progressBar.visibility = View.GONE
                Utils.customToastMsgError(mActivity, message, false)
            }

        })
    }

    private val onClickItemListener = object : HotelTalkReplyCommentAdapter.OnClickItemListener{
        @SuppressLint("NotifyDataSetChanged")
        override fun onClickListener(item: TravelTalkReplyDetail, action: String) {
            if (action == "do_like"){
                var numLike = item.total_likes!!.toInt()
                for (itemModel in travelTalkReplyDetailList) {
                    if (item.id == itemModel.id) {
                        if (item.is_user_liked){
                            itemModel.is_user_liked = false
                            numLike -= 1
                        } else {
                            itemModel.is_user_liked = true
                            numLike += 1
                        }
                        itemModel.total_likes = numLike.toString()
                        break
                    }
                }
                replyCommentAdapter.notifyDataSetChanged()

                val hashMap = RequestHashMapData.requestDataLike(userId, "reply", item.id!!, if (item.is_user_liked) "unlike" else "like")
                RequestHashMapData.requestServiceLikeUnLike(mActivity, progressBar,"do_reply_comment", hashMap, object : RequestHashMapData.Companion.OnSuccessEditStatusListener{
                    override fun onSuccess(msg: String) {}
                })

            } else {
                callBackListener.onDoReplyCallBack(item)
            }
        }

        @SuppressLint("NotifyDataSetChanged")
        override fun onClickSubListener(item: TravelTalkReplyDetail, hotelTalkReplyCommentSubAdapter : HotelTalkReplyCommentAdapter) {
            val hashMap = RequestHashMapData.requestDataLike(userId, "reply", item.id!!, if (item.is_user_liked) "unlike" else "like")
            RequestHashMapData.requestServiceLikeUnLike(mActivity, progressBar,"do_like", hashMap, object : RequestHashMapData.Companion.OnSuccessEditStatusListener{
                override fun onSuccess(msg: String) {}
            })

            var numLike = item.total_likes!!.toInt()
            if (item.is_user_liked){
                item.is_user_liked = false
                numLike -= 1
            } else {
                item.is_user_liked = true
                numLike += 1
            }
            item.total_likes = numLike.toString()
            hotelTalkReplyCommentSubAdapter.notifyDataSetChanged()
            replyCommentAdapter.notifyDataSetChanged()
        }
    }

    private fun onScrollInfoRecycle() {
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true
                }
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                currentItem = linearLayoutManager.childCount
                total = linearLayoutManager.itemCount
                scrollDown = linearLayoutManager.findFirstVisibleItemPosition()
                if (dy > 0) {
                    if (isScrolling && currentItem + scrollDown == total) {
                        if (total == size) {
                            isScrolling = false
                            currentPage++
                            progressBar.visibility = View.VISIBLE
                            recyclerView.scrollToPosition(travelTalkReplyDetailList.size - 1)
                            requestServiceWs()
                            size += 10
                        } else {
                            progressBar.visibility = View.GONE
                        }
                    }
                }
            }
        })
    }

    interface InitCallBackListener{
        fun onCallBack(item : HotelTravelTalkModel)
        fun onDoReplyCallBack(item: TravelTalkReplyDetail)
    }
}