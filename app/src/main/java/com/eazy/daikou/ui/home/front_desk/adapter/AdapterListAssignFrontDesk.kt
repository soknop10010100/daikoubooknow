package com.eazy.daikou.ui.home.front_desk.adapter

import android.content.Context
import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.front_desk.Pickers
import de.hdodenhof.circleimageview.CircleImageView

class AdapterListAssignFrontDesk(private val context: Context, private val listAssign: ArrayList<Pickers>): RecyclerView.Adapter<AdapterListAssignFrontDesk.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.custom_view_assign_to_front_desk, parent, false)
        return  ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val assignModel = listAssign[position]
        if (assignModel != null){
            if (assignModel.image != null){
                Glide.with(holder.iconImageUser).load(assignModel.image).into(holder.iconImageUser)
            } else {
                Glide.with(holder.iconImageUser).load(R.drawable.no_image).into(holder.iconImageUser)
            }
            holder.assignerNameTv.text = if(assignModel.user_name != null) assignModel.user_name else "- - -"
            holder.phoneNumberTv.text = if(assignModel.phone != null) assignModel.phone else "- - -"
            holder.assignedDateTv.text = if(assignModel.created_at != null) Utils.formatDateFromString("yyyy-MM-dd", "dd-MMM-yyyy", assignModel.created_at) else "- - -"
            holder.statusTv.text = if(assignModel.status != null) assignModel.status else "- - -"

            when (assignModel.status) {
                "pending" -> {
                    holder.statusTv.text = Utils.getText(context, R.string.pending)
                    holder.statusTv.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(context, R.color.yellow))
                }
                "accepted" -> {
                    holder.statusTv.text = Utils.getText(context, R.string.picked_up)
                    holder.statusTv.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(context, R.color.green))
                }
                "rejected" -> {
                    holder.statusTv.text = Utils.getText(context, R.string.picked_up)
                    holder.statusTv.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(context, R.color.red))
                }
                "picked_up" -> {
                    holder.statusTv.text = Utils.getText(context, R.string.picked_up)
                    holder.statusTv.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(context, R.color.blue))
                }
                else -> {
                    holder.statusTv.text = "- - -"
                    holder.statusTv.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(context, R.color.appBarColorOld))

                }
            }
        }
    }

    override fun getItemCount(): Int {
        return listAssign.size
    }

    class ViewHolder(view : View) : RecyclerView.ViewHolder(view){
        var iconImageUser : CircleImageView = view.findViewById(R.id.iconImageUser)
        var assignerNameTv : TextView = view.findViewById(R.id.assignerNameTv)
        var phoneNumberTv : TextView = view.findViewById(R.id.phoneNumberTv)
        var assignedDateTv : TextView = view.findViewById(R.id.assignedDateTv)
        var statusTv : TextView = view.findViewById(R.id.statusTv)
    }
}