package com.eazy.daikou.ui.home.hrm.report_attendance

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Color
import android.location.Geocoder
import android.os.Bundle
import android.provider.MediaStore
import android.view.*
import android.widget.*
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.base.MockUpData
import com.eazy.daikou.request_data.request.hr_ws.MyAttendanceWs
import com.eazy.daikou.helper.GPSTracker
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.helper.map.GPSTrackerActivity
import com.ebanx.swipebtn.SwipeButton
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import java.text.SimpleDateFormat
import java.util.*


class CheckInOutAttendanceActivity : BaseActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    private lateinit var geocoder: Geocoder
    private  var userLat = 0.0
    private  var userLng = 0.0
    private lateinit var swipeBtnCheckIn : SwipeButton
    private lateinit var dateTv : TextView
    private lateinit var timeTv : TextView
    private lateinit var linearSelfie: LinearLayout
    private lateinit var relativeSelfie: RelativeLayout
    private lateinit var btnAddImg: RelativeLayout
    private lateinit var imageSelfie: ImageView
    private lateinit var convertBase64 : Bitmap
    private var isSelfiePhoto = false
    private val CAMERA_REQUEST = 1888
    private var REQUEST_CODE = 0
    private val MY_CAMERA_PERMISSION_CODE = 100

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_check_in_out_attendance)

        initView()

        initData()

        initAction()

    }

    private fun initView(){
        swipeBtnCheckIn = findViewById(R.id.swipeCheckIn)
        dateTv = findViewById(R.id.dateTv)
        timeTv = findViewById(R.id.timeTv)
        imageSelfie = findViewById(R.id.imageSelfie)
        btnAddImg = findViewById(R.id.btnAddImg)
        relativeSelfie = findViewById(R.id.relativeSelfie)
        linearSelfie = findViewById(R.id.linearSelfie)

        val backBtn : ImageView = findViewById(R.id.btn_back)
        backBtn.setOnClickListener{
            finish()
        }
    }
    private fun  initData(){
        if (intent != null && intent.hasExtra("selfie_photo")){
            isSelfiePhoto = intent.getBooleanExtra("selfie_photo", false)
        }
    }

    private fun initAction(){
        val mapFragment = (supportFragmentManager.findFragmentById(R.id.google_map) as SupportMapFragment?)!!
        mapFragment.getMapAsync(this)
        callLocation()
        geocoder = Geocoder(this, Locale.getDefault())

        swipeBtnCheckIn.setOnStateChangeListener {
            // setProgressDialog()
            openSomeActivityForResult()
        }

        // Just Back Listener Enabled Button Check On List
        val intent = Intent()
        intent.putExtra("isClickedFirst", true)
        setResult(RESULT_OK, intent)

        val dateFormShow = SimpleDateFormat(" EEEE, dd MMM yyyy", Locale.getDefault())
        dateTv.text = dateFormShow.format(Date())
        val timeFormat = SimpleDateFormat("HH : mm", Locale.getDefault())
        timeTv.text = timeFormat.format(Date())

        startActionSelfie()
        getSelfiePhoto()
    }

    private fun openSomeActivityForResult() {
        val intent = Intent(this, GPSTrackerActivity::class.java)
        REQUEST_CODE = 0
        resultLauncher.launch(intent)
    }

    private fun getSelfiePhoto(){
        btnAddImg.setOnClickListener {
            if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(arrayOf(Manifest.permission.CAMERA), MY_CAMERA_PERMISSION_CODE)
            } else {
                val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)

                REQUEST_CODE = CAMERA_REQUEST
                resultLauncher.launch(cameraIntent)
            }
        }
    }

    private var resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode == RESULT_OK) {
            if (REQUEST_CODE == CAMERA_REQUEST){
                if(result.data != null){
                    val thumbnail = result.data!!.extras!!.get("data") as Bitmap?
                    imageSelfie.setImageBitmap(thumbnail)

                    convertBase64 = thumbnail as Bitmap
                    relativeSelfie.visibility = View.VISIBLE
                    btnAddImg.visibility = View.GONE
                    // imageSelfie.setImageBitmap(photoSelfie)
                }
            } else {
                if (result.data != null){
                    val data = result.data!!.getBooleanExtra("isSuccess", false)
                    if (data){
                        setProgressDialog()
                    } else {
                        Utils.customToastMsgError(this@CheckInOutAttendanceActivity, "You can check in / out without no your current location", false)
                    }
                }
            }
        }
    }

    private fun startActionSelfie(){
        if (isSelfiePhoto){
            linearSelfie.visibility = View.VISIBLE
        } else{
            linearSelfie.visibility = View.GONE
        }
    }
    private fun setProgressDialog() {
        val llPadding = 30
        val ll = LinearLayout(this)
        ll.orientation = LinearLayout.HORIZONTAL
        ll.setPadding(llPadding, llPadding, llPadding, llPadding)
        ll.gravity = Gravity.CENTER
        var llParam = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        llParam.gravity = Gravity.CENTER
        ll.layoutParams = llParam
        val progressBar = ProgressBar(this)
        progressBar.isIndeterminate = true
        progressBar.setPadding(0, 0, llPadding, 0)
        progressBar.layoutParams = llParam
        llParam = LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        llParam.gravity = Gravity.CENTER
        val tvText = TextView(this)
        tvText.text = resources.getString(R.string.loading)
        tvText.setTextColor(Color.parseColor("#000000"))
        tvText.textSize = 20f
        tvText.layoutParams = llParam
        ll.addView(progressBar)
        ll.addView(tvText)
        val builder: AlertDialog.Builder = AlertDialog.Builder(this)
        builder.setCancelable(false)
        builder.setView(ll)
        val dialog: AlertDialog = builder.create()
        dialog.show()
        val window: Window? = dialog.window
        if (window != null) {
            val layoutParams = WindowManager.LayoutParams()
            layoutParams.copyFrom(dialog.window!!.attributes)
            layoutParams.width = LinearLayout.LayoutParams.WRAP_CONTENT
            layoutParams.height = LinearLayout.LayoutParams.WRAP_CONTENT
            dialog.window!!.attributes = layoutParams
        }
        requestServiceCheckOut(dialog)
    }


    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        zoomMap(latitude, longitude)

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return
        }
        mMap.isMyLocationEnabled = true
        mMap.uiSettings.isZoomControlsEnabled = false
        mMap.uiSettings.isMyLocationButtonEnabled = true
        mMap.uiSettings.isCompassEnabled = false //hide compass

        addMarker(latitude, longitude)
    }

    private fun callLocation() {
        val gps = GPSTracker(this)
        if (gps.canGetLocation()) {
            userLat = gps.latitude
            userLng = gps.longitude
        }
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private fun addMarker(lat: Double, lon: Double) {
        val latLng = LatLng(lat, lon)
        val marker = mMap.addMarker(MarkerOptions().position(latLng)
                .title(resources.getString(R.string.you_are_here))
        )
        marker?.showInfoWindow()
    }

    private fun zoomMap(lat: Double, lng: Double) {
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(lat, lng), 15f))
    }

    private fun requestServiceCheckOut(progressBar : AlertDialog){
        val hashMap : HashMap<String, Any?> = HashMap()
        hashMap["user_business_key"] = MockUpData.userBusinessKey(UserSessionManagement(this))
        hashMap["active_account_business_key"] = MockUpData.activeAccountBusinessKey(UserSessionManagement(this))
        hashMap["device_type"] = "Android"
        hashMap["button_action"] = "check_in"
        hashMap["user_lat"]  = latitude.toString()
        hashMap["user_long"] = longitude.toString()
//        if (isSelfiePhoto ){
//            hashMap["capture_photo"] = Utils.convert(convertBase64)
//        }else{
//            Utils.customToastMsgError(this, "The Photo is Required !", false)
//        }

        MyAttendanceWs().checkOutAttendance(this, hashMap, object : MyAttendanceWs.OnAttendanceCallBackListener{
            override fun onLoadListSuccess(msg: String) {
                progressBar.dismiss()
                val intent = Intent()
                intent.putExtra("status", true)
                setResult(RESULT_OK, intent)
                finish()
                Utils.customToastMsgError(this@CheckInOutAttendanceActivity, msg, true)
            }

            override fun onLoadFail(message: String) {
                progressBar.dismiss()
                val intent = Intent()
                intent.putExtra("status", false)
                setResult(RESULT_OK, intent)
                finish()
                Utils.customToastMsgError(this@CheckInOutAttendanceActivity, message, false)
            }

        })
    }

}