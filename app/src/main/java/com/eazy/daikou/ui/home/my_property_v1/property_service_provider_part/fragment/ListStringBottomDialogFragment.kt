package com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.fragment

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.DialogFragment
import com.eazy.daikou.R
import com.eazy.daikou.helper.Utils
import com.google.android.material.bottomsheet.BottomSheetDialogFragment


class ListStringBottomDialogFragment : BottomSheetDialogFragment() {

    private var onClickOnSortBy: OnClickAllAction? = null
    private var position : Int = 0
    private var listString = ArrayList<String>()
    private lateinit var title : String
    private lateinit var action : String

    fun newInstance(position: Int, listString: ArrayList<String>?, action: String) : ListStringBottomDialogFragment {
        val args = Bundle()
        val fragment = ListStringBottomDialogFragment()
        args.putInt("position", position)
        args.putSerializable("listString", listString)
        args.putString("action", action)
        fragment.arguments = args
        return fragment
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.layout_list_string_dialog_fragment, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, R.style.AppBottomSheetDialogTheme)
    }

    @SuppressLint("ResourceAsColor")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val txtClear = view.findViewById<TextView>(R.id.txtClear)
        val linearLayout = view.findViewById<LinearLayout>(R.id.ListString)
        val txtTitle = view.findViewById<TextView>(R.id.title)
        val closeTv = view.findViewById<LinearLayout>(R.id.close_bttn)

        if (arguments != null) {
            position = requireArguments().getInt("position")
            listString = requireArguments().getSerializable("listString") as ArrayList<String>
            title = requireArguments().getString("title").toString()
            action = requireArguments().getString("action").toString()
        }

        if (action == "no_clear"){
            txtClear.visibility = View.GONE
            closeTv.visibility = View.VISIBLE
        }else{
            txtClear.visibility = View.VISIBLE
            closeTv.visibility = View.GONE
        }

        txtTitle.text = "CLEANING SERVICE LIST"

        var notePosition = 0
        for (list : String in listString){
            notePosition++
            val relativeLayout = RelativeLayout(context)
            val titleString = TextView(context)
            val imageCheck = ImageView(context)
            val viewLine = View(context)
            relativeLayout.id = notePosition

            titleString.text = list
            titleString.textSize = 15f
            val typeface = context?.let {
                ResourcesCompat.getFont(it, R.font.lexend_deca_regular_title)
            }
            titleString.typeface = typeface

            if (position == notePosition){
            //    imageCheck.setImageResource(R.drawable.icons8_checkmark_150)
                titleString.setTextColor(Color.parseColor("#9C50FD"))
            }

            val paramString: RelativeLayout.LayoutParams = RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT)
            titleString.layoutParams = paramString

            val paramImage: RelativeLayout.LayoutParams = RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT)
            paramImage.addRule(RelativeLayout.ALIGN_PARENT_RIGHT)
            imageCheck.layoutParams = paramImage

            val paramViewLine: LinearLayout.LayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, Utils.dpToPx(context, 2))
            viewLine.setBackgroundResource(R.color.gray)
            viewLine.layoutParams = paramViewLine

            relativeLayout.setPadding(Utils.dpToPx(context, 15), Utils.dpToPx(context, 15), Utils.dpToPx(context, 15), Utils.dpToPx(context, 15))
            relativeLayout.addView(imageCheck)
            relativeLayout.addView(titleString)

            linearLayout.addView(relativeLayout)
            linearLayout.addView(viewLine)

            relativeLayout.setOnClickListener {
                onClickOnSortBy?.onClickPopup(list, relativeLayout.id, action)
                dismiss()
            }
        }

        txtClear.setOnClickListener {
            onClickOnSortBy?.onClickClear(action, 0)
            position = 0
            dismiss()
        }
        closeTv.setOnClickListener {
            onClickOnSortBy?.onClickClear(action, 0)
            position = 0
            dismiss()
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        val parent = parentFragment
        onClickOnSortBy = if (parent != null) {
            parent as OnClickAllAction
        } else {
            context as OnClickAllAction
        }
    }

    interface OnClickAllAction {
        fun onClickPopup(txtChoose: String?, position: Int, action: String)
        fun onClickClear(action: String , position : Int)

    }
}

