package com.eazy.daikou.ui.home.service_provider_new.activity

import android.os.Bundle
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.viewpager.widget.ViewPager
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.ui.home.hrm.leave_request.adapter.TabsLeaveAdapter
import com.eazy.daikou.ui.home.service_provider_new.fragment_service_provider.ProductProviderFragment
import com.eazy.daikou.ui.home.service_provider_new.fragment_service_provider.ServiceProviderFragment
import com.google.android.material.tabs.TabLayout

class ServiceProviderListHistoryActivity : BaseActivity() {

    private lateinit var progressBar: ProgressBar
    private lateinit var viewPager : ViewPager
    private lateinit var tabLayout : TabLayout


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_service_provider_list_history)

        initView()

        initData()

        initAction()
    }

    private fun  initView(){
        viewPager = findViewById(R.id.viewPagerTab)
        tabLayout = findViewById(R.id.tabLayouts)

    }
    private fun initData(){

    }
    private fun initAction(){
        findViewById<ImageView>(R.id.imageBack).setOnClickListener { finish() }
        findViewById<TextView>(R.id.toolbarTv).text = "List Service Provider"

        val adapter = TabsLeaveAdapter(supportFragmentManager)
        // add fragment to the list
        adapter.addFragment(ServiceProviderFragment(), getString(R.string.service_list))
        adapter.addFragment(ProductProviderFragment(), getString(R.string.product_list))

        viewPager.adapter = adapter
        tabLayout.setupWithViewPager(viewPager)

    }

}