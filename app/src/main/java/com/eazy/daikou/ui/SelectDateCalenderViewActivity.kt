package com.eazy.daikou.ui

import android.os.Bundle
import android.widget.CalendarView
import android.widget.ImageView
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class SelectDateCalenderViewActivity : BaseActivity() {

    private lateinit var dateFormatServer : SimpleDateFormat
    private var date : String = ""
    private var getStartDate = ""
    private var requestCode = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.select_location_booking_fragment)

        initData()

        initAction()

    }

    private fun initData(){
        if (intent != null && intent.hasExtra("date")){
            date = intent.getStringExtra("date").toString()
        }
        if (intent != null && intent.hasExtra("start_date")){
            getStartDate = intent.getStringExtra("start_date").toString()
        }
        if (intent != null && intent.hasExtra("code")){
            requestCode = intent.getIntExtra("code", 0)
        }
    }

    private fun initAction(){
        findViewById<ImageView>(R.id.btnClose).setOnClickListener{finish()}
        val calendarViewAlert: CalendarView = findViewById(R.id.date_picker)

        dateFormatServer = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())

        if (date != "") calendarViewAlert.setDate (milliseconds(date), true, true)

        calendarViewAlert.setOnDateChangeListener { _: CalendarView, year: Int, month: Int, dayOfMonth: Int ->
            val checkCalendar = Calendar.getInstance()
            checkCalendar[year, month] = dayOfMonth

            val calendar = Calendar.getInstance()
            calendar[year, month] = dayOfMonth

            val date = dateFormatServer.format(calendar.time)
            val intent = intent
            intent.putExtra("date", date)
            setResult(RESULT_OK, intent)
            finish()
        }
        if (getStartDate != "" && requestCode == 987){  // requestCode = 987 is select end date
            calendarViewAlert.minDate = milliseconds(getStartDate)
        } else {
            calendarViewAlert.minDate = System.currentTimeMillis() - 1000
        }
    }

    private fun milliseconds(date: String?): Long {
        try {
            val mDate = date?.let { dateFormatServer.parse(it) }
            return mDate?.time!!
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return 0
    }

}