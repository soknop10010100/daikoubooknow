package com.eazy.daikou.ui.home.inspection_work_order

import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.request_data.request.work_oder_ws.WorkOrderWs
import com.eazy.daikou.request_data.static_data.StaticUtilsKey
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.inspection.SubItemTemplateAPIModel
import com.eazy.daikou.model.work_order.ParentTaskWorkOrderModel
import com.eazy.daikou.ui.home.inspection_work_order.adapter.ParentTaskListAdapter
import java.util.*
import kotlin.collections.ArrayList

class ParentTaskWorkOrderActivity : BaseActivity() {

    private lateinit var progressBar: ProgressBar
    private lateinit var unitNoTv : TextView
    private lateinit var propertyNameTv : TextView
    private lateinit var templateNameTv : TextView
    private lateinit var recyclerView: RecyclerView
    private lateinit var inspectionNameEd: EditText
    private lateinit var optionTypeTv: TextView
    private lateinit var parentTaskWorkOrderModel: ParentTaskWorkOrderModel

    private var parentTaskId : String = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_parent_task_work_order)

        initView()

        initData()

        requestData()
    }

    private fun initView(){
        val titleToolbar = findViewById<TextView>(R.id.titleToolbar)
        findViewById<View>(R.id.layoutMap).visibility = View.INVISIBLE
        findViewById<View>(R.id.iconBack).setOnClickListener { finish() }
        titleToolbar.text = resources.getString(R.string.parent_task).uppercase(Locale.ROOT)

        progressBar = findViewById(R.id.progressItem)
        inspectionNameEd = findViewById(R.id.editHeadItemEdt)
        recyclerView = findViewById(R.id.subInspection)

        unitNoTv = findViewById(R.id.unitNoTv)
        templateNameTv = findViewById(R.id.templateTv)
        propertyNameTv = findViewById(R.id.propertyNameTv)
        optionTypeTv = findViewById(R.id.optionTypeTv)

        findViewById<View>(R.id.swipe_layout).visibility = View.GONE
        findViewById<View>(R.id.editLayoutInspection).visibility = View.VISIBLE
        findViewById<View>(R.id.iconAddImage).visibility = View.GONE
        findViewById<View>(R.id.iconAddEdit).visibility = View.GONE

    }

    private fun initData(){
        if(intent != null && intent.hasExtra("parent_task_id")){
            parentTaskId = intent.getStringExtra("parent_task_id").toString()
        }
        inspectionNameEd.isEnabled = false
    }

    private fun requestData(){
        WorkOrderWs().getParentTaskList(this, parentTaskId, object : WorkOrderWs.CallBackParenTaskListener{
            override fun onSuccessParentTask(itemTemplateAPIModel: ParentTaskWorkOrderModel?) {
                progressBar.visibility = View.GONE
                parentTaskWorkOrderModel = itemTemplateAPIModel!!

                setValues()
            }

            override fun onFailed(msg: String?) {
                progressBar.visibility = View.GONE
                if(msg == StaticUtilsKey.no_item_found){
                    Utils.validateViewNoItemFound(this@ParentTaskWorkOrderActivity, findViewById(R.id.dataLayout), true)
                } else {
                    Utils.customToastMsgError(this@ParentTaskWorkOrderActivity, msg, false)
                }
            }

        })
    }

    private fun setValues(){
        propertyNameTv.text = if (parentTaskWorkOrderModel.propertyName != null) parentTaskWorkOrderModel.propertyName else ". . ."
        unitNoTv.text = if (parentTaskWorkOrderModel.unitNo != null) parentTaskWorkOrderModel.unitNo else ". . ."
        templateNameTv.text = if (parentTaskWorkOrderModel.templateName != null) parentTaskWorkOrderModel.templateName else ". . ."

        Utils.setOptionTypeInspection(this, parentTaskWorkOrderModel.optionType, optionTypeTv)

        if (parentTaskWorkOrderModel.inspectionTitle != null) {
            inspectionNameEd.setText(parentTaskWorkOrderModel.inspectionTitle)
        } else {
            inspectionNameEd.setText(". . .")
        }
        inspectionNameEd.isEnabled = false

        initRecyclerView()

    }

    private fun initRecyclerView(){
        val parentTaskList : ArrayList<SubItemTemplateAPIModel> = ArrayList()
        if (parentTaskWorkOrderModel.inspectionSubTitle != null){
            parentTaskList.add(parentTaskWorkOrderModel.inspectionSubTitle)

            recyclerView.layoutManager = LinearLayoutManager(this)
            recyclerView.adapter = ParentTaskListAdapter(parentTaskList, parentTaskWorkOrderModel.optionType, this@ParentTaskWorkOrderActivity)
        }
    }
}