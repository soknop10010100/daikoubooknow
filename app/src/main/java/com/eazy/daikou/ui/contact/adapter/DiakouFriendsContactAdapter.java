package com.eazy.daikou.ui.contact.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.eazy.daikou.R;
import com.eazy.daikou.model.contact.FriendContactModel;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class DiakouFriendsContactAdapter extends RecyclerView.Adapter<DiakouFriendsContactAdapter.ViewHolder> {

    private final List<FriendContactModel> contactModelList ;
    private final Context context;

    public DiakouFriendsContactAdapter(List<FriendContactModel> contactModelList, Context context) {
        this.contactModelList = contactModelList;
        this.context = context;
    }

    @NonNull
    @Override
    public DiakouFriendsContactAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.diaku_adapter_layout,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DiakouFriendsContactAdapter.ViewHolder holder, int position) {
        FriendContactModel contactModel = contactModelList.get(position);
        if (contactModel != null) {
            if (contactModel.getFirstName() != null && contactModel.getLastName() != null) {
                holder.diakouName.setText(contactModel.getFirstName() + " " + contactModel.getLastName());
            } else if (contactModel.getFirstName() != null) {
                holder.diakouName.setText(contactModel.getFirstName());
            } else if (contactModel.getLastName() != null) {
                holder.diakouName.setText(contactModel.getLastName());
            } else {
                holder.diakouName.setText(context.getResources().getString(R.string.no_name));
            }

            if (contactModel.getPhonecode() != null && contactModel.getPhoneNo() != null) {
                holder.phoneNumber.setText(contactModel.getPhonecode() + " " + contactModel.getPhoneNo());
            } else if (contactModel.getPhoneNo() != null){
                holder.phoneNumber.setText(contactModel.getPhoneNo());
            } else {
                holder.phoneNumber.setText(context.getResources().getString(R.string.no_phone_number));
            }

            holder.emailTv.setText(contactModel.getEmail() != null ? contactModel.getEmail() : context.getResources().getString(R.string.no_email));

            if (contactModel.getPhotoUrl() != null) {
                Glide.with(holder.profile_image).load(contactModel.getPhotoUrl()).into(holder.profile_image);
            } else {
                Glide.with(holder.profile_image).load(R.drawable.ic_my_profile).into(holder.profile_image);
            }
        }
    }

    @Override
    public int getItemCount() {
        return contactModelList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView diakouName, phoneNumber, emailTv;
        private final CircleImageView profile_image;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            emailTv = itemView.findViewById(R.id.friend_email);
            diakouName = itemView.findViewById(R.id.diaku_name);
            phoneNumber = itemView.findViewById(R.id.custmer_phone_number);
            profile_image = itemView.findViewById(R.id.profile_image);
        }
    }

    public void clear() {
        int size = contactModelList.size ();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                contactModelList.remove ( 0 );
            }
            notifyItemRangeRemoved ( 0, size );
        }
    }
}
