package com.eazy.daikou.ui.home.hrm.leave_request

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.*
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.base.MockUpData
import com.eazy.daikou.request_data.request.hr_ws.LeaveManagementWs
import com.eazy.daikou.request_data.static_data.StaticUtilsKey
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.model.hr.LeaveCategory
import com.eazy.daikou.model.hr.LeaveTypeCategory
import com.eazy.daikou.ui.home.hrm.leave_request.adapter.ListTypeCategoryAdapter

class LeaveTypeCategoryActivity : BaseActivity() {

    private lateinit var recyclerView : RecyclerView
    private lateinit var loading : ProgressBar
    private lateinit var titleTv : TextView
    private lateinit var editSearchNameApprove : EditText
    private lateinit var btnClearSearch : ImageView
    private lateinit var noDataLayout: LinearLayout
    private lateinit var leaveCategoryAdapter : ListTypeCategoryAdapter
    private  var leaveCategoriesList = ArrayList<LeaveCategory>()

    private var type : String= ""
    private var itemId : String = ""

    @SuppressLint("NotifyDataSetChanged", "SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_leave_type_category)

        initView()

        initData()

        if (type == StaticUtilsKey.leave_period_action){
            val listPeriod: ArrayList<LeaveCategory> = getListPeriod()
            for (item in listPeriod){
                item.isClick = item.id == itemId
            }
            val layoutManager = GridLayoutManager(this, 2)
            recyclerView.layoutManager = layoutManager
            leaveCategoryAdapter =  ListTypeCategoryAdapter(listPeriod, itemClickListener)
            recyclerView.adapter = leaveCategoryAdapter
            loading.visibility = View.GONE
        } else {
            val layoutManager = LinearLayoutManager(this)
            recyclerView.layoutManager = layoutManager
            getListCategory("")
        }

    }

    private fun initView(){
        recyclerView = findViewById(R.id.list_String)
        titleTv = findViewById(R.id.name_left)
        editSearchNameApprove = findViewById(R.id.editSearchNameApprove)
        noDataLayout = findViewById(R.id.noDataLayout)
        btnClearSearch = findViewById(R.id.btnClearSearch)
        val btnBack = findViewById<TextView>(R.id.btn_back)
        btnBack.setOnClickListener { finish() }
        loading = findViewById(R.id.progressItem)
        loading.visibility = View.VISIBLE
    }

    private fun initData(){
        if(intent.hasExtra("action")){
            type = intent.getStringExtra("action") as String
        }
        if(intent.hasExtra("id")){
            itemId = intent.getStringExtra("id").toString()
        }

        getSearchLeaveType()

        when (type) {
            StaticUtilsKey.leave_category_action -> {
                titleTv.text = resources.getString(R.string.choose_leave_type)
            }
            StaticUtilsKey.leave_period_action -> {
                titleTv.text = resources.getString(R.string.period)
            }
            else -> {
                titleTv.text = resources.getString(R.string.choose_approved_by)
                editSearchNameApprove.visibility = View.VISIBLE
            }
        }
    }

    private fun getSearchLeaveType(){
        editSearchNameApprove.addTextChangedListener(object : TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

            override fun afterTextChanged(s: Editable?) {
               btnClearSearch.setOnClickListener{
                   editSearchNameApprove.setText("")
                   s?.clear()
               }
                if (s.toString().isNotEmpty()) {
                    loading.visibility = View.VISIBLE
                    btnClearSearch.visibility = View.VISIBLE
                    leaveCategoriesList.clear()
                    getListCategory(s.toString())
                } else {
                    loading.visibility = View.VISIBLE
                    btnClearSearch.visibility = View.GONE
                    leaveCategoriesList.clear()
                    getListCategory("")
                }

            }

        })
    }

    private fun getListCategory(keySearch: String){
        LeaveManagementWs().getLeaveCategory(this, MockUpData.userBusinessKey(UserSessionManagement(this)), keySearch,leaveTypeCallBackListener)
    }
    private val leaveTypeCallBackListener = object : LeaveManagementWs.OnCallBackLeaveTypeCategoryListener{
        @SuppressLint("NotifyDataSetChanged")
        override fun onLoadListSuccessFull(listLeaveCategory: LeaveTypeCategory) {
            loading.visibility = View.GONE
            leaveCategoriesList.clear()

            if(type == StaticUtilsKey.leave_category_action){
                for (item in listLeaveCategory.leave_categories){
                    item.isClick = item.id == itemId
                }
                leaveCategoryAdapter =  ListTypeCategoryAdapter(listLeaveCategory.leave_categories, itemClickListener)
            } else {
                if (listLeaveCategory.approved_by.size > 0){
                    noDataLayout.visibility = View.GONE
                    recyclerView.visibility = View.VISIBLE
                } else {
                    noDataLayout.visibility = View.VISIBLE
                    recyclerView.visibility = View.GONE
                }

                for (item in listLeaveCategory.approved_by){
                    item.isClick = item.id == itemId
                }
                leaveCategoryAdapter = ListTypeCategoryAdapter(StaticUtilsKey.leave_approved_by_action, listLeaveCategory.approved_by, itemClickListener)
            }
            recyclerView.adapter = leaveCategoryAdapter
        }

        override fun onLoadFail(message: String) {
            loading.visibility = View.GONE
           Toast.makeText(this@LeaveTypeCategoryActivity, message, Toast.LENGTH_SHORT).show()
        }

    }

    private val itemClickListener = object : ListTypeCategoryAdapter.ItemClickOnLeaveCategory{
        override fun onClickItemLeaveCategory(name : String, id: String,action : String) {
            val  intent = intent
            intent.putExtra("name_leave",name)
            intent.putExtra("id_leave",id)
            intent.putExtra("action",type)
            setResult(RESULT_OK, intent)
            finish()
        }
    }

    private fun getListPeriod(): ArrayList<LeaveCategory> {
        var start = 0.5
        val end = 35
        var i = 0.5
        while (i < end) {
            if (start + 0.5 <= end) {
                start += 0.5
                val leaveTypeCategory = LeaveCategory()
                leaveTypeCategory.id = "$i".replace(".0", "")
                leaveTypeCategory.leave_category = "$i".replace(".0", "") + " " + resources.getString(R.string.day_s).lowercase()
                leaveCategoriesList.add(leaveTypeCategory)
                i += 0.5
            } else {
                break
            }
        }
        return leaveCategoriesList
    }

}