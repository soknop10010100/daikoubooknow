package com.eazy.daikou.ui.home.my_property.property_service.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.databinding.MainServiceTimeAvailableBinding
import com.eazy.daikou.model.my_property.property_service.ServiceAvailableTimeModel
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener

class MainServiceTypeAdapter(private val list: ArrayList<ServiceAvailableTimeModel>, private val onClickListener: ClickCallBackListener):
    RecyclerView.Adapter<MainServiceTypeAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = MainServiceTimeAvailableBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBindView(list[position], onClickListener)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ViewHolder(private val mBind: MainServiceTimeAvailableBinding) : RecyclerView.ViewHolder(mBind.root) {

        fun onBindView(service: ServiceAvailableTimeModel, onClickListener: ClickCallBackListener){
            mBind.timeLayout.hint = String.format("%s %s", "Select Time For ", service.name)
            mBind.timeTV.setText(service.numberOfTime)

            mBind.timeTV.setOnClickListener(CustomSetOnClickViewListener {
                onClickListener.onClickCallBack(service)
            })
        }
    }

    interface ClickCallBackListener{
        fun onClickCallBack(feeTypeModel: ServiceAvailableTimeModel)
    }

    companion object {
        fun addListRegService(day : String) : ServiceAvailableTimeModel {
            val item = ServiceAvailableTimeModel(day, "")
            val timeList = ArrayList<String>()
            for (i in 0 until 24){
                timeList.add("1$i : 1$i")
            }
            item.timeAvailableList = timeList
            return item
        }

    }
}