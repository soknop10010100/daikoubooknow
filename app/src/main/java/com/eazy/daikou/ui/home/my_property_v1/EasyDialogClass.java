package com.eazy.daikou.ui.home.my_property_v1;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.eazy.daikou.helper.Utils;
import com.michael.easydialog.EasyDialog;
import com.eazy.daikou.R;

public class EasyDialogClass {

    public static void createFilterEasyDialog(EasyDialog easyDialog, Context context, View view, View imageView, boolean isBooking){
        if(view.getParent() != null) {
            ((ViewGroup)view.getParent()).removeView(view);
        }
        easyDialog
                .setLayout(view)
                .setBackgroundColor(Utils.getColor(context, isBooking ? R.color.white : R.color.color_dark_gray_center))
                .setLocationByAttachedView(imageView)
                .setGravity(EasyDialog.GRAVITY_BOTTOM)
                .setAnimationTranslationShow(EasyDialog.GRAVITY_BOTTOM, 500, 60, 0, 0, 0, 0)
                .setAnimationAlphaShow(1000, 0.3f, 1.0f)
                .setAnimationTranslationDismiss(EasyDialog.DIRECTION_X, 500, 0,0)
                .setAnimationAlphaDismiss(500, 1.0f, 0.0f)
                .setTouchOutsideDismiss(true)
                .setMatchParent(true)
                .setOutsideColor(context.getResources().getColor(R.color.transparent_color))
                .setMarginLeftAndRight(24, 24);
        easyDialog.show();
    }

    public static void createFilterEasyDialogNearLinearLayout(EasyDialog easyDialog, Context context, View view, LinearLayout linearLayout){
        if(view.getParent() != null) {
            ((ViewGroup)view.getParent()).removeView(view);
        }
        easyDialog
                .setLayout(view)
                .setBackgroundColor(Utils.getColor(context, R.color.color_dark_gray))
                .setLocationByAttachedView(linearLayout)
                .setGravity(EasyDialog.GRAVITY_BOTTOM)
                .setAnimationTranslationShow(EasyDialog.GRAVITY_BOTTOM, 500, 60, 0, 0, 0, 0)
                .setAnimationAlphaShow(1000,  1.0f)
                .setAnimationTranslationDismiss(EasyDialog.DIRECTION_X, 500, 0, 0)
                .setAnimationAlphaDismiss(500, 1.0f, 0.0f)
                .setTouchOutsideDismiss(true)
                .setMatchParent(true)
                .setOutsideColor(context.getResources().getColor(R.color.transparent_color))
                .setMarginLeftAndRight(24, 24);
        easyDialog.show();
    }

    public static void createDeleteItemEasyDialog(EasyDialog easyDialog, Context context, View view, ImageView imageView){
        if(view.getParent() != null) {
            ((ViewGroup)view.getParent()).removeView(view);
        }
        easyDialog
                .setLayout(view)
                .setBackgroundColor(context.getResources().getColor(R.color.white))
                .setLocationByAttachedView(imageView)
                .setGravity(EasyDialog.GRAVITY_BOTTOM)
                .setAnimationTranslationShow(EasyDialog.DIRECTION_X, 1000, -600, 100, -50, 50, 0)
                .setAnimationAlphaShow(1000, 0.3f, 1.0f)
                .setAnimationTranslationDismiss(EasyDialog.DIRECTION_X, 500, -50, 800)
                .setAnimationAlphaDismiss(500, 1.0f, 0.0f)
                .setTouchOutsideDismiss(true)
                .setMatchParent(false)
                .setMarginLeftAndRight(30, 30)
                .setOutsideColor(context.getResources().getColor(R.color.transparent));
        easyDialog.show();
    }

    public static void createLeftMenuEasyDialog(EasyDialog easyDialog, Context context, View view, ImageView imageView){
        if(view.getParent() != null) {
            ((ViewGroup)view.getParent()).removeView(view);
        }
        easyDialog
                .setLayout(view)
                .setBackgroundColor(context.getResources().getColor(R.color.white))
                .setLocationByAttachedView(imageView)
                .setGravity(EasyDialog.GRAVITY_LEFT)
                .setAnimationTranslationShow(EasyDialog.GRAVITY_LEFT, 500, 60, 0, 0, 0, 0)
                .setAnimationAlphaShow(1000, 0.3f, 1.0f)
                .setAnimationTranslationDismiss(EasyDialog.DIRECTION_X, 500, 0, 0)
                .setAnimationAlphaDismiss(500, 1.0f, 0.0f)
                .setTouchOutsideDismiss(true)
                .setMatchParent(false)
                .setMarginLeftAndRight(30, 30)
                .setOutsideColor(context.getResources().getColor(R.color.transparent_color));
        easyDialog.show();
    }

}
