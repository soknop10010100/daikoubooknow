package com.eazy.daikou.ui.home.my_property.my_unit

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.helper.CustomStartIntentUtilsClass.Companion.onStartPaymentInvoiceActivity
import com.eazy.daikou.helper.GetDataUtils
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.CustomCategoryModel
import com.eazy.daikou.model.my_property.my_property.PropertyMyUnitModel
import com.eazy.daikou.model.my_property.my_property.PropertyPaymentUnitModel
import com.eazy.daikou.ui.CustomMenuHeaderAdapter
import com.eazy.daikou.ui.home.my_property.adapter.PropertyPaymentUnitAdapter

class PropertyPaymentMyUnitActivity : BaseActivity() {

    private lateinit var progressBar : ProgressBar
    private lateinit var recyclerView: RecyclerView
    private lateinit var propertyPaymentUnitAdapter : PropertyPaymentUnitAdapter
    private lateinit var linearLayoutManager: LinearLayoutManager
    private var propertyPaymentUnitModelList : ArrayList<PropertyPaymentUnitModel> = ArrayList()
    private var action : String = ""
    private var unitNo = ""
    private lateinit var linearPaymentOption : LinearLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment_schedule_my_unit)

        initView()

        initData()

        initAction()
    }

    private fun initView() {
        linearPaymentOption = findViewById(R.id.linearPaymentOption)
        recyclerView = findViewById(R.id.recyclerPaymentUnit)
        progressBar = findViewById(R.id.progressItem)
        progressBar.visibility = View.GONE

        Utils.customOnToolbar(this, resources.getString(R.string.payment_schedule)){finish()}

        findViewById<LinearLayout>(R.id.paymentLayout).background = Utils.setDrawable(this, R.drawable.bg_below_shap_white)

    }

    private fun initData(){
        // init data
        if (intent != null && intent.hasExtra("action")){
            action = intent.getStringExtra("action").toString()
        }

        unitNo = GetDataUtils.getDataFromString("unit_no", this)

        if (intent != null &&  intent.hasExtra("unit_detail_Item_model")){
            val unitDetailItemModel = intent.getSerializableExtra("unit_detail_Item_model") as PropertyMyUnitModel

            propertyPaymentUnitModelList = unitDetailItemModel.payment_schedules

            setValuePaymentHeader(unitDetailItemModel)
        }

        setMenuStepAdapter(this)

    }

    private fun initAction() {
        linearLayoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = linearLayoutManager

        initRecyclerView()

    }

    private fun initRecyclerView(){
        propertyPaymentUnitAdapter = PropertyPaymentUnitAdapter(this, propertyPaymentUnitModelList, onClickAdapterListener)
        recyclerView.adapter = propertyPaymentUnitAdapter
    }

    private var onClickAdapterListener = object : PropertyPaymentUnitAdapter.OnCallBackListenerPayment{
        override fun onClickBackItemPayment(paymentsModel: PropertyPaymentUnitModel) {
            onStartPaymentInvoiceActivity(
                this@PropertyPaymentMyUnitActivity,
                paymentsModel.payment_status,
                paymentsModel.invoice_receipt_url,
                paymentsModel.kess_payment_link
            )
        }
    }

    // More step
    private fun setMenuStepAdapter(context: Activity){
        val list = ArrayList<CustomCategoryModel>()
        list.addAll(CustomMenuHeaderAdapter.addMenuStepList(context, unitNo,"payment_scheduled"))

        CustomMenuHeaderAdapter.setMenuStepAdapter(context, "step_menus", list, object : CustomMenuHeaderAdapter.ClickCallBackListener{
            override fun onClickCallBack(item: CustomCategoryModel) {
                if (item.id == "my_unit_detail"){
                    finish()
                } else {    // Back Home
                    val intent = Intent()
                    intent.putExtra("menu", item.id)
                    setResult(RESULT_OK, intent)
                    finish()
                }
            }
        })
    }

    private fun setValuePaymentHeader(detailProperty : PropertyMyUnitModel){

        linearPaymentOption.visibility = View.GONE
        // Payment
        Utils.setValueOnText(findViewById(R.id.buyDateTv), detailProperty.purchase_date)
        Utils.setValueOnText(findViewById(R.id.originalPriceTv), detailProperty.price)
        Utils.setValueOnText(findViewById(R.id.voucherAmountTv), detailProperty.voucher_amount)
        Utils.setValueOnText(findViewById(R.id.discountTv), detailProperty.discount)
        Utils.setValueOnText(findViewById(R.id.paymentOptionTv), detailProperty.payment_term_type)
        Utils.setValueOnText(findViewById(R.id.sellingPriceTv), detailProperty.sale_price)

        // Account
        findViewById<LinearLayout>(R.id.propertyLayout).visibility = View.VISIBLE
        if (detailProperty.account != null){
            Utils.setValueOnText(findViewById(R.id.accountNameTv), detailProperty.account!!.name)
        }
    }

}