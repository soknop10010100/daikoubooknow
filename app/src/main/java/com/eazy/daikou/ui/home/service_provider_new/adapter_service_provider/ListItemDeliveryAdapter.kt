package com.eazy.daikou.ui.home.service_provider_new.adapter_service_provider

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eazy.daikou.R

class ListItemDeliveryAdapter(private val context: Context, private val callBack: CallBackItemListener): RecyclerView.Adapter<ListItemDeliveryAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.custom_layout_view_item_delivery, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        Glide.with(context).load(R.drawable.cv_home_overtime).into(holder.imageDelivery)
        holder.paymentMethodTv.text = "By my Own"

        holder.itemView.setOnClickListener {  callBack.clickCallBack("more") }
    }

    override fun getItemCount(): Int {
        return 2
    }
    class ViewHolder(view: View): RecyclerView.ViewHolder(view){

        var imageDelivery: ImageView= view.findViewById(R.id.imageDelivery)
        var paymentMethodTv: TextView = view.findViewById(R.id.paymentMethodTv)
    }

    interface CallBackItemListener{
        fun clickCallBack(action: String)
    }
}