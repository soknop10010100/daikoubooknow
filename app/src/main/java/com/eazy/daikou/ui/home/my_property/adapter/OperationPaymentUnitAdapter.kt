package com.eazy.daikou.ui.home.my_property.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.my_property.payment_schedule_my_unit.OperationPaymentInfoModel

class OperationPaymentUnitAdapter (private val context : Context, private val operationPaymentSchedulesList: ArrayList<OperationPaymentInfoModel>, private val callBackPayment : OnCallBackListenerPayment):
    RecyclerView.Adapter<OperationPaymentUnitAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.operation_payment_unit_layout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val operationPaymentUnit = operationPaymentSchedulesList[position]
        if (operationPaymentUnit != null){
            Utils.setValueOnText(holder.invoiceNoTv, operationPaymentUnit.invoice_no)
            Utils.setValueOnText(holder.issueDateTv, operationPaymentUnit.issue_dt)
            Utils.setValueOnText(holder.dueDateTv, operationPaymentUnit.due_dt)
            Utils.setValueOnText(holder.totalPriceTv, operationPaymentUnit.total)
            Utils.setValueOnText(holder.itemNameTv, operationPaymentUnit.item_name)

            if (operationPaymentUnit.payment_status != null){ when (operationPaymentUnit.payment_status) {
                "paid" -> {
                    holder.statusTv.text = context.resources.getString(R.string.paid)
                    Utils.setBgTint(holder.statusTv, R.color.green)
                }
                "unpaid" -> {
                    holder.statusTv.text = context.resources.getString(R.string.un_paid)
                    Utils.setBgTint(holder.statusTv, R.color.red)
                }
                else -> {
                    holder.statusTv.text = context.resources.getString(R.string.overdue)
                    Utils.setBgTint(holder.statusTv, R.color.yellow)
                }
            }

            }

            holder.paymentLayout.setOnClickListener(CustomSetOnClickViewListener { callBackPayment.onClickBackItemPayment(operationPaymentUnit) })
        }
    }

    override fun getItemCount(): Int {
        return operationPaymentSchedulesList.size
    }

    class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView){
        var invoiceNoTv : TextView = itemView.findViewById(R.id.invoiceNoTv)
        var issueDateTv : TextView = itemView.findViewById(R.id.issueDateTv)
        var dueDateTv : TextView = itemView.findViewById(R.id.dueDateTv)
        var totalPriceTv : TextView = itemView.findViewById(R.id.totatPriceTv)
        var statusTv : TextView = itemView.findViewById(R.id.statusTv)
        var itemNameTv : TextView = itemView.findViewById(R.id.itemNameTv)
        var paymentLayout : CardView = itemView.findViewById(R.id.paymentLayout)
    }

    interface OnCallBackListenerPayment{
        fun onClickBackItemPayment(paymentsModel : OperationPaymentInfoModel)
    }

    fun clear() {
        val size: Int = operationPaymentSchedulesList.size
        if (size > 0) {
            for (i in 0 until size) {
                operationPaymentSchedulesList.removeAt(0)
            }
            notifyItemRangeRemoved(0, size)
        }
    }
}