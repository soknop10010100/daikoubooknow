package com.eazy.daikou.ui.home.laws.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.eazy.daikou.R;
import com.eazy.daikou.helper.Utils;
import com.eazy.daikou.model.law.LawsDetailModel;

import java.util.List;

public class LawsDetailAdapter extends RecyclerView.Adapter<LawsDetailAdapter.ItemViewHolder> {
    private final List<LawsDetailModel> lawsDetailList;
    private final LawsClickCallBackListener callBackListener;

    public LawsDetailAdapter(List<LawsDetailModel> lawsDetailList, LawsClickCallBackListener callBackListener) {
        this.lawsDetailList = lawsDetailList;
        this.callBackListener = callBackListener;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rowView = LayoutInflater.from(parent.getContext()).inflate(R.layout.my_role_main_model, parent, false);
        return new ItemViewHolder(rowView);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        LawsDetailModel lawsDetailModel = lawsDetailList.get(position);
        if (lawsDetailModel != null){
            holder.icon.setImageDrawable(ResourcesCompat.getDrawable(holder.icon.getContext().getResources(), R.drawable.ic_home_notebook_v2, null));
            holder.title.setText(lawsDetailModel.getTitle() != null ? lawsDetailModel.getTitle() : "- - -");
            holder.descriptionTv.setText(String.format("%s", "Read . . ."));
            Utils.setBgTint(holder.cardIcon, R.color.blue);
            holder.cardLayout.setOnClickListener(v -> callBackListener.lawsCallBack(lawsDetailModel));
        }
    }

    @Override
    public int getItemCount() {
        return lawsDetailList.size();
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder{
        private final ImageView icon;
        private final TextView title, descriptionTv;
        private final CardView cardIcon;
        private final CardView cardLayout;
        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            icon = itemView.findViewById(R.id.imgProfile);
            title = itemView.findViewById(R.id.name_item_hr);
            descriptionTv = itemView.findViewById(R.id.descriptionTv);
            cardIcon = itemView.findViewById(R.id.cardIcon);
            cardLayout = itemView.findViewById(R.id.card_hr);
        }
    }

    public interface LawsClickCallBackListener{
        void lawsCallBack(LawsDetailModel lawsDetailModel);
    }

    public void clear() {
        int size = lawsDetailList.size ();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                lawsDetailList.remove ( 0 );
            }
            notifyItemRangeRemoved ( 0, size );
        }
    }
}
