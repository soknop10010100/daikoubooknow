package com.eazy.daikou.ui.home.my_property.payment

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.activity.result.ActivityResult
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.base.MockUpData.getAccountUserId
import com.eazy.daikou.base.MockUpData.getUserItem
import com.eazy.daikou.helper.BroadcastTypes
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.ui.home.my_property.my_unit.PropertyPaymentItemFragment
import com.eazy.daikou.ui.profile.SwitchProfileAndPropertyActivity

class PropertyPaymentActivity : BaseActivity() {

    private var propertyId = ""
    private var categoryType = "sale"
    private lateinit var accountNameTv : TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_property_payment)

        // Init View
        Utils.customOnToolbar(this, resources.getString(R.string.payment)) { finish() }
        accountNameTv = findViewById(R.id.nameTv)

        // Switch Property
        val user = getUserItem(UserSessionManagement(this))
        Utils.setValueOnText(accountNameTv, user.accountName)

        // Alert Property
        findViewById<View>(R.id.mainLayout).setOnClickListener(CustomSetOnClickViewListener{
            val intent = Intent(this@PropertyPaymentActivity, SwitchProfileAndPropertyActivity::class.java).apply {
                putExtra("action", "switch_property")
            }

            activityLauncher.launch(intent) { result: ActivityResult ->
                val hashMap = SwitchProfileAndPropertyActivity.checkDisplayAccount(result)

                if (hashMap.isEmpty())  return@launch

                if (hashMap.containsKey("name")){
                    accountNameTv.text = hashMap["name"].toString()

                    // Send to home fragment to set account name
                    val intentBroad = Intent(BroadcastTypes.UPDATE_IMAGE.name).apply {
                        putExtra("user_account_name", hashMap["name"].toString())
                    }
                    LocalBroadcastManager.getInstance(this).sendBroadcast(intentBroad)

                    // Refresh List
                    replaceFragment()
                }
            }
        })

        // On click category
        initClickMenuCategory()

        // Refresh List
        replaceFragment()
    }

    private fun initClickMenuCategory() {
        val categoryLayout: LinearLayout = findViewById(R.id.categoryLayout)
        for (i in 0 until categoryLayout.childCount) {
            categoryLayout.getChildAt(i).setOnClickListener { v ->
                setBackground(v.id, categoryLayout)
                categoryType = when (i) {
                    0 -> {
                        "sale"
                    }
                    2 -> {
                        "lease"
                    }
                    else -> {
                        "operation"
                    }
                }

                replaceFragment()
            }
        }
    }

    private fun setBackground(id: Int, categoryLayout: LinearLayout) {
        for (i in 0 until categoryLayout.childCount) {
            val rowView: View = categoryLayout.getChildAt(i)
            if (rowView is TextView) {
                rowView.setTextColor(
                    Utils.getColor(this,
                        if (id == rowView.id) R.color.appBarColorOld else R.color.gray
                    )
                )
            }
        }
    }

    private fun replaceFragment() {
        propertyId = getAccountUserId(UserSessionManagement(this))
        val transaction = supportFragmentManager.beginTransaction()
        val propertyPaymentFragment = PropertyPaymentItemFragment.newInstance(categoryType, propertyId)
        transaction.replace(R.id.container, propertyPaymentFragment, propertyPaymentFragment.tag)
        transaction.disallowAddToBackStack()
        transaction.commit()
    }

    override fun onBackPressed() {
        finish()
    }

}