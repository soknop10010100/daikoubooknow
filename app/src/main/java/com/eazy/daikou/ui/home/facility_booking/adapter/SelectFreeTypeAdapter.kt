package com.eazy.daikou.ui.home.facility_booking.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.model.facility_booking.FeeTypeModel
import java.util.*

class SelectFreeTypeAdapter(private val context : Context,private val freeTypeList : List<FeeTypeModel>, private val clickCallBack : ClickCallBackListener) : RecyclerView.Adapter<SelectFreeTypeAdapter.ItemViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.suggestion_complaint_model, parent, false)
        return ItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val feeTypeModel : FeeTypeModel = freeTypeList[position]
        holder.nameCategoryTv.text = String.format(Locale.US, "%s", feeTypeModel.freeTypeName)
        holder.linearLayoutView.visibility = View.VISIBLE
        holder.linearLayout.visibility = View.GONE

        if (feeTypeModel.isClick) {
            holder.nameCategoryTv.setTextColor(ContextCompat.getColor(context, R.color.white))
            holder.linearLayoutView.setBackgroundResource(R.drawable.on_swipe_shape_color_app)
        } else {
            if (feeTypeModel.isEnableItem){
                holder.nameCategoryTv.isEnabled = true
                holder.nameCategoryTv.setTextColor(ContextCompat.getColor(context, R.color.black))
            } else {
                holder.nameCategoryTv.isEnabled = false
                holder.nameCategoryTv.setTextColor(ContextCompat.getColor(context, R.color.gray))
            }
            holder.linearLayoutView.setBackgroundResource(R.drawable.bg_swap_app_appbar_strok)
        }

        holder.nameCategoryTv.setOnClickListener{clickCallBack.onClickCallBack(feeTypeModel)}

    }

    override fun getItemCount(): Int {
        return freeTypeList.size
    }

    interface ClickCallBackListener{
        fun onClickCallBack(feeTypeModel: FeeTypeModel)
    }

    class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val mainLayout: LinearLayout = itemView.findViewById(R.id.mainLayout)
        val linearLayout: LinearLayout = itemView.findViewById(R.id.linear_card)
        val linearLayoutView: LinearLayout = itemView.findViewById(R.id.linear_card_wrap_content)
        val nameCategoryTv: TextView = itemView.findViewById(R.id.text_category_wrap_content)
    }
}