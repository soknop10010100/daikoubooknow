package com.eazy.daikou.ui.home.book_now.booking_hotel

import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.ScrollView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.request_data.request.book_now_ws.BookingHotelWS
import com.eazy.daikou.request_data.request.book_now_ws.RequestHashMapData
import com.eazy.daikou.helper.DateUtil
import com.eazy.daikou.helper.GetDataUtils
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.booking_hotel.HotelBookingHistoryDetailModel
import com.eazy.daikou.model.booking_hotel.HotelBookingHistoryModel
import com.eazy.daikou.ui.home.book_now.booking_hotel.adapter.RoomHisDetailBookedAdapter

class HotelHistoryDetailActivity : BaseActivity() {

    private var hotelId = ""
    private lateinit var progressBar: ProgressBar
    private lateinit var recyclerView: RecyclerView
    private lateinit var btnCancel : TextView
    //use info
    private lateinit var nameUserTv : TextView
    private lateinit var phoneUserTv : TextView
    private lateinit var emailUserTv : TextView
    private lateinit var cityTv : TextView
    private lateinit var countryTv : TextView
    private lateinit var specialRequirements : TextView
    //hotel info
    private lateinit var noTv : TextView
    private lateinit var hotelNameTv : TextView
    private lateinit var phoneHotelTv : TextView
    private lateinit var emailHotelTv : TextView
    private lateinit var nightTv : TextView
    private lateinit var guestTv : TextView
    //room info
    private lateinit var createdDateTv : TextView
    private lateinit var startDateBookingTv : TextView
    private lateinit var endDateTv : TextView
    private lateinit var paymentMethodTv : TextView
    private lateinit var totalPriceTv : TextView
    private lateinit var statusTv : TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hotel_history_detail)

        initView()

        initData()

        initAction()

    }

    private fun initView(){
        progressBar = findViewById(R.id.progressItem)
        btnCancel = findViewById(R.id.btnCancel)
        recyclerView = findViewById(R.id.recyclerView)
        //use info
        nameUserTv = findViewById(R.id.nameUserTv)
        phoneUserTv = findViewById(R.id.phoneUserTv)
        emailUserTv = findViewById(R.id.emailUserTv)
        cityTv = findViewById(R.id.cityTv)

        countryTv = findViewById(R.id.countryTv)
        specialRequirements = findViewById(R.id.specialRequirements)
        //hotel info
        noTv = findViewById(R.id.noTv)
        hotelNameTv = findViewById(R.id.hotelNameTv)
        phoneHotelTv = findViewById(R.id.phoneHotelTv)
        emailHotelTv = findViewById(R.id.emailHotelTv)
        nightTv = findViewById(R.id.nightTv)
        countryTv = findViewById(R.id.countryTv)
        guestTv = findViewById(R.id.guestTv)
        //room info
        createdDateTv = findViewById(R.id.createdDateTv)
        startDateBookingTv = findViewById(R.id.startDateBookingTv)
        endDateTv = findViewById(R.id.endDateTv)
        paymentMethodTv = findViewById(R.id.paymentMethodTv)
        totalPriceTv = findViewById(R.id.totalPriceTv)
        statusTv = findViewById(R.id.statusTv)
    }

    private fun initData(){
        hotelId = GetDataUtils.getDataFromString("hotel_id", this)
    }

    private fun initAction(){
        Utils.customOnToolbar(this, resources.getString(R.string.booking_detail)){finish()}

        requestHotelDetail()

        btnCancel.setOnClickListener {
            Utils.customAlertDialogConfirm(this, resources.getString(R.string.are_you_sure_to_cancel_booking)){
                requestHotelDetail()
            }
        }
    }

    private fun requestHotelDetail(){
        progressBar.visibility = View.VISIBLE
        BookingHotelWS().getHistoryDetailHotelBookingWs(this, hotelId, object : BookingHotelWS.OnCallBackHistoryHotelListener{
            override fun onSuccessHistoryHotel(hotelHistoryList: ArrayList<HotelBookingHistoryModel>) {
                // Get List hotel history
            }

            override fun onSuccessHistoryDetailHotel(hotelHistory: HotelBookingHistoryDetailModel) {
                progressBar.visibility = View.GONE
                findViewById<RelativeLayout>(R.id.mainHoldLayout).visibility = View.VISIBLE
                setValueDetail(hotelHistory)
            }

            override fun onFailed(message: String) {
                progressBar.visibility = View.GONE
                Utils.customToastMsgError(this@HotelHistoryDetailActivity, message, false)
            }
        })
    }

    private fun setValueDetail(hotelHistory: HotelBookingHistoryDetailModel){
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = RoomHisDetailBookedAdapter(hotelHistory.room_booking_history)
        recyclerView.isNestedScrollingEnabled = false
        if (hotelHistory.room_booking_history.size == 0)    findViewById<CardView>(R.id.roomLayout).visibility = View.GONE

        //user info
        var name = ""
        if (hotelHistory.first_name != null){
            name = hotelHistory.first_name!!
        }
        if (hotelHistory.last_name != null){
            name += " " + hotelHistory.last_name!!
        }
        Utils.setValueOnText(nameUserTv, name)
        Utils.setValueOnText(phoneUserTv, hotelHistory.phone)
        Utils.setValueOnText(emailUserTv, hotelHistory.email)
        Utils.setValueOnText(cityTv, hotelHistory.city)
        Utils.setValueOnText(countryTv, hotelHistory.country)
        Utils.setValueOnText(specialRequirements, hotelHistory.customer_notes)
        //hotel info
        Utils.setValueOnText(noTv, hotelHistory.code)
        Utils.setValueOnText(hotelNameTv, hotelHistory.hotel_name)
        Utils.setValueOnText(phoneHotelTv, hotelHistory.hotel_telephone)
        Utils.setValueOnText(emailHotelTv, hotelHistory.hotel_email)
        Utils.setValueOnText(nightTv, hotelHistory.total_night)
        Utils.setValueOnText(guestTv, hotelHistory.total_guests)
        Utils.setValueOnText(totalPriceTv, hotelHistory.total_price_display)
        Utils.setValueOnText(paymentMethodTv, resources.getString(R.string.online_payment))

        //date
        if (hotelHistory.created_at != null){
            createdDateTv.text = DateUtil.formatDateComplaint(hotelHistory.created_at)
        } else {
            createdDateTv.text = ". . ."
        }
        if (hotelHistory.start_date != null){
            startDateBookingTv.text = DateUtil.formatDateComplaint(hotelHistory.start_date)
        } else {
            startDateBookingTv.text = ". . ."
        }
        if (hotelHistory.end_date != null){
            endDateTv.text = DateUtil.formatDateComplaint(hotelHistory.end_date)
        } else {
            endDateTv.text = ". . ."
        }
        //status
        if (hotelHistory.status != null) {
            var statusVal = ""
            var color = R.color.appBarColorOld
            when(hotelHistory.status){
                "pending"->{
                    statusVal = resources.getString(R.string.pending)
                    color = R.color.yellow
                }
                "completed" ->{
                    statusVal = resources.getString(R.string.completed)
                    color = R.color.green
                }
                "cancelled" ->{
                    statusVal = resources.getString(R.string.cancel)
                    color = R.color.red
                }
                "draft" ->{
                    statusVal = resources.getString(R.string.draft)
                    color = R.color.yellow
                }
                "paid" ->{
                    statusVal = resources.getString(R.string.paid)
                    color = R.color.book_now_secondary
                }
                "unpaid" ->{
                    statusVal = resources.getString(R.string.update)
                    color = R.color.red
                }
            }
            Utils.setValueOnText(statusTv, statusVal)
            Utils.setBgTint(statusTv, color)
        } else {
            Utils.setValueOnText(statusTv, ". . .")
        }

        Utils.customOnToolbar(this, String.format("%s %s", RequestHashMapData.categoryTitle(this)[hotelHistory.category], resources.getString(R.string.booking_detail))){finish()}

        btnCancel.visibility = View.GONE
        if (btnCancel.visibility == View.GONE) {
            Utils.setNoMarginBottomOnButton(findViewById<ScrollView>(R.id.mainScroll), 0)
        }
    }
}