package com.eazy.daikou.ui.home.home_menu.adapter

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.home.SubItemHomeModel

class HomeShowItemViewAdapter(private var activity: Activity, private val bookingList: List<SubItemHomeModel>, private val context: Context, private val propertyClick: PropertyClick) :
    RecyclerView.Adapter<HomeShowItemViewAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.home_item_view_model, parent, false)
        val itemViewParam = LinearLayout.LayoutParams(Utils.getWidth(activity) / 2, LinearLayout.LayoutParams.WRAP_CONTENT)
        view.layoutParams = itemViewParam
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val propertyModel = bookingList[position]
        if (propertyModel != null) {
            if (propertyModel.name != null) {
                holder.propertyName.text = propertyModel.name
            } else {
                if(propertyModel.action == "facility_booking"){
                    holder.propertyName.text = String.format("%s : %s", context.getString(R.string.space_name), "N/A")
                } else {
                    holder.propertyName.text = String.format("%s : %s", context.getString(R.string.property_name), "N/A")
                }
            }

            if (propertyModel.detailName != null) {
                if(propertyModel.action == "facility_booking"){
                    holder.address.text = String.format("%s %s", propertyModel.detailName, context.resources.getString(R.string.per_hour))
                } else {
                    holder.address.text = propertyModel.detailName
                }
            } else {
                if(propertyModel.action == "facility_booking"){
                    holder.address.text = String.format("%s : %s", context.getString(R.string.price), "N/A")
                } else {
                    holder.address.text = String.format("%s : %s", context.getString(R.string.address), " N/A")
                }
            }

            Glide.with(holder.iconWishlist).load(if(propertyModel.imageUrl != null)    propertyModel.imageUrl else R.drawable.no_image).into(holder.imageView)

            if (propertyModel.action == "hotel_property"){
                holder.iconWishlist.visibility = View.VISIBLE
                Glide.with(holder.iconWishlist).load(if (propertyModel.isWishlist)    R.drawable.ic_my_favourite else R.drawable.ic_favorite_border).into(holder.iconWishlist)
            } else {
                holder.iconWishlist.visibility = View.GONE
            }

            holder.freeIconLayout.visibility = if (propertyModel.isFreeBooking) View.VISIBLE else View.GONE

            holder.itemView.setOnClickListener {
              propertyClick.onBookingClick(propertyModel)
            }

            val heightImg = Utils.dpToPx(activity, 220)
            if (position % 3 == 0) {
                holder.layoutImage.layoutParams.height = heightImg + Utils.dpToPx(activity, 10)
            } else{
                holder.layoutImage.layoutParams.height = heightImg / 2
            }

        }
    }

    override fun getItemCount(): Int {
        return bookingList.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val propertyName: TextView = itemView.findViewById(R.id.property_name)
        val address: TextView = itemView.findViewById(R.id.buildingAddress)
        val imageView: ImageView = itemView.findViewById(R.id.image)
        var freeIconLayout: RelativeLayout = itemView.findViewById(R.id.freeIconLayout)
        val iconWishlist : ImageView = itemView.findViewById(R.id.iconWishlist)
        var layoutImage : RelativeLayout = itemView.findViewById(R.id.layoutImage)
    }

    interface PropertyClick {
        fun onBookingClick(propertyModel: SubItemHomeModel?)
    }
}