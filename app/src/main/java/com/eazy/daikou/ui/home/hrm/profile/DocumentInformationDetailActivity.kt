package com.eazy.daikou.ui.home.hrm.profile

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.model.hr.Document
import org.w3c.dom.Text

class DocumentInformationDetailActivity : BaseActivity() {

    private lateinit var nameTv: TextView
    private lateinit var issueDateTv: TextView
    private lateinit var startDateTv: TextView

    private lateinit var btnBack: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_document_informayion_detail)

        initView()
        initAction()

    }
    private fun initView(){
        btnBack = findViewById(R.id.btn_back)
        nameTv = findViewById(R.id.nameTv)
        issueDateTv = findViewById(R.id.issueDateTv)
        startDateTv = findViewById(R.id.startDateTv)

    }
    private fun initAction(){
        btnBack.setOnClickListener { finish() }

       if (intent != null && intent.hasExtra("document_detail")){
           val document = intent.getSerializableExtra("document_detail") as Document
           setValue(document)
       }

    }
    private fun setValue(document: Document){

        nameTv.text = document.name
        issueDateTv.text = document.issue_dt
        startDateTv.text = document.start_dt

    }
}