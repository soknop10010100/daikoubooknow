package com.eazy.daikou.ui.home.data_record

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.res.ColorStateList
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.request_data.request.data_record.DataRecordWS
import com.eazy.daikou.helper.DateUtil
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.helper.Utils.setValueOnText
import com.eazy.daikou.model.CustomCategoryModel
import com.eazy.daikou.model.data_record.AllStatus
import com.eazy.daikou.model.data_record.DetailQuotationModel
import com.eazy.daikou.model.data_record.PaymentSchedule
import com.eazy.daikou.model.profile.User
import com.eazy.daikou.ui.CustomMenuHeaderAdapter
import com.eazy.daikou.ui.home.data_record.adapter.AdapterPaymentScheduleDR
import com.eazy.daikou.ui.home.my_property.my_unit.PropertyMyUnitDetailActivity
import com.google.gson.Gson

class DetailQuotationDRActivity : BaseActivity() {

    private lateinit var linearTotalQuotation : LinearLayout
    private lateinit var iconDownPaymentSchedule : ImageView
    private lateinit var linearPaymentSchedule : LinearLayout
    private lateinit var linearDownPaymentSchedule : LinearLayout
    private lateinit var createDateTv : TextView
    private lateinit var customerNameTv : TextView
    private lateinit var customerPhoneTv : TextView
    private lateinit var channelTv : TextView
    private lateinit var supportTeamTv : TextView
    private lateinit var paymentTernTypeTv : TextView
    private lateinit var optionTv : TextView
    private lateinit var discountTv : TextView
    private lateinit var unitNameTv : TextView
    private lateinit var noDataPaymentSchedule : TextView
    private lateinit var progressBar: ProgressBar
    private lateinit var quotationNoTv : TextView
    private lateinit var priorityTv : TextView
    private lateinit var priceTv : TextView
    private lateinit var voucherAmountTv : TextView
    private lateinit var sellingPriceQuotationTv : TextView
    private lateinit var expiredDateTv : TextView
    private lateinit var floorNoTv : TextView
    private lateinit var unitNoTv : TextView
    private lateinit var priceUnitTv : TextView
    private lateinit var bedRoomTv : TextView
    private lateinit var livingRoomTv : TextView
    private lateinit var bathRoomTv : TextView
    private lateinit var maidRoomTv : TextView
    private lateinit var totalAreaTv : TextView
    private lateinit var privateAreaTv : TextView
    private lateinit var commentAreaTv : TextView
    private lateinit var linearQuotation : LinearLayout

    private lateinit var projectNameTv : TextView
    private lateinit var unitTypeTv : TextView
    private lateinit var amountTotalTv : TextView
    private lateinit var intersAmountTotalTv : TextView
    private lateinit var paymentsTotalTv : TextView
    private lateinit var brokerTv : TextView
    private lateinit var agencyTv : TextView

    private var idList = ""
    private var userName = ""
    private var userPhone = ""
    private lateinit var user : User

    private lateinit var menuStepRecycle : RecyclerView
    private lateinit var recyclerPaymentReschedule : RecyclerView
    private lateinit var adapterPaymentSchedule : AdapterPaymentScheduleDR
    private val listPaymentSchedule: ArrayList<PaymentSchedule> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_quotation_dractivity2)

        initView()

        initData()

        initAction()
    }

    private fun initView(){

        brokerTv = findViewById(R.id.brokerTv)
        agencyTv = findViewById(R.id.agencyTv)
        linearPaymentSchedule = findViewById(R.id.linearPaymentSchedule)
        linearDownPaymentSchedule = findViewById(R.id.linearDownPaymentSchedule)
        iconDownPaymentSchedule = findViewById(R.id.iconDownPaymentSchedule)
        createDateTv = findViewById(R.id.createDateTv)
        customerNameTv = findViewById(R.id.customerNameTv)
        customerPhoneTv = findViewById(R.id.customerPhoneTv)
        channelTv = findViewById(R.id.channelTv)
        supportTeamTv = findViewById(R.id.supportTeamTv)
        paymentTernTypeTv = findViewById(R.id.paymentTernTypeTv)
        optionTv = findViewById(R.id.optionTv)
        discountTv = findViewById(R.id.discountTv)
        unitNameTv = findViewById(R.id.unitNameTv)
        projectNameTv = findViewById(R.id.projectNameTv)
        unitTypeTv = findViewById(R.id.unitTypeTv)
        noDataPaymentSchedule = findViewById(R.id.noDataPaymentSchedule)
        quotationNoTv = findViewById(R.id.quotationNoTv)
        priorityTv = findViewById(R.id.priorityTv)
        priceTv = findViewById(R.id.priceTv)
        voucherAmountTv = findViewById(R.id.voucherQuotationTv)
        sellingPriceQuotationTv = findViewById(R.id.sellingPriceQuotationTv)
        floorNoTv = findViewById(R.id.floorNoTv)
        expiredDateTv = findViewById(R.id.expiredDateTv)
        unitNoTv = findViewById(R.id.unitNoTv)
        priceUnitTv = findViewById(R.id.priceUnitTv)
        bedRoomTv = findViewById(R.id.bedRoomTv)
        livingRoomTv = findViewById(R.id.livingRoomTv)
        bathRoomTv = findViewById(R.id.bathRoomTv)
        maidRoomTv = findViewById(R.id.maidRoomTv)
        totalAreaTv = findViewById(R.id.totalAreaTv)
        privateAreaTv = findViewById(R.id.privateAreaTv)
        commentAreaTv = findViewById(R.id.commentAreaTv)
        linearQuotation = findViewById(R.id.linearQuotation)
        progressBar = findViewById(R.id.progressItem)
        recyclerPaymentReschedule = findViewById(R.id.recyclerPaymentReschedule)
        menuStepRecycle = findViewById(R.id.menuStepRecycle)
        amountTotalTv = findViewById(R.id.amountTotalTv)
        intersAmountTotalTv = findViewById(R.id.intersAmountTotalTv)
        paymentsTotalTv = findViewById(R.id.paymentsTotalTv)
        linearTotalQuotation = findViewById(R.id.linearTotalQuotation)
    }

    private fun initData(){
        if (intent != null && intent.hasExtra("id_quotation")){
            idList = intent.getStringExtra("id_quotation") as String
        }

        user = Gson().fromJson(UserSessionManagement(this).userDetail, User::class.java)
        val name = user.name
        val phone = user.phoneNumber

        if (name != null) { userName = name }
        if (phone != null) { userPhone = phone }
    }

    private fun initAction(){
        Utils.customOnToolbar(this, "Quotation  Detail"){ finish() }

        requestServiceAPi()

        linearDownPaymentSchedule.setOnClickListener( CustomSetOnClickViewListener{
            loadViewDropList(recyclerPaymentReschedule, iconDownPaymentSchedule, linearPaymentSchedule)
        })

        val linearLayoutManager = LinearLayoutManager(this)
        recyclerPaymentReschedule.layoutManager = linearLayoutManager
        adapterPaymentSchedule = AdapterPaymentScheduleDR(this, "",listPaymentSchedule, callBackClickInvoice)
        recyclerPaymentReschedule.adapter = adapterPaymentSchedule

    }

    private val callBackClickInvoice = object : AdapterPaymentScheduleDR.CallBackItemClickListener {
        override fun clickItemCallback(paymentSchedule: PaymentSchedule) {}
    }

    private fun setClickUnit(idUnit : String){
        unitTypeTv.setOnClickListener(CustomSetOnClickViewListener{
            val intent = Intent(this@DetailQuotationDRActivity, PropertyMyUnitDetailActivity::class.java)
            intent.putExtra("id", idUnit)
            intent.putExtra("action_type", "data_record_unit")
            startActivity(intent)
        })
    }

    private fun setMenuStepAdapter(context: Activity, listStepMenu : ArrayList<AllStatus>){
        val list = ArrayList<CustomCategoryModel>()
        for (item in listStepMenu){
            list.add(CustomCategoryModel(item.status, item.status_display, item.is_active))
        }

        CustomMenuHeaderAdapter.setMenuStepAdapter(context, "status", list, object : CustomMenuHeaderAdapter.ClickCallBackListener{
            override fun onClickCallBack(item: CustomCategoryModel) {}
        })
    }

    private fun loadViewDropList(recyclerViewDrop: RecyclerView, iconDrop : ImageView, linearData : LinearLayout){
        if(recyclerViewDrop.visibility == View.GONE){
            recyclerViewDrop.visibility = View.VISIBLE
            linearData.visibility = View.VISIBLE
            iconDrop.setImageResource(R.drawable.ic_arrow_next)

        } else {
            recyclerViewDrop.visibility = View.GONE
            iconDrop.setImageResource(R.drawable.ic_drop_down)
            linearData.visibility = View.GONE
        }
    }

    private fun requestServiceAPi(){
        progressBar.visibility = View.VISIBLE
        DataRecordWS().getQuotationDetailWS(this, idList, callBackQuotation )
    }

    private val callBackQuotation = object : DataRecordWS.OnCallBackDetailQuotationListener{
        @SuppressLint("NotifyDataSetChanged")
        override fun onSuccess(detailOpportunity: DetailQuotationModel) {
            progressBar.visibility = View.GONE
            linearQuotation.visibility = View.VISIBLE
            setData(detailOpportunity)

            setMenuStepAdapter(this@DetailQuotationDRActivity, detailOpportunity.all_status)

            if (detailOpportunity.payment_schedules.size > 0){
                listPaymentSchedule.addAll(detailOpportunity.payment_schedules)
                linearTotalQuotation.visibility = View.VISIBLE
            } else {
                noDataPaymentSchedule.visibility = View.VISIBLE
            }
            adapterPaymentSchedule.notifyDataSetChanged()
        }

        override fun onFailed(error: String) {
            progressBar.visibility = View.GONE
            Utils.customToastMsgError(this@DetailQuotationDRActivity, error, false)
        }
    }

    private fun setData(detailQuotation : DetailQuotationModel){

        if (detailQuotation.priority != null){
            priorityTv.text = detailQuotation.priority
            priorityTv.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(this, R.color.blue))
        } else{
            priorityTv.text = ". . ."
        }

        if (detailQuotation.unit != null){
            setClickUnit(detailQuotation.unit!!.id.toString())

            setValueOnText(unitNameTv, detailQuotation.unit!!.name)
            floorNoTv.text = if (detailQuotation.unit!!.floor_no != null) detailQuotation.unit!!.floor_no else ". . ."
            unitNoTv.text = if (detailQuotation.unit!!.name != null) detailQuotation.unit!!.name else " . . ."
            priceUnitTv.text = if (detailQuotation.unit!!.price != null) detailQuotation.unit!!.price else ". . ."

            if (detailQuotation.unit!!.type != null){
                bedRoomTv.text = detailQuotation.unit!!.type!!.total_bedroom
                livingRoomTv.text = if (detailQuotation.unit!!.type!!.total_living_room != null) detailQuotation.unit!!.type!!.total_living_room else ". . ."
                bathRoomTv.text = if (detailQuotation.unit!!.type!!.total_bathroom != null) detailQuotation.unit!!.type!!.total_bathroom else ". . ."
                maidRoomTv.text =  if (detailQuotation.unit!!.type!!.total_maid_room != null) detailQuotation.unit!!.type!!.total_maid_room else ". . ."
                totalAreaTv.text = if (detailQuotation.unit!!.type!!.total_area_sqm != null) detailQuotation.unit!!.type!!.total_area_sqm else ". . ."
                privateAreaTv.text = if (detailQuotation.unit!!.type!!.total_private_area != null) detailQuotation.unit!!.type!!.total_private_area else ". . ."
                commentAreaTv.text = if (detailQuotation.unit!!.type!!.total_common_area != null) detailQuotation.unit!!.type!!.total_common_area else ". . ."
                setValueOnText(unitTypeTv, detailQuotation.unit!!.type!!.name)
            } else{
                bedRoomTv.text = ". . ."
                livingRoomTv.text = ". . ."
                bathRoomTv.text = ". . ."
                maidRoomTv.text =  ". . ."
                totalAreaTv.text = ". . ."
                privateAreaTv.text = ". . ."
                commentAreaTv.text = ". . ."
                unitTypeTv.text =  ". . ."
            }

        } else {
            unitNameTv.text = ". . ."
            floorNoTv.text = ". . ."
            unitNoTv.text = ". . ."
            priceUnitTv.text = ". . ."
        }

        setValueOnText(projectNameTv, if(detailQuotation.account != null) detailQuotation.account!!.name else ". . .")

        setValueOnText(createDateTv, String.format("%s %s", DateUtil.formatDateComplaint(detailQuotation.created_at), DateUtil.formatDateToConvertOnlyTimeZoneNoSecond(detailQuotation.created_at)))
        setValueOnText(quotationNoTv, detailQuotation.no)
        setValueOnText(customerNameTv, userName)
        setValueOnText(customerPhoneTv, userPhone)
        setValueOnText(channelTv, detailQuotation.channel_name)
        setValueOnText(agencyTv, detailQuotation.agency_name)
        setValueOnText(brokerTv, detailQuotation.broker_name)
        setValueOnText(supportTeamTv,detailQuotation.supporting_team)
        setValueOnText(paymentTernTypeTv, detailQuotation.payment_term_type)
        setValueOnText(optionTv, detailQuotation.option)
        setValueOnText(priceTv, detailQuotation.price)
        setValueOnText(voucherAmountTv, detailQuotation.voucher_amount)
        setValueOnText(discountTv, detailQuotation.discount)
        setValueOnText(sellingPriceQuotationTv, detailQuotation.sale_price)
        setValueOnText(amountTotalTv, detailQuotation.total_amount)
        setValueOnText(intersAmountTotalTv, detailQuotation.total_interest_amount)
        setValueOnText(paymentsTotalTv, detailQuotation.total_payment_amount)

        expiredDateTv.text = if (detailQuotation.expire_date != null)  Utils.formatDateFromString("yyyy-MM-dd", "dd-MMM-yyyy", detailQuotation.expire_date) else ". . ."

    }
}