package com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.adapter_v2

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.model.my_property.cleaning.ServiceCategories

class ServiceCleaningServiceCategoryAdapter(private val context: Context, private val listCategory: ArrayList<ServiceCategories>, private val callBackItem: CallBackItemCategoryListener): RecyclerView.Adapter<ServiceCleaningServiceCategoryAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.custom_view_text_view_layout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val serviceCategoryList : ServiceCategories = listCategory[position]
        if (serviceCategoryList != null){
            if (serviceCategoryList.category_name != null){
                holder.itemNameTv.text = String.format("%s", serviceCategoryList.category_name)
            } else {
                holder.itemNameTv.text = "- - -"
            }
            holder.checkClick.visibility = if(serviceCategoryList.isClick) View.VISIBLE else View.INVISIBLE
            holder.line.visibility = if((listCategory.size - 1) == position) View.GONE else View.VISIBLE

            holder.itemView.setOnClickListener { callBackItem.callBackItem(serviceCategoryList) }
        }
    }

    override fun getItemCount(): Int {
        return listCategory.size
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val itemNameTv : TextView = itemView.findViewById(R.id.item_name)
        val checkClick : ImageView = itemView.findViewById(R.id.checkClick)
        val line : View = itemView.findViewById(R.id.line)
    }

    interface CallBackItemCategoryListener{
        fun callBackItem(serviceCategoryList : ServiceCategories)
    }
}