package com.eazy.daikou.ui.home.book_now.booking_hotel

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.request_data.request.book_now_ws.BookingHotelWS
import com.eazy.daikou.request_data.request.book_now_ws.RequestHashMapData
import com.eazy.daikou.request_data.static_data.StaticUtilsKey
import com.eazy.daikou.helper.AbsoluteFitLayoutManager
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.helper.Utils.spanHorizentalGridLayoutManager
import com.eazy.daikou.helper.map.MapsGenerateClass
import com.eazy.daikou.model.booking_hotel.HotelLocationDetailModel
import com.eazy.daikou.model.booking_hotel.LocationHotelModel
import com.eazy.daikou.model.booking_hotel.NearbyHotelLocationModel
import com.eazy.daikou.model.home.SubItemHomeModel
import com.eazy.daikou.ui.home.book_now.booking_hotel.adapter.BookingHomeItemAdapter
import com.eazy.daikou.ui.home.book_now.booking_hotel.adapter.HotelCategoryItemAdapter
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.appbar.AppBarLayout.Behavior.DragCallback
import java.util.*
import kotlin.collections.ArrayList


class HotelTopDestinationDetailActivity : BaseActivity(), OnMapReadyCallback {

    private lateinit var mapFragment : SupportMapFragment
    private lateinit var layoutMaps : RelativeLayout
    private var subHomeModelCategoryList: ArrayList<SubItemHomeModel> = ArrayList()
    private lateinit var recyclerViewCategory : RecyclerView

    private lateinit var bookingHomeItemAdapter: BookingHomeItemAdapter
    private lateinit var recyclerViewItem: RecyclerView
    private var locationHotelModelList: ArrayList<LocationHotelModel> = ArrayList()
    private var locationId = ""
    private lateinit var titleToolbar : TextView
    private lateinit var locationName : TextView
    private lateinit var recyclerViewNearby: RecyclerView
    private lateinit var progressBar : ProgressBar
    private lateinit var descriptionTv : TextView

    // Init maps
    private var mMap: GoogleMap? = null
    private val hashMapLatLng : HashMap<String, Any> = HashMap()
    private var category = "hotel"
    private var locationList : ArrayList<SubItemHomeModel> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hotel_top_destination_detail)

        initView()

        initAction()

    }

    private fun initView(){
        recyclerViewCategory = findViewById(R.id.recyclerView)
        layoutMaps = findViewById(R.id.layoutIMaps)
        layoutMaps.layoutParams.height = Utils.setHeightOfScreen(this, 1.35)

        recyclerViewItem = findViewById(R.id.recyclerViewItem)
        titleToolbar = findViewById(R.id.titleToolbar)
        locationName = findViewById(R.id.locationName)
        recyclerViewNearby = findViewById(R.id.recyclerViewTest)
        progressBar = findViewById(R.id.progressItem)
        descriptionTv = findViewById(R.id.descriptionTv)

        findViewById<ImageView>(R.id.iconBack).setOnClickListener { finish() }

        val hashMapData: HashMap<String, Any> = RequestHashMapData.getStoreHotelData()
        if (hashMapData != null){
            titleToolbar.text = hashMapData["location_title"].toString()
            locationName.text = hashMapData["location_title"].toString()
            locationId = hashMapData["location_id"].toString()
        }
        category = StaticUtilsKey.category
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun initAction(){
        // init maps
        initMapsLayout()

        // init category
        subHomeModelCategoryList = RequestHashMapData.addCategoryItemList(resources)

        initRecyclerViewCategory()

        //init item
        initItemHotel()

        locationHotelModelList = RequestHashMapData.getStoreLocationHotelData()
        setClickItemCategory(category)

        titleToolbar.setOnClickListener(CustomSetOnClickViewListener{ startSelectCategory("select_location") })

        //set drag on map
        val appBarLayout = findViewById<View>(R.id.appBarLayout) as AppBarLayout
        val params = appBarLayout.layoutParams as CoordinatorLayout.LayoutParams
        val behavior = AppBarLayout.Behavior()
        params.behavior = behavior
        behavior.setDragCallback(object : DragCallback() {
            override fun canDrag(appBarLayout: AppBarLayout): Boolean {
                return false
            }
        })
    }

    private fun initRecyclerViewCategory(){
        recyclerViewCategory.isNestedScrollingEnabled = false
        recyclerViewCategory.layoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
        var bookingHomeItemAdapter : HotelCategoryItemAdapter? = null
        bookingHomeItemAdapter = HotelCategoryItemAdapter(false, subHomeModelCategoryList, object : BookingHomeItemAdapter.PropertyClick{
            @SuppressLint("NotifyDataSetChanged")
            override fun onBookingClick(propertyModel: SubItemHomeModel?) {
                if (propertyModel != null) {
                    setClickItemCategory(propertyModel.id!!)
                }
                bookingHomeItemAdapter?.notifyDataSetChanged()

                if (propertyModel != null) {
                    category = propertyModel.id!!
                    initServicePropertyList()
                }
            }

            override fun onBookingWishlistClick(propertyModel: SubItemHomeModel?) {

            }

        })
        recyclerViewCategory.adapter = bookingHomeItemAdapter
    }

    private fun setClickItemCategory(categoryId : String){
        for (itemSub in subHomeModelCategoryList){
            itemSub.isClickCategory = itemSub.id == categoryId
        }
    }

    private fun initItemHotel(){
        recyclerViewItem.layoutManager = spanHorizentalGridLayoutManager(this, true)
        recyclerViewNearby.layoutManager =
            AbsoluteFitLayoutManager(
                this,
                1,
                RecyclerView.HORIZONTAL,
                false,
                2
            )

        initServicePropertyList()
    }

    private fun initRecyclerViewItem(action: String, recyclerView: RecyclerView, itemList : ArrayList<SubItemHomeModel>){
        bookingHomeItemAdapter = BookingHomeItemAdapter(this, action, itemList, onClickSubItemBookingListener)
        recyclerView.adapter = bookingHomeItemAdapter
    }

    private var onClickSubItemBookingListener = object : BookingHomeItemAdapter.PropertyClick {
        override fun onBookingClick(propertyModel: SubItemHomeModel?) {
            if (propertyModel != null) {
                val intent = Intent(this@HotelTopDestinationDetailActivity, HotelBookingDetailHotelActivity::class.java)
                intent.putExtra("hotel_name", propertyModel.name)
                intent.putExtra("category", propertyModel.titleCategory)
                intent.putExtra("hotel_image", propertyModel.imageUrl)
                intent.putExtra("hotel_id", propertyModel.id)
                intent.putExtra("price", propertyModel.priceVal)
                intent.putExtra("start_date", "")
                intent.putExtra("end_date", "")
                intent.putExtra("adults", "")
                intent.putExtra("children", "")
                resultLauncher.launch(intent)
            }
        }

        override fun onBookingWishlistClick(propertyModel: SubItemHomeModel?) {

        }
    }

    private val resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
        if (result.resultCode == Activity.RESULT_OK) {
            //Call back when click wishlist hotel on detail screen
            initItemHotel()
        }
    }

    private fun startSelectCategory(action : String){
        val selectLocationBookingFragment : SelectLocationBookingFragment =  SelectLocationBookingFragment().newInstance(locationId, action, locationHotelModelList)
        selectLocationBookingFragment.initListener(onCallBackListener)
        selectLocationBookingFragment.show(supportFragmentManager, "fragment")
    }

    private val onCallBackListener = object : SelectLocationBookingFragment.OnClickCallBackListener{
        override fun onClickSelectLocation(titleHeader: LocationHotelModel) {
            titleToolbar.text = titleHeader.location_name
            locationName.text = titleHeader.location_name
            locationId = titleHeader.location_id.toString()

            initServicePropertyList()
        }

        override fun onClickSelect(titleHeader: String, r: String, ad: String, ch: String) { }
    }

    // Init Maps
    private fun initMapsLayout() {
        mapFragment = (supportFragmentManager.findFragmentById(R.id.google_map) as SupportMapFragment?)!!
        mapFragment.getMapAsync(this)
    }

    private fun initServicePropertyList() {
        progressBar.visibility = View.VISIBLE
        BookingHotelWS().getLocationDetailWs(this, locationId, category, latitude.toString(), longitude.toString(), object :
            BookingHotelWS.OnCallBackLocationDetailListener{
            override fun onSuccess(locationDetail: HotelLocationDetailModel, hashMap: HashMap<String, ArrayList<SubItemHomeModel>>) {
                progressBar.visibility = View.GONE

                //Set Value
                Utils.setTextHtml(findViewById(R.id.descriptionTv), locationDetail.description)

                if (hashMap.containsKey("total_places")) {
                    locationList.clear()
                    locationList = hashMap["total_places"]!!
                    addMarker(hashMap["total_places"]!!)
                    initRecyclerViewItem("detail_all_location", recyclerViewItem, hashMap["total_places"]!!)
                }

                if (hashMap.containsKey("nearby")) {
                    initRecyclerViewItem("bestseller_listing", recyclerViewNearby, hashMap["nearby"]!!)
                }
            }

            override fun onFailed(message: String) {
                progressBar.visibility = View.GONE
                Utils.customToastMsgError(this@HotelTopDestinationDetailActivity, message, false)
            }

        })
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap!!.mapType = GoogleMap.MAP_TYPE_NORMAL

        zoomMap(latitude, longitude)

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }

        mMap!!.isMyLocationEnabled = true
        mMap!!.uiSettings.isZoomControlsEnabled = false
        mMap!!.uiSettings.isMyLocationButtonEnabled = true
        mMap!!.uiSettings.isCompassEnabled = false

        mMap!!.setOnMarkerClickListener { marker: Marker ->
            try {
                val subItemHomeModel = hashMapLatLng[marker.title] as SubItemHomeModel
                val locationModel = NearbyHotelLocationModel()

                locationModel.id = subItemHomeModel.id
                locationModel.name = subItemHomeModel.name
                locationModel.star_rate = subItemHomeModel.rating
                locationModel.price_display = subItemHomeModel.priceVal
                locationModel.original_price_display = subItemHomeModel.priceOriginalBeforeDiscount
                locationModel.image = subItemHomeModel.imageUrl
                locationModel.distance = subItemHomeModel.distance
                locationModel.is_wishlist = subItemHomeModel.isWishlist

                val bttSheet = HotelDetailNearbyLocFragment.newInstance(category, locationModel)
                bttSheet.show(supportFragmentManager, "fragment")
            } catch (ex : Exception){
                Utils.logDebug("Jeeeeeeeeeeeee", ex.message)
            }
            true
        }

    }

    private fun zoomMap(lat: Double, lng: Double) {
        mMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(lat, lng), 15f))
    }

    private fun addMarker(itemList : ArrayList<SubItemHomeModel>) {
        if (mMap == null)   return

        mMap!!.clear()
        for (latLongPropertyModel in itemList) {
            drawPLaceOnMap(latLongPropertyModel)
        }
    }

    private fun drawPLaceOnMap(latLongPropertyModel: SubItemHomeModel) {
        if (latLongPropertyModel.coordLat != null && latLongPropertyModel.coordLng != null) {
            try {
                hashMapLatLng[latLongPropertyModel.id!!] = latLongPropertyModel
                val latLng = LatLng(latLongPropertyModel.coordLat!!.toDouble(), latLongPropertyModel.coordLng!!.toDouble())
                mMap!!.addMarker(
                    MarkerOptions().title(latLongPropertyModel.id!!).position(latLng).icon(
                        BitmapDescriptorFactory.fromBitmap(
                            MapsGenerateClass.setMarkerBitmapFromView(this, latLongPropertyModel.imageUrl!!, latLongPropertyModel.priceVal!!)!!
                        )
                    )
                )
            } catch (exception: Exception) {
                exception.printStackTrace()
            }
        }
    }
}