package com.eazy.daikou.ui.home.book_now.booking_hotel.travel_article

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AbsListView
import android.widget.ProgressBar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.request_data.request.book_now_ws.BookingHotelWS
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.booking_hotel.HotelArticleModel
import com.eazy.daikou.ui.home.book_now.booking_hotel.adapter.HotelArticlesAdapter

class HotelArticlesActivity : BaseActivity() {

    private lateinit var progressBar : ProgressBar
    private lateinit var refreshLayout: SwipeRefreshLayout
    private lateinit var recyclerView: RecyclerView
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var hotelArticlesAdapter: HotelArticlesAdapter

    private var currentItem = 0
    private var total : Int = 0
    private var scrollDown : Int = 0
    private var currentPage : Int = 1
    private var size : Int = 4
    private var isScrolling = false
    private val hotelArticleList : ArrayList<HotelArticleModel> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hotel_articles)

        initView()

        initAction()

    }

    private fun initView(){
        progressBar = findViewById(R.id.progressItem)
        refreshLayout = findViewById(R.id.swipe_layouts)
        recyclerView = findViewById(R.id.recyclerView)

        Utils.customOnToolbar(this, resources.getString(R.string.travel_articles)){finish()}
    }

    private fun initAction(){
        refreshLayout.setColorSchemeResources(R.color.appBarColorOld)
        refreshLayout.setOnRefreshListener { refreshList() }

        linearLayoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = linearLayoutManager

        initItemRecycleView()

        onScrollInfoRecycle()
    }

    private fun refreshList(){
        currentPage = 1
        size = 4
        isScrolling = true

        hotelArticlesAdapter.clear()
        requestServiceWs()
    }

    private fun requestServiceWs(){
        progressBar.visibility = View.VISIBLE
        BookingHotelWS().getTravelArticleWs(this, currentPage, 10, object : BookingHotelWS.OnCallBackTravelArticleListener{
           @SuppressLint("NotifyDataSetChanged")
           override fun onSuccessArticleList(hotelDashboardModel: ArrayList<HotelArticleModel>) {
               progressBar.visibility = View.GONE
               refreshLayout.isRefreshing = false
               hotelArticleList.addAll(hotelDashboardModel)

               Utils.validateViewNoItemFound(this@HotelArticlesActivity, recyclerView, hotelArticleList.size <= 0)

               hotelArticlesAdapter.notifyDataSetChanged()
           }

           override fun onSuccessArticleDetail(hotelDashboardModel: HotelArticleModel) {}

           override fun onFailed(message: String) {
               progressBar.visibility = View.GONE
               refreshLayout.isRefreshing = false
               Utils.customToastMsgError(this@HotelArticlesActivity, message, false)
           }
       })
    }

    private fun initItemRecycleView(){
        progressBar.visibility = View.GONE
        hotelArticlesAdapter = HotelArticlesAdapter(hotelArticleList, object : HotelArticlesAdapter.OnClickItemListener{
            @SuppressLint("NotifyDataSetChanged")
            override fun onClickListener(hotelArticleModel: HotelArticleModel, action: String) {
                if (action == "share_article"){
                    Utils.shareLinkMultipleOption(this@HotelArticlesActivity, hotelArticleModel.url, hotelArticleModel.title)
                } else { // Detail
                    val intent = Intent(this@HotelArticlesActivity, HotelArticlesDetailActivity::class.java)
                    intent.putExtra("hotel_articles_id", hotelArticleModel.id)
                    startActivity(intent)

                    //set viewer number
                    var viewCount = if (hotelArticleModel.view != null) hotelArticleModel.view!!.toInt() else 0
                    viewCount++
                    hotelArticleModel.view = viewCount.toString()
                    hotelArticlesAdapter.notifyDataSetChanged()
                }
            }
        })
        recyclerView.adapter = hotelArticlesAdapter

        requestServiceWs()
    }

    private fun onScrollInfoRecycle() {
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true
                }
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                currentItem = linearLayoutManager.childCount
                total = linearLayoutManager.itemCount
                scrollDown = linearLayoutManager.findFirstVisibleItemPosition()
                if (dy > 0) {
                    if (isScrolling && currentItem + scrollDown == total) {
                        if (total == size) {
                            isScrolling = false
                            currentPage++
                            progressBar.visibility = View.VISIBLE
                            recyclerView.scrollToPosition(hotelArticleList.size - 1)
                            initItemRecycleView()
                            size += 4
                        } else {
                            progressBar.visibility = View.GONE
                        }
                    }
                }
            }
        })
    }

}