package com.eazy.daikou.ui.home.inspection_work_order;

import static com.eazy.daikou.helper.Utils.convert;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.eazy.daikou.R;
import com.eazy.daikou.base.BaseActivity;
import com.eazy.daikou.base.BaseCameraActivity;
import com.eazy.daikou.request_data.request.work_oder_ws.WorkOrderWs;
import com.eazy.daikou.request_data.static_data.StaticUtilsKey;
import com.eazy.daikou.helper.UserSessionManagement;
import com.eazy.daikou.helper.Utils;
import com.eazy.daikou.model.work_order.ImageListModel;
import com.eazy.daikou.model.work_order.WorkOrderDetailModel;
import com.eazy.daikou.model.work_order.WorkOrderItemModel;
import com.eazy.daikou.model.work_order.WorkOrderProgressModel;
import com.eazy.daikou.ui.home.complaint_solution.complaint.ComplaintDetailActivity;
import com.eazy.daikou.ui.home.front_desk.ListImageFrontDeskAdapter;
import com.eazy.daikou.ui.home.inspection_work_order.adapter.ImageListWorkOrderAdapter;
import com.eazy.daikou.ui.home.inspection_work_order.adapter.WorkProgressListAdapter;
import com.eazy.daikou.helper.DateNumPickerFragment;
import com.eazy.daikou.ui.sign_up.NationalityActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class WorkOrderDetailActivity extends BaseActivity implements View.OnClickListener {

    private TextView btnSave;
    private TextView txtImageCount;
    private TextView subjectEdt;
    private TextView noTv;
    private TextView startDateTv;
    private TextView dueDateTv;
    private TextView departmentTv;
    private TextView assigneeTv;
    private TextView doneStatusTv;
    private TextView txtNoImage;
    private TextView noItemTv;
    private TextView txtNoPhotoTv;
    private TextView statusTv;
    private TextView priorityTv;
    private TextView estimateTimeTv;
    private EditText totalComponentListEdt;
    private EditText descriptionTv;
    private ProgressBar progressBar;
    private ViewPager viewPager;
    private Button detailMenuTv;
    private Button workProgressMenuTv;
    private RelativeLayout detailItemLayout;
    private LinearLayout workProgressLayout;
    private LinearLayout layoutMenuParentTask;
    private WorkOrderDetailModel workOrderDetailModel;
    private ImageView btnAddImage;
    private ImageView btnCreateWorkOrder;
    private ImageView iconMenu;
    private RecyclerView imageRecyclerView;
    private RecyclerView workProgressRecycle;

    private final List<ImageListModel> bitmapList = new ArrayList<>();
    private final HashMap<String, String> hashMapImageList =new HashMap<>();

    private final int REQUEST_IMAGE = 562;
    private final int REQUEST_CODE_ESTIMATE_TIME = 442;
    private final int REQUEST_CREATE_UPDATE_WORK_ORDER = 754;
    private int post = 0 ;
    private boolean isFirstViewImage = true;
    private String getStatusVal = "";
    private String getPriorityVal = "";
    private String getEstimateTimeVal = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_work_order_detail);

        initView();

        initData();

        initAction();

    }

    private void initView(){
        TextView titleToolbar = findViewById(R.id.titleToolbar);
        findViewById(R.id.iconBack).setOnClickListener(view -> finish());
        titleToolbar.setText(getResources().getString(R.string.word_order_detail).toUpperCase(Locale.ROOT));
        progressBar = findViewById(R.id.progressItem);
        progressBar.setVisibility(View.GONE);
        layoutMenuParentTask = findViewById(R.id.layoutMap);

        iconMenu = findViewById(R.id.addressMap);
        iconMenu.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_about));
        btnSave = findViewById(R.id.btnSave);
        viewPager = findViewById(R.id.viewpagers);
        txtImageCount = findViewById(R.id.txtImageCount);
        btnAddImage = findViewById(R.id.btnAddImage);
        imageRecyclerView  = findViewById(R.id.list_image);
        txtNoImage = findViewById(R.id.txtNoImageAdd);
        detailMenuTv = findViewById(R.id.detailMenuTv);
        workProgressMenuTv = findViewById(R.id.workProgressMenuTv);
        detailItemLayout = findViewById(R.id.DetailItemLayout);
        btnCreateWorkOrder = findViewById(R.id.btnCreateWorkOrder);
        workProgressLayout = findViewById(R.id.workProgressLayout);
        workProgressRecycle = findViewById(R.id.workProgressRecycle);
        noItemTv = findViewById(R.id.noItemTv);
        txtNoPhotoTv = findViewById(R.id.txtNoPhoto);

        subjectEdt = findViewById(R.id.subjectEdt);
        noTv = findViewById(R.id.noTv);
        startDateTv = findViewById(R.id.startDateTv);
        dueDateTv = findViewById(R.id.dueDateTv);
        departmentTv = findViewById(R.id.departmentTv);
        assigneeTv = findViewById(R.id.assigneeTv);
        doneStatusTv = findViewById(R.id.doneStatusTv);
        totalComponentListEdt = findViewById(R.id.totalComponentListEdt);
        descriptionTv = findViewById(R.id.descriptionTv);
        statusTv = findViewById(R.id.statusTv);
        priorityTv = findViewById(R.id.priorityTv);
        estimateTimeTv = findViewById(R.id.estimateTimeTv);
    }

    private void initData(){
        if (getIntent() != null && getIntent().hasExtra("work_order_detail")){
            workOrderDetailModel = (WorkOrderDetailModel) getIntent().getSerializableExtra("work_order_detail");
        }
        if (workOrderDetailModel.getStatus() != null)   getStatusVal = Utils.getValueFromKeyStatus(this).get(workOrderDetailModel.getStatus());
        if (workOrderDetailModel.getPriority() != null)   getPriorityVal = Utils.getPriorityOnWorkOrder(this).get(workOrderDetailModel.getPriority());
        if (workOrderDetailModel.getEstimateDuration() != null)   getEstimateTimeVal = workOrderDetailModel.getEstimateDuration();

        for (String image : workOrderDetailModel.getWorkOrderImages()) {
            String convertUrlToBitmap = Utils.convertUrlToBase64(image);
            hashMapImageList.put(post + "", image);
            addUrlImage(post + "", convertUrlToBitmap);
        }

        layoutMenuParentTask.setVisibility(workOrderDetailModel.getWorkOrderType().equalsIgnoreCase("other") ? View.INVISIBLE : View.VISIBLE);

    }

    private void initAction(){
        iconMenu.setOnClickListener(this);
        btnSave.setOnClickListener(this);
        btnAddImage.setOnClickListener(this);
        detailMenuTv.setOnClickListener(this);
        workProgressMenuTv.setOnClickListener(this);
        btnCreateWorkOrder.setOnClickListener(this);
        statusTv.setOnClickListener(this);
        priorityTv.setOnClickListener(this);
        estimateTimeTv.setOnClickListener(this);
        subjectEdt.setOnClickListener(this);

        setValue();

        loadImage();

        setImageRecyclerView(post);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(WorkOrderDetailActivity.this);
        workProgressRecycle.setLayoutManager(linearLayoutManager);

        initRecycleViewWorkOrder();
    }

    private void setValue(){
        subjectEdt.setText(workOrderDetailModel.getSubject() != null ? workOrderDetailModel.getSubject() : ". . .");
        noTv.setText(workOrderDetailModel.getWorkOrderNo() != null ? workOrderDetailModel.getWorkOrderNo() : ". . .");
        departmentTv.setText(workOrderDetailModel.getDepartmentName() != null ? workOrderDetailModel.getDepartmentName() : ". . .");
        if (workOrderDetailModel.getAssigneeUserInfo() != null) {
            assigneeTv.setText(workOrderDetailModel.getAssigneeUserInfo().getFullName() != null ? workOrderDetailModel.getAssigneeUserInfo().getFullName() : ". . .");
        }

        startDateTv.setText(workOrderDetailModel.getStartDate() != null ? Utils.formatDateFromString("yyyy-MM-dd", "dd, MMM yyyy", workOrderDetailModel.getStartDate()) : ". . .");
        dueDateTv.setText(workOrderDetailModel.getDueDate() != null ?  Utils.formatDateFromString("yyyy-MM-dd", "dd, MMM yyyy", workOrderDetailModel.getDueDate()) : ". . .");

        if (workOrderDetailModel.getComponentList() != null)    totalComponentListEdt.setText(workOrderDetailModel.getComponentList());

        if (workOrderDetailModel.getDescription() != null)  descriptionTv.setText(workOrderDetailModel.getDescription());

        statusTv.setText(!getStatusVal.equals("") ? getStatusVal : Utils.getText(this, R.string.status));

        priorityTv.setText(getPriorityVal != null && !getPriorityVal.equals("") ? getPriorityVal : Utils.getText(this, R.string.priority));

        estimateTimeTv.setText(!getEstimateTimeVal.equals("") ? getEstimateTimeVal + " " + Utils.getText(this, R.string.hour).toLowerCase(Locale.ROOT) : Utils.getText(this, R.string.estimate_time));

        setValueOnDonePercent(workOrderDetailModel.getDonePercent());
    }

    private void setValueOnDonePercent(String donePercent){
        doneStatusTv.setText(donePercent != null ? donePercent : ". . .");
        assert donePercent != null;
        doneStatusTv.setBackgroundTintList(Utils.setDoneStatusPercentOnWorkOrder(this, donePercent) != null ?
                Utils.setDoneStatusPercentOnWorkOrder(this, donePercent) : null);
    }


    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnSave:
                HashMap<String, Object> hashMap = hashMapGetData();
                progressBar.setVisibility(View.GONE);
                new WorkOrderWs().saveWorkOrderDetail(this, "save_work_order_detail", hashMap, callBackWorkOrderListener);
                break;
            case R.id.addressMap:
                startParentTaskActivity();
                break;
            case R.id.btnAddImage:
                startActivityForResult(new Intent(WorkOrderDetailActivity.this, BaseCameraActivity.class), REQUEST_IMAGE);
                break;
            case R.id.detailMenuTv:
                setBackgroundMenu(true);
                break;
            case R.id.workProgressMenuTv:
                setBackgroundMenu(false);
                break;
            case R.id.btnCreateWorkOrder:
                startWorkOrderAction("create_new_work_order", null, workOrderDetailModel.getWorkOrderId());
                break;
            case R.id.statusTv:
                selectStatusPriorityDuring("status_work_order", getStatusVal);
                break;
            case R.id.priorityTv:
                selectStatusPriorityDuring("priority_work_order", getPriorityVal);
                break;
            case R.id.estimateTimeTv:
                Intent intent = new Intent(WorkOrderDetailActivity.this, NationalityActivity.class);
                intent.putExtra("action", "estimate_time");
                startActivityForResult(intent, REQUEST_CODE_ESTIMATE_TIME);
                break;
            case R.id.subjectEdt:
                if (workOrderDetailModel.getSubject() != null) {
                    Utils.popupMessage(getResources().getString(R.string.subject), workOrderDetailModel.getSubject(), WorkOrderDetailActivity.this);
                }
                break;
        }
    }

    private void startParentTaskActivity(){
        Intent intent;
        if (workOrderDetailModel.getWorkOrderType().equalsIgnoreCase("complain")){
            intent = new Intent(WorkOrderDetailActivity.this, ComplaintDetailActivity.class);
            intent.putExtra("id", workOrderDetailModel.getParent_task_id());
        } else {
            intent = new Intent(WorkOrderDetailActivity.this, ParentTaskWorkOrderActivity.class);
            intent.putExtra("parent_task_id", workOrderDetailModel.getParent_task_id());
        }
        startActivity(intent);
    }

    private void selectStatusPriorityDuring(String action, String value) {
        DateNumPickerFragment dateNumPickerFragment = DateNumPickerFragment.newInstance(action, value);
        dateNumPickerFragment.CallBackListener((title, values, id) -> {
            if(title.equals("status_work_order")){
                statusTv.setText(values);
                getStatusVal = values;
            } else if (title.equals("priority_work_order")) {
                priorityTv.setText(values);
                getPriorityVal = values;
            }
        });
        dateNumPickerFragment.show(getSupportFragmentManager(), "Dialog Fragment");
    }

    private Integer getPriority(HashMap<Integer, String> hashMap, String value){
        for (Map.Entry<Integer, String> map : hashMap.entrySet()){
            if (value.equals(map.getValue())){
                return map.getKey();
            }
        }
        return 1;
    }

    private String getStatus(HashMap<String, String> hashMap, String value){
        for (Map.Entry<String, String> map : hashMap.entrySet()){
            if (value.equals(map.getValue())){
                return map.getKey();
            }
        }
        return "";
    }

    private void setBackgroundMenu(boolean isClickDetailMenu){
        Utils.setBackgroundTintView(this, detailMenuTv, isClickDetailMenu ? R.color.color_gray_item : R.color.color_un_select_item);
        Utils.setBackgroundTintView(this, workProgressMenuTv, isClickDetailMenu ? R.color.color_un_select_item : R.color.color_gray_item);
        detailMenuTv.setTextColor(Utils.getColor(this, isClickDetailMenu ? R.color.white : R.color.gray));
        workProgressMenuTv.setTextColor(Utils.getColor(this, isClickDetailMenu ? R.color.gray : R.color.white));

        btnCreateWorkOrder.setVisibility(checkHideBtnAdd(isClickDetailMenu, workOrderDetailModel.isHideButtonAdd()));

        detailItemLayout.setVisibility(isClickDetailMenu ? View.VISIBLE : View.GONE);
        workProgressLayout.setVisibility(isClickDetailMenu ? View.GONE : View.VISIBLE);
    }

    private int checkHideBtnAdd(boolean isClickDetailMenu, boolean isHideBtnAdd){
        if (!isClickDetailMenu && !isHideBtnAdd){
            return View.VISIBLE;
        } else {
            return View.GONE;
        }
    }

    private void loadImage() {
        ListImageFrontDeskAdapter listImageAdapter = new ListImageFrontDeskAdapter(getSupportFragmentManager(), workOrderDetailModel.getItemImages());
        viewPager.setAdapter(listImageAdapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                if (isFirstViewImage){
                    txtImageCount.setText((position + 1) + "/" + workOrderDetailModel.getItemImages().size());
                    isFirstViewImage = false;
                }
            }

            @SuppressLint("SetTextI18n")
            @Override
            public void onPageSelected(int position) {
                txtImageCount.setText((position + 1) + "/" + workOrderDetailModel.getItemImages().size());
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        viewPager.setVisibility(workOrderDetailModel.getItemImages().size() > 0 ? View.VISIBLE : View.GONE);
        txtNoPhotoTv.setVisibility(workOrderDetailModel.getItemImages().size() == 0 ? View.VISIBLE : View.GONE);
        txtImageCount.setVisibility(workOrderDetailModel.getItemImages().size() == 0 ? View.GONE : View.VISIBLE);

        listImageAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == REQUEST_IMAGE ){
            assert data != null;
            if(data.hasExtra("image_path")){
                String imagePath = data.getStringExtra("image_path");
                addUrlImage("", imagePath);
                setImageRecyclerView(post);
            }
            if(data.hasExtra("select_image")){
                String imagePath = data.getStringExtra("select_image");
                addUrlImage("", imagePath);
                setImageRecyclerView(post);
            }

        } else if (resultCode == RESULT_OK && requestCode == REQUEST_CODE_ESTIMATE_TIME ){
            assert data != null;
            String valueEstimateTime = data.getStringExtra("estimate_time");
            String estimateTimeTittle = data.getStringExtra("estimate_time_name");
            estimateTimeTv.setText(estimateTimeTittle);
            getEstimateTimeVal = valueEstimateTime;
        } else if (resultCode == RESULT_OK && requestCode == REQUEST_CREATE_UPDATE_WORK_ORDER){
            initClickOnItemDetailService(workOrderDetailModel.getWorkOrderId());
        }
    }

    private void addUrlImage(String posKeyDefine, String imagePath){
        Bitmap loadedBitmap = BaseCameraActivity.convertByteToBitmap(imagePath);
        post += 1;
        ImageListModel imageListModel = new ImageListModel();
        imageListModel.setKeyImage(posKeyDefine);
        imageListModel.setBitmapImage(loadedBitmap);
        bitmapList.add(imageListModel);
    }

    private void setImageRecyclerView(int post){

        if (!bitmapList.isEmpty()){
            txtNoImage.setVisibility(View.GONE);
        } else {
            txtNoImage.setVisibility(View.VISIBLE);
        }
        imageRecyclerView.setLayoutManager(new GridLayoutManager(this, 1, RecyclerView.HORIZONTAL, false));
        ImageListWorkOrderAdapter pickUpAdapter = new ImageListWorkOrderAdapter(this, post, bitmapList, onClearImage);
        imageRecyclerView.setAdapter(pickUpAdapter);

        btnAddImage.setVisibility(Utils.visibleView(bitmapList));
    }

    private final ImageListWorkOrderAdapter.OnClearImage onClearImage = new ImageListWorkOrderAdapter.OnClearImage() {

        @Override
        public void onClickRemove(ImageListModel bitmap, int post) {
            if (bitmap.getKeyImage().equals("")){
                removeImage(post, bitmap);
            } else {
                if (hashMapImageList.get(bitmap.getKeyImage()) != null){
                    deleteImageAlert(WorkOrderDetailActivity.this, StaticUtilsKey.work_order_image, workOrderDetailModel.getWorkOrderId(),
                            hashMapImageList.get(bitmap.getKeyImage()), bitmap, post);
                }

            }

        }

        @Override
        public void onViewImage(ImageListModel bitmap) {
            if (bitmap.getBitmapImage() != null){
                Utils.openImageOnDialog(WorkOrderDetailActivity.this, convert(bitmap.getBitmapImage()));
            }
        }
    };

    private void removeImage(int position, ImageListModel bitmap){
        bitmapList.remove(bitmap);

        setImageRecyclerView(position);
    }

    private void initRecycleViewWorkOrder(){

        WorkProgressListAdapter workProgressListAdapter = new WorkProgressListAdapter(WorkOrderDetailActivity.this, workOrderDetailModel.getWorkOrderInProgress(), workOrderProgressModel -> {
            progressBar.setVisibility(View.VISIBLE);
            new WorkOrderWs().getDetailWorkProgressById(WorkOrderDetailActivity.this, workOrderProgressModel.getWorkProgressId(), new WorkOrderWs.CallBackWorkProgressListener() {

                @Override
                public void onSuccessWorkOrderDetail(WorkOrderProgressModel workOrderDetailModel) {
                    progressBar.setVisibility(View.GONE);
                    startWorkOrderAction("update_work_order", workOrderDetailModel, workOrderDetailModel.getWorkOrderId());
                }

                @Override
                public void onFailed(String msg) {
                    progressBar.setVisibility(View.GONE);
                    Utils.customToastMsgError(WorkOrderDetailActivity.this, msg, false);
                }
            });
        });
        workProgressRecycle.setAdapter(workProgressListAdapter);

        workProgressRecycle.setVisibility(workOrderDetailModel.getWorkOrderInProgress().size() > 0 ? View.VISIBLE : View.GONE);
        noItemTv.setVisibility(workOrderDetailModel.getWorkOrderInProgress().size() == 0 ? View.VISIBLE : View.GONE);
    }

    private void startWorkOrderAction(String action, WorkOrderProgressModel workOrderProgressModel, String work_order_id){
        Intent intent = new Intent(WorkOrderDetailActivity.this, CreateWorkProgressActivity.class);
        intent.putExtra("action", action);
        intent.putExtra("work_order_id", work_order_id);
        intent.putExtra("work_order_model",workOrderDetailModel);
        if (action.equals("update_work_order")) intent.putExtra("work_order_detail", workOrderProgressModel);
        startActivityForResult(intent, REQUEST_CREATE_UPDATE_WORK_ORDER);
    }

    private HashMap<String, Object> hashMapGetData(){
        HashMap<String, Object> hashMap = new HashMap<>();
        List<String> listImage = new ArrayList<>();
        if (bitmapList.size() == 0){
            listImage.add("");
        }
        for (ImageListModel bitmap : bitmapList){
            if (bitmap.getKeyImage().equals("")) {
                listImage.add(convert(bitmap.getBitmapImage()));
            }
        }
        hashMap.put("user_id", new UserSessionManagement(WorkOrderDetailActivity.this).getUserId());
        hashMap.put("status", getStatus(Utils.getValueFromKeyStatus(WorkOrderDetailActivity.this), getStatusVal));
        hashMap.put("priority", getPriority(Utils.getPriorityOnWorkOrder(WorkOrderDetailActivity.this), getPriorityVal));
        hashMap.put("estimate_duration", getEstimateTimeVal);
        hashMap.put("component_list", totalComponentListEdt.getText().toString().trim());
        hashMap.put("description", descriptionTv.getText().toString().trim());
        hashMap.put("work_order_id", workOrderDetailModel.getWorkOrderId());
        hashMap.put("new_work_order_images", listImage);

        return hashMap;
    }

    private void deleteImageAlert(Context context, String actionType, String workOrderId, String filePathImage, ImageListModel bitmap, int position){
        new AlertDialog.Builder(context)
                .setTitle(getResources().getString(R.string.confirm))
                .setMessage(getResources().getString(R.string.are_you_sure_to_delete))
                .setPositiveButton(getResources().getString(R.string.yes), (dialog, which) -> {
                    requestServiceDelete(actionType, workOrderId, filePathImage, bitmap, position);
                    dialog.dismiss();
                })
                .setNegativeButton(getResources().getString(R.string.no), (dialog, which) -> dialog.dismiss())
                .setCancelable(false)
                .setIcon(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_report_problem, null))
                .show();
    }

    private void requestServiceDelete(String type, String workOrderId, String filePathImage, ImageListModel bitmap, int position){
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("delete_image_type", type);
        hashMap.put("work_order_id", workOrderId);
        hashMap.put("file_path", filePathImage);

        progressBar.setVisibility(View.VISIBLE);
        new WorkOrderWs().deleteImageWorkOrder(WorkOrderDetailActivity.this, hashMap,
                new WorkOrderWs.CallBackWorkOrderListener() {
            @Override
            public void onSuccessDeleteImageWorkOrder(String msg) {
                progressBar.setVisibility(View.GONE);
                Utils.customToastMsgError(WorkOrderDetailActivity.this, msg, true);

                removeImage(position, bitmap);
                hashMapImageList.remove(bitmap.getKeyImage());
            }

            @Override
            public void onFailed(String msg) {
                progressBar.setVisibility(View.GONE);
                Utils.customToastMsgError(WorkOrderDetailActivity.this, msg, false);
            }
        });
    }

    private final WorkOrderWs.CallBackWorkOrderListener callBackWorkOrderListener = new WorkOrderWs.CallBackWorkOrderListener() {
        @Override
        public void onSuccessDeleteImageWorkOrder(String msg) {
            progressBar.setVisibility(View.GONE);
            Utils.customToastMsgError(WorkOrderDetailActivity.this, msg, true);
            setResult(RESULT_OK);
            finish();
        }

        @Override
        public void onFailed(String msg) {
            progressBar.setVisibility(View.GONE);
            Utils.customToastMsgError(WorkOrderDetailActivity.this, msg, false);
        }
    };

    private void initClickOnItemDetailService(String workOrderId ){
        progressBar.setVisibility(View.VISIBLE);
        new WorkOrderWs().getDetailWorkOrderById(this, workOrderId, new WorkOrderWs.CallBackListener() {
            @Override
            public void onSuccessWorkOrderList(List<WorkOrderItemModel> workOrderItemModelList) { }

            @Override
            public void onSuccessWorkOrderDetail(WorkOrderDetailModel getWorkOrderDetailModel) {
                progressBar.setVisibility(View.GONE);
                if (getWorkOrderDetailModel != null) {
                    workOrderDetailModel = new WorkOrderDetailModel();
                    workOrderDetailModel = getWorkOrderDetailModel;

                    setValueOnDonePercent(workOrderDetailModel.getDonePercent());
                    btnCreateWorkOrder.setVisibility(checkHideBtnAdd(false, workOrderDetailModel.isHideButtonAdd()));

                    initRecycleViewWorkOrder();

                    setResult(RESULT_OK);
                }
            }

            @Override
            public void onFailed(String msg) {
                progressBar.setVisibility(View.VISIBLE);
                Utils.customToastMsgError(WorkOrderDetailActivity.this, msg, false);
            }
        });
    }

}