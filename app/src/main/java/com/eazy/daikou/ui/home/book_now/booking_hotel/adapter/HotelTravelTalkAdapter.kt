package com.eazy.daikou.ui.home.book_now.booking_hotel.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.helper.DateUtil
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.helper.Utils.spanGridImageLayoutManager
import com.eazy.daikou.model.booking_hotel.HotelTravelTalkModel

class HotelTravelTalkAdapter(private val context: Context, private val hotelArticlesList : ArrayList<HotelTravelTalkModel>, private val onClickListener : OnClickItemListener) : RecyclerView.Adapter<HotelTravelTalkAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.hotel_travel_talk_model, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBindingView(context, hotelArticlesList[position], onClickListener)
    }

    override fun getItemCount(): Int {
        return hotelArticlesList.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val subjectTv : TextView = itemView.findViewById(R.id.subjectTv)
        private val descriptionTv : TextView = itemView.findViewById(R.id.descriptionTv)
        private val mainLayout : CardView = itemView.findViewById(R.id.mainLayout)
        private val btnLike : ImageView = itemView.findViewById(R.id.btnLike)
        private val btnLikeLayout : LinearLayout = itemView.findViewById(R.id.btnLikeLayout)
        private val btnShareLayout : LinearLayout = itemView.findViewById(R.id.btnShareLayout)
        private val commentLayout : LinearLayout = itemView.findViewById(R.id.commentLayout)
        private val profileLayout : LinearLayout = itemView.findViewById(R.id.profileLayout)
        private val recyclerView : RecyclerView = itemView.findViewById(R.id.recyclerView)

        fun onBindingView(context: Context, item: HotelTravelTalkModel, onClickListener : OnClickItemListener){
            //User Profile
            if (item.created_by != null){
                setValueTxt(itemView.findViewById(R.id.nameTv), item.created_by!!.name)
                Glide.with(context).load(if (item.created_by!!.image != null) item.created_by!!.image else R.drawable.ic_my_profile).into(itemView.findViewById(R.id.profile_image))

                setValueTxt(itemView.findViewById(R.id.numPost), String.format("%s %s", item.created_by!!.total_posts, context.resources.getString(R.string.post)))
                setValueTxt(itemView.findViewById(R.id.numLike), String.format("%s %s", item.created_by!!.total_likes, context.resources.getString(R.string.like)))
                setValueTxt(itemView.findViewById(R.id.numComment), String.format("%s %s", item.created_by!!.total_comments, context.resources.getString(R.string.comment)))
            } else {
                Utils.setValueOnText(itemView.findViewById(R.id.nameTv), "- - -")
                Glide.with(context).load(if (item.created_by!!.image != null) item.created_by!!.image else R.drawable.ic_my_profile).into(itemView.findViewById(R.id.profile_image))

                setValueTxt(itemView.findViewById(R.id.numPost), "")
                setValueTxt(itemView.findViewById(R.id.numLike), "")
                setValueTxt(itemView.findViewById(R.id.numComment), "")
            }

            // Content Body
            Utils.setTextHtml(subjectTv, item.title)

            setValueTxt(descriptionTv, item.description)
            if (item.category != null){
                setValueTxt(itemView.findViewById(R.id.typeTv), item.category!!.name)
            } else {
                setValueTxt(itemView.findViewById(R.id.typeTv), "")
            }

            val durationTv = itemView.findViewById<TextView>(R.id.durationTv)
            if (item.created_at != null){
                durationTv.text = DateUtil.getDurationDate(durationTv.context, item.created_at)
            } else {
                durationTv.text = "- - -"
            }

            setValueTxt(itemView.findViewById(R.id.totalShare), String.format("%s %s", item.total_shares, context.getString(R.string.share)))
            setValueTxt(itemView.findViewById(R.id.totalComment), String.format("%s %s", item.total_replies, context.resources.getString(R.string.comment)))
            setValueTxt(itemView.findViewById(R.id.totalLike), String.format("%s %s", item.total_likes, context.resources.getString(R.string.like)))

            Glide.with(btnLike).load(if (item.is_user_liked)    R.drawable.ic_like_fb else R.drawable.ic_unlike_fb).into(btnLike)

            // Show Image
            recyclerView.layoutManager = spanGridImageLayoutManager(item.images, context)
            recyclerView.isNestedScrollingEnabled = false
            recyclerView.adapter = CustomImageGridLayoutAdapter("show_5_items", context, item.images, object : CustomImageGridLayoutAdapter.OnClickCallBackLister{
                override fun onClickCallBack(value: String) {
                    onClickListener.onClickListener(item, "detail_action")
                }
            })

            // Do Action
            mainLayout.setOnClickListener(CustomSetOnClickViewListener{
                onClickListener.onClickListener(item, "detail_action")
            })

            btnLikeLayout.setOnClickListener{
                onClickListener.onClickListener(item, "like_action")
            }

            btnShareLayout.setOnClickListener(CustomSetOnClickViewListener{
                onClickListener.onClickListener(item, "share_action")
            })

            commentLayout.setOnClickListener(CustomSetOnClickViewListener{
                onClickListener.onClickListener(item, "comment_action")
            })

            profileLayout.setOnClickListener(CustomSetOnClickViewListener{
                onClickListener.onClickListener(item, "view_profile_action")
            })

            subjectTv.setOnClickListener {
                if (subjectTv.maxLines == 2) {
                    subjectTv.maxLines = 50
                } else {
                    subjectTv.maxLines = 2
                }
            }
            descriptionTv.setOnClickListener {
                if (descriptionTv.maxLines == 3) {
                    descriptionTv.maxLines = 50
                } else {
                    descriptionTv.maxLines = 3
                }
            }
        }

        private fun setValueTxt(textView: TextView, value: String?) {
            textView.text = value ?: " "
        }

    }

    fun clear() {
        val size: Int = hotelArticlesList.size
        if (size > 0) {
            for (i in 0 until size) {
                hotelArticlesList.removeAt(0)
            }
            notifyItemRangeRemoved(0, size)
        }
    }

    interface OnClickItemListener {
        fun onClickListener(hotelArticleModel: HotelTravelTalkModel, action: String)
    }
}