package com.eazy.daikou.ui.home.my_property.property_service.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.databinding.ServiceByCategoryModelBinding
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.my_property.property_service.ServiceByCategoryModel
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener

class SubServiceByCategoryAdapter(private val activity : Activity, private val list: ArrayList<ServiceByCategoryModel>, private val itemClickService: OnClickItemListener):
    RecyclerView.Adapter<SubServiceByCategoryAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = ServiceByCategoryModelBinding.inflate(LayoutInflater.from(parent.context), parent, false)

        val itemViewParam = LinearLayout.LayoutParams(Utils.getWidth(activity) / 2, LinearLayout.LayoutParams.WRAP_CONTENT)
        view.mainLayout.layoutParams = itemViewParam

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val service : ServiceByCategoryModel = list[position]
        holder.onBindView(service, itemClickService)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    interface OnClickItemListener {
        fun onClick(propertyService: ServiceByCategoryModel)
    }

    class ViewHolder(private val mBind: ServiceByCategoryModelBinding) : RecyclerView.ViewHolder(mBind.root) {

        fun onBindView(service : ServiceByCategoryModel, itemClickService: OnClickItemListener){
            Utils.setValueOnText(mBind.nameTv, service.name)
            Utils.setValueOnText(mBind.descriptionTv, service.description)
            Utils.setValueOnText(mBind.priceTv, service.price)
            Glide.with(mBind.image.context).load(if (service.image != null) service.image else R.drawable.no_image).into(mBind.image)

            mBind.materialLayout.setOnClickListener ( CustomSetOnClickViewListener {
                itemClickService.onClick(service)
            })
        }

    }

}