package com.eazy.daikou.ui.home.my_property.property_service

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import androidx.activity.result.ActivityResult
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.databinding.ActivityPropertyServiceListBinding
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.my_property.property_service.PropertyServiceListModel
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.ui.home.book_now.booking_hotel.adapter.HotelHomeListViewPager
import com.eazy.daikou.ui.home.data_record.fragment.QuotationFragment
import com.eazy.daikou.ui.home.my_property.my_unit.PropertyMyUnitDetailActivity
import com.eazy.daikou.ui.home.my_property.property_service.adapter.PropertyServiceListAdapter

class PropertyServiceListActivity : BaseActivity() {

    private lateinit var mBinding : ActivityPropertyServiceListBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivityPropertyServiceListBinding.inflate(layoutInflater)
        setContentView(mBinding.root)

        initView()

        initAction()

    }

    private fun initView() {
        Utils.customOnToolbar(this, resources.getString(R.string.property_service)) { finish() }
    }

    private fun initAction() {
        val adapter = HotelHomeListViewPager(supportFragmentManager).also {
            it.addFragment(PropertyServiceListFragment.newInstance("all"), resources.getString(R.string.all))
            it.addFragment(PropertyServiceListFragment.newInstance("new"), resources.getString(R.string.new_))
            it.addFragment(PropertyServiceListFragment.newInstance("pending"), resources.getString(R.string.pending))
            it.addFragment(PropertyServiceListFragment.newInstance("accept"), resources.getString(R.string.accepted))
            it.addFragment(PropertyServiceListFragment.newInstance("processing"), resources.getString(R.string.processing))
            it.addFragment(PropertyServiceListFragment.newInstance("complete"), resources.getString(R.string.completed))
        }
        mBinding.viewpager.adapter = adapter
        mBinding.slidingTabs.setupWithViewPager(mBinding.viewpager)
    }

}