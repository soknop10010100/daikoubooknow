package com.eazy.daikou.ui.home.inspection_work_order

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.appcompat.view.menu.MenuBuilder
import androidx.appcompat.view.menu.MenuPopupHelper
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.base.MockUpData
import com.eazy.daikou.request_data.request.inspection_ws.InspectionWS
import com.eazy.daikou.request_data.request.work_oder_ws.WorkOrderWs
import com.eazy.daikou.request_data.static_data.StaticUtilsKey
import com.eazy.daikou.helper.CustomStartIntentUtilsClass.Companion.onStartSplashScreenActivity
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.home.MyRolePermissionModel
import com.eazy.daikou.model.inspection.*
import com.eazy.daikou.model.profile.User
import com.eazy.daikou.model.work_order.WorkOrderDetailModel
import com.eazy.daikou.model.work_order.WorkOrderItemModel
import com.eazy.daikou.ui.home.inspection_work_order.adapter.ItemInspectionAdapter
import com.eazy.daikou.ui.home.complaint_solution.adpater.CategoryPropertyAdapter
import com.google.gson.Gson
import java.io.Serializable


class InspectionListActivity : BaseActivity(), View.OnClickListener{

    private lateinit var itemRecyclerView : RecyclerView
    private lateinit var categoryRecyclerView : RecyclerView
    private lateinit var textNoRecordTv : TextView
    private lateinit var progressBar: ProgressBar
    private lateinit var itemCategoryAdapter : CategoryPropertyAdapter
    private lateinit var itemInspectionAdapter : ItemInspectionAdapter
    private lateinit var switchMenuTop : ImageView
    private lateinit var iconSwitchMenuMaintenance : ImageView
    private lateinit var btnAdd : ImageView
    private lateinit var titleToolbar : TextView
    private var itemLayout : LinearLayout ?= null
    private var refreshLayout : SwipeRefreshLayout? = null
    private var linearLayoutManager: LinearLayoutManager? = null
    private var itemLinearLayoutManager: LinearLayoutManager? = null

    private var workOrderItemList : ArrayList<WorkOrderItemModel> = ArrayList()
    private var inspectionTypeList : ArrayList<ItemInspectionModel> = ArrayList()
    private var inspectionInfoList : ArrayList<InfoInspectionModel> = ArrayList()

    private var currentItem = 0
    private var total : Int = 0
    private var scrollDown : Int = 0
    private var currentPage : Int = 1
    private var size : Int = 10
    private var isScrolling = false
    private var inspectionTypeId : String = ""
    private var inspectionTypeName : String = ""
    private var action = ""
    private var categoryWorkOrderType = ""
    private var userRole = ""
    private var userId = ""
    private var activeAccountId = ""
    private var propertyId = ""
    private var workOrderType : String = ""
    private var workOrderId : String = ""
    private var maintenanceType : String = "preventive"
    private var isClickMenuMaintenanceType = false
    private val REQUEST_CODE_WORK_ORDER = 444
    private val REQUEST_CODE_CREATE_INSPECTION_FORM = 445
    private var userPermissionModel: MyRolePermissionModel ?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_inspection_list)

        initView()

        initData()

        initAction()

    }


    private fun initView(){
        refreshLayout = findViewById(R.id.swipe_layouts)
        itemRecyclerView = findViewById(R.id.listInspection)
        categoryRecyclerView = findViewById(R.id.inspectionCategory)
        textNoRecordTv = findViewById(R.id.no_data)
        progressBar = findViewById(R.id.progressItem)
        btnAdd = findViewById(R.id.btnAdd)
        btnAdd.visibility = View.GONE

        titleToolbar = findViewById(R.id.titleToolbar)
        findViewById<ImageView>(R.id.iconBack).setOnClickListener{ onBackFish() }

        findViewById<LinearLayout>(R.id.layoutMap).visibility = View.VISIBLE
        switchMenuTop = findViewById(R.id.addressMap)
        iconSwitchMenuMaintenance = findViewById(R.id.iconAdd)
        itemLayout = findViewById(R.id.mainListLayout)
    }

    private fun initData(){
        if (intent != null && intent.hasExtra("action")){
            action = intent.getStringExtra("action").toString()
        }

        if (intent != null && intent.hasExtra("user_permission")){
            userPermissionModel = (intent.getSerializableExtra("user_permission") as MyRolePermissionModel?)!!
        }

        // Get Data From Notification My Firebase
        if (intent != null && intent.hasExtra("work_order_id")){
            workOrderId = intent.getStringExtra("work_order_id").toString()
        }
        if (intent != null && intent.hasExtra("work_order_type")){
            workOrderType = intent.getStringExtra("work_order_type").toString()
        }

        userRole = if(MockUpData.ROLE_INSPECTION_WORK_ORDER) "inspector" else "not_inspector"

        StaticUtilsKey.isActionMenuHome = action

        val userSessionManagement = UserSessionManagement(this)
        val mUser = Gson().fromJson(userSessionManagement.userDetail, User::class.java)

        activeAccountId = Utils.validateNullValue(mUser.accountId)
        userId = userSessionManagement.userId
        propertyId = Utils.validateNullValue(mUser.activePropertyIdFk)

        linearLayoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL,false)
        categoryRecyclerView.layoutManager = linearLayoutManager

        itemLinearLayoutManager = LinearLayoutManager(this)
        itemRecyclerView.layoutManager = itemLinearLayoutManager

    }


    private fun initAction(){

        refreshLayout!!.setColorSchemeResources(R.color.colorPrimary)
        refreshLayout!!.setOnRefreshListener { this.refreshItemList() }

        when (action) {
            StaticUtilsKey.work_order_action -> {
                findViewById<LinearLayout>(R.id.layoutMap).visibility = View.INVISIBLE
                titleToolbar.text = resources.getString(R.string.word_order).uppercase()

                inspectionTypeList = initListCategoryWorkOrder(this)

                if (workOrderType == ""){
                    inspectionTypeList[0].isClick = true
                    categoryWorkOrderType = inspectionTypeList[0].id
                }

                initCategoryInspectionType()

            } else -> {     // Inspection , Maintenance
                getListInspectionType()
                titleToolbar.text = if (action == StaticUtilsKey.inspection_action)
                    resources.getString(R.string.inspection).uppercase() else resources.getString(R.string.maintenance).uppercase()
            }
        }

        switchMenuTop.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_calendar))
        switchMenuTop.setOnClickListener(this)
        iconSwitchMenuMaintenance.setOnClickListener(this)
        btnAdd.setOnClickListener(this)

        if (action == StaticUtilsKey.maintenance_action)   iconSwitchMenuMaintenance.visibility = View.VISIBLE
    }

    private fun refreshItemList(){
        currentPage = 1
        size = 10
        isScrolling = true

        if (itemInspectionAdapter != null)  itemInspectionAdapter.clear()

        if(action == StaticUtilsKey.work_order_action) {
            initListItemWorkOrder()
        } else {
            getListItemInfoInspectionList()
        }
    }

    private fun getListInspectionType(){
        InspectionWS().getInspection_MaintenanceType(this@InspectionListActivity, action,inspectionObject)
    }

    private val inspectionObject = object : InspectionWS.CallBackItemInspectionListener{
        override fun onSuccess(itemInspectionModels: MutableList<ItemInspectionModel>?) {
            progressBar.visibility = View.GONE

            inspectionTypeList = ArrayList()
            if (action == StaticUtilsKey.maintenance_action){
                for (inspectionCategoryList in itemInspectionModels!!){
                    if (inspectionCategoryList.main_category_type != null){
                        if (maintenanceType == "corrective") {
                            if (inspectionCategoryList.main_category_type.equals("corrective")) {
                                inspectionTypeList.add(inspectionCategoryList)
                            }
                        } else {
                            if (inspectionCategoryList.main_category_type.equals("preventive")) {
                                inspectionTypeList.add(inspectionCategoryList)
                            }
                        }
                    }
                }

            } else {
                itemInspectionModels.let { inspectionTypeList.addAll(it!!) }
            }

            inspectionTypeList[0].isClick = true
            inspectionTypeId = inspectionTypeList[0].id
            inspectionTypeName = inspectionTypeList[0].inspection_name

            initCategoryInspectionType()
            if(userPermissionModel != null){
                if (userPermissionModel!!.is_role){
                    btnAdd.visibility = View.VISIBLE
                } else {
                    btnAdd.visibility = View.GONE
                }
            } else {    // no get permission in my role, from more screen
                btnAdd.visibility = View.GONE
            }
        }

        override fun onSuccessGetInspectionFormList(itemInspectionModels: MutableList<ItemTemplateAPIModel>?) {}

        override fun onSuccessSaveTemplate(itemInspectionModels: TemplateFormInspectionModel?) {}

        override fun onSuccessSaveInspectionList(msg: String?, itemTemplateModel: ItemTemplateModel?, modelList: ItemTemplateAPIModel?) {}

        override fun onSuccessGetInspectorList(inspectorModelList: MutableList<InspectorModel>?) {}

        override fun onFailed(msg: String?) {
            progressBar.visibility = View.GONE
            Utils.customToastMsgError(this@InspectionListActivity, msg, false)
        }

    }

    @SuppressLint("NotifyDataSetChanged")
    private fun initCategoryInspectionType(){
        itemCategoryAdapter =
            CategoryPropertyAdapter(
                this,
                inspectionTypeList,
                onClickOnCategory
            )
        categoryRecyclerView.adapter = itemCategoryAdapter

        if (workOrderType != ""){   // From Notification
            for (i in inspectionTypeList.indices) {
                if (inspectionTypeList[i].id == workOrderType) {
                    categoryRecyclerView.scrollToPosition(i)
                    inspectionTypeList[i].isClick = true
                    categoryWorkOrderType = workOrderType
                } else {
                    inspectionTypeList[i].isClick = false
                }
            }

            itemCategoryAdapter.notifyDataSetChanged()

            initClickOnItemDetailService(workOrderId)
        }

        if(isClickMenuMaintenanceType){
            isClickMenuMaintenanceType = false
            refreshItemList()
        } else {
            initItemRecycle()

            onScrollInfoRecycle()
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private var onClickOnCategory = CategoryPropertyAdapter.ClickInspectionCallBack { floorSpinnerModel ->
            for (selectPropertyGuest in inspectionTypeList) {
                selectPropertyGuest.isClick = floorSpinnerModel.id == selectPropertyGuest.id
            }

            if(action == StaticUtilsKey.work_order_action){
                categoryWorkOrderType = floorSpinnerModel.id
            } else{
                inspectionTypeId = floorSpinnerModel.id
                inspectionTypeName = floorSpinnerModel.inspection_name
            }

            refreshItemList()
            itemCategoryAdapter.notifyDataSetChanged()
        }

    private fun initItemRecycle(){
        if (action == StaticUtilsKey.work_order_action){
            itemInspectionAdapter = ItemInspectionAdapter(StaticUtilsKey.work_order_action, workOrderItemList, this@InspectionListActivity, onClickItemDetail)
            itemRecyclerView.adapter = itemInspectionAdapter

            initListItemWorkOrder()
        }
        else {
            itemInspectionAdapter = ItemInspectionAdapter(inspectionInfoList, this@InspectionListActivity, onClickItemDetail)
            itemRecyclerView.adapter = itemInspectionAdapter

            getListItemInfoInspectionList()
        }
    }

    private var onClickItemDetail = object : ItemInspectionAdapter.ClickBackListener{
        override fun clickOnItemAcceptOpenMap(infoInspectionModel: InfoInspectionModel) {

            val intent = Intent(this@InspectionListActivity, ChooseInspectionTypeActivity::class.java)
            intent.putExtra("inspection_template_id", infoInspectionModel.id)
            intent.putExtra("template_id", infoInspectionModel.inspectionTemplateId)
            intent.putExtra("action", StaticUtilsKey.form_template_type)
            intent.putExtra("action_type", "action_detail")
            startActivity(intent)

        }

        override fun clickOnItemWorkOrder(workOrderItemModel: WorkOrderItemModel) {
            progressBar.visibility = View.VISIBLE
            initClickOnItemDetailService(workOrderItemModel.id)
        }
    }


    private fun getListItemInfoInspectionList(){
        progressBar.visibility = View.VISIBLE
        InspectionWS().getInspection_MaintenanceItemAll(this@InspectionListActivity,  action, currentPage, 10, propertyId, inspectionTypeId,
                object : InspectionWS.CallBackItemInspectionInfoListener{

                @SuppressLint("NotifyDataSetChanged")
                override fun onSuccess(itemInspectionModels: MutableList<InfoInspectionModel>?) {
                    progressBar.visibility = View.GONE
                    refreshLayout!!.isRefreshing = false
                    itemInspectionModels?.let { inspectionInfoList.addAll(it) }

                    textNoRecordTv.visibility = if (inspectionInfoList.size > 0) View.GONE else View.VISIBLE
                    itemRecyclerView.visibility = if (inspectionInfoList.size > 0) View.VISIBLE else View.GONE

                    itemInspectionAdapter.notifyDataSetChanged()
                }

                override fun onFailed(msg: String?) {
                    refreshLayout!!.isRefreshing = false
                    progressBar.visibility = View.GONE
                    Utils.customToastMsgError(this@InspectionListActivity, msg, false)
                }

            })
    }

    private fun onScrollInfoRecycle() {
        itemRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true
                }
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                currentItem = itemLinearLayoutManager!!.childCount
                total = itemLinearLayoutManager!!.itemCount
                scrollDown = itemLinearLayoutManager!!.findFirstVisibleItemPosition()
                if (dy > 0) {
                    if (isScrolling && currentItem + scrollDown == total) {
                        if (total == size) {
                            isScrolling = false
                            currentPage++
                            progressBar.visibility = View.VISIBLE
                            if (action == StaticUtilsKey.work_order_action) {
                                recyclerView.scrollToPosition(workOrderItemList.size - 1)
                            } else {
                                recyclerView.scrollToPosition(inspectionInfoList.size - 1)
                            }
                            initItemRecycle()
                            size += 10
                        } else {
                            progressBar.visibility = View.GONE
                        }
                    }
                }
            }
        })
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnAdd -> {
                val intent = Intent(this@InspectionListActivity, ChooseInspectionTypeActivity::class.java)
                intent.putExtra("inspection_list",inspectionTypeList as Serializable)
                intent.putExtra("inspection_type_id", inspectionTypeId)
                intent.putExtra("inspection_type_name", inspectionTypeName)
                intent.putExtra("action", StaticUtilsKey.inspection_type)
                startActivityForResult(intent, REQUEST_CODE_CREATE_INSPECTION_FORM)
            }
            R.id.addressMap -> {
                val intent = Intent(this@InspectionListActivity, CalendarInspectionActivity::class.java)
                intent.putExtra("action", action)
                startActivity(intent)
            }
            R.id.iconAdd -> {
                initMainMenuMaintenance(v)
            }
        }
    }


    @SuppressLint("NotifyDataSetChanged", "Range", "RestrictedApi")
    private fun initMainMenuMaintenance(view: View) {
        val menuBuilder = MenuBuilder(this@InspectionListActivity)
        val inflater = MenuInflater(this@InspectionListActivity)
        inflater.inflate(R.menu.menu_option_maintenance, menuBuilder)
        val optionsMenu = MenuPopupHelper(this, menuBuilder, view)
        optionsMenu.setForceShowIcon(true)

        menuBuilder.setCallback(object : MenuBuilder.Callback{
            override fun onMenuItemSelected(menu: MenuBuilder, item: MenuItem): Boolean {
                return when (item.itemId) {
                    R.id.preventive_maintenance -> {
                        if (maintenanceType != "preventive") {
                            maintenanceType = "preventive"
                            iconSwitchMenuMaintenance.setImageDrawable(item.icon)
                            isClickMenuMaintenanceType = true
                            getListInspectionType()
                        }
                        true
                    }
                    R.id.corrective_maintenance -> {
                        if (maintenanceType != "corrective") {
                            maintenanceType = "corrective"
                            iconSwitchMenuMaintenance.setImageDrawable(item.icon)
                            isClickMenuMaintenanceType = true
                            getListInspectionType()
                        }
                        true
                    }
                    else -> false
                }
            }

            override fun onMenuModeChange(menu: MenuBuilder) {}

        })
        optionsMenu.show()
    }

    // ==================================== Work Order Part ========================================

    private fun initListItemWorkOrder() {
        progressBar.visibility = View.VISIBLE
        WorkOrderWs().getListWorkOrder(this@InspectionListActivity, currentPage,  categoryWorkOrderType,
            propertyId, userId, userRole, activeAccountId, callBackWorkOrderLister)
    }

    private fun initClickOnItemDetailService(workOrderId : String){
        WorkOrderWs().getDetailWorkOrderById(this@InspectionListActivity, workOrderId, callBackWorkOrderLister)
    }


    private var callBackWorkOrderLister = object : WorkOrderWs.CallBackListener{
        @SuppressLint("NotifyDataSetChanged")
        override fun onSuccessWorkOrderList(workOrderItemModelList: MutableList<WorkOrderItemModel>?) {
            progressBar.visibility = View.GONE
            refreshLayout!!.isRefreshing = false
            workOrderItemModelList?.let { workOrderItemList.addAll(it) }
            textNoRecordTv.visibility = if (workOrderItemList.size > 0) View.GONE else View.VISIBLE
            itemInspectionAdapter.notifyDataSetChanged()
        }

        override fun onSuccessWorkOrderDetail(workOrderDetailModel: WorkOrderDetailModel?) {
            progressBar.visibility = View.GONE
            val intent = Intent(this@InspectionListActivity, WorkOrderDetailActivity::class.java)
            intent.putExtra("work_order_detail", workOrderDetailModel)
            startActivityForResult(intent, REQUEST_CODE_WORK_ORDER)
        }

        override fun onFailed(msg: String?) {
            refreshLayout!!.isRefreshing = false
            progressBar.visibility = View.GONE
            Utils.customToastMsgError(this@InspectionListActivity, msg, false)
        }

    }

    private fun initListCategoryWorkOrder(context : Context) : ArrayList<ItemInspectionModel>{
        progressBar.visibility = View.GONE
        val inspectionType : ArrayList<ItemInspectionModel> = ArrayList()

        var itemInspectionModel = ItemInspectionModel()
        itemInspectionModel.id = "all"
        itemInspectionModel.inspection_name = Utils.getText(context, R.string.all)
        inspectionType.add(itemInspectionModel)

        itemInspectionModel = ItemInspectionModel()
        itemInspectionModel.id = "pre_handover"
        itemInspectionModel.inspection_name = Utils.getText(context, R.string.pre_hand_over)
        inspectionType.add(itemInspectionModel)

        itemInspectionModel = ItemInspectionModel()
        itemInspectionModel.id = "audit"
        itemInspectionModel.inspection_name = Utils.getText(context, R.string.audit_inspection)
        inspectionType.add(itemInspectionModel)

        itemInspectionModel = ItemInspectionModel()
        itemInspectionModel.id = "maintenance"
        itemInspectionModel.inspection_name = Utils.getText(context, R.string.maintenance)
        inspectionType.add(itemInspectionModel)

        itemInspectionModel = ItemInspectionModel()
        itemInspectionModel.id = "complain"
        itemInspectionModel.inspection_name = Utils.getText(context, R.string.complain)
        inspectionType.add(itemInspectionModel)

        itemInspectionModel = ItemInspectionModel()
        itemInspectionModel.id = "other"
        itemInspectionModel.inspection_name = Utils.getText(context, R.string.other)
        inspectionType.add(itemInspectionModel)

        return inspectionType
    }

    override fun onBackPressed() {
        onBackFish()
    }

    private fun onBackFish(){
        if (action == StaticUtilsKey.work_order_action){
            if (workOrderType != ""){
                onStartSplashScreenActivity(this@InspectionListActivity, "already_notification")
                finishAffinity()
            } else {
                finish()
            }
        } else {
            finish()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == RESULT_OK && requestCode == REQUEST_CODE_WORK_ORDER){
            refreshItemList()
        } else if (resultCode == RESULT_OK && requestCode == REQUEST_CODE_CREATE_INSPECTION_FORM) {
            if (data != null) {
                inspectionTypeId = data.getStringExtra("id").toString()
                inspectionTypeName = data.getStringExtra("inspection_name").toString()

                for (i in inspectionTypeList.indices) {
                    if (inspectionTypeList[i].id == inspectionTypeId) {
                        categoryRecyclerView.scrollToPosition(i)
                        inspectionTypeList[i].isClick = true
                    } else {
                        inspectionTypeList[i].isClick = false
                    }
                }

                itemCategoryAdapter.notifyDataSetChanged()
                refreshItemList()
            }
        }
    }

}