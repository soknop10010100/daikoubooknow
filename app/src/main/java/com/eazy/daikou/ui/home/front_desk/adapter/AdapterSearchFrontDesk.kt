package com.eazy.daikou.ui.home.front_desk.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.model.front_desk.SearchFrontDeskModel

class AdapterSearchFrontDesk(
    private val context: Context,
    private val listSearch: ArrayList<SearchFrontDeskModel>,
    private val callBackSearch: CallBackSearchListener
) : RecyclerView.Adapter<AdapterSearchFrontDesk.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context)
            .inflate(R.layout.custom_view_text_view_layout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val searchFrontDesk: SearchFrontDeskModel = listSearch[position]

        holder.itemTitleTv.text = String.format("%s %s %s", searchFrontDesk.name, "|", searchFrontDesk.phone)
        holder.line.visibility = if (listSearch.size - 1 == position) View.GONE else View.VISIBLE

        holder.itemView.setOnClickListener { callBackSearch.onClickItemSearch(searchFrontDesk) }
    }

    override fun getItemCount(): Int {
        return listSearch.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var itemTitleTv: TextView = view.findViewById(R.id.item_name)
        val line: View = itemView.findViewById(R.id.line)
    }

    interface CallBackSearchListener {
        fun onClickItemSearch(searchFrontDesk: SearchFrontDeskModel)
    }
}