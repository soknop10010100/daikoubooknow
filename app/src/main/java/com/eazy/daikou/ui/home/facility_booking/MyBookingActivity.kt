package com.eazy.daikou.ui.home.facility_booking

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.cardview.widget.CardView
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.request_data.request.facility_booking_ws.FacilityBookingWs
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.facility_booking.my_booking.MyBookingItemsModel
import com.eazy.daikou.model.facility_booking.my_booking.MyBookingModel
import com.eazy.daikou.model.facility_booking.my_booking.MyCategoryBookingModel
import com.eazy.daikou.ui.home.facility_booking.adapter.MyBookingDetailAdapter
import com.eazy.daikou.ui.home.facility_booking.adapter.MyCategoryBookingAdapter
import com.eazy.daikou.helper.web.WebPayActivity
import libs.mjn.prettydialog.PrettyDialog
import java.text.SimpleDateFormat
import java.util.*

class MyBookingActivity : BaseActivity() {

    private lateinit var itemRecyclerView: RecyclerView
    private lateinit var itemPaidExpiredRecyclerView: RecyclerView
    private lateinit var progressBar: ProgressBar

    private lateinit var myBookingAdapter: MyCategoryBookingAdapter

    private lateinit var linearLayoutManager : LinearLayoutManager
    private lateinit var refreshLayout: SwipeRefreshLayout

    private lateinit var waitingPayment : TextView
    private lateinit var alreadyBookingTv : TextView
    private lateinit var expiredTv : TextView
    private lateinit var linearLayoutMenus : LinearLayout

    private lateinit var mySubBookingAdapter: MyBookingDetailAdapter
    private var itemCardBookingList : ArrayList<MyCategoryBookingModel> = ArrayList()
    private var itemSubCardBookingList : ArrayList<MyBookingItemsModel> = ArrayList()

    private var currentItem = 0
    private  var total:Int = 0
    private  var scrollDown:Int = 0
    private  var currentPage:Int = 1
    private  var size:Int = 10
    private var isScrolling = false

    private var myBookingId = ""
    private var bookingStatusKey = "pending"
    private var currentDateTime = ""
    private var isExpiredStatus = true

    // No Item Layout
    private lateinit var noResultLayout: LinearLayout
    private lateinit var descriptionNoItemTv: TextView
    private lateinit var emptyBookingImage: ImageView
    private lateinit var continueBookingLayout: CardView
    private lateinit var noItemTitleTv : TextView
    private lateinit var prettyDialog: PrettyDialog
    private var REQUEST_WEB_PAY = 768

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_booking)

        initView()

        initData()

        initAction()
    }

    private fun initView(){
        itemPaidExpiredRecyclerView = findViewById(R.id.recyclerItemNotPending)
        itemRecyclerView = findViewById(R.id.recyclerItem)
        progressBar = findViewById(R.id.progressItem)
        refreshLayout = findViewById(R.id.swipe_layouts)

        waitingPayment = findViewById(R.id.waitingPayment)
        alreadyBookingTv = findViewById(R.id.alreadyBookingTv)
        expiredTv = findViewById(R.id.expiredTv)
        linearLayoutMenus = findViewById(R.id.linearLayoutMenus)

        noResultLayout = findViewById(R.id.txtNoResult)

        // No ItemLayout
        descriptionNoItemTv = findViewById(R.id.descriptionNoItemTv)
        noItemTitleTv = findViewById(R.id.noItemTv)
        emptyBookingImage = findViewById(R.id.imageNoItem)
        continueBookingLayout = findViewById(R.id.continueBookingLayout)
        descriptionNoItemTv.visibility = View.INVISIBLE
        Glide.with(emptyBookingImage).load(R.drawable.no_my_booking_item).into(emptyBookingImage)
        noItemTitleTv.text = resources.getString(R.string.there_is_no_booking_yet)

        Utils.customOnToolbar(this, resources.getString(R.string.my_booking)){finish()}
    }

    private fun initData(){
        if (intent != null && intent.hasExtra("my_booking_id")){
            myBookingId = intent.getStringExtra("my_booking_id").toString()
        }
    }

    private fun initAction(){
        linearLayoutManager = LinearLayoutManager(this)
        itemRecyclerView.layoutManager = linearLayoutManager
        linearLayoutManager = LinearLayoutManager(this)
        itemPaidExpiredRecyclerView.layoutManager = linearLayoutManager

        prettyDialog = PrettyDialog(this)

        refreshLayout.setColorSchemeResources(R.color.colorPrimary)
        refreshLayout.setOnRefreshListener { refreshList() }

        val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        val timeFormat = SimpleDateFormat("k:mm:ss", Locale.getDefault())
        currentDateTime = simpleDateFormat.format(Date()) + " " + timeFormat.format(Date())

        initRecyclerView()

        initPaidOrExpiredRecycler()

        onScrollMyBookingList()

        setClickOnMenuItem()

        continueBookingLayout.setOnClickListener{
            setResult(RESULT_OK)
            finish()
        }

    }

    private fun refreshList(){
        currentPage = 1
        size = 10
        isScrolling = false

        if (bookingStatusKey == "pending") {
            if (myBookingAdapter != null) myBookingAdapter.clear()
        } else {
            if (mySubBookingAdapter != null) mySubBookingAdapter.clear()
        }
        requestListMyBooking()
    }

    private fun initRecyclerView(){
        myBookingAdapter = MyCategoryBookingAdapter(this, itemCardBookingList, onClickCallBack)
        itemRecyclerView.adapter = myBookingAdapter

        requestListMyBooking()
    }

    private fun initPaidOrExpiredRecycler(){
        mySubBookingAdapter = MyBookingDetailAdapter(itemSubCardBookingList, object : MyBookingDetailAdapter.CallBackListener{
            override fun onClickCallBack(myBookingDetail: MyBookingItemsModel) {
                val intent = Intent(this@MyBookingActivity, MyBookingByItemDetailActivity::class.java)
                intent.putExtra("my_booking_id", myBookingDetail.booking_item_id)
                intent.putExtra("booking_status", bookingStatusKey)
                startActivity(intent)
            }

        })
        itemPaidExpiredRecyclerView.adapter = mySubBookingAdapter
    }

    private var onClickCallBack = object : MyCategoryBookingAdapter.ClickCallBackListener{
        override fun onClickCallBack(itemMainCategory: MyCategoryBookingModel, myBookingItemsModel: MyBookingItemsModel) {
            val intent = Intent(this@MyBookingActivity, MyBookingByItemDetailActivity::class.java)
            intent.putExtra("my_booking_id", myBookingItemsModel.booking_item_id)
            intent.putExtra("booking_status", bookingStatusKey)
            startActivity(intent)
        }

        override fun onClickCallBack(itemMainCategory: MyCategoryBookingModel, doAction: String) {
            if (doAction == "pay_now"){
                if (itemMainCategory.payment_kess_link != null && itemMainCategory.payment_kess_link != "") {
                    val intent = Intent(this@MyBookingActivity, WebPayActivity::class.java)
                    intent.putExtra("toolBarTitle", "Kess Pay")
                    intent.putExtra("linkUrl", itemMainCategory.payment_kess_link)
                    startActivityForResult(intent, REQUEST_WEB_PAY)
                } else {
                    Toast.makeText(this@MyBookingActivity, "Link Payment is empty.", Toast.LENGTH_SHORT).show()
                }
            } else {
                customPrettyDialog(itemMainCategory)
            }
        }

    }

    private fun requestListMyBooking(){
        progressBar.visibility = View.VISIBLE
        FacilityBookingWs().getMyBookingDetailList(this, currentPage, 10, UserSessionManagement(this).userId, currentDateTime, bookingStatusKey, callBackListener)
    }

    private var callBackListener = object : FacilityBookingWs.MyBookingCallbackListener{
        override fun onLoadSuccessFull(myBookingModelList: MutableList<MyBookingModel>?) {}

        @SuppressLint("NotifyDataSetChanged")
        override fun onLoadBookingDetailSuccessFull(myBookingModelList: MutableList<MyCategoryBookingModel>) {
            progressBar.visibility = View.GONE
            refreshLayout.isRefreshing = false

            for (myBookingItem in myBookingModelList){
                if (bookingStatusKey == "completed") {
                    for (bookingItem in myBookingItem.booking_items) {
                        if (isExpiredStatus) {
                            if (bookingItem.expired_status) {
                                itemSubCardBookingList.add(bookingItem)
                            }
                        }else {
                            if (!bookingItem.expired_status) {
                                itemSubCardBookingList.add(bookingItem)

                            }
                        }
                    }
                } else {
                    if (myBookingItem.booking_items.size > 0){
                        itemCardBookingList.add(myBookingItem)
                    }
                }
            }

            setViewOnResult()

            if (bookingStatusKey == "completed") {
                mySubBookingAdapter.notifyDataSetChanged()
            } else {
                myBookingAdapter.notifyDataSetChanged()
            }

            isEnableMenuHeader(true)
        }

        @SuppressLint("NotifyDataSetChanged")
        override fun onLoadMessageSuccessFull(itemMainCategory: MyCategoryBookingModel, msg: String?) {
            progressBar.visibility = View.GONE
            Utils.customToastMsgError(this@MyBookingActivity, msg, false)
            itemCardBookingList.remove(itemMainCategory)
            prettyDialog.dismiss()

            myBookingAdapter.notifyDataSetChanged()
        }

        override fun onLoadFailed(error: String?) {
            progressBar.visibility = View.GONE
            refreshLayout.isRefreshing = false
            Utils.customToastMsgError(this@MyBookingActivity, error, false)

            isEnableMenuHeader(true)
        }

    }

    private fun onScrollMyBookingList() {
        itemRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true
                }
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                currentItem = linearLayoutManager.childCount
                total = linearLayoutManager.itemCount
                scrollDown = linearLayoutManager.findFirstVisibleItemPosition()
                if (dy > 0) {
                    if (isScrolling && currentItem + scrollDown == total) {
                        if (total == size) {
                            isScrolling = false
                            currentPage++
                            progressBar.visibility = View.VISIBLE
                            recyclerView.scrollToPosition(if(bookingStatusKey == "completed") (itemSubCardBookingList.size - 1) else (itemCardBookingList.size - 1))
                            requestListMyBooking()
                            size += 10
                        } else {
                            progressBar.visibility = View.GONE
                        }
                    }
                }
            }
        })
    }

    private fun setClickOnMenuItem(){
        for (i in 0 until linearLayoutMenus.childCount) {
            linearLayoutMenus.getChildAt(i).setOnClickListener {
                if (progressBar.visibility == View.GONE) {
                    setClickOnMenuHeader(i)
                    when (i) {
                        0 -> {
                            bookingStatusKey = "pending"
                        }
                        1 -> {
                            bookingStatusKey = "completed"
                            isExpiredStatus = false
                        }
                        2 -> {
                            bookingStatusKey = "completed"
                            isExpiredStatus = true
                        }
                    }
                    isEnableMenuHeader(false)
                    refreshList()
                }
            }
        }
    }

    private fun setClickOnMenuHeader(backUpId : Int){
        for (i in 0 until linearLayoutMenus.childCount) {
            val rowView: View = linearLayoutMenus.getChildAt(i)
            if (rowView is TextView) {
                if (i == backUpId) {
                    rowView.background = ResourcesCompat.getDrawable(resources, R.drawable.custom_shape_line_bottom_appbar, null)
                    rowView.setTextColor(Utils.getColor(this, R.color.white))
                } else {
                    rowView.background = null
                    rowView.setTextColor(Utils.getColor(this, R.color.gray))
                }
            }
        }

    }

    private fun setViewOnResult(){
        if (bookingStatusKey == "completed") {
            noResultLayout.visibility = if (itemSubCardBookingList.isEmpty()) View.VISIBLE else View.GONE
            itemPaidExpiredRecyclerView.visibility = if (itemSubCardBookingList.isEmpty()) View.GONE else View.VISIBLE
            itemRecyclerView.visibility = View.GONE
        } else {
            noResultLayout.visibility = if (itemCardBookingList.isEmpty()) View.VISIBLE else View.GONE
            itemRecyclerView.visibility = if (itemCardBookingList.isEmpty()) View.GONE else View.VISIBLE
            itemPaidExpiredRecyclerView.visibility = View.GONE
        }
    }


    private fun customPrettyDialog(itemMainCategory: MyCategoryBookingModel) {
        prettyDialog = PrettyDialog(this)
        prettyDialog
            .setTitle(resources.getString(R.string.delete))
            .setMessage(resources.getString(R.string.are_you_sure_to_cancel_booking))
            .setIcon(R.drawable.ic_close, R.color.red)
            { prettyDialog.dismiss() }
            .addButton(
                resources.getString(R.string.no),
                R.color.pdlg_color_white,
                R.color.greenSea
            )
            {
                prettyDialog.dismiss()
            }

            .addButton(
                resources.getString(R.string.delete),
                R.color.white,
                R.color.red
            )
            {
                progressBar.visibility = View.VISIBLE
                val hashMap : HashMap<String, Any?> = HashMap()
                hashMap["booking_id"] = itemMainCategory.booking_id
                FacilityBookingWs()
                    .cancelBookingItem(this@MyBookingActivity, hashMap, itemMainCategory, callBackListener)
            }

            .setAnimationEnabled(true)
            .show()
        prettyDialog.setCancelable(false)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == RESULT_OK && requestCode == REQUEST_WEB_PAY){
            if (data != null && data.hasExtra("status")) {
                if (data.getStringExtra("status") == "success=1") {
                    Utils.popupMessage(getString(R.string.payment), getString(R.string.payment_successful), this@MyBookingActivity)
                    refreshList()
                } else {
                    Utils.popupMessage(getString(R.string.payment), getString(R.string.something_went_wrong), this@MyBookingActivity)
                }
            }
        }
    }

    private fun isEnableMenuHeader(isEnable : Boolean){
        waitingPayment.isEnabled = isEnable
        alreadyBookingTv.isEnabled = isEnable
        expiredTv.isEnabled = isEnable
    }

}