package com.eazy.daikou.ui.home.development_project.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.home_page.home_project.AllBranches
import com.squareup.picasso.Picasso

class RecommendItemPropertyAdapter(private val listPropertyItem: ArrayList<AllBranches>, private val clickListener: OnClickProperty) : RecyclerView.Adapter<RecommendItemPropertyAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.custom_item_property_recommend, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = listPropertyItem[position]
        if (model != null){
            Utils.setValueOnText(holder.textNameBuilding, model.name)
            Utils.setValueOnText(holder.textAddress, model.address)
            Utils.setValueOnText(holder.textAddressCity, model.city)

            if (model.image != null) {
                Picasso.get().load(model.image).placeholder(R.drawable.no_image)
                    .into(holder.imageBuilding)
            } else {
                holder.imageBuilding.setImageResource(R.drawable.no_image)
            }

            holder.btnProperty.setOnClickListener (CustomSetOnClickViewListener{
                clickListener.click(listPropertyItem[position])
            })
        }

    }

    override fun getItemCount(): Int {
        return listPropertyItem.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val imageBuilding : ImageView = itemView.findViewById(R.id.img_property)
        val textNameBuilding : TextView = itemView.findViewById(R.id.text_name_building)
        val textAddress : TextView = itemView.findViewById(R.id.text_address)
        val btnProperty : CardView = itemView.findViewById(R.id.btn_property)
        val textAddressCity : TextView = itemView.findViewById(R.id.text_address_city)
    }

    interface OnClickProperty {
        fun click(property: AllBranches)
    }

    fun clear() {
        val size: Int = listPropertyItem.size
        if (size > 0) {
            for (i in 0 until size) {
                listPropertyItem.removeAt(0)
            }
            notifyItemRangeRemoved(0, size)
        }
    }

}