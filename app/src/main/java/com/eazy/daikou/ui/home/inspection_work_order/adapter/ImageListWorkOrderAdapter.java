package com.eazy.daikou.ui.home.inspection_work_order.adapter;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.eazy.daikou.R;
import com.eazy.daikou.base.BaseAdapter;
import com.eazy.daikou.helper.Utils;
import com.eazy.daikou.model.work_order.ImageListModel;

import java.util.ArrayList;
import java.util.List;

public class ImageListWorkOrderAdapter extends BaseAdapter {

    private ImageView imageView;
    private ImageView imageClear;
    private final OnClearImage onClearImage;
    private final List<ImageListModel> listImage;
    private final int post;

    public ImageListWorkOrderAdapter(Context context, int post, List<?> dataList, OnClearImage onClearImage) {
        super(context, R.layout.layout_notification_pickup_image, dataList);
        this.onClearImage = onClearImage;
        this.listImage = (List<ImageListModel>) dataList;
        this.post = post;
    }

    @Override
    public View getView(View view) {
        imageClear = view.findViewById(R.id.btn_clear);
        imageView = view.findViewById(R.id.image_view);
        return view;
    }

    @Override
    public void onBindViewHold(int position, Object itemView) {
        ImageListModel bitmap = listImage.get(position);
        Glide.with(imageView).load(bitmap.getBitmapImage()).into(imageView);
        imageClear.setOnClickListener(v -> onClearImage.onClickRemove(bitmap, post));
        imageView.setOnClickListener(v -> onClearImage.onViewImage(bitmap));
    }

    public static void setImageRecyclerView(RecyclerView recyclerView, LinearLayout btnAddImg, Context context, ArrayList<ImageListModel> bitmapList, Integer post, OnClearImage onClearImage) {
        recyclerView.setLayoutManager(new GridLayoutManager(context, 1, RecyclerView.HORIZONTAL, false));
        ImageListWorkOrderAdapter pickUpAdapter = new ImageListWorkOrderAdapter(context, post, bitmapList, onClearImage);
        recyclerView.setAdapter(pickUpAdapter);
        btnAddImg.setVisibility(Utils.visibleView(bitmapList));
    }

    public interface OnClearImage{
        void onClickRemove(ImageListModel bitmap, int post);
        void onViewImage(ImageListModel bitmap);
    }
}
