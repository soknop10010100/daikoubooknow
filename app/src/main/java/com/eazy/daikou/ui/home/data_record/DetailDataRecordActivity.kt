package com.eazy.daikou.ui.home.data_record

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.request_data.request.data_record.DataRecordWS
import com.eazy.daikou.helper.DateUtil
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.CustomCategoryModel
import com.eazy.daikou.model.data_record.AllStatus
import com.eazy.daikou.model.data_record.DetailOpportunityModel
import com.eazy.daikou.model.data_record.Interesting
import com.eazy.daikou.model.data_record.Quotation
import com.eazy.daikou.model.profile.User
import com.eazy.daikou.ui.CustomMenuHeaderAdapter
import com.eazy.daikou.ui.home.data_record.adapter.AdapterOpportunityDetail
import com.eazy.daikou.ui.home.data_record.adapter.AdapterOpportunityQuotation
import com.eazy.daikou.ui.home.my_property.my_unit.PropertyMyUnitDetailActivity
import com.google.gson.Gson

class DetailDataRecordActivity : BaseActivity() {

    private lateinit var linearDownInteresting : LinearLayout
    private lateinit var linearDownRecommended : LinearLayout
    private lateinit var linearDownQuotation : LinearLayout
    private lateinit var linearInteresting : LinearLayout
    private lateinit var linearRecommended : LinearLayout
    private lateinit var linearQuotation : LinearLayout
    private lateinit var iconDownInteresting : ImageView
    private lateinit var iconDownRecommended : ImageView
    private lateinit var iconDownQuotation : ImageView
    private lateinit var customerNameTv : TextView
    private lateinit var customerPhoneTv : TextView
    private lateinit var noDataInteresting : TextView
    private lateinit var noDataRecommended : TextView
    private lateinit var noDataQuotation : TextView
    private lateinit var linearOpportunity : LinearLayout
    private lateinit var progressBar: ProgressBar
    private var idOpportunity = ""
    private lateinit var projectNameTv: TextView
    private lateinit var createDateTv: TextView
    private lateinit var opportunityNameTv: TextView
    private lateinit var recyclerInteresting: RecyclerView
    private lateinit var recyclerRecommended: RecyclerView
    private lateinit var recyclerQuotation: RecyclerView
    private lateinit var linearLayoutManager: LinearLayoutManager

    private lateinit var user : User
    private var userName = ""
    private var userPhone = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_data_record)

        initView()

        initData()

        iniAction()
    }

    private fun initView() {
        linearDownInteresting = findViewById(R.id.linearDownInteresting)
        linearDownRecommended = findViewById(R.id.linearDownRecommended)
        linearDownQuotation = findViewById(R.id.linearDownQuotation)
        linearInteresting = findViewById(R.id.linearInteresting)
        linearRecommended = findViewById(R.id.linearRecommended)
        linearQuotation = findViewById(R.id.linearQuotation)
        iconDownInteresting = findViewById(R.id.iconDownInteresting)
        iconDownRecommended = findViewById(R.id.iconDownRecommended)
        iconDownQuotation = findViewById(R.id.iconDownQuotation)
        customerNameTv = findViewById(R.id.customerNameTv)
        customerPhoneTv = findViewById(R.id.customerPhoneTv)
        noDataInteresting = findViewById(R.id.noDataInteresting)
        noDataRecommended = findViewById(R.id.noDataRecommended)
        noDataQuotation = findViewById(R.id.noDataQuotation)
        linearOpportunity = findViewById(R.id.linearOpportunity)
        progressBar = findViewById(R.id.progressItem)
        projectNameTv = findViewById(R.id.projectNameTv)
        createDateTv = findViewById(R.id.createDateTv)
        opportunityNameTv = findViewById(R.id.opportunityNameTv)
        recyclerInteresting = findViewById(R.id.recyclerInteresting)
        recyclerRecommended = findViewById(R.id.recyclerRecommended)
        recyclerQuotation = findViewById(R.id.recyclerQuotation)
    }

    private fun initData() {
        if (intent != null && intent.hasExtra("id_opportunity")) {
            idOpportunity = intent.getStringExtra("id_opportunity") as String
        }

        user = Gson().fromJson(UserSessionManagement(this).userDetail, User::class.java)
        val name = user.name
        val phone = user.phoneNumber

        if (name != null) { userName = name }

        if (phone != null) { userPhone = phone }

    }

    private fun iniAction() {
        Utils.customOnToolbar(this, getString(R.string.opportunity_detail)) { finish() }
        requestServiceDetailAPI()

        linearDownInteresting.setOnClickListener( CustomSetOnClickViewListener{
            loadViewDropList(recyclerInteresting, iconDownInteresting, linearInteresting)
        })

        linearDownRecommended.setOnClickListener( CustomSetOnClickViewListener{
            loadViewDropList(recyclerRecommended, iconDownRecommended, linearRecommended)
        })

        linearDownQuotation.setOnClickListener( CustomSetOnClickViewListener{
            loadViewDropList(recyclerQuotation, iconDownQuotation, linearQuotation)
        })
    }

    private fun setMenuStepAdapter(context: Activity, listStepMenu : ArrayList<AllStatus>){
        val list = ArrayList<CustomCategoryModel>()
        for (item in listStepMenu){
            list.add(CustomCategoryModel(item.status, item.status_display, item.is_active))
        }

        CustomMenuHeaderAdapter.setMenuStepAdapter(context, "status", list, object : CustomMenuHeaderAdapter.ClickCallBackListener{
            override fun onClickCallBack(item: CustomCategoryModel) {}
        })
    }

    private fun loadViewDropList(recyclerViewDrop: RecyclerView, iconDrop : ImageView, linearData : LinearLayout){
        if(recyclerViewDrop.visibility == View.GONE){
            recyclerViewDrop.visibility = View.VISIBLE
            linearData.visibility = View.VISIBLE
            iconDrop.setImageResource(R.drawable.ic_arrow_next)

        } else {
            recyclerViewDrop.visibility = View.GONE
            iconDrop.setImageResource(R.drawable.ic_drop_down)
            linearData.visibility = View.GONE
        }
    }

    private fun setRecyclerView(recyclerView: RecyclerView, listDetailInteresting: ArrayList<Interesting>) {
        linearLayoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = linearLayoutManager
        val adapterOpportunity = AdapterOpportunityDetail(this, listDetailInteresting, callBackItemInteresting)
        recyclerView.adapter = adapterOpportunity
    }

    private val callBackItemInteresting = object :
        AdapterOpportunityDetail.CallBackClickItemListener {
        override fun clickItemListener(interesting: Interesting) {
            val intent = Intent(this@DetailDataRecordActivity, PropertyMyUnitDetailActivity::class.java)
            intent.putExtra("id", interesting.unit!!.id)
            intent.putExtra("action_type", "data_record_unit")
            startActivity(intent)
        }
    }

    private fun setRecyclerViewQuotation(detailList: ArrayList<Quotation>) {
        linearLayoutManager = LinearLayoutManager(this)
        recyclerQuotation.layoutManager = linearLayoutManager
        val adapterQuotation = AdapterOpportunityQuotation(this, detailList, callBackToQuotation)
        recyclerQuotation.adapter = adapterQuotation
    }

    private val callBackToQuotation  = object :
        AdapterOpportunityQuotation.CallBackItemClickListener {
        override fun clickItemListener(quotation: Quotation) {
            val intent = Intent(this@DetailDataRecordActivity, DetailQuotationDRActivity:: class.java)
            intent.putExtra("id_quotation" , quotation.id)
            startActivity(intent)
        }
    }

    private fun requestServiceDetailAPI() {
        progressBar.visibility = View.VISIBLE
        DataRecordWS().getDetailOpportunityWS(this, idOpportunity, callBackServiceDetail)
    }

    private val callBackServiceDetail = object : DataRecordWS.OnCallBackDetailOpportunityListener {
        override fun onSuccess(detailOpportunity: DetailOpportunityModel) {
            progressBar.visibility = View.GONE
            linearOpportunity.visibility = View.VISIBLE

            setDataDetail(detailOpportunity)

            setMenuStepAdapter(this@DetailDataRecordActivity, detailOpportunity.all_status)

            if (detailOpportunity.interestings.size > 0 ){
                setRecyclerView(recyclerInteresting, detailOpportunity.interestings)
            } else {
                noDataInteresting.visibility = View.VISIBLE
            }

            if (detailOpportunity.recommends.size > 0) {
                setRecyclerView(recyclerRecommended, detailOpportunity.recommends)
            } else {
                noDataRecommended.visibility = View.VISIBLE
            }

            if (detailOpportunity.quotations.size > 0){
                setRecyclerViewQuotation(detailOpportunity.quotations)
            } else {
                noDataQuotation.visibility = View.VISIBLE
            }
        }

        override fun onFailed(error: String) {
            progressBar.visibility = View.GONE
            Utils.customToastMsgError(this@DetailDataRecordActivity, error, false)
        }
    }

    private fun setDataDetail(detailOpportunity: DetailOpportunityModel) {
        customerNameTv.text = userName
        customerPhoneTv.text = userPhone
        projectNameTv.text = if (detailOpportunity.account!!.name != null) detailOpportunity.account!!.name else "- - -"
        createDateTv.text = if (detailOpportunity.created_at != null) String.format("%s %s", DateUtil.formatDateComplaint(detailOpportunity.created_at), DateUtil.formatDateToConvertOnlyTimeZoneNoSecond(detailOpportunity.created_at)) else ". . . "

        opportunityNameTv.text = if (detailOpportunity.name != null) detailOpportunity.name else "- - -"

    }
}