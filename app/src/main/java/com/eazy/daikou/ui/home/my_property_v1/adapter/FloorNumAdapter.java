package com.eazy.daikou.ui.home.my_property_v1.adapter;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.eazy.daikou.R;
import com.eazy.daikou.model.floor_plan.FloorListModel;

import android.widget.TextView;

import java.util.List;

public class FloorNumAdapter extends RecyclerView.Adapter<FloorNumAdapter.ItemViewHolder>{

    private final List<FloorListModel> numFloorList;
    private final ClickCallBackListener callBackListener;
    private final Context context;

    public FloorNumAdapter(Context context, List<FloorListModel> numFloorList, ClickCallBackListener callBackListener) {
        this.numFloorList = numFloorList;
        this.callBackListener = callBackListener;
        this.context = context;
    }

    @NonNull
    @Override
    public FloorNumAdapter.ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rowView = LayoutInflater.from(parent.getContext()).inflate(R.layout.num_floor_btn_model, parent, false);
        return new ItemViewHolder(rowView);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(@NonNull FloorNumAdapter.ItemViewHolder holder, int position) {
        FloorListModel floorListModel = numFloorList.get(position);
        if (floorListModel != null){
            holder.numFloor.setText(floorListModel.getFloorNo());
            holder.numFloor.setTextColor(ContextCompat.getColor(context,R.color.white));

            if (floorListModel.isClick()){
                holder.numFloor.setBackground(context.getDrawable(R.drawable.bg_shap_color_appbar));
                holder.numFloor.setTextColor(ContextCompat.getColor(context,R.color.white));
            } else {
                holder.numFloor.setBackground(context.getDrawable(R.drawable.card_view_shape));
                holder.numFloor.setTextColor(ContextCompat.getColor(context,R.color.black));
            }

            holder.cardViewLayout.setOnClickListener(view -> callBackListener.clickCallBack(floorListModel));

        }

    }

    @Override
    public int getItemCount() {
        return numFloorList.size();
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder{

        private final TextView numFloor;
        private final CardView cardViewLayout;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            numFloor = itemView.findViewById(R.id.btnNumFl);
            cardViewLayout = itemView.findViewById(R.id.cardViewLayout);
        }
    }


    public interface ClickCallBackListener{
        void clickCallBack(FloorListModel floorListModel);
    }
}
