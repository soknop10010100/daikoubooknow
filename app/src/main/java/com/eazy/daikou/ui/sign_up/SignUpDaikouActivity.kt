package com.eazy.daikou.ui.sign_up

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.text.InputType
import android.util.Patterns
import android.view.View
import android.widget.*
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts.StartActivityForResult
import androidx.cardview.widget.CardView
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.base.MockUpData
import com.eazy.daikou.helper.AppAlertCusDialog
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.request_data.request.profile_ws.ProfileWs
import com.eazy.daikou.request_data.request.sign_up_v2.CheckAccountExitsWs
import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog
import com.google.android.material.textfield.TextInputEditText
import com.rilixtech.widget.countrycodepicker.CountryCodePicker
import java.text.SimpleDateFormat
import java.util.*

class SignUpDaikouActivity : BaseActivity() {

    private lateinit var birthDateTV: TextView
    private lateinit var nationalityTv: TextView
    private lateinit var genderTv: TextView

    private lateinit var firstNameEd: EditText
    private lateinit var lastNameEd: EditText
    private lateinit var emailEd: EditText
    private lateinit var passwordEd: EditText
    private lateinit var confirmPasswordEd: EditText
    private lateinit var phoneEd: TextInputEditText

    private lateinit var submitButton: CardView
    private lateinit var countryCodePicker: CountryCodePicker

    private lateinit var genderClick: RelativeLayout
    private lateinit var birthDayClick: RelativeLayout
    private lateinit var nationalityClick: RelativeLayout

    private var dateBirth = ""
    private var gender = "m"
    private var nationalityID = ""

    private lateinit var progressBar: View
    private lateinit var app: AppAlertCusDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up_v2)

        initView()

        initAction()
    }


    private fun initView() {
        app = AppAlertCusDialog()
        Utils.customOnToolbar(this, getString(R.string.sign_up).uppercase(Locale.getDefault())) { finish() }
        birthDateTV = findViewById(R.id.birthDateTv)
        nationalityTv = findViewById(R.id.nationalityTv)
        genderTv = findViewById(R.id.genderTv)

        submitButton = findViewById(R.id.submitBtn)
        firstNameEd = findViewById(R.id.firstNameTv)
        lastNameEd = findViewById(R.id.lastNameTv)
        passwordEd = findViewById(R.id.passwordTv)
        emailEd = findViewById(R.id.emailTv)
        confirmPasswordEd = findViewById(R.id.conPasswordTv)

        phoneEd = findViewById(R.id.txtPhoneNumber)

        countryCodePicker = findViewById(R.id.ccp)
        countryCodePicker.setDefaultCountryUsingNameCode(country_code)

        genderClick = findViewById(R.id.genderLayout)
        birthDayClick = findViewById(R.id.birthDateLayout)
        nationalityClick = findViewById(R.id.nationalityLayout)
        progressBar = findViewById(R.id.progressItem)
        progressBar.visibility = View.GONE

        //Set Value
        genderTv.setText(R.string.male)
        phoneEd.hint = resources.getString(R.string.phone_number_)

        Utils.setEdInputTypeMode(passwordEd, InputType.TYPE_TEXT_VARIATION_PASSWORD)
        Utils.setEdInputTypeMode(confirmPasswordEd, InputType.TYPE_TEXT_VARIATION_PASSWORD)

        passwordEd.hint = resources.getString(R.string.password_6_digits)
        confirmPasswordEd.hint = resources.getString(R.string.confirm_password)

    }


    private fun initAction() {
        submitButton.setOnClickListener(CustomSetOnClickViewListener { validateInformationUser() } )

        genderClick.setOnClickListener(CustomSetOnClickViewListener { clickGender() })

        birthDayClick.setOnClickListener(CustomSetOnClickViewListener { clickSelectDate() } )

        nationalityClick.setOnClickListener(CustomSetOnClickViewListener { clickSelectNation() } )

    }

    //validate information user input
    private fun validateInformationUser() {
        when {
            firstNameEd.text.toString().isEmpty() -> {
                app.showDialog(this, getString(R.string.first_name_is_require))
            }
            lastNameEd.text.toString().isEmpty() -> {
                app.showDialog(this, getString(R.string.last_name_is_require))
            }
            dateBirth.isEmpty() -> {
                app.showDialog(this, getString(R.string.birthday_is_require))
            }
            nationalityID.isEmpty() -> {
                app.showDialog(this, getString(R.string.nationality_is_require))
            }
            emailEd.text.toString().isEmpty() -> {
                app.showDialog(this, getString(R.string.email_is_require))
            }
            !Patterns.EMAIL_ADDRESS.matcher(emailEd.text.toString()).matches() -> {
                app.showDialog(this, getString(R.string.use_your_email_address))
            }
            phoneEd.text!!.toString().isEmpty() -> {
                app.showDialog(this, getString(R.string.phone_number_is_require))
            }
            passwordEd.text.toString()
                .isNotEmpty() && passwordEd.text.toString().length < 6 -> {
                app.showDialog(this, getString(R.string.password_6_digits))

            }
            passwordEd.text.toString().isEmpty() -> {
                app.showDialog(this, getString(R.string.password_is_require))
            }
            confirmPasswordEd.text.toString().isEmpty() -> {
                app.showDialog(this, getString(R.string.confirm_password_is_require))
            }
            passwordEd.text.toString() != confirmPasswordEd.text.toString() -> {
                app.showDialog(this, getString(R.string.password_and_confirm_password_must_the_same))
            }
            else -> {
                checkEmailExist()
            }
        }
    }

    //check email is exist or not
    private fun checkEmailExist() {
        progressBar.visibility = View.VISIBLE
        // check email
        CheckAccountExitsWs().checkUserNameExist(
            this,
            emailEd.text.toString().trim(),
            object : CheckAccountExitsWs.OnResponseUserNameExist {
                override fun onSuccessFull(message: String) {
                    progressBar.visibility = View.GONE
                    checkPhoneNumberExist()
                }

                override fun onFailed(msg: String) {
                    progressBar.visibility = View.GONE
                    app.showDialog(
                        this@SignUpDaikouActivity,
                        getString(R.string.this_email_already_exist)
                    )

                }

                override fun onInternetConnection(mess: String) {
                    progressBar.visibility = View.GONE
                    Toast.makeText(this@SignUpDaikouActivity, mess, Toast.LENGTH_SHORT).show()
                }

            }
        )

    }

    //check phone number is exist or not
    private fun checkPhoneNumberExist() {
        val phoneNumber = countryCodePicker.defaultCountryCode +
                if (phoneEd.text.toString()[0] == '0') phoneEd.text.toString()
                    .substring(1, phoneEd.text.toString().length)
                else phoneEd.text.toString()

        progressBar.visibility = View.GONE
        Utils.logDebug("f8888", phoneNumber)
        CheckAccountExitsWs().checkUserNameExist(
            this,
            phoneNumber,
            object : CheckAccountExitsWs.OnResponseUserNameExist {
                override fun onSuccessFull(message: String) {
                    progressBar.visibility = View.GONE
                    requestService()
                }

                override fun onFailed(msg: String) {
                    progressBar.visibility = View.GONE
                    app.showDialog(
                        this@SignUpDaikouActivity,
                        getString(R.string.this_phone_number_already_exist)
                    )
                }

                override fun onInternetConnection(mess: String) {
                    progressBar.visibility = View.GONE
                    Toast.makeText(this@SignUpDaikouActivity, mess, Toast.LENGTH_SHORT).show()
                }

            }
        )

    }

    // create user
    private fun requestService() {
        val hashMap = HashMap<String, Any>()
        hashMap["first_name"] = firstNameEd.text.toString()
        hashMap["last_name"] = lastNameEd.text.toString()
        hashMap["gender"] = gender
        hashMap["dob"] = dateBirth
        hashMap["nationality_code"] = nationalityID
        hashMap["email"] = emailEd.text.toString().trim()
        hashMap["phone_code"] = countryCodePicker.defaultCountryCode
        hashMap["phone_no"] = phoneEd.text.toString().trim()
        hashMap["password"] = passwordEd.text.toString().trim()

        MockUpData.setHashMapAccount(hashMap)

        requestOTPCode()
    }

    //request otp code
    private fun requestOTPCode() {
        progressBar.visibility = View.VISIBLE
        val hashMap = HashMap<String, Any>()
        val phoneNumber = countryCodePicker.defaultCountryCode +
                if (phoneEd.text.toString()[0] == '0') phoneEd.text.toString()
                    .substring(1, phoneEd.text.toString().length)
                else phoneEd.text.toString()

        hashMap["username"] = phoneNumber
        ProfileWs().sendAlertVerifyPinCodeEmailPhone(
            this,
            hashMap,
            object : ProfileWs.CallBackUpdateEmailPhoneListener {
                override fun onSuccessVerifyCode(pinCode: String?) {
                    progressBar.visibility = View.GONE
                    val intent =
                        Intent(this@SignUpDaikouActivity, VerifySmsCodeV2Activity::class.java)
                    intent.putExtra("phone", phoneNumber)
                    intent.putExtra("phone_code", countryCodePicker.defaultCountryCode)
                    intent.putExtra("hashmap_request_code", hashMap)
                    intent.putExtra("pin_code", pinCode)
                    startActivity(intent)
                }

                override fun onFailed(error: String?) {
                    progressBar.visibility = View.GONE
                    app.showDialog(this@SignUpDaikouActivity, error)
                }

            })
    }

    //let user select gender
    private fun clickGender() {
        val alertDialogBuilder = AlertDialog.Builder(this)

        alertDialogBuilder.setTitle(getString(R.string.sealect_gender))

        val genders = arrayOf(getString(R.string.male), getString(R.string.female))
        var itemChecked: Int

        itemChecked = if (gender == "m") 0 else 1
        alertDialogBuilder.setCancelable(true)

        alertDialogBuilder.setSingleChoiceItems(genders, itemChecked) { dialog, which ->
            itemChecked = which

            if (itemChecked == 0) {
                gender = "m"
                genderTv.text = getString(R.string.male)
            } else {
                gender = "f"
                genderTv.text = getString(R.string.female)
            }

            dialog.dismiss()
        }

        val dialog = alertDialogBuilder.create()
        dialog.show()
    }


    //let user select date of birth
    private fun clickSelectDate() {
        SingleDateAndTimePickerDialog.Builder(this)
            .bottomSheet()
            .curved()
            .displayMinutes(false)
            .displayHours(false)
            .displayDays(false)
            .displayMonth(true)
            .displayYears(true)
            .displayDaysOfMonth(true)
            .listener {
                val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.US)
                val stringFormat = dateFormat.format(it)
                dateBirth = stringFormat
                birthDateTV.text = dateBirth
            }.display()
    }


    private val resultLauncher = registerForActivityResult(StartActivityForResult()) { result: ActivityResult ->
        if (result.resultCode == RESULT_OK) {
            if (result.data != null) {
                val data = result.data
                val name = data!!.getStringExtra("name_national")
                val id = data.getStringExtra("id_national")
                nationalityTv.text = name
                nationalityID = id!!
            }
        }
    }

    //let user select country
    private fun clickSelectNation() {
        val intent = Intent(this@SignUpDaikouActivity, NationalityActivity::class.java)
        intent.putExtra("action", "nationality")
        resultLauncher.launch(intent)
    }
}