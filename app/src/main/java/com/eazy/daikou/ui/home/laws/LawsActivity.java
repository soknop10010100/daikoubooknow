package com.eazy.daikou.ui.home.laws;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.eazy.daikou.R;
import com.eazy.daikou.base.BaseActivity;
import com.eazy.daikou.request_data.request.book_quote_ws.RuleAndNewsWs;
import com.eazy.daikou.helper.Utils;
import com.eazy.daikou.model.law.LawsModel;
import com.eazy.daikou.model.law.RuleNewModel;
import com.eazy.daikou.ui.home.laws.adapter.LawsAdapter;

import java.util.ArrayList;
import java.util.List;

public class LawsActivity extends BaseActivity {

    private ProgressBar progressBar;
    private RecyclerView lawsRecyclerView;
    private LinearLayoutManager linearLayoutManager;
    private CardView noItem;
    private LawsAdapter lawsAdapter;
    private SwipeRefreshLayout refreshLayout;

    private final List<LawsModel> lawsList = new ArrayList<>();

    private int currentItem, total, scrollDown, currentPage = 1, size = 20;
    private Boolean isScrolling = true ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_laws_detail);

        // init view
        progressBar = findViewById(R.id.progressItem);
        lawsRecyclerView = findViewById(R.id.listLawsDetail);
        noItem = findViewById(R.id.noItemListLaws);
        findViewById(R.id.layoutMap).setVisibility(View.INVISIBLE);
        Utils.customOnToolbar(this, getResources().getString(R.string.laws), this::onBackPressed);

        // init action
        linearLayoutManager = new LinearLayoutManager(this);
        lawsRecyclerView.setLayoutManager(linearLayoutManager);

        initRuleNewsRecyclerView();

        initRecyclerViewScroll();

        refreshLayout = findViewById(R.id.swipe_layouts);
        refreshLayout.setColorSchemeResources(R.color.colorPrimary);
        refreshLayout.setOnRefreshListener(this::refreshList);

    }

    private void refreshList(){
        currentPage = 1;
        size = 20;
        isScrolling = true;
        if (lawsAdapter != null)    lawsAdapter.clear();
        requestService();
    }

    private void initRuleNewsRecyclerView(){
        lawsAdapter = new LawsAdapter(this, "", lawsList, clickCallBackLaws);
        lawsRecyclerView.setAdapter(lawsAdapter);
        requestService();
    }

    private final LawsAdapter.ClickCallBackListener clickCallBackLaws = lawsModel -> {
        Intent intent = new Intent(LawsActivity.this, LawsDetailActivity.class);
        intent.putExtra("category_id", lawsModel.getId());
        intent.putExtra("category_name", lawsModel.getName());
        startActivity(intent);
    };

    private void requestService(){
        progressBar.setVisibility(View.VISIBLE);
        new RuleAndNewsWs().getLaws(this, currentPage, 20, ruleNewsCallBackListener);
    }

    private final RuleAndNewsWs.RuleNewsCallBackListener ruleNewsCallBackListener = new RuleAndNewsWs.RuleNewsCallBackListener() {
        @Override
        public void getRuleNewsModel(List<RuleNewModel> ruleNewsModels) {}

        @SuppressLint("NotifyDataSetChanged")
        @Override
        public void getLaws(List<LawsModel> lawsModelList) {
            progressBar.setVisibility(View.GONE);
            lawsList.addAll(lawsModelList);
            refreshLayout.setRefreshing(false);
            lawsAdapter.notifyDataSetChanged();

            noItem.setVisibility(lawsList.size() == 0 ? View.VISIBLE : View.GONE);
            lawsRecyclerView.setVisibility(lawsList.size() > 0 ? View.VISIBLE : View.GONE);
        }

        @Override
        public void onFailed(String error) {
            refreshLayout.setRefreshing(false);
            progressBar.setVisibility(View.GONE);
            Utils.customToastMsgError(LawsActivity.this, error, false);
        }
    };

    private void initRecyclerViewScroll() {
        lawsRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true;
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                currentItem = linearLayoutManager.getChildCount();
                total = linearLayoutManager.getItemCount();
                scrollDown = linearLayoutManager.findFirstVisibleItemPosition();
                if (dy > 0) {
                    if (isScrolling && (currentItem + scrollDown == total)) {
                        if (total == size) {
                            isScrolling = false;
                            currentPage++;
                            progressBar.setVisibility(View.VISIBLE);
                            recyclerView.scrollToPosition(lawsList.size() - 1);
                            initRuleNewsRecyclerView();
                            size += 20;
                        }
                    }
                }
            }
        });
    }

}