package com.eazy.daikou.ui.home.my_property.my_unit;

import android.content.Context;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.eazy.daikou.base.BaseActivity;
import com.eazy.daikou.base.BaseBottomSheetDialogFragment;
import com.eazy.daikou.helper.Utils;
import com.eazy.daikou.model.my_property.my_property.Type;
import com.eazy.daikou.R;

import org.jetbrains.annotations.NotNull;

import java.util.Locale;

public class DetailUnitBottomSheetFragment extends BaseBottomSheetDialogFragment {

    private TextView roomStyleTv, bedRoomTv, bathRoomTv, directionTv, privateAreaTv;
    private TextView floorNoTv, livingRoomTv, maidRoomTv, totalAreaTv, commonAreaTv;
    private Type unitDetailModel;
    private String floorNo;

    public static DetailUnitBottomSheetFragment newInstance(String floorNo, Type unitDetailModel) {
        DetailUnitBottomSheetFragment fragment = new DetailUnitBottomSheetFragment();
        Bundle args = new Bundle();
        args.putSerializable("unit_detail", unitDetailModel);
        args.putString("floor_no", floorNo);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_detail_unit_b_s, container, false);

        initView(root);

        commonAreaTv();

        return root;
    }

    private void commonAreaTv() {
        if (getArguments() != null && getArguments().containsKey("unit_detail")) {
            unitDetailModel = (Type) getArguments().getSerializable("unit_detail");
        }

        if (getArguments() != null && getArguments().containsKey("floor_no")) {
            floorNo = getArguments().getString("floor_no");
        }

        if (unitDetailModel != null){
            Utils.setValueOnText(roomStyleTv, unitDetailModel.getStyle());
            Utils.setValueOnText(bedRoomTv, unitDetailModel.getTotal_bedroom());
            Utils.setValueOnText(bathRoomTv, unitDetailModel.getTotal_bathroom());
            Utils.setValueOnText(privateAreaTv, unitDetailModel.getTotal_private_area());
            Utils.setValueOnText(floorNoTv, floorNo);
            Utils.setValueOnText(livingRoomTv, unitDetailModel.getTotal_living_room());
            Utils.setValueOnText(maidRoomTv, unitDetailModel.getTotal_maid_room());
            Utils.setValueOnText(totalAreaTv, unitDetailModel.getTotal_area_sqm());
            Utils.setValueOnText(commonAreaTv, unitDetailModel.getTotal_common_area());

            directionTv.setText("");
            for (int i = 0; i < unitDetailModel.getDirection().size(); i++) {
                directionTv.append(String.format("%s %s", unitDetailModel.getDirection().get(i).toUpperCase(Locale.ROOT), ", "));
            }
        }
    }

    private void initView(View view) {
        roomStyleTv = view.findViewById(R.id.roomStyleTv);
        bedRoomTv = view.findViewById(R.id.bedRoomTv);
        bathRoomTv = view.findViewById(R.id.bathroomTv);
        directionTv = view.findViewById(R.id.directionTv);
        privateAreaTv = view.findViewById(R.id.privateAreaTv);
        floorNoTv = view.findViewById(R.id.floorTv);
        livingRoomTv = view.findViewById(R.id.livingRoomTv);
        maidRoomTv = view.findViewById(R.id.maidRoomTv);
        totalAreaTv = view.findViewById(R.id.totalAreaTv);
        commonAreaTv = view.findViewById(R.id.commentAreaTv);

        view.findViewById(R.id.close).setOnClickListener(v -> dismiss());
    }

    private Context mActivity;
    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        if (mActivity == null && context instanceof BaseActivity){
            mActivity = context;
        }
    }
}