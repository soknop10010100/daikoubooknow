package com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.eazy.daikou.R;
import com.eazy.daikou.model.utillity_tracking.model.UnitImgModel;

import java.util.ArrayList;

public class MyUnitsAdapter extends RecyclerView.Adapter<MyUnitsAdapter.ViewHolder> {

    private final ArrayList<UnitImgModel> listUnit;
    private final ItemClickOnService itemClick;
    private final Context context;
    private int mSelectedPos = -1;
    private int stat = -1;

    public MyUnitsAdapter(ArrayList<UnitImgModel> listUnit, int post, Context context, ItemClickOnService itemClick) {
        this.listUnit = listUnit;
        this.itemClick = itemClick;
        this.context = context;
        this.stat = post;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view  = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_adapter_unit_item,parent,false);
        return new ViewHolder(view);
    }

    @SuppressLint("NotifyDataSetChanged")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        UnitImgModel unit = listUnit.get(position);
        if(unit != null){
            holder.nameCategoryTv.setText(unit.getUnitNo());
            holder.itemView.setOnClickListener(view -> {
                mSelectedPos = position;
                stat = mSelectedPos;
                itemClick.onClickItem(unit.getUnitNo(),unit.getUnitId(),unit.getPropertyUnitTypeIdFk(),position,stat);
                notifyDataSetChanged();
            });

            if (position == mSelectedPos||position == stat){
                holder.nameCategoryTv.setTextColor(Color.WHITE);
                holder.linearLayout.setBackgroundResource(R.drawable.card_view_shape_color_app);
            } else {
                holder.linearLayout.setBackgroundResource(R.drawable.shape_transparent_bg);
                holder.nameCategoryTv.setTextColor(Color.GRAY);

            }

        }

    }

    @Override
    public int getItemCount() {
        return listUnit.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView nameCategoryTv;
        private final LinearLayout linearLayout;
        public ViewHolder(View view) {
            super(view);
            linearLayout = view.findViewById(R.id.linear_card);
            nameCategoryTv = view.findViewById(R.id.text_category);
        }
    }

    public interface ItemClickOnService{
        void onClickItem(String unitNo,String unitId,String unitTypeId,int position,int stat);
    }
}