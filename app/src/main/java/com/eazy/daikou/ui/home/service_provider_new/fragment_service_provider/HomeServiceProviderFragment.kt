package com.eazy.daikou.ui.home.service_provider_new.fragment_service_provider

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.CompositePageTransformer
import androidx.viewpager2.widget.MarginPageTransformer
import androidx.viewpager2.widget.ViewPager2
import com.denzcoskun.imageslider.ImageSlider
import com.denzcoskun.imageslider.models.SlideModel
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseFragment
import com.eazy.daikou.request_data.request.service_provider_ws.ServiceProviderNewViewV3WS
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.service_provider.*
import com.eazy.daikou.ui.home.service_provider.ServiceProviderMainCategoryActivity
import com.eazy.daikou.ui.home.service_provider_new.activity.ServiceProviderDetailActivity
import com.eazy.daikou.ui.home.service_provider_new.activity.ServiceProviderViewCategoryListActivity
import com.eazy.daikou.ui.home.service_provider_new.activity.ServiceProviderListCompanyActivity
import com.eazy.daikou.ui.home.service_provider_new.adapter_service_provider.CategoryServiceProviderAdapter
import com.eazy.daikou.ui.home.service_provider_new.adapter_service_provider.MainItemAdapter
import com.eazy.daikou.ui.home.service_provider_new.adapter_service_provider.SlideImageAdapter
import kotlin.math.abs

class HomeServiceProviderFragment : BaseFragment() {

    private lateinit var progressBar: ProgressBar
    private lateinit var viewPagerImageSlider: ViewPager2
    private val sliderHandler: Handler = Handler(Looper.getMainLooper())
    private lateinit var recyclerCategory: RecyclerView
    private lateinit var recyclerViewMain: RecyclerView
    private lateinit var gridLayoutManager: GridLayoutManager
    private lateinit var linearLayoutManager: LinearLayoutManager
    private var mainItemList: ArrayList<MainItemModel> = ArrayList()
    private var subCategoryModelList : ArrayList<SubCategory> = ArrayList()
    private var sliderImageList: ArrayList<ImageSliderModel> = ArrayList()

    private var sessionManagement: UserSessionManagement? = null
    private var userId = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_home_service_provider, container, false)

        initView(view)

        initAction()

        return view
    }

    private fun initView(view: View) {
        progressBar = view.findViewById(R.id.progressItem)
        recyclerCategory = view.findViewById(R.id.recyclerCategory)
        recyclerViewMain = view.findViewById(R.id.recyclerViewMain)
        viewPagerImageSlider = view.findViewById(R.id.viewPagerImageSlider)

        view.findViewById<ImageView>(R.id.btnBack).setOnClickListener { mActivity.finish() }

        val imageSlider: ImageSlider = view.findViewById(R.id.slider)
        val sliderTravelTalk: ImageSlider = view.findViewById(R.id.sliderTravelTalk)
        val slideModels: ArrayList<SlideModel> = ArrayList()
        slideModels.add(SlideModel("https://i.pinimg.com/474x/15/0c/d7/150cd7dec5567c7c759dc0f899d5ac5a.jpg"))
        slideModels.add(SlideModel("https://free-psd-templates.com/wp-content/uploads/2018/07/free_psd_preview_last_15-free-hotel-banners-collection-in-psd.jpg"))
        slideModels.add(SlideModel("http://glamourhotel.vn/upload/images/Banner20-01(1).jpg"))
        slideModels.add(SlideModel("https://indiater.com/wp-content/uploads/2019/03/sea-view-hotel-miami-beach-holiday-package-banner.jpg"))

        Utils.setViewSliderImage(imageSlider, slideModels)
        Utils.setViewSliderImage(sliderTravelTalk, slideModels)
    }

    private fun initAction() {

        sessionManagement = UserSessionManagement(mActivity)
        userId = sessionManagement!!.userId

        requestServiceHomePageAPI ()

    }

    private fun requestServiceHomePageAPI(){
        progressBar.visibility = View.VISIBLE
        ServiceProviderNewViewV3WS().getListHomePageProviderWS(mActivity, "1", "10", userId, homePageServiceCallBack)
    }
    private val homePageServiceCallBack : ServiceProviderNewViewV3WS.CallBackHomePageListener = object : ServiceProviderNewViewV3WS.CallBackHomePageListener{
        override fun onSuccessFul(homePageProvider: ArrayList<MainItemModel>, subCategoryList : ArrayList<SubCategory>, sliderImage: ArrayList<ImageSliderModel>) {
            progressBar.visibility = View.GONE
            mainItemList.addAll(homePageProvider)
            subCategoryModelList.addAll(subCategoryList)
            sliderImageList.addAll(sliderImage)

            if (sliderImage.size > 0)   slideImageView(sliderImage)

            categoryServiceProvider()

            companyServiceProvider(mainItemList)
        }

        override fun onLoadFailed(error: String) {
            progressBar.visibility = View.GONE
            Utils.customToastMsgError(mActivity, error, false)
        }
    }
    private fun categoryServiceProvider(){
        gridLayoutManager = GridLayoutManager(mActivity, 4)
        recyclerCategory.layoutManager = gridLayoutManager
        val adapterCategory =  CategoryServiceProviderAdapter(mActivity, subCategoryModelList, callBackCategory)
        recyclerCategory.adapter = adapterCategory
    }

    private val callBackCategory : CategoryServiceProviderAdapter.CallBackItemCategoryLister = object : CategoryServiceProviderAdapter.CallBackItemCategoryLister{
        override fun itemClickListener(category: SubCategory) {
            if (category.id == "more"){
                val list : ArrayList<SubCategory> = ArrayList()
                for (item in subCategoryModelList){
                    if (item.id != category.id){
                        list.add(item)
                    }
                }
                val intent = Intent(mActivity, ServiceProviderMainCategoryActivity::class.java)
                intent.putExtra("all_category", list)
                startActivity(intent)
            } else {
                val intent = Intent(mActivity, ServiceProviderViewCategoryListActivity::class.java)
                intent.putExtra("user_id", userId)
                intent.putExtra("name_category",category.title)
                intent.putExtra("data_main_model", mainItemList)
                intent.putExtra("data_image_slider",sliderImageList)
                startActivity(intent)
            }
        }
    }
    private fun companyServiceProvider(mainItemListModel: ArrayList<MainItemModel>){
        linearLayoutManager = LinearLayoutManager(mActivity)
        recyclerViewMain.layoutManager = linearLayoutManager
        val adapterMain = MainItemAdapter(mActivity, mActivity, mainItemListModel, callBackMain)
        recyclerViewMain.adapter = adapterMain

    }

    private val callBackMain: MainItemAdapter.CallBackItemSeeAll = object : MainItemAdapter.CallBackItemSeeAll{
        override fun clickSeeAllListener(mainItem: MainItemModel) {
            when (mainItem.action) {
                "Company" -> {
                    val intent = Intent(mActivity, ServiceProviderListCompanyActivity::class.java)
                    startActivity(intent)
                }
                "Feature" -> {
                    val intent = Intent(mActivity, ServiceProviderDetailActivity::class.java)
                    startActivity(intent)
                }
                "Service" -> {
                    val intent = Intent(mActivity, ServiceProviderDetailActivity::class.java)
                    startActivity(intent)
                }
            }
        }

        override fun clickCompanyToActivityListener(companyData: SubCategory) {
            val intent = Intent(mActivity, ServiceProviderListCompanyActivity::class.java)
            startActivity(intent)
        }

        override fun clickServiceProviderListener(Service: SubCategory) {
            val intent = Intent(mActivity, ServiceProviderDetailActivity::class.java)
            startActivity(intent)
        }
    }

    private fun slideImageView(sliderImage: ArrayList<ImageSliderModel>){
        viewPagerImageSlider.adapter = SlideImageAdapter(sliderImage, viewPagerImageSlider)
        viewPagerImageSlider.clipToPadding = false
        viewPagerImageSlider.clipChildren = false
        viewPagerImageSlider.offscreenPageLimit = 3
        viewPagerImageSlider.getChildAt(0).overScrollMode = RecyclerView.OVER_SCROLL_NEVER;
        val compositePageTransformer = CompositePageTransformer()
        compositePageTransformer.addTransformer(MarginPageTransformer(40))

        compositePageTransformer.addTransformer { page, position ->
            val r = 1 - abs(position)
            page.scaleY = 0.85f + r * 0.15f
        }

        viewPagerImageSlider.setPageTransformer(compositePageTransformer)

        viewPagerImageSlider.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                sliderHandler.removeCallbacks(sliderRunnable)
                sliderHandler.postDelayed(sliderRunnable, 3000)// slide duration 4 seconds
            }
        })
    }

    private val sliderRunnable =
        Runnable { viewPagerImageSlider.currentItem = viewPagerImageSlider.currentItem + 1 }

    override fun onPause() {
        super.onPause()
        sliderHandler.removeCallbacks(sliderRunnable)
    }

    override fun onResume() {
        super.onResume()
        sliderHandler.postDelayed(sliderRunnable, 3000)
    }
}