package com.eazy.daikou.ui.home.home_menu

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.Constant
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.base.MockUpData
import com.eazy.daikou.helper.AppAlertCusDialog
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.helper.web.WebsiteActivity
import com.eazy.daikou.model.hr.ItemHR
import com.eazy.daikou.model.profile.User
import com.eazy.daikou.ui.ScannerQRCodeAllActivity
import com.eazy.daikou.ui.home.book_now.booking_hotel.HotelBookingMainActivity
import com.eazy.daikou.ui.home.facility_booking.FacilityBookingActivity
import com.eazy.daikou.ui.home.facility_booking.MyBookingActivity
import com.eazy.daikou.ui.home.facility_booking.MyCardAddToCardBookingActivity
import com.eazy.daikou.ui.home.hrm.MyRoleMainActivity
import com.eazy.daikou.ui.home.hrm.adapter.ItemHRAdapter
import com.eazy.daikou.ui.home.parking.ParkingListActivity
import com.eazy.daikou.ui.home.quote_book.QuoteBookActivity
import com.eazy.daikou.ui.home.laws.LawsActivity
import com.eazy.daikou.ui.home.development_project.PropertyDevelopmentListActivity
import com.eazy.daikou.ui.profile.ChangeLanguageFragment
import com.google.gson.Gson


class SubHomeMenusActivity : BaseActivity() {

    private var actionCategory : String = ""
    private lateinit var recyclerViewItem : RecyclerView
    private var userType = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sub_home_menus)

        initView()

        initData()

        initAction()

    }

    private fun initView(){
        recyclerViewItem = findViewById(R.id.recyclerViewHome)
        findViewById<LinearLayout>(R.id.layoutMap).visibility = View.GONE
    }

    private fun initData(){
        if(intent != null && intent.hasExtra("action")){
            actionCategory = intent.getStringExtra("action").toString()
        }

        val userSessionManagement = UserSessionManagement(this)
        val mUser = Gson().fromJson(userSessionManagement.userDetail, User::class.java)

        userType = mUser.activeUserType

    }

    private fun initAction(){
        recyclerViewItem.layoutManager = Utils.spanGridLayoutManager(this, getListHomeMenu(this).size)
        val adapter = ItemHRAdapter(this, "sub_home_menu", getListHomeMenu(this), onClickCallBack)
        recyclerViewItem.adapter = adapter

        Utils.validateViewNoItemFound(this@SubHomeMenusActivity, recyclerViewItem, getListHomeMenu(this).isEmpty())
    }

    private var onClickCallBack = object : ItemHRAdapter.ItemClickOnListener{

        override fun onItemClickId(id: Int) {}

        override fun onItemClickAction(action: String?) {
            when (action) {
                "my_property" -> {
                    startNewActivity(MyRoleMainActivity::class.java, "sub_my_property")
                }
                "property_listing" -> {
                    startNewActivity(MyRoleMainActivity::class.java, "property_listing")
                }
                "transportation_booking" ->{
                    startNewActivity(MyRoleMainActivity::class.java, "transportation_booking")
                }
                "development_project" ->{
                    startNewActivity(PropertyDevelopmentListActivity::class.java, "development_project")
                }
                "facility" ->{
                    startNewActivity(FacilityBookingActivity::class.java, "facility")
                }
                "my_card" ->{
                    startNewActivity(MyCardAddToCardBookingActivity::class.java, "my_card")
                }
                "my_booking" ->{
                    startNewActivity(MyBookingActivity::class.java, "my_booking")
                }
                "constructor" ->{
                    AppAlertCusDialog.underConstructionDialog(this@SubHomeMenusActivity, resources.getString(R.string.coming_soon))
                    // startNewActivity(PropertyServiceOutActivity::class.java, "constructor")
                }
                "scan" ->{
                    startNewActivity(ScannerQRCodeAllActivity::class.java, "home_screen")
                }
                "my_qr_code"->{
                    selectChangeLanguage("qr_code_user")
                }
                "my_parking" ->{
                    startActivity(Intent(this@SubHomeMenusActivity, ParkingListActivity::class.java))
                }
                "books" ->{
                    startActivity(Intent(this@SubHomeMenusActivity, QuoteBookActivity::class.java))
                }
                "laws" ->{
                    startActivity(Intent(this@SubHomeMenusActivity, LawsActivity::class.java))
                }
                "beetube" ->{
                    val videoUrl = Intent(this@SubHomeMenusActivity, WebsiteActivity::class.java)
                    videoUrl.putExtra("action", "beetube")
                    videoUrl.putExtra("linkUrlVideo", Constant.url_beetube)
                    startActivity(videoUrl)
                }
                "news" ->{
                    val videoUrl = Intent(this@SubHomeMenusActivity, WebsiteActivity::class.java)
                    videoUrl.putExtra("action", "beetube_new")
                    videoUrl.putExtra("linkUrlVideo", Constant.url_beetube_news)
                    startActivity(videoUrl)
                }
                "hotel_booking"->{
                   startNewActivity(HotelBookingMainActivity::class.java, action)
                }
                else ->{
                    Utils.customToastMsgError(this@SubHomeMenusActivity,resources.getString(R.string.coming_soon), true)
                }
            }
        }
    }

    private fun selectChangeLanguage(key: String) {
        val changeLanguageAlert = ChangeLanguageFragment.newInstance(key)
        changeLanguageAlert.show(supportFragmentManager, changeLanguageAlert.javaClass.simpleName)
    }

    private fun startNewActivity(toActivityClass: Class<*>, action: String) {
        val intent = Intent(this@SubHomeMenusActivity, toActivityClass)
        intent.putExtra("action", action)
        startActivity(intent)
    }

    private fun getListHomeMenu(context: Context): List<ItemHR> {
        val homeViewModelList: MutableList<ItemHR> = ArrayList()
        when (actionCategory) {
            "property" -> {
                Utils.customOnToolbar(this@SubHomeMenusActivity, resources.getString(R.string.property_).uppercase()){finish()}
                if (!MockUpData.isUserAsGuest(UserSessionManagement(this)))
                    homeViewModelList.add(ItemHR("my_property", R.drawable.home_my_property, context.getString(R.string.my_property), getString(R.string.you_can_view_your_own_property_here), R.color.greenSea))
                homeViewModelList.add(ItemHR("development_project", R.drawable.home_project_development_v2, context.getString(R.string.project_development),getString(R.string.all_the_projects_in_cambodia), R.color.calendar_active_month_bg))
                homeViewModelList.add(ItemHR("property_listing", R.drawable.home_project_sale_v2, context.getString(R.string.property_listing),getString(R.string.search_property_buy_sale_and_rent), R.color.yellow))
            }
            "facility_book" -> {
                Utils.customOnToolbar(this@SubHomeMenusActivity, resources.getString(R.string.facility).uppercase()){finish()}
                homeViewModelList.add(ItemHR("facility", R.drawable.home_facility_booking_v2, context.getString(R.string.facility_booking),getString(R.string.this_is_for_your_facility_service_booking), R.color.greenSea))
                homeViewModelList.add(ItemHR("my_card", R.drawable.ic_report_description, context.getString(R.string.my_cart),getString(R.string.you_can_view_your_own_booking), R.color.calendar_active_month_bg))
                homeViewModelList.add(ItemHR("my_booking", R.drawable.add_to_card, context.getString(R.string.my_booking),getString(R.string.you_can_make_order_to_cart), R.color.yellow))
            }
            "education" ->{
                Utils.customOnToolbar(this@SubHomeMenusActivity, resources.getString(R.string.education).uppercase()){finish()}
                homeViewModelList.add(ItemHR("news", R.drawable.ic_home_rule_news, resources.getString(R.string.news),getString(R.string.realtime_news_and_broadcasting), R.color.greenSea))
                homeViewModelList.add(ItemHR("beetube", R.drawable.ic_home_rule_news, resources.getString(R.string.beetube),getString(R.string.entertainment_movices_and_videos), R.color.yellow))
                homeViewModelList.add(ItemHR("laws", R.drawable.ic_home_law, resources.getString(R.string.laws),getString(R.string.regulation_and_permit), R.color.red))
                homeViewModelList.add(ItemHR("books", R.drawable.home_books, resources.getString(R.string.book),getString(R.string.novels_historical_horrible), R.color.blue))
                homeViewModelList.add(ItemHR("professional_training", R.drawable.ic_home_online_video_v2, resources.getString(R.string.professional_training),getString(R.string.education_videos_training_and_skills), R.color.appBarColorOld))
            }
            "accessibility" ->{
                Utils.customOnToolbar(this@SubHomeMenusActivity, resources.getString(R.string.accessibility).uppercase()){finish()}
                homeViewModelList.add(ItemHR("scan", R.drawable.ic_home_scan_qr, resources.getString(R.string.scan_qr_code),getString(R.string.no_description), R.color.greenSea))
                homeViewModelList.add(ItemHR("my_qr_code", R.drawable.ic_qr_code, resources.getString(R.string.my_qr_code),getString(R.string.no_description), R.color.yellow))
                homeViewModelList.add(ItemHR("my_parking", R.drawable.home_parking, resources.getString(R.string.my_parking),getString(R.string.no_description), R.color.red))
            }
            "booking" ->{
                Utils.customOnToolbar(this@SubHomeMenusActivity, resources.getString(R.string.booking).uppercase()){finish()}
                homeViewModelList.add(ItemHR("hotel_booking", R.drawable.icon_hotel_booking, resources.getString(R.string.booking_now), resources.getString(R.string.can_book_hotel_that_you_want_to_stay), R.color.color_book_now))
                homeViewModelList.add(ItemHR("transportation_booking", R.drawable.ic_home_transport_v2, resources.getString(R.string.transportation_booking), resources.getString(R.string.can_book_transportation_that_you_want_to_go), R.color.greenSea))
            }
        }

        return homeViewModelList
    }

}