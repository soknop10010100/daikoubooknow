package com.eazy.daikou.ui.home.hrm.notice_board.adapter

import android.annotation.SuppressLint
import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R

class AttachmentNoticeAdapter(private val context : Activity, private val listAttachment: ArrayList<String>,private val callBackItem: CallbackItemAttachment): RecyclerView.Adapter<AttachmentNoticeAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.layout_item_attachment, parent, false)
        return ViewHolder(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data : String = listAttachment[position]
        if (data != null){
            holder.nameFileTv.text = String.format("%s %s", context.resources.getString(R.string.attachment), (position  + 1))
            holder.btnOpenFile.setOnClickListener { callBackItem.onClickBackItem(data)
            }
        }
    }

    override fun getItemCount(): Int {
        return listAttachment.size
    }

    class ViewHolder( view: View) : RecyclerView.ViewHolder(view){
        val nameFileTv : TextView = view.findViewById(R.id.nameFileTv)
        val btnOpenFile : CardView = view.findViewById(R.id.btnOpenFile)
    }

    interface CallbackItemAttachment{
        fun onClickBackItem(attachmentModel: String)
    }
}