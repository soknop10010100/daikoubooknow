package com.eazy.daikou.ui.home.quote_book;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.eazy.daikou.R;
import com.eazy.daikou.base.BaseActivity;
import com.eazy.daikou.request_data.request.book_quote_ws.QuoteWs;
import com.eazy.daikou.helper.Utils;
import com.eazy.daikou.helper.web.WebsiteActivity;
import com.eazy.daikou.model.book.BookModelV2;
import com.eazy.daikou.model.book.BookSellAllV2;
import com.eazy.daikou.ui.home.quote_book.adapter.QuoteAllBookAdapter;

import java.util.ArrayList;
import java.util.List;

public class QuoteAllBookActivity extends BaseActivity {

    private RecyclerView recyclerView;
    private QuoteAllBookAdapter quoteAllBookAdapter;
    private final List<BookSellAllV2>  bookSellAllV2List = new ArrayList<>();
    private BookModelV2 bookModelV2;
    private TextView titleToolbar;
    private ImageView iconBack;
    private ProgressBar progress;
    private SwipeRefreshLayout swipeRefreshLayout;

    private int currentItem, total , scrollDown, currentPage = 1 ,size = 10;
    private boolean isScrolling;
    private String item_type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_all_book);

        initView();

        initData();

        initAction();
    }

    private void initView(){
        recyclerView = findViewById(R.id.recyclerViewAll);
        titleToolbar  = findViewById(R.id.titleToolbar);
        iconBack = findViewById(R.id.iconBack);
        progress = findViewById(R.id.progressItem);
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);

    }

    private void initData(){
        if (getIntent() != null && getIntent().hasExtra("list_of_book")){
            bookModelV2 = (BookModelV2) getIntent().getSerializableExtra("list_of_book");
        }
        if (getIntent() != null && getIntent().hasExtra("key_type")){
           item_type = (String) getIntent().getSerializableExtra("key_type");
        }

    }

    private void initAction() {

        if (bookModelV2.getCategoryName() != null){
            titleToolbar.setText(bookModelV2.getCategoryName());
        }
        iconBack.setOnClickListener(v->finish());

        initRecyclerView();

        onRefreshLayout();

        onScrollView();
    }

    private void onRefreshLayout(){
        swipeRefreshLayout.setOnRefreshListener(() -> {

            currentPage = 1;
            size = 10;
            isScrolling = true;
            quoteAllBookAdapter.clear();

              onRequestContact();

        });
    }

    private void onScrollView() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true;
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                currentItem = linearLayoutManager.getChildCount();
                total = linearLayoutManager.getItemCount();
                scrollDown = linearLayoutManager.findFirstVisibleItemPosition();
                if (dy > 0) {
                    if (isScrolling && (currentItem + scrollDown == total)) {
                        if (total == size) {
                            isScrolling = false;
                            currentPage++;
                            progress.setVisibility(View.VISIBLE);
                            recyclerView.scrollToPosition(bookSellAllV2List.size() - 1);
                            initRecyclerView();
                            size += 10;
                        } else {
                            progress.setVisibility(View.GONE);
                        }
                    }
                }
            }
        });
    }

    private void onRequestContact() {
        new QuoteWs().getQuoteBookAll(QuoteAllBookActivity.this, currentPage,10 ,item_type,
                bookModelV2.getCategoryId(), quoteBookAllCallBackListener);
    }

    private final QuoteWs.QuoteBookAllCallBackListener quoteBookAllCallBackListener = new QuoteWs.QuoteBookAllCallBackListener() {
        @SuppressLint("NotifyDataSetChanged")
        @Override
        public void getQuoteBookAll(List<BookSellAllV2> quotesList) {

            progress.setVisibility(View.GONE);
            swipeRefreshLayout.setRefreshing(false);

            bookSellAllV2List.addAll(quotesList);

            quoteAllBookAdapter.notifyDataSetChanged();

        }

        @Override
        public void onFailed(String error) {
            swipeRefreshLayout.setRefreshing(false);
            recyclerView.setVisibility(View.GONE);
            Toast.makeText(QuoteAllBookActivity.this, error, Toast.LENGTH_SHORT).show();
        }
    };

    private void initRecyclerView() {
        quoteAllBookAdapter = new QuoteAllBookAdapter(this,bookSellAllV2List, callBackAllBook);
        recyclerView.setAdapter(quoteAllBookAdapter);

        onRequestContact();
    }

    private final QuoteAllBookAdapter.CallBackAllBook callBackAllBook = bookSellAllV2 -> {

        if (bookSellAllV2.getFilePath() != null && bookSellAllV2.getFilePath().contains(".pdf")) {
            Utils.openDefaultPdfView(QuoteAllBookActivity.this, bookSellAllV2.getFilePath());
        } else {
             if (bookSellAllV2.getFilePath() != null || bookSellAllV2.getImage() != null) {
            if (bookSellAllV2.getImage() != null) {
                if (bookSellAllV2.getImage().contains(".jpg") || bookSellAllV2.getImage().contains(".jpeg") || bookSellAllV2.getImage().contains(".png")) {
                    Intent intent = new Intent(this, WebsiteActivity.class);
                    intent.putExtra("linkUrlNews", bookSellAllV2.getImage());
                    startActivity(intent);
                } else {
                    Toast.makeText(this, getResources().getText(R.string.not_available_website), Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(this, getResources().getText(R.string.not_available_website), Toast.LENGTH_SHORT).show();
            }

            } else {
                Toast.makeText(QuoteAllBookActivity.this,getResources().getText(R.string.not_available_website) , Toast.LENGTH_SHORT).show();
            }
        }
    };

}