package com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.adapter_v2

import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.request_data.static_data.StaticUtilsKey
import com.eazy.daikou.helper.DateUtil
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.my_property.cleaning.ActivityLogs

class ActivityLogInfoAdapter(private val action : String, private val list : ArrayList<ActivityLogs>): RecyclerView.Adapter<ActivityLogInfoAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.all_schedule_employee_adater_layout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val listService: ActivityLogs = list[position]
        if (listService != null){
            holder.allScheduleLayout.visibility = View.GONE
            holder.allActivityLogLayout.visibility = View.VISIBLE
            holder.remarkTv.text = if (listService.remark != null) listService.remark else "- - -"
            holder.checkInTimeTv.text = if (listService.created_date != null) DateUtil.formatTimeLocal(listService.created_date) else "- - -"
            holder.checkInDateTv.text = if (listService.created_date != null) DateUtil.formatDateComplaint(listService.created_date) else "- - -"

            val context = holder.statusTv.context
            if (listService.status != null) {
                when (listService.status) {
                    "late" -> {
                        holder.statusTv.backgroundTintList =
                            ColorStateList.valueOf(ContextCompat.getColor(context, R.color.red))
                        holder.statusTv.text = context.resources.getString(R.string.late)
                    }
                    "normal" -> {
                        holder.statusTv.backgroundTintList =
                            ColorStateList.valueOf(ContextCompat.getColor(context, R.color.green))
                        holder.statusTv.text = context.resources.getString(R.string.normal)
                    }
                    else -> {
                        holder.statusTv.backgroundTintList = ColorStateList.valueOf(
                            ContextCompat.getColor(
                                context,
                                R.color.green
                            )
                        )
                        holder.statusTv.text = context.resources.getString(R.string.early)
                    }
                }
            } else {
                holder.statusTv.text = "- - -"
            }

            holder.dateLabelTv.text = if (listService.activity_type == "check_out") Utils.getText(context, R.string.check_out_date) else Utils.getText(context, R.string.check_in_date)
            holder.timeLabelTv.text = if (listService.activity_type == "check_out") Utils.getText(context, R.string.check_out_time) else Utils.getText(context, R.string.check_in_time)

            if (action == StaticUtilsKey.employee_type){
                holder.allActivityLogLayout.background = ResourcesCompat.getDrawable(holder.allActivityLogLayout.context.resources, R.drawable.shape_transparent_bg, null)
            } else {
                holder.allActivityLogLayout.background = ResourcesCompat.getDrawable(holder.allActivityLogLayout.context.resources, R.drawable.custom_card_item_gradient, null)
            }
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view){
        var remarkTv: TextView = view.findViewById(R.id.remarkTv)
        var checkInTimeTv: TextView = view.findViewById(R.id.checkInTimeTv)
        var checkInDateTv: TextView = view.findViewById(R.id.checkInDateTv)
        var statusTv: TextView = view.findViewById(R.id.statusTv)
        var allActivityLogLayout : RelativeLayout = view.findViewById(R.id.allActivityLogLayout)
        var allScheduleLayout : RelativeLayout = view.findViewById(R.id.allScheduleLayout)
        var timeLabelTv : TextView = view.findViewById(R.id.timeTv)
        var dateLabelTv : TextView = view.findViewById(R.id.dateTv)
    }
}