package com.eazy.daikou.ui.home.quote_book.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.eazy.daikou.R;
import com.eazy.daikou.helper.DateUtil;
import com.eazy.daikou.model.book.BookSellAllV2;

import java.util.List;

public class QuoteAllBookAdapter  extends RecyclerView.Adapter<QuoteAllBookAdapter.ViewHolder>{

    private final List<BookSellAllV2> bookSellAllV2List;
    private final Context context;
    private final CallBackAllBook callBackAllBook;

    public QuoteAllBookAdapter(Context context, List<BookSellAllV2> bookSellAllV2List, CallBackAllBook callBackAllBook) {
        this.bookSellAllV2List = bookSellAllV2List ;
        this.context = context;
        this.callBackAllBook = callBackAllBook;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_view_viewall_book,parent,false);
        return new ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        BookSellAllV2 bookSellAllV2 = bookSellAllV2List.get(position);
        if (bookSellAllV2 != null) {
            if (bookSellAllV2.getTitle() != null) {
                holder.tvNameBook.setText(bookSellAllV2.getTitle());
            } else {
                holder.tvNameBook.setText("- - - - - -");
            }
            holder.tvCreateDate.setText(context.getResources().getText(R.string.create_date) + ": " + DateUtil.formatDateComplaint(bookSellAllV2.getCreatedDate()));

            if (bookSellAllV2.getFilePath() != null) {
                if (bookSellAllV2.getImage() != null){
                    Glide.with(context).load(bookSellAllV2.getImage()).into(holder.imageViewBook);
                } else {
                    Glide.with(context).load(R.drawable.no_image).into(holder.imageViewBook);
                }
            } else if (bookSellAllV2.getImage() != null) {
                if (bookSellAllV2.getImage().contains(".jpg") || bookSellAllV2.getImage().contains(".jpeg") || bookSellAllV2.getImage().contains(".png")) {
                    Glide.with(context).load(bookSellAllV2.getImage()).into(holder.imageViewBook);
                } else {
                    Glide.with(context).load(R.drawable.no_image).into(holder.imageViewBook);
                }
            } else {
                Glide.with(context).load(R.drawable.no_image).into(holder.imageViewBook);
            }

            holder.itemView.setOnClickListener(view -> {
                callBackAllBook.onSelectItemAll(bookSellAllV2);
            });
        }
    }

    @Override
    public int getItemCount() {
        return bookSellAllV2List.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView tvNameBook, tvCreateDate;
        private final ImageView imageViewBook;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvNameBook = itemView.findViewById(R.id.tvNameBook);
            tvCreateDate = itemView.findViewById(R.id.tvCreateDate);
            imageViewBook = itemView.findViewById(R.id.ImageViewBook);

        }
    }

    public interface CallBackAllBook {
        void onSelectItemAll(BookSellAllV2 bookSellAllV2);
    }

    public void clear() {
        int size = bookSellAllV2List.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                bookSellAllV2List.remove(0);
            }
            notifyItemRangeRemoved(0, size);
        }
    }
}
