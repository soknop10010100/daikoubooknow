package com.eazy.daikou.ui.home.my_property.adapter

import android.content.Context
import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.helper.DateUtil
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.my_property.my_property.PropertyPaymentUnitModel

class PropertyPaymentUnitAdapter (private val context : Context, private val listPaymentModel: ArrayList<PropertyPaymentUnitModel>, private val callBackPayment : OnCallBackListenerPayment):
    RecyclerView.Adapter<PropertyPaymentUnitAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.property_payment_unit_layout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val paymentsModel : PropertyPaymentUnitModel = listPaymentModel[position]

        Utils.setValueOnText(holder.iDOrderTv, paymentsModel.invoice_no)
        Utils.setValueOnText(holder.endDateLeaseTv, Utils.formatDateFromString("yyyy-MM-dd", "dd-MMM-yyyy", paymentsModel.date_pay))
        Utils.setValueOnText(holder.percentageTv, paymentsModel.percentage)
        Utils.setValueOnText(holder.amountPercentageTv, paymentsModel.amount)
        Utils.setValueOnText(holder.interestTv, paymentsModel.interest)
        Utils.setValueOnText(holder.amountInterestTv, paymentsModel.interest_amount)
        Utils.setValueOnText(holder.amountPaymentTv, paymentsModel.payment_amount)
        Utils.setValueOnText(holder.stageTv, paymentsModel.stage)
        Utils.setValueOnText(holder.totalTermsTv, paymentsModel.total_term)

        // Data
        holder.createDateTv.text = DateUtil.appendFormatDateTime(paymentsModel.created_at)

        // Status
        displayStatus(if (paymentsModel.payment_status != null) paymentsModel.payment_status!! else ". . .", holder.statusTv, holder.style)

        holder.mainLayout.setOnClickListener (CustomSetOnClickViewListener{
            callBackPayment.onClickBackItemPayment(paymentsModel)
        })
    }

    override fun getItemCount(): Int {
        return listPaymentModel.size
    }

    class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView){
        var style : TextView = itemView.findViewById(R.id.style)
        var iDOrderTv : TextView = itemView.findViewById(R.id.idOrderTv)
        var interestTv : TextView = itemView.findViewById(R.id.interestTv)
        var statusTv : TextView = itemView.findViewById(R.id.statusTv)
        var amountPercentageTv : TextView = itemView.findViewById(R.id.amountPercentageTv)
        var percentageTv : TextView = itemView.findViewById(R.id.percentageTv)
        var stageTv : TextView = itemView.findViewById(R.id.stageTv)
        var amountInterestTv : TextView = itemView.findViewById(R.id.amountInterestTv)
        var endDateLeaseTv : TextView = itemView.findViewById(R.id.endDateLeaseTv)
        var amountPaymentTv : TextView = itemView.findViewById(R.id.amountPaymentTv)
        var totalTermsTv : TextView = itemView.findViewById(R.id.totalTermsTv)
        var createDateTv : TextView = itemView.findViewById(R.id.createDateTv)
        var mainLayout : CardView = itemView.findViewById(R.id.mainLayout)
    }

    interface OnCallBackListenerPayment{
        fun onClickBackItemPayment(paymentsModel : PropertyPaymentUnitModel)
    }

    fun clear() {
        val size: Int = listPaymentModel.size
        if (size > 0) {
            for (i in 0 until size) {
                listPaymentModel.removeAt(0)
            }
            notifyItemRangeRemoved(0, size)
        }
    }

    private fun displayStatus(status : String, txtStatus : TextView, txtStyle: TextView){
        var color = R.color.red
        var valStatus = "- - -"
        when (status) {
            "paid" -> {
                valStatus = context.resources.getString(R.string.paid)
                color = R.color.appBarColorOld
            }

            "unpaid" ->{
                valStatus = context.resources.getString(R.string.un_paid)
                color = R.color.red
            }

            "no_invoice" ->{
                valStatus = context.resources.getString(R.string.no_invoice)
                color = R.color.yellow
            }

            "overdue" -> {
                valStatus = context.resources.getString(R.string.overdue)
                color = R.color.red
            }
        }

        txtStatus.text = valStatus
        txtStatus.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(context, color))
        txtStyle.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(context, color))
    }
}