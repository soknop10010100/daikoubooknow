package com.eazy.daikou.ui.home.my_property_v1.property_service_employee.service_adapter_v2

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.helper.DateUtil
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.my_property.service_property_employee.ServicePropertyHistoryEmployeeModel
import kotlin.collections.ArrayList

class ServicePropertyHistoryAdapter(private val context : Context, private val actionDateWork : String, private val serviceList: ArrayList<ServicePropertyHistoryEmployeeModel>, private val itemClickService: ItemClickOnService): RecyclerView.Adapter<ServicePropertyHistoryAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // val v = LayoutInflater.from(parent.context).inflate(R.layout.layout_item_unit_schedule, parent, false)
        val v = LayoutInflater.from(parent.context).inflate(R.layout.layout_history_cleaner_adapter, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
       val service : ServicePropertyHistoryEmployeeModel = serviceList[position]
        if (service != null){

            Utils.setValueOnText(holder.orderNoTv, service.work_schedule_id)
            Utils.setValueOnText(holder.unitTv, service.unit_no)

            if (service.is_common_area){
                holder.linearCommonArea.visibility = View.VISIBLE
                holder.linearPrivateArea.visibility = View.GONE
            } else {
                holder.linearCommonArea.visibility = View.GONE
                holder.linearPrivateArea.visibility = View.VISIBLE
            }

            holder.spaceNameTv.text = if (service.space_name != null) service.space_name else  "- - -"

            holder.floorTv.text = if (service.floor_no != null) service.floor_no else  "- - -"

            holder.zoneNameTv.text = if (service.zone_name != null) service.zone_name else  "- - -"

            if(service.schedule_start_date!=null){
                holder.startDateTv.text = DateUtil.formatDateComplaint(service.schedule_start_date)
            } else{
                holder.startDateTv.text = "_ _ _"
            }

            if(service.schedule_end_date!=null){
                holder.endDateTv.text = DateUtil.formatDateComplaint(service.schedule_end_date)
            } else{
                holder.endDateTv.text = "_ _ _"
            }

            if(service.schedule_start_time!=null){
                holder.startTimeTv.text = Utils.formatDateTime(service.schedule_start_time,"hh:mm:ss","hh:mm a")
            } else{
                holder.startTimeTv.text = "_ _ _"
            }

            if (service.schedule_end_time!=null){
                holder.endTimeTv.text = Utils.formatDateTime(service.schedule_end_time.toString(),"hh:mm:ss","hh:mm a")
            }else{
                holder.endTimeTv.text = "_ _ _"
            }
            if (service.sv_name != null){
                holder.serviceNameTv.text = service.sv_name
            } else {
                holder.serviceNameTv.text = "_ _ _"
            }
            if (service.sv_term != null){
                holder.serviceTermTv.text = service.sv_term
            } else {
                holder.serviceTermTv.text = "_ _ _"
            }

            holder.dateWorkingLayout.visibility = if(actionDateWork == "all") View.VISIBLE else View.GONE

            if (service.schedule_status != null){
                holder.statusTv.backgroundTintList = Utils.setColorStatusService(context, service.schedule_status)
                holder.style.backgroundTintList = Utils.setColorStatusService(context, service.schedule_status)
                holder.statusTv.text = Utils.getValueFromKeyStatusService(context)[service.schedule_status]
            } else{
                holder.statusTv.backgroundTintList = Utils.setColorStatusService(context, "")
                holder.statusTv.text = "_ _ _"
            }

            holder.itemView.setOnClickListener(
                CustomSetOnClickViewListener{
                    itemClickService.onClickUnitSchedule(service)
                }
            )
        }
    }

    override fun getItemCount(): Int {
        return serviceList.size
    }

    interface ItemClickOnService {
        fun onClickUnitSchedule(cleanerSchedule: ServicePropertyHistoryEmployeeModel)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val orderNoTv: TextView = view.findViewById(R.id.no_order)
        val unitTv: TextView = view.findViewById(R.id.unit_schedule)
        val startDateTv: TextView = view.findViewById(R.id.start_date_text)
        val endDateTv: TextView = view.findViewById(R.id.end_date_tex)
        val startTimeTv: TextView = view.findViewById(R.id.start_time_text)
        val endTimeTv: TextView = view.findViewById(R.id.end_time_text)
        val statusTv: TextView = view.findViewById(R.id.status_schedule)
        val serviceNameTv: TextView = view.findViewById(R.id.service_name_cleaner)
        val serviceTermTv: TextView = view.findViewById(R.id.service_term_cleaner)
        val dateWorkingLayout: LinearLayout = view.findViewById(R.id.dateWorkingLayout)
        val spaceNameTv: TextView = view.findViewById(R.id.spaceNameTv)
        val floorTv : TextView = view.findViewById(R.id.floorTv)
        val zoneNameTv : TextView = view.findViewById(R.id.zoneNameTv)
        val linearPrivateArea: LinearLayout = view.findViewById(R.id.linearPrivateArea)
        val linearCommonArea : LinearLayout = view.findViewById (R.id.linearCommonArea)
        val style : TextView = view.findViewById(R.id.style)

    }

    fun clear() {
        val size: Int = serviceList.size
        if (size > 0) {
            for (i in 0 until size) {
                serviceList.removeAt(0)
            }
            notifyItemRangeRemoved(0, size)
        }
    }

}