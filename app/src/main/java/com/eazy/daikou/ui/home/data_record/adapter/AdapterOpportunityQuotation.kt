package com.eazy.daikou.ui.home.data_record.adapter
import android.content.Context
import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.helper.DateUtil
import com.eazy.daikou.helper.Utils.setValueOnText
import com.eazy.daikou.model.data_record.Quotation

class AdapterOpportunityQuotation (private val context: Context, private val quotationList : ArrayList<Quotation>, private val callBackItem : CallBackItemClickListener) :
    RecyclerView.Adapter<AdapterOpportunityQuotation.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context)
            .inflate(R.layout.custom_opportunity_quotation, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBinding(quotationList[position], context, callBackItem)

    }

    override fun getItemCount(): Int {
        return quotationList.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var noTv: TextView = view.findViewById(R.id.noTv)
        var createDateTv: TextView = view.findViewById(R.id.createDateTv)
        var unitNameTv: TextView = view.findViewById(R.id.unitNameTv)
        var sellingPriceTv: TextView = view.findViewById(R.id.sellingPriceTv)
        var unitTypeTv: TextView = view.findViewById(R.id.unitTypeTv)
        var statusQuotationTv: TextView = view.findViewById(R.id.statusQuotationTv)
        var streetFloorTv: TextView = view.findViewById(R.id.streetFloorTv)

        fun onBinding(quotation: Quotation, context: Context ,callBackItem: CallBackItemClickListener) {
            setValueOnText(createDateTv, String.format("%s %s", DateUtil.formatDateComplaint(quotation.created_at), DateUtil.formatDateToConvertOnlyTimeZoneNoSecond(quotation.created_at)))
            setValueOnText(noTv, quotation.no)
            if (quotation.unit != null) {
                setValueOnText(unitNameTv, quotation.unit!!.name)
                setValueOnText(sellingPriceTv, quotation.unit!!.price)
                setValueOnText(unitTypeTv, quotation.unit!!.type!!.name)
                setValueOnText(streetFloorTv, quotation.unit!!.floor_no)
            } else {
                unitNameTv.text = ". . ."
                sellingPriceTv.text = ". . ."
                unitTypeTv.text = ". . ."
            }
            if (quotation.status != null) {

                displayStatus(quotation.status.toString(), statusQuotationTv, context)
            } else {
                statusQuotationTv.text = "- - -"
                statusQuotationTv.backgroundTintList =
                    ColorStateList.valueOf(ContextCompat.getColor(statusQuotationTv.context,
                        R.color.appBarColorOld))
            }

            itemView.setOnClickListener { callBackItem.clickItemListener(quotation) }
        }

        private fun displayStatus(status: String, txtStatus: TextView, context: Context) {
            var color = R.color.appBarColor
            var valStatus = "- - -"
            when (status) {
                "draft" -> {
                    valStatus = context.getString(R.string.draft)
                    color = R.color.appBarColorOld
                }

                "need_review" -> {
                    valStatus = context.getString(R.string.need_review)
                    color = R.color.gray
                }

                "in_review" -> {
                    valStatus = context.getString(R.string.in_review)
                    color = R.color.yellow
                }

                "approved" ->{
                    valStatus = context.getString(R.string.approved)
                    color = R.color.green
                }

                "rejected" -> {
                    valStatus = context.getString(R.string.rejected)
                    color = R.color.red
                }

                "presented" -> {
                    valStatus = context.getString(R.string.presented)
                    color = R.color.blue
                }

                "accepted" -> {
                    valStatus = context.getString(R.string.accepted)
                    color = R.color.green
                }

                "denied" -> {
                    valStatus = context.getString(R.string.denied)
                    color = R.color.red
                }
            }

            txtStatus.text = valStatus
            txtStatus.backgroundTintList = ColorStateList.valueOf (ContextCompat.getColor(context, color))
        }


    }

    interface CallBackItemClickListener {
        fun clickItemListener(quotation: Quotation)
    }

}