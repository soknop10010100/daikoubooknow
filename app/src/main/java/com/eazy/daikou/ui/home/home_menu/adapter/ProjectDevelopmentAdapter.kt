package com.eazy.daikou.ui.home.home_menu.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.home_page.DevelopmentProjects
import com.squareup.picasso.Picasso

class ProjectDevelopmentAdapter(private val listPropertyItem: ArrayList<DevelopmentProjects>, private val onClickProject: OnclickProject) :
    RecyclerView.Adapter<ProjectDevelopmentAdapter.ViewHolder>() {

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val layoutImage: View = itemView.findViewById(R.id.layoutImage)
        val imageView: ImageView = itemView.findViewById(R.id.image)
        val textProjectName: TextView = itemView.findViewById(R.id.property_name)
        val textAddressProject: TextView = itemView.findViewById(R.id.buildingAddress)
        val cityTv: TextView = itemView.findViewById(R.id.cityTv)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.custom_item_project_development_home_v2, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        //set image
        if (listPropertyItem[position].image != null && listPropertyItem[position].image != "") {
            Picasso.get().load(listPropertyItem[position].image).placeholder(R.drawable.no_image)
                .into(holder.imageView)
        } else {
            holder.imageView.setImageResource(R.drawable.no_image)
        }

        //set project name
        if (listPropertyItem[position].name != null && listPropertyItem[position].name != "") {
            holder.textProjectName.text = listPropertyItem[position].name
        } else {
            holder.textProjectName.text = ""
        }
        if (listPropertyItem[position].city != null && listPropertyItem[position].city != "") {
            holder.cityTv.text = listPropertyItem[position].city
            holder.cityTv.visibility = View.VISIBLE
        } else {
            holder.cityTv.visibility = View.GONE
        }

        //set project address
        if (listPropertyItem[position].address != null && listPropertyItem[position].address != "") {
            holder.textAddressProject.text = listPropertyItem[position].address
        } else {
            holder.textAddressProject.text = ""
        }

        holder.layoutImage.setOnClickListener {
            onClickProject.onClickProject(listPropertyItem[position])
        }

        val heightImg: Int
        if (position == 0 || position % 2 == 0 || position % 3 == 0) {
            heightImg = Utils.dpToPx(holder.textAddressProject.context, 220)
            holder.layoutImage.layoutParams.height = heightImg
        } else {
            heightImg = Utils.dpToPx(holder.textAddressProject.context, 180)
            holder.layoutImage.layoutParams.height = heightImg
        }

    }

    override fun getItemCount(): Int {
        return listPropertyItem.size
    }

    interface OnclickProject {
        fun onClickProject(project: DevelopmentProjects)
    }
}