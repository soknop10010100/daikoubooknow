package com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.request_data.request.service_property_ws.cleaning_service.ServiceCleaningWs
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.model.my_property.service_property_employee.AllService
import com.eazy.daikou.model.profile.User
import com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.adapter.ServiceNameAdapter
import com.google.gson.Gson

class ListAllServiceNameActivity : BaseActivity() {

    private lateinit var recyclerView : RecyclerView
    private lateinit var titleTv : TextView
    private lateinit var btnBack : ImageView
    private lateinit var serviceNameAdapter : ServiceNameAdapter
    private lateinit var layoutManager : LinearLayoutManager

    private  var listAllService = ArrayList<AllService>()

    private var CODE_REFRESH =996
    private var propertyId = ""
    private var userId = " "
    private var unitTypeId = " "
    private var serviceTerm = ""
    private var serviceType = ""
    private var unitId = ""
    private var currentItem = 0
    private var total: Int = 0
    private var scrollDown: Int = 0
    private var currentPage: Int = 1
    private var size: Int = 10
    private var isScrolling = false
    private lateinit var progressBar: ProgressBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_all_service_name)

        initView()

        initData()

        initAction()

    }

    private fun initView(){
        recyclerView = findViewById(R.id.list_service_name)
        titleTv = findViewById(R.id.titleToolbar)
        btnBack = findViewById(R.id.iconBack)
        progressBar = findViewById(R.id.progressItem)
    }

    private fun initAction(){
        btnBack.setOnClickListener { finish() }

        onScrollListParking()

    }

    @SuppressLint("SetTextI18n")
    private fun initData(){

        if(intent.hasExtra("service_type")){
            serviceType = intent.getStringExtra("service_type") as  String
        }
        if(intent.hasExtra("service_term")){
            serviceTerm = intent.getStringExtra("service_term") as  String
        }

        if(intent.hasExtra("unitId")){
            unitId = intent.getStringExtra("unitId") as String
        }
        if(intent.hasExtra("unitTypeId")){
            unitTypeId = intent.getStringExtra("unitTypeId") as String
        }
        val sessionManagement = UserSessionManagement(this)
        val gson = Gson()
        val user = gson.fromJson(sessionManagement.userDetail, User::class.java)

        propertyId = user.activePropertyIdFk
        if(sessionManagement.userId!=null){
            userId = sessionManagement.userId
        }

        titleTv.text = " List All Service Cleaning( $serviceType )"

        layoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = layoutManager

        getServiceName()

    }

    private fun getServiceName(){
        progressBar.visibility = View.VISIBLE
        ServiceCleaningWs().getServiceName(this, currentPage, unitTypeId, propertyId, serviceType, serviceTerm, serviceName)
    }

    private fun onScrollListParking() {
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true
                }
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                currentItem = layoutManager.childCount
                total = layoutManager.itemCount
                scrollDown = layoutManager.findFirstVisibleItemPosition()
                if (dy > 0) {
                    if (isScrolling && currentItem + scrollDown == total) {
                        if (total == size) {
                            isScrolling = false
                            currentPage++
                            recyclerView.scrollToPosition(listAllService.size - 1)
                            size += 10
                            getServiceName()
                        }
                    }
                }
            }
        })
    }



    private val serviceName = object : ServiceCleaningWs.OnLoadRequestAllServiceCallListener{
        @SuppressLint("NotifyDataSetChanged")
        override fun onLoadSuccessFull(listService: MutableList<AllService>) {
            progressBar.visibility = View.GONE
            listAllService.addAll(listService)
            serviceNameAdapter = ServiceNameAdapter(this@ListAllServiceNameActivity,listAllService,itemServiceName)
            recyclerView.adapter = serviceNameAdapter
            serviceNameAdapter.notifyDataSetChanged()

        }

        override fun onLoadFail(message: String) {
            progressBar.visibility = View.GONE
            Toast.makeText(this@ListAllServiceNameActivity,message,Toast.LENGTH_SHORT).show()
        }

    }
    private val itemServiceName = object : ServiceNameAdapter.ItemClickOnService{
        override fun onClick(service: AllService) {
            val intent = Intent(this@ListAllServiceNameActivity,ServicePropertyDateTimeActivity::class.java)
            intent.putExtra("title",service.cleaningServiceName)
            intent.putExtra("service",service)
            intent.putExtra("service_term",serviceTerm)
            intent.putExtra("user_id",userId)
            intent.putExtra("unitId",unitId)
            startActivityForResult(intent,CODE_REFRESH)
        }

    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(data!=null && resultCode == RESULT_OK){
            if(requestCode == CODE_REFRESH){
                if(data.hasExtra("isRefresh")){
                    val  intent = Intent()
                    intent.putExtra("isRefresh","is")
                    setResult(RESULT_OK,intent)
                    finish()
                }
            }
        }
    }
}