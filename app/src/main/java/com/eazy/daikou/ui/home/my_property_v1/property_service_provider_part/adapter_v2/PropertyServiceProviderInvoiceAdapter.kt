package com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.adapter_v2

import android.content.Context
import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.model.my_property.cleaning.PaymentInfo

class PropertyServiceProviderInvoiceAdapter (private val context: Context, private val listInvoice: ArrayList<PaymentInfo>, private val callBackItemInvoice: CallBackItemInvoiceListener):RecyclerView.Adapter<PropertyServiceProviderInvoiceAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.layout_view_custom_invoice_list_service_provider, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val invoicePayment: PaymentInfo = listInvoice[position]
        if (invoicePayment != null){

            holder.createDateTv.text = if (invoicePayment.created_dt != null) invoicePayment.created_dt else "..."
            holder.fromDateTv.text = if (invoicePayment.from_date != null)  invoicePayment.from_date else "..."
            holder.toDateTv.text = if (invoicePayment.to_date != null) invoicePayment.to_date else "..."
            if (invoicePayment.payment_status != null && invoicePayment.payment_status.equals("unpaid")){
                holder.paymentStatusTv.text = invoicePayment.payment_status
                holder.paymentStatusTv.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(context, R.color.red))
            } else if (invoicePayment.payment_status != null && invoicePayment.payment_status.equals("unpaid")){
                holder.paymentStatusTv.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(context, R.color.green))
                holder.paymentStatusTv.text = invoicePayment.payment_status
            } else{
                holder.paymentStatusTv.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(context, R.color.appBarColorOld))
                holder.paymentStatusTv.text = "..."
            }

            holder.itemView.setOnClickListener { callBackItemInvoice.callBackItem(invoicePayment)  }
        }
    }

    override fun getItemCount(): Int {
        return listInvoice.size
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        var createDateTv: TextView = view.findViewById(R.id.createDateTv)
        var fromDateTv: TextView = view.findViewById(R.id.fromDateTv)
        var toDateTv: TextView = view.findViewById(R.id.toDateTv)
        var paymentStatusTv: TextView = view.findViewById(R.id.paymentStatusTv)

    }

    interface CallBackItemInvoiceListener{
        fun callBackItem(invoicePayment: PaymentInfo)
    }
}