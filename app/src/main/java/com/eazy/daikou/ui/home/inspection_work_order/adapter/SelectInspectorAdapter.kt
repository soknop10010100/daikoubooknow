package com.eazy.daikou.ui.home.inspection_work_order.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.model.inspection.InspectorModel

class SelectInspectorAdapter(private val inspectorList : ArrayList<InspectorModel>, private val onClickCallBack : OnClickCallBackListener):
    RecyclerView.Adapter<SelectInspectorAdapter.ItemViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        return ItemViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.choose_inspector_model, parent, false))
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val inspectorModel = inspectorList[position]
        if (inspectorModel != null){
            holder.title.text = inspectorModel.userName

            if (inspectorModel.isCheckInspector){
                holder.iconSelect.background = ResourcesCompat.getDrawable(holder.iconSelect.resources, R.drawable.circle_appbar, null)
            } else {
                holder.iconSelect.background = ResourcesCompat.getDrawable(holder.iconSelect.resources, R.drawable.bg_circle_camera, null)
            }
            holder.layout.setOnClickListener {
                onClickCallBack.onClickCallBack(inspectorModel)
            }
        }
    }

    override fun getItemCount(): Int {
        return inspectorList.size
    }

    class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var iconSelect : ImageView = itemView.findViewById(R.id.iconSelect)
        var title : TextView = itemView.findViewById(R.id.title)
        var layout : LinearLayout = itemView.findViewById(R.id.layout)
    }

    interface OnClickCallBackListener {
        fun onClickCallBack(inspectorModel: InspectorModel)
    }
}