package com.eazy.daikou.ui.home.home_menu.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.helper.AbsoluteFitLayoutManager
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.helper.Utils.spanHorizentalGridLayoutManager
import com.eazy.daikou.model.home.MainItemHomeModel
import com.eazy.daikou.model.home.SubItemHomeModel
import com.eazy.daikou.ui.home.book_now.booking_hotel.adapter.BookingHomeItemAdapter
import com.eazy.daikou.ui.home.book_now.booking_hotel.adapter.HotelVisitItemAdapter
import com.eazy.daikou.ui.home.hrm.adapter.ItemHRAdapter
import com.eazy.daikou.ui.home.my_property_v1.adapter.PropertyFeatureAdapter


class MainHomeShowItemAdapter(
    private val action: String,
    private val itemList: ArrayList<MainItemHomeModel>,
    private val context: Activity,
    private val propertyClick: PropertyClick
) :
    RecyclerView.Adapter<MainHomeShowItemAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(context).inflate(R.layout.main_item_home_layout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val propertyModel = itemList[position]
        holder.propertyName.text =
            if (propertyModel.headerTitle != null) propertyModel.headerTitle else ""

        when (action) {
            "more_menu_activity" -> {
                holder.btnSeeAll.visibility = View.GONE
                holder.recyclerView.layoutManager = Utils.spanGridLayoutManager(
                    context,
                    propertyModel.subItemMoreModelList.size
                )
                val adapter = ItemHRAdapter(
                    context,
                    "sub_home_menu",
                    propertyModel.subItemMoreModelList,
                    object : ItemHRAdapter.ItemClickOnListener {
                        override fun onItemClickId(id: Int) {}

                        override fun onItemClickAction(action: String?) {
                            action?.let {
                                propertyClick.onClickSeeAllCallBackListener(
                                    it,
                                    "",
                                    propertyModel.subItemMoreModelList
                                )
                            }
                        }

                    })
                holder.recyclerView.adapter = adapter
                holder.recyclerView.isNestedScrollingEnabled = false
                holder.linRecyclerView.background = null
            }

            "property_detail_item" -> {
                holder.recyclerView.layoutManager = GridLayoutManager(context, 2)
                holder.recyclerView.adapter = propertyModel.headerTitle?.let {
                    PropertyFeatureAdapter(
                        it,
                        propertyModel.subItemMoreModelList
                    )
                }

                holder.linRecyclerView.setBackgroundResource(R.drawable.custom_card_item_gradient)
                holder.linRecyclerView.setPadding(5, 5, 5, 5)

                val layoutParams = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
                )
                layoutParams.setMargins(0, 0, 30, 0)
                holder.headerTitleLayout.layoutParams = layoutParams
                holder.btnSeeAll.visibility = View.GONE
                holder.recyclerView.isNestedScrollingEnabled = false
            }

            "booking_menu" -> {
                holder.descriptionTv.visibility = View.VISIBLE
                holder.propertyName.setPadding(20, 20, 20, 0)

                holder.propertyName.setTextColor(
                    Utils.getColor(
                        context,
                        R.color.book_now_secondary
                    )
                )

                holder.linRecyclerView.setPadding(5, 5, 5, 5)
                Utils.setValueOnText(holder.descriptionTv, propertyModel.descriptionTitle)

                val linearLayoutManager = LinearLayoutManager(context)

                when {
                    propertyModel.mainAction.equals("all_nearby_hotels", false) -> {
                        holder.recyclerView.layoutManager =
                            AbsoluteFitLayoutManager(
                                context,
                                1,
                                RecyclerView.HORIZONTAL,
                                false,
                                2
                            )
                    }
                    propertyModel.mainAction.equals("featuring_hotel", false) -> {
                        holder.recyclerView.layoutManager =
                            spanHorizentalGridLayoutManager(context, true)
                    }
                    propertyModel.mainAction.equals("bestseller_listing", false) -> {
                        holder.recyclerView.layoutManager = linearLayoutManager
                    }
                    else -> {
                        holder.recyclerView.layoutManager =
                            AbsoluteFitLayoutManager(
                                context,
                                1,
                                RecyclerView.HORIZONTAL,
                                false,
                                2
                            )
                    }
                }

                val bookingHomeItemAdapter = BookingHomeItemAdapter(
                    context,
                    propertyModel.mainAction!!,
                    propertyModel.subItemHomeModelList,
                    onClickSubItemBookingListener
                )
                holder.recyclerView.adapter = bookingHomeItemAdapter

                //See All Button
                holder.btnSeeAll.setCompoundDrawablesWithIntrinsicBounds(
                    0,
                    0,
                    R.drawable.ic_arrow_next,
                    0
                )
                holder.btnSeeAll.text = ""
                holder.btnSeeAll.setPadding(20, 20, 20, 0)

                // Click see all
                holder.headerTitleLayout.setOnClickListener(
                    CustomSetOnClickViewListener {
                        propertyClick.onClickSeeAllCallBackListener(
                            propertyModel.headerTitle!!,
                            propertyModel.mainAction!!,
                            propertyModel.subItemHomeModelList
                        )
                    }
                )
                holder.descriptionTv.setOnClickListener(
                    CustomSetOnClickViewListener {
                        propertyClick.onClickSeeAllCallBackListener(
                            propertyModel.headerTitle!!,
                            propertyModel.mainAction!!,
                            propertyModel.subItemHomeModelList
                        )
                    }
                )
            }

            "booking_visit" -> {
                holder.descriptionTv.visibility = View.GONE
                holder.propertyName.setPadding(20, 20, 20, 0)

                holder.propertyName.setTextColor(
                    Utils.getColor(
                        context,
                        R.color.book_now_secondary
                    )
                )

                holder.linRecyclerView.setPadding(5, 5, 5, 5)

                val linearLayoutManager = LinearLayoutManager(context)

                when {
                    propertyModel.mainAction.equals("popular", false) -> {
                        holder.recyclerView.layoutManager =
                            AbsoluteFitLayoutManager(context, 1, RecyclerView.HORIZONTAL, false, 2)
                    }
                    propertyModel.mainAction.equals("food", false) -> {
                        holder.recyclerView.layoutManager =
                            spanHorizentalGridLayoutManager(context, true)
                    }
                    propertyModel.mainAction.equals("activity", false) -> {
                        holder.recyclerView.layoutManager =
                            AbsoluteFitLayoutManager(context, 1, RecyclerView.HORIZONTAL, false, 2)
                    }
                    propertyModel.mainAction.equals("news", false) -> {
                        holder.recyclerView.layoutManager = linearLayoutManager
                    }
                    else -> {   // visit sub by location
                        holder.recyclerView.layoutManager =
                            AbsoluteFitLayoutManager(context, 1, RecyclerView.HORIZONTAL, false, 2)
                    }
                }

                val bookingHomeItemAdapter = HotelVisitItemAdapter(
                    context,
                    propertyModel.mainAction!!,
                    propertyModel.subItemHomeModelList,
                    onClickSubItemBookingListener
                )
                holder.recyclerView.adapter = bookingHomeItemAdapter

                holder.btnSeeAll.visibility = View.GONE

            }

            else -> {
                holder.recyclerView.layoutManager =
                    spanHorizentalGridLayoutManager(context, false)
                holder.recyclerView.adapter = HomeShowItemViewAdapter(
                    context,
                    propertyModel.subItemHomeModelList,
                    context,
                    onClickSubItemListener
                )

                holder.btnSeeAll.setOnClickListener(CustomSetOnClickViewListener {
                    propertyClick.onClickSeeAllCallBackListener(
                        propertyModel.headerTitle!!,
                        propertyModel.mainAction!!,
                        propertyModel.subItemHomeModelList
                    )
                }
                )
                holder.linRecyclerView.background = null
            }
        }
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val propertyName: TextView = itemView.findViewById(R.id.headerInfoTv)
        val recyclerView: RecyclerView = itemView.findViewById(R.id.recyclerView)
        val btnSeeAll: TextView = itemView.findViewById(R.id.btnSeeAll)
        val linRecyclerView: LinearLayout = itemView.findViewById(R.id.linRecyclerView)
        val headerTitleLayout: RelativeLayout = itemView.findViewById(R.id.headerTitleLayout)
        val descriptionTv: TextView = itemView.findViewById(R.id.descriptionTv)
    }

    private var onClickSubItemListener = object : HomeShowItemViewAdapter.PropertyClick {
        override fun onBookingClick(propertyModel: SubItemHomeModel?) {
            if (propertyModel != null) {
                propertyClick.onClickCallBackListener("", propertyModel)
            }
        }

    }

    private var onClickSubItemBookingListener = object : BookingHomeItemAdapter.PropertyClick {
        override fun onBookingClick(propertyModel: SubItemHomeModel?) {
            if (propertyModel != null) {
                propertyClick.onClickCallBackListener("", propertyModel)
            }
        }

        override fun onBookingWishlistClick(propertyModel: SubItemHomeModel?) {
            if (propertyModel != null) {
                propertyClick.onClickCallBackListener("action_wishlist", propertyModel)
            }
        }

    }

    interface PropertyClick {
        fun onClickCallBackListener(actionWishlist: String, subItemHomeModel: SubItemHomeModel)
        fun onClickSeeAllCallBackListener(
            action: String,
            actionType: String,
            subItemHomeModel: ArrayList<*>
        )
    }

    fun clear() {
        val size: Int = itemList.size
        if (size > 0) {
            for (i in 0 until size) {
                itemList.removeAt(0)
            }
            notifyItemRangeRemoved(0, size)
        }
    }

}