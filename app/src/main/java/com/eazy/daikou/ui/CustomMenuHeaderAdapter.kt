package com.eazy.daikou.ui

import android.app.Activity
import android.content.Context
import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.CustomCategoryModel

class CustomMenuHeaderAdapter(private val action: String, private val list: ArrayList<CustomCategoryModel>, private val itemClickListener: ClickCallBackListener) :
    RecyclerView.Adapter<CustomMenuHeaderAdapter.ItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.custom_menu_header_model, parent, false)
        return ItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.setImage(action, list, list[position], position, itemClickListener)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private var txt1: TextView = view.findViewById(R.id.txt1)
        private var txt2: TextView = view.findViewById(R.id.txt2)
        private var img3: ImageView = view.findViewById(R.id.img3)
        private var img2: ImageView = view.findViewById(R.id.img2)
        private var img1: ImageView = view.findViewById(R.id.img1)
        private var iconCheckDone1 : ImageView = view.findViewById(R.id.iconCheckDone1)
        private var iconCheckDone2 : ImageView = view.findViewById(R.id.iconCheckDone2)

        private var layout1: LinearLayout = view.findViewById(R.id.layout1)
        private var layout2: RelativeLayout = view.findViewById(R.id.layout2)
        private var mainLayout: LinearLayout = view.findViewById(R.id.mainLayout)
        private var linear1 : LinearLayout = view.findViewById(R.id.linear1)
        private var linear2 : LinearLayout = view.findViewById(R.id.linear2)

        fun setImage(action: String, list: ArrayList<CustomCategoryModel>, item: CustomCategoryModel, position: Int, itemClickListener: ClickCallBackListener) {
            val layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            val size = Utils.dpToPx(mainLayout.context, 10)

            Utils.setValueOnText(txt1, item.name)
            Utils.setValueOnText(txt2, item.name)

            if (position == 0) {
                layout1.visibility = View.VISIBLE
                layout2.visibility = View.GONE
                layoutParams.setMargins(size, 0, 0, 0)
            } else {
                layout2.visibility = View.VISIBLE
                layout1.visibility = View.GONE
                if (list.size - 1 == position) {
                    layoutParams.setMargins(0, 0, size, 0)
                    img3.visibility = View.VISIBLE
                } else {
                    layoutParams.setMargins(0, 0, 0, 0)
                    img3.visibility = View.VISIBLE
                }
            }
            mainLayout.layoutParams = layoutParams

            itemView.setOnClickListener(CustomSetOnClickViewListener {
                itemClickListener.onClickCallBack(item)
            })

            // Data record status
            if (action == "status"){
                img1.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(img3.context, R.color.gray))
                img2.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(img2.context, R.color.white))
                img3.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(img3.context, R.color.gray))
                if (item.isClick){
                    linear1.setBackgroundColor(Utils.getColor(img2.context, R.color.green))
                    linear2.setBackgroundColor(Utils.getColor(img2.context, R.color.green))
                    img1.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(img3.context, R.color.green))
                    img3.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(img3.context, R.color.green))
                    iconCheckDone1.visibility = View.VISIBLE
                    iconCheckDone2.visibility = View.VISIBLE
                } else {
                    linear1.setBackgroundColor(Utils.getColor(img2.context, R.color.gray))
                    linear2.setBackgroundColor(Utils.getColor(img2.context, R.color.gray))
                    img1.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(img3.context, R.color.gray))
                    img3.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(img3.context, R.color.gray))
                    iconCheckDone1.visibility = View.GONE
                    iconCheckDone2.visibility = View.GONE
                }
            }
        }
    }

    interface ClickCallBackListener {
        fun onClickCallBack(item: CustomCategoryModel)
    }

    companion object {
        fun addMenuStepList(context : Context, unitNo : String, action : String) : ArrayList<CustomCategoryModel> {
            val list = ArrayList<CustomCategoryModel>()
            list.add(CustomCategoryModel("home", context.getString(R.string.title_home)))
            if (action == "my_property"){
                list.add(CustomCategoryModel("my_property", context.getString(R.string.my_property)))
            } else if (action == "payment_scheduled"){
                list.add(CustomCategoryModel("my_property", context.getString(R.string.my_property)))
                list.add(CustomCategoryModel("my_unit", context.getString(R.string.my_unit_)))
                list.add(CustomCategoryModel("my_unit_detail", unitNo))
            }
            return list
        }

        fun setMenuStepAdapter(context: Activity, action: String, list : ArrayList<CustomCategoryModel>, clickCallBackListener : ClickCallBackListener){
            val menuStepRecycle = context.findViewById<RecyclerView>(R.id.menuStepRecycle)
            menuStepRecycle.layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
            menuStepRecycle.adapter = CustomMenuHeaderAdapter(action, list, object : ClickCallBackListener{
                override fun onClickCallBack(item: CustomCategoryModel) {
                    clickCallBackListener.onClickCallBack(item)
                }
            })
        }
    }

}