package com.eazy.daikou.ui.home.my_property.my_unit

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.widget.Toast
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.helper.DateUtil
import com.eazy.daikou.helper.GetDataUtils
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.home.HomeViewModel
import com.eazy.daikou.model.my_property.my_property.DocumentModel
import com.eazy.daikou.model.my_property.payment_schedule_my_unit.DeveloperInfo
import com.eazy.daikou.ui.home.hrm.adapter.MyRoleMainItemAdapter
import com.eazy.daikou.ui.home.my_property.adapter.DeveloperPaymentAdapter

class MyUnitDocumentDeveloperActivity : BaseActivity() {

    private lateinit var recyclerView: RecyclerView
    private var action = ""
    private var developerInfoList : ArrayList<DeveloperInfo> = ArrayList()
    private var listDocument : ArrayList<DocumentModel> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_unit_document_acitivity)

        initView()

        initData()

        initAction()

    }

    private fun initView(){
        recyclerView = findViewById(R.id.recyclerView)
    }

    private fun initData(){
        action = GetDataUtils.getDataFromString("action", this)

        if (intent != null && intent.hasExtra("developer_list")){
            developerInfoList = intent.getSerializableExtra("developer_list") as ArrayList<DeveloperInfo>
        }

        if (intent != null && intent.hasExtra("document_list")){
            listDocument = intent.getSerializableExtra("document_list") as ArrayList<DocumentModel>
        }
    }

    private fun initAction(){
        recyclerView.layoutManager = LinearLayoutManager(this)
        if (action == "developer_info"){
            Utils.customOnToolbar(this, resources.getString(R.string.developer_info)){finish()}

            initRecyclerViewDeveloper()
        } else {
            Utils.customOnToolbar(this, resources.getString(R.string.my_document)){finish()}

            initRecyclerViewDocument()
        }
    }

    // ====================== Developer Info ===================
    private fun initRecyclerViewDeveloper() {
        recyclerView.adapter = DeveloperPaymentAdapter(developerInfoList)

        Utils.validateViewNoItemFound(this, recyclerView, developerInfoList.size <= 0)
    }

    // ======================= Document ========================
    private fun initRecyclerViewDocument(){
        val homeViewModels: ArrayList<HomeViewModel> = ArrayList()
        for(item in listDocument){
            val createdDt = DateUtil.appendFormatDateTime(item.created_at)
            homeViewModels.add(HomeViewModel(createdDt, item.file_path,
                ResourcesCompat.getDrawable(resources, R.drawable.ic_document, null), item.name, color()))
        }

        recyclerView.adapter = MyRoleMainItemAdapter(homeViewModels, object : MyRoleMainItemAdapter.HomeCallBack{
            override fun clickIconListener(item: HomeViewModel) {
                Utils.openFilePdfOrPic(item.action, this@MyUnitDocumentDeveloperActivity)
            }
        })

        Utils.validateViewNoItemFound(this, recyclerView, listDocument.size <= 0)
    }

    private var i = 0
    private fun color() : Int{
        return Utils.setColorBackground(i) { index -> i = index }
    }
}