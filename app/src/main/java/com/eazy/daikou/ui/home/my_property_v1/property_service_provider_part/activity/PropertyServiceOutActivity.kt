package com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.base.MockUpData
import com.eazy.daikou.request_data.request.service_property_ws.ServiceAndPropertyWs
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.helper.web.WebsitePropertyActivity
import com.eazy.daikou.model.my_property.service_property_employee.ServicePropertyIn
import com.eazy.daikou.model.my_property.service_provider.ServicePropertyModel
import com.eazy.daikou.model.my_property.service_provider.ServiceTypeModel
import com.eazy.daikou.model.profile.User
import com.eazy.daikou.ui.MainActivity
import com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.adapter.CategoryServiceAdapter
import com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.adapter.PropertyAndServiceAdapter
import com.google.gson.Gson

class PropertyServiceOutActivity : BaseActivity() {

    private var recyclerView: RecyclerView? = null
    private lateinit var gridLayoutManager: StaggeredGridLayoutManager
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var progressBar: ProgressBar
    private lateinit var pullRefresh: SwipeRefreshLayout
    private lateinit var recyclerViewTap: RecyclerView
    private lateinit var tapAdapter: CategoryServiceAdapter
    private lateinit var propertyAndServiceAdapter: PropertyAndServiceAdapter
    private lateinit var user: User
    private lateinit var switchView: ImageView

    private var propertyServiceList: ArrayList<ServicePropertyModel> = ArrayList()
    private var serviceTypeModelList: ArrayList<ServiceTypeModel> = ArrayList()

    private var isBack = ""

    private var currentItem = 0
    private var total: Int = 0
    private var scrollDown: Int = 0
    private var currentPage: Int = 1
    private var size: Int = 10
    private var propertyId = ""
    private var categoryId = ""
    private var isScrollItem = false

    @SuppressLint("CutPasteId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_property_service)

        initView()

        initData()

        initAction()
    }

    private fun initView(){
        val titleTv = findViewById<TextView>(R.id.title)
        titleTv.text = "Constructor Service"

        progressBar = findViewById(R.id.progressItem)
        recyclerViewTap = findViewById(R.id.list_all_tap)
        recyclerView = findViewById(R.id.list_all_property_service)
        pullRefresh = findViewById(R.id.pull_fresh)
        switchView = findViewById(R.id.switchView)
    }

    private fun initData(){
        val userSessionManagement = UserSessionManagement(this)
        user = Gson().fromJson(userSessionManagement.userDetail, User::class.java)
        propertyId = user.activePropertyIdFk

        MockUpData.SWITCH_VIEW = 1
        switchView.visibility = View.VISIBLE
        switchView.setImageResource(R.drawable.ic_file_row)
        pullRefresh.setColorSchemeResources(R.color.colorPrimary)

        if(intent.hasExtra("is_back")){
            isBack = intent.getStringExtra("is_back") as String
        }
    }

    private fun initAction(){
        val layoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
        recyclerViewTap.layoutManager = layoutManager

        findViewById<ImageView>(R.id.backButton).setOnClickListener {
            if(isBack == "is"){
                startActivity(Intent(this, MainActivity::class.java))
            } else {
                finish()
            }

        }

        initServiceProperty(propertyServiceList)

        getCategoryService()
    }

    private fun getCategoryService(){
        progressBar.visibility = View.VISIBLE
        ServiceAndPropertyWs().getListServiceCategory(this, propertyId, "outdoor_service", callBackService)
    }

    private fun requestItemListService(){
        progressBar.visibility = View.VISIBLE
        ServiceAndPropertyWs().getListOutDoorServiceProperty(this, propertyId, "outdoor_service", categoryId, currentPage, callBackService)
    }

    private val callBackService = object : ServiceAndPropertyWs.OnCallBackServiceInListener {
        @SuppressLint("NotifyDataSetChanged")
        override fun onLoadSuccessFull(listService: ArrayList<ServicePropertyIn>) {
            // Load service in door
        }

        @SuppressLint("NotifyDataSetChanged")
        override fun onLoadGetServiceOutDoor(serviceModels: ArrayList<ServicePropertyModel>) {
            progressBar.visibility = View.GONE
            pullRefresh.isRefreshing = false
            if (!isScrollItem)  propertyServiceList.clear()        //Clear item
            propertyServiceList.addAll(serviceModels)
            //addList()

            Utils.validateViewNoItemFound(this@PropertyServiceOutActivity, recyclerView, propertyServiceList.size <= 0)
            propertyAndServiceAdapter.notifyDataSetChanged()
        }

        override fun onLoadSuccess(listService: ArrayList<ServiceTypeModel>) {
            progressBar.visibility = View.GONE
            pullRefresh.isRefreshing = false
            serviceTypeModelList.addAll(listService)

            if (serviceTypeModelList.size > 0) {
                categoryId = serviceTypeModelList[0].serviceCategoryKey
                serviceTypeModelList[0].isClick = true
                tapAdapter = CategoryServiceAdapter(serviceTypeModelList, this@PropertyServiceOutActivity, itemClickListener)
                recyclerViewTap.adapter = tapAdapter

                 requestItemListService()
            } else {
                Utils.validateViewNoItemFound(this@PropertyServiceOutActivity, recyclerView, true)
            }
        }

        override fun onLoadFail(message: String?) {
            progressBar.visibility = View.GONE
            pullRefresh.isRefreshing = false
            Utils.customToastMsgError(this@PropertyServiceOutActivity, message, false)
        }

    }

    @SuppressLint("NotifyDataSetChanged")
    private var itemClickListener = CategoryServiceAdapter.ItemClickCategoryServiceProvider { listString ->
        isScrollItem = false
        categoryId = listString
        for (itemService in serviceTypeModelList){
            itemService.isClick = itemService.serviceCategoryKey == listString
        }
        tapAdapter.notifyDataSetChanged()

        refreshList()
    }

    private fun initServiceProperty(serviceProperty: ArrayList<ServicePropertyModel>) {
        gridLayoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
        linearLayoutManager = LinearLayoutManager(this)
        recyclerView?.layoutManager = gridLayoutManager

        propertyAndServiceAdapter = PropertyAndServiceAdapter(
            this@PropertyServiceOutActivity,
            2,
            serviceProperty,
            itemClickCallback
        )
        recyclerView?.adapter = propertyAndServiceAdapter

        initSwitchItem(serviceProperty)

        recyclerView?.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (MockUpData.SWITCH_VIEW == 1) {
                    currentItem = linearLayoutManager.childCount
                    total = linearLayoutManager.itemCount
                    scrollDown = linearLayoutManager.findFirstVisibleItemPosition()
                } else if (MockUpData.SWITCH_VIEW == 2) {
                    currentItem = gridLayoutManager.childCount
                    total = gridLayoutManager.itemCount
                    val into = IntArray((recyclerView.layoutManager as StaggeredGridLayoutManager).spanCount)
                    (recyclerView.layoutManager as StaggeredGridLayoutManager).findFirstVisibleItemPositions(into)
                    val firstVisiblePosition: Int = Int.MAX_VALUE
                    for (pos in into) {
                        scrollDown = pos.coerceAtMost(firstVisiblePosition) - 1
                    }
                }
                if (dy > 0) {
                    if (currentItem + scrollDown == total) {
                        if (total == size) {
                            currentPage++
                            progressBar.visibility = View.VISIBLE
                            isScrollItem = true
                            recyclerView.scrollToPosition(propertyServiceList.size - 1)
                            requestItemListService()
                            size += 10
                        }
                    }
                }
            }
        })

        pullRefresh.setOnRefreshListener {
            refreshList()
        }
    }

    private fun initSwitchItem(serviceProperty: ArrayList<ServicePropertyModel>){
        if (MockUpData.SWITCH_VIEW == 1) {
            recyclerView?.layoutManager = linearLayoutManager
            propertyAndServiceAdapter.setViewType()
        } else if (MockUpData.SWITCH_VIEW == 2) {
            recyclerView?.layoutManager = gridLayoutManager
            propertyAndServiceAdapter.setViewType()
        }

        switchView.setOnClickListener {
            if (MockUpData.SWITCH_VIEW == 2) {
                recyclerView?.layoutManager = linearLayoutManager
                MockUpData.SWITCH_VIEW = 1
                switchView.setImageResource(R.drawable.ic_file_row)
            } else {
                recyclerView?.layoutManager = gridLayoutManager
                MockUpData.SWITCH_VIEW = 2
                switchView.setImageResource(R.drawable.ic_file_list_numbered_24)
            }
            propertyAndServiceAdapter = PropertyAndServiceAdapter(
                this@PropertyServiceOutActivity,
                2,
                serviceProperty,
                itemClickCallback
            )
            recyclerView?.adapter = propertyAndServiceAdapter
            recyclerView?.scrollToPosition(scrollDown - 1)
        }
    }

    private fun refreshList(){
        currentPage = 1
        size = 10

        propertyAndServiceAdapter.clear()
        requestItemListService()
    }

    private val itemClickCallback = object : PropertyAndServiceAdapter.OnClickProperty {
        override fun onClickCompany(category: ServicePropertyModel) {
                if (category.serviceWebsite != null) {
                    val intent = Intent(applicationContext, WebsitePropertyActivity::class.java)
                    intent.putExtra("linkUrlServiceProperty", category.serviceWebsite)
                    intent.putExtra("contractor_name", category.serviceName)
                    startActivity(intent)
                } else {
                    Toast.makeText(applicationContext, resources.getString(R.string.not_available_website), Toast.LENGTH_SHORT).show()
                }
        }
        override fun onClickMenu(title: String, product: ServicePropertyModel) {}
    }

}