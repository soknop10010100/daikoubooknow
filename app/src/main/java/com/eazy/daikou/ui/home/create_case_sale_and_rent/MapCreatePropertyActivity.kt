package com.eazy.daikou.ui.home.create_case_sale_and_rent

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Point
import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.helper.GPSTracker
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import java.io.IOException
import java.lang.Exception
import java.util.*

class MapCreatePropertyActivity : BaseActivity() , OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    private lateinit var geocoder: Geocoder
    private lateinit var centerImage: ImageView
    private lateinit var search: LinearLayout
    private lateinit var txtMap: TextView
    private var saveCountryCode = ""
    private var saveLat = 0.0
    private  var saveLng = 0.0
    private  var userLat = 0.0
    private  var userLng = 0.0
    private  var checkSelectMap = " "
    private  var fullAddress  = ""
    private var intentLat = 0.0
    private  var intentLng = 0.0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_map_create_property)

        initView()

    }

    private fun initView(){
        val btnBack: TextView = findViewById(R.id.backButton)
        search = findViewById(R.id.map_delivery_location)
        txtMap = findViewById(R.id.txtMap)
        centerImage = findViewById(R.id.center_image)

        btnBack.setOnClickListener { finish() }

        if (intent != null && intent.hasExtra("gotoMap")) {
            checkSelectMap = intent.getStringExtra("gotoMap") as String
            intentLat = intent.getDoubleExtra("userLat", 0.0)
            intentLng = intent.getDoubleExtra("userLng", 0.0)
        }
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = (supportFragmentManager
            .findFragmentById(R.id.google_map) as SupportMapFragment?)!!
        mapFragment.getMapAsync(this)
        callLocation()
        geocoder = Geocoder(this, Locale.getDefault())
        findViewById<Button>(R.id.setLocationButton).setOnClickListener {
            val address = displayAddressByImageCenter()
            popupInformLocation(address)
        }
    }


    private fun callLocation() {
        val gps = GPSTracker(this)
        if (gps.canGetLocation()) {
            userLat = gps.latitude
            userLng = gps.longitude
        }
    }
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        if (checkSelectMap == "gotoMap") {
            zoomMap(intentLat, intentLng)
            checkSelectMap = "gotoMapTaxi"
        } else {
            zoomMap(latitude, longitude)
        }
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }
        mMap.isMyLocationEnabled = true
        mMap.uiSettings.isZoomControlsEnabled = false
        mMap.uiSettings.isMyLocationButtonEnabled = true
        mMap.uiSettings.isCompassEnabled = false //hide compass

        mMap.setOnCameraIdleListener { displayAddressByImageCenter() }
    }
    private fun zoomMap(lat: Double, lng: Double) {
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(lat, lng), 15f))
    }

    private fun displayAddressByImageCenter(): String {
        try {
            val h = centerImage.height
            val w = centerImage.width / 2
            val x = centerImage.x.toInt() + w
            val y = centerImage.y.toInt() + h / 2
            val ll = mMap.projection.fromScreenLocation(Point(x, y + 45))
            val addresses = geocoder.getFromLocation(ll.latitude, ll.longitude, 1)
            val fullAdd: String = getFullAddress(addresses)
            try { saveCountryCode = if (addresses[0].countryName != null) addresses[0].countryCode else country_code
            } catch (e: Exception) {
                e.printStackTrace()
            }
            txtMap.text = String.format("%s", fullAdd)
            fullAddress = fullAdd
            saveLat = ll.latitude
            saveLng = ll.longitude
            return fullAdd
        } catch (e: IOException) {

        }
        return ""
    }
    private fun getFullAddress(addresses: List<Address>): String {
        return if (addresses.isNotEmpty()) {
            addresses[0].getAddressLine(0)
        } else ""
    }

    private fun popupInformLocation(fullAddress: String) {
        val builder = AlertDialog.Builder(this)
        builder.setMessage(fullAddress)
        builder.setNegativeButton(getString(R.string.no), null)
        builder.setPositiveButton(getString(R.string.yes)) { _, _ ->
            val data = Intent()
            data.putExtra("delivery_address", fullAddress)
            data.putExtra("lat", saveLat)
            data.putExtra("lng", saveLng)
            setResult(RESULT_OK, data)
            finish()
        }
        builder.show()
    }

}