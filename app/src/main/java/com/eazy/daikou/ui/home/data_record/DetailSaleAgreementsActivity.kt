package com.eazy.daikou.ui.home.data_record

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.request_data.request.data_record.DataRecordWS
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.helper.web.WebsitePropertyActivity
import com.eazy.daikou.model.CustomCategoryModel
import com.eazy.daikou.model.data_record.AllStatus
import com.eazy.daikou.model.data_record.DetailHoldAndReservedModel
import com.eazy.daikou.model.data_record.PaymentSchedule
import com.eazy.daikou.model.profile.User
import com.eazy.daikou.ui.CustomMenuHeaderAdapter
import com.eazy.daikou.ui.home.data_record.adapter.AdapterPaymentScheduleDR
import com.google.gson.Gson
import libs.mjn.prettydialog.PrettyDialog

class DetailSaleAgreementsActivity : BaseActivity() {

    private lateinit var progressBar: ProgressBar
    private lateinit var menuStepRecycle : RecyclerView
    private lateinit var projectNameTv : TextView

    private lateinit var spaNoTv : TextView
    private lateinit var unitTypeSPATv : TextView
    private lateinit var unitSpaTv : TextView
    private lateinit var floorTv : TextView
    private lateinit var customerNameTv : TextView
    private lateinit var customerPhoneBookingTv : TextView
    private lateinit var purchaseDatePsaTv : TextView

    private lateinit var bookingNoTv : TextView
    private lateinit var sellingPriceTv : TextView
    private lateinit var channelTv : TextView
    private lateinit var agencyTv : TextView
    private lateinit var brokerTv : TextView
    private lateinit var supportTeamTv : TextView
    private lateinit var discountTv : TextView
    private lateinit var voucherAmountTv : TextView
    private lateinit var amountTv : TextView
    private lateinit var startDateTv : TextView
    private lateinit var endDateTv : TextView
    private lateinit var amountTotalTv : TextView
    private lateinit var intersAmountTv : TextView
    private lateinit var paymentsTv : TextView

    private lateinit var prettyDialog: PrettyDialog

    private lateinit var iconViewInvoice : LinearLayout
    private lateinit var linearPaymentSchedule : LinearLayout
    private lateinit var linearDownPaymentSchedule : LinearLayout
    private lateinit var iconDownPaymentSchedule : ImageView
    private lateinit var noDataPaymentSchedule : TextView
    private lateinit var lineaTotalPayment : LinearLayout
    private lateinit var linearSaleAgreement : LinearLayout
    private lateinit var recyclerPaymentReschedule : RecyclerView
    private lateinit var adapterPaymentSchedule : AdapterPaymentScheduleDR
    private val listPaymentSchedule: ArrayList<PaymentSchedule> = ArrayList()

    private var idList = ""
    private lateinit var user : User
    private var userName = ""
    private var userPhone = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_sale_agreements)

        initView()

        initData()

        initAction()

    }

    private fun initView(){
        lineaTotalPayment = findViewById(R.id.lineaTotalPayment)
        noDataPaymentSchedule = findViewById(R.id.noDataPaymentSchedule)
        linearPaymentSchedule = findViewById(R.id.linearPaymentSchedule)
        linearDownPaymentSchedule = findViewById(R.id.linearDownPaymentSchedule)
        recyclerPaymentReschedule = findViewById(R.id.recyclerPaymentReschedule)
        iconDownPaymentSchedule = findViewById(R.id.iconDownPaymentSchedule)
        linearSaleAgreement = findViewById(R.id.linearSaleAgreement)

        progressBar = findViewById(R.id.progressItem)
        menuStepRecycle = findViewById(R.id.menuStepRecycle)
        projectNameTv = findViewById(R.id.projectNameTv)
        amountTotalTv = findViewById(R.id.amountTotalTv)
        intersAmountTv = findViewById(R.id.intersAmountTv)
        paymentsTv = findViewById(R.id.paymentsTv)

        spaNoTv = findViewById(R.id.spaNoTv)
        unitTypeSPATv = findViewById(R.id.unitTypeSPATv)
        unitSpaTv = findViewById(R.id.unitTv)
        floorTv = findViewById(R.id.floorTv)
        customerNameTv = findViewById(R.id.customerNameTv)
        customerPhoneBookingTv = findViewById(R.id.customerPhoneBookingTv)
        purchaseDatePsaTv = findViewById(R.id.purchaseDatePsaTv)

        bookingNoTv = findViewById(R.id.bookingNoTv)
        sellingPriceTv = findViewById(R.id.sellingPriceTv)
        channelTv = findViewById(R.id.channelTv)
        agencyTv = findViewById(R.id.agencyTv)
        brokerTv = findViewById(R.id.brokerTv)
        supportTeamTv = findViewById(R.id.supportTeamTv)
        discountTv = findViewById(R.id.discountTv)
        voucherAmountTv = findViewById(R.id.voucherAmountTv)
        amountTv = findViewById(R.id.amountTv)
        startDateTv = findViewById(R.id.startDateTv)
        endDateTv = findViewById(R.id.endDateTv)

        iconViewInvoice = findViewById(R.id.iconViewInvoice)
    }

    private fun initData(){

      if (intent != null && intent.hasExtra("id_list")){
          idList = intent.getStringExtra("id_list") as String
      }

        user = Gson().fromJson(UserSessionManagement(this).userDetail, User::class.java)
        val name = user.name
        val phone = user.phoneNumber

        if (name != null) { userName = name }
        if (phone != null) { userPhone = phone }
    }

    private fun initAction(){
        findViewById<ImageView>(R.id.iconBack).setOnClickListener{ finish()}
        findViewById<TextView>(R.id.titleToolbar).text = getString(R.string.sale_agreement_detail)

        linearDownPaymentSchedule.setOnClickListener( CustomSetOnClickViewListener{
            loadViewDropList(recyclerPaymentReschedule, iconDownPaymentSchedule, linearPaymentSchedule)
        })
        serviceApiSPA()

        recyclerPaymentSchedule()
    }

    private fun buttonPayment(detailHoldAndReserve: DetailHoldAndReservedModel) {
        iconViewInvoice.setOnClickListener {
            val intent = Intent(this, WebsitePropertyActivity::class.java)
            intent.putExtra("url_web", detailHoldAndReserve.invoice_receipt_url)
            intent.putExtra("link_kess",detailHoldAndReserve.payment_kess_url)
            startActivity(intent)
        }
    }

    private fun recyclerPaymentSchedule(){
        val linearLayoutManager = LinearLayoutManager(this)
        recyclerPaymentReschedule.layoutManager = linearLayoutManager
        adapterPaymentSchedule = AdapterPaymentScheduleDR(this, "action_click",listPaymentSchedule, callBackClickInvoice)
        recyclerPaymentReschedule.adapter = adapterPaymentSchedule
    }

    private val callBackClickInvoice = object : AdapterPaymentScheduleDR.CallBackItemClickListener {
        override fun clickItemCallback(paymentSchedule: PaymentSchedule) {
            if (paymentSchedule.payment_status == "no_invoice"){
                prettyDialog = PrettyDialog(this@DetailSaleAgreementsActivity)
                prettyDialog
                    .setTitle(getString(R.string.invalid_invoice))
                    .setIcon(R.drawable.pdlg_icon_info)
                    .setIconTint(R.color.pdlg_color_green)
                    .show()
            } else{
                val intent = Intent(this@DetailSaleAgreementsActivity, WebsitePropertyActivity::class.java)
                intent.putExtra("url_web", paymentSchedule.invoice_receipt_url)
                //  intent.putExtra("link_kess",detailHoldAndReserve.payment_kess_url)
                startActivity(intent)
            }
        }
    }

    private fun loadViewDropList(recyclerViewDrop: RecyclerView, iconDrop : ImageView, linearData : LinearLayout){
        if(recyclerViewDrop.visibility == View.GONE){
            recyclerViewDrop.visibility = View.VISIBLE
            linearData.visibility = View.VISIBLE
            iconDrop.setImageResource(R.drawable.ic_arrow_next)

        } else {
            recyclerViewDrop.visibility = View.GONE
            iconDrop.setImageResource(R.drawable.ic_drop_down)
            linearData.visibility = View.GONE
        }
    }

    private fun setMenuStepAdapter(context: Activity, listStepMenu : ArrayList<AllStatus>){
        val list = ArrayList<CustomCategoryModel>()
        for (item in listStepMenu){
            list.add(CustomCategoryModel(item.status, item.status_display, item.is_active))
        }

        CustomMenuHeaderAdapter.setMenuStepAdapter(context, "status", list, object : CustomMenuHeaderAdapter.ClickCallBackListener{
            override fun onClickCallBack(item: CustomCategoryModel) {}
        })
    }

    private fun serviceApiSPA(){
        progressBar.visibility = View.VISIBLE
        DataRecordWS().getSaleAgreementDetailWS(this, idList, callBackSaleAgreementsDetail)
    }

    private val callBackSaleAgreementsDetail = object  : DataRecordWS.OnCallBackDetailHoldAndReservedListener{
        @SuppressLint("NotifyDataSetChanged")
        override fun onSuccess(detailOpportunity: DetailHoldAndReservedModel) {
            progressBar.visibility = View.GONE

            linearSaleAgreement.visibility = View.VISIBLE

            setDataDetail(detailOpportunity)

            buttonPayment(detailOpportunity)

            setMenuStepAdapter(this@DetailSaleAgreementsActivity, detailOpportunity.all_status)

            if (detailOpportunity.payment_schedules.size > 0){
                listPaymentSchedule.addAll(detailOpportunity.payment_schedules)
                lineaTotalPayment.visibility = View.VISIBLE
            } else {
                noDataPaymentSchedule.visibility = View.VISIBLE
            }
            adapterPaymentSchedule.notifyDataSetChanged()
        }

        override fun onFailed(error: String) {
            progressBar.visibility = View.GONE
            Utils.customToastMsgError(this@DetailSaleAgreementsActivity, error, false)
        }
    }

    private fun setDataDetail(detailSaleAgreement: DetailHoldAndReservedModel){


        Utils.setValueOnText(projectNameTv, if(detailSaleAgreement.account != null) detailSaleAgreement.account!!.name else ". . .")

        Utils.setValueOnText(unitSpaTv, detailSaleAgreement.unit!!.name)
        Utils.setValueOnText(floorTv, detailSaleAgreement.unit!!.floor_no)
        Utils.setValueOnText(unitTypeSPATv, if(detailSaleAgreement.unit!!.type != null) detailSaleAgreement.unit!!.type!!.name else ". . .")

        Utils.setValueOnText(spaNoTv,detailSaleAgreement.spa_no)
        Utils.setValueOnText(customerNameTv, userName)
        Utils.setValueOnText(customerPhoneBookingTv, userPhone)
        Utils.setValueOnText(purchaseDatePsaTv, Utils.formatDateFromString("yyyy-MM-dd", "dd-MMM-yyyy", detailSaleAgreement.purchase_date))

        if (detailSaleAgreement.booking != null){
            Utils.setValueOnText(bookingNoTv, detailSaleAgreement.booking!!.code)
            Utils.setValueOnText(sellingPriceTv, detailSaleAgreement.booking!!.sale_price)
            Utils.setValueOnText(channelTv, if (detailSaleAgreement.booking!!.channel != null) detailSaleAgreement.booking!!.channel!!.name else ". . .")
            Utils.setValueOnText(agencyTv,  if (detailSaleAgreement.booking!!.agency != null) detailSaleAgreement.booking!!.agency!!.name else ". . .")
            Utils.setValueOnText(brokerTv, if (detailSaleAgreement.booking!!.broker != null) detailSaleAgreement.booking!!.broker!!.name else ". . .")
            Utils.setValueOnText(supportTeamTv, if (detailSaleAgreement.booking!!.supporting_team != null ) detailSaleAgreement.booking!!.supporting_team!!.name else ". . .")
            Utils.setValueOnText(discountTv, detailSaleAgreement.booking!!.discount)
            Utils.setValueOnText(voucherAmountTv, detailSaleAgreement.booking!!.voucher_amount)
            Utils.setValueOnText(amountTv,detailSaleAgreement.booking!!.amount)
        }

        Utils.setValueOnText(amountTotalTv, detailSaleAgreement.total_amount)
        Utils.setValueOnText(intersAmountTv, detailSaleAgreement.total_interest_amount)
        Utils.setValueOnText(paymentsTv, detailSaleAgreement.total_payment_amount)



        startDateTv.text = if (detailSaleAgreement.booking!!.start_date != null) Utils.formatDateFromString("yyyy-MM-dd", "dd-MMM-yyyy", detailSaleAgreement.booking!!.start_date) else ". . ."
        endDateTv.text = if(detailSaleAgreement.booking!!.end_date != null)  Utils.formatDateFromString("yyyy-MM-dd", "dd-MMM-yyyy", detailSaleAgreement.booking!!.end_date) + " "+
                Utils.formatDateFromString("kk:mm", "hh:mm a", detailSaleAgreement.booking!!.end_time) else ". . ."
    }
}