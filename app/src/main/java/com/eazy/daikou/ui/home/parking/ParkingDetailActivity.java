package com.eazy.daikou.ui.home.parking;

import static com.eazy.daikou.helper.Utils.convert;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.eazy.daikou.R;
import com.eazy.daikou.base.BaseActivity;
import com.eazy.daikou.base.BaseCameraActivity;
import com.eazy.daikou.base.MockUpData;
import com.eazy.daikou.request_data.request.parking_ws.ParkingWs;
import com.eazy.daikou.request_data.request.parking_ws.PaymentParkingWs;
import com.eazy.daikou.helper.AbsoluteFitLayoutManager;
import com.eazy.daikou.helper.DateUtil;
import com.eazy.daikou.helper.UserSessionManagement;
import com.eazy.daikou.helper.Utils;
import com.eazy.daikou.model.parking.AllComment;
import com.eazy.daikou.model.parking.Parking;
import com.eazy.daikou.helper.web.WebPayActivity;
import com.eazy.daikou.ui.home.parking.adapter.ColorParkingAdapter;
import com.eazy.daikou.ui.home.parking.adapter.ListAllCommentAdapter;
import com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.adapter.ServiceTermAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

public class ParkingDetailActivity extends BaseActivity implements SelectPaymentMethodBottomSheetFragment.OnClickFeedBack {

    private TextView entranceGateDateTv;
    private TextView entranceGateTimeTv;
    private TextView nameProjectTv;
    private TextView platNumberTv;
    private TextView entranceLotTv;
    private TextView entranceTypeTv;
    private TextView zoneTv;
    private Parking myParking;
    private CardView cardAddInformation;
    private RelativeLayout clickPhoto1;
    private RelativeLayout clickPhoto2;
    private RelativeLayout clickPhoto3;
    private ImageView imageView1;
    private ImageView imageView2;
    private ImageView imageView3;
    private ImageView camera1;
    private ImageView camera2;
    private ImageView camera3;
    private EditText descriptionEdt;
    private EditText plateNumberEdt;
    private EditText modelCarEdt;
    private TextView createsServiceBtn;
    private TextView pricePerTv;
    private ProgressBar progressBar;
    private LinearLayout inputEditText;
    private TextView btnSeeAll;
    private CardView priceLinear;
    private CardView commentLinear;
    private CardView summaryLinear;
    private CardView paymentLinear;
    private RecyclerView recyclerView;
    private LinearLayout linearPrice;
    private LinearLayout linearPlatNo;
    private LinearLayout linearAccept;
    private TextView dateLeaveTv;
    private TextView timeLeaveTv;
    private TextView statusPaymentTv;
    private TextView priceTv;
    private TextView durationTv;
    private TextView parkingTypeTv;
    private TextView textPaymentTv;
    private TextView cardPayTv;
    private TextView btnAccept;
    private TextView carColorTv;
    private TextView modelCarTv;
    private TextView receiverTv;
    private ImageView imageViewPay;
    private LinearLayout linearAllView;
    private LinearLayout linearColor;
    private LinearLayout linearColorTx;
    private LinearLayout linearModelCar;
    private LinearLayout linearReceiver;
    private LinearLayout linearTypeParking;
    private RecyclerView colorRecycleView;
    private RecyclerView typeRecyclerView;
    private ColorParkingAdapter adapter;

    private TextView typeParkingTv;
    private TextView notificationClick;
    private TextView receiveAmountTv;
    private TextView changedAmountTv;
    private TextView ticketNoTv;
    private LinearLayout linearAddColor;
    private LinearLayout relateLotNo;
    private LinearLayout relateLotType;
    private LinearLayout relateZone;
    private LinearLayout linearTicketNo;
    private RelativeLayout imageScan;
    private CardView cardViewPlateCar;
    private ImageView imageViewPlateCar;
    private TextView paymentMethodTv;
    private TextView oderNoTv ;
    private TextView btnBack ;
    private  Dialog dialog;

    private int statClickColor = -1;
    private String userType, userId;
    private String message = "";
    private String messageReport = "";
    private final int CODE_1 = 902;
    private final int CODE_2 = 903;
    private final int CODE_3 = 904;
    private final int CODE_QR = 120;
    private final int CODE_PAY = 122;
    private final int CODE_PAID = 124;
    private final int CODE_SUCCESS_PAY = 654;
    private int state = 0;
    private String typeParking = "hourly";
    private String orderId = "";
    private String colorName = "";
    private String typePay = "PAY BY KESS";
    private int imagePay;
    private String isMachine = "";
    private String qrCodeText, qrCodeP,qrCode = " ",action = "";
    private final List<AllComment> listComment = new ArrayList<>();
    private final ArrayList<String> listImage = new ArrayList<>();
    private RelativeLayout infoLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parking_detail);

        initView();

        initData();

        initAction();

    }

    private void initView() {
        btnBack = findViewById(R.id.btn_back);
        notificationClick = findViewById(R.id.btn_notificaton);
        notificationClick.setVisibility(View.VISIBLE);
        linearAllView = findViewById(R.id.linear_all);
        nameProjectTv = findViewById(R.id.project_name_Text);
        entranceGateDateTv = findViewById(R.id.date_parking_Text);
        entranceGateTimeTv = findViewById(R.id.time_parking_Text);
        entranceLotTv = findViewById(R.id.lot_no_text);
        entranceTypeTv = findViewById(R.id.type_text);
        platNumberTv = findViewById(R.id.plate_number);
        zoneTv = findViewById(R.id.zone_Text);
        typeRecyclerView = findViewById(R.id.list_duration);

        relateLotNo = findViewById(R.id.relate_lot_no);
        relateLotType = findViewById(R.id.relate_lot_type);
        relateZone = findViewById(R.id.relate_zone);

        //card_employee
        cardAddInformation = findViewById(R.id.card_for_staff);
        clickPhoto1 = findViewById(R.id.image_1);
        clickPhoto2 = findViewById(R.id.image_2);
        clickPhoto3 = findViewById(R.id.image_3);
        imageView1 = findViewById(R.id.photo_1);
        imageView2 = findViewById(R.id.photo_2);
        imageView3 = findViewById(R.id.photo_3);
        camera1 = findViewById(R.id.camera_1);
        camera2 = findViewById(R.id.camera_2);
        camera3 = findViewById(R.id.camera_3);

        plateNumberEdt = findViewById(R.id.plate_number_Text);
        descriptionEdt = findViewById(R.id.description_Text);
        createsServiceBtn = findViewById(R.id.creates_serivce);
        pricePerTv = findViewById(R.id.price_per_Text);
        progressBar = findViewById(R.id.progressItem);
        progressBar.setVisibility(View.GONE);

        colorRecycleView = findViewById(R.id.color_list);
        linearColor = findViewById(R.id.linear_color);
        modelCarEdt = findViewById(R.id.model_car_edit);
        linearColorTx = findViewById(R.id.linear_color_Text);
        carColorTv = findViewById(R.id.car_color);
        modelCarTv = findViewById(R.id.model_car_Text);
        linearModelCar = findViewById(R.id.linear_model_car);
        typeParkingTv = findViewById(R.id.type_parking);
        linearTypeParking = findViewById(R.id.linear_type);
        linearAddColor = findViewById(R.id.add_info_color);
        imageScan = findViewById(R.id.image_scan);
        paymentMethodTv = findViewById(R.id.type_text_parking_payment_method);

        ///comment
        inputEditText = findViewById(R.id.input_text_number);
        btnSeeAll = findViewById(R.id.btn_seeAll);
        priceLinear = findViewById(R.id.card_price);
        commentLinear = findViewById(R.id.card_comment);
        recyclerView = findViewById(R.id.list_all_comment);
        linearPrice = findViewById(R.id.linear_price);
        linearPlatNo = findViewById(R.id.linear_plate);
        paymentLinear = findViewById(R.id.card_payment);

        ///card Completed Payment
        summaryLinear = findViewById(R.id.card_success);
        dateLeaveTv = findViewById(R.id.leave_date_Text);
        timeLeaveTv = findViewById(R.id.leave_time_Text);
        priceTv = findViewById(R.id.total_price_Text);
        statusPaymentTv = findViewById(R.id.statust_payment);
        durationTv = findViewById(R.id.duration_text);
        receiverTv = findViewById(R.id.receive_from_Tex);
        linearReceiver = findViewById(R.id.recieve_by_linear);
        parkingTypeTv = findViewById(R.id.type_text_parking);
        receiveAmountTv = findViewById(R.id.receive_amount);
        changedAmountTv = findViewById(R.id.changed_amount);
        linearTicketNo = findViewById(R.id.relate_parking_no);
        ticketNoTv = findViewById(R.id.ticket_no_tv);
        imageViewPlateCar = findViewById(R.id.image_plate_car);
        cardViewPlateCar = findViewById(R.id.card_image_plate_car);
        oderNoTv = findViewById(R.id.order_no);

        //card_payment
        imageViewPay = findViewById(R.id.icon_pay);
        textPaymentTv = findViewById(R.id.text_pay);
        cardPayTv = findViewById(R.id.tex_card);

        //linearAccept
        linearAccept = findViewById(R.id.btn_linear_Conform);
        btnAccept = findViewById(R.id.btn_accept_paid);
        Utils.hideKeyboard(ParkingDetailActivity.this);
        infoLayout = findViewById(R.id.infoLayout);
    }

    @SuppressLint("SetTextI18n")
    private void initData() {
        if (getIntent().hasExtra("order_id")) {
            orderId = getIntent().getStringExtra("order_id");
        }
        if (getIntent() != null && getIntent().hasExtra("type")) {
            userType = getIntent().getStringExtra("type");
        }

        if (getIntent() != null && getIntent().hasExtra("userId")) {
            userId = getIntent().getStringExtra("userId");
        }

        if (userId == null) {
            UserSessionManagement user = new UserSessionManagement(this);
            userId = user.getUserId();
        }

        if(getIntent() != null && getIntent().hasExtra("parking_with_machine")){
            isMachine = getIntent().getStringExtra("parking_with_machine");
        }

        if(getIntent()!=null && getIntent().hasExtra("qrcode")){
            qrCode = getIntent().getStringExtra("qrcode");
        }

        if (MockUpData.getStatPayment() != null) {
            state = MockUpData.getStatPayment();
        }
        if (MockUpData.getStatColor() != null) {
            statClickColor = MockUpData.getStatColor();
        }

        if (MockUpData.getImagePay() != null) {
            imagePay = MockUpData.getImagePay();
            Glide.with(this).load(imagePay).into(imageViewPay);
        }
        if (MockUpData.getTextPay() != null) {
            typePay = MockUpData.getTextPay();
            if (typePay.equals("PAY BY KESS")) {
                cardPayTv.setVisibility(View.VISIBLE);
            } else {
                cardPayTv.setVisibility(View.GONE);
            }
            textPaymentTv.setText(typePay);
        }

        if (getIntent() != null && getIntent().hasExtra("message")) {
            message = getIntent().getStringExtra("message");
        }

        if (getIntent() != null && getIntent().hasExtra("parking_report")) {
            messageReport = getIntent().getStringExtra("parking_report");
        }
        progressBar.setVisibility(View.VISIBLE);


        if(getIntent()!=null && getIntent().hasExtra("action")){
            action = getIntent().getStringExtra("action");
        }

        addViewColor();

        getDetailParking();

    }
    private void addViewColor(){
        if (userType.equals("employee")||userType.equals("developer") || userType.equals("admin")) {
            AbsoluteFitLayoutManager layout = new AbsoluteFitLayoutManager(ParkingDetailActivity.this, 1, RecyclerView.HORIZONTAL, false, 6);
            colorRecycleView.setLayoutManager(layout);
            adapter = new ColorParkingAdapter(statClickColor, MockUpData.listColor(), clickColor);
            colorRecycleView.setAdapter(adapter);

            AbsoluteFitLayoutManager layoutManager = new AbsoluteFitLayoutManager(ParkingDetailActivity.this, 1, RecyclerView.HORIZONTAL, false, 6);
            typeRecyclerView.setLayoutManager(layoutManager);
            ServiceTermAdapter typeAdapter = new ServiceTermAdapter(MockUpData.listTypeParking(), "parking", 0, this, itemClickOnService);
            typeRecyclerView.setAdapter(typeAdapter);
            notificationClick.setVisibility(View.GONE);
        } else {
            notificationClick.setVisibility(View.VISIBLE);
        }
    }


    @SuppressLint("SetTextI18n")
    private void showPayment() {
        if (myParking.getPaymentMethod().equals("Cash")) {
            Glide.with(imageViewPay).load(R.drawable.more_cash).into(imageViewPay);
            cardPayTv.setVisibility(View.GONE);
            textPaymentTv.setText("PAY BY CASH");
        } else {
            cardPayTv.setVisibility(View.VISIBLE);
            Glide.with(this).load(R.drawable.kess_logo_new).into(imageViewPay);
            textPaymentTv.setText("PAY BY KESS");
        }
    }

    private void initAction() {
        clickPhoto1.setOnClickListener(view -> startActivityToCamera(CODE_1));
        clickPhoto2.setOnClickListener(view -> startActivityToCamera(CODE_2));
        clickPhoto3.setOnClickListener(view -> startActivityToCamera(CODE_3));

        imageScan.setOnClickListener(view -> startActivityForResult(new Intent(ParkingDetailActivity.this, ScanParkActivity.class), CODE_QR));

        btnSeeAll.setOnClickListener(view -> {
            Intent intent = new Intent(ParkingDetailActivity.this, ListAllCommentParkingActivity.class);
            intent.putExtra("parking", myParking);
            startActivity(intent);
        });

        notificationClick.setOnClickListener(view -> {
            Intent intent = new Intent(ParkingDetailActivity.this, ListAllCommentParkingActivity.class);
            intent.putExtra("parking", myParking);
            startActivity(intent);
        });

        createsServiceBtn.setOnClickListener(view -> {
            if (userType.equals("employee")||userType.equals("developer") || userType.equals("admin")) {
                progressBar.setVisibility(View.VISIBLE);
                if (typePay.equals("PAY BY CASH")) {
                    acceptPaymentForEmployee();
                }
                if (plateNumberEdt.getText().toString().isEmpty() && myParking.getCarPlateNo() == null) {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(ParkingDetailActivity.this, getString(R.string.plat_no_file_is_re), Toast.LENGTH_LONG).show();
                } else if (colorName.isEmpty() && myParking.getCarPlateNo() == null) {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(ParkingDetailActivity.this, getString(R.string.color_file_is_re), Toast.LENGTH_LONG).show();
                }else if(descriptionEdt.getText().toString().isEmpty()){
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(ParkingDetailActivity.this, getString(R.string.comment_file_is_re), Toast.LENGTH_LONG).show();
                }
                else if (listImage.size() == 0) {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(ParkingDetailActivity.this, getResources().getString(R.string.image_file_is_require), Toast.LENGTH_LONG).show();
                } else {
                    updateServiceParking();
                }

            } else {
                Intent intent = new Intent(ParkingDetailActivity.this, ScanParkActivity.class);
                startActivityForResult(intent, CODE_PAY);
            }
        });

        btnAccept.setOnClickListener(view -> {
            Intent intent = new Intent(ParkingDetailActivity.this,PaidDialogActivity.class);
            intent.putExtra("parking_fee", myParking.getParkingFee() + "");
            intent.putExtra("parking_id", myParking.getId());
            intent.putExtra("user_id",userId);
            intent.putExtra("type","no_printer");
            startActivityForResult(intent,CODE_PAID);
        });

        btnBack.setOnClickListener(view -> onBackPressed());
    }

    @Override
    public void onBackPressed() {
//        if(myParking.getPaymentStatus().equals("paid") || myParking.getPaymentStatus().equals("Paid")){
//            finish();
//        }else {
//            showDialog(ParkingDetailActivity.this,"Are you sure to back ?","Cancel");
//        }
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && data != null) {
            if (requestCode == CODE_1) {
                getDataFromCamera(data,"1",imageView1);
                camera1.setVisibility(View.GONE);
            } else if (requestCode == CODE_2) {
                getDataFromCamera(data,"1",imageView2);
                camera2.setVisibility(View.GONE);
            } else if (requestCode == CODE_3) {
                getDataFromCamera(data,"1",imageView3);
                camera3.setVisibility(View.GONE);

            } else if (requestCode == CODE_QR) {
                if (data.hasExtra("result_scan"))
                    qrCodeText = data.getStringExtra("result_scan");
                linearAddColor.setVisibility(View.VISIBLE);
                imageScan.setVisibility(View.GONE);
            } else if (requestCode == CODE_PAY) {
                progressBar.setVisibility(View.GONE);
                if (data.hasExtra("result_scan")) {
                    qrCodeP = data.getStringExtra("result_scan");
                }
                parkingCheckOut(qrCodeP,orderId);

            } else if (requestCode == CODE_SUCCESS_PAY) {
                Intent intent = new Intent(ParkingDetailActivity.this, ParkingCompletedActivity.class);
                intent.putExtra("parking", myParking);
                startActivity(intent);
                finish();
            }else if(requestCode == CODE_PAID){
                Intent intent = new Intent();
                setResult(RESULT_OK,intent);
                finish();
            }

            dataForUpdate();
        }
    }

    private void startActivityToCamera(int code){
        Intent intent = new Intent(ParkingDetailActivity.this, BaseCameraActivity.class);
        intent.putExtra("no_select","yes");
        startActivityForResult(intent, code);
    }

    private void getDataFromCamera(Intent data ,String code,ImageView imageView){
        if(data.hasExtra("image_path")){
            String imagePath = data.getStringExtra("image_path");
            getFileCamera(imagePath,code,imageView);
        }
        if(data.hasExtra("select_image")){
            String imagePath = data.getStringExtra("select_image");
            getFileCamera(imagePath,code,imageView);
        }

    }

    private void getFileCamera(String imagePath,String code ,ImageView imageView){
        Bitmap loadedBitmap = BaseCameraActivity.convertByteToBitmap(imagePath);
        Glide.with(imageView).load(loadedBitmap).into(imageView);
        String  fileImage = convert(loadedBitmap);
        if (fileImage != null) {
            switch (code) {
                case "1":
                case "2":
                case "3":
                    listImage.add(fileImage);
                    break;
            }

        }
    }

    private HashMap<String, Object> dataForUpdate() {
        ArrayList<String> listAllImage = new ArrayList<>(listImage);
        String carPlateNo = plateNumberEdt.getText().toString();
        String commentText = descriptionEdt.getText().toString();
        String modelCar = modelCarEdt.getText().toString();
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("user_id", userId);
        hashMap.put("parking_list_id", myParking.getId());
        hashMap.put("scanqr_code_info", qrCodeText);
        hashMap.put("car_plate_no", carPlateNo);
        hashMap.put("comment", commentText);
        hashMap.put("images", listAllImage);
        hashMap.put("car_model", modelCar);
        hashMap.put("car_color", colorName);
        hashMap.put("parking_type", typeParking);
        return hashMap;
    }

    //getDetail
    private void getDetailParking() {
        new ParkingWs().getDetailParking(this, orderId, new ParkingWs.OnRequestDetailCallBackListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onLoadSuccess(Parking parkings) {
                myParking = parkings;
                progressBar.setVisibility(View.GONE);
                linearAllView.setVisibility(View.VISIBLE);

                if (myParking != null) {
                    ///show popUp message to Accept when have report
                    if(!messageReport.isEmpty() && !message.isEmpty()){
                        showDialog(ParkingDetailActivity.this,message,"report");
                    }

                    if(!qrCode.isEmpty() && action.equals("add_info")){
                        qrCodeText = qrCode;
                        cardAddInformation.setVisibility(View.VISIBLE);
                        imageScan.setVisibility(View.GONE);
                        linearAddColor.setVisibility(View.VISIBLE);

                    }else if (!qrCode.isEmpty() && action.equals("check_out")){
                        qrCodeText = qrCode;
                        findViewById(R.id.linear_staff).setVisibility(View.GONE);
                        cardAddInformation.setVisibility(View.GONE);
                        imageScan.setVisibility(View.GONE);
                        parkingCheckOut(qrCode,orderId);
                    }

                    //check permission
                    if (userType != null && userType.equals("employee") || Objects.equals(userType, "developer") || Objects.equals(userType, "admin")) {
                        addValueForEmployee(myParking);
                    } else {
                        addValueForOwner(myParking);
                    }

                    //show detail
                    addValueMyDetail(myParking);

                    ///show Comment
                    addValueForComment(myParking);

                    //show color and model
                    addValueColorAndModel(myParking);

                    //linear summary
                    addValueSummary(myParking);

                    // set margin bottom == 0
                    if (linearPrice.getVisibility() == View.GONE && linearAccept.getVisibility() == View.GONE){
                        Utils.setNoMarginBottomOnButton(findViewById(R.id.mainScroll), 0);
                    }
                    infoLayout.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onLoadFail(String message) {
                progressBar.setVisibility(View.GONE);
                linearAllView.setVisibility(View.GONE);
                Toast.makeText(ParkingDetailActivity.this, message, Toast.LENGTH_LONG).show();
            }
        });
    }

    @SuppressLint("SetTextI18n")
    private void addValueMyDetail(Parking myParking){
        if (myParking.getPropertyName() != null) {
            nameProjectTv.setText(String.format(Locale.US, "%s", myParking.getPropertyName()));
        } else {
            nameProjectTv.setText("___");
        }

        oderNoTv.setText(myParking.getOrderNo());
        entranceGateDateTv.setText(String.format(Locale.US, "%s", DateUtil.formatDate(myParking.getEntranceDate())));
        entranceGateTimeTv.setText(String.format(Locale.US, "%s", DateUtil.formatTimeLocal(myParking.getEntranceDate())));
        priceTv.setText(String.format(Locale.US, "%s %s", myParking.getParkingFeeCurrency(), myParking.getParkingFee()));
        durationTv.setText(Utils.covertTime(myParking.getParkingDurationInMinute(), myParking.getParkingType()));
        statusPaymentTv.setText(myParking.getPaymentStatus());

        if(myParking.getParkingNoName()!=null){
            entranceLotTv.setText(myParking.getParkingNoName());
        }else {
            relateLotNo.setVisibility(View.GONE);
        }

        if (myParking.getParkingLotTypeName()!=null){
            entranceTypeTv.setText(myParking.getParkingLotTypeName());
        }else {
            relateLotType.setVisibility(View.GONE);
        }

        if (myParking.getParkingZoneName()!=null){
            zoneTv.setText(String.format(Locale.US, "%s", myParking.getParkingZoneName()));
        }else {
            relateZone.setVisibility(View.GONE);
        }
        paymentMethodTv.setText(myParking.getPaymentMethod());

    }

    @SuppressLint("SetTextI18n")
    private void addValueForEmployee(Parking myParking){
        paymentLinear.setEnabled(false);
        priceLinear.setVisibility(View.GONE);
        dataForUpdate();

        Utils.logDebug("jeheeheheheehe", myParking.getPayByCashStatus());
        // PayByCash
        if (myParking.getPayByCashStatus().equals("no_action") && myParking.getPaymentStatus().equals("unpaid")) {
            paymentLinear.setVisibility(View.GONE);
            summaryLinear.setVisibility(View.GONE);
            linearAccept.setVisibility(View.GONE);
            linearPrice.setVisibility(View.VISIBLE);
            if(myParking.getTicketNo() != null && isMachine.equals("1") && qrCode.isEmpty() ){
                cardAddInformation.setVisibility(View.GONE);
            } else {
                cardAddInformation.setVisibility(View.VISIBLE);
            }

        } else if (myParking.getPayByCashStatus().equals("pending") && myParking.getPaymentStatus().equals("unpaid")) {
            inputEditText.setVisibility(View.GONE);
            paymentLinear.setVisibility(View.VISIBLE);
            summaryLinear.setVisibility(View.VISIBLE);
            linearPrice.setVisibility(View.GONE);
            textPaymentTv.setText("PAY BY CASH");
            cardPayTv.setVisibility(View.GONE);
            cardAddInformation.setVisibility(View.GONE);
            Glide.with(ParkingDetailActivity.this).load(R.drawable.more_cash).into(imageViewPay);
            linearAccept.setVisibility(View.VISIBLE);

        } else if (myParking.getPayByCashStatus().equals("accept")) {
            linearAccept.setVisibility(View.GONE);
            cardAddInformation.setVisibility(View.GONE);
            if (myParking.getPaymentMethod() != null) {
                paymentMethodTv.setText(myParking.getPaymentMethod());
                showPayment();
            }

        } else if (myParking.getPayByCashStatus().equals("reject")) {
            cardPayTv.setVisibility(View.GONE);
            priceLinear.setVisibility(View.GONE);
            linearAccept.setVisibility(View.GONE);
            summaryLinear.setVisibility(View.VISIBLE);

            linearPrice.setVisibility(View.GONE);
            cardAddInformation.setVisibility(View.GONE);
        }

        if (myParking.getPaymentStatus().equals("paid")) {
            cardAddInformation.setVisibility(View.GONE);
            linearPrice.setVisibility(View.GONE);
            summaryLinear.setVisibility(View.VISIBLE);
            paymentLinear.setEnabled(false);
            paymentLinear.setVisibility(View.VISIBLE);
            if (myParking.getPaymentMethod() != null) {
                showPayment();
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private void addValueForOwner(Parking myParking){
        notificationClick.setVisibility(View.VISIBLE);
        priceLinear.setVisibility(View.VISIBLE);
        createsServiceBtn.setText(R.string.leaves);
        pricePerTv.setText(getResources().getString(R.string.price) + " : " + myParking.getParkingFeeCurrency() + myParking.getParkingFee() + " / " + Utils.covertTime(myParking.getParkingDurationInMinute(), myParking.getParkingType()));
        cardAddInformation.setVisibility(View.GONE);

        if(action.equalsIgnoreCase("go_detail")){
            createsServiceBtn.setOnClickListener(view -> {
                SelectPaymentMethodBottomSheetFragment bottom = new SelectPaymentMethodBottomSheetFragment().newInstance("action", state);
                bottom.show(getSupportFragmentManager(), "SelectPaymentMethodBottomSheetFragment");
            });
        }

    }

    @SuppressLint("SetTextI18n")
    private void addValueForComment(Parking myParking){
        //Comment
        if (!myParking.getAllDocuments().isEmpty()) {
            int i = 1;
            for (AllComment comment : myParking.getAllDocuments()) {
                if (i < 2) {
                    if (!comment.getComment().isEmpty() || comment.getAllImages().size() != 0) {
                        listComment.add(comment);
                        commentLinear.setVisibility(View.VISIBLE);
                    }
                }
                i++;
            }

            if (myParking.getAllDocuments().size() >= 1) {
                btnSeeAll.setVisibility(View.VISIBLE);
            } else {
                btnSeeAll.setVisibility(View.GONE);
            }

            //add Comment
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ParkingDetailActivity.this);
            recyclerView.setLayoutManager(linearLayoutManager);
            ListAllCommentAdapter adapter = new ListAllCommentAdapter(listComment);
            recyclerView.setAdapter(adapter);
        } else {
            commentLinear.setVisibility(View.GONE);
        }

        ///Show Comment
        if (myParking.getCarPlateNo() != null && !myParking.getCarPlateNo().isEmpty()) {
            platNumberTv.setText(String.format(Locale.US, "%s", myParking.getCarPlateNo()));
            inputEditText.setVisibility(View.GONE);
            plateNumberEdt.setVisibility(View.GONE);
            linearColor.setVisibility(View.GONE);
            linearPlatNo.setVisibility(View.VISIBLE);

        } else {
            linearPlatNo.setVisibility(View.GONE);
            platNumberTv.setText("- - -");
        }
    }

    private void addValueColorAndModel(Parking myParking){
        if (myParking.getCarColor() != null) {
            carColorTv.setText(myParking.getCarColor());
            linearColorTx.setVisibility(View.VISIBLE);
        } else {
            linearColorTx.setVisibility(View.GONE);
        }

        if (myParking.getParkingType() != null) {
            typeParkingTv.setText(myParking.getParkingType());
        } else {
            linearTypeParking.setVisibility(View.GONE);
        }

        if (myParking.getCarModel() != null && !myParking.getCarModel().isEmpty()) {
            linearModelCar.setVisibility(View.VISIBLE);
            modelCarTv.setText(myParking.getCarModel());
        } else {
            linearModelCar.setVisibility(View.GONE);
        }

        if(myParking.getCarImagePlate()!=null){
            cardViewPlateCar.setVisibility(View.VISIBLE);
            Glide.with(imageViewPlateCar).load(myParking.getCarImagePlate()).into(imageViewPlateCar);
        }else {
            cardViewPlateCar.setVisibility(View.GONE);
        }

    }

    private void  addValueSummary(Parking myParking){
        if (myParking.getPaymentStatus().equalsIgnoreCase("unpaid")) {
            linearPrice.setVisibility(View.VISIBLE);
            summaryLinear.setVisibility(View.GONE);
            paymentLinear.setEnabled(true);
        } else {
            linearPrice.setVisibility(View.GONE);
            summaryLinear.setVisibility(View.VISIBLE);
            paymentLinear.setEnabled(false);
            cardAddInformation.setVisibility(View.GONE);
        }

        if (myParking.getExitDate() != null) {
            dateLeaveTv.setText(DateUtil.formatDate(myParking.getExitDate().toString()));
            timeLeaveTv.setText(DateUtil.formatTimeLocal(myParking.getExitDate().toString()));
        }


        if (myParking.getEmployeeReceiverUserName() != null) {
            receiverTv.setText(myParking.getEmployeeReceiverUserName().toString());
        } else {
            linearReceiver.setVisibility(View.GONE);
        }
        receiveAmountTv.setText(String.format(Locale.US,"%s %s", myParking.getParkingFeeCurrency(), myParking.getReceivedAmount()));
        changedAmountTv.setText(String.format("%s %s", myParking.getParkingFeeCurrency(), myParking.getChangedAmount()));

        parkingTypeTv.setText(myParking.getParkingType());

        if(myParking.getTicketNo()!=null){
            ticketNoTv.setText(myParking.getTicketNo());
        }else {
            linearTicketNo.setVisibility(View.GONE);
        }
    }

    //Color
    @SuppressLint("NotifyDataSetChanged")
    private final ColorParkingAdapter.ItemClickColor clickColor = (name, po, stat) -> {
        statClickColor = stat;
        MockUpData.setStatColor(stat);
        colorName = name;
        adapter.notifyDataSetChanged();
    };

    //typeParking
    private final ServiceTermAdapter.ItemClickOnService itemClickOnService = (listString, type, position, stat) -> {
        if (listString.equalsIgnoreCase("Hourly")) {
            typeParking = "hourly";
        } else {
            typeParking = "daily";
        }

    };

    //update
    private void updateServiceParking() {
        new ParkingWs().updateServiceParking(this, dataForUpdate(), new ParkingWs.OnRequestCreateCallBackListener() {
            @Override
            public void onLoadSuccess(String listParking) {
                progressBar.setVisibility(View.GONE);
                Utils.customToastMsgError(ParkingDetailActivity.this, listParking, true);
                Intent intent = new Intent();
                intent.putExtra("refresh", true);
                setResult(RESULT_OK, intent);
                finish();
            }

            @Override
            public void onLoadSuccessListener(String action, String parkingId) {}

            @Override
            public void onLoadFail(String message) {
                progressBar.setVisibility(View.GONE);
                Utils.customToastMsgError(ParkingDetailActivity.this, message, false);
            }
        });
    }

    //CheckOut
    private void parkingCheckOut(String qrCodeP,String orderId){
        HashMap<String,Object> hashMap = new HashMap<>();
        hashMap.put("parking_list_id",orderId);
        hashMap.put("scanqr_code_info",qrCodeP);
        hashMap.put("user_id",userId);
        new ParkingWs().checkOutServiceParking(this, hashMap, new ParkingWs.OnRequestCreateCallBackListener() {
            @Override
            public void onLoadSuccess(String listParking) {
                SelectPaymentMethodBottomSheetFragment bottom = new SelectPaymentMethodBottomSheetFragment().newInstance("action", state);
                bottom.show(getSupportFragmentManager(), "SelectPaymentMethodBottomSheetFragment");
            }

            @Override
            public void onLoadSuccessListener(String action, String parkingId) {

            }

            @Override
            public void onLoadFail(String message) {
                Toast.makeText(ParkingDetailActivity.this, message, Toast.LENGTH_LONG).show();
            }
        });
    }

    //Payment method Kess
    private void getLinkPayment() {
        new PaymentParkingWs().getPayment(this, myParking.getId(), qrCodeP, new PaymentParkingWs.OnRequestPaymentCallBack() {
            @Override
            public void onLoadSuccess(String linkKESSPay) {
                progressBar.setVisibility(View.GONE);
                Intent intent = new Intent(ParkingDetailActivity.this, WebPayActivity.class);
                intent.putExtra("linkUrl", linkKESSPay);
                intent.putExtra("toolBarTitle", "Kess Pay");
                startActivityForResult(intent, CODE_SUCCESS_PAY);
            }

            @Override
            public void onLoadFail(String message) {
                progressBar.setVisibility(View.GONE);
                Utils.customToastMsgError(ParkingDetailActivity.this, message, false);
            }
        });
    }

    //Pay by Cash
    private void payByCash() {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("parking_list_id", myParking.getId());
        hashMap.put("scanqr_code_info", qrCodeP);
        new PaymentParkingWs().getPaymentPayByCash(this, hashMap, onRequestPaymentCallBack);
    }

    //accept Payment
    private void acceptPaymentForEmployee() {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("parking_list_id", myParking.getId());
        hashMap.put("status", "accept");
        hashMap.put("user_id", userId);
        new PaymentParkingWs().acceptPaymentPayByCash(this, hashMap, onRequestPaymentCallBack);
    }


    private final PaymentParkingWs.OnRequestPaymentCallBack onRequestPaymentCallBack = new PaymentParkingWs.OnRequestPaymentCallBack() {
        @Override
        public void onLoadSuccess(String message) {
            progressBar.setVisibility(View.GONE);
            Intent intent = new Intent();
            setResult(RESULT_OK, intent);
            finish();
            Utils.customToastMsgError(ParkingDetailActivity.this, message, true);
        }

        @Override
        public void onLoadFail(String message) {
            progressBar.setVisibility(View.GONE);
            Utils.customToastMsgError(ParkingDetailActivity.this, message, false);
        }
    };

    //selectPayment
    @Override
    public void onClickSubmit(@Nullable String txtChoose, int image, int stat) {
        imagePay = image;
        typePay = txtChoose;
        state = stat;
        MockUpData.setStatPayment(state);
        MockUpData.setImagePay(imagePay);
        MockUpData.setTextPay(typePay);
        if (typePay.equals("PAY BY KESS")) {
            cardPayTv.setVisibility(View.VISIBLE);
        } else {
            cardPayTv.setVisibility(View.GONE);
        }
        if (typePay.equals("PAY BY KESS")) {
            progressBar.setVisibility(View.VISIBLE);
            getLinkPayment();
        } else {
            progressBar.setVisibility(View.VISIBLE);
            payByCash();
        }

        textPaymentTv.setText(typePay);
        Glide.with(this).load(image).into(imageViewPay);

    }

    //Accept notification when report assign from backend
    @SuppressLint("SetTextI18n")
    public void showDialog(Activity activity, String msg,String type) {
        dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.custom_dialoge_messge);
        TextView title = dialog.findViewById(R.id.sub);
        title.setText(type);
        TextView text = dialog.findViewById(R.id.text_name);
        text.setText(msg);
        TextView  btnCancel = dialog.findViewById(R.id.btn_closed);
        TextView  btnOK = dialog.findViewById(R.id.btn_accept);
        btnOK.setOnClickListener(view -> {
            acceptNotification(type);
            dialog.dismiss();
        });

        btnCancel.setOnClickListener(view -> dialog.dismiss());
        dialog.show();
    }

    private void acceptNotification(String type){
        HashMap<String,Object> hashMap= new HashMap<>();
        if (type.equals("report")){
            hashMap.put("parking_history_id", myParking.getId());
        }else {
            hashMap.put("parking_list_id", myParking.getId());
        }

       new ParkingWs().acceptNotification(this, hashMap,type, new ParkingWs.OnRequestCreateCallBackListener() {
           @Override
           public void onLoadSuccess(String listParking) {
               Intent intent = new Intent();
               setResult(RESULT_OK,intent);
               finish();
           }

           @Override
           public void onLoadSuccessListener(String action, String parkingId) {

           }

           @Override
           public void onLoadFail(String message) {
               progressBar.setVisibility(View.GONE);
               Toast.makeText(ParkingDetailActivity.this, message, Toast.LENGTH_LONG).show();
           }
       });
    }

}