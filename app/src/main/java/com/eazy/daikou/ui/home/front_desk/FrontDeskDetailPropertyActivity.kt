package com.eazy.daikou.ui.home.front_desk

import android.content.res.ColorStateList
import android.graphics.Bitmap
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.request_data.request.front_desk_ws.FrontDeskWS
import com.eazy.daikou.helper.AbsoluteFitLayoutManager
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.front_desk.DetailFrontDeskModel
import com.eazy.daikou.model.front_desk.Pickers
import com.eazy.daikou.model.front_desk.SearchFrontDeskModel
import com.eazy.daikou.ui.home.complaint_solution.complaint.adapter.AdapterDisplayListImage
import com.eazy.daikou.ui.home.front_desk.adapter.AdapterListAssignFrontDesk
import com.eazy.daikou.ui.home.front_desk.fragment.PickerItemFragment
import com.eazy.daikou.ui.QRCodeAlertDialog

class FrontDeskDetailPropertyActivity : BaseActivity() {

    private lateinit var iconAssignNextDown: LinearLayout
    private lateinit var iconDownNext: ImageView
    private lateinit var progressBarView: ProgressBar
    private lateinit var recyclerViewImage: RecyclerView
    private lateinit var recyclerAssignTo: RecyclerView
    private lateinit var idTv: TextView
    private lateinit var createDateTv: TextView
    private lateinit var statusTv: TextView
    private lateinit var nameDeliveryTv: TextView
    private lateinit var phoneDeliveryTv: TextView
    private lateinit var ownerNameTv: TextView
    private lateinit var phoneOwnerTv: TextView
    private lateinit var unitNoTv: TextView
    private lateinit var textRemarkTv: TextView
    private lateinit var iconQrCode: ImageView
    private lateinit var noDataTv: TextView
    private lateinit var noRemarkTv: TextView
    private lateinit var iconAddMore: ImageView
    private lateinit var adapterAssign: AdapterListAssignFrontDesk
    private var listImage = ArrayList<String>()
    private var listPicker = ArrayList<Pickers>()
    private var detailFrontModel: DetailFrontDeskModel = DetailFrontDeskModel()
    private var idList = ""
    private lateinit var linearViewDetail: LinearLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_front_desk_detail_property)

        initView()

        initData()

        initAction()
    }

    private fun initView() {
        findViewById<ImageView>(R.id.iconBack).setOnClickListener { finish() }
        linearViewDetail = findViewById(R.id.linearViewDetail)
        progressBarView = findViewById(R.id.progressItem)
        idTv = findViewById(R.id.idTv)
        createDateTv = findViewById(R.id.createDateTv)
        statusTv = findViewById(R.id.statusTv)
        nameDeliveryTv = findViewById(R.id.nameDeliveryTv)
        phoneDeliveryTv = findViewById(R.id.phoneDeliveryTv)
        ownerNameTv = findViewById(R.id.ownerNameTv)
        phoneOwnerTv = findViewById(R.id.phoneOwnerTv)
        unitNoTv = findViewById(R.id.unitNoTv)
        textRemarkTv = findViewById(R.id.textRemarkTv)
        iconQrCode = findViewById(R.id.iconQrCode)
        recyclerViewImage = findViewById(R.id.recyclerViewImage)
        recyclerAssignTo = findViewById(R.id.recyclerAssignTo)
        iconAddMore = findViewById(R.id.addressMap)
        iconAddMore.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_home_add_more))
        findViewById<LinearLayout>(R.id.layoutMap).visibility = View.VISIBLE
        noDataTv = findViewById(R.id.noDataTv)
        noRemarkTv = findViewById(R.id.noRemarkTv)
        iconDownNext = findViewById(R.id.iconDownNext)
        iconAssignNextDown = findViewById(R.id.iconAssignNextDown)
    }

    private fun initData() {
        if (intent != null && intent.hasExtra("id")) {
            idList = intent.getStringExtra("id") as String
        }
    }

    private fun initAction() {
        findViewById<TextView>(R.id.titleToolbar).text = getString(R.string.front_desk_detail)
        iconAddMore.visibility = View.VISIBLE
        requestServiceDetailAPI()

        iconAddMore.setOnClickListener(CustomSetOnClickViewListener {
            clickBottomAddMore()
        })
        iconAssignNextDown.setOnClickListener(CustomSetOnClickViewListener {
            loadView(recyclerAssignTo)
        })
    }

    private fun loadView(recyclerAssignTo: RecyclerView) {
        if (recyclerAssignTo.visibility == View.GONE) {
            recyclerAssignTo.visibility = View.VISIBLE
            iconDownNext.setImageResource(R.drawable.ic_drop_down)
        } else {
            recyclerAssignTo.visibility = View.GONE
            iconDownNext.setImageResource(R.drawable.ic_arrow_next)
        }
    }

    private fun clickBottomAddMore() {
        val bottomSheet = PickerItemFragment().newInstance()
        bottomSheet.initDataListener(callBackItemClickFormBottomSheet)
        bottomSheet.show(supportFragmentManager, "")
    }

    private val callBackItemClickFormBottomSheet: PickerItemFragment.CallBackItemAfterClick =
        object : PickerItemFragment.CallBackItemAfterClick {
            override fun clickItemListener(searchFrontDesk: SearchFrontDeskModel) {
                val userID = searchFrontDesk.id.toString()
                requestAssignPickerAPI(userID)
            }
        }

    private fun requestServiceDetailAPI() {
        progressBarView.visibility = View.VISIBLE
        FrontDeskWS().getDetailFrontDeskWS(
            this@FrontDeskDetailPropertyActivity,
            idList,
            callBackDetail
        )
    }

    private fun requestAssignPickerAPI(userId: String) {
        val hashMap = HashMap<String, Any?>()
        hashMap["frontdesk_id"] = detailFrontModel.id
        hashMap["user_id"] = userId
        progressBarView.visibility = View.VISIBLE
        FrontDeskWS().getAssignPickerFrontDeskWS(
            this@FrontDeskDetailPropertyActivity,
            hashMap,
            callBackPicker
        )
    }

    private val callBackDetail = object : FrontDeskWS.CallBackDetailFrontDesk {
        override fun onSuccessful(detail: DetailFrontDeskModel) {
            progressBarView.visibility = View.GONE
            detailFrontModel = detail
            linearViewDetail.visibility = View.VISIBLE
            initSetData(detail)
            listImage.addAll(detail.images)
            listPicker.addAll(detail.pickers)
            if (listImage.size <= 0) {
                recyclerViewImage.visibility = View.GONE
                noDataTv.visibility = View.VISIBLE
            }
            initRecyclerViewImage()
            initRecyclerViewPicker()
        }

        override fun onFailed(error: String) {
            progressBarView.visibility = View.GONE
            Utils.customToastMsgError(this@FrontDeskDetailPropertyActivity, error, false)
        }
    }

    private val callBackPicker = object : FrontDeskWS.CallBackFrontDeskCreateListener {
        override fun onSuccessFul(message: String) {
            progressBarView.visibility = View.GONE
            Utils.customToastMsgError(this@FrontDeskDetailPropertyActivity, message, true)
        }

        override fun onFailed(mess: String) {
            progressBarView.visibility = View.GONE
            Utils.customToastMsgError(this@FrontDeskDetailPropertyActivity, mess, false)
        }

    }

    private fun initRecyclerViewPicker() {
        val linearManager = LinearLayoutManager(this)
        recyclerAssignTo.layoutManager = linearManager
        adapterAssign = AdapterListAssignFrontDesk(this, listPicker)
        recyclerAssignTo.adapter = adapterAssign
    }

    private fun initRecyclerViewImage() {
        recyclerViewImage.layoutManager =
            AbsoluteFitLayoutManager(this, 1, RecyclerView.HORIZONTAL, false, 2)
        recyclerViewImage.adapter = AdapterDisplayListImage(this, listImage)
    }

    private fun initSetData(detailModel: DetailFrontDeskModel) {
        val qrCodeImageBitmap: Bitmap = Utils.getQRCodeImage512(detailModel.qr_code_data)

        if (detailModel.qr_code_data != null) {
            iconQrCode.setImageBitmap(qrCodeImageBitmap)

            iconQrCode.setOnClickListener(CustomSetOnClickViewListener {
                val qrCodeAlertDialog = QRCodeAlertDialog(
                    this@FrontDeskDetailPropertyActivity,
                    qrCodeImageBitmap
                )
                qrCodeAlertDialog.show()
            })
        }
        idTv.text = if (detailModel.no != null) detailModel.no else "- - -"
        createDateTv.text = if (detailModel.created_at != null) Utils.formatDateFromString(
            "yyyy-MM-dd",
            "dd-MMM-yyyy",
            detailModel.created_at
        ) else "- - -"
        nameDeliveryTv.text =
            if (detailModel.delivery_name != null) detailModel.delivery_name else "- - -"
        phoneDeliveryTv.text =
            if (detailModel.delivery_phone != null) detailModel.delivery_phone else "- - -"
        ownerNameTv.text =
            if (detailModel.user!!.name != null) detailModel.user!!.name else " - - -"
        phoneOwnerTv.text = if (detailModel.full_phone != null) detailModel.full_phone else "- - -"
        unitNoTv.text = "- - - "
        when (detailModel.status) {
            "new" -> {
                statusTv.text = Utils.getText(this, R.string.new_)
                statusTv.backgroundTintList =
                    ColorStateList.valueOf(ContextCompat.getColor(this, R.color.appBarColorOld))
            }
            "picked_up" -> {
                statusTv.text = Utils.getText(this, R.string.picked_up)
                statusTv.backgroundTintList =
                    ColorStateList.valueOf(ContextCompat.getColor(this, R.color.green))
            }
            else -> {
                statusTv.text = "- - -"
                statusTv.backgroundTintList =
                    ColorStateList.valueOf(ContextCompat.getColor(this, R.color.appBarColorOld))
            }
        }

        if (detailModel.description != null) {
            textRemarkTv.text = detailModel.description
        } else {
            noRemarkTv.visibility = View.VISIBLE
        }
    }
}