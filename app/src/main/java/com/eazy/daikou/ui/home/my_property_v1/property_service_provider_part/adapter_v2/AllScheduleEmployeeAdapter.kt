package com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.adapter_v2

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.model.my_property.cleaning.AllScheduleEmployee

class AllScheduleEmployeeAdapter(private val context: Context, private val list : ArrayList<AllScheduleEmployee>, private val callBackItemService: CallBackItemServiceProviderProperty): RecyclerView.Adapter<AllScheduleEmployeeAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.all_schedule_employee_adater_layout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val listService: AllScheduleEmployee = list[position]
        if (listService != null){
            holder.userNameTv.text = if (listService.user_full_name != null) listService.user_full_name else "- - -"
            holder.phoneTv.text = if (listService.user_phone != null) listService.user_phone else "- - -"
            holder.emailTv.text = if (listService.user_email != null) listService.user_email else "- - -"

            if (listService.user_profile_image != null){
                Glide.with(holder.imageView).load(listService.user_profile_image).into(holder.imageView)
            } else {
                Glide.with(holder.imageView).load(R.drawable.no_image).into(holder.imageView)
            }
            holder.mainLayout.setOnClickListener {  callBackItemService.callBackItem(listService)}
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view){
        var phoneTv: TextView = view.findViewById(R.id.phone_number_service)
        var emailTv: TextView = view.findViewById(R.id.emailTv)
        var userNameTv: TextView = view.findViewById(R.id.userNameTv)
        var imageView: ImageView = view.findViewById(R.id.imageView)
        var mainLayout : RelativeLayout = view.findViewById(R.id.mainLayout)
    }

    interface CallBackItemServiceProviderProperty{
        fun callBackItem(allScheduleEmployee: AllScheduleEmployee)
    }
}