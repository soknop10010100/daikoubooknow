package com.eazy.daikou.ui.home.service_provider.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.eazy.daikou.R;
import com.eazy.daikou.model.my_property.service_provider.ServiceProvider;

import java.util.ArrayList;
import java.util.List;

public class ItemServicesAdapter extends RecyclerView.Adapter<ItemServicesAdapter.ViewHolder> {

   private final Context context;
   private List<ServiceProvider> serviceProviderList  = new ArrayList<>();
   private final clickBack clickBack;

    public ItemServicesAdapter(Context context, List<ServiceProvider> serviceProviderList,clickBack clickBack) {
        this.context = context;
        this.serviceProviderList = serviceProviderList;
        this.clickBack = clickBack;
    }

    @NonNull
    @Override
    public ItemServicesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.services_providers_layout,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemServicesAdapter.ViewHolder holder, int position) {
        ServiceProvider serviceProvider = serviceProviderList.get(position);
        if(serviceProvider!=null){
            holder.phoneNumber.setText(serviceProvider.getCompanyContact());
            holder.organization.setText(serviceProvider.getContractorName());
            if(serviceProvider.getCompanyLogo()!=null){
                Glide.with(holder.itemView.getContext()).load(serviceProvider.getCompanyLogo()).into(holder.imageView);
            }else {
                Glide.with(holder.itemView.getContext()).load(R.drawable.ic_person_profile_false_24).into(holder.imageView);
            }

            holder.itemView.setOnClickListener(view -> clickBack.back(serviceProvider));
        }

    }

    @Override
    public int getItemCount() {
        return serviceProviderList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView organization,phoneNumber;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.image);
            organization = itemView.findViewById(R.id.servicename);
            phoneNumber = itemView.findViewById(R.id.custmer_phone_number);
        }
    }
    public interface clickBack{
        void back(ServiceProvider serviceProvider);
    }
}
