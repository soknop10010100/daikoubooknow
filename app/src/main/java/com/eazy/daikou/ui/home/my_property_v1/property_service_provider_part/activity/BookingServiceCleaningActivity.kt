package com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.request_data.request.service_property_ws.cleaning_service.ServiceCleaningWs
import com.eazy.daikou.helper.AbsoluteFitLayoutManager
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.my_property.service_property_employee.AllService
import com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.adapter.ServiceTermAdapter

class BookingServiceCleaningActivity : BaseActivity() {

    private lateinit var serviceFeeTv : TextView
    private lateinit var serviceTaxTv : TextView
    private lateinit var timeAvailableTv : TextView
    private lateinit var dayWorkTv : TextView
    private lateinit var allService : AllService
    private lateinit var serviceNameTv : TextView
    private lateinit var serviceTypeTv : TextView
    private lateinit var serviceTermTv : TextView
    private lateinit var btnBack : TextView
    private lateinit var totalPriceTv : TextView
    private lateinit var priceTv : TextView
    private lateinit var recyclerView : RecyclerView
    private lateinit var serviceTermAdapter : ServiceTermAdapter
    private lateinit var btnBooking : TextView
    private lateinit var loading : ProgressBar
    private lateinit var dateStartTv : TextView
    private lateinit var dateEndTv : TextView

    private  var listDay : ArrayList<String> = ArrayList()
    private var listTime : ArrayList<String> = ArrayList()

    private var timeStart = 0
    private var timeEnd = 0
    private var dateStart = ""
    private var dateEnd = ""
    private var start = ""
    private var end = ""
    private var timeTotal = ""
    private var totalPrice = ""
    private var currency =""
    private var dayWork = ""
    private var userId = ""
    private var unitId = ""
    private var totalWeek = 0
    private var periodTime = 0
    private lateinit var durationTv : TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_price_to_ceate_cleaning)

        initView()

        initData()

        initAction()

    }

    private fun initView() {
        serviceNameTv = findViewById(R.id.name_text_service)
        serviceTermTv = findViewById(R.id.service_term_text)
        serviceTypeTv = findViewById(R.id.service_type_text)
        serviceFeeTv = findViewById(R.id.service_fee_tex)
        serviceTaxTv = findViewById(R.id.tax_text)
        dayWorkTv = findViewById(R.id.day_work_text)
        timeAvailableTv = findViewById(R.id.time_available_text)
        btnBack = findViewById(R.id.btn_back)
        recyclerView = findViewById(R.id.list_time)
        priceTv = findViewById(R.id.price_tex)
        totalPriceTv = findViewById(R.id.total_text)
        dateStartTv = findViewById(R.id.date_start)
        dateEndTv = findViewById(R.id.date_end)
        btnBooking = findViewById(R.id.btn_booking)
        loading = findViewById(R.id.loading_more)
        loading.visibility = View.GONE
        durationTv = findViewById(R.id.durationTv)
    }

    @SuppressLint("SetTextI18n")
    private fun initData() {
        listDay = ArrayList()
        if (intent.hasExtra("service")) {
            allService = intent.getSerializableExtra("service") as AllService
        }
        if(intent.hasExtra("start_date")){
            dateStart = intent.getStringExtra("start_date") as String
        }
        if(intent.hasExtra("end_date")){
            dateEnd = intent.getStringExtra("end_date") as String
        }
        if(intent.hasExtra("day_work")){
            dayWork = intent.getStringExtra("day_work") as String
        }
        if(intent.hasExtra("price")){
            totalPrice = intent.getStringExtra("price") as String
        }
        if(intent.hasExtra("currency")){
            currency = intent.getStringExtra("currency") as String
        }
        if(intent.hasExtra("user_id")){
            userId = intent.getStringExtra("user_id") as String
        }
        if(intent.hasExtra("unitId")){
            unitId = intent.getStringExtra("unitId") as String
        }

        if(intent.hasExtra("list_day")){
            listDay = intent.getSerializableExtra("list_day") as ArrayList<String>

        }
        if(intent.hasExtra("total_week")){
            totalWeek = intent.getIntExtra("total_week",-1)
        }

        serviceNameTv.text = allService.cleaningServiceName
        serviceTypeTv.text = if (allService.cleaningServiceType != null) Utils.convertFirstCapital(allService.cleaningServiceType) else "- - -"
        serviceTermTv.text = if (allService.serviceTerm != null) Utils.convertFirstCapital(allService.serviceTerm) else "- - -"
        serviceFeeTv.text = allService.serviceFee
        serviceTaxTv.text = allService.serviceTax
        durationTv.text = allService.cleaningPeriod.toString() + " " + resources.getString(R.string.min)
        dateEndTv.text = dateEnd
        dateStartTv.text = dateStart
        totalPriceTv.text = "$currency $totalPrice"
        priceTv.text = "$currency $totalPrice"

        if(dayWork.isNotEmpty()){
            dayWorkTv.text = dayWork
        }else{
            for (day in listDay){
                dayWorkTv.append("$day ")
            }
        }

        start = Utils.formatDateTime(allService.startTime, "hh:mm:ss", "hh:mm a")
        end = Utils.formatDateTime(allService.endTime, "hh:mm:ss", "hh:mm a")

        timeAvailableTv.text = getString(R.string.available_time) + "( "+ start + " - " +end + " )"

        periodTime = allService.cleaningPeriod

        timeStart = allService.startTimeInMinute
        timeEnd = allService.endTimeInMinute - periodTime

        listTime.add(Utils.covertTimes(timeStart))
        timeTotal = Utils.covertTimes(timeStart)
        var i = 30
        while (i < timeEnd) {
            if (timeStart + 30 <= timeEnd) {
                timeStart += 30
                i += 30
                listTime.add(Utils.covertTimes(timeStart))
            } else {
                break
            }
        }
        addTapTime()

    }

    private fun initAction() {
        btnBack.setOnClickListener { finish() }
        btnBooking.setOnClickListener {
            loading.visibility = View.VISIBLE
            createServiceCleaning()
        }
    }

    private fun addTapTime(){
        val layout = AbsoluteFitLayoutManager(
            this,
            4,
            RecyclerView.HORIZONTAL,
            false,
            4
        )
        recyclerView.layoutManager = layout
        serviceTermAdapter = ServiceTermAdapter(listTime,"serviceTime",0,this,  itemClickTap)
        recyclerView.adapter = serviceTermAdapter
    }

    private val itemClickTap = ServiceTermAdapter.ItemClickOnService { listString, _, _, _ ->
        timeTotal = listString
    }

    private  fun initDataToCreate() : HashMap<String, Any> {
        val hashMap = HashMap<String, Any>()
        hashMap["property_id"] = allService.propertyId
        hashMap["unit_id"] = unitId
        hashMap["user_id"] = userId
        hashMap["start_date"] = dateStart
        hashMap["end_date"] = dateEnd
        hashMap["start_time"] = timeTotal
        hashMap["service_type"] = allService.cleaningServiceType
        hashMap["service_term"] = allService.serviceTerm
        hashMap["service_tax"] = allService.serviceTax.replace("%","")
        hashMap["service_fee"] = allService.serviceFee.replace("$","")
        hashMap["total_price"] = totalPrice
        hashMap["service_id"] = allService.id
        if(allService.billItemId!=null){
            hashMap["bill_item_id"] = allService.billItemId
        }else{
            hashMap["bill_item_id"] = ""
        }
        hashMap["service_fee_id"] = allService.cleaningServiceFeeId
        hashMap["total_week"] = totalWeek
        if(dayWork.isNotEmpty()){
            hashMap["mon"] = dayWork == "Mon"
            hashMap["tue"] = dayWork == "Tue"
            hashMap["wed"] = dayWork == "Wed"
            hashMap["thu"] = dayWork == "Thu"
            hashMap["fri"] = dayWork == "Fri"
            hashMap["sat"] = dayWork == "Sat"
            hashMap["sun"] = dayWork == "Sun"
        } else {
            hashMap["mon"] = allService.isMon
            hashMap["tue"] = allService.isTue
            hashMap["wed"] = allService.isWed
            hashMap["thu"] = allService.isThu
            hashMap["fri"] = allService.isFri
            hashMap["sat"] = allService.isSat
            hashMap["sun"] = allService.isSun
        }
        hashMap["created_from"] = "mobile"

        return hashMap
    }

    //create ServiceCleaning
    private  fun createServiceCleaning(){
        loading.visibility = View.VISIBLE
        btnBooking.isEnabled = false
        ServiceCleaningWs().createServiceCleaning(this,initDataToCreate(),serviceCleaningCreate)
    }

    private val serviceCleaningCreate = object : ServiceCleaningWs.OnLoadRequestCreateCallBack{
        override fun onLoadSuccessFull(message: String) {
            loading.visibility = View.GONE
            btnBooking.isEnabled = true
            val  intent = Intent()
            intent.putExtra("isRefresh","is")
            setResult(RESULT_OK,intent)
            finish()
            Toast.makeText(this@BookingServiceCleaningActivity,message, Toast.LENGTH_LONG).show()
        }

        override fun onLoadFail(message: String) {
            loading.visibility = View.GONE
            btnBooking.isEnabled = true
            Toast.makeText(this@BookingServiceCleaningActivity,message, Toast.LENGTH_LONG).show()
        }

    }
}