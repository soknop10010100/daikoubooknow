package com.eazy.daikou.ui.home.hrm.over_time

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.request_data.request.hr_ws.OvertimeWs
import com.eazy.daikou.request_data.request.hr_ws.OvertimeWs.OnCallBackEmployeeNameListener
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.hr.ItemOvertimeModel
import com.eazy.daikou.model.profile.User
import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog
import com.google.gson.Gson
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class CreateOverTimeHRActivity : BaseActivity() {

    private lateinit var progressBar : ProgressBar
    private lateinit var dateTv : TextView
    private lateinit var hourTv : EditText
    private lateinit var startTimeTv : TextView
    private lateinit var endTimeTv : TextView
    private lateinit var overTimeCategoryTv : TextView
    private lateinit var categoryShift : TextView
    private lateinit var approvedByTv : TextView
    private lateinit var descriptionTv : EditText
    private lateinit var btnSave : TextView
    private lateinit var imgCoverProfile: ImageView

    private  var idCategory : String = ""
    private  var idShift : String = ""
    private  var idApprovedBy : String = ""
    private var dateOvertime : String = ""
    private var startDate : String = ""
    private var endDate : String = ""
    private var userKey = ""
    private var actionType =""
    private var accountUserBusinessKey = ""
    private lateinit var lastSelectedCalendar : Calendar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_over_time_hractivity)

        initData()

        initView()

        initAction()

    }
    private fun initView(){
        dateTv = findViewById(R.id.dateTv)
        hourTv = findViewById(R.id.hourTv)
        startTimeTv = findViewById(R.id.startTimeTv)
        endTimeTv = findViewById(R.id.endTimeTv)
        progressBar = findViewById(R.id.progressItem)
        overTimeCategoryTv = findViewById(R.id.OvertimeCategoryTv)
        categoryShift = findViewById(R.id.categoryShift)
        approvedByTv = findViewById(R.id.approvedByTv)
        descriptionTv = findViewById(R.id. descriptionTv)
        btnSave =findViewById(R.id.btnSave)
        imgCoverProfile = findViewById(R.id.imgCoverProfile)
        imgCoverProfile.setImageResource(R.drawable.cv_home_overtime)

        findViewById<TextView>(R.id.btn_back).setOnClickListener{finish()}
    }

    private fun initData(){
        // Save user in local
        val user : User = Gson().fromJson(UserSessionManagement(this).userDetail, User::class.java)
        if (user.userBusinessKey != null)   userKey = user.userBusinessKey
        if (user.activeAccountBusinessKey != null)   accountUserBusinessKey = user.activeAccountBusinessKey

        lastSelectedCalendar = Calendar.getInstance()

        // Just Back Listener Enabled Button Check On List
        val intent = Intent()
        intent.putExtra("isClickedFirst", true)
        setResult(RESULT_OK, intent)
    }

    @SuppressLint("SimpleDateFormat")
    private fun initAction() {
        getSelectionDialog()

        getStartAndEndTime()

        btnSave.setOnClickListener(
            CustomSetOnClickViewListener{ onCreateListOvertime() })

        dateTv.setOnClickListener (
            CustomSetOnClickViewListener{ calendarDate() })

        progressBar.visibility = View.GONE
    }

    private fun onCreateListOvertime(){
        val hashMap : HashMap<String, Any> = HashMap()
        hashMap["user_business_key"] = userKey
        hashMap["account_business_key"] = accountUserBusinessKey
        hashMap["overtime_category_id"] = idCategory
        hashMap["shift_id"] = idShift
        hashMap["approved_by_user_business_key"] = idApprovedBy
        hashMap["hour"] = hourTv.text.toString()
        hashMap["date_of_overtime"] = dateOvertime
        hashMap["start_time"] = startDate
        hashMap["end_time"] = endDate
        hashMap["description"] = descriptionTv.text.toString()

        progressBar.visibility = View.VISIBLE
        Utils.logDebug("jjjjjjjjjjjjjjj", Utils.getStringGson(hashMap))
        OvertimeWs().getCreateListOvertimeWs(this, hashMap, onCallBackCreateOvertime )

    }

    private fun getSelectionDialog(){
        overTimeCategoryTv.setOnClickListener(CustomSetOnClickViewListener{ requestListItemFromApi(userKey, "overtime_category") })

        categoryShift.setOnClickListener(CustomSetOnClickViewListener{ requestListItemFromApi(userKey, "category_shift") })

        approvedByTv.setOnClickListener(CustomSetOnClickViewListener{ requestListItemFromApi(userKey,"approved") })
    }

    private fun calendarDate(){
        val alertLayout = LayoutInflater.from(this).inflate(R.layout.activity_calendar_view_dialog, null)
        val textTitle = alertLayout.findViewById<TextView>(R.id.txtTitle)
        val dialog = AlertDialog.Builder(this, R.style.AlertShape)
        dialog.setView(alertLayout)
        dialog.setCancelable(false)
        textTitle.text = getString(R.string.date_of_overtime)
        val alertDialog = dialog.show()

        alertLayout.findViewById<ImageView>(R.id.btnClose).setOnClickListener{alertDialog.dismiss()}
        val calendarViewAlert: CalendarView = alertLayout.findViewById(R.id.date_picker)

        // Set Select Date
        if (lastSelectedCalendar != null)     calendarViewAlert.setDate (lastSelectedCalendar.timeInMillis, true, true)

        calendarViewAlert.setOnDateChangeListener { _: CalendarView, year: Int, month: Int, dayOfMonth: Int ->
            val checkCalendar = Calendar.getInstance()
            checkCalendar[year, month] = dayOfMonth

            val calendar = Calendar.getInstance()
            calendar[year, month] = dayOfMonth

            lastSelectedCalendar = calendar
            val formattedDate = SimpleDateFormat("yyyy-MM-dd")
            dateOvertime = formattedDate.format(calendar.time)
            dateTv.text = dateOvertime

            alertDialog.dismiss()
        }
        calendarViewAlert.minDate = System.currentTimeMillis() - 1000

    }

    @SuppressLint("SimpleDateFormat")
    private fun getStartAndEndTime(){

        val formatToServer = SimpleDateFormat("kk:mm")
        val formatToDisplay = SimpleDateFormat("hh:mm a")

        startTimeTv.setOnClickListener(
            CustomSetOnClickViewListener {
            SingleDateAndTimePickerDialog.Builder(this)
                .bottomSheet()
                .curved()
                .minutesStep(5)
                .backgroundColor(getColor(R.color.appBarColor))
                .mainColor(Color.WHITE)
                .displayMinutes(true)
                .displayHours(true)
                .displayDays(false)
                .displayMonth(false)
                .displayYears(false)
                .displayDaysOfMonth(false)
                .displayAmPm(true)
                .titleTextColor(getColor(R.color.white))
                .title((Utils.getText(this, R.string.start_time)))
                .listener { date ->
                    startDate = formatToServer.format(date)
                    startTimeTv.text = formatToDisplay.format(date)
                }.display()
        })

        endTimeTv.setOnClickListener (
            CustomSetOnClickViewListener{
                SingleDateAndTimePickerDialog.Builder(this)
                    .bottomSheet()
                    .curved()
                    .minutesStep(5)
                    .backgroundColor(getColor(R.color.appBarColor))
                    .mainColor(Color.WHITE)
                    .displayMinutes(true)
                    .displayHours(true)
                    .displayDays(false)
                    .displayMonth(false)
                    .displayYears(false)
                    .displayDaysOfMonth(false)
                    .displayAmPm(true)
                    .titleTextColor(getColor(R.color.white))
                    .title((Utils.getText(this, R.string.end_time)))
                    .listener { date ->
                        endDate = formatToServer.format(date)
                        endTimeTv.text = formatToDisplay.format(date)
                    }.display()
            })
    }

    private fun startBottomSheetDialog(itemOvertimeModelList: ArrayList<ItemOvertimeModel> ){
        val bottomSheetFragment = ItemOvertimeFragment().newInstance(actionType, itemOvertimeModelList)
        bottomSheetFragment.initListener(callBackItemFromFragment)
        bottomSheetFragment.show(supportFragmentManager, "list_item_overtime")
    }

    private val callBackItemFromFragment: ItemOvertimeFragment.CallBackItemToCreate = object : ItemOvertimeFragment.CallBackItemToCreate{
        override fun clickItemCallBack(itemOvertimeModel: ItemOvertimeModel) {
            when (actionType) {
                "overtime_category" -> {
                    idCategory = itemOvertimeModel.id!!
                    overTimeCategoryTv.text = itemOvertimeModel.name
                }
                "category_shift" -> {
                    idShift = itemOvertimeModel.id!!
                    categoryShift.text = itemOvertimeModel.name
                }
                "approved" -> {
                    idApprovedBy = itemOvertimeModel.user_business_key!!
                    approvedByTv.text = itemOvertimeModel.user_name
                }
            }
        }
    }

    private fun requestListItemFromApi(userBusinessKey: String, actionRequest: String) {
        when (actionRequest){
            "overtime_category"->{
                progressBar.visibility = View.VISIBLE
                actionType = "overtime_category"
                OvertimeWs().getCategoryOvertimeWs(this, userBusinessKey, onCallBackEmployeeNameListener)
            }
            "category_shift"->{
                progressBar.visibility = View.VISIBLE
                actionType = "category_shift"
                OvertimeWs().getAllShiftListWs(this, userBusinessKey, onCallBackEmployeeNameListener)
            }
            "approved"->{
                progressBar.visibility = View.VISIBLE
                actionType = "approved"
                OvertimeWs().getEmployeeNameListWs(this, userBusinessKey, "", onCallBackEmployeeNameListener)
           }
        }
    }

    private val onCallBackEmployeeNameListener: OnCallBackEmployeeNameListener = object : OnCallBackEmployeeNameListener{
        override fun onLoadSuccessFull(itemOvertimeModel: ArrayList<ItemOvertimeModel>) {
           progressBar.visibility = View.GONE
            startBottomSheetDialog( itemOvertimeModel)
        }
        override fun onLoadFailed(message: String) {
            progressBar.visibility = View.GONE
            Utils.customToastMsgError(this@CreateOverTimeHRActivity, message, false)
        }
    }

    private val onCallBackCreateOvertime: OvertimeWs.OnCallBackCreateOvertime = object : OvertimeWs.OnCallBackCreateOvertime{
        override fun onLoadSuccessFull(message: String) {
            progressBar.visibility = View.GONE
            Utils.customToastMsgError(this@CreateOverTimeHRActivity,message, true)
            setResult(RESULT_OK)
            finish()
        }

        override fun onLoadFailed(message: String) {
            progressBar.visibility = View.GONE
            Utils.customToastMsgError(this@CreateOverTimeHRActivity,message,false)
        }
    }
}