package com.eazy.daikou.ui.home.book_now.booking_hotel.travel_article

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.request_data.request.book_now_ws.BookingHotelWS
import com.eazy.daikou.helper.DateUtil
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.booking_hotel.HotelArticleModel
import com.eazy.daikou.ui.home.book_now.booking_hotel.adapter.HotelArticlesRelatedAdapter
import de.hdodenhof.circleimageview.CircleImageView

class HotelArticlesDetailActivity : BaseActivity() {

    private var hotelArticleId: String = ""
    private lateinit var progressBar: ProgressBar
    private lateinit var imageArticle : ImageView
    private lateinit var recyclerView: RecyclerView
    private lateinit var titleHeaderTv: TextView
    private lateinit var typeArticleTv : TextView
    private lateinit var dateTv : TextView
    private lateinit var namePosterTv : TextView
    private lateinit var imgProfilePoster : CircleImageView
    private lateinit var durationTv : TextView
    private lateinit var descriptionTv : TextView
    private lateinit var btnShare : ImageView
    private lateinit var numViewerTv : TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hotel_articles_detail)

        initView()

        initData()

        initAction()

    }

    private fun initView(){
        progressBar = findViewById(R.id.progressItem)
        imageArticle = findViewById(R.id.imageArticle)
        recyclerView = findViewById(R.id.recyclerView)
        findViewById<ImageView>(R.id.btn_back).setOnClickListener { finish() }
        titleHeaderTv = findViewById(R.id.titleHeaderTv)
        typeArticleTv = findViewById(R.id.typeArticleTv)
        dateTv = findViewById(R.id.dateTv)
        namePosterTv = findViewById(R.id.posterTv)

        imgProfilePoster = findViewById(R.id.imgProfilePoster)
        durationTv = findViewById(R.id.durationTv)
        descriptionTv = findViewById(R.id.descriptionTv)
        btnShare = findViewById(R.id.btnShare)
        numViewerTv = findViewById(R.id.numViewerTv)

        // Set height on image slide
        imageArticle.layoutParams.height = Utils.setHeightOfScreen(this, 3.0)
    }

    private fun getStatusBarHeight(): Int {
        var result = 0
        val resourceId = resources.getIdentifier("status_bar_height", "dimen", "android")
        if (resourceId > 0) {
            result = resources.getDimensionPixelSize(resourceId)
        }
        return result
    }

    private fun initData(){
        if(intent != null && intent.hasExtra("hotel_articles_id")){
            hotelArticleId = intent.getSerializableExtra("hotel_articles_id") as String
        }
    }

    private fun initAction(){
        BookingHotelWS().getTravelArticleDetailWs(this, hotelArticleId, object : BookingHotelWS.OnCallBackTravelArticleListener{
            override fun onSuccessArticleList(hotelDashboardModel: ArrayList<HotelArticleModel>) {}

            override fun onSuccessArticleDetail(hotelDashboardModel: HotelArticleModel) {
                progressBar.visibility = View.GONE

                setValues(hotelDashboardModel)
            }

            override fun onFailed(message: String) {
                progressBar.visibility = View.GONE
                Utils.customToastMsgError(this@HotelArticlesDetailActivity, message, false)
            }

        })
    }

    private fun setValues(hotelDashboardModel: HotelArticleModel){
        Utils.setTextHtml(descriptionTv, hotelDashboardModel.content)
        Utils.setValueOnText(titleHeaderTv, hotelDashboardModel.title)
        Utils.setValueOnText(typeArticleTv, hotelDashboardModel.category_name)
        Utils.setValueOnText(numViewerTv, hotelDashboardModel.view)

        if (hotelDashboardModel.created_at != null){
            dateTv.text = DateUtil.formatDateComplaint(hotelDashboardModel.created_at)
        } else {
            dateTv.text = "- - -"
        }

        if (hotelDashboardModel.created_by != null){
            Utils.setValueOnText(namePosterTv, hotelDashboardModel.created_by!!.name)
            Glide.with(imgProfilePoster).load(if (hotelDashboardModel.created_by!!.image != null) hotelDashboardModel.created_by!!.image else R.drawable.ic_my_profile).into(imgProfilePoster)
        }

        Glide.with(imageArticle).load(if (hotelDashboardModel.image != null) hotelDashboardModel.image else R.drawable.no_image).into(imageArticle)

        Utils.setValueOnText(durationTv, DateUtil.getDurationDate(this, hotelDashboardModel.created_at))

        btnShare.setOnClickListener(CustomSetOnClickViewListener{
            Utils.shareLinkMultipleOption(this@HotelArticlesDetailActivity, hotelDashboardModel.url, hotelDashboardModel.title)
        })

        recyclerView.layoutManager = LinearLayoutManager(this,  RecyclerView.HORIZONTAL, false)
        recyclerView.adapter = HotelArticlesRelatedAdapter(this, hotelDashboardModel.latest_news, object : HotelArticlesRelatedAdapter.OnClickItemListener{
            override fun onClickItem(id: String) {
                if (id == null) return
                val intent = Intent(this@HotelArticlesDetailActivity, HotelArticlesDetailActivity::class.java)
                intent.putExtra("hotel_articles_id", id)
                startActivity(intent)
            }

        })
    }

}