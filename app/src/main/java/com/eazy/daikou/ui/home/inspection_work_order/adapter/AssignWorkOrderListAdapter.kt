package com.eazy.daikou.ui.home.inspection_work_order.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.helper.DateUtil
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.work_order.AssignWorkOrderModel

class AssignWorkOrderListAdapter(private val context: Context, private val assignWorkOrderList : ArrayList<AssignWorkOrderModel>, private val itemClickListener: CategoryItemClickListener) :
    RecyclerView.Adapter<AssignWorkOrderListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(context).inflate(R.layout.work_order_model, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val assignWorkOrderModel : AssignWorkOrderModel = assignWorkOrderList[position]
        if (assignWorkOrderModel != null){
            holder.detailItemLayout.visibility = View.GONE
            holder.assignWorkOrderLayout.visibility = View.VISIBLE

            Utils.setValueOnText(holder.workOrderNoTv, assignWorkOrderModel.workOrderNo)
            Utils.setValueOnText(holder.reportTypeTv, assignWorkOrderModel.reportTypeNo)

            Utils.setValueOnText(holder.doneStatusTv, assignWorkOrderModel.donePercent)
            if (assignWorkOrderModel.donePercent != null) {
                holder.doneStatusTv.backgroundTintList = if (Utils.setDoneStatusPercentOnWorkOrder(context, assignWorkOrderModel.donePercent) != null)
                    Utils.setDoneStatusPercentOnWorkOrder(context, assignWorkOrderModel.donePercent) else null
            }
            if (assignWorkOrderModel.issueDate != null) {
                holder.issueDateTv.text = DateUtil.formatDateComplaint(assignWorkOrderModel.issueDate)
            } else {
                holder.issueDateTv.text = ". . ."
            }

            holder.statusTv.text = if (assignWorkOrderModel.status != null) Utils.getValueFromKeyStatus(context)[assignWorkOrderModel.status] else ". . ."
            holder.statusTv.backgroundTintList = if (assignWorkOrderModel.status != null)  Utils.setColorStatusOnWorkOrder(context, assignWorkOrderModel.status) else null

            holder.assignLayout.setOnClickListener{itemClickListener.itemClickListener(assignWorkOrderModel)}
        }
    }

    override fun getItemCount(): Int {
        return assignWorkOrderList.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val workOrderNoTv: TextView = itemView.findViewById(R.id.workOrderNoTv)
        val issueDateTv: TextView = itemView.findViewById(R.id.issueAssignWorkOrderTv)
        val doneStatusTv: TextView = itemView.findViewById(R.id.doneStatusOrderTv)
        val reportTypeTv: TextView = itemView.findViewById(R.id.reportTypeTv)
        val assignLayout: LinearLayout = itemView.findViewById(R.id.assignLayout)
        val statusTv : TextView = itemView.findViewById(R.id.statusWorkOrderTv)
        val detailItemLayout: CardView = itemView.findViewById(R.id.detailItemLayout)

        val assignWorkOrderLayout: CardView = itemView.findViewById(R.id.assignWorkOrderLayout)

    }

    interface CategoryItemClickListener {
        fun itemClickListener(assignWorkOrderModel: AssignWorkOrderModel)
    }
}