package com.eazy.daikou.ui.home.complaint_solution.complaint.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.model.inspection.InfoInspectionModel
import com.eazy.daikou.model.my_property.my_property.PropertyMyUnitModel
import com.eazy.daikou.model.profile.PropertyItemModel
import com.eazy.daikou.model.profile.UnitNoModel
import com.eazy.daikou.model.work_order.WorkOrderItemModel
import com.eazy.daikou.request_data.static_data.StaticUtilsKey
import com.eazy.daikou.ui.home.inspection_work_order.adapter.ItemInspectionAdapter

class AdapterItemListComplaint() : RecyclerView.Adapter<AdapterItemListComplaint.ViewHolder>() {

    private lateinit var accountList: ArrayList<PropertyItemModel>
    private lateinit var unitNoList: ArrayList<UnitNoModel>
    private lateinit var context: Context
    private lateinit var clickBackListener: CallBackItemClickListener
    private var action : String = ""

    constructor(context: Context, action : String,accountList: ArrayList<PropertyItemModel>, clickBackListener: CallBackItemClickListener) : this() {
        this.context = context
        this.action = action
        this.accountList = accountList
        this.clickBackListener = clickBackListener
    }

    constructor(context: Context, unitNoList: ArrayList<UnitNoModel>, clickBackListener: CallBackItemClickListener) : this() {
        this.unitNoList = unitNoList
        this.context = context
        action = "unit_no"
        this.clickBackListener = clickBackListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
       val view = LayoutInflater.from(context).inflate(R.layout.custom_view_text_view_layout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (action == "unit_no") {
            unitNoList[position].name?.let {
                holder.onBind(if ((unitNoList.size - 1) == position) View.GONE else View.VISIBLE,
                    it
                )
            }

            holder.itemView.setOnClickListener {  clickBackListener.onClickUnit(unitNoList[position])}

        } else {
            accountList[position].name?.let {
                holder.onBind(if ((accountList.size - 1) == position) View.GONE else View.VISIBLE,
                    it
                )
            }

            holder.itemView.setOnClickListener {  clickBackListener.onClickAccount(accountList[position])}

        }

    }

    override fun getItemCount(): Int {
        return if (action == "unit_no") unitNoList.size else accountList.size
    }

    class ViewHolder(view : View) : RecyclerView.ViewHolder(view){
        val itemNameTv: TextView = itemView.findViewById(R.id.item_name)
        val line: View = itemView.findViewById(R.id.line)
        fun onBind(visible : Int, name : String) {
            itemNameTv.text = name
            line.visibility = visible
        }
    }

    interface CallBackItemClickListener {
        fun onClickAccount(itemComplaint : PropertyItemModel)
        fun onClickUnit(myUnit : UnitNoModel)
    }

    fun clear() {
        clearList(if (action == "unit_no") unitNoList else accountList)
    }

    private fun clearList(list : ArrayList<*>) {
        val size: Int = list.size
        if (size > 0) {
            for (i in 0 until size) {
                list.removeAt(0)
            }
            notifyItemRangeRemoved(0, size)
        }
    }
}