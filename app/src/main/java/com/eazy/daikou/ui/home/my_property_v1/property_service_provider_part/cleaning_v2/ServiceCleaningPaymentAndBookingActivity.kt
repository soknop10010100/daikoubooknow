package com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.cleaning_v2

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.res.ColorStateList
import android.os.Bundle
import android.view.View
import android.view.Window
import android.widget.*
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.base.MockUpData
import com.eazy.daikou.request_data.request.service_property_ws_v2.ServiceProviderPropertyWs
import com.eazy.daikou.helper.DateUtil
import com.eazy.daikou.helper.GetDataUtils
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.my_property.cleaning.AllScheduleEmployee
import com.eazy.daikou.model.my_property.cleaning.PaymentInfo
import com.eazy.daikou.model.my_property.cleaning.ServiceProviderDetailModel
import com.eazy.daikou.ui.home.facility_booking.adapter.AvailableDayAdapter
import com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.activity.ServicePropertyDateTimeActivity
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class ServiceCleaningPaymentAndBookingActivity : BaseActivity() {

    private lateinit var imageCoverView: ImageView
    private lateinit var progressBar: ProgressBar
    private lateinit var numberUnitTv: TextView
    private lateinit var serviceCategoryTv: TextView
    private lateinit var serviceTermTv: TextView
    private lateinit var serviceNameTv: TextView
    private lateinit var startDateTv: TextView
    private lateinit var endDateTv: TextView
    private lateinit var startTimeTv: TextView
    private lateinit var endTimeTv: TextView
    private lateinit var durationTv: TextView
    private lateinit var qtyTv: TextView
    private lateinit var totalTv: TextView
    private lateinit var subTotalTv: TextView
    private lateinit var btnConfirmBooking: TextView
    private lateinit var serviceDetailModel: ServiceProviderDetailModel

    private var actionDetail = ""
    private var idRegister = ""
    private var action = "confirm"
    private var unitId = ""
    private var serviceId = ""
    private var serviceTerm = ""
    private var numQuantity = ""
    private var startDate = ""
    private var startTime = ""
    private var listDays : ArrayList<String> = ArrayList()
    private lateinit var recyclerView: RecyclerView
    private lateinit var taxTv : TextView
    private lateinit var numWeekTv : TextView
    private lateinit var mainLayout : RelativeLayout
    private lateinit var btnRescheduleLinear : LinearLayout

    //status layout
    private lateinit var statusLayout : LinearLayout
    private lateinit var createdDateTv : TextView
    private lateinit var statusTv : TextView
    private lateinit var noTv : TextView

    //action bottom
    private lateinit var btnPayInvoice : TextView
    private lateinit var btnCancel : TextView
    private lateinit var btnReschedule : TextView
    private lateinit var btnConfirm : TextView
    private var notificationType = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_service_cleaning_payment_and_booking)

        initView()

        initData()

        initAction()

    }

    private fun initView(){
        imageCoverView = findViewById(R.id.imageCoverView)
        numberUnitTv = findViewById(R.id.numberUnitTv)
        serviceCategoryTv = findViewById(R.id.serviceCategoryTv)
        serviceTermTv = findViewById(R.id.serviceTermTv)
        serviceNameTv = findViewById(R.id.serviceNameTv)
        startDateTv = findViewById(R.id.startDateTv)
        endDateTv = findViewById(R.id.endDateTv)
        startTimeTv = findViewById(R.id.startTimeTv)
        endTimeTv = findViewById(R.id.endTimeTv)
        durationTv = findViewById(R.id.durationTv)
        qtyTv = findViewById(R.id.qtyTv)
        totalTv = findViewById(R.id.totalTv)
        subTotalTv = findViewById(R.id.subTotalTv)
        btnConfirmBooking = findViewById(R.id.btnConfirmBooking)
        progressBar = findViewById(R.id.progressItem)
        recyclerView = findViewById(R.id.recyclerView)
        taxTv = findViewById(R.id.taxTv)
        numWeekTv = findViewById(R.id.numWeekTv)
        mainLayout = findViewById(R.id.mainLayout)

        // status
        btnPayInvoice = findViewById(R.id.btnPayInvoice)
        statusLayout = findViewById(R.id.statusLayout)
        statusTv = findViewById(R.id.statusTv)
        createdDateTv = findViewById(R.id.createdDateTv)
        noTv = findViewById(R.id.noTv)
        btnRescheduleLinear = findViewById(R.id.btnRescheduleLinear)

        //action bottom
        btnCancel = findViewById(R.id.btnCancel)
        btnReschedule = findViewById(R.id.btnReschedule)
        btnConfirm = findViewById(R.id.btnConfirm)

    }

    private fun initData(){
        serviceId = GetDataUtils.getDataFromString("service_id", this)
        unitId = GetDataUtils.getDataFromString("unit_id", this)
        serviceTerm = GetDataUtils.getDataFromString("service_term", this)
        numQuantity = GetDataUtils.getDataFromString("quantity", this)
        startTime = GetDataUtils.getDataFromString("start_time", this)
        startDate = GetDataUtils.getDataFromString("start_date", this)

        if(intent != null && intent.hasExtra("action_notification")){
            notificationType = intent.getStringExtra("action_notification") as String
            actionDetail = "detail_display"
            idRegister = intent.getStringExtra("id") as String
        }

        if (intent != null && intent.hasExtra("available_days")){
            listDays = intent.getSerializableExtra("available_days") as ArrayList<String>
        }
        if (intent != null && intent.hasExtra("action_get_list") && intent.hasExtra("id_register")){
            actionDetail = intent.getStringExtra("action_get_list") as String
            idRegister = intent.getStringExtra("id_register") as String
        }
    }

    private fun initAction(){

        findViewById<ImageView>(R.id.btnBack).setOnClickListener { finish() }

        //Generate Summary Detail
        onRequestApiDetail()

        Utils.logDebug("requestApiCreateList", Utils.getStringGson(requestApiCreateList()))

        //Request Service
        btnConfirmBooking.setOnClickListener(
            CustomSetOnClickViewListener{
                action = "submit"
                Utils.customAlertDialogConfirm(this, "Are you sure to request this service ?"){
                    progressBar.visibility = View.VISIBLE
                    ServiceProviderPropertyWs().createListRequestServiceProviderWs(this, actionDetail, idRegister, requestApiCreateList(), onCallBackCreateServiceProvider)
                }
            }
        )

        if (actionDetail == "detail_display"){
            btnConfirmBooking.visibility = View.GONE
            statusLayout.visibility = View.VISIBLE
        }
        onRescheduleDetail()
    }

    private fun onRescheduleDetail(){
        btnRescheduleLinear.setOnClickListener(
            CustomSetOnClickViewListener {
                val bottomSheetFragment = PropertyServiceProviderRescheduleFragment().newInstance(serviceDetailModel)
                bottomSheetFragment.show(supportFragmentManager, "history_schedule")
            }
        )
    }
    private fun showDialogApproveReject(actionButton : String) {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.layout_custome_alert_confirm)
        val btnCancel = dialog.findViewById<View>(R.id.btn_cancel) as TextView
        val btnOk = dialog.findViewById<View>(R.id.btn_ok) as TextView
        val titleTv = dialog.findViewById<TextView>(R.id.title)
        val linearDescription = dialog.findViewById<LinearLayout>(R.id.layout)
        val descriptionEdt: EditText = dialog.findViewById(R.id.description)

        when (actionButton) {
            "reject" -> {
                titleTv.text = getString(R.string.are_you_sure_to_reject)
                linearDescription.visibility = View.VISIBLE
            }
            "accept" -> {
                titleTv.text = resources.getString(R.string.are_u_sure_to_approve)
                linearDescription.visibility = View.GONE
            }
        }
        btnOk.setOnClickListener {
            when (actionButton) {
                "reject" -> {
                    if (descriptionEdt.text.isNotEmpty()) { requestServiceApiRescheduleAndAccept(actionButton, descriptionEdt.text.toString())
                        dialog.dismiss()
                    } else {
                        Utils.customToastMsgError(this, getString(R.string.please_input_your_reason), false)
                    }
                }
                "accept" -> {
                    requestServiceApiRescheduleAndAccept(actionButton, "")
                    dialog.dismiss()
                }
            }
        }
        btnCancel.setOnClickListener { dialog.dismiss() }

        dialog.show()
    }

    private fun onRequestApiDetail(){
        progressBar.visibility = View.VISIBLE
        ServiceProviderPropertyWs().createListRequestServiceProviderWs(this, actionDetail, idRegister, requestApiCreateList(), onCallBackCreateServiceProvider)
    }

    private fun onActionButtonListDetail(actionButton: String?, paymentInfo: ArrayList<PaymentInfo>){
        when (actionButton) {
            "new" -> {
                btnCancel.visibility = View.VISIBLE
                btnReschedule.visibility = View.GONE
                btnConfirm.visibility = View.GONE
            }
            "pending" -> {
                btnCancel.visibility = View.VISIBLE
                btnReschedule.visibility = View.VISIBLE
                btnConfirm.visibility = View.VISIBLE
            }
            "accepted" -> {
                btnCancel.visibility = View.GONE
                btnReschedule.visibility = View.GONE
                btnConfirm.visibility = View.GONE
                btnPayInvoice.visibility = View.VISIBLE
            }
            "cancelled", "confirmed" -> {
                btnCancel.visibility = View.GONE
                btnReschedule.visibility = View.GONE
                btnConfirm.visibility = View.GONE
            }
            else -> {
                btnCancel.visibility = View.GONE
                btnReschedule.visibility = View.GONE
                btnConfirm.visibility = View.GONE
            }
        }

        btnCancel.setOnClickListener(
            CustomSetOnClickViewListener {
                showDialogApproveReject("reject")
            }
        )

        btnReschedule.setOnClickListener(
            CustomSetOnClickViewListener {
                openRescheduleActivityForResult("reschedule")
            }
        )
        btnConfirm.setOnClickListener(
            CustomSetOnClickViewListener {
                showDialogApproveReject("accept")
            }
        )
        btnPayInvoice.setOnClickListener(
            CustomSetOnClickViewListener {
                startOpenInvoice(paymentInfo)
            }
        )

        //notification invoice
        if (notificationType == "invoice_operation_service_property"){
            startOpenInvoice(paymentInfo)
        }

    }

    private fun startOpenInvoice(paymentInfo: ArrayList<PaymentInfo>){
        val intent = Intent(this, PropertyServicePaymentInvoiceActivity::class.java)
        intent.putExtra("payment_info", paymentInfo)
        startActivity(intent)
    }

    private fun openRescheduleActivityForResult(actionButton: String) {
        if (serviceDetailModel != null) {
            val intent = Intent(this, ServicePropertyDateTimeActivity::class.java)
            intent.putExtra("action_key_button", actionButton)
            intent.putExtra("service_detail_model", serviceDetailModel)
            resultLauncher.launch(intent)
        }
    }

    private fun requestServiceApiRescheduleAndAccept(actionButton: String, remark: String){
        val hashMap : HashMap<String, Any> = HashMap()
        hashMap["service_registration_id"] = idRegister
        hashMap["user_id"] = UserSessionManagement(this).userId
        hashMap["action"] = actionButton
        if (actionButton == "reject") {
            hashMap["remark"] = remark
        }

        ServiceProviderPropertyWs().rescheduleAndAcceptServiceProviderWs(this, hashMap, callBackRescheduleAndAccept)
    }

    private val callBackRescheduleAndAccept : ServiceProviderPropertyWs.OnCallBackRescheduleAndAccept = object : ServiceProviderPropertyWs.OnCallBackRescheduleAndAccept{
        override fun onLoadSuccessFull(message: String) {
            progressBar.visibility = View.GONE
            Utils.customToastMsgError(this@ServiceCleaningPaymentAndBookingActivity, message, true)

            onRequestApiDetail()
            Utils.setResultBack(this@ServiceCleaningPaymentAndBookingActivity, "", false)
        }

        override fun onLoadFailed(error: String) {
            progressBar.visibility = View.GONE
            Utils.customToastMsgError(this@ServiceCleaningPaymentAndBookingActivity, error, false)
        }
    }

    private val onCallBackCreateServiceProvider: ServiceProviderPropertyWs.OnCallBackCreateServiceProvider = object : ServiceProviderPropertyWs.OnCallBackCreateServiceProvider{
        override fun onSuccessGetData(serviceDetail: ServiceProviderDetailModel) {
            progressBar.visibility = View.GONE
            mainLayout.visibility = View.VISIBLE
            if (serviceDetail != null) {
                serviceDetailModel = serviceDetail
                getDataForDisplay()
            }
        }

        override fun onLoadSuccessFull(message: String) {
            progressBar.visibility = View.GONE
            mainLayout.visibility = View.VISIBLE
            Utils.customToastMsgError(this@ServiceCleaningPaymentAndBookingActivity, message, true)

            //Set Result CallBack
            Utils.setResultBack(this@ServiceCleaningPaymentAndBookingActivity, "", true)
        }

        override fun onLoadFailed(error: String) {
            progressBar.visibility = View.GONE
            Utils.customToastMsgError(this@ServiceCleaningPaymentAndBookingActivity, error, false)
        }
    }

    private fun getDataForDisplay(){
        //Service Detail
        if (serviceDetailModel.service_detail != null) {
            numberUnitTv.text =
                if (serviceDetailModel.service_detail!!.unit_no != null) serviceDetailModel.service_detail!!.unit_no else "- - -"
            serviceCategoryTv.text =
                if (serviceDetailModel.service_detail!!.service_category != null) serviceDetailModel.service_detail!!.service_category else "- - -"
            serviceTermTv.text =
                if (serviceDetailModel.service_detail!!.service_term != null) serviceDetailModel.service_detail!!.service_term else "- - -"
            serviceNameTv.text =
                if (serviceDetailModel.service_detail!!.service_name != null) serviceDetailModel.service_detail!!.service_name else "- - -"
            if (serviceDetailModel.service_detail!!.service_photo != null){
                Glide.with(this).load(serviceDetailModel.service_detail!!.service_photo).into(imageCoverView)
            } else{
                Glide.with(this).load(R.drawable.no_image).into(imageCoverView)
            }
        }
        //DateAndTime
        if (serviceDetailModel.date_and_time != null){
            startDateTv.text = if (serviceDetailModel.date_and_time!!.start_date != null) serviceDetailModel.date_and_time!!.start_date else "- - -"
            endDateTv.text = if (serviceDetailModel.date_and_time!!.end_date != null) serviceDetailModel.date_and_time!!.end_date else "- - -"
            startTimeTv.text = if (serviceDetailModel.date_and_time!!.start_time != null) Utils.formatDateTime(serviceDetailModel.date_and_time!!.start_time, "hh:mm:ss", "h:mm a") else "- - -"
            endTimeTv.text = if (serviceDetailModel.date_and_time!!.end_time != null) Utils.formatDateTime(serviceDetailModel.date_and_time!!.end_time, "hh:mm:ss", "h:mm a") else "- - -"
            durationTv.text = if (serviceDetailModel.date_and_time!!.duration != null) serviceDetailModel.date_and_time!!.duration else "- - -"
        }
        //Sub Total
        if (serviceDetailModel.booking_summary != null){
            qtyTv.text = if (serviceDetailModel.booking_summary!!.qty != null) serviceDetailModel.booking_summary!!.qty else "- - -"
            totalTv.text = if (serviceDetailModel.booking_summary!!.total != null) serviceDetailModel.booking_summary!!.total else "- - -"
            subTotalTv.text = if (serviceDetailModel.booking_summary!!.subtotal != null) serviceDetailModel.booking_summary!!.subtotal else "- - -"
            taxTv.text = if (serviceDetailModel.booking_summary!!.tax != null) serviceDetailModel.booking_summary!!.tax else "- - -"
        }

        //Sub Total
        if (serviceDetailModel.registration_info != null){
            Utils.setValueOnText(noTv, serviceDetailModel.registration_info!!.reg_no)
            Utils.setValueOnText(createdDateTv, DateUtil.formatTimeZoneLocal(serviceDetailModel.registration_info!!.created_date))
            if (serviceDetailModel.registration_info!!.status != null){
                statusTv.backgroundTintList = setColorStatusOnWorkOrder(this, serviceDetailModel.registration_info!!.status)
                statusTv.text = getValueFromKeyStatus(this)[serviceDetailModel.registration_info!!.status]
            } else {
                statusTv.backgroundTintList = setColorStatusOnWorkOrder(this, "")
                statusTv.text = "- - -"
            }
        }

        //Check action list reschedule log
        btnRescheduleLinear.visibility = if (serviceDetailModel.history_logs.size > 0) View.VISIBLE else View.GONE

        initRecycleDay()

        if (serviceDetailModel.registration_info != null)   onActionButtonListDetail(serviceDetailModel.registration_info?.status, serviceDetailModel.registration_info!!.payment_info )
    }

    private fun requestApiCreateList() : HashMap<String, Any>{
        val hashMap : HashMap<String, Any> = HashMap()
        hashMap["action"] = action
        hashMap["user_id"] = UserSessionManagement(this).userId
        hashMap["service_id"] = serviceId
        hashMap["unit_id"] = unitId
        hashMap["active_user_type"] = MockUpData.getUserItem(UserSessionManagement(this)).activeUserType
        hashMap["days"] = listDays
        hashMap["start_date"] = startDate
        hashMap["start_time"] = startTime
        hashMap["qty"] = numQuantity
        return hashMap
    }

    private fun initRecycleDay() {
        recyclerView.layoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
        val dayList : ArrayList<String> = ArrayList()
        for (item in serviceDetailModel.reg_schedule_date){
            dayList.add(DateUtil.formatSimpleDateToDayDate(item, "yyyy-MM-dd", "dd EEE MM/yyyy"))
        }
        val serviceTermAdapter = AvailableDayAdapter("available_day_service_property", dayList, object : AvailableDayAdapter.OnClickCallBackListener{
            override fun onClickBack(data: String) {
                val dateFormat = DateUtil.formatSimpleDateToDayDate( data, "dd EEE MM/yyyy", "yyyy-MM-dd")
                val allScheduleEmployees : ArrayList<AllScheduleEmployee> = ArrayList()
                for (item in serviceDetailModel.all_schedule_employees){
                    if (item.working_date != null){
                        if (dateFormat == item.working_date){
                            allScheduleEmployees.add(item)
                        }
                    }
                }
                val intent = Intent(this@ServiceCleaningPaymentAndBookingActivity, ServicePropertyAssignEmployeeActivity::class.java)
                intent.putExtra("all_schedule_employees", allScheduleEmployees)
                intent.putExtra("action", "all_schedule_employees")
                startActivity(intent)
            }

        })
        recyclerView.adapter = serviceTermAdapter

        numWeekTv.setOnClickListener {
            val intent = Intent(this@ServiceCleaningPaymentAndBookingActivity, ServicePropertyAssignEmployeeActivity::class.java)
            intent.putExtra("available_day", dayList)
            intent.putExtra("action", "available_day")
            intent.putExtra("all_schedule_employees", serviceDetailModel.all_schedule_employees)
            startActivity(intent)
        }
    }

    private var resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode == Activity.RESULT_OK) {
            val data: Intent? = result.data
            if (data != null) {
                //Set Result CallBack
                val isBackSuccess = data.getBooleanExtra("list_reschedule", false)
                val isResultSuccess = data.getBooleanExtra("is_result_success", false)
                if (isBackSuccess)  {
                    val action = data.getStringExtra("action")
                    if (action == "rescheduled_service"){    // when already reschedule service
                        onRequestApiDetail()

                        Utils.setResultBack(this@ServiceCleaningPaymentAndBookingActivity, action, false)

                    } else {    // when already create service
                        Utils.setResultBack(this@ServiceCleaningPaymentAndBookingActivity, "", true)
                    }
                } else if (isResultSuccess){
                    val action = data.getStringExtra("action")
                    if (action == "rescheduled_service"){    // when already reschedule service
                        onRequestApiDetail()
                        Utils.setResultBack(this@ServiceCleaningPaymentAndBookingActivity, action, false)
                    }
                }
            }
        }
    }

    private fun setColorStatusOnWorkOrder(context: Context?, valueStatus: String?): ColorStateList {
        return when (valueStatus) {
            "new" -> ColorStateList.valueOf(ContextCompat.getColor(context!!, R.color.blue))
            "pending" -> ColorStateList.valueOf(ContextCompat.getColor(context!!, R.color.yellow))
            "accepted" -> ColorStateList.valueOf(ContextCompat.getColor(context!!, R.color.green))
            "cancelled" -> ColorStateList.valueOf(ContextCompat.getColor(context!!, R.color.red))
            "confirmed" -> ColorStateList.valueOf(ContextCompat.getColor(context!!, R.color.appBarColorOld))
            else -> ColorStateList.valueOf(ContextCompat.getColor(context!!, R.color.appBarColorOld))
        }
    }

    private fun getValueFromKeyStatus(context: Context?): HashMap<String, String> {
        val hashMap = HashMap<String, String>()
        hashMap["new"] = Utils.getText(context, R.string.new_)
        hashMap["pending"] = Utils.getText(context, R.string.pending)
        hashMap["accepted"] = Utils.getText(context, R.string.accepted)
        hashMap["cancelled"] = Utils.getText(context, R.string.cancel)
        hashMap["confirmed"] = Utils.getText(context, R.string.confirm)
        return hashMap
    }
}