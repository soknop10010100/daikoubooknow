package com.eazy.daikou.ui.home.parking.printer;

import com.eazy.daikou.base.MyApplication;

public class BaseApp extends MyApplication {

    @Override
    public void onCreate() {
        super.onCreate();
        init();
    }

    /**
     * Connect print service through interface library
     */
    private void init(){
        SunmiPrintHelper.getInstance().initSunmiPrinterService(this);
    }
}
