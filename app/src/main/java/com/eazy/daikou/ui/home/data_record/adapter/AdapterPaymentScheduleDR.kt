package com.eazy.daikou.ui.home.data_record.adapter

import android.content.Context
import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.data_record.PaymentSchedule

class AdapterPaymentScheduleDR(private val context: Context,private val actionType : String, private val paymentScheduleList: ArrayList<PaymentSchedule>, private val callBackItemClickItem : CallBackItemClickListener) : RecyclerView.Adapter<AdapterPaymentScheduleDR.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.custom_layout_payment_schedule, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val paymentSchedule : PaymentSchedule = paymentScheduleList[position]
        if (paymentSchedule != null){
            holder.stageTv.text = if (paymentSchedule.stage != null) paymentSchedule.stage else "- - -"
            holder.percentageTv.text = if (paymentSchedule.percentage != null) paymentSchedule.percentage else "- - -"
            holder.amountTv.text = if (paymentSchedule.amount != null) paymentSchedule.amount else "- - - "
            holder.intersTv.text = if (paymentSchedule.interest != null) paymentSchedule.interest else "- - -"
            holder.intersAmountTv.text = if (paymentSchedule.interest_amount != null) paymentSchedule.interest_amount else "- - -"
            holder.totalTermMonthTv.text = if (paymentSchedule.total_term != null) paymentSchedule.total_term else "- - -"
            holder.paymentTv.text = if (paymentSchedule.payment_amount != null) paymentSchedule.payment_amount else "- - - "

            if (actionType == "action_click"){
                holder.linearInvoice.visibility = View.VISIBLE
                holder.linearStatus.visibility = View.VISIBLE
                holder.linearPayDate.visibility = View.VISIBLE
                holder.itemView.setOnClickListener{callBackItemClickItem.clickItemCallback(paymentSchedule)}
            }
            Utils.setValueOnText(holder.payDateTv, paymentSchedule.date_pay)
            setBackgroundColor(if (paymentSchedule.payment_status != null) paymentSchedule.payment_status.toString() else ". . .", holder.statusTv)
        }
    }

    override fun getItemCount(): Int {
       return  paymentScheduleList.size
    }

    class ViewHolder(view : View) : RecyclerView.ViewHolder(view){

        var stageTv: TextView = view.findViewById(R.id.stageTv)
        var percentageTv: TextView = view.findViewById(R.id.percentageTv)
        var amountTv: TextView = view.findViewById(R.id.amountTv)
        var intersTv: TextView = view.findViewById(R.id.intersTv)
        var intersAmountTv: TextView = view.findViewById(R.id.intersAmountTv)
        var totalTermMonthTv: TextView = view.findViewById(R.id.totalTermMonthTv)
        var paymentTv: TextView = view.findViewById(R.id.paymentTv)
        var payDateTv: TextView = view.findViewById(R.id.payDateTv)
        var statusTv: TextView = view.findViewById(R.id.statusTv)
        var linearInvoice: LinearLayout = view.findViewById(R.id.linearInvoice)
        var linearStatus: LinearLayout = view.findViewById(R.id.linearStatus)
        var linearPayDate: LinearLayout = view.findViewById(R.id.linearPayDate)
    }

    interface CallBackItemClickListener{
        fun clickItemCallback(paymentSchedule : PaymentSchedule)
    }

    private fun setBackgroundColor(keyStatus: String, textStatus: TextView){
        var color = R.color.appBarColor
        var translateKey = "- - -"
        when(keyStatus){
            "paid" -> {
               color = R.color.green
                translateKey = context.getString(R.string.paid)
            }

            "unpaid" -> {
                color = R.color.red
                translateKey = context.getString(R.string.un_paid)
            }

            "no_invoice" -> {
                color = R.color.yellow
                translateKey = context.getString(R.string.no_invoice)
            }
        }

        textStatus.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(context, color))
        textStatus.text = translateKey
    }
}