package com.eazy.daikou.ui.home.quote_book.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.eazy.daikou.R;
import com.eazy.daikou.model.book.BookModelV2;

import java.util.List;

public class QuoteBookAdapter extends RecyclerView.Adapter<QuoteBookAdapter.ViewHolder>{

    private final Context context;
    private final List<BookModelV2> bookCategoryList;
    private final CallBackQuoteBook callBackQuoteBook;

    public QuoteBookAdapter(Context context, List<BookModelV2> bookCategoryList, CallBackQuoteBook callBackQuoteBook) {
        this.context = context;
        this.bookCategoryList = bookCategoryList;
        this.callBackQuoteBook = callBackQuoteBook;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
       View view = LayoutInflater.from(context).inflate(R.layout.layout_item_book_quote,parent,false);
       return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        BookModelV2 bookCategory = bookCategoryList.get(position);
        if (bookCategory != null){
            holder.tv_titleBook.setText(bookCategory.getCategoryName());
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, RecyclerView.HORIZONTAL,false);
            holder.recyclerView.setLayoutManager(linearLayoutManager);

            QuoteItemBookAdapter quoteItemBookAdapter = new QuoteItemBookAdapter(context,bookCategory.getAll());
            holder.recyclerView.setAdapter(quoteItemBookAdapter);

            holder.tvViewAll.setOnClickListener(view -> callBackQuoteBook.clickAllBook(bookCategory));

        }
    }

    @Override
    public int getItemCount() {
        return bookCategoryList.size();
    }

    public static class  ViewHolder extends RecyclerView.ViewHolder{

        private final RecyclerView recyclerView;
        private final TextView tv_titleBook;
        private final TextView tvViewAll;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            recyclerView = itemView.findViewById(R.id.recyclerViewBookHor);
            tv_titleBook = itemView.findViewById(R.id.Tv_titleBook);
            tvViewAll = itemView.findViewById(R.id.tvViewAll);
        }
    }

    public interface CallBackQuoteBook{
        void clickAllBook(BookModelV2 bookCategory);
    }
}
