package com.eazy.daikou.ui.home.utility_tracking;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.eazy.daikou.R;
import com.eazy.daikou.base.BaseActivity;
import com.eazy.daikou.request_data.request.track_water_ws.TrackWaterElectricityWs;
import com.eazy.daikou.request_data.sql_database.TrackWaterElectricityDatabase;
import com.eazy.daikou.request_data.static_data.TypeTrackingWaterElectricity;
import com.eazy.daikou.helper.AppAlertCusDialog;
import com.eazy.daikou.helper.InternetConnection;
import com.eazy.daikou.helper.UserSessionManagement;
import com.eazy.daikou.model.home.MyRolePermissionModel;
import com.eazy.daikou.model.profile.User;
import com.eazy.daikou.model.utillity_tracking.model.W_ETrackingModel;
import com.eazy.daikou.ui.home.utility_tracking.adapter.ListWaterElectricityAdapter;
import com.eazy.daikou.model.utillity_tracking.model.FloorUtilNoModel;
import com.google.gson.Gson;

import org.json.JSONException;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class ListWaterElectricityActivity extends BaseActivity {

    private User user;
    private LinearLayoutManager linearLayoutManager;
    private ProgressBar progressBar;
    private ListWaterElectricityAdapter listWaterElectricityAdapter;
    private TextView noItemTv , txtTrackingDate , txtDownloadSync;
    private TrackWaterElectricityDatabase database;
    private CardView warningView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ImageView btnSync ,btnAdd;
    private float percentSyncData , percentOneSyncData;
    private RelativeLayout progressSync;
    private MyRolePermissionModel userPermissionModel;
    private RecyclerView recyclerView;
    private SimpleDateFormat formatter;

    private final List<W_ETrackingModel> listTrackingW_E = new ArrayList<>();
    private List<FloorUtilNoModel> floorUtilNoModelList = new ArrayList<>();

    private final int REQUEST_UPLOAD_UTILITY = 1231;

    private int currentItem, total, scrollDown, currentPage = 1, size = 10;
    private Boolean isScrolling = true;
    private Boolean isSyncData = false;
    private Boolean isFinishSynData = false;
    private Boolean isFinishLoop = false;
    private Boolean isClickSave = false;
    private Boolean isGetDateFromLocalFirst = false;
    private String action;

    @SuppressLint({"CutPasteId", "SetTextI18n"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_water_electricity);

        UserSessionManagement sessionManagement = new UserSessionManagement(this);
        user = new Gson().fromJson(sessionManagement.getUserDetail(), User.class);

        recyclerView = findViewById(R.id.listView);
        btnAdd = findViewById(R.id.btnAdd);
        progressBar = findViewById(R.id.progressItem);
        noItemTv = findViewById(R.id.noItemTv);
        warningView  = findViewById(R.id.warningView);
        swipeRefreshLayout = findViewById(R.id.swipe_layout);
        txtTrackingDate = findViewById(R.id.txtTrackingDate);
        btnSync = findViewById(R.id.btnSync);
        ProgressBar progress_bar_sync = findViewById(R.id.progress_bar);
        progressSync = findViewById(R.id.view_progress);
        txtDownloadSync = findViewById(R.id.txtDownloadSync);

        TextView title = findViewById(R.id.titleToolbar);
        ImageView btnBack = findViewById(R.id.iconBack);
        LinearLayout layoutMap = findViewById(R.id.layoutMap);
        ImageView imageSearch = findViewById(R.id.addressMap);
        TextView txtScreen = findViewById(R.id.txtScreen);
        layoutMap.setVisibility(View.VISIBLE);
        imageSearch.setImageResource(R.drawable.ic_search);
        imageSearch.setVisibility(View.GONE);

        if (getIntent() != null && getIntent().hasExtra("action")){
            action = getIntent().getStringExtra("action");
            userPermissionModel = (MyRolePermissionModel) getIntent().getSerializableExtra("user_permission");
        }

        formatter = new SimpleDateFormat("yyyy-MM-dd");
        database = new TrackWaterElectricityDatabase(getBaseContext());
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);

        switch (action) {
            case "water_track":
                Boolean isWater = true;
                Boolean isUnit = true;
                title.setText(getString(R.string.water_record));
                txtScreen.setText(getString(R.string.dot_unit));
                break;
            case "water_common":
                isWater = true;
                title.setText(getString(R.string.water_record));
                txtScreen.setText(getString(R.string.dot_common_area));
                break;
            case "water_ppwsa":
                isWater = true;
                title.setText(getString(R.string.water_record));
                txtScreen.setText(getString(R.string.dot_ppwsa));
                break;
            case "electricity_track":
                isUnit = true;
                title.setText(getString(R.string.electricity_record));
                txtScreen.setText(getString(R.string.dot_unit));
                break;
            case "electricity_common":
                title.setText(getString(R.string.electricity_record));
                txtScreen.setText(getString(R.string.dot_common_area));
                break;
            case "electricity_edc":
                title.setText(getString(R.string.electricity_record));
                txtScreen.setText(getString(R.string.dot_edc));
                break;
        }

        initRecyclerView(recyclerView);

        initScrollRecordW_E(recyclerView);

        // get data from local and create or update list
        try {
            getAllFloorFromLocalDatabase();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        checkHaveStoreDataInLocal();

        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
        swipeRefreshLayout.setOnRefreshListener(() -> {
            currentPage = 1;
            size = 10;
            listTrackingW_E.clear();
            getListRecordTrackW_E(recyclerView);
        });

        btnBack.setOnClickListener(v -> finish());

        //Click On Warning if not syn with server
        warningView.setOnClickListener(v -> {
            Intent intent = new Intent(ListWaterElectricityActivity.this , UploadWaterElectricityActivity.class);
            intent.putExtra("action",action);
            try {
                intent.putExtra("list", (Serializable) database.getAllFloorHaveDataV2(action));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            intent.putExtra("purpose",TypeTrackingWaterElectricity.UTILITY_PURPOSE_CREATE);
            intent.putExtra("click_action","warning");
            intent.putExtra("txtTrackingDate", txtTrackingDate.getText().toString().trim());
            startActivityForResult(intent,REQUEST_UPLOAD_UTILITY);
        });

        btnAdd.setOnClickListener(v ->{
            String date = database.getFloorSaveDate();
            if (!date.equals("")) {
                try {

                    Date dateSave = formatter.parse(date);
                    Date dateToday = formatter.parse(formatter.format(new Date()));
                    assert dateSave != null;
//                    if (dateSave.compareTo(dateToday) < 0) {
//                        new AppAlertCusDialog().showDialog(ListWaterElectricityActivity.this,  getResources().getString(R.string.please_get_new_data_before_close_internet));
//                    } else {

                        if (InternetConnection.checkInternetConnection(btnAdd)){
                            W_ETrackingModel eTrackingModel = new W_ETrackingModel();
                            String today = formatter.format(new Date());
                            eTrackingModel.setTrackingDate(today);

                            progressBar.setVisibility(View.VISIBLE);
                            getListAllFloorAndUnit(eTrackingModel, true);

                        } else {
                            if (isGetDateFromLocalFirst) {
                                Intent intent = new Intent(ListWaterElectricityActivity.this, UploadWaterElectricityActivity.class);
                                intent.putExtra("action", action);
                                intent.putExtra("list", (Serializable) floorUtilNoModelList);
                                intent.putExtra("purpose", TypeTrackingWaterElectricity.UTILITY_PURPOSE_CREATE);
                                intent.putExtra("click_action", "add");
                                startActivityForResult(intent, REQUEST_UPLOAD_UTILITY);
                            } else {
                                Toast.makeText(ListWaterElectricityActivity.this,  getResources().getString(R.string.please_wait), Toast.LENGTH_SHORT).show();
                            }
                        }

//                    }

                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            else {
                new AppAlertCusDialog().showDialog(ListWaterElectricityActivity.this,  getResources().getString(R.string.please_get_new_data_before_close_internet));
            }
        });

        btnSync.setOnClickListener(v ->{
            progressSync.setVisibility(View.VISIBLE);
            List<HashMap<String, String>> list = database.getFloorDetailList(action);
            percentSyncData = 100 / list.size();
            percentOneSyncData = percentSyncData;
            for (int i = 0 ; i < list.size() ; i++){
                HashMap<String,Object> hashMap = new HashMap<>();
                hashMap.put("id",list.get(i).get("id"));
                hashMap.put("property_id",list.get(i).get("property_id"));
                hashMap.put("location_id",list.get(i).get("location_id"));
                hashMap.put("utility_area",TypeTrackingWaterElectricity.getUtilityType().get(action));
                hashMap.put("utility_type",list.get(i).get("utility_type"));
                hashMap.put("tracking_date",list.get(i).get("tracking_date"));
                hashMap.put("current_usage",list.get(i).get("current_usage"));
                hashMap.put("total_usage",list.get(i).get("total_usage"));
                hashMap.put("utility_photo_file",list.get(i).get("utility_photo"));
                hashMap.put("purpose",list.get(i).get("purpose"));
                saveUtility(hashMap);
                isFinishLoop = i < list.size(); // to check if sync success
                isSyncData = true; // to check sync percent
            }
        });
    }

    private void refreshList(){
        currentPage = 1;
        size = 10;
        isScrolling = true;
        if (listWaterElectricityAdapter != null){
            listWaterElectricityAdapter.clear();
            getListRecordTrackW_E(recyclerView);
        } else {
            if (!listTrackingW_E.isEmpty())     listTrackingW_E.clear();
            initRecyclerView(recyclerView);
            initScrollRecordW_E(recyclerView);
        }
    }

    private void checkHaveStoreDataInLocal(){
        // check if have data in local show warning screen to the user
        int haveSavingData = 0;
        List<HashMap<String,String>> hashMaps = new ArrayList<>();
        try {
            haveSavingData = database.getFloorDetailList(action).size();
            hashMaps = database.getFloorDetailList(action);
        } catch (Exception e ){
            e.printStackTrace();
        }

        Boolean isHaveDateSaveInLocal = false;
        if (haveSavingData != 0){
            isHaveDateSaveInLocal = true;
            btnAdd.setVisibility(View.GONE);
            warningView.setVisibility(View.VISIBLE);
            txtTrackingDate.setText(hashMaps.get(0).get("tracking_date"));
            btnSync.setVisibility(View.VISIBLE);
        } else {
            isHaveDateSaveInLocal = false;
            warningView.setVisibility(View.GONE);
            btnSync.setVisibility(View.GONE);

            // Code before have check internet connection
            if (!userPermissionModel.is_role()) {
                btnAdd.setVisibility(View.GONE);
            } else {
                btnAdd.setVisibility(View.VISIBLE);
            }
        }
    }

    private void getAllFloorFromLocalDatabase() throws JSONException {
        floorUtilNoModelList.addAll(database.getAllFloor());
        isGetDateFromLocalFirst = true;

        if (InternetConnection.checkInternetConnection(recyclerView)){
            recyclerView.setVisibility(View.VISIBLE);
            noItemTv.setVisibility(View.GONE);
            // if get floor from local is empty, it mean that in local don't have any data
            if (floorUtilNoModelList.isEmpty()){
                getAllFloor("create");
            } else {
                getAllFloor("update");
            }
        } else {
            progressBar.setVisibility(View.GONE);
            noItemTv.setVisibility(View.VISIBLE);   noItemTv.setText(getString(R.string.please_check_your_internet_connection));
            recyclerView.setVisibility(View.GONE);
        }

    }

    private void getAllFloor(String action){
        // action to check create or update floor in local and type to check after clear data from local
        String today = formatter.format(new Date());
        new TrackWaterElectricityWs().getAllFloorRecord( user.getActivePropertyIdFk() ,today, action, ListWaterElectricityActivity.this,new TrackWaterElectricityWs.OnTrackWaterCallBack() {
            @Override
            public void onSuccess(String message , List<FloorUtilNoModel> list) {
                if (floorUtilNoModelList.size() > 0)    floorUtilNoModelList = new ArrayList<>();
                floorUtilNoModelList.addAll(list);
            }

            @Override
            public void onFail(String message) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(ListWaterElectricityActivity.this,message,Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getListRecordTrackW_E(RecyclerView recyclerView){
        progressBar.setVisibility(View.VISIBLE);
        new TrackWaterElectricityWs().getAllListTracking(currentPage, 10, user.getActivePropertyIdFk(), ListWaterElectricityActivity.this, action, new TrackWaterElectricityWs.OnRecordTrackWaterCallBack() {
            @Override
            public void onSuccessSubApi(String message, List<W_ETrackingModel> list) {
                swipeRefreshLayout.setRefreshing(false);
                progressBar.setVisibility(View.GONE);
                if (isFinishSynData){
                    if (!listTrackingW_E.isEmpty()){
                        isFinishSynData = false;
                        listTrackingW_E.clear();
                    }
                }
                listTrackingW_E.addAll(list);
                listWaterElectricityAdapter.notifyDataSetChanged();

                noItemTv.setVisibility(listTrackingW_E.size() == 0 ? View.VISIBLE : View.GONE);
                recyclerView.setVisibility(listTrackingW_E.size() > 0 ? View.VISIBLE : View.GONE);
            }

            @Override
            public void onFail(String message) {
                swipeRefreshLayout.setRefreshing(false);
                progressBar.setVisibility(View.GONE);
                Toast.makeText(ListWaterElectricityActivity.this, message, Toast.LENGTH_SHORT).show();
            }

        });
    }

    private void initRecyclerView(RecyclerView recyclerView) {
        listWaterElectricityAdapter = new ListWaterElectricityAdapter(listTrackingW_E, action ,onClickItemTracking);
        recyclerView.setAdapter(listWaterElectricityAdapter);
        getListRecordTrackW_E(recyclerView);
    }

    private void initScrollRecordW_E(RecyclerView recyclerView){
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL){
                    isScrolling = true;
                }
            }
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                currentItem = linearLayoutManager.getChildCount();
                total = linearLayoutManager.getItemCount();
                scrollDown = linearLayoutManager.findFirstVisibleItemPosition();
                if(dy > 0) {
                    if (isScrolling && (currentItem + scrollDown == total)) {
                        if(total == size) {
                            isScrolling = false;
                            currentPage++;
                            progressBar.setVisibility(View.VISIBLE);
                            recyclerView.scrollToPosition(listTrackingW_E.size() - 1);
                            isFinishSynData = false;
                            initRecyclerView(recyclerView);
                            size+=10;
                        }else {
                            progressBar.setVisibility(View.GONE);
                        }
                    }
                }

            }
        });
    }

    private final ListWaterElectricityAdapter.OnClickItemTracking onClickItemTracking = new ListWaterElectricityAdapter.OnClickItemTracking() {
        @Override
        public void onClickItemTrack(W_ETrackingModel eTrackingModel) {
            if (userPermissionModel.is_role()){
                progressBar.setVisibility(View.VISIBLE);
                getListAllFloorAndUnit(eTrackingModel, false);
            }
        }
    };

    private void getListAllFloorAndUnit(W_ETrackingModel eTrackingModel, boolean isClickButtonAdd){
        new TrackWaterElectricityWs().getAllFloorAndUnit( user.getActivePropertyIdFk() , eTrackingModel.getTrackingDate(),  action,ListWaterElectricityActivity.this,new TrackWaterElectricityWs.OnTrackWaterCallBack() {
            @Override
            public void onSuccess(String message , List<FloorUtilNoModel> list) {
                progressBar.setVisibility(View.GONE);
                onOpenClickUpload(eTrackingModel, list, isClickButtonAdd);
            }

            @Override
            public void onFail(String message) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(ListWaterElectricityActivity.this,message,Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void onOpenClickUpload(W_ETrackingModel trackingDate , List<FloorUtilNoModel> list, boolean isClickAdd){
        floorUtilNoModelList = new ArrayList<>();
        floorUtilNoModelList.addAll(list);
        Intent intent = new Intent(ListWaterElectricityActivity.this , UploadWaterElectricityActivity.class);

        if (isClickAdd){
            intent.putExtra("purpose",TypeTrackingWaterElectricity.UTILITY_PURPOSE_CREATE);
        } else {
            intent.putExtra("tracking",trackingDate);
            intent.putExtra("purpose",TypeTrackingWaterElectricity.UTILITY_PURPOSE_UPDATE);
        }
        intent.putExtra("action",action);
        intent.putExtra("list", (Serializable) floorUtilNoModelList);
        startActivityForResult(intent, REQUEST_UPLOAD_UTILITY);
    }

    private void saveUtility(HashMap<String,Object> hashMap){
        new TrackWaterElectricityWs().createWaterElectricUtility(ListWaterElectricityActivity.this, hashMap, onShowingMessage);
    }

    private final TrackWaterElectricityWs.OnShowingMessage onShowingMessage = new TrackWaterElectricityWs.OnShowingMessage() {
        @SuppressLint("SetTextI18n")
        @Override
        public void onSuccess(String message , String id) {
            isFinishSynData = true;
            if (isSyncData){ // check to set percent download
                float total = percentSyncData - percentOneSyncData;
                txtDownloadSync.setText(total + "");
                percentSyncData += percentSyncData;
                database.deleteRecordInFloorDetail(id);
            }
            if (isFinishLoop){ // clear data after finish sync
                checkHaveStoreDataInLocal();
                refreshList();
                progressSync.setVisibility(View.GONE);
                Toast.makeText(ListWaterElectricityActivity.this,message,Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onFail(String message) {
            progressSync.setVisibility(View.GONE);
            Toast.makeText(ListWaterElectricityActivity.this,message,Toast.LENGTH_SHORT).show();
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable @org.jetbrains.annotations.Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_UPLOAD_UTILITY && resultCode == RESULT_OK){
            try {
                floorUtilNoModelList = new ArrayList<>();
                if (data != null)   isClickSave = data.getBooleanExtra("is_clicked_save", true);
                if (isClickSave){   isClickSave = false;    refreshList(); }
                isGetDateFromLocalFirst = false;
                getAllFloorFromLocalDatabase();
                checkHaveStoreDataInLocal();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
