package com.eazy.daikou.ui.home.complaint_solution.complaint.adapter

import android.media.AudioAttributes
import android.media.MediaPlayer
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.SeekBar
import android.widget.SeekBar.OnSeekBarChangeListener
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.helper.DateUtil
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.complaint.Comment
import java.io.IOException
import java.util.concurrent.TimeUnit

class ComplaintMainReplyAdapter(private val list: ArrayList<Comment>, private val onClickListener: OnClickItemListener) :
    RecyclerView.Adapter<ComplaintMainReplyAdapter.ViewHolder>(){

    var mSeekBar: SeekBar? = null
    var mTvAudioLength: TextView? = null
    var mMediaPlayer: MediaPlayer? = null
    var seekHandler = Handler(Looper.getMainLooper())

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.hotel_reply_comment_item_model, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBindingView(list[position], onClickListener)

        val item = list[position]
        val mediaPlayer = MediaPlayer()
        // mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC)
        val urlVoice = Utils.validateNullValue(item.voice)
        try {
            mediaPlayer.setAudioAttributes(
                AudioAttributes.Builder()
                    .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                    .build()
            )
            mediaPlayer.setDataSource(urlVoice)
            mediaPlayer.prepare() // might take long for buffering.
        } catch (e: IOException) {
            e.printStackTrace()
        }
        mMediaPlayer = mediaPlayer
        mTvAudioLength = holder.maxSecondTv
        mSeekBar = holder.seekBar
        holder.seekBar.max = mediaPlayer.duration

        // run.run()
        holder.seekBar.setOnSeekBarChangeListener(object : OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                if (mediaPlayer != null && fromUser) {
                    mediaPlayer.seekTo(progress)
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {}
            override fun onStopTrackingTouch(seekBar: SeekBar) {}
        })
        holder.maxSecondTv.text = calculateDuration(mediaPlayer.duration)

        holder.playVoiceBtn.setOnClickListener {
            if (!mediaPlayer.isPlaying) {
                mediaPlayer.start()
                holder.playVoiceBtn.setImageDrawable(ContextCompat.getDrawable(holder.playVoiceBtn.context, R.drawable.ic_pause))
                run = Runnable {
                    // Update SeekBar every 100 miliseconds
                    holder.seekBar.progress = mediaPlayer.currentPosition
                    seekHandler.postDelayed(run, 100)
                    // For Showing time of audio(inside runnable)
                    val miliSeconds = mediaPlayer.currentPosition
                    if (miliSeconds != 0) {
                        // if audio is playing, showing current time
                        val minutes = TimeUnit.MILLISECONDS.toMinutes(miliSeconds.toLong())
                        val seconds = TimeUnit.MILLISECONDS.toSeconds(miliSeconds.toLong())
                        if (minutes == 0L) {
                            holder.maxSecondTv.text = String.format("%s : %s / %s", 0, seconds ,calculateDuration(mediaPlayer.duration))
                        } else {
                            if (seconds >= 60) {
                                val sec = seconds - minutes * 60
                                holder.maxSecondTv.text = String.format("%s : %s / %s", minutes, sec ,calculateDuration(mediaPlayer.duration))
                            }
                        }
                    } else {
                        val totalTime = mediaPlayer.duration
                        val minutes = TimeUnit.MILLISECONDS.toMinutes(totalTime.toLong())
                        val seconds = TimeUnit.MILLISECONDS.toSeconds(totalTime.toLong())
                        if (minutes == 0L) {
                            holder.maxSecondTv.text = String.format("%s : %s", 0, seconds)
                        } else {
                            if (seconds >= 60) {
                                val sec = seconds - minutes * 60
                                holder.maxSecondTv.text = String.format("%s : %s", minutes, sec)
                            }
                        }
                    }

                    // Call Back Listener
                    onClickListener.onCallBackListener(mediaPlayer)

                }
                run.run()
            } else {
                mediaPlayer.pause()
                holder.playVoiceBtn.setImageDrawable(ContextCompat.getDrawable(holder.playVoiceBtn.context, R.drawable.ic_play))
            }
        }

    }

    private var run: Runnable = object : Runnable {
        override fun run() {
            val seekHandler = Handler(Looper.getMainLooper())
            mSeekBar!!.progress = mMediaPlayer!!.currentPosition
            seekHandler.postDelayed(this, 100)
            val miliSeconds = mMediaPlayer!!.currentPosition
            if (miliSeconds != 0) {
                // if Audio is playing, showing current time;
                val minutes = TimeUnit.MILLISECONDS.toMinutes(miliSeconds.toLong())
                val seconds = TimeUnit.MILLISECONDS.toSeconds(miliSeconds.toLong())
                if (minutes == 0L) {
                    mTvAudioLength!!.text = String.format("%s : %s", 0, seconds)
                } else {
                    if (seconds >= 60) {
                        val sec = seconds - minutes * 60
                        mTvAudioLength!!.text = String.format("%s : %s", minutes, sec)
                    }
                }
            } else {
                // Displaying total time if audio not playing
                val totalTime = mMediaPlayer!!.duration
                val minutes = TimeUnit.MILLISECONDS.toMinutes(totalTime.toLong())
                val seconds = TimeUnit.MILLISECONDS.toSeconds(totalTime.toLong())
                if (minutes == 0L) {
                    mTvAudioLength!!.text = String.format("%s : %s", 0, seconds)
                } else {
                    if (seconds >= 60) {
                        val sec = seconds - minutes * 60
                        mTvAudioLength!!.text = String.format("%s : %s", minutes, sec)
                    }
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val descriptionTv: TextView = itemView.findViewById(R.id.descriptionTv)
        private val nameTv: TextView = itemView.findViewById(R.id.nameTv)
        private val profileImage: ImageView = itemView.findViewById(R.id.profile_image)
        private val imageComment: ImageView = itemView.findViewById(R.id.imageComment)
        private val durationTv: TextView = itemView.findViewById(R.id.durationTv)

        // Voice
        val playVoiceBtn : ImageView = itemView.findViewById(R.id.play_voice_button)
        val maxSecondTv : TextView = itemView.findViewById(R.id.maxSecondTv)
        val seekBar : SeekBar = itemView.findViewById(R.id.seekBar)

        fun onBindingView(item: Comment, onClickListener: OnClickItemListener){
            if (item.uploader_user != null){
                Utils.setValueOnText(nameTv, item.uploader_user!!.name)
                Glide.with(profileImage).load(if (item.uploader_user!!.image != null) item.uploader_user!!.image else R.drawable.ic_my_profile).into(profileImage)
            } else {
                Utils.setValueOnText(nameTv, ". . .")
                Glide.with(profileImage).load(R.drawable.ic_my_profile).into(profileImage)
            }

            if (item.comment != null){
                descriptionTv.text = item.comment
                descriptionTv.visibility = View.VISIBLE
            } else {
                descriptionTv.visibility = View.GONE
            }

            if (item.created_at != null){
                durationTv.text = DateUtil.getDurationDate(durationTv.context, item.created_at)
            } else {
                durationTv.visibility = View.GONE
            }

            // Image Display
            itemView.findViewById<CardView>(R.id.imageCardLayout).visibility = if (item.image != null) View.VISIBLE else View.GONE
            Glide.with(imageComment).load(if (item.image != null) item.image else R.drawable.no_image).into(imageComment)

            itemView.findViewById<CardView>(R.id.imageCardLayout).setOnClickListener(CustomSetOnClickViewListener{
                onClickListener.onClickListener(item, "view_image")
            })

            // Voice Display
            itemView.findViewById<LinearLayout>(R.id.voiceDisplayLayout).visibility = if (item.voice != null)   View.VISIBLE else View.GONE

            // Sub reply comment
            itemView.findViewById<TextView>(R.id.commentTV).visibility = View.GONE

            // Like
            itemView.findViewById<TextView>(R.id.txtLike).visibility = View.GONE
            itemView.findViewById<TextView>(R.id.txtTotalLike).visibility = View.GONE
        }
    }

    private fun calculateDuration(duration: Int): String {
        var finalDuration = ""
        val minutes: Long = TimeUnit.MILLISECONDS.toMinutes(duration.toLong())
        val seconds: Long = TimeUnit.MILLISECONDS.toSeconds(duration.toLong())
        if (minutes == 0L) {
            finalDuration = String.format("%s : %s", 0, seconds)
        } else {
            if (seconds >= 60) {
                val sec = seconds - minutes * 60
                finalDuration = String.format("%s : %s", minutes, sec)
            }
        }
        return finalDuration
    }

    interface OnClickItemListener {
        fun onClickListener(item : Comment, action : String)
        fun onClickVoiceListener(item : Comment, action : String, playVoiceBtn : ImageView, maxSecondTv : TextView, seekBar: SeekBar)
        fun onCallBackListener(mediaPlayer: MediaPlayer)
    }

    fun clear() {
        val size: Int = list.size
        if (size > 0) {
            for (i in 0 until size) {
                list.removeAt(0)
            }
            notifyItemRangeRemoved(0, size)
        }
    }

    fun clearMediaPlayer() {
        if (mMediaPlayer != null) {
            if (mMediaPlayer!!.isPlaying) {
                mMediaPlayer!!.stop()
            }
            mMediaPlayer!!.release()
            mMediaPlayer = null
        }
        seekHandler.removeCallbacks(run)
    }

}