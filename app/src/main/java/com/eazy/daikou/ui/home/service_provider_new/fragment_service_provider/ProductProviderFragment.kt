package com.eazy.daikou.ui.home.service_provider_new.fragment_service_provider

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseFragment
import com.eazy.daikou.ui.home.service_provider_new.activity.DetailListProviderActivity
import com.eazy.daikou.ui.home.service_provider_new.adapter_service_provider.ListServiceProviderAdapter

class ProductProviderFragment : BaseFragment() {

    private lateinit var recyclerView: RecyclerView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_product_provider, container, false)

        initView(view)

        initAction()

        return view
    }
    private fun initView(view : View){
        recyclerView = view.findViewById(R.id.recyclerListProvider)

    }

    private fun initAction(){
        val linearLayoutManager = LinearLayoutManager(mActivity)
        recyclerView.layoutManager = linearLayoutManager
        val listServiceProvider = ListServiceProviderAdapter(mActivity, callBackListener)
        recyclerView.adapter = listServiceProvider
    }

    private val callBackListener : ListServiceProviderAdapter.ClickItemCallBackListener = object : ListServiceProviderAdapter.ClickItemCallBackListener{
        override fun clickItemListener() {
            startActivity(Intent(mActivity, DetailListProviderActivity::class.java))
        }
    }
}