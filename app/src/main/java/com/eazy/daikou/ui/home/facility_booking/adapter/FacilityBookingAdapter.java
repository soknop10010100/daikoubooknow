package com.eazy.daikou.ui.home.facility_booking.adapter;


import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.eazy.daikou.R;
import com.eazy.daikou.model.facility_booking.FacilityBookingModel;

import java.util.ArrayList;
import java.util.List;

public class FacilityBookingAdapter extends RecyclerView.Adapter<FacilityBookingAdapter.ViewHolder>{

    private final Context context;
    private final ArrayList<FacilityBookingModel> facilityBookingOneArrayList;
    private final onClickCallbackList onClickCallbackList;

    public FacilityBookingAdapter(Context context, ArrayList<FacilityBookingModel> facilityBookingOneArrayList, FacilityBookingAdapter.onClickCallbackList onClickCallbackList) {
        this.context = context;
        this.facilityBookingOneArrayList = facilityBookingOneArrayList;
        this.onClickCallbackList = onClickCallbackList;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_facility_booking,parent,false);
        return new ViewHolder(view);
    }

    @SuppressLint("ResourceType")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        FacilityBookingModel facilityBooking = facilityBookingOneArrayList.get(position);
        if (facilityBooking != null){
            holder.titleName.setText(facilityBooking.getSpaceName() != null ? facilityBooking.getSpaceName() : "- - -");
            if (facilityBooking.getProperty() != null){
                holder.locationFacility.setText(facilityBooking.getProperty().getName() != null ?  facilityBooking.getProperty().getName() : "- - -");
            } else {
                holder.locationFacility.setText("- - -");
            }
            if (facilityBooking.getSpacePricing() != null) {
                if (facilityBooking.getSpacePricing().getPrice() != null) {
                    holder.priceFacility.setText("$ " + facilityBooking.getSpacePricing().getPrice());
                } else {
                    holder.priceFacility.setText("$ . . .");
                }
            } else {
                holder.priceFacility.setText("$ . . .");
            }

            holder.freeIconLayout.setVisibility(facilityBooking.isFree() ? View.VISIBLE : View.GONE);

            Glide.with(context).load(facilityBooking.getSpaceImages() != null ? facilityBooking.getSpaceImages() : R.drawable.no_image).into(holder.imageView);

            holder.itemView.setOnClickListener(v-> onClickCallbackList.onClickBackList(facilityBooking));

        }

    }

    @Override
    public int getItemCount() {
        return facilityBookingOneArrayList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        private final TextView titleName,locationFacility,priceFacility;
        private final ImageView imageView;
        private final RelativeLayout freeIconLayout;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageViewBooking);
            titleName = itemView.findViewById(R.id.titleName);
            locationFacility = itemView.findViewById(R.id.locationFacility);
            priceFacility = itemView.findViewById(R.id.priceFacility);
            freeIconLayout = itemView.findViewById(R.id.freeIconLayout);
        }
    }

    public void clear() {
        int size = facilityBookingOneArrayList.size();
        facilityBookingOneArrayList.clear();
        notifyItemRangeRemoved(0, size);
    }

    public interface onClickCallbackList{
        void onClickBackList(FacilityBookingModel facilityBooking);
    }
}
