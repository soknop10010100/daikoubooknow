package com.eazy.daikou.ui.home.book_now.booking_hotel

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AbsListView
import android.widget.ProgressBar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.base.MockUpData
import com.eazy.daikou.request_data.request.book_now_ws.BookingHotelWS
import com.eazy.daikou.request_data.request.book_now_ws.RequestHashMapData
import com.eazy.daikou.helper.GetDataUtils
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.booking_hotel.HotelBookingHistoryDetailModel
import com.eazy.daikou.model.booking_hotel.HotelBookingHistoryModel
import com.eazy.daikou.ui.home.book_now.booking_hotel.adapter.HistoryBookingHotelAdapter

class HotelBookingHistoryActivity : BaseActivity() {

    private lateinit var progressBar: ProgressBar
    private lateinit var recyclerView: RecyclerView
    private lateinit var historyBookingHotelAdapter : HistoryBookingHotelAdapter
    private var action = ""
    private var currentItem = 0
    private var total : Int = 0
    private var scrollDown : Int = 0
    private var currentPage : Int = 1
    private var size : Int = 10
    private var isScrolling = false
    private var userHotelId = ""
    private lateinit var refreshLayout: SwipeRefreshLayout
    private lateinit var linearLayoutManager : LinearLayoutManager
    private var historyHotelList : ArrayList<HotelBookingHistoryModel> = ArrayList()
    private var hotelId = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_booking_hotel_history)

        initView()

        initAction()
    }

    private fun initView(){
        progressBar = findViewById(R.id.progressItem)
        recyclerView = findViewById(R.id.recyclerView)
        refreshLayout = findViewById(R.id.swipe_layouts)
    }

    private fun initAction(){
        action = GetDataUtils.getDataFromString("action", this)
        hotelId = GetDataUtils.getDataFromString("id", this)
        userHotelId = MockUpData.getEazyHotelUserId(UserSessionManagement(this))

        Utils.customOnToolbar(this, if (action == "hotel_history_hotel") resources.getString(R.string.my_booking_history) else resources.getString(R.string.booking_report) ){onBackPressed()}

        linearLayoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = linearLayoutManager

        initRecyclerView()

        onScrollRecyclerView()

        refreshLayout.setColorSchemeResources(R.color.colorPrimary)
        refreshLayout.setOnRefreshListener { refreshList() }

        if (action.equals("hotel_booking_notification", ignoreCase = true)) {
            startDetailHotel(hotelId)
        }

    }

    private fun refreshList(){
        currentPage = 1
        size = 10
        isScrolling = true

        historyBookingHotelAdapter.clear()
        requestDataHistory()
    }

    private fun requestDataHistory(){
        progressBar.visibility = View.VISIBLE
        BookingHotelWS().getHistoryHotelBooking(this, action, RequestHashMapData.hashMapHistoryData(currentPage, userHotelId), requestServiceWsListener)
    }

    private val requestServiceWsListener = object : BookingHotelWS.OnCallBackHistoryHotelListener{
            @SuppressLint("NotifyDataSetChanged")
            override fun onSuccessHistoryHotel(hotelHistoryList: ArrayList<HotelBookingHistoryModel>) {
                progressBar.visibility = View.GONE
                refreshLayout.isRefreshing = false
                historyHotelList.addAll(hotelHistoryList)

                historyBookingHotelAdapter.notifyDataSetChanged()

                Utils.validateViewNoItemFound(this@HotelBookingHistoryActivity, recyclerView, historyHotelList.size <= 0)
            }

            override fun onSuccessHistoryDetailHotel(hotelHistory: HotelBookingHistoryDetailModel) {
                // Get History Detail
            }

            override fun onFailed(message: String) {
                progressBar.visibility = View.GONE
                refreshLayout.isRefreshing = false
                Utils.customToastMsgError(this@HotelBookingHistoryActivity, message, false)
            }
    }

    private fun initRecyclerView(){
        historyBookingHotelAdapter = HistoryBookingHotelAdapter(this, historyHotelList, object : HistoryBookingHotelAdapter.OnClickCallBackLister{
            override fun onClickCallBack(hotelBookingHistoryModel: HotelBookingHistoryModel) {
                startDetailHotel(hotelBookingHistoryModel.id!!)
            }

        })
        recyclerView.adapter = historyBookingHotelAdapter

        requestDataHistory()
    }

    private fun startDetailHotel(hotelId : String){
        val intent = Intent(this@HotelBookingHistoryActivity, HotelHistoryDetailActivity::class.java)
        intent.putExtra("hotel_id", hotelId)
        startActivity(intent)
    }

    private fun onScrollRecyclerView() {
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true
                }
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                currentItem = linearLayoutManager.childCount
                total = linearLayoutManager.itemCount
                scrollDown = linearLayoutManager.findFirstVisibleItemPosition()
                if (dy > 0) {
                    if (isScrolling && currentItem + scrollDown == total) {
                        if (total == size) {
                            isScrolling = false
                            currentPage++
                            progressBar.visibility = View.VISIBLE
                            recyclerView.scrollToPosition(historyHotelList.size - 1)
                            requestDataHistory()
                            size += 10
                        } else {
                            progressBar.visibility = View.GONE
                        }
                    }
                }
            }
        })
    }


    override fun onBackPressed() {
        super.onBackPressed()
        if (action == "from_complete_payment" || action =="hotel_booking_notification"){
            val intent = Intent(this, HotelBookingMainActivity::class.java)
            startActivity(intent)
        }
        finish()
    }

}