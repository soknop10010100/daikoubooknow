package com.eazy.daikou.ui.home.inspection_work_order

import android.annotation.SuppressLint
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.base.MockUpData
import com.eazy.daikou.request_data.request.inspection_ws.InspectionWS
import com.eazy.daikou.request_data.request.inspection_ws.InspectionWS.CallBackUpdateInspectionListener
import com.eazy.daikou.request_data.static_data.StaticUtilsKey
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.helper.Utils.setTintColorOnImageStepProgress
import com.eazy.daikou.model.inspection.*
import com.eazy.daikou.model.inspection.ItemTemplateModel.SubItemTemplateModel
import com.eazy.daikou.model.my_property.service_provider.SelectPropertyModel
import com.eazy.daikou.model.my_unit_info.UnitModel
import com.eazy.daikou.model.profile.User
import com.eazy.daikou.ui.home.inspection_work_order.adapter.ItemInspectorAdapter
import com.eazy.daikou.ui.home.inspection_work_order.adapter.SelectInspectionAdapter
import com.eazy.daikou.ui.home.inspection_work_order.adapter.SelectInspectorAdapter
import com.eazy.daikou.ui.home.inspection_work_order.adapter.TemplateFormAdapter
import com.eazy.daikou.helper.DateNumPickerFragment
import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog
import com.google.gson.Gson
import libs.mjn.prettydialog.PrettyDialog
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class ChooseInspectionTypeActivity : BaseActivity(), View.OnClickListener, SelectUnitNoAndClerkFragment.OnClickItemListener{

    private var recyclerView : RecyclerView?= null
    private lateinit var progressBar : ProgressBar
    private lateinit var btnNext : ImageView
    private lateinit var selectInspectionAdapter : SelectInspectionAdapter
    private lateinit var templateInspectionAdapter : TemplateFormAdapter
    private var layoutBtnNext : LinearLayout ?= null
    private var actionBtnLayout : RelativeLayout ?= null
    private lateinit var btnSave: LinearLayout
    private lateinit var btnAddNew : LinearLayout
    private lateinit var menuMainTemplate : LinearLayout
    private lateinit var templateMenuTv : TextView
    private lateinit var infoTemplateMenuTv : TextView
    private lateinit var cardViewTemplate : CardView
    private lateinit var unitNoTv : TextView
    private lateinit var dateTv : TextView
    private lateinit var timeTv : TextView
    private lateinit var duringTv : TextView
    private lateinit var clerkTv : TextView
    private lateinit var userTypeTv : TextView
    private lateinit var inspectionNameEdt : EditText
    private lateinit var unitNoLayout : RelativeLayout
    private lateinit var calendarLayout : LinearLayout
    private lateinit var calendarView: CalendarView
    private var linearLayoutManager: LinearLayoutManager? = null
    private lateinit var backUpItemTemplateModel : ItemTemplateModel
    private lateinit var relativeLayout : RelativeLayout
    private lateinit var stepLayout : RelativeLayout
    private lateinit var tf : SimpleDateFormat

    private var itemTemplateAPIModelList : ArrayList<ItemTemplateAPIModel> = ArrayList()
    private var inspectionList : ArrayList<ItemInspectionModel> = ArrayList()
    private val listAddForm : ArrayList<ItemTemplateModel> = ArrayList()
    private val inspectorList : ArrayList<InspectorModel> = ArrayList()

    private val REQUEST_BACK_INSPECTION_FORM = 658
    private val REQUEST_SCHEDULE_TEMPLATE = 758
    private val REQUEST_EDIT_ITEM = 958
    private var count : Int = 1
    private var action : String =""
    private var action_type_detail : String =""
    private var selectConductDate : String = ""
    private var selectConductTime : String = ""
    private var selectDuring : String = "30"
    private var selectUnitNoId : String = ""
    private var selectEmployeeId : String = ""
    private var selectUserType : String = ""
    private var isUnitNo : Boolean = true
    private var isUnitInspectionClick : Boolean = false

    private  var templateTypeId : String = ""
    private var getInspectionId : String = ""
    private var getInspectionName : String = ""
    private var getTemplateId : String = ""
    private var currentItem = 0
    private  var total : Int = 0
    private  var scrollDown : Int = 0
    private  var currentPage : Int = 1
    private  var size : Int = 16
    private var isScrolling = false
    private var inspectionIdTemplateSave = ""
    //select inspector
    private lateinit var inspectorRecycle : RecyclerView
    private lateinit var inspectorIcon : ImageView
    private lateinit var layoutInspector : LinearLayout
    private val inspectorsIdList : ArrayList<InspectorModel> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.acitivity_choose_inspection_form)

        Utils.changeStatusBarColor(ContextCompat.getColor(this, R.color.appBarColor), window)

        initView()

        initData()

        initAction()

    }

    private fun initView(){
        findViewById<ImageView>(R.id.iconBack).setOnClickListener{onBackFinish()}

        recyclerView = findViewById(R.id.inspectionRecycle)
        progressBar = findViewById(R.id.progressItem)
        btnNext = findViewById(R.id.btnNext)
        layoutBtnNext = findViewById(R.id.layoutBtnNext)
        actionBtnLayout = findViewById(R.id.actionBtnLayout)
        btnSave = findViewById(R.id.btnSave)
        btnAddNew = findViewById(R.id.btnAddNewRecord)

        cardViewTemplate = findViewById(R.id.selectTemplateLayout)
        dateTv = findViewById(R.id.conductDateTv)
        timeTv = findViewById(R.id.conductTimeTv)
        duringTv = findViewById(R.id.conductDuringTv)
        unitNoTv = findViewById(R.id.unitNoTv)
        inspectionNameEdt = findViewById(R.id.inspectionNameTv)
        clerkTv = findViewById(R.id.clerkTv)
        unitNoLayout = findViewById(R.id.unitNoLayout)
        calendarLayout = findViewById(R.id.calendarLayout)
        calendarView  = findViewById(R.id.date_picker)
        userTypeTv = findViewById(R.id.userTypeTv)

        templateMenuTv = findViewById(R.id.templateLayoutTv)
        infoTemplateMenuTv = findViewById(R.id.infoTemplateLayoutTv)
        menuMainTemplate = findViewById(R.id.menuMainTemplate)

        relativeLayout = findViewById(R.id.relativeLayout)
        stepLayout = findViewById(R.id.stepLayout)
        inspectorRecycle = findViewById(R.id.inspectorRecycle)
        inspectorIcon = findViewById(R.id.inspectorIcon)
        layoutInspector = findViewById(R.id.layoutInspector)
    }

    private fun initData(){
        if(intent != null && intent.hasExtra("inspection_list")) {
            inspectionList = intent.getSerializableExtra("inspection_list") as ArrayList<ItemInspectionModel>
        }
        if(intent != null && intent.hasExtra("action")) {
            action = intent.getStringExtra("action").toString()
        }
        if(intent != null && intent.hasExtra("action_type")) {
            action_type_detail = intent.getStringExtra("action_type").toString()
        }
        if(intent != null && intent.hasExtra("inspection_type_id")) {
            getInspectionId = intent.getStringExtra("inspection_type_id").toString()
        }
        if(intent != null && intent.hasExtra("inspection_type_name")) {
            getInspectionName = intent.getStringExtra("inspection_type_name").toString()
        }

        //In Inspection Form
        if(intent != null && intent.hasExtra("template_id")) {
            getTemplateId = intent.getStringExtra("template_id").toString()
        }
        if(intent != null && intent.hasExtra("inspection_template_id")) {
            inspectionIdTemplateSave = intent.getStringExtra("inspection_template_id").toString()
        }

        // Set default user type
        userTypeTv.text = resources.getString(R.string.owner)
        selectUserType = "owner"

        //From Scan My Unit Activity
        if(intent != null && intent.hasExtra("my_unit_info")){
            val myUnitInfo = intent.getSerializableExtra("my_unit_info") as UnitModel
            setDefaultValueFromScan(myUnitInfo)
        }
    }

    private fun setDefaultValueFromScan(myUnitInfo : UnitModel){
        selectUnitNoId = myUnitInfo.unit_id!!
        unitNoTv.setBackgroundResource(R.drawable.shape_transparent_bg)
        unitNoTv.text = myUnitInfo.unit_no
        unitNoTv.isEnabled = false
        val labelUnitNoTv : TextView = findViewById(R.id.labelUnitNoTv)
        val labelUserTypeTv : TextView = findViewById(R.id.labelUserTypeTv)
        labelUnitNoTv.setBackgroundResource(R.color.transparent)

        selectUserType = if (myUnitInfo.current_owner_tenant != null) myUnitInfo.current_owner_tenant!! else ""
        when (selectUserType) {
            "owner" -> {
                userTypeTv.text = resources.getString(R.string.owner)
                userTypeTv.setBackgroundResource(R.drawable.shape_transparent_bg)
                labelUserTypeTv.setBackgroundResource(R.color.transparent)
                userTypeTv.isEnabled = false
            }
            "tenant" -> {
                userTypeTv.text = resources.getString(R.string.tenant)
                userTypeTv.setBackgroundResource(R.drawable.shape_transparent_bg)
                labelUserTypeTv.setBackgroundResource(R.color.transparent)
                userTypeTv.isEnabled = false
            }
            else -> {
                userTypeTv.text = resources.getString(R.string.owner)
                selectUserType = "owner"
            }
        }
    }

    private fun initAction(){

        linearLayoutManager = LinearLayoutManager(this@ChooseInspectionTypeActivity)
        recyclerView!!.layoutManager = linearLayoutManager

        checkStepProgress(action)

        when {
            // Step 3 After Click Btn Next From Select Template And Schedule List
            action.equals(StaticUtilsKey.form_template_type, ignoreCase = true) -> {
                layoutBtnNext!!.visibility = View.GONE
                actionBtnLayout!!.visibility = View.VISIBLE
                initRecycleViewTemplateForm()

                if (action_type_detail == "action_detail") {
                    // onScrollTemplateRecycle()
                }

                // val scrollView : ScrollView = findViewById(R.id.mainLayoutScroll)
                val relativeParams = relativeLayout.layoutParams as RelativeLayout.LayoutParams
                relativeParams.bottomMargin = 0
                relativeLayout.layoutParams = relativeParams

            }

            // Step 2 After Click Btn Next From Select Inspection Type List
            action.equals(StaticUtilsKey.template_type, ignoreCase = true) -> {
                findViewById<CardView>(R.id.selectTemplateLayout).visibility = View.GONE
                menuMainTemplate.visibility = View.VISIBLE
                relativeLayout.visibility = View.VISIBLE

                initRecycleView()

                requestServiceTemplate()

                onScrollTemplateRecycle()

                initRecycleViewItemInspector()
            }

            // Step 1 After Click Btn Add From Inspection List
            else -> {
                progressBar.visibility = View.GONE
                initRecycleView()
            }
        }

        btnNext.setOnClickListener{ actionClickOnBtnNext() }

        // ********** Implement On Choose Template ****************
        if (getInspectionName != "" && getInspectionName.contains("Unit Inspection")){
            isUnitInspectionClick = true
            onClickMenuTemplateInspection(isGeneralInspection = false, isTemplateMenuSelect = false, isMainMenu = false)
        } else{
            onClickMenuTemplateInspection(isGeneralInspection = true, isTemplateMenuSelect = false, isMainMenu = false)
            isUnitInspectionClick = false
        }

        templateMenuTv.setOnClickListener{
            onClickMenuTemplateInspection(
                isGeneralInspection = false,
                isTemplateMenuSelect = true,
                isMainMenu = true
            )
        }

        infoTemplateMenuTv.setOnClickListener{
            onClickMenuTemplateInspection(
                isGeneralInspection = false,
                isTemplateMenuSelect = false,
                isMainMenu = true
            )
        }

        setDefaultValueSchedule()

        calendarView.setOnDateChangeListener { _: CalendarView?, year: Int, month: Int, dayOfMonth: Int ->
            selectConductDate = year.toString() + "-" + (month + 1) + "-" + dayOfMonth
            dateTv.text = Utils.formatDateFromString(
                    "yyyy-MM-dd",
                    "dd, MMM yyyy",
                    selectConductDate
            )
        }
        calendarView.minDate = System.currentTimeMillis() - 1000

        dateTv.setOnClickListener(this)
        timeTv.setOnClickListener(this)
        duringTv.setOnClickListener(this)
        unitNoTv.setOnClickListener(this)
        clerkTv.setOnClickListener(this)
        userTypeTv.setOnClickListener(this)
        layoutInspector.setOnClickListener(this)

        // ************** Implement On Create Inspection Form ******************
        btnSave.setOnClickListener{ hashMapDataSaveInspection() }

        btnAddNew.setOnClickListener{ customPrettyDialog(PrettyDialog(this)) }

    }

    private fun onClickMenuTemplateInspection(isGeneralInspection : Boolean, isTemplateMenuSelect : Boolean, isMainMenu : Boolean){
        if (isMainMenu){
            if (isTemplateMenuSelect){
                templateMenuTv.setBackgroundResource(R.drawable.custom_card_view_color_menu)
                templateMenuTv.setTextColor(Utils.getColor(this, R.color.white))
                infoTemplateMenuTv.background = null
                infoTemplateMenuTv.setTextColor(Utils.getColor(this, R.color.gray))
            } else {
                templateMenuTv.background = null
                templateMenuTv.setTextColor(Utils.getColor(this, R.color.gray))
                infoTemplateMenuTv.setBackgroundResource(R.drawable.custom_card_view_color_menu)
                infoTemplateMenuTv.setTextColor(Utils.getColor(this, R.color.white))
            }

            findViewById<CardView>(R.id.selectTemplateLayout).visibility = if (isTemplateMenuSelect) View.GONE else View.VISIBLE
            relativeLayout.visibility = if (isTemplateMenuSelect) View.VISIBLE else View.GONE
        } else{
             unitNoLayout.visibility = if (isGeneralInspection) View.GONE else View.VISIBLE
        }
    }

    private fun actionClickOnBtnNext(){
        val intent = Intent(this@ChooseInspectionTypeActivity, ChooseInspectionTypeActivity::class.java)

        when {
            // Step 1 After Click Btn Next Select Inspection Type List
            action.equals(StaticUtilsKey.inspection_type, ignoreCase = true) -> {
                intent.putExtra("action", StaticUtilsKey.template_type)
                intent.putExtra("inspection_type_id", getInspectionId)
                intent.putExtra("inspection_type_name", getInspectionName)
                if (getInspectionId == "") {
                    Toast.makeText(this@ChooseInspectionTypeActivity, "Please, Select Inspection Type !", Toast.LENGTH_SHORT).show()
                } else {
                    startActivityForResult(intent, REQUEST_SCHEDULE_TEMPLATE)
                }
            }

            // Step 2 After Click Btn Next Select Template Type List
            action.equals(StaticUtilsKey.template_type, ignoreCase = true) -> { checkValidate() }

            else -> {
                Utils.customToastMsgError(this@ChooseInspectionTypeActivity, resources.getString(R.string.please_file_the_all_fields), false)
            }
        }
    }

    private fun checkValidate(){
        val msg: String
        if(cardViewTemplate.visibility == View.GONE){
            onClickMenuTemplateInspection(isGeneralInspection = true, isTemplateMenuSelect = false, isMainMenu = true)
        } else {
            when {
                selectUserType == "" && isUnitInspectionClick -> {
                    msg = resources.getString(R.string.please_select_inspector_type)
                    Utils.customToastMsgError(this@ChooseInspectionTypeActivity, msg, false)
                    userTypeTv.setBackgroundResource(R.drawable.shape_error)
                }
                selectUnitNoId == "" && isUnitInspectionClick -> {
                    msg = resources.getString(R.string.please_select_unit_no)
                    Utils.customToastMsgError(this@ChooseInspectionTypeActivity, msg, false)
                    unitNoTv.setBackgroundResource(R.drawable.shape_error)
                }
                inspectorsIdList.size == 0 && isUnitInspectionClick ->{
                    msg = resources.getString(R.string.please_select_inspector)
                    Utils.customToastMsgError(this@ChooseInspectionTypeActivity, msg, false)
                    layoutInspector.setBackgroundResource(R.drawable.shape_error)
                }
                inspectionNameEdt.text.toString().trim() == "" -> {
                    msg = resources.getString(R.string.please_write_inspection_name)
                    Utils.customToastMsgError(this@ChooseInspectionTypeActivity, msg, false)
                    inspectionNameEdt.setBackgroundResource(R.drawable.shape_error)
                }
                selectEmployeeId == "" -> {
                    msg = resources.getString(R.string.please_select_clerk)
                    Utils.customToastMsgError(this@ChooseInspectionTypeActivity, msg, false)
                    clerkTv.setBackgroundResource(R.drawable.shape_error)
                }
                else ->{
                    progressBar.visibility = View.VISIBLE
                    btnNext.isClickable = false
                    InspectionWS().saveTemplateInspectionMaintenanceSchedule(this@ChooseInspectionTypeActivity, StaticUtilsKey.isActionMenuHome, hashMapDataSaveTemplateSchedule(), callBackListListener)

                }
            }
        }
    }

    private fun checkStepProgress(typeAction : String){
        setTintColorOnImageStepProgress(findViewById(R.id.step1Img),  findViewById(R.id.step2Img), findViewById(R.id.step3Img), findViewById(R.id.titleToolbar), typeAction,this@ChooseInspectionTypeActivity)
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun initRecycleView(){
        for (selectPropertyGuest in inspectionList) {
            selectPropertyGuest.isClick = selectPropertyGuest.id.equals(getInspectionId)
        }
        selectInspectionAdapter = SelectInspectionAdapter(inspectionList, action){ inspection ->
            for (selectPropertyGuest in inspectionList) {
                selectPropertyGuest.isClick = inspection.id == selectPropertyGuest.id
            }
            if (action == StaticUtilsKey.inspection_type) {
                getInspectionId = inspection.id
                getInspectionName = inspection.inspection_name
            } else {
                templateTypeId = inspection.id
                inspectionNameEdt.setText(if(inspection.templateName != null) inspection.templateName else "")
            }
            selectInspectionAdapter.notifyDataSetChanged()
        }
        recyclerView?.adapter = selectInspectionAdapter
    }

    private fun initRecycleViewTemplateForm(){
        progressBar.visibility = View.VISIBLE

        templateInspectionAdapter = TemplateFormAdapter(this@ChooseInspectionTypeActivity, listAddForm, onClickCallBack)
        recyclerView?.adapter = templateInspectionAdapter

        when {
            action_type_detail == "action_detail" -> {
                requestServiceListByInspection()

                btnSave.isClickable = false
                btnSave.isEnabled = false
                btnSave.alpha = 0.2F

            } action == StaticUtilsKey.form_template_type -> {
                requestServiceListTemplateForm(getTemplateId)
            }
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun addListAction(itemInspectionModelList: ArrayList<ItemTemplateAPIModel>){
        var i = 1
        val hashMap = HashMap<String, List<SubItemTemplateModel>>()

        for (itemInspectionAPI in itemInspectionModelList) {
            val itemTemplateModel = ItemTemplateModel( i.toString(), "AA$i", itemInspectionAPI.optionType)
            itemTemplateModel.itemTemplateAPIModel = itemInspectionAPI
            itemTemplateModel.main_category_inspection = if (itemInspectionAPI.inspectionTitle != null) itemInspectionAPI.inspectionTitle else ""

            itemTemplateModel.isAlreadySave = !StaticUtilsKey.isActionSavedTemplate

            val listSub: ArrayList<SubItemTemplateModel> = ArrayList()

            var subIndex = 1
            for (itemInspectionModel in itemInspectionAPI.inspectionSubTitle){
                when (itemInspectionAPI.optionType) {
                    StaticUtilsKey.detail_button_key -> {
                        val subItemTemplateModel = SubItemTemplateModel(count.toString() + itemTemplateModel.value,
                            "BB$count", itemInspectionAPI.optionType, "", "", "", false
                        )

                        subItemTemplateModel.item_room_id = itemInspectionModel.roomItemId

                        if (itemInspectionModel.itemNo != null) {
                            subItemTemplateModel.item_no = itemInspectionModel.itemNo
                        } else {
                            subItemTemplateModel.item_no = "$i.$subIndex"
                        }

                        subItemTemplateModel.sub_item_category = if (itemInspectionModel.itemName != null) itemInspectionModel.itemName else ""
                        subItemTemplateModel.item_description = if (itemInspectionModel.itemDesc != null) itemInspectionModel.itemDesc else ""
                        subItemTemplateModel.item_condition = if (itemInspectionModel.itemCondition != null) itemInspectionModel.itemCondition else ""

                        subItemTemplateModel.reportType = itemInspectionModel.reportType
                        subItemTemplateModel.workOrderNoDefault = itemInspectionModel.workOrderNoDefault
                        subItemTemplateModel.workOrderSubjectDefault = itemInspectionModel.workOrderSubjectDefault

                        listSub.add(subItemTemplateModel)

                    }
                    StaticUtilsKey.simplify_button_key -> {
                        val itemCondition = itemInspectionModel.itemCondition.split(";").toTypedArray()
                        val subItemTemplateModel = SubItemTemplateModel(count.toString() + itemTemplateModel.value,
                            "BB$count", itemInspectionAPI.optionType, itemCondition[0].replace(" ", ""), itemCondition[1].replace(" ", ""), itemCondition[2].replace(" ", ""), false
                        )
                        subItemTemplateModel.item_room_id = itemInspectionModel.roomItemId

                        if (itemInspectionModel.itemNo != null) {
                            subItemTemplateModel.item_no = itemInspectionModel.itemNo
                        } else {
                            subItemTemplateModel.item_no = "$i.$subIndex"
                        }

                        if (itemInspectionModel.itemName != null)
                            subItemTemplateModel.sub_item_category = itemInspectionModel.itemName

                        subItemTemplateModel.reportType = itemInspectionModel.reportType
                        subItemTemplateModel.workOrderNoDefault = itemInspectionModel.workOrderNoDefault
                        subItemTemplateModel.workOrderSubjectDefault = itemInspectionModel.workOrderSubjectDefault

                        listSub.add(subItemTemplateModel)
                    }
                    else -> {
                        val  subItemTemplateModel : SubItemTemplateModel = if (itemInspectionModel.itemCondition != null){
                            SubItemTemplateModel(count.toString() + itemTemplateModel.value,
                                "BB$count", itemInspectionAPI.optionType, itemInspectionModel.itemCondition, "","", false
                            )
                        } else{
                            SubItemTemplateModel(count.toString() + itemTemplateModel.value,
                                "BB$count", itemInspectionAPI.optionType, "", "","", false
                            )
                        }
                        subItemTemplateModel.item_room_id = itemInspectionModel.roomItemId
                        if (itemInspectionModel.itemName != null)
                            subItemTemplateModel.sub_item_category = itemInspectionModel.itemName

                        if (itemInspectionModel.itemNo != null) {
                            subItemTemplateModel.item_no = itemInspectionModel.itemNo
                        } else {
                            subItemTemplateModel.item_no = "$i.$subIndex"
                        }

                        subItemTemplateModel.reportType = itemInspectionModel.reportType
                        subItemTemplateModel.workOrderNoDefault = itemInspectionModel.workOrderNoDefault
                        subItemTemplateModel.workOrderSubjectDefault = itemInspectionModel.workOrderSubjectDefault

                        listSub.add(subItemTemplateModel)
                    }
                }
                subIndex++

            }

            hashMap[itemTemplateModel.value] = listSub
            MockUpData.isRemove = true
            MockUpData.setListSubInspection(hashMap)
            listAddForm.add(itemTemplateModel)
            i++
            count++
        }

        templateInspectionAdapter.notifyDataSetChanged()

    }

    private var onClickCallBack = object : TemplateFormAdapter.ClickCallBackListener {
        override fun onClickDeleteCallBack(itemTemplateModel: ItemTemplateModel) {
            backUpItemTemplateModel = itemTemplateModel

            if(itemTemplateModel.isAlreadySave){
                AlertDialog.Builder(this@ChooseInspectionTypeActivity)
                    .setTitle(resources.getString(R.string.confirm))
                    .setMessage(resources.getString(R.string.are_you_sure_to_delete))
                    .setPositiveButton(resources.getString(R.string.yes)) { dialog: DialogInterface, _: Int ->
                        deleteInspectionList(StaticUtilsKey.main_key, itemTemplateModel.itemTemplateAPIModel.inspectionRoomId)
                        dialog.dismiss()
                    }
                    .setNegativeButton(
                        resources.getString(R.string.no)
                    ) { dialog: DialogInterface, _: Int -> dialog.dismiss() }
                    .setCancelable(false)
                    .setIcon(ResourcesCompat.getDrawable(resources, R.drawable.ic_report_problem, null))
                    .show()
            } else{
                if (listAddForm.size > 0) {
                    listAddForm.remove(itemTemplateModel)
                    MockUpData.isRemove = true
                   initRecycleViewInspection()
                }
            }
        }

        override fun onClickCameraCallBack(itemTemplateModel: ItemTemplateModel) {
            val intent = Intent(this@ChooseInspectionTypeActivity, AddImageInspectionActivity::class.java)
            intent.putExtra("value", itemTemplateModel.value)
            intent.putExtra("action", "main_action")
            intent.putExtra("item_model_main", itemTemplateModel)
            startActivity(intent)

        }

        override fun onClickSubCameraCallBack(itemTemplateModel: SubItemTemplateModel?, uniqueKey : String) {
            val intent = Intent(this@ChooseInspectionTypeActivity, AddImageInspectionActivity::class.java)
            intent.putExtra("value", uniqueKey)
            intent.putExtra("action", "sub_category_action")
            intent.putExtra("item_model", itemTemplateModel)
            startActivity(intent)

        }

        override fun onWriteCategoryItem(itemTemplateModel: ItemTemplateModel?, write_text: String?, position : Int) {
            itemTemplateModel!!.main_category_inspection = write_text
            setButtonEnable()
        }

        override fun onEditItemInspectionItem(itemTemplateModel: ItemTemplateModel?) {
            val intent = Intent(this@ChooseInspectionTypeActivity, EditItemInspectionActivity::class.java)
            intent.putExtra("listItemInspectionModel", listAddForm)
            intent.putExtra("itemInspectionModel", itemTemplateModel)
            startActivityForResult(intent, REQUEST_EDIT_ITEM)
        }

    }

    private fun initRecycleViewInspection(){
        templateInspectionAdapter = TemplateFormAdapter(this@ChooseInspectionTypeActivity, listAddForm, onClickCallBack)
        recyclerView?.adapter = templateInspectionAdapter
    }

    @SuppressLint("Range")
    private fun setButtonEnable(){
        btnSave.isClickable = isEnableButton(listAddForm)
        btnSave.isEnabled = isEnableButton(listAddForm)
        if (isEnableButton(listAddForm)){
            btnSave.alpha = 100F
        } else{
            btnSave.alpha = 0.2F
        }
    }

    private fun isEnableButton(list : List<ItemTemplateModel>) : Boolean{
        for(itemTemplate in list){
            if (itemTemplate.main_category_inspection == null || itemTemplate.main_category_inspection == ""){
                return false
            }
        }
        return true
    }

    override fun onBackPressed() {
        onBackFinish()
    }

    private fun onBackFinish(){
        when (action) {
            StaticUtilsKey.form_template_type -> {
                StaticUtilsKey.noteId = ""
                StaticUtilsKey.is_check_key = ""
                val intent = Intent()
                intent.putExtra("action", action)
                setResult(RESULT_OK, intent)
                finish()
            } else -> {
                finish()
            }
        }
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.conductDateTv -> {
                if (calendarLayout.visibility == View.VISIBLE) {
                    dateTv.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_drop_down, 0)
                    calendarLayout.visibility = View.GONE
                } else {
                    dateTv.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.right_arrow_back, 0)
                    calendarLayout.visibility = View.VISIBLE
                }
            }
            R.id.conductTimeTv ->{
                SingleDateAndTimePickerDialog.Builder(this@ChooseInspectionTypeActivity)
                    .minutesStep(15)
                    .bottomSheet()
                    .curved()
                    .displayMinutes(true)
                    .displayHours(true)
                    .displayDays(false)
                    .displayMonth(false)
                    .displayYears(false)
                    .displayAmPm(false)
                    .displayDaysOfMonth(false)
                    .listener { date: Date? ->
                        selectConductTime = date?.let { tf.format(it) }.toString()
                        timeTv.text = selectConductTime
                    }.display()
            }
            R.id.conductDuringTv ->{
                selectTime("time")
            }
            R.id.userTypeTv ->{
                selectTime("inspector_type")
            }
            R.id.unitNoTv ->{
                if(selectUserType != "") {
                    isUnitNo = true
                    selectUnitNo()
                } else {
                    Utils.customToastMsgError(this@ChooseInspectionTypeActivity, resources.getString(R.string.please_select_inspector_type), false)
                    userTypeTv.setBackgroundResource(R.drawable.shape_error)
                }
            }
            R.id.clerkTv ->{
                isUnitNo = false
                selectUnitNo()
            }
            R.id.layoutInspector ->{
                if (inspectorList.size > 0) {
                    startSelectInspector()
                } else {
                    selectInspector()
                }
            }
        }
    }

    private fun selectUnitNo(){
        when {
            isUnitNo -> {
                val bttSheet = SelectUnitNoAndClerkFragment().newInstance(selectUnitNoId, selectUserType, StaticUtilsKey.unit_no_action)
                bttSheet.show(supportFragmentManager, "fragment")
            }
            else -> {
                val bttSheet = SelectUnitNoAndClerkFragment().newInstance(selectEmployeeId, "",StaticUtilsKey.department_action)
                bttSheet.show(supportFragmentManager, "fragment")
            }
        }
    }


    @SuppressLint("SetTextI18n")
    private fun setDefaultValueSchedule() {
        val dfServer = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        dateTv.text = SimpleDateFormat("dd, MMM yyyy", Locale.getDefault()).format(Date())
        selectConductDate = dfServer.format(Date())

        tf = SimpleDateFormat("kk:mm", Locale.getDefault())
        selectConductTime = "01:15"
        timeTv.text = selectConductTime

        duringTv.text = selectDuring + " " + resources.getString(R.string.minutes).lowercase()
    }

    private fun selectTime(action : String) {
        val monthDialog = DateNumPickerFragment.newInstance(action)
        monthDialog.CallBackListener { title, value, id ->
            if(title.equals("inspector_type")){
                selectUserType = value
                userTypeTv.text = selectUserType
                selectUserType = if(value == resources.getString(R.string.owner)){
                    "owner"
                } else{
                    "tenant"
                }

                unitNoTv.text = ""
                selectUnitNoId = ""
                inspectorsIdList.clear()
                inspectorList.clear()
                itemInspectorAdapter.notifyDataSetChanged()
                userTypeTv.setBackgroundResource(R.drawable.shape_transparent_bg)
            } else {
                selectDuring = id
                duringTv.text = value
            }

        }
        monthDialog.show(supportFragmentManager, "dialog_fragment")
    }

    override fun onClickSelect(selectPropertyGuest: SelectPropertyModel) {
        if(isUnitNo) {
            unitNoTv.text = selectPropertyGuest.unitNo
            if (selectPropertyGuest.id != null) selectUnitNoId = selectPropertyGuest.id
            unitNoTv.setBackgroundResource(R.drawable.shape_transparent_bg)

            inspectorsIdList.clear()
            inspectorList.clear()
            itemInspectorAdapter.notifyDataSetChanged()
        } else {
            if (selectPropertyGuest.propertyId != null) selectEmployeeId = selectPropertyGuest.propertyId
            clerkTv.text = selectPropertyGuest.departmentName
        }
    }

    override fun onClickSelectEmployee(selectEmployee: Author_ClerkUserInfo) {
        clerkTv.text = selectEmployee.employeeFullName
        if (selectEmployee.employeeId != null)      selectEmployeeId = selectEmployee.employeeId

        clerkTv.setBackgroundResource(R.drawable.shape_transparent_bg)
    }

    private fun onScrollTemplateRecycle() {
        recyclerView!!.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true
                }
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                currentItem = linearLayoutManager!!.childCount
                total = linearLayoutManager!!.itemCount
                scrollDown = linearLayoutManager!!.findFirstVisibleItemPosition()
                if (dy > 0) {
                    if (isScrolling && currentItem + scrollDown == total) {
                        if (total == size) {
                            isScrolling = false
                            currentPage++
                            progressBar.visibility = View.VISIBLE
                            if (action_type_detail == "action_detail") {
                                recyclerView.scrollToPosition(itemTemplateAPIModelList.size - 1)
                                initRecycleViewTemplateForm()
                            } else {
                                recyclerView.scrollToPosition(inspectionList.size - 1)
                                // initRecycleView()
                                requestServiceTemplate()
                            }
                            size += 16
                        } else {
                            progressBar.visibility = View.GONE
                        }
                    }
                }
            }
        })
    }

    @SuppressLint("NotifyDataSetChanged", "Range")
    private fun customPrettyDialog(prettyDialog: PrettyDialog) {
        prettyDialog
            .setTitle(resources.getString(R.string.option_type))
            .setMessage(resources.getString(R.string.select_option_type_inspection))
            .setIcon(
                R.drawable.ic_close,
                R.color.red
            )
            { prettyDialog.dismiss() }
            .addButton(
                resources.getString(R.string.detailed),
                R.color.pdlg_color_white,
                R.color.blue
            )
            {
                count++
                listAddForm.add(
                    ItemTemplateModel(
                        count.toString(),
                        "AA$count",
                        StaticUtilsKey.detail_button_key
                    )
                )
                MockUpData.isRemove = false
                StaticUtilsKey.noteId = ""
                StaticUtilsKey.is_check_key = StaticUtilsKey.detail_button_key
                prettyDialog.dismiss()
                initRecycleViewInspection()

                setButtonEnable()
            }

            .addButton(
                resources.getString(R.string.simplified),
                R.color.pdlg_color_white,
                R.color.yellow
            )
            {
                count++
                listAddForm.add(
                    ItemTemplateModel(
                        count.toString(),
                        "AA$count",
                        StaticUtilsKey.simplify_button_key
                    )
                )
                MockUpData.isRemove = false
                StaticUtilsKey.noteId = ""
                StaticUtilsKey.is_check_key = StaticUtilsKey.simplify_button_key
                prettyDialog.dismiss()
                initRecycleViewInspection()

                setButtonEnable()
            }

            .addButton(
                resources.getString(R.string.question),
                R.color.white,
                R.color.green
            )
            {   count++
                listAddForm.add(
                    ItemTemplateModel(
                        count.toString(),
                        "AA$count",
                        StaticUtilsKey.question_button_key
                    )
                )
                MockUpData.isRemove = false
                StaticUtilsKey.noteId = ""
                StaticUtilsKey.is_check_key = StaticUtilsKey.question_button_key
                prettyDialog.dismiss()

                //templateInspectionAdapter.notifyDataSetChanged()
                initRecycleViewInspection()

                setButtonEnable()
            }

            .setAnimationEnabled(true)
            .show()
        prettyDialog.setCancelable(false)
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_EDIT_ITEM && resultCode == RESULT_OK) {
            if(data != null){
                for (item in listAddForm){
                    val itemTemplate : ItemTemplateModel = data.getSerializableExtra("item_value") as ItemTemplateModel
                    if (item.itemTemplateAPIModel.inspectionRoomId == itemTemplate.itemTemplateAPIModel.inspectionRoomId){
                        item.itemTemplateAPIModel = itemTemplate.itemTemplateAPIModel
                        break
                    }
                }
            }
            templateInspectionAdapter.notifyDataSetChanged()
        } else  if (requestCode == REQUEST_BACK_INSPECTION_FORM && resultCode == RESULT_OK) {
            setResult(RESULT_OK)
            finish()
        } else  if (requestCode == REQUEST_SCHEDULE_TEMPLATE && resultCode == RESULT_OK) {
            val intent = intent
            intent.putExtra("id", getInspectionId)
            intent.putExtra("inspection_name", getInspectionName)
            setResult(RESULT_OK, intent)
            finish()
        }

    }

    private fun deleteInspectionList(inspectionLevel: String, inspectionId: String) {
        progressBar.visibility = View.VISIBLE
        val hashMap = HashMap<String, Any>()
        hashMap["inspection_level"] = inspectionLevel
        if (inspectionLevel.equals(StaticUtilsKey.main_key, ignoreCase = true)) {
            hashMap["inspection_room_id"] = inspectionId
        } else {
            hashMap["inspection_room_item_id"] = inspectionId
        }
        InspectionWS()
            .DeleteInspectionList(this@ChooseInspectionTypeActivity, hashMap, callBackListener)
    }

    private val callBackListener: CallBackUpdateInspectionListener = object : CallBackUpdateInspectionListener {

            override fun onSuccessSaveInspectionList(msg: String, modelList: ItemTemplateAPIModel) {}

            override fun onSuccessDeleteInspectionList(msg: String) {
                progressBar.visibility = View.GONE
                Utils.customToastMsgError(this@ChooseInspectionTypeActivity, msg, true)

                if (listAddForm.size > 0) {
                    listAddForm.remove(backUpItemTemplateModel)
                    MockUpData.isRemove = true
                    initRecycleViewInspection()
                }
            }

            override fun onFailed(msg: String) {
                progressBar.visibility = View.GONE
                Utils.customToastMsgError(this@ChooseInspectionTypeActivity, msg, false)
            }
    }

    // Request List From Detail Inspection Form
    private fun requestServiceListByInspection(){
        progressBar.visibility = View.VISIBLE
        StaticUtilsKey.isActionSavedTemplate = false
        InspectionWS().getListInspection_MaintenanceForm(this@ChooseInspectionTypeActivity, StaticUtilsKey.isActionMenuHome, currentPage, 100, inspectionIdTemplateSave, callBackListListener)
    }

    // Request List Template Inspection Form After Save Schedule
    private fun requestServiceListTemplateForm(inspectionId : String){
        progressBar.visibility = View.VISIBLE
        StaticUtilsKey.isActionSavedTemplate = true
        InspectionWS().getListTemplateInspection(this@ChooseInspectionTypeActivity, inspectionId, callBackListListener)
    }

    private fun hashMapDataSaveTemplateSchedule() :HashMap<String, Any>{
        val hashMap = HashMap<String, Any>()
        hashMap["property_id"] = Gson().fromJson(UserSessionManagement(this).userDetail, User::class.java).accountId
        hashMap["inspection_type_id"] = getInspectionId
        hashMap["inspection_template_id"] = templateTypeId
        hashMap["conduct_date"] = selectConductDate
        hashMap["conduct_time"] = selectConductTime
        hashMap["conduct_duration"] = selectDuring
        hashMap["clerk_employee_id"] = selectEmployeeId
        hashMap["inspection_name"] = inspectionNameEdt.text.toString().trim()
        hashMap["user_id"] = UserSessionManagement(this).userId

        val listHashmap: ArrayList<HashMap<String, Any?>> = ArrayList()
        for (item in inspectorsIdList){
            val hashMapSub = HashMap<String, Any?>()
            hashMapSub["owner_rent_id"] = item.ownerRentId
            hashMapSub["owner_rent_user_id"] = item.ownerRentUserId
            listHashmap.add(hashMapSub)
        }
        if (isUnitInspectionClick) {
            hashMap["inspection_user_type"] = selectUserType
            hashMap["unit_id"] = selectUnitNoId
            hashMap["inspectors"] = listHashmap
        }
        return hashMap
    }

    private fun hashMapDataSaveInspection(){
        val hashMap = HashMap<String, Any>()
        for (itemTemplateModel in listAddForm) {
            if(!itemTemplateModel.isAlreadySave) {
                hashMap["user_id"] = UserSessionManagement(this@ChooseInspectionTypeActivity).userId
             //   hashMap["property_id"] = Gson().fromJson(UserSessionManagement(this).userDetail, User::class.java).activePropertyIdFk
                hashMap["template_id"] = getTemplateId
                hashMap["inspection_id"] = inspectionIdTemplateSave

                hashMap["option_type"] = itemTemplateModel.type_menu

                hashMap["inspection_name"] =
                    if (itemTemplateModel.main_category_inspection != null) itemTemplateModel.main_category_inspection else ""

                val listHashmap: ArrayList<HashMap<String, Any?>> = ArrayList()
                if (itemTemplateModel.value != null) {
                    for (subItemTemplateModel in MockUpData.getListSubInspection()[itemTemplateModel.value]!!) {
                        val hashMapSub = HashMap<String, Any?>()
                        hashMapSub["item_no"] = subItemTemplateModel.item_no

                        if (itemTemplateModel.type_menu == StaticUtilsKey.detail_button_key) {
                            hashMapSub["item_name"] =
                                if (subItemTemplateModel.sub_item_category != null) subItemTemplateModel.sub_item_category else ""
                            hashMapSub["item_desc"] =
                                if (subItemTemplateModel.item_description != null) subItemTemplateModel.item_description else ""
                            hashMapSub["item_condition"] =
                                if (subItemTemplateModel.item_condition != null) subItemTemplateModel.item_condition else ""
                        } else {
                            hashMapSub["item_name"] =
                                if (subItemTemplateModel.sub_item_category != null) subItemTemplateModel.sub_item_category else ""
                            if (itemTemplateModel.type_menu == StaticUtilsKey.simplify_button_key) {
                                hashMapSub["item_desc"] = "Clean; Undamage; Working;"
                                var itemCon = ""
                                itemCon += if (subItemTemplateModel.isClickNA != "") subItemTemplateModel.isClickNA.uppercase() + "; " else "N/A; "
                                itemCon += if (subItemTemplateModel.clickUnDamage != "") subItemTemplateModel.clickUnDamage.uppercase() + "; " else "N/A; "
                                itemCon += if (subItemTemplateModel.clickWorking != "") subItemTemplateModel.clickWorking.uppercase() else "N/A"

                                hashMapSub["item_condition"] = itemCon
                            } else {
                                hashMapSub["item_desc"] = ""
                                var itemDes: String
                                if (subItemTemplateModel.isClickNA != "") {
                                    itemDes = when (subItemTemplateModel.isClickNA) {
                                        resources.getString(R.string.yes).uppercase() -> {
                                            resources.getString(R.string.yes)
                                                .uppercase() + "; " + "N/A" + "; " + "N/A"
                                        }
                                        resources.getString(R.string.no).uppercase() -> {
                                            "N/A" + "; " + resources.getString(R.string.no)
                                                .uppercase() + "; " + "N/A"
                                        }
                                        else -> {
                                            "N/A; N/A; N/A"
                                        }
                                    }

                                    subItemTemplateModel.isClickNA.uppercase() + "; "
                                } else {
                                    itemDes = "N/A; N/A; N/A"
                                }

                                hashMapSub["item_condition"] = itemDes
                            }
                        }

                        listHashmap.add(hashMapSub)
                    }
                }

                val listHashmapBackup: ArrayList<HashMap<String, Any?>> = ArrayList()

                for (listMap in listHashmap) {
                    listHashmapBackup.add(listMap)
                }
                hashMap["sub_inspection_info"] = listHashmapBackup

                progressBar.visibility = View.VISIBLE
                InspectionWS().saveInspectionList(this@ChooseInspectionTypeActivity, hashMap, itemTemplateModel, callBackListListener)

            }
        }
        if (listAddForm.size > 0){
            if(listAddForm[listAddForm.size - 1].isAlreadySave)
                Toast.makeText(this@ChooseInspectionTypeActivity, resources.getString(R.string.please_add_new_items), Toast.LENGTH_SHORT).show()
        }
    }

    private fun requestServiceTemplate(){
        InspectionWS().getTemplateTypeInspection(this@ChooseInspectionTypeActivity, StaticUtilsKey.isActionMenuHome, currentPage, 16,
            Gson().fromJson(UserSessionManagement(this).userDetail, User::class.java).accountId,
            getInspectionId, callBackListListener)
    }

    @SuppressLint("NotifyDataSetChanged")
    private var callBackListListener = object : InspectionWS.CallBackItemInspectionListener{
        override fun onSuccess(itemInspectionModels: MutableList<ItemInspectionModel>?) {
            progressBar.visibility = View.GONE
            itemInspectionModels?.let { inspectionList.addAll(it) }
            if (inspectionList.size > 0){
                inspectionList[0].isClick = true
                templateTypeId = inspectionList[0].id
                inspectionNameEdt.setText(if(inspectionList[0].templateName != null) inspectionList[0].templateName else "")
            }

            selectInspectionAdapter.notifyDataSetChanged()

        }

        override fun onSuccessGetInspectionFormList(itemInspectionModels: MutableList<ItemTemplateAPIModel>?) {
            progressBar.visibility = View.GONE
            itemInspectionModels?.let { itemTemplateAPIModelList.addAll(it) }

            addListAction(itemTemplateAPIModelList)
        }

        override fun onSuccessSaveTemplate(itemInspectionModels: TemplateFormInspectionModel) {
            btnNext.isClickable = true
            progressBar.visibility = View.GONE
            intent.putExtra("action", StaticUtilsKey.form_template_type)
            intent.putExtra("template_id", templateTypeId)
//            intent.putExtra("inspection_template_id", getInspectionId)
            intent.putExtra("inspection_template_id", itemInspectionModels.inspectionId)
            startActivityForResult(intent, REQUEST_BACK_INSPECTION_FORM)
        }

        override fun onSuccessSaveInspectionList(msg: String?, itemTemplateModel: ItemTemplateModel, apiSaveInspectionList : ItemTemplateAPIModel) {
            progressBar.visibility = View.GONE
            itemTemplateModel.itemTemplateAPIModel = apiSaveInspectionList

            itemTemplateModel.isAlreadySave = true

            val subInspectionAllList: ArrayList<SubItemTemplateModel> = ArrayList()
            val hashMap = HashMap<String, List<SubItemTemplateModel>>()

            subInspectionAllList.addAll(MockUpData.getListSubInspection()[itemTemplateModel.value]!!)

            for (i in apiSaveInspectionList.inspectionSubTitle.indices) {
                try {
                    subInspectionAllList[i].item_room_id = apiSaveInspectionList.inspectionSubTitle[i].roomItemId

                    subInspectionAllList[i].reportType = apiSaveInspectionList.inspectionSubTitle[i].reportType
                    subInspectionAllList[i].workOrderNoDefault = apiSaveInspectionList.inspectionSubTitle[i].workOrderNoDefault
                    subInspectionAllList[i].workOrderSubjectDefault = apiSaveInspectionList.inspectionSubTitle[i].workOrderSubjectDefault

                } catch (ex : Exception) { }
            }

            hashMap[itemTemplateModel.value] = subInspectionAllList

            for (listTemplate in listAddForm){
                if(listTemplate.value != itemTemplateModel.value){
                    hashMap[listTemplate.value] = MockUpData.getListSubInspection()[listTemplate.value]!!
                }
            }
            MockUpData.setListSubInspection(hashMap)

            templateInspectionAdapter = TemplateFormAdapter(this@ChooseInspectionTypeActivity, listAddForm, onClickCallBack)
            recyclerView?.adapter = templateInspectionAdapter

            btnSave.isClickable = false
            btnSave.isEnabled = false
            btnSave.alpha = 0.2F
        }

        override fun onSuccessGetInspectorList(inspectorModelList: MutableList<InspectorModel>?) {
            progressBar.visibility = View.GONE
            inspectorList.clear()
            inspectorModelList?.let { inspectorList.addAll(it) }

            startSelectInspector()
        }

        override fun onFailed(msg: String?) {
            btnNext.isClickable = true
            progressBar.visibility = View.GONE
            Utils.customToastMsgError(this@ChooseInspectionTypeActivity, msg, false)
        }

    }

    private fun selectInspector(){
        val msg: String
        when {
            selectUserType == "" && isUnitInspectionClick -> {
                msg = resources.getString(R.string.please_select_inspector_type)
                Utils.customToastMsgError(this@ChooseInspectionTypeActivity, msg, false)
                userTypeTv.setBackgroundResource(R.drawable.shape_error)
            }
            selectUnitNoId == "" && isUnitInspectionClick -> {
                msg = resources.getString(R.string.please_select_unit_no)
                Utils.customToastMsgError(this@ChooseInspectionTypeActivity, msg, false)
                unitNoTv.setBackgroundResource(R.drawable.shape_error)
            }
            else ->{
                InspectionWS().getInspectors(this@ChooseInspectionTypeActivity, selectUnitNoId, selectUserType, callBackListListener)
            }
        }
    }

    private lateinit var selectInspectorAdapter : SelectInspectorAdapter
    private fun startSelectInspector(){
        val recyclerView: RecyclerView
        val alertLayout = LayoutInflater.from(this).inflate(R.layout.activity_select_inspector, null)
        val dialog = AlertDialog.Builder(this, R.style.AlertShape)
        dialog.setView(alertLayout)
        val alertDialog = dialog.show()
        recyclerView = alertLayout.findViewById(R.id.recyclerViewLang)
        val noItemTv : TextView = alertLayout.findViewById(R.id.noItemTv)
        alertLayout.findViewById<TextView>(R.id.txtOk).setOnClickListener { alertDialog.dismiss() }

        recyclerView.layoutManager = LinearLayoutManager(this)
        selectInspectorAdapter = SelectInspectorAdapter(inspectorList, object : SelectInspectorAdapter.OnClickCallBackListener{
            @SuppressLint("NotifyDataSetChanged")
            override fun onClickCallBack(inspectorModel: InspectorModel) {
                inspectorsIdList.clear()
                for (item in inspectorList) {
                    if (inspectorModel.ownerRentId == item.ownerRentId) {
                        item.isCheckInspector = !inspectorModel.isCheckInspector
                    }
                    if (item.isCheckInspector){
                        inspectorsIdList.add(item)
                    }
                }
                selectInspectorAdapter.notifyDataSetChanged()
                initRecycleViewItemInspector()
            }

        })
        recyclerView.adapter = selectInspectorAdapter

        noItemTv.visibility = if (inspectorList.size > 0) View.GONE else View.VISIBLE
    }

    private lateinit var itemInspectorAdapter : ItemInspectorAdapter
    private fun initRecycleViewItemInspector(){
        inspectorRecycle.layoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL,false)
        itemInspectorAdapter = ItemInspectorAdapter(inspectorsIdList, object : ItemInspectorAdapter.OnClickCallBackListener{
            @SuppressLint("NotifyDataSetChanged")
            override fun onClickCallBack(inspectorModel: InspectorModel) {
                inspectorsIdList.clear()
                for (item in inspectorList) {
                    if (inspectorModel.ownerRentId == item.ownerRentId) {
                        item.isCheckInspector = false
                    }
                    if (item.isCheckInspector){
                        inspectorsIdList.add(item)
                    }
                }
                itemInspectorAdapter.notifyDataSetChanged()
            }
        })
        inspectorRecycle.adapter = itemInspectorAdapter
        layoutInspector.setBackgroundResource(R.drawable.shape_transparent_bg)
    }

}