package com.eazy.daikou.ui.home.hrm.profile

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.model.hr.Qualification
import com.eazy.daikou.model.hr.Relationship

class RelationshipDetailActivity : BaseActivity() {

    private lateinit var nameTv:TextView
    private lateinit var dateOfBirthTv:TextView
    private lateinit var relationshipTv:TextView
    private lateinit var occupationTv:TextView
    private lateinit var companyTv:TextView
    private lateinit var jobTitleTv:TextView
    private lateinit var nationality:TextView
    private lateinit var addressTv:TextView

    private lateinit var btnBack:TextView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_relationship_detail)

        initView()
        initAction()

    }
    private fun initView(){
        nameTv = findViewById(R.id.nameTv)
        dateOfBirthTv = findViewById(R.id.dateOfBirthTv)
        relationshipTv = findViewById(R.id.relationShipTv)
        occupationTv = findViewById(R.id.occupationTv)
        companyTv = findViewById(R.id.companyTv)
        jobTitleTv = findViewById(R.id.jobTitleTv)
        nationality = findViewById(R.id.nationalityTv)
        addressTv = findViewById(R.id.addressTv)
        btnBack = findViewById(R.id.btn_back)

    }
    private fun initAction(){
        btnBack.setOnClickListener { finish() }

        if (intent != null && intent.hasExtra("relationship_detail")){
            val relationship = intent.getSerializableExtra("relationship_detail") as Relationship
            setValues(relationship)
        }

    }

    private fun setValues(relationship: Relationship){
        nameTv.text = relationship.name
        dateOfBirthTv.text = relationship.dob
        relationshipTv.text = relationship.relationship
        occupationTv.text = relationship.occupation
        companyTv.text = relationship.company
        jobTitleTv.text = relationship.job_title
        addressTv.text = relationship.address
    }
}