package com.eazy.daikou.ui.home.book_now.booking_hotel.travel_talk

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.FragmentTransaction
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.booking_hotel.HotelTravelTalkModel
import com.google.android.material.appbar.AppBarLayout
import de.hdodenhof.circleimageview.CircleImageView
import kotlin.math.abs

class HotelTravelTalkViewProfileActivity : BaseActivity() {

    private var hotelTravelTalkModel: HotelTravelTalkModel? = null
    private lateinit var txtUsername: TextView
    private lateinit var txtEmail: TextView
    private lateinit var imgProfile: CircleImageView
    private var categoryType = "my_thread"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hotel_travel_talk_view_profile)

        initView()

        initData()

        initAction()

    }

    private fun initView() {
        findViewById<ImageView>(R.id.btnBack).setOnClickListener { finish() }

        txtEmail = findViewById(R.id.txtEmail)
        txtUsername = findViewById(R.id.txtUsername)
        imgProfile = findViewById(R.id.img_profile)
    }

    private fun initData() {
        if (intent != null && intent.hasExtra("hotel_travel_talk")) {
            hotelTravelTalkModel =
                intent.getSerializableExtra("hotel_travel_talk") as HotelTravelTalkModel
        }

        if (hotelTravelTalkModel != null) {
            if (hotelTravelTalkModel!!.created_by != null) {
                txtUsername.text =
                    if (hotelTravelTalkModel!!.created_by!!.name != null) hotelTravelTalkModel!!.created_by!!.name else "Me"
                Glide.with(imgProfile)
                    .load(if (hotelTravelTalkModel!!.created_by!!.image != null) hotelTravelTalkModel!!.created_by!!.image else R.drawable.ic_my_profile)
                    .into(imgProfile)

                when {
                    hotelTravelTalkModel!!.created_by!!.email != null -> {
                        txtEmail.text = hotelTravelTalkModel!!.created_by!!.email
                    }
                    hotelTravelTalkModel!!.created_by!!.phone != null -> {
                        txtEmail.text = hotelTravelTalkModel!!.created_by!!.email
                    }
                    else -> {
                        txtEmail.visibility = View.GONE
                    }
                }

                findViewById<TextView>(R.id.numOfPostTv).text =
                    if (hotelTravelTalkModel!!.created_by!!.total_posts != null)
                        String.format("%s %s", hotelTravelTalkModel!!.created_by!!.total_posts, resources.getString(R.string.post)) else "0"
                findViewById<TextView>(R.id.numOfLikeTv).text =
                    if (hotelTravelTalkModel!!.created_by!!.total_likes != null)
                        String.format("%s %s", hotelTravelTalkModel!!.created_by!!.total_likes, resources.getString(R.string.like)) else "0"
                findViewById<TextView>(R.id.numOfCommentTv).text =
                    if (hotelTravelTalkModel!!.created_by!!.total_comments != null)
                        String.format("%s %s", hotelTravelTalkModel!!.created_by!!.total_comments, resources.getString(R.string.comment)) else "0"
            }
        }

        //Custom app bar scroll to top
        val titleToolbarTv = findViewById<TextView>(R.id.titleToolbar)
        titleToolbarTv.text = txtUsername.text.toString().trim()
        val appBarLayout = findViewById<AppBarLayout>(R.id.appBarLayout)
        appBarLayout.addOnOffsetChangedListener { appBarLayout, verticalOffset ->
            if (abs(verticalOffset) - appBarLayout.totalScrollRange == 0) {
                // Collapsed
                titleToolbarTv.visibility = View.VISIBLE
            } else {
                // Expanded
                titleToolbarTv.visibility = View.INVISIBLE

            }
        }
    }

    private fun initAction() {
        // On Click Ask Question
        findViewById<ImageView>(R.id.askQuestionTv).setOnClickListener(CustomSetOnClickViewListener {
            val intent = Intent(this, HotelAskQuestionTravelTalkActivity::class.java)
            resultLauncher.launch(intent)
        })

        initClickMenu()

        reLoadFragment(categoryType)
    }

    private fun initClickMenu() {
        val categoryLayout: LinearLayout = findViewById(R.id.categoryLayout)

        for (i in 0 until categoryLayout.childCount) {
            Utils.logDebug("jeeeeeeeeeeeee", i.toString())
            categoryLayout.getChildAt(i).setOnClickListener { v ->
                setBackground(v.id, categoryLayout)

                categoryType = when (i) {
                    0 -> {
                        "my_thread"
                    }
                    2 -> {
                        "my_like"
                    }
                    else -> {
                        "my_reply"
                    }
                }
                reLoadFragment(categoryType)
            }
        }
    }

    private val resultLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            if (result.resultCode == Activity.RESULT_OK) {
                if (categoryType == "my_thread") {
                    reLoadFragment(categoryType)
                }
            }
        }

    private fun setBackground(id: Int, categoryLayout: LinearLayout) {
        for (i in 0 until categoryLayout.childCount) {
            val rowView: View = categoryLayout.getChildAt(i)
            if (rowView is LinearLayout) {
                if (id == rowView.id) {
                    rowView.setBackgroundColor(
                        Utils.getColor(
                            this@HotelTravelTalkViewProfileActivity,
                            R.color.color_secondary_text_home
                        )
                    )
                } else {
                    rowView.setBackgroundColor(
                        Utils.getColor(
                            this@HotelTravelTalkViewProfileActivity,
                            R.color.transparent
                        )
                    )
                }
            }
        }
    }

    @SuppressLint("DetachAndAttachSameFragment")
    private fun reLoadFragment(categoryType: String) {
        var userId = ""
        if (hotelTravelTalkModel!!.created_by != null) {
            if (hotelTravelTalkModel!!.created_by!!.id != null) {
                userId = hotelTravelTalkModel!!.created_by!!.id!!
            }
        }
        val ft: FragmentTransaction = supportFragmentManager.beginTransaction()
        ft.replace(R.id.nav_fragment, HotelProfileItemFragment.newInstance(categoryType, userId))
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        ft.addToBackStack(null)
        ft.commit()
    }

    override fun onBackPressed() {
        finish()
    }
}