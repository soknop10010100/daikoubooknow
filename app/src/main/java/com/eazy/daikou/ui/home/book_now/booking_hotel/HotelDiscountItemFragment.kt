package com.eazy.daikou.ui.home.book_now.booking_hotel

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AbsListView
import android.widget.ProgressBar
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseFragment
import com.eazy.daikou.request_data.request.book_now_ws.BookingHotelWS
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.home.SubItemHomeModel
import com.eazy.daikou.ui.home.book_now.booking_hotel.adapter.HotelItemDiscountAdapter

class HotelDiscountItemFragment : BaseFragment() {

    private var category = ""
    private lateinit var progressBar : ProgressBar
    private var itemDiscountList = ArrayList<SubItemHomeModel>()
    private lateinit var adapterItemDiscount: HotelItemDiscountAdapter
    private lateinit var linearLayoutManager : LinearLayoutManager
    private var currentItem = 0
    private var total : Int = 0
    private var scrollDown : Int = 0
    private var currentPage : Int = 1
    private var size : Int = 10
    private var isScrolling = false
    private lateinit var recyclerViewItemDiscount: RecyclerView

    private var callBackListener: CallBackListener? = null

    companion object {
        @JvmStatic
        fun newInstance(category : String) =
            HotelDiscountItemFragment().apply {
                arguments = Bundle().apply {
                    putString("category", category)
                }
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            category = it.getString("category").toString()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_hotel_discount_item, container, false)

        initView(view)

        initAction()

        return view
    }

    private fun initView(view : View){
        recyclerViewItemDiscount = view.findViewById(R.id.recyclerView)
        progressBar = view.findViewById(R.id.progressItem)
        progressBar.visibility = View.GONE
    }

    private fun initAction(){
        linearLayoutManager = LinearLayoutManager(mActivity)
        recyclerViewItemDiscount.layoutManager = linearLayoutManager

        initRecyclerViewItem()

        onScrollItemRecycle()
    }

    private fun refreshItemList(){
        currentPage = 1
        size = 10
        isScrolling = true

        itemDiscountList.clear()
        requestListWs()
    }

    private fun initRecyclerViewItem() {
        adapterItemDiscount = HotelItemDiscountAdapter(itemDiscountList, object : HotelItemDiscountAdapter.OnClickItemListener {
            override fun onItemClick(subItemHomeModel: SubItemHomeModel) {
                val intent = Intent(mActivity, HotelBookingDetailHotelActivity::class.java).apply {
                    putExtra("hotel_name", subItemHomeModel.name)
                    putExtra("category", subItemHomeModel.titleCategory)
                    putExtra("hotel_image", subItemHomeModel.imageUrl)
                    putExtra("hotel_id", subItemHomeModel.id)
                    putExtra("price", subItemHomeModel.priceVal)
                }
                resultLauncher.launch(intent)
            }
        })
        recyclerViewItemDiscount.adapter = adapterItemDiscount

        requestListWs()
    }

    private val resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
        if (result.resultCode == Activity.RESULT_OK) {
            refreshItemList()
        }
    }

    private fun requestListWs(){
        progressBar.visibility = View.VISIBLE
        BookingHotelWS().getDiscountItemWs(mActivity, currentPage, 10, category, object : BookingHotelWS.OnCallBackDiscountListener{
            @SuppressLint("NotifyDataSetChanged")
            override fun onSuccessDiscountList(sliderBanner: ArrayList<String>, discountList: ArrayList<SubItemHomeModel>) {
                progressBar.visibility = View.GONE
                itemDiscountList.addAll(discountList)
                adapterItemDiscount.notifyDataSetChanged()

                Utils.validateViewNoItemFound(mActivity, recyclerViewItemDiscount, itemDiscountList.size <= 0)

                if(callBackListener != null)    callBackListener!!.onCallBack(sliderBanner)
            }

            override fun onFailed(message: String) {
                progressBar.visibility = View.GONE
                Utils.customToastMsgError(mActivity, message, false)
            }

        })
    }

    private fun onScrollItemRecycle() {
        recyclerViewItemDiscount.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true
                }
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                currentItem = linearLayoutManager.childCount
                total = linearLayoutManager.itemCount
                scrollDown = linearLayoutManager.findFirstVisibleItemPosition()
                if (dy > 0) {
                    if (isScrolling && currentItem + scrollDown == total) {
                        if (total == size) {
                            isScrolling = false
                            currentPage++
                            progressBar.visibility = View.VISIBLE
                            recyclerView.scrollToPosition(itemDiscountList.size - 1)
                            initRecyclerViewItem()
                            size += 10
                        } else {
                            progressBar.visibility = View.GONE
                        }
                    }
                }
            }
        })
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            callBackListener = context as CallBackListener
        } catch (e: ClassCastException) {
            throw ClassCastException("$context must implement onViewSelected")
        }
    }

    override fun onDetach() {
        super.onDetach()
        if (callBackListener != null)   callBackListener = null
    }

    interface CallBackListener {
        fun onCallBack(sliderBanner: ArrayList<String>)
    }

}