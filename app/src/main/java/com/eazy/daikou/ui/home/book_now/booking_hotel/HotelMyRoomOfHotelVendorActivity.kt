package com.eazy.daikou.ui.home.book_now.booking_hotel

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.widget.AbsListView
import android.widget.ProgressBar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.request_data.request.book_now_ws.BookingHotelWS
import com.eazy.daikou.request_data.request.book_now_ws.RequestHashMapData
import com.eazy.daikou.helper.GetDataUtils
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.booking_hotel.HotelMyRoomOfHotelVendorModel
import com.eazy.daikou.model.booking_hotel.HotelMyVendorModel
import com.eazy.daikou.model.home.MainItemHomeModel
import com.eazy.daikou.ui.home.book_now.booking_hotel.adapter.MyRoomOfHotelVendorAdapter

class HotelMyRoomOfHotelVendorActivity : BaseActivity() {

    private lateinit var progressBar: ProgressBar
    private lateinit var recyclerView: RecyclerView
    private lateinit var refreshLayout: SwipeRefreshLayout
    private lateinit var myHotelOfVendorAdapter : MyRoomOfHotelVendorAdapter
    private var hotelId = ""
    private var hotelName = ""

    private var currentItem = 0
    private var total : Int = 0
    private var scrollDown : Int = 0
    private var currentPage : Int = 1
    private var size : Int = 10
    private var isScrolling = false
    private lateinit var linearLayoutManager: LinearLayoutManager

    private var listRoomHotelMyVendorModel : ArrayList<HotelMyRoomOfHotelVendorModel> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hotel_my_room_of_hotel_vendor_acitivity)

        initView()

        initData()

        initAction()

    }

    private fun initView(){
        progressBar = findViewById(R.id.progressItem)
        progressBar.visibility = View.GONE
        recyclerView = findViewById(R.id.recyclerView)
        refreshLayout = findViewById(R.id.swipe_layouts)
    }

    private fun initData(){
        hotelId = GetDataUtils.getDataFromString("hotel_id", this)
        hotelName = GetDataUtils.getDataFromString("hotel_name", this)

        Utils.customOnToolbar(this, hotelName){finish()}
    }

    private fun initAction(){
        refreshLayout.setColorSchemeResources(R.color.colorPrimary)
        refreshLayout.setOnRefreshListener { refreshList()}

        linearLayoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = linearLayoutManager
        myHotelOfVendorAdapter = MyRoomOfHotelVendorAdapter(this, listRoomHotelMyVendorModel, object : MyRoomOfHotelVendorAdapter.PropertyClick{
            override fun onBookingClick(action : String, propertyModel: HotelMyRoomOfHotelVendorModel) {
                editStatusRoomWs(propertyModel.room_id!!, action)
            }
        })
        recyclerView.adapter = myHotelOfVendorAdapter

        requestDataWs()

        // On Scroll Item
        onScrollInfoRecycle()

    }

    private fun editStatusRoomWs(hotelId: String, status: String){
        progressBar.visibility = View.VISIBLE

        BookingHotelWS().doEditStatus(this, "status_room", RequestHashMapData.editStatusHotelVendor(hotelId, status, "", "", "", true), onCallBackWsListener)
    }

    private fun refreshList(){
        myHotelOfVendorAdapter.clear()
        requestDataWs()
    }

    private fun requestDataWs(){
        progressBar.visibility = View.VISIBLE
        BookingHotelWS().getRoomOfHotelVendorWs(this, currentPage, 10, hotelId, onCallBackWsListener)
    }

    private val onCallBackWsListener = object : BookingHotelWS.OnCallBackHotelVendorListener{
        override fun onSuccessShowWishlistHotel(locationHotelModel: ArrayList<MainItemHomeModel>) {}

        @SuppressLint("NotifyDataSetChanged")
        override fun onSuccessHotelOfVendor(hotelHistoryList: ArrayList<HotelMyVendorModel>) {}

        @SuppressLint("NotifyDataSetChanged")
        override fun onSuccessRoomHotel(hotelHistory: ArrayList<HotelMyRoomOfHotelVendorModel>) {
            progressBar.visibility = View.GONE
            refreshLayout.isRefreshing = false
            listRoomHotelMyVendorModel.addAll(hotelHistory)

            myHotelOfVendorAdapter.notifyDataSetChanged()

            Utils.validateViewNoItemFound(this@HotelMyRoomOfHotelVendorActivity, recyclerView, listRoomHotelMyVendorModel.size <= 0)
        }

        override fun onSuccessEditStatus(successMsg: String) {
            progressBar.visibility = View.GONE
            refreshLayout.isRefreshing = false
            Utils.customToastMsgError(this@HotelMyRoomOfHotelVendorActivity, successMsg, true)

            refreshList()
        }

        override fun onFailed(message: String) {
            progressBar.visibility = View.GONE
            refreshLayout.isRefreshing = false
            Utils.customToastMsgError(this@HotelMyRoomOfHotelVendorActivity, message, false)
        }

    }

    private fun onScrollInfoRecycle() {
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true
                }
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                currentItem = linearLayoutManager.childCount
                total = linearLayoutManager.itemCount
                scrollDown = linearLayoutManager.findFirstVisibleItemPosition()
                if (dy > 0) {
                    if (isScrolling && currentItem + scrollDown == total) {
                        if (total == size) {
                            isScrolling = false
                            currentPage++
                            progressBar.visibility = View.VISIBLE
                            recyclerView.scrollToPosition(listRoomHotelMyVendorModel.size - 1)

                            requestDataWs()
                            size += 10
                        } else {
                            progressBar.visibility = View.GONE
                        }
                    }
                }
            }
        })
    }

}