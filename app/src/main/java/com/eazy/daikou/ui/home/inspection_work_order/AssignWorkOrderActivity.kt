package com.eazy.daikou.ui.home.inspection_work_order

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.request_data.request.work_oder_ws.WorkOrderWs
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.inspection.ItemTemplateModel
import com.eazy.daikou.model.work_order.AssignWorkOrderModel
import com.eazy.daikou.model.work_order.WorkOrderDetailModel
import com.eazy.daikou.model.work_order.WorkOrderItemModel
import com.eazy.daikou.ui.home.inspection_work_order.adapter.AssignWorkOrderListAdapter

class AssignWorkOrderActivity : BaseActivity() {

    private lateinit var itemRecyclerView : RecyclerView
    private lateinit var categoryRecyclerView : RecyclerView
    private lateinit var refreshLayout : SwipeRefreshLayout
    private lateinit var progressBar : ProgressBar
    private lateinit var btnAdd : ImageView
    private var assignWorkOrderList : ArrayList<AssignWorkOrderModel> = ArrayList()
    private lateinit var subItemInspection : ItemTemplateModel.SubItemTemplateModel
    private val REQUEST_CREATE_UPDATE_WORK_ORDER = 774
    private lateinit var noItemTv : TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_assign_work_order)

        initView()

        initAction()

    }

    private fun initView(){
        refreshLayout = findViewById(R.id.swipe_layouts)
        itemRecyclerView = findViewById(R.id.listInspection)
        categoryRecyclerView = findViewById(R.id.inspectionCategory)
        categoryRecyclerView.visibility = View.GONE
        progressBar = findViewById(R.id.progressItem)
        btnAdd = findViewById(R.id.btnAdd)
        noItemTv = findViewById(R.id.no_data)
        btnAdd.visibility = View.GONE

        // Custom Action Toolbar
        Utils.customOnToolbar(this@AssignWorkOrderActivity, resources.getString(R.string.assign_work_order)) { finish() }
    }

    private fun initAction(){
        itemRecyclerView.layoutManager = LinearLayoutManager(this)

        if (intent != null && intent.hasExtra("subItemInspectionModel")){
            subItemInspection = intent.getSerializableExtra("subItemInspectionModel") as ItemTemplateModel.SubItemTemplateModel
        }

        initAssignWorkOrder()

        btnAdd.setOnClickListener{
            val intent = Intent(this@AssignWorkOrderActivity, CreateWorkOrderActivity::class.java)
            intent.putExtra("subItemInspectionModel", subItemInspection)
            startActivityForResult(intent, REQUEST_CREATE_UPDATE_WORK_ORDER)
        }

        refreshLayout.setColorSchemeResources(R.color.colorPrimary)
        refreshLayout.setOnRefreshListener { refreshLayout.isRefreshing = false }
    }

    private fun initClickOnItemDetailWorkOrderService(workOrderId : String){
        progressBar.visibility = View.VISIBLE
        WorkOrderWs()
            .getDetailWorkOrderById(this@AssignWorkOrderActivity, workOrderId, object : WorkOrderWs.CallBackListener{
            override fun onSuccessWorkOrderList(workOrderItemModelList: MutableList<WorkOrderItemModel>?) {}

            override fun onSuccessWorkOrderDetail(workOrderDetailModel: WorkOrderDetailModel) {
                progressBar.visibility = View.GONE
                val intent = Intent(this@AssignWorkOrderActivity, WorkOrderDetailActivity::class.java)
                intent.putExtra("work_order_detail", workOrderDetailModel)
                startActivityForResult(intent, REQUEST_CREATE_UPDATE_WORK_ORDER)
            }

            override fun onFailed(msg: String?) {
                progressBar.visibility = View.GONE
                Utils.customToastMsgError(this@AssignWorkOrderActivity, msg, false)
            }
        })
    }


    private fun initAssignWorkOrder(){

        WorkOrderWs()
            .getListAssignWorkOrder(this, subItemInspection.item_room_id, object : WorkOrderWs.CallBackAssignWorkOrderListener{
            override fun onSuccessAssignWorkOrder(itemTemplateAPIModel: MutableList<AssignWorkOrderModel>) {
                progressBar.visibility = View.GONE
                assignWorkOrderList.addAll(itemTemplateAPIModel)

                // Init RecyclerView Adapter
                val assignWorkOrderListAdapter = AssignWorkOrderListAdapter(this@AssignWorkOrderActivity, assignWorkOrderList, object : AssignWorkOrderListAdapter.CategoryItemClickListener {
                    override fun itemClickListener(assignWorkOrderModel: AssignWorkOrderModel) {
                        if (assignWorkOrderModel.id != null)   initClickOnItemDetailWorkOrderService(assignWorkOrderModel.id)
                    }
                })
                itemRecyclerView.adapter = assignWorkOrderListAdapter

                btnAdd.visibility = View.VISIBLE

                noItemTv.visibility = if (assignWorkOrderList.size > 0) View.GONE else View.VISIBLE
                itemRecyclerView.visibility = if (assignWorkOrderList.size > 0) View.VISIBLE else View.GONE
            }

            override fun onFailed(msg: String?) {
                progressBar.visibility = View.GONE
                Utils.customToastMsgError(this@AssignWorkOrderActivity, msg, false)
            }

        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK && requestCode == REQUEST_CREATE_UPDATE_WORK_ORDER) {
            assignWorkOrderList = ArrayList()
            initAssignWorkOrder()
        }
    }
}