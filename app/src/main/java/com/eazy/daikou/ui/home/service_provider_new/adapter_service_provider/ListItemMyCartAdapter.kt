package com.eazy.daikou.ui.home.service_provider_new.adapter_service_provider

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import org.w3c.dom.Text

class ListItemMyCartAdapter(private val context: Context): RecyclerView.Adapter<ListItemMyCartAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
       val view = LayoutInflater.from(context).inflate(R.layout.custom_layout_view_item_my_cart, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        Glide.with(context).load(R.drawable.cv_cleaning).into(holder.imageItemMyCart)

    }

    override fun getItemCount(): Int {
        return 2
    }
    class ViewHolder(view: View): RecyclerView.ViewHolder(view){

        var imageItemMyCart : ImageView = view.findViewById(R.id.imageItemMyCart)
        var productNameTv : TextView = view.findViewById(R.id.productNameTv)
        var locationTv : TextView = view.findViewById(R.id.locationTv)
        var numberItemTv : TextView = view.findViewById(R.id.numberItemTv)
        var amountItemTv : TextView = view.findViewById(R.id.amountItemTv)
        var totalAmountItemTv : TextView = view.findViewById(R.id.totalAmountItemTv)
    }

}