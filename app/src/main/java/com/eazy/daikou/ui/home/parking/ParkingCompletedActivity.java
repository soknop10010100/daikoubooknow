package com.eazy.daikou.ui.home.parking;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.eazy.daikou.R;
import com.eazy.daikou.base.BaseActivity;
import com.eazy.daikou.helper.DateUtil;
import com.eazy.daikou.helper.Utils;
import com.eazy.daikou.model.parking.Parking;

import java.util.Locale;

public class ParkingCompletedActivity extends BaseActivity {

    private TextView nameProjectTV;
    private TextView priceTv;
    private TextView totalPriceTv;
    private TextView durationTv;
    private TextView entranceDateTv;
    private TextView leaveDateTv;
    private TextView lotNoTv;
    private TextView paymentMethodTv;
    private TextView btnDone;
    private Parking parking;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parking_completed);

        initView();

        initData();

        initAction();

    }

    private void initView() {

        nameProjectTV = findViewById(R.id.name_pro_Text);
        priceTv = findViewById(R.id.price_Text);
        totalPriceTv = findViewById(R.id.total_price_parking);
        entranceDateTv = findViewById(R.id.text_entrance_Date);
        leaveDateTv = findViewById(R.id.leave_date);
        lotNoTv = findViewById(R.id.lot_no_Text);
        durationTv = findViewById(R.id.duration_Text);
        paymentMethodTv = findViewById(R.id.payment_method);
        btnDone = findViewById(R.id.btn_done);

    }

    @SuppressLint("SetTextI18n")
    private void initData() {
        if (getIntent().hasExtra("parking")) {
            parking = (Parking) getIntent().getSerializableExtra("parking");
        }
        nameProjectTV.setText(String.format(Locale.US, "%s", parking.getPropertyName()));
        entranceDateTv.setText(String.format(Locale.US, "%s", DateUtil.formatTimeZoneLocal(parking.getEntranceDate())));
        leaveDateTv.setText(String.format(Locale.US, "%s", DateUtil.formatTimeZoneLocal(parking.getCurrentDateTime())));
        priceTv.setText(String.format(Locale.US, "%s %s",parking.getParkingFeeCurrency(), parking.getParkingFee()));
        lotNoTv.setText(String.format(Locale.US, "%s", parking.getParkingNoName()));
        totalPriceTv.setText(String.format(Locale.US, "%s %s",parking.getParkingFeeCurrency(), parking.getParkingFee()));
        durationTv.setText(String.format(Locale.US, "%s", Utils.covertTime(parking.getParkingDurationInMinute(),parking.getParkingType())));
        paymentMethodTv.setText("Pay by Kess");

    }

    private void initAction(){
        btnDone.setOnClickListener(view -> {
            startActivity(new Intent(ParkingCompletedActivity.this, ParkingListActivity.class));
            finish();
        });
    }


}
