package com.eazy.daikou.ui.home.utility_tracking;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.eazy.daikou.R;
import com.eazy.daikou.base.BaseActivity;
import com.eazy.daikou.request_data.request.track_water_ws.TrackWaterElectricityWs;
import com.eazy.daikou.request_data.sql_database.TrackWaterElectricityDatabase;
import com.eazy.daikou.request_data.static_data.TypeTrackingWaterElectricity;
import com.eazy.daikou.helper.InternetConnection;
import com.eazy.daikou.helper.UserSessionManagement;
import com.eazy.daikou.helper.Utils;
import com.eazy.daikou.model.floor_plan.FloorSpinnerModel;
import com.eazy.daikou.model.profile.User;
import com.eazy.daikou.model.utillity_tracking.model.W_ETrackingModel;
import com.eazy.daikou.ui.home.utility_tracking.adapter.UtilityWaterElectricityAdapter;
import com.eazy.daikou.model.utillity_tracking.model.FloorUtilNoModel;
import com.eazy.daikou.model.utillity_tracking.model.LastTrackData;
import com.eazy.daikou.model.utillity_tracking.model.UtilNoCommonAreaModel;
import com.google.gson.Gson;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

public class UploadWaterElectricityActivity extends BaseActivity {

    private TextView floorTv, txtDateTv, txtNoResultTv, titleTv, txtTitleListTv, txtActionTv;
    private RecyclerView recyclerView;
    private W_ETrackingModel eTrackingModel;
    private TrackWaterElectricityDatabase data;
    private ProgressBar progressBar;
    private ImageView btnBack, btnDelete;
    private LinearLayout viewSpinner;

    private final HashMap<String, String> hashMapListFloor = new LinkedHashMap<>();
    private final List<FloorSpinnerModel> floorSpinnerModels = new ArrayList<>();
    private final List<String> floorName = new ArrayList<>();
    private final HashMap<String, List<UtilNoCommonAreaModel>> hashMap = new LinkedHashMap<>();
    private final HashMap<String, String> mapNameFloor = new LinkedHashMap<>();
    private List<FloorUtilNoModel> floorUtilNoModelList;

    private static final int REQUEST_CODE_SELECT_FLOOR = 192;
    private final int REQUEST_UPLOAD = 4124;

    private boolean isWater = false, isClickSave = false;
    private String action = "", purpose, clickAction = "", tracking_date_warning = "", selectFloor = "", tracking_date, selectFloorName = "";

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_water_electricity);

        initView();

        initData();

        initAction();

    }

    private void initView() {
        recyclerView = findViewById(R.id.listView);
        txtTitleListTv = findViewById(R.id.txtTitleList);
        titleTv = findViewById(R.id.titleToolbar);
        btnBack = findViewById(R.id.iconBack);
        txtNoResultTv = findViewById(R.id.txtNoResult);
        txtDateTv = findViewById(R.id.txtDate);
        viewSpinner = findViewById(R.id.viewSpinner);
        txtActionTv = findViewById(R.id.txtAction);
        btnDelete = findViewById(R.id.btn_delete);
        progressBar = findViewById(R.id.progressItem);
        progressBar.setVisibility(View.GONE);
        floorTv = findViewById(R.id.floorEd);
    }

    private void initData() {
        @SuppressLint("SimpleDateFormat") SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        data = new TrackWaterElectricityDatabase(UploadWaterElectricityActivity.this);

        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));

        //Get Data from activity
        if (getIntent().hasExtra("action")) {
            action = getIntent().getStringExtra("action");
            floorUtilNoModelList = (List<FloorUtilNoModel>) getIntent().getSerializableExtra("list");
        }

        if (getIntent().hasExtra("purpose")) {
            purpose = getIntent().getStringExtra("purpose");
        }

        if (getIntent().hasExtra("click_action")) {
            clickAction = getIntent().getStringExtra("click_action");
            tracking_date_warning = getIntent().getStringExtra("txtTrackingDate");
            if (!clickAction.equals("add")) {
                btnDelete.setVisibility(View.VISIBLE);
            }
        } else {
            btnDelete.setVisibility(View.GONE);
        }

        if (getIntent().hasExtra("tracking")) {
            eTrackingModel = (W_ETrackingModel) getIntent().getSerializableExtra("tracking");
            txtDateTv.setText(eTrackingModel.getTrackingDate());
            txtDateTv.setClickable(false);
        } else {
            if (clickAction.equals("warning")) {
                txtDateTv.setText(tracking_date_warning);
                txtDateTv.setClickable(false);
                tracking_date = tracking_date_warning;
            } else {
                txtDateTv.setText(formatter.format(new Date()));
                tracking_date = formatter.format(new Date());
                txtDateTv.setClickable(true);
                txtDateTv.setTextColor(Utils.getColor(this, R.color.appBarColorOld));
                txtDateTv.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_drop_down, 0);
                txtDateTv.setOnClickListener(v -> showDateTimePicker(txtDateTv));
            }
        }
    }


    private void initAction() {
        switch (action) {
            case "water_track":
                isWater = true;
                if (purpose.equals(TypeTrackingWaterElectricity.UTILITY_PURPOSE_CREATE)) {
                    titleTv.setText(getString(R.string.create_water_track));
                } else {
                    titleTv.setText(getString(R.string.update_water_track));
                }
                txtActionTv.setText(getString(R.string.dot_unit));
                txtTitleListTv.setText(getString(R.string.unit_no));
                break;
            case "water_common":
                isWater = true;
                txtTitleListTv.setText(getString(R.string.location));
                txtActionTv.setText(getString(R.string.dot_common_area));
                if (purpose.equals(TypeTrackingWaterElectricity.UTILITY_PURPOSE_CREATE)) {
                    titleTv.setText(getString(R.string.create_water_common_area));
                } else {
                    titleTv.setText(getString(R.string.update_water_common_area));
                }
                break;
            case "water_ppwsa":
                isWater = true;
                viewSpinner.setVisibility(View.GONE);
                if (purpose.equals(TypeTrackingWaterElectricity.UTILITY_PURPOSE_CREATE)) {
                    titleTv.setText(getString(R.string.create_ppwsa));
                } else {
                    titleTv.setText(getString(R.string.update_ppwsa));
                }
                txtActionTv.setText(getString(R.string.dot_ppwsa).substring(0, 5));
                txtTitleListTv.setText(getString(R.string.location));
                break;
            case "electricity_track":
                txtActionTv.setText(getString(R.string.dot_unit));
                if (purpose.equals(TypeTrackingWaterElectricity.UTILITY_PURPOSE_CREATE)) {
                    titleTv.setText(getString(R.string.create_eletrcity_track));
                } else {
                    titleTv.setText(getString(R.string.update_electricity_track));
                }
                txtTitleListTv.setText(getString(R.string.unit_no));
                break;
            case "electricity_common":
                txtActionTv.setText(getString(R.string.dot_common_area));
                if (purpose.equals(TypeTrackingWaterElectricity.UTILITY_PURPOSE_CREATE)) {
                    titleTv.setText(getString(R.string.create_electricity_common_area));
                } else {
                    titleTv.setText(getString(R.string.update_electricity_common_area));
                }
                txtTitleListTv.setText(getString(R.string.location));
                break;
            case "electricity_edc":
                viewSpinner.setVisibility(View.GONE);
                txtTitleListTv.setText(getString(R.string.location));
                if (purpose.equals(TypeTrackingWaterElectricity.UTILITY_PURPOSE_CREATE)) {
                    titleTv.setText(getString(R.string.create_edc));
                } else {
                    titleTv.setText(getString(R.string.update_edc));
                }
                txtActionTv.setText(getString(R.string.dot_edc));
                break;
        }

        hashMapAddListFloor();

        btnDelete.setOnClickListener(v -> popupMessageToClearData());

        btnBack.setOnClickListener(v -> BackButton());
    }

    public void showDateTimePicker(TextView textView) {
        Calendar calendar = Calendar.getInstance();
        int yy = calendar.get(Calendar.YEAR);
        int mm = calendar.get(Calendar.MONTH);
        int dd = calendar.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePicker = new DatePickerDialog(this, (view, year, monthOfYear, dayOfMonth) -> {
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, monthOfYear);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            textView.setText(df.format(calendar.getTime()));
            tracking_date = df.format(calendar.getTime());

            if (InternetConnection.checkInternetConnection(textView)) {
                getListAllFloorAndUnit(df.format(calendar.getTime()));
            } else {
                if (floorSpinnerModels.size() > 0) floorSpinnerModels.clear();
                hashMapAddListFloor();
                if (!action.equals("water_ppwsa") && !action.equals("electricity_edc")) {
                    if (hashMap.get(selectFloor) != null) {
                        if (Objects.requireNonNull(hashMap.get(selectFloor)).size() == 0) {
                            txtNoResultTv.setVisibility(View.VISIBLE);
                        } else {
                            txtNoResultTv.setVisibility(View.GONE);
                        }
                        UtilityWaterElectricityAdapter waterElectricityAdapter = new UtilityWaterElectricityAdapter(UploadWaterElectricityActivity.this, hashMap.get(selectFloor), isWater, tracking_date, clickAction, action, onClickUtil);
                        recyclerView.setAdapter(waterElectricityAdapter);
                    }
                }
            }
        }, yy, mm, dd);
        datePicker.getDatePicker().setMaxDate(System.currentTimeMillis() - 1000);
        datePicker.show();
    }

    private void getListAllFloorAndUnit(String tracking_date) {
        progressBar.setVisibility(View.VISIBLE);
        new TrackWaterElectricityWs().getAllFloorAndUnit(new Gson().fromJson(new UserSessionManagement(this).getUserDetail(), User.class).getActivePropertyIdFk(), tracking_date, action, UploadWaterElectricityActivity.this, new TrackWaterElectricityWs.OnTrackWaterCallBack() {
            @Override
            public void onSuccess(String message, List<FloorUtilNoModel> list) {
                progressBar.setVisibility(View.GONE);
                if (floorSpinnerModels.size() > 0) floorSpinnerModels.clear();
                floorUtilNoModelList = new ArrayList<>();
                floorUtilNoModelList.addAll(list);
                hashMapAddListFloor();
                if (!action.equals("water_ppwsa") && !action.equals("electricity_edc")) {
                    if (hashMap.get(selectFloor) != null) {
                        if (Objects.requireNonNull(hashMap.get(selectFloor)).size() == 0) {
                            txtNoResultTv.setVisibility(View.VISIBLE);
                        } else {
                            txtNoResultTv.setVisibility(View.GONE);
                        }
                        UtilityWaterElectricityAdapter waterElectricityAdapter = new UtilityWaterElectricityAdapter(UploadWaterElectricityActivity.this, hashMap.get(selectFloor), isWater, tracking_date, clickAction, action, onClickUtil);
                        recyclerView.setAdapter(waterElectricityAdapter);
                    }
                }
            }

            @Override
            public void onFail(String message) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(UploadWaterElectricityActivity.this, message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void popupMessageToClearData() {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(UploadWaterElectricityActivity.this, R.style.DialogTheme);
        builder.setMessage(getString(R.string.are_you_sure_to_clear_all_record));
        builder.setNegativeButton(getResources().getString(R.string.cancel), null);
        builder.setPositiveButton(getResources().getString(R.string.yes), (dialog, which) -> {
            data.deleteAllRecordInFloorDetail(action);
            setResult(RESULT_OK);
            finish();
        });
        builder.show();
    }

    private final UtilityWaterElectricityAdapter.OnClickUtil onClickUtil = (utilNoModel) -> {
        Intent intent = new Intent(UploadWaterElectricityActivity.this, UtilityUploadDialog.class);
        intent.putExtra("utilNoModel", utilNoModel);
        intent.putExtra("action", action);
        intent.putExtra("purpose", purpose);
        intent.putExtra("trackingDate", eTrackingModel);
        intent.putExtra("click_action", clickAction);
        intent.putExtra("tracking_date", tracking_date);
        startActivityForResult(intent, REQUEST_UPLOAD);
    };

    private void hashMapAddListFloor() {
        if (floorUtilNoModelList != null) {
            for (FloorUtilNoModel floorUtilNoModel : floorUtilNoModelList) {
                List<UtilNoCommonAreaModel> list = new ArrayList<>();
                String total = "0";
                switch (action) {
                    case "water_track":
                        if (floorUtilNoModel.getUtilNoModelList() != null) {
                            list.addAll(floorUtilNoModel.getUtilNoModelList());
                            if (floorUtilNoModel.getUtilNoModelList().size() > 0) {
                                total = floorUtilNoModel.getUtilNoModelList().get(0).getTotal_recorded_amount_water();
                            } else {
                                total = "0";
                            }
                        }
                        break;
                    case "electricity_track":
                        if (floorUtilNoModel.getUtilNoModelList() != null) {
                            list.addAll(floorUtilNoModel.getUtilNoModelList());
                            if (floorUtilNoModel.getUtilNoModelList().size() > 0) {
                                total = floorUtilNoModel.getUtilNoModelList().get(0).getTotal_recorded_amount_electric();
                            } else {
                                total = "0";
                            }
                        }
                        break;
                    case "water_common":
                    case "water_ppwsa":
                        if (floorUtilNoModel.getWaterCommonList() != null) {
                            list.addAll(floorUtilNoModel.getWaterCommonList());
                            if (floorUtilNoModel.getWaterCommonList().size() > 0) {
                                total = floorUtilNoModel.getWaterCommonList().get(0).getTotal_recorded_amount_water();
                            } else {
                                total = "0";
                            }
                        }
                        break;
                    case "electricity_common":
                    case "electricity_edc":
                        if (floorUtilNoModel.getElectricityCommonList() != null) {
                            list.addAll(floorUtilNoModel.getElectricityCommonList());
                            if (floorUtilNoModel.getElectricityCommonList().size() > 0) {
                                total = floorUtilNoModel.getElectricityCommonList().get(0).getTotal_recorded_amount_electric();
                            } else {
                                total = "0";
                            }
                        }
                        break;
                }
                if (list.size() > 0) {
                    hashMap.put(floorUtilNoModel.getFloorName(), list);
                    mapNameFloor.put(floorUtilNoModel.getFloorName(), floorUtilNoModel.getFloorNoName());
                    if (!floorUtilNoModel.getFloorName().equals(TypeTrackingWaterElectricity.PPWSA_FLOOR) && !floorUtilNoModel.getFloorName().equals(TypeTrackingWaterElectricity.EDC_FLOOR)) {
                        hashMapListFloor.put(floorUtilNoModel.getFloorNoName(), total);
                    }
                }
            }

            // Water PPWSA and EDC
            if (action.equals("water_ppwsa")) {
                if (hashMap.get(TypeTrackingWaterElectricity.PPWSA_FLOOR) != null) {
                    UtilityWaterElectricityAdapter waterElectricityAdapter = new UtilityWaterElectricityAdapter(UploadWaterElectricityActivity.this, hashMap.get(TypeTrackingWaterElectricity.PPWSA_FLOOR), isWater, tracking_date, clickAction, action, onClickUtil);
                    recyclerView.setAdapter(waterElectricityAdapter);
                } else {
                    txtNoResultTv.setVisibility(View.VISIBLE);
                }
            } else if (action.equals("electricity_edc")) {
                if (hashMap.get(TypeTrackingWaterElectricity.EDC_FLOOR) != null) {
                    UtilityWaterElectricityAdapter waterElectricityAdapter = new UtilityWaterElectricityAdapter(UploadWaterElectricityActivity.this, hashMap.get(TypeTrackingWaterElectricity.EDC_FLOOR), isWater, tracking_date, clickAction, action, onClickUtil);
                    recyclerView.setAdapter(waterElectricityAdapter);
                } else {
                    txtNoResultTv.setVisibility(View.VISIBLE);
                }
            }

            for (Map.Entry<String, String> entry : mapNameFloor.entrySet()) {
                if (!entry.getKey().equals(TypeTrackingWaterElectricity.PPWSA_FLOOR) && !entry.getKey().equals(TypeTrackingWaterElectricity.EDC_FLOOR)) {
                    floorName.add(entry.getValue());
                }
            }
            for (Map.Entry<String, String> map : hashMapListFloor.entrySet()) {
                if (!map.getKey().equals(TypeTrackingWaterElectricity.PPWSA_FLOOR) && !map.getKey().equals(TypeTrackingWaterElectricity.EDC_FLOOR)) {
                    floorSpinnerModels.add(new FloorSpinnerModel(map.getKey(), map.getValue()));
                }
            }

            if (!action.equals("electricity_edc") && !action.equals("water_ppwsa")) {
                if (floorName.size() > 0) {
                    selectFloorName = floorName.get(0);
                    setSelectFloorWithAdapter(floorName.get(0));
                }
            }

            floorTv.setOnClickListener(view -> {
                Intent intent = new Intent(this, SelectFloorActivity.class);
                intent.putExtra("floorList", (Serializable) floorSpinnerModels);
                intent.putExtra("floorName", selectFloorName);
                startActivityForResult(intent, REQUEST_CODE_SELECT_FLOOR);
            });
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable @org.jetbrains.annotations.Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_UPLOAD && resultCode == RESULT_OK) {
            UtilNoCommonAreaModel utilNoCommonAreaModel = null;
            String tracking_date_upload = "";
            assert data != null;
            if (data.hasExtra("utilNoModel")) {
                utilNoCommonAreaModel = (UtilNoCommonAreaModel) data.getSerializableExtra("utilNoModel");
                tracking_date_upload = data.getStringExtra("tracking_date_upload");
                isClickSave = data.getBooleanExtra("is_click_save", true);
                if (!InternetConnection.checkInternetConnection(txtDateTv)) {
                    txtDateTv.setClickable(false);
                    txtDateTv.setTextColor(Utils.getColor(this, R.color.gray));
                    txtDateTv.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                }
            }
            List<UtilNoCommonAreaModel> list = new ArrayList<>();

            if (action.equals("water_ppwsa")) {
                for (UtilNoCommonAreaModel util : Objects.requireNonNull(hashMap.get(TypeTrackingWaterElectricity.PPWSA_FLOOR))) {
                    assert utilNoCommonAreaModel != null;
                    if (util.getId().equals(utilNoCommonAreaModel.getId())) {
                        util.setLastTrackDataWater(new LastTrackData(utilNoCommonAreaModel.getLastTrackDataWater().getLastTrackingDate(), utilNoCommonAreaModel.getLastTrackDataWater().getLastCurrentUsage(), utilNoCommonAreaModel.getLastTrackDataWater().getCurrent_usage(), true, tracking_date_upload));
                    }
                    list.add(util);
                }
                hashMap.put(TypeTrackingWaterElectricity.PPWSA_FLOOR, list);
                UtilityWaterElectricityAdapter waterElectricityAdapter = new UtilityWaterElectricityAdapter(UploadWaterElectricityActivity.this, hashMap.get(TypeTrackingWaterElectricity.PPWSA_FLOOR), isWater, tracking_date, clickAction, action, onClickUtil);
                recyclerView.setAdapter(waterElectricityAdapter);
                saveDataAgainDatabase(hashMap.get(TypeTrackingWaterElectricity.PPWSA_FLOOR), TypeTrackingWaterElectricity.PPWSA_FLOOR);
            } else if (action.equals("electricity_edc")) {
                for (UtilNoCommonAreaModel util : Objects.requireNonNull(hashMap.get(TypeTrackingWaterElectricity.EDC_FLOOR))) {
                    assert utilNoCommonAreaModel != null;
                    if (util.getId().equals(utilNoCommonAreaModel.getId())) {
                        util.setLastTrackDataElectricity(new LastTrackData(utilNoCommonAreaModel.getLastTrackDataElectricity().getLastTrackingDate(), utilNoCommonAreaModel.getLastTrackDataElectricity().getLastCurrentUsage(), utilNoCommonAreaModel.getLastTrackDataElectricity().getCurrent_usage(), true, tracking_date_upload));
                    }
                    list.add(util);
                }
                hashMap.put(TypeTrackingWaterElectricity.EDC_FLOOR, list);
                UtilityWaterElectricityAdapter waterElectricityAdapter = new UtilityWaterElectricityAdapter(UploadWaterElectricityActivity.this, hashMap.get(TypeTrackingWaterElectricity.EDC_FLOOR), isWater, tracking_date, clickAction, action, onClickUtil);
                recyclerView.setAdapter(waterElectricityAdapter);
                saveDataAgainDatabase(hashMap.get(TypeTrackingWaterElectricity.EDC_FLOOR), TypeTrackingWaterElectricity.EDC_FLOOR);

            } else {
                for (UtilNoCommonAreaModel util : Objects.requireNonNull(hashMap.get(selectFloor))) {
                    assert utilNoCommonAreaModel != null;
                    if (util.getId().equals(utilNoCommonAreaModel.getId())) {
                        if ("electricity_track".equals(action)) {
                            util.setLastTrackDataElectricity(new LastTrackData(utilNoCommonAreaModel.getLastTrackDataElectricity().getLastTrackingDate(), utilNoCommonAreaModel.getLastTrackDataElectricity().getLastCurrentUsage(), utilNoCommonAreaModel.getLastTrackDataElectricity().getCurrent_usage(), true, tracking_date_upload));
                        } else if ("water_track".equals(action)) {
                            util.setLastTrackDataWater(new LastTrackData(utilNoCommonAreaModel.getLastTrackDataWater().getLastTrackingDate(), utilNoCommonAreaModel.getLastTrackDataWater().getLastCurrentUsage(), utilNoCommonAreaModel.getLastTrackDataWater().getCurrent_usage(), true, tracking_date_upload));
                        } else {
                            if ("electricity_common".equals(action)) {
                                util.setLastTrackDataElectricity(new LastTrackData(utilNoCommonAreaModel.getLastTrackDataElectricity().getLastTrackingDate(), utilNoCommonAreaModel.getLastTrackDataElectricity().getLastCurrentUsage(), utilNoCommonAreaModel.getLastTrackDataElectricity().getCurrent_usage(), true, tracking_date_upload));
                            } else if ("water_common".equals(action)) {
                                util.setLastTrackDataWater(new LastTrackData(utilNoCommonAreaModel.getLastTrackDataWater().getLastTrackingDate(), utilNoCommonAreaModel.getLastTrackDataWater().getLastCurrentUsage(), utilNoCommonAreaModel.getLastTrackDataWater().getCurrent_usage(), true, tracking_date_upload));
                            }
                        }
                    }
                    list.add(util);
                }
                hashMap.put(selectFloor, list);
                UtilityWaterElectricityAdapter waterElectricityAdapter = new UtilityWaterElectricityAdapter(UploadWaterElectricityActivity.this, hashMap.get(selectFloor), isWater, tracking_date, clickAction, action, onClickUtil);
                recyclerView.setAdapter(waterElectricityAdapter);
                saveDataAgainDatabase(hashMap.get(selectFloor), selectFloor);
            }
        } else if (resultCode == RESULT_OK && requestCode == REQUEST_CODE_SELECT_FLOOR && data != null) {
            setSelectFloorWithAdapter(data.getStringExtra("floor_name"));
            selectFloorName = data.getStringExtra("floor_name");
        }
    }

    private void setSelectFloorWithAdapter(String floorName) {
        floorTv.setText(floorName);
        for (Map.Entry<String, String> entry : mapNameFloor.entrySet()) {
            if (floorName.equals(entry.getValue())) {
                selectFloor = entry.getKey();
            }
        }
        if (hashMap.get(selectFloor) != null) {
            if (Objects.requireNonNull(hashMap.get(selectFloor)).size() == 0) {
                txtNoResultTv.setVisibility(View.VISIBLE);
            } else {
                txtNoResultTv.setVisibility(View.GONE);
            }
            UtilityWaterElectricityAdapter waterElectricityAdapter = new UtilityWaterElectricityAdapter(UploadWaterElectricityActivity.this, hashMap.get(selectFloor), isWater, tracking_date, clickAction, action, onClickUtil);
            recyclerView.setAdapter(waterElectricityAdapter);

        }
    }

    private void saveDataAgainDatabase(List<UtilNoCommonAreaModel> list, String floor) {
        // save data again to check data already have
        if (!InternetConnection.checkInternetConnection(recyclerView)) {
            for (int i = 0; i < floorUtilNoModelList.size(); i++) {
                if (floorUtilNoModelList.get(i).getFloorName().equals(floor)) {
                    FloorUtilNoModel floorUtilNoModel = new FloorUtilNoModel();
                    floorUtilNoModel.setFloorName(floor);
                    floorUtilNoModel.setFloorNoName(mapNameFloor.get(floor));
                    List<UtilNoCommonAreaModel> utilNoCommonAreaModelsWater = floorUtilNoModelList.get(i).getWaterCommonList();
                    List<UtilNoCommonAreaModel> utilNoCommonAreaModelsUtil = floorUtilNoModelList.get(i).getUtilNoModelList();
                    List<UtilNoCommonAreaModel> utilNoCommonAreaModelsElectricity = floorUtilNoModelList.get(i).getElectricityCommonList();
                    if (action.equals(TypeTrackingWaterElectricity.WATER_TRACK)) {
                        floorUtilNoModel.setUtilNoModelList(list);
                        floorUtilNoModel.setWaterCommonList(utilNoCommonAreaModelsWater);
                        floorUtilNoModel.setElectricityCommonList(utilNoCommonAreaModelsElectricity);
                    } else if (action.equals(TypeTrackingWaterElectricity.WATER_COMMON)) {
                        floorUtilNoModel.setWaterCommonList(list);
                        floorUtilNoModel.setUtilNoModelList(utilNoCommonAreaModelsUtil);
                        floorUtilNoModel.setElectricityCommonList(utilNoCommonAreaModelsElectricity);
                    } else if (action.equals(TypeTrackingWaterElectricity.WATER_PPWSA)) { // ppwsa
                        floorUtilNoModel.setWaterCommonList(list);
                        floorUtilNoModel.setUtilNoModelList(utilNoCommonAreaModelsUtil);
                        floorUtilNoModel.setElectricityCommonList(utilNoCommonAreaModelsElectricity);
                    } else if (action.equals(TypeTrackingWaterElectricity.ELECTRICITY_TRACK)) {
                        floorUtilNoModel.setUtilNoModelList(list);
                        floorUtilNoModel.setElectricityCommonList(utilNoCommonAreaModelsElectricity);
                        floorUtilNoModel.setWaterCommonList(utilNoCommonAreaModelsWater);
                    } else if (action.equals(TypeTrackingWaterElectricity.ELECTRICITY_COMMON)) {
                        floorUtilNoModel.setElectricityCommonList(list);
                        floorUtilNoModel.setWaterCommonList(utilNoCommonAreaModelsWater);
                        floorUtilNoModel.setUtilNoModelList(utilNoCommonAreaModelsUtil);
                    } else if (action.equals(TypeTrackingWaterElectricity.ELECTRICITY_EDC)) { // EDC
                        floorUtilNoModel.setElectricityCommonList(list);
                        floorUtilNoModel.setUtilNoModelList(utilNoCommonAreaModelsUtil);
                        floorUtilNoModel.setWaterCommonList(utilNoCommonAreaModelsWater);
                    }
                    floorUtilNoModelList.set(i, floorUtilNoModel);
                }
            }
            TrackWaterElectricityDatabase trackWaterElectricityDatabase = new TrackWaterElectricityDatabase(UploadWaterElectricityActivity.this);
            String storeDate = new Gson().toJson(floorUtilNoModelList);
            trackWaterElectricityDatabase.deleteAllFloorHaveRecord(action);
            trackWaterElectricityDatabase.insertFloorHaveData(storeDate, action);
        }
    }

    @Override
    public void onBackPressed() {
        BackButton();
    }

    private void BackButton() {
        Intent intent = getIntent();
        intent.putExtra("is_clicked_save", isClickSave);
        setResult(RESULT_OK, intent);
        finish();
    }
}
