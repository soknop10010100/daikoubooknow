package com.eazy.daikou.ui.home.hrm.payroll.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.base.MockUpData
import com.eazy.daikou.helper.DateUtil
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.hr.PayrollList


class PayRollAdapter(private val listPayroll: ArrayList<PayrollList>, private val itemClick: ItemClickListener) : RecyclerView.Adapter<PayRollAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.layout_adapter_pay_roll, parent, false)
        return ViewHolder(v)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data : PayrollList = listPayroll[position]
        holder.overNoTv.text = if (data.order_no != "") data.order_no else "---"
        holder.paidDateTv.text = if (data.paid_date != "") data.paid_date else "---"
        holder.paymentTypeTv.text = if (data.payment_type != "") data.payment_type else "---"
        if (MockUpData.IS_CLICK_HIDE){
            holder.totalNetSalaryTv.text = "***"
        } else {
             holder.totalNetSalaryTv.text = if (data.net_salary != "")  data.net_salary +" "+ data.currency_code  else "0"+" "+data.currency_symbol
        }
        holder.itemView.setOnClickListener {
            itemClick.onLoadSuccess(data)
        }
    }
    override fun getItemCount(): Int {
        return listPayroll.size
    }

    interface ItemClickListener{
        fun onLoadSuccess(data: PayrollList)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var overNoTv : TextView = view.findViewById(R.id.overNoTv)
        var paidDateTv : TextView = view.findViewById(R.id.paidDateTv)
        var paymentTypeTv : TextView = view.findViewById(R.id.paymentTypeTv)
        var totalNetSalaryTv : TextView = view.findViewById(R.id.netSalaryTv)
    }

    fun clear() {
        val size: Int = listPayroll.size
        if (size > 0) {
            for (i in 0 until size) {
                listPayroll.removeAt(0)
            }
            notifyItemRangeRemoved(0, size)
        }
    }

}