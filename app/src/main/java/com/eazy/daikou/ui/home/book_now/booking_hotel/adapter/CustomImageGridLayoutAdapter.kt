package com.eazy.daikou.ui.home.book_now.booking_hotel.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.helper.Utils

class CustomImageGridLayoutAdapter(private val action : String, private val context : Context, private val list: List<String>, private val onClickListener : OnClickCallBackLister) : RecyclerView.Adapter<CustomImageGridLayoutAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.show_only_image_view_layout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val item : String = list[position]
        if (item != null){
            Glide.with(holder.image).load(item).into(holder.image)

            holder.totalImage.text = String.format("%s %s", "+", list.size.toString())

            holder.totalImage.visibility =
                if(action == "show_all_image") View.GONE else{ if (position == 4) View.VISIBLE else View.GONE}

            val heightImg: Int = if (action != "show_all_image"){
                if (list.size == 1 || list.size == 2){
                    Utils.setHeightOfScreen(context, 4.0)
                } else {
                    Utils.dpToPx(context, 130)
                }
            } else {
                Utils.dpToPx(context, 130)
            }
            holder.imageLayout.layoutParams.height = heightImg

            holder.itemView.setOnClickListener (CustomSetOnClickViewListener{
                onClickListener.onClickCallBack(item)
            })
        }
    }

    override fun getItemCount(): Int {
        return if (action == "show_all_image"){
            list.size
        } else {
            if (list.size > 5){
                5
            } else {
                list.size
            }
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val image : ImageView = itemView.findViewById(R.id.image)
        val totalImage : TextView = itemView.findViewById(R.id.totalImage)
        val imageLayout : CardView = itemView.findViewById(R.id.imageLayout)
    }

    interface OnClickCallBackLister{
        fun onClickCallBack(value : String)
    }
}