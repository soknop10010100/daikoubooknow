package com.eazy.daikou.ui.home.hrm.resignation

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.request_data.request.hr_ws.ResignationWs
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.hr.ApproveResignType
import com.eazy.daikou.model.hr.ItemResignationModel
import com.eazy.daikou.model.profile.User
import com.google.gson.Gson
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class CreateResignRequestActivity : BaseActivity() {

    private lateinit var progressBar: ProgressBar
    private lateinit var btnBack : TextView
    private lateinit var titleHeader : TextView
    private lateinit var approveTv : TextView
    private lateinit var TypeResignTv : TextView
    private lateinit var imgCover : ImageView
    private lateinit var lastDayTv : TextView
    private lateinit var phoneTv : EditText
    private lateinit var reasonTv : EditText
    private lateinit var btnSave : TextView
    private lateinit var user : User
    private var approveByList : ArrayList<ApproveResignType> = ArrayList()
    private var resignTypeList : ArrayList<ApproveResignType> = ArrayList()

    private var REQUEST_CODE_ITEM = 843
    private var isApproveAction = true
    private var approveId = ""
    private var resignTypeId = ""
    private var accountBusinessKey = ""
    private var userBusinessKey : String = ""
    private var dateFormat : String = ""
    private var approveUserKey = ""
    private var typeResign = ""
    private lateinit var lastSelectedCalendar : Calendar


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_resign_request)

        initView()

        initData()

        initAction()

    }

    private fun initView(){
        progressBar = findViewById(R.id.progressItem)
        btnBack = findViewById(R.id.btn_back)
        titleHeader = findViewById(R.id.titleHeader)
        imgCover = findViewById(R.id.img_cover)
        approveTv = findViewById(R.id.approveTv)
        TypeResignTv = findViewById(R.id.TypeResignTv)
        lastDayTv = findViewById(R.id.lastDayTv)
        phoneTv = findViewById(R.id.phoneTv)
        reasonTv = findViewById(R.id.reasonTv)
        btnSave = findViewById(R.id.btnSave)

        progressBar.visibility = View.GONE
        lastSelectedCalendar = Calendar.getInstance()

    }

    private fun initData(){
        user = Gson().fromJson(UserSessionManagement(this).userDetail, User::class.java)
        val businessKy = user.userBusinessKey
        val accountKey = user.activeAccountBusinessKey
        if (businessKy != null || accountKey != null){
            userBusinessKey = businessKy
            accountBusinessKey = accountKey
        }
        // Just Back Listener Enabled Button Check On List
        val intent = Intent()
        intent.putExtra("isClickedFirst", true)
        setResult(RESULT_OK, intent)

    }

    private fun initAction(){
        titleHeader.visibility = View.VISIBLE
        titleHeader.text = getString(R.string.create_resignation)
        imgCover.setImageResource(R.drawable.ic_cv_home_resignation)
        btnBack.setOnClickListener { finish() }

        approveTv.setOnClickListener {
            isApproveAction = true
            if (approveByList.size > 0) {
                getItemOnCategory(approveByList,"approveList")

            } else {
                Utils.customToastMsgError(this, "Approve by is empty !", false)
            }
        }

        TypeResignTv.setOnClickListener {
            isApproveAction = false
            if (resignTypeList.size > 0) {
                getItemOnCategory(resignTypeList, "typeList")
            } else {
                Utils.customToastMsgError(this, "Resign Type is empty !", false)
            }
        }

        lastDayTv.setOnClickListener { calendarDate() }

        btnSave.setOnClickListener {  onCrateListResignation()}

        requestServiceItemAPI("")

    }

    private fun requestServiceItemAPI(searchApprove : String ){
        ResignationWs().getCategoryItemResignWs(this, userBusinessKey, searchApprove,callBackListItem)
    }

    private val callBackListItem : ResignationWs.OnListItemResignCallBackListener = object : ResignationWs.OnListItemResignCallBackListener{
        override fun onSuccessful(itemResignationModel: ItemResignationModel) {
            progressBar.visibility = View.GONE
            if (itemResignationModel.approver.isNotEmpty())   approveByList.addAll(itemResignationModel.approver)

            if (itemResignationModel.resignation_types.isNotEmpty())   resignTypeList.addAll(itemResignationModel.resignation_types)
        }

        override fun onFailed(error: String) {
            progressBar.visibility = View.GONE
            Utils.customToastMsgError(this@CreateResignRequestActivity, error, false)
        }

    }

    @SuppressLint("SimpleDateFormat")
    private fun calendarDate(){
        val alertLayout = LayoutInflater.from(this).inflate(R.layout.activity_calendar_view_dialog, null)
        val textTitle = alertLayout.findViewById<TextView>(R.id.txtTitle)
        val dialog = AlertDialog.Builder(this, R.style.AlertShape)
        dialog.setView(alertLayout)
        dialog.setCancelable(false)
        val alertDialog = dialog.show()
        textTitle.text = getString(R.string.last_day_of_employee)
        alertLayout.findViewById<ImageView>(R.id.btnClose).setOnClickListener{alertDialog.dismiss()}
        val calendarViewAlert: CalendarView = alertLayout.findViewById(R.id.date_picker)

        // Set Select Date
        if (lastSelectedCalendar != null)     calendarViewAlert.setDate (lastSelectedCalendar.timeInMillis, true, true)

        calendarViewAlert.setOnDateChangeListener { _: CalendarView, year: Int, month: Int, dayOfMonth: Int ->
            val checkCalendar = Calendar.getInstance()
            checkCalendar[year, month] = dayOfMonth

            val calendar = Calendar.getInstance()
            calendar[year, month] = dayOfMonth

            lastSelectedCalendar = calendar
            val formattedDate = SimpleDateFormat("yyyy-MM-dd")
            val formatDateForDisplay = SimpleDateFormat("dd-MMMM-yyyy")
            dateFormat = formattedDate.format(calendar.time)
            lastDayTv.text = formatDateForDisplay.format(calendar.time)

            alertDialog.dismiss()
        }
     //   calendarViewAlert.minDate = System.currentTimeMillis() - 1000

    }

    private fun onCrateListResignation(){
        val hashMap : HashMap<String, Any> = HashMap()
        hashMap["user_business_key"] = userBusinessKey
        hashMap["account_business_key"] = accountBusinessKey
        hashMap["approver_user_business_key"] = approveUserKey
        hashMap["last_date"] = dateFormat
        hashMap["type"] = typeResign
        hashMap["contact"] = phoneTv.text.toString()
        hashMap["reason"] = reasonTv.text.toString()


        progressBar.visibility = View.VISIBLE
        ResignationWs().getCreateListResignationWs(this, hashMap, onclickCallBackListCreate)
    }

    private val onclickCallBackListCreate : ResignationWs.OnCallBackCreateResignationListener = object : ResignationWs.OnCallBackCreateResignationListener{
        override fun onLoadSuccessFull(message: String) {
            progressBar.visibility = View.GONE
            Utils.customToastMsgError(this@CreateResignRequestActivity, message, true)
            setResult(RESULT_OK)
            finish()
        }

        override fun onLoadFailed(message: String) {
            progressBar.visibility = View.GONE
            Utils.customToastMsgError(this@CreateResignRequestActivity, message, false)

        }

    }


    private fun getItemOnCategory(itemList : ArrayList<ApproveResignType>, action : String){
        val intent = Intent(this@CreateResignRequestActivity,ItemCategoryResignActivity::class.java)
        intent.putExtra("list_item_resign", itemList)
        intent.putExtra("action", action)
        if (isApproveAction) {
            intent.putExtra("id", approveId)
        } else {
            intent.putExtra("id", resignTypeId)
        }
        startActivityForResult(intent, REQUEST_CODE_ITEM)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK && requestCode == REQUEST_CODE_ITEM){
            if (data != null){
                val id = data.getStringExtra("id").toString()
                val name = data.getStringExtra("name").toString()
                val approveUserBusiness = data.getStringExtra("approver_user_business_key").toString()
                if (isApproveAction){
                    approveTv.text = name
                    approveId = id
                    approveUserKey = approveUserBusiness
                    setClickOnItem(id, approveByList)
                } else {
                    TypeResignTv.text = name
                    typeResign = name
                    resignTypeId = id
                    setClickOnItem(id, resignTypeList)
                }
            }
        }
    }

    private fun setClickOnItem(id : String, itemCategoryResignList : ArrayList<ApproveResignType>){
        for (itemList in itemCategoryResignList){
            itemList.isClick = itemList.id == id
        }
    }

}