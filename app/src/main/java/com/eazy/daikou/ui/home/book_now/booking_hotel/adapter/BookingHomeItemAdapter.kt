package com.eazy.daikou.ui.home.book_now.booking_hotel.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.request_data.request.book_now_ws.RequestHashMapData
import com.eazy.daikou.request_data.static_data.StaticUtilsKey
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.home.SubItemHomeModel
import com.squareup.picasso.Picasso

class BookingHomeItemAdapter(private var activity: Activity, private val action: String, private val bookingList: ArrayList<SubItemHomeModel>, private val propertyClick: PropertyClick) : RecyclerView.Adapter<BookingHomeItemAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.booking_main_item_layout, parent, false)
        if (action == "top_destination" || action == "related_hotel_detail"){
            val itemViewParam = LinearLayout.LayoutParams(Utils.getWidth(activity) / 2, LinearLayout.LayoutParams.WRAP_CONTENT)
            view.layoutParams = itemViewParam
        }
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val propertyModel = bookingList[position]
        if (propertyModel != null) {
            if (propertyModel.imageUrl != null) {
                Picasso.get().load(propertyModel.imageUrl).into(holder.imageView)
            } else {
                Picasso.get().load(R.drawable.no_image).into(holder.imageView)
            }

            //Set price
            Utils.setTextStrikeStyle(holder.priceDiscount)
            if (propertyModel.priceOriginalBeforeDiscount != null) {
                holder.priceDiscount.text = propertyModel.priceOriginalBeforeDiscount
            } else {
                holder.priceDiscount.visibility = View.GONE
            }

            Utils.setValueOnText(holder.priceTv, propertyModel.priceVal)
            holder.hotelName.text = if (propertyModel.name != null) propertyModel.name else ""

            holder.ratingStar.rating =
                if (propertyModel.rating != null && propertyModel.rating != ""){
                    if (propertyModel.rating!!.toFloat() >= 0){
                        propertyModel.rating!!.toFloat()
                    } else {
                        0.0F
                    }
                } else {
                    0.0F
                }

            when (propertyModel.action) {
                "bestseller_listing", "all_nearby_hotels", "featuring_hotel", "related_hotel_detail", "see_all_function_hotel"-> {
                    holder.ratingStar.visibility = View.VISIBLE
                    holder.priceLayout.visibility = View.VISIBLE
                    Utils.setValueOnText(holder.address, propertyModel.address)
                    Utils.setValueOnText(holder.distanceTv, propertyModel.distance)

                    holder.iconWishlist.visibility = View.VISIBLE
                    Glide.with(holder.iconWishlist).load(if (propertyModel.isWishlist)    R.drawable.ic_my_favourite else R.drawable.ic_favorite_border).into(holder.iconWishlist)
                    holder.mainLayout.setPadding(5,0,0,0)

                    if (propertyModel.action == "all_nearby_hotels"){
                        holder.distanceTv.visibility = if (propertyModel.distance != null) View.VISIBLE else View.INVISIBLE
                    } else {
                        if (propertyModel.action == "see_all_function_hotel"){
                            holder.distanceTv.visibility = if (propertyModel.distance != null) View.VISIBLE else View.GONE
                        } else {
                            holder.distanceTv.visibility = View.GONE
                        }
                    }

                    // item can book check
                    holder.availableLayout.visibility = if (propertyModel.isBookingAvailable) View.GONE else View.VISIBLE
                }
                else -> {
                    holder.ratingStar.visibility = View.GONE
                    holder.priceLayout.visibility = View.GONE
                    holder.distanceTv.visibility = View.GONE
                    Utils.setValueOnText(holder.address, propertyModel.totalHotel)
                    holder.address.append(String.format("%s %s", " ", RequestHashMapData.categoryTitle(holder.address.context)[StaticUtilsKey.category]))
                    holder.iconWishlist.visibility = View.GONE

                    if (propertyModel.action == "top_destination")  holder.cardViewLayout.layoutParams.height = Utils.dpToPx(activity, 110)

                    // item can book check
                    holder.availableLayout.visibility = View.GONE
                }
            }

            holder.cardViewLayout.setOnClickListener(
                CustomSetOnClickViewListener{
                    propertyClick.onBookingClick(propertyModel)
                }
            )

            holder.iconWishlist.setOnClickListener(
                CustomSetOnClickViewListener{
                    propertyClick.onBookingWishlistClick(propertyModel)
                }
            )

            if (propertyModel.action == "featuring_hotel" || action == "detail_all_location") {
                val heightImg = Utils.dpToPx(activity, 220)
                if (position % 3 == 0) {
                    holder.cardViewLayout.layoutParams.height = heightImg + Utils.dpToPx(activity, 10)
                } else{
                    holder.cardViewLayout.layoutParams.height = heightImg / 2
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return bookingList.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val hotelName: TextView = itemView.findViewById(R.id.property_name)
        val address: TextView = itemView.findViewById(R.id.addressTv)
        val imageView: ImageView = itemView.findViewById(R.id.image)
        val priceLayout : LinearLayout = itemView.findViewById(R.id.priceLayout)
        val ratingStar : RatingBar = itemView.findViewById(R.id.ratingStar)
        val priceTv : TextView =  itemView.findViewById(R.id.priceTv)
        val iconWishlist : ImageView = itemView.findViewById(R.id.iconWishlist)
        val mainLayout : LinearLayout = itemView.findViewById(R.id.mainLayout)
        val cardViewLayout : CardView = itemView.findViewById(R.id.cardViewLayout)
        val priceDiscount : TextView = itemView.findViewById(R.id.priceDiscount)
        val distanceTv : TextView = itemView.findViewById(R.id.distanceTv)
        val availableLayout : RelativeLayout = itemView.findViewById(R.id.availableLayout)
    }

    interface PropertyClick {
        fun onBookingClick(propertyModel: SubItemHomeModel?)
        fun onBookingWishlistClick(propertyModel: SubItemHomeModel?)
    }

    fun clear() {
        val size: Int = bookingList.size
        if (size > 0) {
            for (i in 0 until size) {
                bookingList.removeAt(0)
            }
            notifyItemRangeRemoved(0, size)
        }
    }
}