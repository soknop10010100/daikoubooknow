package com.eazy.daikou.ui.home.book_now.booking_hotel

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.base.BaseFragment
import com.eazy.daikou.base.MockUpData
import com.eazy.daikou.request_data.request.book_now_ws.BookingHotelWS
import com.eazy.daikou.request_data.request.book_now_ws.RequestHashMapData
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.booking_hotel.HotelBookingDetailModel
import com.eazy.daikou.model.booking_hotel.LocationHotelModel
import com.eazy.daikou.model.home.MainItemHomeModel
import com.eazy.daikou.model.home.SubItemHomeModel
import com.eazy.daikou.model.home.UserDeactivated
import com.eazy.daikou.ui.home.book_now.booking_hotel.adapter.BookingHomeItemAdapter

@SuppressLint("ValidFragment")
class HotelBookingSearchHotelFragment : BaseFragment {

    private lateinit var progressBar: ProgressBar
    private lateinit var recyclerView: RecyclerView
    // Search Hotel
    private lateinit var selectLocationTv : TextView
    private lateinit var selectRoomTv : TextView
    private lateinit var startRangDateTv : TextView
    private var locationId = ""
    private var selectItemGuest = ""
    private var locationTitle = ""
    private var room = "1"
    private var adults = "1"
    private var children = "0"
    private var startDate = ""
    private var endDate = ""
    private lateinit var btnSearchTv : TextView
    private var subItemHomeModelList = ArrayList<SubItemHomeModel>()
    private lateinit var bookingHomeItemAdapter: BookingHomeItemAdapter
    private var locationHotelModelList: ArrayList<LocationHotelModel> = ArrayList()
    private lateinit var linearLayoutManager: LinearLayoutManager
    private var eazyhotelUserId = ""
    private var hotelId = "" // Get to store hotel for call back wish list

    @SuppressLint("ValidFragment")
    constructor()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view =  inflater.inflate(R.layout.fragment_hotel_booking_search_location, container, false)

        initView(view)

        initAction()

        return view
    }

    private fun initView(view: View){
        progressBar = view.findViewById(R.id.progressItem)
        progressBar.visibility = View.GONE
        recyclerView = view.findViewById(R.id.recyclerView)

        selectLocationTv = view.findViewById(R.id.selectLocationTv)
        selectRoomTv = view.findViewById(R.id.selectRoomTv)
        startRangDateTv = view.findViewById(R.id.startDateTv)
        btnSearchTv = view.findViewById(R.id.btnSearch)

        view.findViewById<ImageView>(R.id.iconBack).visibility = View.GONE
        val titleToolbar : TextView = view.findViewById(R.id.titleToolbar)
        titleToolbar.text = resources.getString(R.string.search_hotel).uppercase()

        val searchHotelLayout : RelativeLayout = view.findViewById(R.id.searchLayout)
        Utils.setBgTint(searchHotelLayout, R.color.transparent)
    }

    private fun initAction(){
        linearLayoutManager = LinearLayoutManager(mActivity)
        recyclerView.layoutManager = linearLayoutManager
        recyclerView.isNestedScrollingEnabled = false
        recyclerView.setHasFixedSize(false)

        bookingHomeItemAdapter = BookingHomeItemAdapter(mActivity, "bestseller_listing", subItemHomeModelList, onClickSubItemBookingListener)
        recyclerView.adapter = bookingHomeItemAdapter

        locationHotelModelList = RequestHashMapData.getStoreLocationHotelData()
        if (locationHotelModelList.size == 0) requestItemLocationList()

        eazyhotelUserId = Utils.validateNullValue(MockUpData.getEazyHotelUserId(UserSessionManagement(mActivity)))

        requestItemHotelList()

        btnSearchTv.setOnClickListener(
            CustomSetOnClickViewListener{
                initStartSearch(true, locationId, locationTitle)
            }
        )

        startRangDateTv.setOnClickListener(
            CustomSetOnClickViewListener{
                startEndDateRang()
            }
        )

        selectLocationTv.setOnClickListener(
            CustomSetOnClickViewListener{
                startSelectCategory("select_location")
            }
        )

        selectRoomTv.setOnClickListener(
            CustomSetOnClickViewListener{
                startSelectCategory("select_room")
            }
        )

        checkValidateButtonPay()

    }

    private fun requestItemLocationList(){
        BookingHotelWS().getLocationHotelBooking(mActivity, onCallBackItemListener)
    }

    private fun requestItemHotelList(){
        BookingHotelWS().getHotelBookingAll(
            mActivity,
            "hotel_home_page", "hotel",
            RequestHashMapData.hashMapHomePageData(1, "hotel", eazyhotelUserId, BaseActivity.latitude.toString(), BaseActivity.longitude.toString()),
            onCallBackItemListener
        )
    }

    private val onCallBackItemListener = object : BookingHotelWS.OnCallBackMyProfileListener{
        override fun onSuccessLocation(locationHotelModel: ArrayList<LocationHotelModel>) {
            progressBar.visibility = View.GONE
            locationHotelModelList.clear()
            locationHotelModelList.addAll(locationHotelModel)
        }

        override fun onSuccessShowHotelDetail(hotelDetailItemModel: HotelBookingDetailModel, mainItemHomeModelLIst: ArrayList<SubItemHomeModel>) {}

        @SuppressLint("NotifyDataSetChanged")
        override fun onSuccessShowHotel(action: String, listImageSlider : MutableList<String>, locationHotelModel: ArrayList<MainItemHomeModel>, itemHomeList: ArrayList<SubItemHomeModel>, userDeactivated : UserDeactivated) {
            progressBar.visibility = View.GONE
            subItemHomeModelList.clear()
            for (item in locationHotelModel){
                if (item.mainAction == "bestseller_listing"){
                    subItemHomeModelList.addAll(item.subItemHomeModelList)
                    bookingHomeItemAdapter.notifyDataSetChanged()
                    return
                }
            }
        }

        override fun onFailed(message: String) {
            progressBar.visibility = View.GONE
            Utils.customToastMsgError(mActivity, message, false)
        }

    }

    private fun initStartSearch(isClickSearch : Boolean, locationId : String, locationHotel : String){
        val value = String.format("%s ∙ %s - %s", locationHotel, startDate, endDate)
        val hashMapData : HashMap<String, Any> = HashMap()
        hashMapData["value"] = if(isClickSearch) value else locationHotel
        hashMapData["location_list"] = locationHotelModelList
        hashMapData["location_id"] = locationId
        hashMapData["location_title"] = locationHotel
        hashMapData["adults"] = if(isClickSearch) adults else ""
        hashMapData["room"] = if(isClickSearch) room else ""
        hashMapData["children"] = if(isClickSearch) children else ""
        hashMapData["start_date"] =  if(isClickSearch) startDate else ""
        hashMapData["end_date"] = if(isClickSearch) endDate else ""
        hashMapData["guest_num"] = if (isClickSearch) selectItemGuest else ""
        hashMapData["from_action"] =  if (isClickSearch) "search_list" else "hotel_location_detail"
        hashMapData["category"] = "hotel"
        RequestHashMapData.storeHotelItemData(hashMapData)

        val intent = Intent(mActivity, HotelSearchShowHotelBookingActivity::class.java)
        startActivity(intent)
    }

    private fun startEndDateRang(){
        val selectLocationBookingFragment = SelectStartEndDateRangBottomsheet.newInstance(startDate, endDate)
        selectLocationBookingFragment.initListener(onCallBackDateListener)
        selectLocationBookingFragment.show(childFragmentManager, "fragment")
    }

    private fun startSelectCategory(action : String){
        val selectLocationBookingFragment : SelectLocationBookingFragment = if (action == "select_location") {
            SelectLocationBookingFragment().newInstance(locationId, action, locationHotelModelList)
        } else {
            SelectLocationBookingFragment().newInstance(room, adults,children, action)
        }
        selectLocationBookingFragment.initListener(onCallBackListener)
        selectLocationBookingFragment.show(childFragmentManager, "fragment")
    }

    private val onCallBackDateListener = object : SelectStartEndDateRangBottomsheet.OnClickCallBackListener{
        override fun onClickSelect(getStartDate: String, getEndDate : String) {
            startDate = getStartDate
            endDate = getEndDate
            startRangDateTv.text = String.format("%s - %s", getStartDate, getEndDate)

            checkValidateButtonPay()
        }
    }

    private val onCallBackListener = object : SelectLocationBookingFragment.OnClickCallBackListener{
        override fun onClickSelectLocation(titleHeader: LocationHotelModel) {
            selectLocationTv.text = titleHeader.location_name
            locationId = titleHeader.location_id.toString()
            locationTitle = titleHeader.location_name!!

            checkValidateButtonPay()
        }

        override fun onClickSelect(titleHeader: String, r: String, ad: String, ch: String) {
            selectRoomTv.text = titleHeader
            selectItemGuest = titleHeader
            room = r
            adults = ad
            children = ch

            checkValidateButtonPay()
        }
    }

    private var onClickSubItemBookingListener = object : BookingHomeItemAdapter.PropertyClick {
        override fun onBookingClick(propertyModel: SubItemHomeModel?) {
            if (propertyModel != null) {
                hotelId = Utils.validateNullValue(propertyModel.id)
                val intent = Intent(mActivity, HotelBookingDetailHotelActivity::class.java)
                intent.putExtra("hotel_name", propertyModel.name)
                intent.putExtra("category", propertyModel.titleCategory)
                intent.putExtra("hotel_image", propertyModel.imageUrl)
                intent.putExtra("hotel_id", propertyModel.id)
                intent.putExtra("price", propertyModel.priceVal)
                intent.putExtra("start_date", startDate)
                intent.putExtra("end_date", endDate)
                intent.putExtra("adults", adults)
                intent.putExtra("children", children)
                resultLauncher.launch(intent)
            }
        }

        override fun onBookingWishlistClick(propertyModel: SubItemHomeModel?) {
            if (propertyModel != null) {
                editStatusHotelWs(propertyModel, "wishlist")
            }
        }
    }

    private val resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
        if (result.resultCode == Activity.RESULT_OK) {
            //Call back when click wishlist hotel on detail screen
            for (item in subItemHomeModelList){
                if (item.id == hotelId){
                    item.isWishlist = !item.isWishlist
                    break
                }
            }
            bookingHomeItemAdapter.notifyDataSetChanged()
        }
    }

    private fun checkValidateButtonPay(){
        if (isEnableButtonPay()){
            btnSearchTv.isEnabled = true
            Utils.setBgTint(btnSearchTv, R.color.gray)
            Utils.setBgTint(btnSearchTv, R.color.color_book_now)
        } else {
            btnSearchTv.isEnabled = false
            Utils.setBgTint(btnSearchTv, R.color.gray)
        }
    }

    private fun isEnableButtonPay() : Boolean{
        if (locationId == "" || selectItemGuest == "" || startDate == "" || endDate == ""){
            return false
        }
        return true
    }

    private fun editStatusHotelWs(propertyModel: SubItemHomeModel, status : String){
        progressBar.visibility = View.VISIBLE
        RequestHashMapData.editStatusHotelWs(mActivity, progressBar, propertyModel.id!!, propertyModel.isWishlist,
            status, eazyhotelUserId, "hotel", object : RequestHashMapData.Companion.OnSuccessEditStatusListener{
                @SuppressLint("NotifyDataSetChanged")
                override fun onSuccess(msg: String) {
                    propertyModel.isWishlist = !propertyModel.isWishlist

                    bookingHomeItemAdapter.notifyDataSetChanged()
                }

            })
    }
}