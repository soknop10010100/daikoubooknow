package com.eazy.daikou.ui.home.book_now.booking_hotel.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.model.home.HomeViewModel

class PaymentMethodOnlineAdapter(private val listName: List<HomeViewModel>) : RecyclerView.Adapter<PaymentMethodOnlineAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.payment_method_online_option, parent, false)
        return ViewHolder(v)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBindingView(listName[position])
    }

    override fun getItemCount(): Int {
        return listName.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var nameTv : TextView = view.findViewById(R.id.nameTv)
        var iconBank : ImageView = view.findViewById(R.id.icon_bank)
        fun onBindingView(item : HomeViewModel){
            nameTv.text = if (item.name != null) item.name else "- - -"
            Glide.with(iconBank).load(if (item.drawable != null) item.drawable else R.drawable.no_image).into(iconBank)
        }
    }

    interface ItemClickOnListener{
        fun onItemClick(homeViewModel : HomeViewModel)
    }

}