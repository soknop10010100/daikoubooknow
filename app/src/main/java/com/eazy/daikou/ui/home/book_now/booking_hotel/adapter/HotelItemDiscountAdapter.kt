package com.eazy.daikou.ui.home.book_now.booking_hotel.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.home.SubItemHomeModel
import com.squareup.picasso.Picasso

class HotelItemDiscountAdapter(private val list: List<SubItemHomeModel>, private val onClickItemListener: OnClickItemListener) :
    RecyclerView.Adapter<HotelItemDiscountAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.custom_item_discount_book_now,parent,false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBindingView(list[position], onClickItemListener)
    }

    class ViewHolder(itemView: View):RecyclerView.ViewHolder(itemView){
        private val tvOriginalPrice: TextView = itemView.findViewById(R.id.priceTv)
        private val imageView = itemView.findViewById<ImageView>(R.id.image)

        private val ratingStar: RatingBar = itemView.findViewById(R.id.ratingStar)
        private val dateBookBetweenTv = itemView.findViewById<TextView>(R.id.dateBookBetweenTv)
        private val iconWishlist : ImageView = itemView.findViewById(R.id.iconWishlist)

        fun onBindingView(item: SubItemHomeModel, onClickItemListener: OnClickItemListener){

            itemView.findViewById<CardView>(R.id.cardViewLayout).setOnClickListener (CustomSetOnClickViewListener{
                onClickItemListener.onItemClick(item)
            })

            Utils.setValueOnText(itemView.findViewById(R.id.property_name), item.name)

            Utils.setTextStrikeStyle(tvOriginalPrice)
            if (item.priceOriginalBeforeDiscount != null) {
                tvOriginalPrice.text = item.priceOriginalBeforeDiscount
            } else {
                tvOriginalPrice.visibility = View.GONE
            }

            //set discount price
            Utils.setValueOnText(itemView.findViewById(R.id.priceDiscount), item.priceVal)

            dateBookBetweenTv.text = ""

            ratingStar.rating =
                if (item.rating != null && item.rating != ""){
                    if (item.rating!!.toFloat() >= 0){
                        item.rating!!.toFloat()
                    } else {
                        0.0F
                    }
                } else {
                    0.0F
                }

            if (item.imageUrl != null) {
                Picasso.get().load(item.imageUrl).into(imageView)
            } else {
                Picasso.get().load(R.drawable.no_image).into(imageView)
            }
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    interface OnClickItemListener{
        fun onItemClick(subItemHomeModel: SubItemHomeModel)
    }
}