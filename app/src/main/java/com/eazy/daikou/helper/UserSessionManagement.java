package com.eazy.daikou.helper;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.google.gson.Gson;
import com.eazy.daikou.model.profile.User;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class UserSessionManagement {        // Class for store local data

    private final SharedPreferences sharedPreferences;
    private final Context _context;
    private final Editor editor;

    private static final String PREF_NAME = "UserSessionManagement";
    private static final String USER = "USER";
    private static final String IS_USER_LOGIN = "IsUserLoggedIn";
    private static final String USER_COOKIE = "User_Cookie";
    private static final String USER_COOKIE_ID = "UserCookieId";

    public void saveArrayList(Context context, String key, List<String> value) {
        SharedPreferences aSharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor aSharedPreferencesEdit = aSharedPreferences.edit();
        Gson gson = new Gson();
        String jsonFavorites = gson.toJson(value);
        aSharedPreferencesEdit.putString(key, jsonFavorites);
        aSharedPreferencesEdit.apply();
    }
    public ArrayList<String> getSavedList(Context context, String key) {
        // SharedPreferences settings;
        List<String> unReadId;

        SharedPreferences settings = context.getSharedPreferences(PREF_NAME,
                Context.MODE_PRIVATE);

        if (settings.contains(key)) {
            String jsonFavorites = settings.getString(key, "");
            Gson gson = new Gson();
            String[] favoriteItems = gson.fromJson(jsonFavorites,
                    String[].class);

            unReadId = Arrays.asList(favoriteItems);
            unReadId = new ArrayList<>(unReadId);

        } else {
            return new ArrayList<>();
        }

        return (ArrayList<String>) unReadId;
    }

    public UserSessionManagement(Context _context) {
        this._context = _context;
        sharedPreferences = _context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    // Create Login Session
    public void saveData(User user, boolean isLoggedIn){

        // Storing login value as True
        editor.putBoolean(IS_USER_LOGIN, isLoggedIn);

        Gson gson = new Gson();

        // Storing name in Pref
        editor.putString(USER,gson.toJson(user));

        // Commit Change
        editor.commit();

    }

    // Check User Login
    public boolean IsUserLoggedIn(String key){
       return sharedPreferences.getBoolean(key,false);
    }

    // Saving Cookie
    public void saveUserCookie(String cookie, String id){
        editor.putString(USER_COOKIE, cookie);
        editor.putString(USER_COOKIE_ID,id);
        editor.commit();
    }

    public String getUserId(){
        return sharedPreferences.getString(USER_COOKIE_ID, null);
    }

    public String getUserCookie(){
        return sharedPreferences.getString(USER_COOKIE,null);
    }

    // Getting the User Detail from SharePreference
    public String getUserDetail(){
        return sharedPreferences.getString(USER, null);
    }

    // Log out
    public void logoutUser(){
        editor.putBoolean(IS_USER_LOGIN,false);
        editor.clear();
        editor.commit();
    }

    public static void saveString(String key, String value, Context activity) {
        SharedPreferences aSharedPreferences = activity.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor aSharedPreferencesEdit = aSharedPreferences.edit();

        aSharedPreferencesEdit.putString(key, value);
        aSharedPreferencesEdit.apply();
    }

    public static String getString(String key, Context activity) {
        SharedPreferences aSharedPreferences = activity.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        return aSharedPreferences.getString(key, "");
    }

    public static void clearString(Context activity) {
        SharedPreferences sharedPreferences = activity.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
    }

    public static void clearStringKey(Activity activity, String keys) {
        SharedPreferences sharedPreferences = activity.getSharedPreferences(keys, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.commit();
    }
}
