package com.eazy.daikou.helper.map

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.PorterDuff
import android.location.Geocoder
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.core.app.ActivityCompat
import androidx.core.content.res.ResourcesCompat
import com.bumptech.glide.Glide
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.base.BaseCameraActivity
import com.eazy.daikou.helper.Utils
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.squareup.picasso.Picasso

class MapsGenerateClass {
    companion object {
        fun initMapsActivity(context: Activity, lat: Double, lng: Double, hotelName : String, mapFragment: SupportMapFragment, callBackOnMapReadyListener : CallBackOnMapReadyListener) {
            mapFragment.getMapAsync(object : OnMapReadyCallback {
                override fun onMapReady(googleMap: GoogleMap) {
                    // Zoom map to lat lng
                    googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(lat, lng), 15f))

                    if (ActivityCompat.checkSelfPermission(
                            context,
                            Manifest.permission.ACCESS_FINE_LOCATION
                        ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                            context, Manifest.permission.ACCESS_COARSE_LOCATION
                        ) != PackageManager.PERMISSION_GRANTED
                    ) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return
                    }

                    googleMap.isMyLocationEnabled = true
                    googleMap.uiSettings.isZoomControlsEnabled = false
                    googleMap.uiSettings.isMyLocationButtonEnabled = true
                    googleMap.uiSettings.isCompassEnabled = false

                    // Add marker
                    val latLng = LatLng(lat, lng)
                    val marker: Marker? = googleMap.addMarker(
                        MarkerOptions().position(latLng)
                            .title(hotelName)
                    )
                    marker!!.showInfoWindow()

                    callBackOnMapReadyListener.onCallBackLister(googleMap)
                }
            })
        }

        fun setMarkerBitmapFromView(context: Activity, imageUrl : String,  name: String): Bitmap? {
            val customMarkerView = (context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater).inflate(R.layout.hotel_show_item_location, null)
            val textView = customMarkerView.findViewById<TextView>(R.id.priceTv)
            textView.text = name
            val image = customMarkerView.findViewById<ImageView>(R.id.imageHotel)

            val convertUrlToBitmap = Utils.convertUrlToBase64(imageUrl)
            val loadedBitmap = BaseCameraActivity.convertByteToBitmap(convertUrlToBitmap)

            // Picasso.get().load(imageUrl).into(image)
            image.setImageBitmap(loadedBitmap)

            Utils.setBgTint(textView, R.color.appBarColorOld)

            customMarkerView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED)
            customMarkerView.layout(
                0,
                0,
                customMarkerView.measuredWidth,
                customMarkerView.measuredHeight
            )
            customMarkerView.buildDrawingCache()
            val returnedBitmap = Bitmap.createBitmap(
                customMarkerView.measuredWidth, customMarkerView.measuredHeight,
                Bitmap.Config.ARGB_8888
            )
            val canvas = Canvas(returnedBitmap)
            canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN)
            val drawable = customMarkerView.background
            drawable?.draw(canvas)
            customMarkerView.draw(canvas)
            val circleDrawable = ResourcesCompat.getDrawable(
                context.resources,
                R.drawable.ic_baseline_location_on_24,
                null
            )
            val canvas1 = Canvas()
            val bitmap1 = Bitmap.createBitmap(
                circleDrawable!!.intrinsicWidth,
                circleDrawable.intrinsicHeight,
                Bitmap.Config.ARGB_8888
            )
            canvas1.setBitmap(bitmap1)
            circleDrawable.setBounds(
                0,
                0,
                circleDrawable.intrinsicWidth,
                circleDrawable.intrinsicHeight
            )
            circleDrawable.draw(canvas1)
            return returnedBitmap
        }

        fun getMarkerBitmapFromView1(context : Activity, isClick : Boolean, name: String): Bitmap? {
            val customMarkerView =
                (context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater).inflate(R.layout.custom_location_maps, null)
            val textView = customMarkerView.findViewById<TextView>(R.id.unitNoTv)
            textView.text = name
            val triangle = customMarkerView.findViewById<ImageView>(R.id.traingle)

            Utils.setBgTint(textView, if (isClick) R.color.book_now_secondary else R.color.appBarColorOld)
            triangle.setColorFilter(Utils.getColor(context, if (isClick) R.color.book_now_secondary else R.color.appBarColorOld))

            customMarkerView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED)
            customMarkerView.layout(
                0,
                0,
                customMarkerView.measuredWidth,
                customMarkerView.measuredHeight
            )
            customMarkerView.buildDrawingCache()
            val returnedBitmap = Bitmap.createBitmap(
                customMarkerView.measuredWidth, customMarkerView.measuredHeight,
                Bitmap.Config.ARGB_8888
            )
            val canvas = Canvas(returnedBitmap)
            canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN)
            val drawable = customMarkerView.background
            drawable?.draw(canvas)
            customMarkerView.draw(canvas)
            val circleDrawable = ResourcesCompat.getDrawable(
                context.resources,
                R.drawable.ic_baseline_location_on_24,
                null
            )
            val canvas1 = Canvas()
            val bitmap1 = Bitmap.createBitmap(
                circleDrawable!!.intrinsicWidth,
                circleDrawable.intrinsicHeight,
                Bitmap.Config.ARGB_8888
            )
            canvas1.setBitmap(bitmap1)
            circleDrawable.setBounds(
                0,
                0,
                circleDrawable.intrinsicWidth,
                circleDrawable.intrinsicHeight
            )
            circleDrawable.draw(canvas1)
            return returnedBitmap
        }

    }

    interface CallBackOnMapReadyListener{
        fun onCallBackLister(map: GoogleMap)
    }
}