package com.eazy.daikou.helper;

import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.NumberPicker;
import android.widget.TextView;
import androidx.annotation.RequiresApi;
import com.eazy.daikou.R;
import com.eazy.daikou.request_data.static_data.StaticUtilsKey;
import com.eazy.daikou.model.my_property.service_provider.SelectPropertyModel;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class DateNumPickerFragment extends BottomSheetDialogFragment implements View.OnClickListener {

    private Calendar calendar;
    private ClickCallBackListener clickCallBackListener;
    private NumberPicker numberPicker;

    private final List<String> dateList = new ArrayList<>();
    private final HashMap<Integer, String> maps = new LinkedHashMap<>();
    private List<SelectPropertyModel> departmentList = new ArrayList<>();
    private final HashMap<String, String> valHashmap = new LinkedHashMap<>();
    private List<String> donePercentList = new ArrayList<>();

    public static final String type = "type";
    private String title = "";
    private String[] dateArr;
    private String[] timeArr;
    private static final int MIN_YEAR = 2000;

    public DateNumPickerFragment() {}

    public static DateNumPickerFragment newInstance(List<SelectPropertyModel> list, String action) {
        DateNumPickerFragment fragment = new DateNumPickerFragment();
        Bundle args = new Bundle();
        args.putSerializable("department_list", (Serializable) list);
        args.putString(type, action);
        fragment.setArguments(args);
        return fragment;
    }

    public static DateNumPickerFragment newInstance(String action) {
        DateNumPickerFragment fragment = new DateNumPickerFragment();
        Bundle args = new Bundle();
        args.putString(type, action);
        fragment.setArguments(args);
        return fragment;
    }

    // Work Order Part
    public static DateNumPickerFragment newInstance(String action, String value) {
        DateNumPickerFragment fragment = new DateNumPickerFragment();
        Bundle args = new Bundle();
        args.putString(type, action);
        args.putString("value", value);
        fragment.setArguments(args);
        return fragment;
    }

    // Work Order Part
    public static DateNumPickerFragment newInstance(String action, List<String> listString) {
        DateNumPickerFragment fragment = new DateNumPickerFragment();
        Bundle args = new Bundle();
        args.putString(type, action);
        args.putSerializable("done_percent_list", (Serializable) listString);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null && getArguments().containsKey(type)) {
            title = getArguments().getString(type);
        }

        if (getArguments() != null && getArguments().containsKey("department_list")) {
            departmentList = (List<SelectPropertyModel>) getArguments().getSerializable("department_list");
        }

        if (getArguments() != null && getArguments().containsKey("done_percent_list")) {
            donePercentList = (List<String>) getArguments().getSerializable("done_percent_list");
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.bottom_sheet_layout, container, false);

        TextView titleHeader = view.findViewById(R.id.city);
        TextView doneBtn = view.findViewById(R.id.done);
        numberPicker = view.findViewById(R.id.number_picker);

        calendar = Calendar.getInstance();

        if (title != null) {
            if (title.equalsIgnoreCase("MONTH")) {
                titleHeader.setText(getResources().getString(R.string.month));
                maps.put(1, getResources().getString(R.string.january));
                maps.put(2, getResources().getString(R.string.february));
                maps.put(3, getResources().getString(R.string.march));
                maps.put(4, getResources().getString(R.string.april));
                maps.put(5, getResources().getString(R.string.may));
                maps.put(6, getResources().getString(R.string.june));
                maps.put(7, getResources().getString(R.string.july));
                maps.put(8, getResources().getString(R.string.august));
                maps.put(9, getResources().getString(R.string.september));
                maps.put(10, getResources().getString(R.string.october));
                maps.put(11, getResources().getString(R.string.november));
                maps.put(12, getResources().getString(R.string.december));
                for (Map.Entry<Integer, String> item : maps.entrySet()) {
                    dateList.add(item.getValue());
                }
                dateArr = dateList.toArray(new String[dateList.size()]);
                getMonth(dateList);
            } else if (title.equalsIgnoreCase("YEAR")) {
                titleHeader.setText(getResources().getString(R.string.year));
                getYear();
            } else if (title.equals("time")) {
                titleHeader.setText(getResources().getString(R.string.duration));
                valHashmap.put("30", "30 ".concat(requireContext().getResources().getString(R.string.minutes).toLowerCase(Locale.ROOT)));
                valHashmap.put("60", "1 ".concat(requireContext().getResources().getString(R.string.hour).toLowerCase(Locale.ROOT)));
                valHashmap.put("90", "1 ".concat(requireContext().getResources().getString(R.string.hour).toLowerCase(Locale.ROOT)).concat(" ").concat("30 ").concat(requireContext().getResources().getString(R.string.minutes).toLowerCase(Locale.ROOT)));
                valHashmap.put("120", "2 ".concat(requireContext().getResources().getString(R.string.hour).toLowerCase(Locale.ROOT)));
                valHashmap.put("180", "2 ".concat(requireContext().getResources().getString(R.string.hour).toLowerCase(Locale.ROOT)).concat(" ").concat("30 ").concat(requireContext().getResources().getString(R.string.minutes).toLowerCase(Locale.ROOT)));

                for (Map.Entry<String, String> maps : valHashmap.entrySet()) {
                    dateList.add(maps.getValue());
                }

                timeArr = dateList.toArray(new String[dateList.size()]);
                if (dateList.size() > 0) {
                    getTime(dateList);
                }

            } else if (title.equalsIgnoreCase("inspector_type")) {
                titleHeader.setText(getResources().getString(R.string.select_inspector));
                List<String> val = new ArrayList<>();
                val.add(getResources().getString(R.string.owner));
                val.add(getResources().getString(R.string.tenant));
                timeArr = val.toArray(new String[val.size()]);
                if (val.size() > 0) {
                    getTime(val);
                }
            } else if (title.equalsIgnoreCase(StaticUtilsKey.department_action)) {
                titleHeader.setText(getResources().getString(R.string.select_department));
                List<String> stringList = new ArrayList<>();
                for (SelectPropertyModel getList : departmentList) {
                    stringList.add(getList.getDepartmentName());
                    valHashmap.put(getList.getDepartmentName(), getList.getId());
                }
                timeArr = stringList.toArray(new String[0]);
                if (stringList.size() > 0) {
                    getTime(stringList);
                }

                if (stringList.size() > 0) {
                    numberPicker.setOnScrollListener((numberPicker, scrollState) -> {
                        if (scrollState == NumberPicker.OnScrollListener.SCROLL_STATE_IDLE) {
                            clickCallBackListener.doneItemClick(title, stringList.get(numberPicker.getValue()), valHashmap.get(stringList.get(numberPicker.getValue())));
                        }
                    });
                }

                doneBtn.setVisibility(View.GONE);
            }
            // Work Order Part
            else if (title.equals("status_work_order")) {

                titleHeader.setText(getResources().getString(R.string.status));
                for (Map.Entry<String, String> item : Utils.getValueFromKeyStatus(requireContext()).entrySet()) {
                    dateList.add(item.getValue());
                }
                dateArr = dateList.toArray(new String[dateList.size()]);
                getMonth(dateList);

            } else if (title.equals("priority_work_order")) {

                titleHeader.setText(getResources().getString(R.string.select_priority));
                for (Map.Entry<Integer, String> item : Utils.getPriorityOnWorkOrder(requireContext()).entrySet()) {
                    dateList.add(item.getValue());
                }
                dateArr = dateList.toArray(new String[dateList.size()]);
                getMonth(dateList);

            } else if (title.equalsIgnoreCase("done_percent") || title.equalsIgnoreCase("select_room") || title.equalsIgnoreCase("select_available_time")) {
                if (title.equalsIgnoreCase("select_room")){ //select_room use for hotel , but now not use
                    titleHeader.setText(getResources().getString(R.string.select_room));
                } else if (title.equalsIgnoreCase("done_percent")){
                    titleHeader.setText(getResources().getString(R.string.done_percents));
                } else  {
                    titleHeader.setText(getResources().getString(R.string.available_time));
                }
                dateArr = donePercentList.toArray(new String[donePercentList.size()]);
                getMonth(donePercentList);
            } else if (title.equalsIgnoreCase("title_membership")) {
                titleHeader.setText(getResources().getString(R.string.title));
                dateArr = donePercentList.toArray(new String[donePercentList.size()]);
                getMonth(donePercentList);
            } else if (title.equalsIgnoreCase("from_time") || title.equalsIgnoreCase("to_time")) {
                titleHeader.setText(getResources().getString(R.string.title));
                dateArr = donePercentList.toArray(new String[donePercentList.size()]);
                getMonth(donePercentList);
            } else if (title.equalsIgnoreCase("max_min_price")) {
                titleHeader.setText(getResources().getString(R.string.price));
                valHashmap.put("1", "All");
                valHashmap.put("2", "From 0$ - 500$");
                valHashmap.put("3", "From 500$ - 5000$");
                valHashmap.put("4", "From 5000$ - 50 000$");
                valHashmap.put("5", "From 50 000$ - 500 000$");
                valHashmap.put("6", "From 500 000$ - 1 000 000$");
                valHashmap.put("7", "From 1 000 000$ - 10 000 000$");
                valHashmap.put("8", "From 10 000 000$ +");
                for (Map.Entry<String, String> maps : valHashmap.entrySet()) {
                    dateList.add(maps.getValue());
                }

                timeArr = dateList.toArray(new String[dateList.size()]);
                if (dateList.size() > 0) {
                    getTime(dateList);
                }
            }
        }

        view.findViewById(R.id.cancel).setOnClickListener(this);
        doneBtn.setOnClickListener(this);

        return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void getMonth(List<String> dateList) {
        if (dateList.size() > 0) {
            numberPicker.setMinValue(0);
            numberPicker.setMaxValue(dateList.size() - 1);
            numberPicker.setDisplayedValues(dateArr);
            numberPicker.setWrapSelectorWheel(false);
        }

    }

    private void getYear() {
        numberPicker.setMinValue(MIN_YEAR);
        numberPicker.setMaxValue(calendar.get(Calendar.YEAR));
        numberPicker.setValue(calendar.get(Calendar.YEAR));
        numberPicker.setWrapSelectorWheel(false);
    }

    private void getTime(List<String> timeList) {
        numberPicker.setMinValue(0);
        numberPicker.setMaxValue(timeList.size() - 1);
        numberPicker.setDisplayedValues(timeArr);
        numberPicker.setWrapSelectorWheel(false);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.cancel) {
            dismiss();
        } else if (view.getId() == R.id.done) {
            if (title.equalsIgnoreCase("MONTH")) {
                if (dateArr.length > 0) {

                    String month = dateArr[numberPicker.getValue()];
                    for (Map.Entry<Integer, String> item : maps.entrySet()) {
                        if (month.equalsIgnoreCase(item.getValue())) {
                            clickCallBackListener.doneItemClick(title, month, item.getKey() + "");
                        }
                    }
                } else {
                    dismiss();
                }
            } else if (title.equalsIgnoreCase("YEAR")) {
                clickCallBackListener.doneItemClick(title, numberPicker.getValue() + "", "0");    //define 0 not use
            } else if (title.equalsIgnoreCase("inspector_type")) {
                clickCallBackListener.doneItemClick(title, timeArr[numberPicker.getValue()], "");
            } else if (title.equals("time")) {
                if (dateList.size() > 0) {
                    for (Map.Entry<String, String> item : valHashmap.entrySet()) {
                        if (dateList.get(numberPicker.getValue()).equalsIgnoreCase(item.getValue())) {
                            clickCallBackListener.doneItemClick(title, dateList.get(numberPicker.getValue()), item.getKey());
                        }
                    }
                } else {
                    dismiss();
                }
            } else if (title.equals("status_work_order") || title.equals("priority_work_order")) {
                if (dateArr.length > 0) {
                    clickCallBackListener.doneItemClick(title, dateArr[numberPicker.getValue()], "");
                }
            } else if (title.equals("done_percent") || title.equalsIgnoreCase("select_room") || title.equalsIgnoreCase("select_available_time")) {
                if (dateArr.length > 0) {
                    clickCallBackListener.doneItemClick(title, dateArr[numberPicker.getValue()], "");
                }
            } else if (title.equals("title_membership")) {
                if (dateArr.length > 0) {
                    clickCallBackListener.doneItemClick(title, dateArr[numberPicker.getValue()], "");
                }
            } else if (title.equalsIgnoreCase("from_time") || title.equalsIgnoreCase("to_time")) {
                if (dateArr.length > 0) {
                    clickCallBackListener.doneItemClick(title, dateArr[numberPicker.getValue()], "");
                }
            } else if (title.equalsIgnoreCase("max_min_price")) {
                if (dateList.size() > 0) {
                    for (Map.Entry<String, String> item : valHashmap.entrySet()) {
                        if (dateList.get(numberPicker.getValue()).equalsIgnoreCase(item.getValue())) {
                            clickCallBackListener.doneItemClick(title, dateList.get(numberPicker.getValue()), item.getKey());
                        }
                    }
                } else {
                    dismiss();
                }
            }
        }
        dismiss();
    }

    public interface ClickCallBackListener {
        void doneItemClick(String title, String date, String id);
    }

    public void CallBackListener(ClickCallBackListener clickCallBackListener) {
        this.clickCallBackListener = clickCallBackListener;
    }
}