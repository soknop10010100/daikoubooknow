package com.eazy.daikou.helper

import com.eazy.daikou.BuildConfig

class CheckIsAppActive {
    companion object {
        var is_daikou_active = isBuildActiveApp()
        private fun isBuildActiveApp(): Boolean {
            return BuildConfig.FLAVOR.equals("app", ignoreCase = true) || BuildConfig.FLAVOR.equals("daikou_app_dev", ignoreCase = true)
        }
    }
}