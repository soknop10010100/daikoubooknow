package com.eazy.daikou.helper;

import static android.app.Activity.RESULT_OK;
import static com.eazy.daikou.repository_ws.Constant.SHARE_PREFERENCE_NAME;
import static com.google.common.io.ByteStreams.toByteArray;

import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.text.InputType;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.PasswordTransformationMethod;
import android.text.style.UnderlineSpan;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.core.view.WindowInsetsControllerCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.denzcoskun.imageslider.ImageSlider;
import com.denzcoskun.imageslider.constants.ScaleTypes;
import com.denzcoskun.imageslider.models.SlideModel;
import com.eazy.daikou.BuildConfig;
import com.eazy.daikou.R;
import com.eazy.daikou.repository_ws.Constant;
import com.eazy.daikou.base.MockUpData;
import com.eazy.daikou.request_data.request.profile_ws.ProfileWs;
import com.eazy.daikou.request_data.static_data.StaticUtilsKey;
import com.eazy.daikou.helper.web.WebsiteActivity;
import com.eazy.daikou.model.home.MyRolePermissionModel;
import com.eazy.daikou.model.profile.User;
import com.eazy.daikou.model.profile.UserTypeProfileModel;
import com.eazy.daikou.model.utillity_tracking.model.SampleDescriptionModel;
import com.eazy.daikou.ui.ShowMultipleDisplayImagesActivity;
import com.eazy.daikou.ui.ViewPagerItemImagesAdapter;
import com.eazy.daikou.ui.QRCodeAlertDialog;
import com.eazy.daikou.ui.ShowDisplayImageLayoutActivity;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.google.android.gms.tasks.Task;
import com.google.android.material.tabs.TabLayout;
import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;

import id.zelory.compressor.Compressor;

public class Utils {

    private static final int EOF = -1;
    private static final int DEFAULT_BUFFER_SIZE = 1024 * 4;

    // Getting the Screen Width
    public static int getScreenWidth(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return displayMetrics.widthPixels;
    }

    // Getting the Screen Height
    public static int getScreenHeight(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float dpHeight = displayMetrics.heightPixels;
        return (int) (dpHeight);
    }

    public static int getWidth(Activity activity) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.widthPixels;
    }

    //Create Date Time for Calendar
    public static Locale getCurrentLocale(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return context.getResources().getConfiguration().getLocales().get(0);
        } else {
            //noinspection deprecation
            return context.getResources().getConfiguration().locale;
        }
    }

    public static void logDebug(String logTitle, String message) {
        if (BuildConfig.DEBUG) {//logDebug only in debug mode
            Log.d(logTitle, message);
        }
    }

    // Request File for Take and Select Image
    public static File from(Context context, Uri uri) {
        InputStream inputStream;
        File tempFile = null;
        try {
            inputStream = context.getContentResolver().openInputStream(uri);
            String fileName = getFileName(context, uri);
            String[] splitName = splitFileName(fileName);
            tempFile = File.createTempFile(splitName[0], splitName[1]);
            tempFile = rename(tempFile, fileName);
            tempFile.deleteOnExit();
            FileOutputStream out = null;
            try {
                out = new FileOutputStream(tempFile);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            if (inputStream != null) {
                copy(inputStream, out);
                inputStream.close();
            }

            if (out != null) {
                out.close();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Log.e(e.getClass().getName(), e.getMessage());
        } catch (IOException e2) {
            Log.e(e2.getClass().getName(), e2.getMessage());
        }
        return tempFile;
    }

    @SuppressLint("Range")
    public static String getFileName(Context context, Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf(File.separator);
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }

    private static String[] splitFileName(String fileName) {
        String name = fileName;
        String extension = "";
        int i = fileName.lastIndexOf(".");
        if (i != -1) {
            name = fileName.substring(0, i);
            extension = fileName.substring(i);
        }

        return new String[]{name, extension};
    }

    private static File rename(File file, String newName) {
        File newFile = new File(file.getParent(), newName);
        if (!newFile.equals(file)) {
            if (newFile.exists() && newFile.delete()) {
                logDebug("FileUtil", "Delete old " + newName + " file");
            }
            if (file.renameTo(newFile)) {
                logDebug("FileUtil", "Rename file to " + newName);
            }
        }
        return newFile;
    }

    private static long copy(InputStream input, OutputStream output) throws IOException {
        long count = 0;
        int n;
        byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
        while (EOF != (n = input.read(buffer))) {
            output.write(buffer, 0, n);
            count += n;
        }
        return count;
    }

    //Compress file
    public static Bitmap getImageCamera(File actualFile, Context mActivity) throws IOException {
        actualFile = new Compressor(mActivity).compressToFile(actualFile);
        String filePath = actualFile.getPath();
        return BitmapFactory.decodeFile(filePath);
    }

    public static byte[] readBytesFromPath(String filePath) {
        FileInputStream fileInputStream = null;
        byte[] bytesArray = null;
        try {
            File file = new File(filePath);
            bytesArray = new byte[(int) file.length()];

            //read file into bytes[]
            fileInputStream = new FileInputStream(file);
            fileInputStream.read(bytesArray);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fileInputStream != null) {
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return bytesArray;
    }

    public static String getFileName(String path) {
        return path.substring(path.lastIndexOf("/") + 1);
    }

    public static Bitmap getImageGallery(Uri data, Context mActivity) {
        Bitmap loadedBitmap;
        // Get and resize profile image
        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        // TRY getActvity() as well if not work
        Cursor cursor = mActivity.getContentResolver().query(data, filePathColumn, null, null, null);
        cursor.moveToFirst();

        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        String picturePath = cursor.getString(columnIndex);
        cursor.close();

        loadedBitmap = BitmapFactory.decodeFile(picturePath);

        ExifInterface exif = null;
        try {
            File pictureFile = new File(picturePath);
            exif = new ExifInterface(pictureFile.getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
            Toast.makeText(mActivity, R.string.unable_to_read_image, Toast.LENGTH_LONG).show();
        }

        int orientation = ExifInterface.ORIENTATION_NORMAL;

        if (exif != null)
            orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                loadedBitmap = rotateBitmap(loadedBitmap, 90);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                loadedBitmap = rotateBitmap(loadedBitmap, 180);
                break;

            case ExifInterface.ORIENTATION_ROTATE_270:
                loadedBitmap = rotateBitmap(loadedBitmap, 270);
                break;
        }
        return loadedBitmap;
    }

    public static Bitmap rotateBitmap(Bitmap bitmap, int degrees) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degrees);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

    //=============================================================================================

    public static void changeLanguage(Activity context, String language) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        saveString("language", language, context);
        Resources res = context.getResources();
        Configuration config = new Configuration(res.getConfiguration());
        config.locale = locale;
        res.updateConfiguration(config, res.getDisplayMetrics());
    }

    public static void saveString(String key, String value, Context activity) {
        SharedPreferences aSharedPreferences = activity.getSharedPreferences(SHARE_PREFERENCE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor aSharedPreferencesEdit = aSharedPreferences.edit();

        aSharedPreferencesEdit.putString(key, value);
        aSharedPreferencesEdit.apply();
    }

    public static String getString(String key, Context activity) {
        SharedPreferences aSharedPreferences = activity.getSharedPreferences(SHARE_PREFERENCE_NAME, Context.MODE_PRIVATE);
        return aSharedPreferences.getString(key, "");
    }

    // Convert Image
    public static String convert(Bitmap bitmap) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, outputStream);

        return Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT);

    }

    public static String convertAudioToBase64(Context context, String fileName) {
        String base64 = "";
        try {
            InputStream inputStream = context.getContentResolver().openInputStream(Uri.fromFile(new File(fileName)));

            byte[] bytearray;
            bytearray = toByteArray(inputStream);

            base64 = Base64.encodeToString(bytearray, Base64.DEFAULT);
        } catch (Exception e) {
            Utils.logDebug("logudteee", e.getMessage() +"");
        }

        return base64;
    }

    public static String videoToBase64(File file) {
        String encodedString = null;

        InputStream inputStream = null;
        try {
            inputStream = new FileInputStream(file);
        } catch (Exception e) {
            e.printStackTrace();
        }
        byte[] bytes;
        byte[] buffer = new byte[8192];
        int bytesRead;
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        try {
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                output.write(buffer, 0, bytesRead);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        bytes = output.toByteArray();
        encodedString = Base64.encodeToString(bytes, Base64.NO_WRAP);
        Utils.logDebug("String", encodedString);

//        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
//            Byte[] encode = java.util.Base64.getEncoder().encode(bytes);
//            Log.e("VERSION IS","O");
//
//
//        }else{
//            encodedBytes = Base64.encodeToString(bytes, ).getBytes();
//            Log.e("VERSION LESS","O");
//        }

        return encodedString;
    }

    public static void hideSoftKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) view.getContext().getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null) {
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static void showKeyboard(Activity activity) {
        if (activity != null) {
            activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    public static void showKeyboardV2(Activity activity){
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    public static Bitmap getResizedBitmap(Context activity, int imgid, int width, int height) {
        Bitmap bMap = BitmapFactory.decodeResource(activity.getResources(), imgid);
        Bitmap bMapScaled = Bitmap.createScaledBitmap(bMap, width, height, true);
        return bMapScaled;
    }

    public static void hideStatusBar(Activity mActivity) {
        mActivity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    public static int dpToPx(Context context, int dp) {
        Resources r = context.getResources();
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics());
        return (int) px;
    }

    public static void widthHeightLayout(View layout, OnCallBackListener onCallBackListener){
        ViewTreeObserver vto = layout.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    layout.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                } else {
                    layout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
                onCallBackListener.onClickBackBtn(layout.getMeasuredWidth(), layout.getMeasuredHeight());
            }
        });
    }

    public interface OnCallBackListener{
        void onClickBackBtn(int width, int height);
    }

    public static String convertFirstCapital(String ToSt) {
        return ToSt.substring(0, 1).toUpperCase() + ToSt.substring(1).toLowerCase();
    }

    public static String getCountryCodeFromSimCard(Context context) {
        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);  //gets the current TelephonyManager
        if (tm != null && tm.getSimState() != TelephonyManager.SIM_STATE_ABSENT) {
            return tm.getNetworkCountryIso();
        }
        return null;
    }

    public static void popupMessage(String title, String message, Activity activity) {
        if (!activity.isFinishing()) {
            androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(new ContextThemeWrapper(activity, R.style.DialogTheme));
            if (title != null) builder.setTitle(title);
            builder.setMessage(message);
            builder.setPositiveButton(String.format(Locale.US, "%s", activity.getString(R.string.ok)), null);
            builder.show();
        }
    }


    public static void setUnderLineText(TextView tv, String textToUnderLine) {
        String tvt = tv.getText().toString();
        int ofe = tvt.indexOf(textToUnderLine);

        UnderlineSpan underlineSpan = new UnderlineSpan();
        SpannableString wordToSpan = new SpannableString(tv.getText());
        for (int ofs = 0; ofs < tvt.length() && ofe != -1; ofs = ofe + 1) {
            ofe = tvt.indexOf(textToUnderLine, ofs);
            if (ofe == -1)
                break;
            else {
                wordToSpan.setSpan(underlineSpan, ofe, ofe + textToUnderLine.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                tv.setText(wordToSpan, TextView.BufferType.SPANNABLE);
            }
        }
    }

    // Save List SampleModelDescription Emergency
    public static void saveArrayList(List<SampleDescriptionModel> list, String key, Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        try {
            Gson gson = new Gson();
            String json = gson.toJson(list);
            editor.putString(key, json);
            editor.apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static List<SampleDescriptionModel> getArrayList(String key, Context context) {
        List<SampleDescriptionModel> getArrayLists = new ArrayList<>();
        try {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
            Gson gson = new Gson();
            String json = prefs.getString(key, null);
            Type type = new TypeToken<List<SampleDescriptionModel>>() {
            }.getType();
            getArrayLists = gson.fromJson(json, type);
            return getArrayLists;
        } catch (Exception e) {
            Utils.logDebug("ejjjjjjjjjjjjj", e.getMessage());
        }
        return getArrayLists;
    }

    public static Bitmap getQRCodeImage(String text) {
        Hashtable<EncodeHintType, String> hints = new Hashtable<>();
        hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");
        QRCodeWriter writer = new QRCodeWriter();
        try {
            BitMatrix bitMatrix = writer.encode(text, BarcodeFormat.QR_CODE, 96, 96, hints);
            int w = bitMatrix.getWidth();
            int h = bitMatrix.getHeight();
            Bitmap bmp = Bitmap.createBitmap(w, h, Bitmap.Config.RGB_565);
            for (int x = 0; x < w; x++) {
                for (int y = 0; y < h; y++) {
                    bmp.setPixel(x, y, bitMatrix.get(x, y) ? Color.BLACK : Color.WHITE);
                }
            }
            return bmp;
        } catch (WriterException e) {
            e.printStackTrace();
            Log.e(e.getClass().getName(), e.getMessage() + "");
        }
        return null;
    }

    public static Bitmap getQRCodeImage512(String text) {
        Hashtable<EncodeHintType, String> hints = new Hashtable<>();
        hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");
        QRCodeWriter writer = new QRCodeWriter();
        try {
            BitMatrix bitMatrix = writer.encode(text, BarcodeFormat.QR_CODE, 512, 512, hints);
            int w = bitMatrix.getWidth();
            int h = bitMatrix.getHeight();
            Bitmap bmp = Bitmap.createBitmap(w, h, Bitmap.Config.RGB_565);
            for (int x = 0; x < w; x++) {
                for (int y = 0; y < h; y++) {
                    bmp.setPixel(x, y, bitMatrix.get(x, y) ? Color.BLACK : Color.WHITE);
                }
            }
            return bmp;
        } catch (WriterException e) {
            e.printStackTrace();
            Log.e(e.getClass().getName(), e.getMessage() + "");
        }
        return null;
    }

    public static void delayProgressBar(final ProgressBar progressBar) {
        progressBar.setVisibility(View.VISIBLE);
        new Handler(Looper.getMainLooper()).postDelayed(() -> progressBar.setVisibility(View.GONE), 1500);
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static String getJsonFromAssets(Context context, String fileName) {
        String jsonString;
        try {
            InputStream is = context.getAssets().open(fileName);

            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();

            jsonString = new String(buffer, StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        return jsonString;
    }

    public static String starDynamic(int phoneLeng) {
        StringBuilder star = new StringBuilder();
        for (int i = 0; i <= phoneLeng; i++) {
            star.append("*");
        }
        return star.toString();
    }


    public static String convertUrlToBase64(String url) {
        URL newUrl;
        Bitmap bitmap;
        String base64 = "";
        try {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            newUrl = new URL(url);
            bitmap = BitmapFactory.decodeStream(newUrl.openConnection().getInputStream());
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
            base64 = Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return base64;
    }

    public static String covertTime(int times, String type) {
        int min = times % 60;
        int hours = times / 60;
        if (type.equals("daily")) {
            int day = hours / 24;
            if (day >= 1) {
                return day + " days ";
            } else {
                return 1 + " day";
            }

        } else {
            if (times < 60) {
                return min + " min";
            } else {
                return hours + " h : " + min + " min";

            }
        }
    }

    public static String covertTimes(int times) {
        int min = times % 60;
        int hours = times / 60;
        String  mi = "";

        if (times < 60) {
            return min + " min";
        } else {
            if(min==0){
                mi ="00";
            }else {
                mi = min + "";
            }
            return hours + ":"+ mi ;
           // return Utils.formatDateTime((hours + ":"+ mi + ":00"),"hh:mm:ss","hh:mm a");
        }
    }


    public static String leftPadding(String userId){
        return "1" + "0000000".substring(userId.length()) + userId;
    }

    public static String formatDateFromString(String inputFormat, String outputFormat, String inputDate) {
        Date parsed;
        String outputDate = "";

        SimpleDateFormat df_input = new SimpleDateFormat(inputFormat, java.util.Locale.getDefault());
        SimpleDateFormat df_output = new SimpleDateFormat(outputFormat, java.util.Locale.getDefault());

        try {
            parsed = df_input.parse(inputDate);
            assert parsed != null;
            outputDate = df_output.format(parsed);

        } catch (ParseException e) {
            Utils.logDebug("logDebugDate", "ParseException - dateFormat");
        }

        return outputDate;

    }


    public static String formatDateTime(String dateStr, String strReadFormat, String strWriteFormat) {

        String formattedDate = dateStr;

        DateFormat readFormat = new SimpleDateFormat(strReadFormat, Locale.getDefault());
        DateFormat writeFormat = new SimpleDateFormat(strWriteFormat, Locale.getDefault());

        Date date = null;

        try {
            date = readFormat.parse(dateStr);
        } catch (ParseException e) {
        }

        if (date != null) {
            formattedDate = writeFormat.format(date);
        }

        return formattedDate;
    }

    public static Bitmap convertToBitmapCamera(String imagePath,Context context) {
        Uri uri = Uri.parse(imagePath);
        Bitmap loadedBitmap = null;
        try {
            loadedBitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), uri);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return loadedBitmap;
    }

    public static Bitmap convertBitmapImage(String imagePath,Context context) {
        Uri uri = Uri.parse(imagePath);
        return Utils.getImageGallery(uri,context );
    }

    public static void saveUserPermissionList(List<MyRolePermissionModel> list, String key, Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        try {
            Gson gson = new Gson();
            String json = gson.toJson(list);
            editor.putString(key, json);
            editor.apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static List<MyRolePermissionModel> getUserPermissionList(String key, Context context) {
        List<MyRolePermissionModel> getArrayLists = new ArrayList<>();
        try {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
            Gson gson = new Gson();
            String json = prefs.getString(key, null);
            Type type = new TypeToken<List<MyRolePermissionModel>>() {
            }.getType();
            getArrayLists = gson.fromJson(json, type);
            return getArrayLists;
        } catch (Exception ignored) {
            logDebug("logdebugcallback", ignored.getMessage());
        }
        return getArrayLists;
    }

    public static void requestQrCodeToServer(Context context, String userId, String userName){
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put( "card_no", Utils.leftPadding(userId));
        hashMap.put( "car_no", "2BL-2222");
        hashMap.put( "card_type", "VIP");
        hashMap.put( "card_in_date", "2050-09-01");
        hashMap.put( "card_amount", "100.0");
        hashMap.put( "car_type", "Sedan");
        hashMap.put( "car_style", "Toyota");
        hashMap.put( "car_color", "Red");
        hashMap.put( "user_name", userName);
        hashMap.put( "user_id", userId);
        hashMap.put( "user_phone_number", "01234567");
        hashMap.put( "user_address", "Phnom Penh");
        hashMap.put( "park_no", "01");
        hashMap.put( "pay_amount", "0.0");
        hashMap.put( "make_date_time", "2021-09-01 10:20:30");
        hashMap.put( "operator_name", "Administrator");
        hashMap.put( "enable", "1");
        hashMap.put( "park_position", "Right");
        hashMap.put( "remark", "How");
        new ProfileWs().putQrCodeToServer(context, hashMap,new ProfileWs.CallBackSendQrCodeListener(){
            @Override
            public void onSuccess(String msg) {
                Utils.saveString(Constant.FIRST_SCAN_QR, "false", context);
            }

            @Override
            public void onSuccess(List<UserTypeProfileModel> list) { }

            @Override
            public void onFailed(String error) {
                // Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
                Utils.saveString(Constant.FIRST_SCAN_QR, "true", context);
            }
        });
    }

    public static void setTintColorOnImageStepProgress(ImageView imgStep1, ImageView imgStep2, ImageView imgStep3, TextView titleTv, String type, Context context){
        if (type.equalsIgnoreCase(StaticUtilsKey.inspection_type)) {
            imgStep1.setColorFilter(getColor(context,  R.color.appBarColorOld));
            titleTv.setText(StaticUtilsKey.isActionMenuHome.equalsIgnoreCase(StaticUtilsKey.inspection_action) ?
                    context.getResources().getString(R.string.select_inspection).toUpperCase(Locale.ROOT) : context.getResources().getString(R.string.select_maintenance_type).toUpperCase(Locale.ROOT));
        } else if (type.equalsIgnoreCase(StaticUtilsKey.template_type)){
            imgStep1.setColorFilter(getColor(context,  R.color.appBarColorOld));
            imgStep2.setColorFilter(getColor(context,  R.color.appBarColorOld));
            titleTv.setText(StaticUtilsKey.isActionMenuHome.equalsIgnoreCase(StaticUtilsKey.inspection_action) ?
                    context.getResources().getString(R.string.choose_inspection_template).toUpperCase(Locale.ROOT) : context.getResources().getString(R.string.choose_maintenance_template).toUpperCase(Locale.ROOT));
        } else {
            imgStep1.setColorFilter(getColor(context,  R.color.appBarColorOld));
            imgStep2.setColorFilter(getColor(context,  R.color.appBarColorOld));
            imgStep3.setColorFilter(getColor(context,  R.color.appBarColorOld));
            titleTv.setText(StaticUtilsKey.isActionMenuHome.equalsIgnoreCase(StaticUtilsKey.inspection_action) ?
                    context.getResources().getString(R.string.inspection_form).toUpperCase(Locale.ROOT) : context.getResources().getString(R.string.maintenance_form).toUpperCase(Locale.ROOT));
        }
    }

    public static int getColor(Context context, int color){
        return ContextCompat.getColor(context, color);
    }

    public static Drawable setDrawable(Context context, int drawableName){
        return ResourcesCompat.getDrawable(context.getResources(), drawableName, null);
    }

    public static void changeStatusBarColor(int color, Window window ){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            View decorView = window.getDecorView();
            WindowInsetsControllerCompat wic = new WindowInsetsControllerCompat(window, decorView);
            wic.setAppearanceLightStatusBars(true);
            window.setStatusBarColor(color);
        }
    }

    public static String getText(Context context, int msg) {
        return context.getResources().getString(msg);
    }

    public static void customToastMsgError(Context context, String message, boolean isSuccess) {
        Toast toast = Toast.makeText(context, message, Toast.LENGTH_LONG);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
            View view = toast.getView();
            view.setBackground(ResourcesCompat.getDrawable(context.getResources(), R.drawable.background_card_border, null));

            int color;
            if (isSuccess) {
                color = R.color.green;
            } else {
                color = R.color.red;
            }

            DrawableCompat.setTint(
                    DrawableCompat.wrap(view.getBackground()).mutate(),
                    ContextCompat.getColor(context, color)
            );

            toast.setGravity(Gravity.TOP, 0, 15);

            TextView text = view.findViewById(android.R.id.message);

            text.setShadowLayer(0f, 0f, 0f, Color.TRANSPARENT);
            text.setTextColor(Color.WHITE);
            text.setTextSize(16);
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            lp.setMargins(0, 0, 0, 0);
            view.setLayoutParams(lp);
            toast.setDuration(Toast.LENGTH_LONG);
        }
        toast.show();
    }

    public static void setBackgroundTintView(Context context, View view, int color){
        Drawable buttonDrawable = view.getBackground();
        buttonDrawable = DrawableCompat.wrap(buttonDrawable);
        DrawableCompat.setTint(buttonDrawable, Utils.getColor(context, color));
        view.setBackground(buttonDrawable);
    }

    public static HashMap<Integer,String> getPriorityOnWorkOrder(Context context){
        HashMap<Integer,String> hashMap = new HashMap<>();
        hashMap.put(1, Utils.getText(context, R.string.low));
        hashMap.put(2, Utils.getText(context, R.string.normal));
        hashMap.put(3,  Utils.getText(context, R.string.high));
        hashMap.put(4,  Utils.getText(context, R.string.urgent));
        return hashMap;
    }

    public static ColorStateList setDoneStatusPercentOnWorkOrder(Context context, String getDonePercent){
        try {
             String []valArr = getDonePercent.split(" ");
            if (Double.parseDouble(valArr[1].replace(" ", "")) <= 0){
                return  ColorStateList.valueOf(ContextCompat.getColor(context, R.color.yellow));
            } else if (Double.parseDouble(valArr[1].replace(" ", "")) < 50) {
                return  ColorStateList.valueOf(ContextCompat.getColor(context, R.color.green));
            } else if (Double.parseDouble(valArr[1].replace(" ", "")) < 100) {
                return  ColorStateList.valueOf(ContextCompat.getColor(context, R.color.blue));
            } else {
                return  ColorStateList.valueOf(ContextCompat.getColor(context, R.color.appBarColorOld));
            }
        } catch (Exception ex){
            ex.printStackTrace();
        }
        return null;
    }

    public static ColorStateList setColorStatusOnWorkOrder(Context context, String valueStatus) {
        switch (valueStatus) {
            case "new":
                return ColorStateList.valueOf(ContextCompat.getColor(context, R.color.appBarColor));
            case "resolved":
                return ColorStateList.valueOf(ContextCompat.getColor(context, R.color.green));
            case "on_hold":
                return ColorStateList.valueOf(ContextCompat.getColor(context, R.color.yellow));
            case "closed":
                return ColorStateList.valueOf(ContextCompat.getColor(context, R.color.red));
            case "in_progress":
                return ColorStateList.valueOf(ContextCompat.getColor(context, R.color.mid_night_blue));
            default:
                return ColorStateList.valueOf(ContextCompat.getColor(context, R.color.appBarColorOld));
        }
    }

    public static HashMap<String,String> getValueFromKeyStatus(Context context){
        HashMap<String,String> hashMap = new HashMap<>();
        hashMap.put("new", Utils.getText(context, R.string.new_));
        hashMap.put("resolved", Utils.getText(context, R.string.resolved));
        hashMap.put("on_hold", Utils.getText(context, R.string.on_hold));
        hashMap.put("closed", Utils.getText(context, R.string.closed));
        hashMap.put("in_progress", Utils.getText(context, R.string.in_progress));
        return hashMap;
    }

    public static void openDefaultPdfView(Context context, String urlImage) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.parse(urlImage), "application/pdf");
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION| Intent.FLAG_ACTIVITY_NO_HISTORY);

        try {
            context.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(context, context.getResources().getText(R.string.cannot_read_pdf_file), Toast.LENGTH_SHORT).show();
        }
    }

    public static HashMap<String,String> getValueFromKeyInspectionStatus(Context context){
        HashMap<String,String> hashMap = new HashMap<>();
        hashMap.put("active", Utils.getText(context, R.string.active));
        hashMap.put("pending", Utils.getText(context, R.string.pending));
        hashMap.put("completed", Utils.getText(context, R.string.completed));
        hashMap.put("assigned", Utils.getText(context, R.string.assigned));
        return hashMap;
    }

    public static void openZoomImage(Context context, String urlImage,  ArrayList<?> imageList){
        Intent intent = new Intent(context, ShowMultipleDisplayImagesActivity.class);
        if (urlImage.equalsIgnoreCase("")){     // ==> Image List as ArrayList<ImagesModel>, Just urlImage param must give ""
            intent.putExtra("image_model_list", imageList);
        }  else if (urlImage.equalsIgnoreCase("image_list_string")){    // ==>  Image List as ArrayList<String>, So urlImage param must give "image_list_string"
            intent.putExtra("image_list", imageList);
        } else {    // urlImage param must give "link url image to display"
            intent = new Intent(context, ShowDisplayImageLayoutActivity.class);
            intent.putExtra("image", urlImage);
            intent.putExtra("key", "display_image");
        }
        context.startActivity(intent);
    }

    public static void openImageOnDialog(Context context, String urlImage){
        if (urlImage != null && !urlImage.equals("")){
            QRCodeAlertDialog qrCodeAlertDialog = new QRCodeAlertDialog(context, urlImage);
            qrCodeAlertDialog.show();
        }
    }

    public static void setValueOnText(TextView textView, String value){
        textView.setText(value != null ? value : ". . .");
    }

    public static int visibleView(List<?> list){
        if (list.size() >= 4){
            return View.GONE;
        } else {
            return View.VISIBLE;
        }
    }

    public static void setOptionTypeInspection(Context context, String optionType, TextView textView){
        if (optionType != null){
            if (optionType.equals(StaticUtilsKey.detail_button_key)){
                textView.setText(context.getResources().getString(R.string.detailed));
                textView.setBackgroundTintList(ColorStateList.valueOf(Utils.getColor(context, R.color.blue)));
            } else if (optionType.equals(StaticUtilsKey.simplify_button_key)){
                textView.setText(context.getResources().getString(R.string.simplified));
                textView.setBackgroundTintList(ColorStateList.valueOf(Utils.getColor(context, R.color.yellow)));
            } else {
                textView.setText(context.getResources().getString(R.string.question));
                textView.setBackgroundTintList(ColorStateList.valueOf(Utils.getColor(context, R.color.green)));
            }
        } else {
            textView.setText(". . .");
        }
    }

    public static String getStringGson(HashMap<?, ?> hashMap){
        return new Gson().toJson(hashMap);
    }

    public static HashMap<String,String> getReportType() {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(StaticUtilsKey.inspection_action, "audit");
        hashMap.put(StaticUtilsKey.maintenance_action, "maintenance");
        hashMap.put(StaticUtilsKey.pre_handover_action, "pre_handover");
        return hashMap;
    }

    // ============ Custom Toolbar ================
    public static void customOnToolbar (Activity activity, String title, ClickCallBackBtnListener clickCallBackBtnListener) {
        TextView titleToolbar = activity.findViewById(R.id.titleToolbar);
        titleToolbar.setText(title);
        activity.findViewById(R.id.iconBack).setOnClickListener(v -> clickCallBackBtnListener.onClickBackBtn());
    }

    public static void customOnToolbarImageHeader (Activity activity, String title, String imgCoverString, int imageCover, ClickCallBackBtnListener clickCallBackBtnListener) {
        TextView titleHeader = activity.findViewById(R.id.titleHeader);
        titleHeader.setVisibility(View.VISIBLE);
        titleHeader.setText(title);
        ImageView imageCoverBooking = activity.findViewById(R.id.img_cover);
        if (imgCoverString != null) {
            Glide.with(activity).load(imgCoverString).into(imageCoverBooking);
        } else if (imageCover != 0){
            Glide.with(activity).load(imageCover).into(imageCoverBooking);
        }
        activity.findViewById(R.id.btn_back).setOnClickListener(v -> clickCallBackBtnListener.onClickBackBtn());
    }

    public interface ClickCallBackBtnListener{
        void onClickBackBtn();
    }

    public static HashMap<String,String> statusAttendanceDetail(){
        HashMap<String,String> hashMap = new HashMap<>();
        hashMap.put("check_out", "OUT");
        hashMap.put("check_in", "IN");
        hashMap.put("break_out", "Break Out");
        hashMap.put("break_in", "Break In");
        return hashMap;
    }

    // ============ Custom Alert Dialog ================
    public static void customAlertDialogConfirm(Context context, String message, ClickCallBackBtnListener clickOnOkBtnCallBackListener){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog
                .setTitle(context.getResources().getString(R.string.confirm))
                .setMessage(message)
                .setPositiveButton(context.getResources().getString(R.string.yes), (dialog, which) -> {
                    clickOnOkBtnCallBackListener.onClickBackBtn();
                    dialog.dismiss();
                })
                .setNegativeButton(context.getResources().getString(R.string.no), (dialog, which) -> dialog.dismiss())
                .setCancelable(false)
                .setIcon(ResourcesCompat.getDrawable(context.getResources(), R.drawable.ic_report_problem, null));

        AlertDialog alert = alertDialog.create();
        alert.show();
        Button cancelButton = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
        cancelButton.setTextColor(Utils.getColor(context, R.color.black));
        Button okButton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        okButton.setTextColor(Utils.getColor(context, CheckIsAppActive.Companion.is_daikou_active() ? R.color.appBarColorOld : R.color.book_now_secondary));
    }

    public static Float formatDecimalFormatValue(Double fPri) {
        DecimalFormat formatDecimal = new DecimalFormat("#.##");
        return java.lang.Float.valueOf(formatDecimal.format(fPri));
    }

    // Set key and value in booking
    public static HashMap<String,String> getValueTypePriceBooking(Context context){
        HashMap<String,String> hashMap = new HashMap<>();
        hashMap.put(StaticUtilsKey.hourly, Utils.getText(context, R.string.hourly));
        hashMap.put(StaticUtilsKey.time_membership, Utils.getText(context, R.string.time_membership));
        hashMap.put(StaticUtilsKey.schedule_membership, Utils.getText(context, R.string.schedule_membership));
        hashMap.put(StaticUtilsKey.free, Utils.getText(context, R.string.free));
        hashMap.put(StaticUtilsKey.daily, Utils.getText(context, R.string.daily));
        return hashMap;
    }

    public static void validateViewNoItemFound(Activity activity, View viewData, boolean isNoItemFound) {
        LinearLayout noDataFound = activity.findViewById(R.id.noDataLayout);
        viewData.setVisibility(isNoItemFound ? View.GONE : View.VISIBLE);
        noDataFound.setVisibility(isNoItemFound ? View.VISIBLE : View.GONE);
    }

    public static void setNoMarginBottomOnButton(View view, int bottom){
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
        layoutParams.setMargins(0, 0, 0, bottom);
        view.setLayoutParams(layoutParams);
    }

    public static void setMarginBottomOnCoordinatorLayout(View view, int bottom){
        CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) view.getLayoutParams();
        layoutParams.setMargins(0, 0, 0, bottom);
        view.setLayoutParams(layoutParams);
    }

    public static HashMap<String,String> getSatisfaction(Context context){
        HashMap<String,String> hashMap = new HashMap<>();
        hashMap.put("very_dissatisfied", "Very Dissatisfaction");
        hashMap.put("dissatisfied", "Dissatisfaction");
        hashMap.put("ok", "Normal");
        hashMap.put("satisfied", "Satisfaction");
        hashMap.put("very_satisfied", "Very Satisfaction");
        return hashMap;
    }

    public static HashMap<String,Integer> getSatisfactionIcon(){
        HashMap<String,Integer> hashMap = new HashMap<>();
        hashMap.put("very_dissatisfied", R.drawable.ic_very_dissatisfied);
        hashMap.put("dissatisfied", R.drawable.ic_dissatisfied);
        hashMap.put("ok", R.drawable.ic_normal);
        hashMap.put("satisfied", R.drawable.ic_satisfied);
        hashMap.put("very_satisfied", R.drawable.ic_very_satisfied);
        return hashMap;
    }

    public static void setBgTint(View view, int color){
        view.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(view.getContext(), color)));
    }

    public static RecyclerView.LayoutManager spanGridLayoutManager(Context context, int size){    // Verticle scroll
        GridLayoutManager layoutManager = new GridLayoutManager(context, 2);
        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if (size -1 == position) {
                    if (position % 2 == 0) {
                        return 2;   // Take SpanCount(2) / 2 = 1 so result 1 column per row
                    } else {
                        return 1;   // Take SpanCount(2) / 1 = 2 so result 2 column per row
                    }
                }
                return 1;   //Span 1 colunm to 2 colunm
            }
        });
        return layoutManager;
    }

    public static RecyclerView.LayoutManager spanHorizentalGridLayoutManager(Context context, Boolean isHotel) {     // Horizontal scroll
        GridLayoutManager layoutManager = isHotel ? new AbsoluteFitLayoutManager(context, 2, RecyclerView.HORIZONTAL, false, 2) :
                                                             new GridLayoutManager(context, 2, GridLayoutManager.HORIZONTAL, false);
        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if (position % 3 == 0) {
                    return 2;   // Take SpanCount(2) / 2 = 1 so result 1 column per row
                } else {
                    return 1;   // Take SpanCount(2) / 1 = 2 so result 2 column per row
                }
            }
        });
        return layoutManager;
    }

    public static RecyclerView.LayoutManager spanGridImageLayoutManager(ArrayList<String> list, Context context) {    // Image Layout View
        GridLayoutManager gridLayoutManager = new GridLayoutManager(context, 6);    //show only maximum 5 images
        if (list.size() < 5){
            if (list.size() % 2 == 0){ // 2 or 4 images size
                gridLayoutManager = new GridLayoutManager(context, 2);
            } else {
                if (list.size() < 5){
                    gridLayoutManager = new GridLayoutManager(context, 2);
                    gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                        @Override
                        public int getSpanSize(int position) {
                            if (position % 3 == 0) {
                                return 2;   // Take SpanCount(2) / 2 = 1 so result 1 column per row
                            } else {
                                return 1;   // Take SpanCount(2) / 1 = 2 so result 2 column per row
                            }
                        }
                    });
                } else {
                    gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                        @Override
                        public int getSpanSize(int position) {
                            if (position == 0 || position == 1){
                                return 3;   // Take SpanCount(6) / 3 = 2 so result 2 column per row
                            } else if (position == 2 || position == 3 || position == 4){
                                return 2; // Take SpanCount(6) / 2 = 3 so result 3 column per row
                            } else {
                                return 3;// Take SpanCount(6) / 3 = 2 so result 2 column per row
                            }
                        }
                    });
                }
            }
        } else {
            gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    if (position == 0 || position == 1){
                        return 3;   // Take SpanCount(6) / 3 = 2 so result 2 column per row
                    } else if (position == 2 || position == 3 || position == 4){
                        return 2; // Take SpanCount(6) / 2 = 3 so result 3 column per row
                    } else {
                        return 3;// Take SpanCount(6) / 3 = 2 so result 2 column per row
                    }
                }
            });
        }

        return gridLayoutManager;
    }

    public static void setResultBack(Activity context, String keySuccess, Boolean isFinishClass){
        Intent intent = new Intent();
        intent.putExtra("action", keySuccess);
        intent.putExtra("is_result_success", true);
        context.setResult(RESULT_OK, intent);
        if (isFinishClass)  context.finish();
    }

    public static void openFilePdfOrPic(String url, Context context){
        if (url.contains(".pdf")) {
            Utils.openDefaultPdfView(context, url);
        } else {
            Intent intent = new Intent(context, WebsiteActivity.class);
            intent.putExtra("linkUrlNews", url);
            context.startActivity(intent);
        }
    }

    public static void customAnimationBackground(View view, int colorFrom, int colorTo){
        ValueAnimator colorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), colorFrom, colorTo);
        colorAnimation.setDuration(250);
        colorAnimation.addUpdateListener(animator -> view.setBackgroundColor((int) animator.getAnimatedValue()));
        colorAnimation.start();
    }

    // Set height layout of screen
    public static Integer setHeightOfScreen(Context context, Double numberDivide){
        int height = Utils.getScreenHeight(context);
        return (int) (height / numberDivide);
    }

    // Get Color
    public static Integer setColorBackground(int j, Utils.ClickCallBackResultItemListener clickCallBackBtnListener) {   //start index j from 0
        Integer[] itemColorArr = {R.color.greenSea,R.color.yellow, R.color.red, R.color.blue, R.color.color_wooden, R.color.appBarColorOld, R.color.light_blue_600, R.color.mid_night_blue};
        if (j % itemColorArr.length == 0 && j > 0)    j = 0;
        j++;
        clickCallBackBtnListener.onClickBackBtn(j);
        return itemColorArr[j - 1];
    }

    public interface ClickCallBackResultItemListener{
        void onClickBackBtn(int index);
    }

    public static void checkEnableView(View view){
        view.setEnabled(false);
        view.postDelayed(() -> view.setEnabled(true),500);
    }

    public static boolean isEmployeeRole(Context context){
        User user = MockUpData.getUserItem(new UserSessionManagement(context));
        if (user.getActiveUserType() == null)   return false;
        return !user.getActiveUserType().equalsIgnoreCase("owner")
                && !user.getActiveUserType().equalsIgnoreCase("tenant")
                && !user.getActiveUserType().equalsIgnoreCase("customer")
                && !MockUpData.isUserAsGuest(new UserSessionManagement(context));
    }

    public static String validateNullValue(String value){
        return value != null ? value : "";
    }

    // check status for service property provider
    public static ColorStateList setColorStatusService(Context context, String valueStatus) {
        switch (valueStatus){
            case "new" :
                return ColorStateList.valueOf(ContextCompat.getColor(context, R.color.appBarColorOld));
            case "arriving" :
                return ColorStateList.valueOf(ContextCompat.getColor(context, R.color.green));
            case "progressing" :
                return ColorStateList.valueOf(ContextCompat.getColor(context, R.color.yellow));
            case "completed" :
                return ColorStateList.valueOf(ContextCompat.getColor(context, R.color.blue));
            case "incomplete" :
                return ColorStateList.valueOf(ContextCompat.getColor(context, R.color.red));
        }
        return null;
    }

    public static HashMap<String, String> getValueFromKeyStatusService(Context context) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("new", Utils.getText(context, R.string.new_));
        hashMap.put("arriving", Utils.getText(context, R.string.arriving));
        hashMap.put("progressing",Utils.getText(context, R.string.progressing));
        hashMap.put("completed", Utils.getText(context, R.string.completed));
        hashMap.put("incomplete", Utils.getText(context,R.string.uncompleted));
        return hashMap;
    }

    public static Drawable setDrawable(Resources resources, int drawable) {
        return ResourcesCompat.getDrawable(resources, drawable, null);
    }

    public static void startNewActivity(Context context, Class<?> toActivityClass, String key, String action) {
        Intent intent = new Intent(context, toActivityClass);
        intent.putExtra(key, action);
        context.startActivity(intent);
    }

    public static void displayImageMultiple(ViewPager viewPager, TabLayout tabLayout, ArrayList<String> image){
        //When have view tab and view pager
        if (image.size() > 0 ) {
            viewPager.setAdapter(new ViewPagerItemImagesAdapter(viewPager.getContext(), image));
            tabLayout.setupWithViewPager(viewPager, true);
        }
    }

    public static void getLastVersionApp(Context context){
        AppUpdateManager appUpdateManager = AppUpdateManagerFactory.create(context);
        Task<AppUpdateInfo> appUpdateInfoTask = appUpdateManager.getAppUpdateInfo();

        appUpdateInfoTask.addOnSuccessListener(appUpdateInfo -> {
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                    // This example applies an immediate update. To apply a flexible update
                    // instead, pass in AppUpdateType.FLEXIBLE
                    && appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE)) {
                AppAlertCusDialog.checkUpdatePopUp(context);
            }
        });
    }

    public static void setTextStrikeStyle(TextView textView){
        // Strike through
        textView.setPaintFlags(textView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
    }

    public static void startOpenLinkMaps(Context context, String coordLat, String coordLong){
        String uri = "http://maps.google.com/maps?q=loc:" + coordLat + "," + coordLong;
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        intent.setPackage("com.google.android.apps.maps");
        context.startActivity(intent);
    }

    public static void disconnectFromFacebook() {   //Logout Facebook
        if (AccessToken.getCurrentAccessToken() == null) {
            return; // already logged out
        }
        new GraphRequest(AccessToken.getCurrentAccessToken(), "/me/permissions/", null, HttpMethod.DELETE, graphResponse -> LoginManager.getInstance().logOut()).executeAsync();
    }

    public static void setViewSliderImage(ImageSlider slider, ArrayList<SlideModel> slideModels) {   //slider image
        slider.setImageList(slideModels, ScaleTypes.CENTER_CROP);
    }

    public static void shareLinkMultipleOption(Context context, String shareBodyText, String titleHeader) {  //Chat, Telegram ...
        if (shareBodyText == null) return;
        try {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("text/plain");
            intent.putExtra(Intent.EXTRA_SUBJECT, titleHeader != null ? titleHeader : "");
            intent.putExtra(Intent.EXTRA_TEXT, shareBodyText);
            context.startActivity(Intent.createChooser(intent, "Choose sharing method"));
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @SuppressLint("ObsoleteSdkInt")
    public static void setTextHtml(TextView textView, String value){
        if (value != null) {
            textView.setText((Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) ?
                Html.fromHtml(value, Html.FROM_HTML_MODE_LEGACY) : Html.fromHtml(value));
        } else {
            textView.setText(". . .");
        }
    }

    public static void setEdInputTypeMode(EditText edt, int inputType) {
        edt.setInputType(inputType);
        edt.setTransformationMethod(PasswordTransformationMethod.getInstance());
    }

}
