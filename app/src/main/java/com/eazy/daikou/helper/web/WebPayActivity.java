package com.eazy.daikou.helper.web;

import static com.eazy.daikou.helper.Utils.logDebug;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Base64;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.ConsoleMessage;
import android.webkit.SafeBrowsingResponse;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.eazy.daikou.R;
import com.eazy.daikou.repository_ws.Constant;
import com.eazy.daikou.base.BaseActivity;

public class WebPayActivity extends BaseActivity {

    private String getLinkUrl;
    private ProgressBar progressBar;
    private WebView webView;
    private TextView txtToolbar;
    private boolean isOpenDeepLink = false;
    private String urlSuccess = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_pay);

        initView();

        initData();

        initAction();
    }

    private void initView(){
        webView = findViewById(R.id.webViewUrl);
        progressBar = findViewById(R.id.progressItem);
        txtToolbar = findViewById(R.id.titleToolbar);
        ImageView backBtn = findViewById(R.id.backWeb);

        backBtn.setVisibility(View.VISIBLE);

        backBtn.setOnClickListener(view -> {
            if (urlSuccess.contains("success=1")) {
                Intent intent =new Intent();
                intent.putExtra("status","success=1");
                setResult(RESULT_OK,intent);
                finish();
                return;
            }

            if (webView.canGoBack()) {
                webView.goBack();
            }  else {
               backFromPaymentWebView();
            }
        });

    }

    private void backFromPaymentWebView(){
        Intent intent =new Intent();
        intent.putExtra("is_back_payment",true);
        setResult(RESULT_OK, intent);
        finish();
    }

    private void initData(){
        if (getIntent() != null && getIntent().hasExtra("linkUrl")) {
            getLinkUrl = getIntent().getStringExtra("linkUrl");
        }

        if (getIntent() != null && getIntent().hasExtra("toolBarTitle")){
            txtToolbar.setText(getIntent().getStringExtra("toolBarTitle"));
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void initAction(){
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.setWebViewClient(new MyWebViewClient());
        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
                webView.setWebChromeClient(new WebChromeClient() {
                    @Override
                    public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
                        logDebug("webViewUrl" , "Console  : " + consoleMessage.message());
                        return true;
                    }
                });
                return super.onConsoleMessage(consoleMessage);
            }
        });
        webView.setNetworkAvailable(true);
        if (getIntent().getStringExtra("toolBarTitle").equals("card")){
            String data = Base64.encodeToString(getLinkUrl.getBytes(), Base64.NO_PADDING);
            webView.loadData(data, "text/html", "base64");
        } else {
            webView.loadUrl(getLinkUrl);
        }
    }

    public class MyWebViewClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            logDebug("webViewUrl" , "url start :       "+ url);
            urlSuccess = url;
            progressBar.setVisibility(View.VISIBLE);
            view.setVisibility(View.GONE);

            if(!isOpenDeepLink){
                super.onPageStarted(view, url, favicon);
            } else {
                view.setVisibility(View.GONE);
                isOpenDeepLink = false;
                webView.goBack();
            }

            if (view.getVisibility() == View.GONE){
                progressBar.setVisibility(View.VISIBLE);
            } else {
                progressBar.setVisibility(View.GONE);
            }
        }

        @Override
        public void onSafeBrowsingHit(WebView view, WebResourceRequest request, int threatType, SafeBrowsingResponse callback) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O_MR1) {
                callback.backToSafety(false);
            }
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            return super.shouldOverrideUrlLoading(view, request);
//            String url = request.getUrl().toString();
//
//            logDebug("webViewUrl" , "url :       "+ url);
//            logDebug("webViewUrl" , "uri :       "+ Uri.parse(url));
//
//            progressBar.setVisibility(View.VISIBLE);
//            if (!url.contains(Constant.baseUrl + "/")){
//                if(url.contains(Constant.kess_url_dev) || url.contains(Constant.kess_url) || !url.contains("https://") || url.contains(".apk")) {
//                    view.setVisibility(View.GONE);
//                    try {
//                        isOpenDeepLink = true;
//                        // startOpenDeepLink(request.getUrl(), Constant.kessChatDeepLink);
//                        Intent intent = new Intent(Intent.ACTION_VIEW);
//                        intent.setData(request.getUrl());
//                        startActivity(intent);
//                    } catch (Exception e) {
//                        isOpenDeepLink = true;
//                        progressBar.setVisibility(View.GONE);
//                        Toast.makeText(WebPayActivity.this, " Please install the app",Toast.LENGTH_SHORT).show();
//                    }
//                }
//            }
//
//            if (view.getVisibility() == View.GONE){
//                progressBar.setVisibility(View.VISIBLE);
//            } else {
//                progressBar.setVisibility(View.GONE);
//            }
//
//            return true;
        }

        @SuppressWarnings("deprecation")  // When use @SuppressWarnings("deprecation") is not deprecated on method "shouldOverrideUrlLoading"
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            super.shouldOverrideUrlLoading(view, url);
            logDebug("webViewUrl" , "url :       "+ url);
            urlSuccess = url;
            progressBar.setVisibility(View.VISIBLE);
            if (!url.contains(Constant.baseUrl + "/")){
                if(url.contains(Constant.kess_url) || !url.contains("https://") || url.contains(".apk")) {
                    view.setVisibility(View.GONE);
                    try {
                        isOpenDeepLink = true;
                        logDebug("webViewUrl" , "uri :       "+ Uri.parse(url));
                        Uri uri = Uri.parse(url);
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(uri);
                        startActivity(intent);
                    } catch (Exception e) {
                        isOpenDeepLink = true;
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(WebPayActivity.this, "Please install the app",Toast.LENGTH_SHORT).show();
                    }
                }
                else if (url.contains("success=1")) {
                    Intent intent =new Intent();
                    intent.putExtra("status","success=1");
                    setResult(RESULT_OK,intent);
                    finish();
                }  else if (url.contains("success=0")) {
                    Intent intent =new Intent();
                    intent.putExtra("status","success=0");
                    setResult(RESULT_OK,intent);
                    finish();
                }
            }

            if (view.getVisibility() == View.GONE){
                progressBar.setVisibility(View.VISIBLE);
            } else {
                progressBar.setVisibility(View.GONE);
            }

            return false;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            logDebug("webViewUrl" , "url finish :       "+ url);
            super.onPageFinished(view, url);
            progressBar.setVisibility(View.VISIBLE);
            urlSuccess = url;

            if (!url.contains(Constant.baseUrl+"/")){
                if (url.contains(Constant.kess_url) || !url.contains("https://") || url.contains(".apk")) {
                    view.setVisibility(View.GONE);
                } else if (url.contains("success=1")) {
                    Intent intent =new Intent();
                    intent.putExtra("status","success=1");
                    setResult(RESULT_OK,intent);
                    finish();
                }  else if (url.contains("success=0")) {
                    Intent intent =new Intent();
                    intent.putExtra("status","success=0");
                    setResult(RESULT_OK,intent);
                    finish();
                }  else {
                    view.setVisibility(View.VISIBLE);
                }
            } else if (url.contains("success=1")) {
                Intent intent =new Intent();
                intent.putExtra("status","success=1");
                setResult(RESULT_OK,intent);
                finish();
            } else if (url.contains("success=0")) {
                Intent intent =new Intent();
                intent.putExtra("status","success=0");
                setResult(RESULT_OK,intent);
                finish();
            } else {
                view.setVisibility(View.VISIBLE);
            }

            if (view.getVisibility() == View.GONE){
                progressBar.setVisibility(View.VISIBLE);
            } else {
                progressBar.setVisibility(View.GONE);
            }

        }
    }

    private void startOpenDeepLink(Uri url, String packageName) {
        Boolean isAppInstalled = isAppAvailable(this, packageName);
        Intent intent = new Intent(Intent.ACTION_VIEW);
        if (isAppInstalled) {
            intent.setData(url);
        } else {
            intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=" + packageName));
        }
        startActivity(intent);
    }

    private Boolean isAppAvailable(Context context, String appName) {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(appName, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    @Override
    protected void onResume() {
        logDebug("webViewUrl", "onResume " + webView.getUrl());
        super.onResume();
        webView.goBack();
        if (webView != null) {
            webView.onResume();
        }

    }

    @Override
    public void onBackPressed() {
        if (urlSuccess.contains("success=1")) {
            Intent intent =new Intent();
            intent.putExtra("status","success=1");
            setResult(RESULT_OK,intent);
            finish();
            return;
        }
        setResult(RESULT_OK);
        if (webView.canGoBack()) {
            webView.goBack();
        } else {
            backFromPaymentWebView();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && webView.canGoBack()) {
            if (urlSuccess.contains("success=1")) {
                Intent intent =new Intent();
                intent.putExtra("status","success=1");
                setResult(RESULT_OK,intent);
                finish();
            }
            webView.goBack();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

}
