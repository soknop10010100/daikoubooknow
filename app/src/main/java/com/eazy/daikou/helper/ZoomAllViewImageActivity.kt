package com.eazy.daikou.helper

import android.annotation.SuppressLint
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import androidx.viewpager.widget.ViewPager
import com.eazy.daikou.R
import com.eazy.daikou.base.BaseActivity
import com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.adapter.ListImageAdapter


class ZoomAllViewImageActivity : BaseActivity() {

    private lateinit var listImageAdapter: ListImageAdapter
    private lateinit var viewPager: ViewPager
    private var image = ArrayList<String>()
    private var totalCount:Int = 0
    private var countTv : TextView? = null
    private var count:Int = 1

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView( R.layout.activity_view_full_image)

        if (intent.hasExtra("image")){
            image = intent.getSerializableExtra("image") as  ArrayList<String>
        }

        viewPager = findViewById(R.id.viewpagers)
        val btnBack = findViewById<ImageView>(R.id.btnBacks)
        countTv = findViewById(R.id.countItem)

        btnBack.setOnClickListener { finish() }

        listImageAdapter = ListImageAdapter(supportFragmentManager, image, "viewImage")
        viewPager.adapter = listImageAdapter

        totalCount = image.size

        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
            @SuppressLint("SetTextI18n")
            override fun onPageSelected(position: Int) {
                count = position + 1
                countTv?.text = "$count/$totalCount"
            }

        })
        countTv?.text = "$count/$totalCount"
        listImageAdapter.notifyDataSetChanged()
    }
}