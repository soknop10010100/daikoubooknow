package com.eazy.daikou.helper.adapter;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.eazy.daikou.R;
import com.eazy.daikou.model.home.LanguageModel;

import java.util.List;

public class AlertDialogAdapter extends RecyclerView.Adapter<AlertDialogAdapter.ItemViewHolder> {

    private final List<LanguageModel> modelList;
    private final ClickCallBackListener clickCallBackListener;
    private final Context context;

    public AlertDialogAdapter(Context context, List<LanguageModel> modelList, ClickCallBackListener clickCallBackListener) {
        this.context = context;
        this.modelList = modelList;
        this.clickCallBackListener = clickCallBackListener;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rowView = LayoutInflater.from(parent.getContext()).inflate(R.layout.language_model,parent,false);
        return new ItemViewHolder(rowView);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        LanguageModel languageModel = modelList.get(position);
        if (languageModel != null){
            holder.iconFlag.setImageResource(languageModel.getIconFlag());
            holder.nameLanguage.setText(languageModel.getNameLanguage());
            holder.iconFlag.setColorFilter(context.getColor(R.color.black));
            holder.nameLanguage.setTextColor(context.getColor(R.color.gray));
            holder.iconTech.setVisibility(View.INVISIBLE);

            holder.linearLayout.setOnClickListener(view -> {
                clickCallBackListener.clickCallBack(position);
            });
        }
    }

    @Override
    public int getItemCount() {
        return modelList.size();
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder{
        private final TextView nameLanguage;
        private final ImageView iconFlag, iconTech;
        private final LinearLayout linearLayout;
        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            iconTech = itemView.findViewById(R.id.icon_tech_default);
            nameLanguage = itemView.findViewById(R.id.name_language);
            iconFlag = itemView.findViewById(R.id.icon_flag);
            linearLayout = itemView.findViewById(R.id.lin_parent);
        }
    }

    public interface ClickCallBackListener{
        void clickCallBack(int position);
    }
}
