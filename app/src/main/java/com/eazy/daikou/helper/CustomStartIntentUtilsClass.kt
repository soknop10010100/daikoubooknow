package com.eazy.daikou.helper

import android.content.Context
import android.content.Intent
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.eazy.daikou.R
import com.eazy.daikou.helper.web.WebsitePropertyActivity
import com.eazy.daikou.ui.SplashScreenActivity
import com.eazy.daikou.ui.home.book_now.booking_hotel.travel_talk.HotelProfileItemFragment

class CustomStartIntentUtilsClass {

    companion object {

        fun onStartSplashScreenActivity(context: Context, key: String) {
            val intent = Intent(context, SplashScreenActivity::class.java)
            intent.putExtra("action", key)
            context.startActivity(intent)
        }

        fun onStartPaymentInvoiceActivity(context: Context, status: String?, urlInvoice: String?, linkPaymentKess: String?) {
            if (urlInvoice == null){
                AppAlertCusDialog.underConstructionDialog(context, "The payment invoice is not generated yet.")
                return
            }
            // Open Web View
            val intent = Intent(context, WebsitePropertyActivity::class.java).apply {
                putExtra("status", status)
                putExtra("url_web", urlInvoice)
                putExtra("link_kess", linkPaymentKess)
            }
            context.startActivity(intent)
        }

        fun refreshFragment(mIdFromFragment : Int, mToFragment : Fragment, supportFragmentManager: FragmentManager) {
            val ft: FragmentTransaction = supportFragmentManager.beginTransaction()
            ft.replace(mIdFromFragment, mToFragment)
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            ft.addToBackStack(null)
            ft.commit()
        }

    }

}