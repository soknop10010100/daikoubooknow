package com.eazy.daikou.helper.web

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CheckConnectionOnWebView
import com.eazy.daikou.base.BaseFragment
import com.eazy.daikou.helper.InternetConnection
import com.eazy.daikou.helper.Utils
import java.util.*

class WebsiteFragment : BaseFragment() {

    private var webViewLayout: RelativeLayout? = null
    private var noInternet: ConstraintLayout? = null
    private var progressBar: ProgressBar? = null
    private var url_website: String? = null
    private var action: String? = null
    private var webView: WebView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null && requireArguments().containsKey("url_website")) {
            url_website = requireArguments().getString("url_website")
            action = requireArguments().getString("action")
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.activity_website_property, container, false)

        webView = rootView.findViewById(R.id.webView)
        noInternet = rootView.findViewById(R.id.no_internet_connection)
        webViewLayout = rootView.findViewById(R.id.webViewLayout)
        progressBar = rootView.findViewById(R.id.progressItem)

        //Title Toolbar
        rootView.findViewById<View>(R.id.iconBack).visibility = View.GONE
        val titleToolbar = rootView.findViewById<TextView>(R.id.titleToolbar)
        titleToolbar.text = Utils.getText(mActivity, R.string.news).uppercase(Locale.ROOT)

        loadWeb(rootView, url_website)

        // Init data
        if (action == "tourism_map") {
            rootView.findViewById<LinearLayout>(R.id.lin).visibility = View.GONE
        }

        return rootView
    }

    private val onConnectionListener: CheckConnectionOnWebView = object : CheckConnectionOnWebView {
        override fun onConnected(isConnected: Boolean) {
            noInternet!!.visibility = View.VISIBLE
            webViewLayout!!.visibility = View.GONE
        }

        override fun urlWebView(url: String) {
            url_website = url
        }
    }

    private fun loadWeb(view: View, URL: String?) {
        noInternet!!.visibility =
            if (InternetConnection.checkInternetConnection(view)) View.GONE else View.VISIBLE
        webViewLayout!!.visibility =
            if (InternetConnection.checkInternetConnection(view)) View.VISIBLE else View.GONE
        if (WebViewUtil.checkInternetConnection(view)) {
            WebViewUtil.loadingWebView(
                webView,
                URL,
                progressBar,
                mActivity,
                view,
                onConnectionListener
            )
        } else {
            Utils.delayProgressBar(progressBar)
        }
    }

    companion object {
        fun newInstance(url: String?, action: String?): WebsiteFragment {
            val fragment = WebsiteFragment()
            val args = Bundle()
            args.putString("url_website", url)
            args.putString("action", action)
            fragment.arguments = args
            return fragment
        }
    }

}