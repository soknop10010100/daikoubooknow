package com.eazy.daikou.helper.web;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.RequiresApi;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.eazy.daikou.R;
import com.eazy.daikou.repository_ws.CheckConnectionOnWebView;
import com.eazy.daikou.base.BaseActivity;
import com.eazy.daikou.helper.InternetConnection;
import com.eazy.daikou.helper.Utils;
import com.eazy.daikou.ui.home.data_record.PayLateRequestDataRecordActivity;

public class WebsitePropertyActivity extends BaseActivity {

    private RelativeLayout webViewLayout;
    private ConstraintLayout noInternet;
    private ProgressBar progressBar;
    private View view;
    private TextView btnPay, btnPayLate;
    private WebView webView;
    private String kessUrl, url_website;
    private LinearLayout constraintLayoutToolbar;
    private ImageView fabBackIcon;
    private TextView titleToolBar;

    @SuppressLint("InflateParams")
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_website_property);
        view = LayoutInflater.from(this).inflate(R.layout.activity_website_property, null);

        initView();

        initData();

        intAction();

    }

    private void initView() {
        webView = findViewById(R.id.webView);
        findViewById(R.id.layoutMap).setVisibility(View.INVISIBLE);

        btnPayLate = findViewById(R.id.btnPayLate);
        noInternet = findViewById(R.id.no_internet_connection);
        webViewLayout = findViewById(R.id.webViewLayout);
        progressBar = findViewById(R.id.progressItem);
        progressBar.setVisibility(View.VISIBLE);
        btnPay = findViewById(R.id.btnPay);
        constraintLayoutToolbar = findViewById(R.id.lin);
        fabBackIcon = findViewById(R.id.iconBackFab);
        titleToolBar = findViewById(R.id.titleToolbar);
        titleToolBar.setText(R.string.app_name);
    }

    private void intAction() {

        findViewById(R.id.iconBack).setOnClickListener(view ->  finish());
        findViewById(R.id.btn_refresh_fragment).setOnClickListener(view -> web(url_website));

        // Init Data
        initDataOldVersion();

        // Init Action
        web(url_website);

        // Do action link kess pay
        btnPay.setOnClickListener(v -> {
            if (kessUrl != null && !kessUrl.equals("")) {
                Intent intent = new Intent(WebsitePropertyActivity.this, WebPayActivity.class);
                intent.putExtra("toolBarTitle", "Kess Pay");
                intent.putExtra("linkUrl", kessUrl);
                resultLauncher.launch(intent);
            } else {
                Toast.makeText(this, getResources().getString(R.string.link_payment_is_empty), Toast.LENGTH_SHORT).show();
            }
        });

        btnPayLate.setOnClickListener(v->{
            Intent intent = new Intent(this, PayLateRequestDataRecordActivity.class);
            startActivity(intent);
        });
    }

    private void initData(){
        if (getIntent() != null && getIntent().hasExtra("url_web")) {
            setDesktopMode(webView, true);
            titleToolBar.setText(getResources().getString(R.string.payment));
            btnPayLate.setVisibility(View.GONE);
            btnPay.setVisibility(View.VISIBLE);
            // constraintLayoutToolbar.setVisibility(View.VISIBLE);
            // fabBackIcon.setVisibility(View.GONE);
            url_website = getIntent().getStringExtra("url_web");
        }

        if (getIntent() != null && getIntent().hasExtra("link_kess")) {
            kessUrl = getIntent().getStringExtra("link_kess");
        }

        if (getIntent() != null && getIntent().hasExtra("status")) {
            btnPay.setVisibility(visibleBtnPay(getIntent().getStringExtra("status")));
        }
    }

    private int visibleBtnPay(String paymentStatus){
        if (paymentStatus != null && !paymentStatus.equals("")) {
            if (!getIntent().getStringExtra("status").equals("paid")) {
                return View.VISIBLE;
            } else {
                return View.GONE;
            }
        } else  {
            return View.GONE;
        }
    }

    private void initDataOldVersion() {
        if (getIntent() != null && getIntent().hasExtra("url_sale")) {
            setDesktopMode(webView, true);
            titleToolBar.setText(getResources().getString(R.string.payment));
            constraintLayoutToolbar.setVisibility(View.GONE);
            fabBackIcon.setVisibility(View.VISIBLE);
            btnPay.setVisibility(View.VISIBLE);
            url_website = getIntent().getStringExtra("url_sale");
        }

        if (getIntent() != null && getIntent().hasExtra("url_lease")) {
            setDesktopMode(webView, true);
            titleToolBar.setText(getString(R.string.payment));
            fabBackIcon.setVisibility(View.VISIBLE);
            btnPay.setVisibility(View.VISIBLE);
            constraintLayoutToolbar.setVisibility(View.GONE);
            url_website = getIntent().getStringExtra("url_lease");
        }

        if (getIntent() != null && getIntent().hasExtra("url_operation")) {
            setDesktopMode(webView, true);
            titleToolBar.setText(getString(R.string.payment));
            btnPay.setVisibility(View.VISIBLE);
            fabBackIcon.setVisibility(View.VISIBLE);
            constraintLayoutToolbar.setVisibility(View.GONE);
            url_website = getIntent().getStringExtra("url_operation");
        }

        if (getIntent() != null && getIntent().hasExtra("url_payment_scheduled")) {
            setDesktopMode(webView, true);
            titleToolBar.setText(getString(R.string.payment_schedule));
            constraintLayoutToolbar.setVisibility(View.GONE);
            fabBackIcon.setVisibility(View.VISIBLE);
            btnPay.setVisibility(View.VISIBLE);
            url_website = getIntent().getStringExtra("url_payment_scheduled");
        }

        if (getIntent() != null && getIntent().hasExtra("type")) {
            String type = getIntent().getStringExtra("type");
            String currentLat = getIntent().getStringExtra("current_lat");
            String currentLng = getIntent().getStringExtra("current_lng");
            String lat = getIntent().getStringExtra("lat");
            String lng = getIntent().getStringExtra("lng");
            if (type.equals("provider")) {
                btnPay.setVisibility(View.VISIBLE);
               // TextView btnDirection = findViewById(R.id.text_btn);
                btnPay.setText(R.string.get_direction);
                btnPay.setOnClickListener(view -> {
                    String uri = "http://maps.google.com/maps?saddr=" + currentLat + "," + currentLng + "&daddr=" + lat + "," + lng;
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                    startActivity(intent);
                });
            }
        }

    }

    private final CheckConnectionOnWebView onConnectionListener = new CheckConnectionOnWebView() {
        @Override
        public void onConnected(boolean isConnected) {
            noInternet.setVisibility(View.VISIBLE);
            webViewLayout.setVisibility(View.GONE);
        }

        @Override
        public void urlWebView(String url) {
            url_website = url;
        }
    };

    private void web(String URL) {
        noInternet.setVisibility(InternetConnection.checkInternetConnection(view) ? View.GONE : View.VISIBLE);
        webViewLayout.setVisibility(InternetConnection.checkInternetConnection(view) ? View.VISIBLE : View.GONE);
        if (WebViewUtil.checkInternetConnection(view)) {
            WebViewUtil.loadWebView(webView, URL, progressBar, this, view, onConnectionListener);
        } else {
            Utils.delayProgressBar(progressBar);
        }
    }

    public void setDesktopMode(WebView webView, boolean enabled) {
        String newUserAgent = webView.getSettings().getUserAgentString();
        if (enabled) {
            try {
                String ua = webView.getSettings().getUserAgentString();
                String androidOSString = webView.getSettings().getUserAgentString().substring(ua.indexOf("("), ua.indexOf(")") + 1);
                newUserAgent = webView.getSettings().getUserAgentString().replace(androidOSString, "(X11; Linux x86_64)");
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            newUserAgent = null;
        }

        webView.getSettings().setUserAgentString(newUserAgent);
        webView.getSettings().setUseWideViewPort(enabled);
        webView.getSettings().setLoadWithOverviewMode(enabled);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.reload();
    }

    private final ActivityResultLauncher<Intent> resultLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        if (result.getData() != null) {
                            if (result.getData().hasExtra("status")) {
                                if (result.getData().getStringExtra("status").equals("success=1")) {
                                    webView.reload();
                                    btnPay.setVisibility(View.GONE);
                                    Utils.popupMessage(getString(R.string.payment), getString(R.string.payment_successful), WebsitePropertyActivity.this);
                                } else {
                                    webView.reload();
                                    btnPay.setVisibility(View.VISIBLE);
                                    Utils.popupMessage(getString(R.string.payment), getString(R.string.something_went_wrong), WebsitePropertyActivity.this);
                                }
                            }
                        }
                    }
                }
            });

    @Override
    public void onBackPressed() {
        finish();
    }

}