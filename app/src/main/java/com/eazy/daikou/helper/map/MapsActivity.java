package com.eazy.daikou.helper.map;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.eazy.daikou.base.BaseActivity;
import com.eazy.daikou.request_data.request.home_ws.EmergencyWs;
import com.eazy.daikou.helper.AppAlertCusDialog;
import com.eazy.daikou.helper.UserSessionManagement;
import com.eazy.daikou.helper.Utils;
import com.eazy.daikou.model.profile.User;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.eazy.daikou.R;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import static com.eazy.daikou.helper.Utils.logDebug;

public class MapsActivity extends BaseActivity implements OnMapReadyCallback{

    private GoogleMap mMap;
    private ImageView imgLocation;
    private Geocoder geocoder;
    private UserSessionManagement sessionManagement;
    private User user;
    private ProgressBar progressBar;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        sessionManagement = new UserSessionManagement(this);
        user = new Gson().fromJson(sessionManagement.getUserDetail(), User.class);
        progressBar = findViewById(R.id.progressItem);
        progressBar.setVisibility(View.GONE);
        imgLocation = findViewById(R.id.imgLocation);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        assert mapFragment != null;
        mapFragment.getMapAsync(this);

        geocoder = new Geocoder(this, Locale.getDefault());

        findViewById(R.id.cancel).setOnClickListener(view -> finish());

        findViewById(R.id.send).setOnClickListener(view -> {
            AppAlertCusDialog.writeReasonAlert(this, getResources().getString(R.string.write_reason), true, (alertDialog, editText) -> {
                progressBar.setVisibility(View.VISIBLE);
                HashMap<String, String> hashMapBody = new HashMap<>();
                hashMapBody.put("user_id", sessionManagement.getUserId());
                hashMapBody.put("description", editText.getText().toString().trim());
                hashMapBody.put("coord_lat", BaseActivity.latitude + "");
                hashMapBody.put("coord_long", BaseActivity.longitude + "");
                hashMapBody.put("active_account_id", Utils.validateNullValue(user.getAccountId()));

                new EmergencyWs().sendEmergency(MapsActivity.this, hashMapBody, callBack);
                alertDialog.dismiss();
            });
        });

    }

    private final EmergencyWs.CallBackEmergency callBack = new EmergencyWs.CallBackEmergency() {
        @Override
        public void onSuccess(String msgSuccess) {
            progressBar.setVisibility(View.GONE);
            Utils.customToastMsgError(MapsActivity.this, msgSuccess, true);

            setResult(RESULT_OK);
            finish();
        }

        @Override
        public void onSuccessFalse(String status) {
            progressBar.setVisibility(View.GONE);
            Utils.customToastMsgError(MapsActivity.this, status, false);
        }


        @Override
        public void onFailed(String mess) {
            progressBar.setVisibility(View.GONE);
            Utils.customToastMsgError(MapsActivity.this, mess, false);
        }
    };

    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(@NonNull GoogleMap googleMap) {
        mMap = googleMap;
        zoomMap(BaseActivity.latitude, BaseActivity.longitude);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled ( false );
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        mMap.getUiSettings().setCompassEnabled(false);

        // For find name of place center
        mMap.setOnCameraIdleListener (this::displayAddressByImageCenter);

        addMarker(BaseActivity.latitude, BaseActivity.longitude);
    }

    private void zoomMap(double lat , double lng) {
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), 15f));
    }

    private void addMarker(double lat, double lon){
        // Drawable circleDrawable = getResources().getDrawable(R.drawable.user_location_);
        // BitmapDescriptor markerIcon = getMarkerIconFromDrawable(circleDrawable);
        LatLng latLng = new LatLng(lat, lon);
        Marker marker = mMap.addMarker(new MarkerOptions().position(latLng)
                .title(getResources().getString(R.string.you_are_here)));
        assert marker != null;
        marker.showInfoWindow();
    }

    private Bitmap getMarkerBitmapFromView(String name) {

        View customMarkerView = ((LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_location_maps, null);

        TextView textView = customMarkerView.findViewById(R.id.unitNoTv);
        textView.setText(name);
        ImageView imageView = customMarkerView.findViewById(R.id.iconMarker);
        imageView.setImageResource(R.drawable.user_location_);
        customMarkerView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        customMarkerView.layout(0, 0, customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight());
        customMarkerView.buildDrawingCache();
        Bitmap returnedBitmap = Bitmap.createBitmap(customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN);
        Drawable drawable = customMarkerView.getBackground();
        if (drawable != null)
            drawable.draw(canvas);
        customMarkerView.draw(canvas);

        Drawable circleDrawable = getResources().getDrawable(R.drawable.user_location_);
        Canvas canvas1 = new Canvas();
        Bitmap bitmap1 = Bitmap.createBitmap(circleDrawable.getIntrinsicWidth(), circleDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        canvas1.setBitmap(bitmap1);
        circleDrawable.setBounds(0, 0, circleDrawable.getIntrinsicWidth(), circleDrawable.getIntrinsicHeight());
        circleDrawable.draw(canvas1);

        return returnedBitmap;
    }

    private BitmapDescriptor getMarkerIconFromDrawable(Drawable drawable) {
        Canvas canvas = new Canvas();
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        canvas.setBitmap(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    private void displayAddressByImageCenter() {

        if (mMap == null) return;
        try {
            int h = imgLocation.getHeight () ;
            int w = imgLocation.getWidth () / 2;
            int X = (int) imgLocation.getX () + w;
            int Y = (int) imgLocation.getY () + h / 2;
            LatLng ll = mMap.getProjection ().fromScreenLocation ( new Point( X, Y + 45) );
            List<Address> addresses = geocoder.getFromLocation ( ll.latitude, ll.longitude, 1 );
            String fullAdd = getFullAddress ( addresses );
            logDebug("fullAddress", fullAdd);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String getFullAddress(List<Address> addresses) {
        if (addresses.size () > 0) {
            return addresses.get ( 0 ).getAddressLine ( 0 );
        }
        return "";
    }
}