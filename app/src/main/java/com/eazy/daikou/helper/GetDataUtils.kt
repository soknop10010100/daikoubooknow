package com.eazy.daikou.helper

import android.app.Activity
import android.os.Bundle

class GetDataUtils {

    companion object {

        // By Intent Activity
        fun getDataFromString(key: String, context: Activity) : String{
            if (context.intent != null && context.intent.hasExtra(key)) {
                return context.intent.getStringExtra(key).toString()
            }
            return ""
        }

        fun <T> getDataFromModelClass(key: String, context: Activity) : T? {
            if (context.intent != null && context.intent.hasExtra(key)) {
                return context.intent.getSerializableExtra(key) as T
            }
            return null
        }

        fun <T> getDataFromListModelClass(key: String, context: Activity): ArrayList<T> {
            if (context.intent != null && context.intent.hasExtra(key)) {
                return context.intent.getSerializableExtra(key) as ArrayList<T>
            }
            return ArrayList()
        }

        // By NewInstant Fragment

        fun getDataFromString(key: String, arguments: Bundle?) : String{
            if (arguments != null && arguments.containsKey(key)) {
                return arguments.getString(key).toString()
            }
            return ""
        }
    }

}