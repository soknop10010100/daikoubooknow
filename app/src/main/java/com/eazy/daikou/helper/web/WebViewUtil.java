package com.eazy.daikou.helper.web;

import static android.app.Activity.RESULT_OK;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.eazy.daikou.repository_ws.Constant;
import com.eazy.daikou.repository_ws.CheckConnectionOnWebView;
import com.eazy.daikou.helper.UserSessionManagement;
import com.eazy.daikou.ui.LoginActivity;

import java.util.HashMap;
import java.util.Map;

import static com.eazy.daikou.helper.Utils.logDebug;

public class WebViewUtil {

    public static boolean isAlreadyStart = false;
    public static boolean isOpenDeepLink = false;

    @SuppressLint({"SetJavaScriptEnabled", "LongLogTag"})
    public static void loadWebView(final WebView webView, String url, final ProgressBar progressBar, final Activity activity, final View root,
                                   final CheckConnectionOnWebView listener) {
        UserSessionManagement userSessionManagement = new UserSessionManagement(activity);
        webView.setWebViewClient(new WebViewClient());

        //set Cookies to web view
        final WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setAppCacheEnabled(true);
        settings.setPluginState(WebSettings.PluginState.ON);
        settings.setDomStorageEnabled(true);
        settings.setSupportZoom(true);
        settings.setUseWideViewPort(true);
        settings.setLoadWithOverviewMode(true);
        webView.setInitialScale(1);
        webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        webView.setScrollbarFadingEnabled(false);
        webView.setNetworkAvailable(true);

        CookieSyncManager.createInstance(activity);
        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.setAcceptCookie(true);
        cookieManager.removeSessionCookie();

        if (userSessionManagement.getUserId() != null && userSessionManagement.getUserCookie() != null) {
            String userId = "acci=".concat(userSessionManagement.getUserId());
            String userCookie = "acct=".concat(userSessionManagement.getUserCookie());

            cookieManager.setCookie(Constant.baseUrl + "/", userId);
            cookieManager.setCookie(Constant.baseUrl + "/", userCookie);
        }


        CookieSyncManager.getInstance().sync();
        webView.loadUrl(url);

        webView.setWebViewClient(new WebViewClient() {
            @SuppressLint("LongLogTag")
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (checkInternetConnection(root)) {
                    if (url.contains("https://webpay.kesspay.io")){
                        if (!isAlreadyStart){
                            isAlreadyStart = true;
                            Intent intent = new Intent(activity, WebPayActivity.class);
                            intent.putExtra("toolBarTitle","Kess Pay");
                            intent.putExtra("linkUrl",url);
                            activity.startActivityForResult(intent,12112);
                        }
                    } else {
                        isAlreadyStart = false;
                        view.loadUrl(url);
                        try {
                            listener.urlWebView(url);
                            if (url.contains("index.php")) {
                                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(activity);
                                SharedPreferences.Editor editor = prefs.edit();
                                editor.putString("autoLock", "false");
                                editor.apply();
                                activity.startActivity(new Intent(activity, LoginActivity.class));
                                activity.finishAffinity();
                                return false;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    logDebug("shouldOverrideUrlLoading", "shouldOverrideUrlLoading: ");
                    webView.setVisibility(View.GONE);
                    listener.onConnected(false);
                    listener.urlWebView(url);
                    return false;
                }
                return super.shouldOverrideUrlLoading(view, url);
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                webView.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onPageCommitVisible(WebView view, String url) {
                super.onPageCommitVisible(view, url);
                progressBar.setVisibility(View.GONE);
                view.setVisibility(View.VISIBLE);
            }
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                webView.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
            }
        });

        webView.setOnKeyListener((v, keyCode, event) -> {
            if (event.getAction() == KeyEvent.ACTION_DOWN) {
                WebView webView1 = (WebView) v;
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    if (webView1.canGoBack()) {
                        if (checkInternetConnection(root)) {
                            if (webView1.canGoBack()) {
                                webView1.goBack();
                            } else {
                                activity.onBackPressed();
                            }
                        } else {
                            webView1.setVisibility(View.GONE);
                            listener.onConnected(false);
                        }
                        return true;
                    }
                }
            }
            return false;
        });

    }

    @SuppressLint({"SetJavaScriptEnabled", "LongLogTag"})
    public static void loadingWebView(final WebView webView, String url, final ProgressBar progressBar, final Activity activity, final View root,
                                   final CheckConnectionOnWebView listener) {
        webView.setWebViewClient(new WebViewClient());

        //set Cookies to web view
        final WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setAppCacheEnabled(true);
        settings.setPluginState(WebSettings.PluginState.ON);
        settings.setDomStorageEnabled(true);
        settings.setSupportZoom(true);
        settings.setUseWideViewPort(true);
        settings.setLoadWithOverviewMode(true);
        webView.setInitialScale(1);
        webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        webView.setScrollbarFadingEnabled(false);
        webView.setNetworkAvailable(true);

        webView.loadUrl(url);

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                webView.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);

                if(!isOpenDeepLink){
                    super.onPageStarted(view, url, favicon);
                } else {
                    view.setVisibility(View.GONE);
                    isOpenDeepLink = false;
                    webView.goBack();
                }
            }

            @Override
            public void onPageCommitVisible(WebView view, String url) {
                super.onPageCommitVisible(view, url);
                progressBar.setVisibility(View.GONE);
                view.setVisibility(View.VISIBLE);
            }

            @SuppressWarnings("deprecation")  // When use @SuppressWarnings("deprecation") is not deprecated on method "shouldOverrideUrlLoading"
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                super.shouldOverrideUrlLoading(view, url);
                String depLinkMap = "https://www.google.com/maps/dir"; // Open map tourism link
                logDebug("webViewUrl" , "url :       "+ url);
                progressBar.setVisibility(View.VISIBLE);
                if (!url.contains(Constant.baseUrl + "/")){
                    if (url.contains(depLinkMap)){
                        view.setVisibility(View.GONE);
                        try {
                            logDebug("webViewUrl" , "uri :       "+ Uri.parse(url));
                            startIntentView(activity, url, true);
                            isOpenDeepLink = true;
                        } catch (Exception e) {
                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(activity, "Please install the app",Toast.LENGTH_SHORT).show();
                            isOpenDeepLink = true;
                        }
                    }
                    if(url.contains(Constant.kess_url) || !url.contains("https://") || url.contains(".apk")) {
                        view.setVisibility(View.GONE);
                        try {
                            logDebug("webViewUrl" , "uri :       "+ Uri.parse(url));
                            startIntentView(activity, url, true);
                            isOpenDeepLink = true;
                        } catch (Exception e) {
                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(activity, "Please install the app",Toast.LENGTH_SHORT).show();
                            isOpenDeepLink = true;
                        }
                    }
                }

                if (view.getVisibility() == View.GONE){
                    progressBar.setVisibility(View.VISIBLE);
                } else {
                    progressBar.setVisibility(View.GONE);
                }

                return false;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                webView.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
            }
        });

        webView.setOnKeyListener((v, keyCode, event) -> {
            if (event.getAction() == KeyEvent.ACTION_DOWN) {
                WebView webView1 = (WebView) v;
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    if (webView1.canGoBack()) {
                        if (checkInternetConnection(root)) {
                            if (webView1.canGoBack()) {
                                webView1.goBack();
                            } else {
                                activity.onBackPressed();
                            }
                        } else {
                            webView1.setVisibility(View.GONE);
                            listener.onConnected(false);
                        }
                        return true;
                    }
                }
            }
            return false;
        });

    }

    private static void startIntentView(Activity mContext, String url, boolean isMapLink) {
        Uri uri = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(uri);
        if (isMapLink)  intent.setPackage("com.google.android.apps.maps");
        mContext.startActivity(intent);
    }

    public static boolean checkInternetConnection(View root) {
        ConnectivityManager manager = (ConnectivityManager) root.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = manager.getActiveNetworkInfo();
        return null != activeNetwork;
    }
}
