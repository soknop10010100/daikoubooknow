package com.eazy.daikou.helper.map;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.eazy.daikou.base.BaseActivity;
import com.eazy.daikou.request_data.request.service_property_ws.PropertyListWs;
import com.eazy.daikou.helper.UserSessionManagement;
import com.eazy.daikou.model.my_property.service_provider.SelectProjectModel;
import com.eazy.daikou.model.profile.User;
import com.eazy.daikou.ui.home.my_property_v1.SelectProjectFragment;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;

import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;

import com.eazy.daikou.R;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;


import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import static com.eazy.daikou.helper.Utils.logDebug;

public class MapsPropertyActivity extends BaseActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private Geocoder geocoder;
    private ImageView mapType, dropdownImg;
    private TextView titleToolBar;
    private SupportMapFragment mapFragment;
    private boolean getFirstClick = true, getFirst = true, firstLocation = true;

    //Select Project
    private RelativeLayout selectProject;
    private SelectProjectFragment fragmentItem, fragmentAll;
    private double longitude = 0.0, latitude = 0.0;
    private String property_id_ofBranch = "";
    private String titleToolBarSt, property_name = "";
    private User user;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps_property);

        initView();
        initData();
        initAction();
    }

    private void initView(){
        titleToolBar = findViewById(R.id.title);
        mapType = findViewById(R.id.mapsType);
        selectProject = findViewById(R.id.selectProject);
        dropdownImg = findViewById(R.id.dropdown);
    }

    private void initData(){
        if (getIntent() != null && getIntent().hasExtra("title")){
            titleToolBarSt = getIntent().getStringExtra("title");
        }

        UserSessionManagement sessionManagement = new UserSessionManagement(getApplicationContext());
        user = new Gson().fromJson(sessionManagement.getUserDetail(), User.class);

        //Show Layout Maps
        findViewById(R.id.layoutMaps).setVisibility(View.VISIBLE);
        findViewById(R.id.custom_toolBar_id).setVisibility(View.VISIBLE);
        findViewById(R.id.imgCamera).setVisibility(View.GONE);
    }

    private void initAction(){
        findViewById(R.id.back).setOnClickListener(view -> finish());
        titleToolBar.setText(titleToolBarSt);
        if (titleToolBarSt.equals(getResources().getString(R.string.select_project))){
            onClickSelectProject();
        } else {
            dropdownImg.setVisibility(View.INVISIBLE);
            selectProject.setClickable(false);

        }
        if (getFirst){
            getFirst = false;
            mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map_google);
            assert mapFragment != null;
            mapFragment.getMapAsync(this);
        }
        geocoder = new Geocoder(this, Locale.getDefault());
        changeMapsType();

    }

    private void changeMapsType(){
        mapType.setOnClickListener(view -> {
            getFirstClick = !getFirstClick;
            mapFragment.getMapAsync(this);
        });
    }
    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (!getFirstClick){
            mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        } else {
            mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings ().setZoomControlsEnabled ( false );
        mMap.getUiSettings ().setMyLocationButtonEnabled(true);
        mMap.getUiSettings().setCompassEnabled(false);//hide compass

        if (titleToolBarSt.equals(getResources().getString(R.string.select_project))){
            if (firstLocation){
                zoomMap(BaseActivity.latitude, BaseActivity.longitude);
                addMarker(BaseActivity.latitude, BaseActivity.longitude, getResources().getString(R.string.you_are_here));
            } else {
                // set location project from api
                if (latitude == 0.0 &&  longitude == 0.0){
                    Toast.makeText(MapsPropertyActivity.this, property_name + " " + getResources().getString(R.string.not_location_yet), Toast.LENGTH_SHORT).show();
                    zoomMap(BaseActivity.latitude, BaseActivity.longitude);
                    addMarker(BaseActivity.latitude, BaseActivity.longitude, getResources().getString(R.string.you_are_here));
                } else {
                    zoomMap(latitude, longitude);
                    addMarker(latitude, longitude, property_name);
                }
                // Click on Maps to set location project
                mMap.setOnMapClickListener(this::popupInformLocation);
            }
        } else {
//            if (user.getCurrentPropertyCoord().getLat() == 0.0 &&  user.getCurrentPropertyCoord().getLng() == 0.0){
//                Toast.makeText(MapsPropertyActivity.this, user.getActivePropertyTitle() + " " + getResources().getString(R.string.not_location_yet), Toast.LENGTH_SHORT).show();
//                zoomMap(BaseActivity.latitude, BaseActivity.longitude);
//                addMarker(BaseActivity.latitude, BaseActivity.longitude, getResources().getString(R.string.you_are_here));
//            } else {
//                zoomMap(user.getCurrentPropertyCoord().getLat(), user.getCurrentPropertyCoord().getLng());
//                addMarker(user.getCurrentPropertyCoord().getLat(), user.getCurrentPropertyCoord().getLng(), user.getActivePropertyTitle());
//            }
        }


    }


    private void zoomMap(double lat , double lng) {
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), 15f));
    }

    private String displayAddressByImageCenter(ImageView imgLoc) {
        if (mMap == null)  return "";
        try {
            int h = imgLoc.getHeight () ;
            int w = imgLoc.getWidth () / 2;
            int X = (int) imgLoc.getX () + w;
            int Y = (int) imgLoc.getY () + h / 2;
            LatLng ll = mMap.getProjection ().fromScreenLocation ( new Point ( X, Y + 45 ) );
            List<Address> addresses = geocoder.getFromLocation ( ll.latitude, ll.longitude, 1 );
            String fullAdd = getFullAddress ( addresses );
            logDebug("fullAddress", fullAdd);
            return fullAdd;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    private String getFullAddress(List<Address> addresses) {
        if (addresses.size () > 0) {
            return addresses.get ( 0 ).getAddressLine ( 0 );
        }
        return "";
    }

    private void addMarker(double lat, double lon, String title){
        if (mMap != null){
            mMap.clear();
            LatLng latLng = new LatLng(lat, lon);
            Marker marker = mMap.addMarker(new MarkerOptions().position(latLng).title(title));
            assert marker != null;
            marker.showInfoWindow();
        }
    }

    private BitmapDescriptor getMarkerIconFromDrawable(Drawable drawable) {
        Canvas canvas = new Canvas();
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        canvas.setBitmap(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    private void popupInformLocation(LatLng point) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(MapsPropertyActivity.this);
        View alertLayout = LayoutInflater.from(MapsPropertyActivity.this).inflate(R.layout.dialog_msg_yes_no, null);
        dialog.setView(alertLayout);
        dialog.setCancelable(false);
        AlertDialog alertDialog = dialog.show();

        TextView txtTitle = alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = alertDialog.findViewById(R.id.txtMessage);
        Button cancel = alertLayout.findViewById(R.id.btnCancel);
        Button ok = alertLayout.findViewById(R.id.btnOk);
        ProgressBar progressBarLogOut = alertLayout.findViewById(R.id.progressItem);
        progressBarLogOut.setVisibility(View.GONE);

        assert txtTitle != null;
        txtTitle.setText(getResources().getString(R.string.confirm));
        assert txtMessage != null;
        txtMessage.setText(getResources().getString(R.string.are_you_sure_to_change_your_location));

        cancel.setOnClickListener(view1 -> alertDialog.dismiss());

        ok.setOnClickListener(view1 -> {
            alertDialog.dismiss();
            HashMap<String, Object> hashMap = new HashMap<>();
            hashMap.put("property_id", property_id_ofBranch);
            hashMap.put("coord_lat", point.latitude);
            hashMap.put("coord_long", point.longitude);

            new PropertyListWs().updateLocationProject(MapsPropertyActivity.this, hashMap, new PropertyListWs.UpdateLocationCallBack() {
                @Override
                public void onStatusSuccess(String msgSuccess) {
                    addMarker(point.latitude, point.longitude, property_name);
                    Toast.makeText(MapsPropertyActivity.this, msgSuccess, Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onStatusFalse(String msgFailed) {
                    Toast.makeText(MapsPropertyActivity.this, msgFailed, Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onLoadingFailed(String msg) {
                    Toast.makeText(MapsPropertyActivity.this, msg, Toast.LENGTH_SHORT).show();
                }
            });
        });
    }

    private void onClickSelectProject(){
        selectProject.setOnClickListener(view -> {
            initDialog();
        });
    }

    private void initDialog(){
        fragmentAll = SelectProjectFragment.newInstance("ALL_PROJECT");
        fragmentAll.initBackListener(clickCallBack);
        fragmentAll.show(getSupportFragmentManager(), "fragment all project");
    }

    private final SelectProjectFragment.ClickBackListener clickCallBack = new SelectProjectFragment.ClickBackListener() {
        @Override
        public void clickBackProject(SelectProjectModel selectProjectModel) {
            fragmentItem = SelectProjectFragment.newInstance("ITEM_PROJECT", selectProjectModel.getBranchId(), selectProjectModel.getBranchName());
            fragmentItem.initBackListener(clickCallBackItem);
            fragmentItem.show(getSupportFragmentManager(), "fragment item project");
        }
    };

    private final SelectProjectFragment.ClickBackListener clickCallBackItem = selectProjectModel ->{
        fragmentAll.dismiss();
        fragmentItem.dismiss();

        property_id_ofBranch = selectProjectModel.getPropertyId();
        latitude = selectProjectModel.getLocation().getCoordLat();
        longitude = selectProjectModel.getLocation().getCoordLong();
        property_name = selectProjectModel.getBuildingName();
        firstLocation = false;
        if (latitude == 0.0 && longitude == 0.0){
            Toast.makeText(MapsPropertyActivity.this, getResources().getString(R.string.not_location_yet),Toast.LENGTH_SHORT).show();
        }
        mapFragment.getMapAsync(this);
    };


}