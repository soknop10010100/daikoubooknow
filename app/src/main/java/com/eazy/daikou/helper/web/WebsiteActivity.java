package com.eazy.daikou.helper.web;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.SafeBrowsingResponse;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;


import com.eazy.daikou.repository_ws.Constant;
import com.eazy.daikou.R;
import com.eazy.daikou.base.BaseActivity;

import java.io.IOException;

import static com.eazy.daikou.helper.Utils.logDebug;

public class WebsiteActivity extends BaseActivity {
    private WebView webView;
    private ProgressBar progressBar;
    private String url = "", action = "";
    private ImageView backBtn;
    private LinearLayout linearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_website);

        initView();

        initData();

        btnBack();

        linearLayout = findViewById(R.id.linear_card);
        linearLayout.setOnClickListener(v -> {
            Intent videoUrl = new Intent(this, WebsiteActivity.class);
            videoUrl.putExtra("linkUrlVideo", Constant.url_beetube_movies);
            startActivity(videoUrl);
        });

        try {
            openFile(WebsiteActivity.this, url);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }


    private void initView() {
        webView = findViewById(R.id.webView);
        progressBar = findViewById(R.id.progressItem);
        backBtn = findViewById(R.id.backWeb);
    }

    private void btnBack() {
        backBtn.setVisibility(View.VISIBLE);
        backBtn.setOnClickListener(view -> {
            if (action.equalsIgnoreCase("payment_from_booking")) {
                setResult(RESULT_OK);
            }
            finish();
        });
    }


    @Override
    public void onBackPressed() {
        if (action.equalsIgnoreCase("payment_from_booking")) {
            setResult(RESULT_OK);
        }
        finish();
    }

    private void initData() {
        if (getIntent() != null && getIntent().hasExtra("URL_Property_List")) {
            url = getIntent().getStringExtra("URL_Property_List");
        }
        if (getIntent() != null && getIntent().hasExtra("linkUrlNews")) {
            url = getIntent().getStringExtra("linkUrlNews");
        }

        if (getIntent() != null && getIntent().hasExtra("linkUrlVideo")) {
            url = getIntent().getStringExtra("linkUrlVideo");
        }

        if (getIntent() != null && getIntent().hasExtra("action")) {
            action = getIntent().getStringExtra("action");
        }

    }

    public class MyWebViewClient extends WebViewClient {
        @SuppressLint("WebViewClientOnReceivedSslError")
        @Override
        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
            super.onReceivedSslError(view, handler, error);
            handler.proceed();
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            logDebug("webViewUrl", "url start :       " + url);
            progressBar.setVisibility(View.VISIBLE);
            view.setVisibility(View.GONE);
        }

        @Override
        public void onPageCommitVisible(WebView view, String url) {
            super.onPageCommitVisible(view, url);
            progressBar.setVisibility(View.GONE);
            view.setVisibility(View.VISIBLE);
            if (action.equals("beetube")) {
                linearLayout.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void onSafeBrowsingHit(WebView view, WebResourceRequest request, int threatType, SafeBrowsingResponse callback) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O_MR1) {
                callback.backToSafety(false);
            }
        }

        @SuppressWarnings("deprecation")  // When use @SuppressWarnings("deprecation") is not deprecated on method "shouldOverrideUrlLoading"
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            logDebug("webViewUrl", "url :       " + url);
            webView.loadUrl(url);
            if (action.equals("beetube")) {
                linearLayout.setVisibility(View.VISIBLE);
            }
            return false;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            logDebug("webViewUrl", "url finish :       " + url);
            super.onPageFinished(view, url);
            progressBar.setVisibility(View.GONE);
            view.setVisibility(View.VISIBLE);
            if (action.equals("beetube")) {
                linearLayout.setVisibility(View.VISIBLE);
            }
        }
    }

    public void openFile(Context context, String url) throws IOException {
        Uri uri = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW);

        if (url.contains(".jpg") || url.contains(".jpeg") || url.contains(".png")) {
            intent.setDataAndType(uri, "image/jpeg");
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            try {
                context.startActivity(intent);
            } catch (Exception exception) {
                Toast.makeText(this, "Cannot Read Quote !", Toast.LENGTH_SHORT).show();
            }
            finish();
        } else {
            loadUrlWeb(url);
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void loadUrlWeb(String url) {
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.setWebViewClient(new MyWebViewClient());
        webView.setNetworkAvailable(true);
        webView.getSettings().getCacheMode();
        webView.getSettings().setGeolocationEnabled(true);
        webView.getSettings().setSupportMultipleWindows(true);
        webView.getSettings().getBlockNetworkImage();
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.loadUrl(url);

        webView.setOnKeyListener((v, keyCode, event) -> {
            if (event.getAction() == KeyEvent.ACTION_DOWN) {
                WebView webView = (WebView) v;

                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    if (webView.canGoBack()) {
                        webView.goBack();
                        return true;
                    }
                }
            }

            return false;
        });
    }

}