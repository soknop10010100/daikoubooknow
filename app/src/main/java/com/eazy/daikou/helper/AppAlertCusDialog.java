package com.eazy.daikou.helper;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.view.menu.MenuBuilder;
import androidx.appcompat.view.menu.MenuPopupHelper;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.eazy.daikou.repository_ws.Constant;
import com.eazy.daikou.repository_ws.CustomSetOnClickViewListener;
import com.eazy.daikou.base.BaseActivity;
import com.eazy.daikou.request_data.request.profile_ws.LoginWs;
import com.eazy.daikou.request_data.request.profile_ws.ProfileWs;
import com.eazy.daikou.request_data.request.sign_up_v2.CheckAccountExitsWs;
import com.eazy.daikou.request_data.sql_database.TrackWaterElectricityDatabase;
import com.eazy.daikou.request_data.static_data.StaticUtilsKey;
import com.eazy.daikou.model.profile.PropertyItemModel;
import com.eazy.daikou.model.profile.User;
import com.eazy.daikou.ui.LoginActivity;
import com.eazy.daikou.R;
import com.eazy.daikou.ui.home.book_now.booking_account.LoginBookNowActivity;
import com.eazy.daikou.ui.home.book_now.booking_account.SignUpBookNowActivity;
import com.eazy.daikou.ui.home.my_property.payment.adapter.PropertyItemAdapter;
import com.google.gson.Gson;
import com.rilixtech.widget.countrycodepicker.CountryCodePicker;
import com.tapadoo.alerter.Alerter;

import java.util.ArrayList;
import java.util.HashMap;

import libs.mjn.prettydialog.PrettyDialog;

public class AppAlertCusDialog {

    private String countryCode = "", verify_pin_code = "", phone_email = "";

    public AppAlertCusDialog() { }

    public void showDialog(Activity activity, String msg) {
        Alerter.create(activity)
                .setTitle(activity.getResources().getString(R.string.error))
                .setText(msg)
                .setBackgroundColorRes(R.color.red)
                .setIcon(R.drawable.icon_notif_error)
                .setIconColorFilter(0)
                .setDuration(1500)
                .show();

    }

    @SuppressLint("RestrictedApi")
    public void alertDialogSimple(Context context, String action,String actionClick, boolean isProfileActionClick) {
        View dialogView = LayoutInflater.from(context).inflate(R.layout.alert_change_email, null);

        // ========================= Init View =====================================
        TextView titleTv = dialogView.findViewById(R.id.titleToolbar);
        TextView titleMessage = dialogView.findViewById(R.id.titleMessage);
        TextView btnOk = dialogView.findViewById(R.id.btnOk);
        TextView btnCancel = dialogView.findViewById(R.id.btnCancel);
        EditText editText = dialogView.findViewById(R.id.editText);
        TextView btnSwitch = dialogView.findViewById(R.id.btnSwitch);
        RelativeLayout linearLayout = dialogView.findViewById(R.id.mainLayout);
        CountryCodePicker countryCodePicker = dialogView.findViewById(R.id.ccp);
        ImageView imgLogo = dialogView.findViewById(R.id.logo);
        ImageView imgLogoSuccess = dialogView.findViewById(R.id.logoSuccess);
        ProgressBar progressBar = dialogView.findViewById(R.id.progressItem);
        progressBar.setVisibility(View.GONE);

        linearLayout.setOnClickListener(v -> Utils.hideSoftKeyboard(linearLayout));

        if (StaticUtilsKey.first_action.equals(action)) {
            if (isProfileActionClick) {
                if (!StaticUtilsKey.is_email_address) {
                    titleTv.setText(context.getResources().getString(R.string.update_phone));
                    titleMessage.setText(context.getResources().getString(R.string.are_you_sure_you_want_to_change_your_phone));
                } else {
                    titleTv.setText(context.getResources().getString(R.string.update_email));
                    titleMessage.setText(context.getResources().getString(R.string.are_you_sure_you_want_to_change_your_email));
                }
            } else {
                titleTv.setText(context.getResources().getString(R.string.please_update_email_phone));
                titleMessage.setText(context.getResources().getString(R.string.instruction_for_update_email_phone));
            }
        } else if (StaticUtilsKey.second_action.equals(action)) {
            editText.setVisibility(View.VISIBLE);
            btnSwitch.setVisibility(View.VISIBLE);
            titleMessage.setText(context.getResources().getString(R.string.we_will_send_verify_code_to_your_email_address_or_phone_number_please_input_your_email_address_or_phone_number_to_get_verify_code));

            Glide.with(context).load(R.drawable.update_email_icon).into(imgLogo);

            titleTv.setText(context.getResources().getString(R.string.input_your_email_add));

            if (isProfileActionClick) {
                btnSwitch.setVisibility(View.GONE);
            }

            if (!StaticUtilsKey.is_email_address) {
                editText.setHint(context.getResources().getString(R.string.enter_your_phone_number));
                titleTv.setText(context.getResources().getString(R.string.enter_your_new_phone_number));
                titleMessage.setText(context.getResources().getString(R.string.we_will_send_a_verify_code_to_phone_number_you_enter_below));

                countryCodePicker.setVisibility(View.VISIBLE);
                editText.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                editText.setInputType(EditorInfo.TYPE_CLASS_NUMBER);
                StaticUtilsKey.is_email_address = false;
                countryCodePicker.setDefaultCountryUsingNameCodeAndApply(BaseActivity.country_code);
                countryCode = countryCodePicker.getFullNumber();
                countryCodePicker.setOnCountryChangeListener(selectedCountry -> countryCode = countryCodePicker.getFullNumber());

            } else {
                editText.setInputType(EditorInfo.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                editText.setHint(context.getResources().getString(R.string.enter_your_email));
                titleTv.setText(context.getResources().getString(R.string.enter_your_new_email));
                titleMessage.setText(context.getResources().getString(R.string.we_will_send_a_verify_code_to_email_address_you_enter_below));

                countryCodePicker.setVisibility(View.GONE);
                Drawable img = ResourcesCompat.getDrawable(context.getResources(), R.drawable.ic_email_profile, null);
                editText.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
                StaticUtilsKey.is_email_address = true;
                countryCode = "";

            }

            // Switch Menu Email Or Phone
            btnSwitch.setOnClickListener(v -> {
                MenuBuilder menuBuilder = new MenuBuilder(context);
                MenuInflater inflater = new MenuInflater(context);
                inflater.inflate(R.menu.switch_email_phone_option, menuBuilder);
                MenuPopupHelper optionsMenu = new MenuPopupHelper(context, menuBuilder, v);
                optionsMenu.setForceShowIcon(true);

                menuBuilder.setCallback(new MenuBuilder.Callback() {
                    @Override
                    public boolean onMenuItemSelected(@NonNull MenuBuilder menu, @NonNull MenuItem item) {
                        if (item.getItemId() == R.id.email){
                            editText.setInputType(EditorInfo.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                            editText.setHint(context.getResources().getString(R.string.your_email_or_phone_number));
                            titleTv.setText(context.getResources().getString(R.string.enter_your_new_email));
                            titleMessage.setText(context.getResources().getString(R.string.we_will_send_a_verify_code_to_email_address_you_enter_below));
                            countryCodePicker.setVisibility(View.GONE);
                            Drawable img = ResourcesCompat.getDrawable(context.getResources(), R.drawable.ic_email_profile, null);
                            editText.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
                            StaticUtilsKey.is_email_address = true;
                            countryCode = "";
                            editText.setText("");
                            btnSwitch.setText(context.getResources().getString(R.string.email));
                            return true;
                        } else if (item.getItemId() == R.id.phone){
                            editText.setHint(context.getResources().getString(R.string.your_phone_number));
                            titleTv.setText(context.getResources().getString(R.string.enter_your_new_phone_number));
                            titleMessage.setText(context.getResources().getString(R.string.we_will_send_a_verify_code_to_phone_number_you_enter_below));
                            countryCodePicker.setVisibility(View.VISIBLE);
                            editText.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                            editText.setInputType(EditorInfo.TYPE_CLASS_NUMBER);
                            StaticUtilsKey.is_email_address = false;
                            countryCodePicker.setDefaultCountryUsingNameCodeAndApply(BaseActivity.country_code);
                            countryCode = countryCodePicker.getFullNumber();
                            countryCodePicker.setOnCountryChangeListener(selectedCountry -> countryCode = countryCodePicker.getFullNumber());
                            editText.setText("");
                            btnSwitch.setText(context.getResources().getString(R.string.phone));
                            return true;
                        } else {
                            return false;
                        }
                    }

                    @Override
                    public void onMenuModeChange(@NonNull MenuBuilder menu) {

                    }
                });
                optionsMenu.show();
            });

            editText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @SuppressLint("Range")
                @Override
                public void afterTextChanged(Editable editable) {
                    if (!editable.toString().isEmpty()) {
                        if (StaticUtilsKey.is_email_address) {
                            if (Patterns.EMAIL_ADDRESS.matcher(editable.toString()).matches()) {
                                btnOk.setEnabled(true);
                                btnOk.setAlpha(100);
                            } else {
                                btnOk.setEnabled(false);
                                btnOk.setAlpha((float) 0.5);
                            }
                        } else {
                            btnOk.setEnabled(true);
                            btnOk.setAlpha(100);
                        }
                    } else {
                        btnOk.setEnabled(false);
                        btnOk.setAlpha((float) 0.5);
                    }
                }
            });
        } else if (StaticUtilsKey.verify_code_action.equals(action)) {
            btnOk.setText(context.getResources().getString(R.string.submit));
            editText.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
            titleTv.setText(context.getResources().getString(R.string.please_input_verify_pin_code));
            titleMessage.setText(context.getResources().getString(R.string.please_enter_the_verification_code_sending_to_your_email));
            editText.setHint(context.getResources().getString(R.string.verify_code));
            editText.setVisibility(View.VISIBLE);
            editText.setInputType(EditorInfo.TYPE_CLASS_NUMBER);
            Glide.with(context).load(R.drawable.update_email_icon).into(imgLogo);
            btnOk.setEnabled(false);
            btnOk.setAlpha((float) 0.5);
            editText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }
                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }
                @SuppressLint("Range")
                @Override
                public void afterTextChanged(Editable editable) {
                    if (editable.length() == 6) {
                        btnOk.setEnabled(true);
                        btnOk.setAlpha(100);
                    } else {
                        btnOk.setEnabled(false);
                        btnOk.setAlpha((float) 0.4);
                    }
                }
            });
        } else {
            btnOk.setText(context.getResources().getString(R.string.close));
            btnCancel.setVisibility(View.GONE);
            imgLogoSuccess.setVisibility(View.VISIBLE);
            imgLogo.setVisibility(View.GONE);
            titleTv.setText(context.getResources().getString(R.string.congratulation));
            titleMessage.setText(R.string.your_email_have_updated_successfully);
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setView(dialogView);
        builder.setCancelable(false);

        AlertDialog alertDialog = builder.show();

        btnCancel.setOnClickListener(v -> {
            if (isProfileActionClick) {
                alertDialog.cancel();
            } else {
                requestServiceFirstLogin(context, new UserSessionManagement(context).getUserId(), alertDialog);
            }
        });

        btnOk.setOnClickListener(v -> {
            if (StaticUtilsKey.first_action.equals(action)) {
                alertDialog.dismiss();
                alertDialogSimple(context, StaticUtilsKey.second_action, actionClick, isProfileActionClick);
            } else if (StaticUtilsKey.second_action.equals(action)) {
                    requestCheckUsername( context, actionClick, StaticUtilsKey.verify_code_action ,countryCode, editText.getText().toString().trim(), progressBar, alertDialog, isProfileActionClick);
            } else if (StaticUtilsKey.verify_code_action.equals(action)) {
                if (verify_pin_code.equalsIgnoreCase(editText.getText().toString().trim())) {
                    if (StaticUtilsKey.is_email_address) {
                        requestUpdateEmailPhone(context, countryCode,phone_email,  "email", alertDialog, progressBar, isProfileActionClick);
                    } else {
                        requestUpdateEmailPhone(context, countryCode,phone_email,  "phone", alertDialog, progressBar, isProfileActionClick);
                    }
                } else {
                    Utils.customToastMsgError(context, context.getResources().getString(R.string.invalid_code_verify), false);
                }
            } else {
                alertDialog.dismiss();
                Utils.customToastMsgError(context, context.getResources().getString(R.string.your_email_have_updated_successfully), true);

                new UserSessionManagement(context).logoutUser();
                Intent intent = new Intent(context, LoginActivity.class);
                context.startActivity(intent);
            }

        });
    }
    private void requestUpdateEmailPhone(Context context, String phoneCode, String email_phone, String action,AlertDialog alertDialog,ProgressBar progressBar, boolean isProfileActionClick){
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("type", action);
        if (action.equals("email")) {
            hashMap.put("email", email_phone);
        } else {
            hashMap.put("phone_code", phoneCode);
            hashMap.put("phone_no", email_phone);
        }
        progressBar.setVisibility(View.GONE);
        new ProfileWs().changeEmailAndPhone(context, hashMap, new ProfileWs.CallBackEditFirstLogin() {
            @Override
            public void onSuccess(String msg) {
                progressBar.setVisibility(View.GONE);
                alertDialog.dismiss();
                Utils.customToastMsgError(context, msg, true);
                new UserSessionManagement(context).logoutUser();
                Intent intent = new Intent(context, LoginActivity.class);
                context.startActivity(intent);
            }

            @Override
            public void onFailed(String error) {
                progressBar.setVisibility(View.GONE);
                Utils.customToastMsgError(context, error, false);
            }
        });

    }
    //Request Check Username
    public void requestCheckUsername(Context context, String type, String action, String phoneCode, String email_phone, ProgressBar progressBar, AlertDialog alertDialog, boolean isProfileActionClick){
        String actionType;
        if (type.equals("email")){
            actionType = email_phone;
        } else {
            actionType = phoneCode + " " + email_phone;
        }

        new CheckAccountExitsWs().checkUserNameExist(context, actionType, new CheckAccountExitsWs.OnResponseUserNameExist() {
            @Override
            public void onSuccessFull(@NonNull String message) {
                requestSendVerifyEmailPhone(context, type, action, phoneCode, email_phone, progressBar, alertDialog, isProfileActionClick);
            }
            @Override
            public void onFailed(@NonNull String msg) {
                Utils.customToastMsgError(context, "This phone or email address was taken. Try another.", false);
            }
            @Override
            public void onInternetConnection(@NonNull String mess) {
                Utils.customToastMsgError(context, mess, false);
            }
        });
    }

    private void requestSendVerifyEmailPhone(Context context, String type, String action, String phoneCode, String email_phone, ProgressBar progressBar, AlertDialog alertDialog, boolean isProfileActionClick) {
        HashMap<String, Object> hashMap = new HashMap<>();
        if (type.equals("email")) {
            hashMap.put("username", email_phone);
        } else {
            if (!email_phone.isEmpty()){
                hashMap.put("username", phoneCode + ""+ email_phone);
            }
        }

        progressBar.setVisibility(View.VISIBLE);
        new ProfileWs().sendAlertVerifyPinCodeEmailPhone(context, hashMap, new ProfileWs.CallBackUpdateEmailPhoneListener() {
            @Override
            public void onSuccessVerifyCode(String code_pin) {
                phone_email = email_phone;
                verify_pin_code = code_pin;

                progressBar.setVisibility(View.GONE);
                alertDialog.dismiss();
                if (StaticUtilsKey.verify_code_action.equals(action)) {
                    alertDialogSimple(context, StaticUtilsKey.verify_code_action,"", isProfileActionClick);
                } else {
                    alertDialogSimple(context, "last_step","", isProfileActionClick);
                }
            }
            @Override
            public void onFailed(java.lang.String error) {
                progressBar.setVisibility(View.GONE);
                Utils.customToastMsgError(context, error, false);
            }
        });
    }

    private void requestServiceFirstLogin(Context context, String userId, AlertDialog alertDialog) {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("user_id", userId);
        hashMap.put("first_login_on_mobile_status", "false");
        ProfileWs profileWS = new ProfileWs();
        profileWS.editFirstLoginStatus(context, hashMap, new ProfileWs.CallBackEditFirstLogin() {
            @Override
            public void onSuccess(String msg) {
                UserSessionManagement userSessionManagement = new UserSessionManagement(context);
                User user = new Gson().fromJson(userSessionManagement.getUserDetail(), User.class);
                user.setFirstUserLogin(false);
                userSessionManagement.saveData(user, true);
                alertDialog.dismiss();
            }
            @Override
            public void onFailed(String error) {
                Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
                alertDialog.dismiss();
            }
        });
    }

    //Alert Dialog
    public static void customAlertDialogYesNo(Context context, String subjectTitle, String message, String titleOk, String titleCancel, ClickCallBackBtnListener clickCallBackBtnListener){
        View alertLayout = LayoutInflater.from(context).inflate(R.layout.dialog_msg_yes_no, null);
        androidx.appcompat.app.AlertDialog.Builder dialog = new androidx.appcompat.app.AlertDialog.Builder(context);
        dialog.setView(alertLayout);
        dialog.setCancelable(false);
        androidx.appcompat.app.AlertDialog alertDialog = dialog.show();

        TextView txtTitle = alertLayout.findViewById(R.id.txtTitle);
        txtTitle.setText(subjectTitle);
        TextView txtMessage = alertLayout.findViewById(R.id.txtMessage);
        txtMessage.setText(message);
        ProgressBar progressBarLogOut = alertLayout.findViewById(R.id.progressItem);
        progressBarLogOut.setVisibility(View.GONE);

        if (!CheckIsAppActive.Companion.is_daikou_active()){
            txtMessage.setTextColor(Utils.getColor(context, R.color.gray));
        }

        Button cancel = alertLayout.findViewById(R.id.btnCancel);
        Button ok = alertLayout.findViewById(R.id.btnOk);
        ok.setText(titleOk);
        cancel.setText(titleCancel);
        cancel.setOnClickListener(view1 -> alertDialog.dismiss());
        ok.setOnClickListener(view1 -> clickCallBackBtnListener.onClickBackBtn(alertDialog, progressBarLogOut, ok, cancel));
    }

    //Request List Sign out
    public static void requestLogOut(Activity context, ProgressBar progressBar){
        HashMap<String, String> hashMapBody = new HashMap<>();
        UserSessionManagement userSessionManagement = new UserSessionManagement(context);
        hashMapBody.put("user_id", userSessionManagement.getUserId());
        hashMapBody.put("device_token", Utils.getString(Constant.FIREBASE_TOKEN, context));
        new LoginWs().logout(context,  new LoginWs.OnLogoutListener() {
            @Override
            public void onSuccess(String msg, User data) {
                progressBar.setVisibility(View.GONE);
                userSessionManagement.logoutUser();

                TrackWaterElectricityDatabase database = new TrackWaterElectricityDatabase(context);
                database.deleteAllFloorRecord();
                database.deleteAllFloorRecordFloorDetail();

                Intent intent = new Intent(context, LoginActivity.class);
                context.startActivity(intent);
                context.finish();

                Utils.customToastMsgError(context, msg, true);
            }

            @Override
            public void onSuccessFalse(String status) {
                progressBar.setVisibility(View.GONE);
                Utils.customToastMsgError(context, status, false);
            }

            @Override
            public void onWrong(String msg) {
                progressBar.setVisibility(View.GONE);
                Utils.customToastMsgError(context, msg, false);
            }

            @Override
            public void onFailed(String mess) {
                progressBar.setVisibility(View.GONE);
                Utils.customToastMsgError(context, mess, false);
            }
        });
    }

    //Custom alert dialog delete account
    public static void deleteUserAccount(Context context, String titleHeader, String message, PrettyDialog prettyDialog, Utils.ClickCallBackBtnListener clickCallBackBtnListener){
        prettyDialog
                .setTitle(titleHeader)
                .setMessage(message)
                .setIcon(
                        R.drawable.pdlg_icon_info,
                        R.color.pdlg_color_green,
                        () -> {})
                .addButton(
                        context.getResources().getString(R.string.yes),
                        R.color.white,
                        R.color.red,
                        () -> {
                            clickCallBackBtnListener.onClickBackBtn();
                            prettyDialog.dismiss();
                        }
                )
                .addButton(
                        context.getResources().getString(R.string.cancel),
                        R.color.white,
                        R.color.color_wooden,
                        prettyDialog::dismiss
                )
                .show();
    }

    // Create Construction Dialog
    public static void underConstructionDialog(Context context, String str) {
        android.app.AlertDialog.Builder dialogBuilder = new android.app.AlertDialog.Builder(context, R.style.DialogAlertShape);   // R.style.DialogShape
        android.app.AlertDialog alertDialog = dialogBuilder.create();

        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        linearLayout.addView(createContentView(context, str));
        linearLayout.addView(createHorizontalLine(context));
        linearLayout.addView(confirmBtn(context, alertDialog));

        alertDialog.setView(linearLayout);
        alertDialog.setCancelable(false);
        alertDialog.show();
    }

    // Create Text View
    @SuppressLint("ResourceAsColor")
    private static TextView createContentView(Context context, String str) {
        TextView textView = new TextView(context);
        textView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 250));
        textView.setGravity(Gravity.CENTER);
        textView.setTextSize(18f);
        textView.setTextColor(Color.BLACK);
        textView.setPadding(10, 10, 10, 10);
        textView.setText(str);
        return textView;
    }

    // Create Horizontal View
    private static View createHorizontalLine(Context context) {
        View view = new View(context);
        view.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 1));
        view.setBackgroundResource(R.color.gray);
        return view;
    }

    // Create Button
    @SuppressLint("ResourceAsColor")
    private static Button confirmBtn(Context context, android.app.AlertDialog alertDialog) {
        Button button = new Button(context);
        button.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        button.setText(context.getString(R.string.ok));
        button.setTextColor(Utils.getColor(context, R.color.appBarColor));
        button.setBackgroundResource(R.color.white);
        button.setGravity(Gravity.CENTER);
        button.setOnClickListener(view -> alertDialog.dismiss());
        return button;
    }

    @SuppressLint("SetTextI18n")
    public static void checkUpdatePopUp(Context mActivity) {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(mActivity, R.style.AlertShape);
        View view = LayoutInflater.from(mActivity).inflate(R.layout.layout_updat_version, null);
        builder.setView(view);
        builder.setCancelable(false);
        TextView title = view.findViewById(R.id.titles);
        TextView text = view.findViewById(R.id.text);
        TextView btnUpdateTo = view.findViewById(R.id.btn_update);
        title.setText(mActivity.getString(R.string.app_name) + " " + mActivity.getString(R.string.new_version) );
        text.setText(mActivity.getString(R.string.app_name) + " " + mActivity.getString(R.string.recommends_that_you_update));
        final Dialog dialog = builder.create();
        dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        btnUpdateTo.setOnClickListener(v -> {
            final String appPackageName = mActivity.getPackageName();
            String urlPlay = getGooglePlayStoreUrl(mActivity);
            try {
                mActivity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(urlPlay)));
            } catch (ActivityNotFoundException anfe) {
                mActivity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
            }
        });
        dialog.show();
    }

    private static String getGooglePlayStoreUrl(Context activity){
        String id = activity.getApplicationInfo().packageName; // current google play is   using package name as id
        PackageManager packageManager = activity.getApplicationContext().getPackageManager();
        Uri marketUri = Uri.parse("market://details?id=" + id);
        Intent marketIntent = new Intent(Intent.ACTION_VIEW).setData(marketUri);
        if (marketIntent.resolveActivity(packageManager) != null)
            return "market://details?id=" + id;
        else
            return "https://play.google.com/store/apps/details?id=" + id;
    }

    public static void writeReasonAlert(Context context, String titleHeader, Boolean isEditText, ClickCallBackAlertListener clickCallBackBtnListener){
        View alertLayout = LayoutInflater.from(context).inflate(R.layout.custom_view_layout_alert_write_description, null);
        AlertDialog.Builder dialog = new AlertDialog.Builder(context , R.style.AlertShape);
        dialog.setView(alertLayout);
        dialog.setCancelable(false);
        AlertDialog alertDialog = dialog.show();

        TextView btnCancel = alertLayout.findViewById(R.id.btn_cancel);
        TextView btnOk = alertLayout.findViewById(R.id.btn_ok);
        TextView titleTv = alertLayout.findViewById(R.id.title);
        EditText descriptionEdt = alertLayout.findViewById(R.id.description);
        titleTv.setText(titleHeader);

        btnCancel.setOnClickListener(view1 -> alertDialog.dismiss());
        btnOk.setOnClickListener(view1 -> clickCallBackBtnListener.onClickBackBtn(alertDialog, descriptionEdt));
    }

    public static void alertPhoneCall(Context context, String phoneNumber){
        if (phoneNumber != null){
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:" + phoneNumber));
            context.startActivity(intent);
        } else {
            Utils.logDebug("daikoulogdebug", "No contact");
        }
    }

    private static void alertHotelLoginSignUp(Context context){
        View alertLayout = LayoutInflater.from(context).inflate(R.layout.hotel_popup_to_login_signup, null);
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setView(alertLayout);
        AlertDialog alertDialog = dialog.show();

        alertLayout.findViewById(R.id.loginSignupAlert).setVisibility(View.VISIBLE);
        alertLayout.findViewById(R.id.loginSignupProfile).setVisibility(View.GONE);

        TextView btnSignUp = alertLayout.findViewById(R.id.btnSignup);
        TextView btnLogin = alertLayout.findViewById(R.id.btnLogin);

        btnSignUp.setOnClickListener(new CustomSetOnClickViewListener(view -> {
            context.startActivity(new Intent(context, SignUpBookNowActivity.class));
            alertDialog.dismiss();
        }));

        btnLogin.setOnClickListener(new CustomSetOnClickViewListener(view -> {
            Intent intent = new Intent(context, LoginBookNowActivity.class);
            intent.putExtra("action", "from_hotel_profile");
            context.startActivity(intent);
            alertDialog.dismiss();
        }));
    }

    public static boolean isSuccessLoggedIn(Context context){
        if (!Constant.isLoggedIn(context)){
            alertHotelLoginSignUp(context);
            return false;
        }
        return true;
    }

    // Custom alert dialog ask complaint, front desk, evaluate
    public static void optionRequestDialog(Context context, PrettyDialog prettyDialog, CallBackAlertActionListener clickCallBackBtnListener){
        prettyDialog
                .setTitle("Confirm !")
                .setMessage("Please select your choice !")
                .setIcon(
                        R.drawable.ic_close,
                        R.color.red,
                        prettyDialog::dismiss)
                .addButton(
                        context.getResources().getString(R.string.complaints_solutions),
                        R.color.white,
                        R.color.green,
                        () -> {
                            clickCallBackBtnListener.onClickBackBtn("complaint");
                            prettyDialog.dismiss();
                        }
                )
                .addButton(
                        context.getResources().getString(R.string.front_desk),
                        R.color.white,
                        R.color.blue,
                        () -> {
                            clickCallBackBtnListener.onClickBackBtn("frontdesk");
                            prettyDialog.dismiss();
                        }
                )
                .addButton(
                        context.getResources().getString(R.string.evaluation),
                        R.color.white,
                        R.color.appBarColorOld,
                        () -> {
                            clickCallBackBtnListener.onClickBackBtn("evaluation");
                            prettyDialog.dismiss();
                        }
                )
                .show();
        prettyDialog.setCancelable(false);
    }

    // Call Back Listener
    public interface ClickCallBackBtnListener{
        void onClickBackBtn(AlertDialog alertDialog, ProgressBar progressBar, Button btnOk, Button btnCancel);
    }

    public interface ClickCallBackAlertListener{
        void onClickBackBtn(AlertDialog alertDialog, EditText editText);
    }

    public interface CallBackAlertActionListener{
        void onClickBackBtn(String action);
    }

}
