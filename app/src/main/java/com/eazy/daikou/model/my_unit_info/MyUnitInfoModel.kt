package com.eazy.daikou.model.my_unit_info

import java.io.Serializable

class MyUnitInfoModel : Serializable{
    var id : String? = null
    var unit_no : String? = null
    var unit_type_id : String? = null
    var unit_type : String? = null
    var unit_type_style : String? = null
    var floor_no : String? = null
    var total_bathroom : String? = null
    var total_living_room : String? = null
    var total_maid_room : String? = null
    var unit_direction : String? = null
    var unit_type_total_area : String? = null
    var unit_type_private_area : String? = null
    var total_bedroom : String? = null
    lateinit var unit_type_common_area : String
    var unit_type_layout_photo : String? = null
    var owner_detail : String? = null
    var tenant_detail : String? = null
    var unit_images : List<String> = ArrayList()
}