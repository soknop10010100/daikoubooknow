package com.eazy.daikou.model.hr

import java.io.Serializable

class OvertimeMainModel : Serializable {
    var total_overtime_hour: String? = null
    var list: ArrayList<OvertimeListModel> = ArrayList()
}