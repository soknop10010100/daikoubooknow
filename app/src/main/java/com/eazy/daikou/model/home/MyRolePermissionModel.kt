package com.eazy.daikou.model.home

import java.io.Serializable

class MyRolePermissionModel : Serializable {
    var title : String? = null
    var operation : String?= null
    var description : String? = null
    var is_role : Boolean = false
}