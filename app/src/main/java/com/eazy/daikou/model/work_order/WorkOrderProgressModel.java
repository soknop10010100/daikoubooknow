package com.eazy.daikou.model.work_order;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class WorkOrderProgressModel implements Serializable {

    @SerializedName("work_progress_id")
    @Expose
    private String workProgressId;
    @SerializedName("working_date")
    @Expose
    private String workingDate;
    @SerializedName("from_time")
    @Expose
    private String fromTime;
    @SerializedName("to_time")
    @Expose
    private String toTime;
    @SerializedName("spent_time")
    @Expose
    private String spentTime;
    @SerializedName("done_percent")
    @Expose
    private String donePercent;
    @SerializedName("order_no")
    @Expose
    private String orderNo;

    // Work Order Detail
    @SerializedName("work_order_id")
    @Expose
    private String workOrderId;
    @SerializedName("comment")
    @Expose
    private String comment;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("done_percent_selection")
    @Expose
    private List<String> donePercentSelection = null;
    @SerializedName("work_order_progress_images")
    @Expose
    private List<String> workOrderProgressImages = null;



    public String getWorkProgressId() {
        return workProgressId;
    }

    public void setWorkProgressId(String workProgressId) {
        this.workProgressId = workProgressId;
    }

    public String getWorkingDate() {
        return workingDate;
    }

    public void setWorkingDate(String workingDate) {
        this.workingDate = workingDate;
    }

    public String getFromTime() {
        return fromTime;
    }

    public void setFromTime(String fromTime) {
        this.fromTime = fromTime;
    }

    public String getToTime() {
        return toTime;
    }

    public void setToTime(String toTime) {
        this.toTime = toTime;
    }

    public String getSpentTime() {
        return spentTime;
    }

    public void setSpentTime(String spentTime) {
        this.spentTime = spentTime;
    }

    public String getDonePercent() {
        return donePercent;
    }

    public void setDonePercent(String donePercent) {
        this.donePercent = donePercent;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getWorkOrderId() {
        return workOrderId;
    }

    public void setWorkOrderId(String workOrderId) {
        this.workOrderId = workOrderId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getDonePercentSelection() {
        return donePercentSelection;
    }

    public void setDonePercentSelection(List<String> donePercentSelection) {
        this.donePercentSelection = donePercentSelection;
    }

    public List<String> getWorkOrderProgressImages() {
        return workOrderProgressImages;
    }

    public void setWorkOrderProgressImages(List<String> workOrderProgressImages) {
        this.workOrderProgressImages = workOrderProgressImages;
    }
}
