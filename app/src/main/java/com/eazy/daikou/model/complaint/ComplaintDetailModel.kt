package com.eazy.daikou.model.complaint

import com.eazy.daikou.model.booking_hotel.UserH
import java.io.Serializable

 class DataComplaintDetail: Serializable{
     var id: String? = null
     var name: String? = null
     var description: String? = null
     var created_at: String? = null
     var priority: String? = null
     var status: String? = null
     var status_display: String? = null
     var no: String? = null
     var type: String? = null
     var option: String? = null
     var voice: String? = null
     var unit: Unit? = null
     var account : AccountComplaintModel = AccountComplaintModel()
     var user : UserH? = null
     var images: ArrayList<String> =ArrayList()
 }

class AccountComplaintModel : Serializable{
    var id : String? = null
    var name : String? = null
}

class Unit : Serializable{
    var id : String? = null
    var name : String? = null
}


class ComplaintMainReplyModel : Serializable {
    // Info Thread Object
    var complaintDetail : DataComplaintDetail? = null
    var replies : ArrayList<ComplaintMainReplyModel> = ArrayList()        // Info Main Object Replies

    // Info Sub Object Replies
    var id : String? = null
    var description : String? = null
    var created_at : String? = null
    var image : String? = null
    var user : UserH? = null
}



