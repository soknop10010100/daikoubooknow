package com.eazy.daikou.model.home;

import android.content.Context;

import com.eazy.daikou.R;

import java.util.ArrayList;

public class LanguageModel {

    private String nameLanguage;
    private int iconFlag;
    private boolean isClick;
    private String key;

    public LanguageModel(String nameLanguage, int iconFlag) {
        this.nameLanguage = nameLanguage;
        this.iconFlag = iconFlag;
    }
    public LanguageModel(String key, String nameLanguage, int iconFlag) {
        this.nameLanguage = nameLanguage;
        this.iconFlag = iconFlag;
        this.key = key;
    }

    public String getNameLanguage() {
        return nameLanguage;
    }

    public void setNameLanguage(String nameLanguage) {
        this.nameLanguage = nameLanguage;
    }

    public int getIconFlag() {
        return iconFlag;
    }

    public void setIconFlag(int iconFlag) {
        this.iconFlag = iconFlag;
    }

    public boolean isClick() {
        return isClick;
    }

    public void setClick(boolean click) {
        isClick = click;
    }

    public static ArrayList<LanguageModel> getLanguage(Context context){
        ArrayList<LanguageModel> list = new ArrayList<>();
        list.add(new LanguageModel("en", context.getResources().getString(R.string.english),R.drawable.united_kingdom_flag ));
        list.add(new LanguageModel("kh", context.getResources().getString(R.string.khmer),  R.drawable.cambodia_flag));
        list.add(new LanguageModel("ch", context.getResources().getString(R.string.chinese),R.drawable.china_flag));
        return list;
    }

    public static ArrayList<LanguageModel> getListChangeImage(Context context, int position,String gone){
        ArrayList<LanguageModel> list = new ArrayList<>();
        if(gone.equals("yes")){
            list.add(new LanguageModel(context.getString(R.string.take_photo),  R.drawable.ic_camera_profile));
        } else {
            list.add(new LanguageModel(context.getString(R.string.take_photo),  R.drawable.ic_camera_profile));
            if (position == 1){
                list.add(new LanguageModel(context.getString(R.string.choose_gallery),R.drawable.image_gallery));
            } else {
                list.add(new LanguageModel(context.getString(R.string.choose_gallery),R.drawable.icons8_video_gallery_90));
            }
        }

        return list;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
