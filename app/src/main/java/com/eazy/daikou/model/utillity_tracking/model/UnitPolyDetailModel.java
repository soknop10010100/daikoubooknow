package com.eazy.daikou.model.utillity_tracking.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class UnitPolyDetailModel implements Serializable {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("property_id_fk")
    @Expose
    private String propertyIdFk;
    @SerializedName("property_unit_type_id_fk")
    @Expose
    private String propertyUnitTypeIdFk;
    @SerializedName("property_unit_type_name")
    @Expose
    private String propertyUnitTypeName;
    @SerializedName("floor_no")
    @Expose
    private String floorNo;
    @SerializedName("unit_no")
    @Expose
    private String unitNo;
    @SerializedName("direction")
    @Expose
    private String direction;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("lease_option")
    @Expose
    private String leaseOption;
    @SerializedName("lease_sqm_area_option")
    @Expose
    private String leaseSqmAreaOption;
    @SerializedName("lease_hourly_rate")
    @Expose
    private String leaseHourlyRate;
    @SerializedName("lease_daily_rate")
    @Expose
    private String leaseDailyRate;
    @SerializedName("lease_monthly_rate")
    @Expose
    private String leaseMonthlyRate;
    @SerializedName("lease_annually_rate")
    @Expose
    private String leaseAnnuallyRate;
    @SerializedName("sale_status")
    @Expose
    private String saleStatus;
    @SerializedName("has_owners")
    @Expose
    private String hasOwners;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("reselling")
    @Expose
    private String reselling;
    @SerializedName("resell_price")
    @Expose
    private String resellPrice;
    @SerializedName("rental_status")
    @Expose
    private String rentalStatus;
    @SerializedName("created_dt")
    @Expose
    private String createdDt;
    @SerializedName("total_bedroom")
    @Expose
    private String totalBedroom;
    @SerializedName("total_bathroom")
    @Expose
    private String totalBathroom;
    @SerializedName("total_living_room")
    @Expose
    private String totalLivingRoom;
    @SerializedName("total_maid_room")
    @Expose
    private String totalMaidRoom;
    @SerializedName("total_area_sqm")
    @Expose
    private String totalAreaSqm;
    @SerializedName("total_private_area")
    @Expose
    private String totalPrivateArea;
    @SerializedName("total_common_area")
    @Expose
    private String totalCommonArea;
    @SerializedName("style")
    @Expose
    private String style;
    @SerializedName("file_path")
    @Expose
    private List<String> filePath = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPropertyIdFk() {
        return propertyIdFk;
    }

    public void setPropertyIdFk(String propertyIdFk) {
        this.propertyIdFk = propertyIdFk;
    }

    public String getPropertyUnitTypeIdFk() {
        return propertyUnitTypeIdFk;
    }

    public void setPropertyUnitTypeIdFk(String propertyUnitTypeIdFk) {
        this.propertyUnitTypeIdFk = propertyUnitTypeIdFk;
    }

    public String getPropertyUnitTypeName() {
        return propertyUnitTypeName;
    }

    public void setPropertyUnitTypeName(String propertyUnitTypeName) {
        this.propertyUnitTypeName = propertyUnitTypeName;
    }

    public String getFloorNo() {
        return floorNo;
    }

    public void setFloorNo(String floorNo) {
        this.floorNo = floorNo;
    }

    public String getUnitNo() {
        return unitNo;
    }

    public void setUnitNo(String unitNo) {
        this.unitNo = unitNo;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getLeaseOption() {
        return leaseOption;
    }

    public void setLeaseOption(String leaseOption) {
        this.leaseOption = leaseOption;
    }

    public String getLeaseSqmAreaOption() {
        return leaseSqmAreaOption;
    }

    public void setLeaseSqmAreaOption(String leaseSqmAreaOption) {
        this.leaseSqmAreaOption = leaseSqmAreaOption;
    }

    public String getLeaseHourlyRate() {
        return leaseHourlyRate;
    }

    public void setLeaseHourlyRate(String leaseHourlyRate) {
        this.leaseHourlyRate = leaseHourlyRate;
    }

    public String getLeaseDailyRate() {
        return leaseDailyRate;
    }

    public void setLeaseDailyRate(String leaseDailyRate) {
        this.leaseDailyRate = leaseDailyRate;
    }

    public String getLeaseMonthlyRate() {
        return leaseMonthlyRate;
    }

    public void setLeaseMonthlyRate(String leaseMonthlyRate) {
        this.leaseMonthlyRate = leaseMonthlyRate;
    }

    public String getLeaseAnnuallyRate() {
        return leaseAnnuallyRate;
    }

    public void setLeaseAnnuallyRate(String leaseAnnuallyRate) {
        this.leaseAnnuallyRate = leaseAnnuallyRate;
    }

    public String getSaleStatus() {
        return saleStatus;
    }

    public void setSaleStatus(String saleStatus) {
        this.saleStatus = saleStatus;
    }

    public String getHasOwners() {
        return hasOwners;
    }

    public void setHasOwners(String hasOwners) {
        this.hasOwners = hasOwners;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReselling() {
        return reselling;
    }

    public void setReselling(String reselling) {
        this.reselling = reselling;
    }

    public String getResellPrice() {
        return resellPrice;
    }

    public void setResellPrice(String resellPrice) {
        this.resellPrice = resellPrice;
    }

    public String getRentalStatus() {
        return rentalStatus;
    }

    public void setRentalStatus(String rentalStatus) {
        this.rentalStatus = rentalStatus;
    }

    public String getCreatedDt() {
        return createdDt;
    }

    public void setCreatedDt(String createdDt) {
        this.createdDt = createdDt;
    }

    public String getTotalBedroom() {
        return totalBedroom;
    }

    public void setTotalBedroom(String totalBedroom) {
        this.totalBedroom = totalBedroom;
    }

    public String getTotalBathroom() {
        return totalBathroom;
    }

    public void setTotalBathroom(String totalBathroom) {
        this.totalBathroom = totalBathroom;
    }

    public String getTotalLivingRoom() {
        return totalLivingRoom;
    }

    public void setTotalLivingRoom(String totalLivingRoom) {
        this.totalLivingRoom = totalLivingRoom;
    }

    public String getTotalMaidRoom() {
        return totalMaidRoom;
    }

    public void setTotalMaidRoom(String totalMaidRoom) {
        this.totalMaidRoom = totalMaidRoom;
    }

    public String getTotalAreaSqm() {
        return totalAreaSqm;
    }

    public void setTotalAreaSqm(String totalAreaSqm) {
        this.totalAreaSqm = totalAreaSqm;
    }

    public String getTotalPrivateArea() {
        return totalPrivateArea;
    }

    public void setTotalPrivateArea(String totalPrivateArea) {
        this.totalPrivateArea = totalPrivateArea;
    }

    public String getTotalCommonArea() {
        return totalCommonArea;
    }

    public void setTotalCommonArea(String totalCommonArea) {
        this.totalCommonArea = totalCommonArea;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public List<String> getFilePath() {
        return filePath;
    }

    public void setFilePath(List<String> filePath) {
        this.filePath = filePath;
    }
}
