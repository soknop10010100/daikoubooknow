package com.eazy.daikou.model.buy_sell

import java.io.Serializable

class LandInformation : Serializable{
    var  width = ""
    var  length = ""
    var  area = ""
    var  shape = ""
    var  topography = ""
    var  side_position = ""
    var  description = ""

}
class BuildingInformation : Serializable{
    var  building_type = ""
    var  width = ""
    var  length = ""
    var  floor_number = ""
    var  net_floor_area = ""
    var  physical_condition = ""
    var  number_of_storey = ""
    var  gross_floor_area = ""
    var  view = ""
    var  construction_year = ""
    var  additional_note = ""
    var construction_completion = ""
}
class ImageProperty : Serializable{
    var  image_file = ""
    var  image_side = ""
    var  file_path = ""
    var  property_for_sale_and_rent_id_fk = ""
    var  created_date = ""
}

class ProfileSeller : Serializable{
    var user_id = ""
    var email = ""
    var full_name = ""
    var full_phone = ""
    var profile_image = ""
}