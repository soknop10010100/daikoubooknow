package com.eazy.daikou.model.hr

import java.io.Serializable

class ItemAttendanceModel : Serializable {
    var id: String? = null
    var attendance_date: String? = null
    val user_id: String? = null
    var check_in: String? = null
    var check_out: String? = null
    var status: String? = null
    var checked_by_device: String? = null
    var break_in_check: Any? = null
    var break_out_check: Any? = null
    var early: String? = null
    var late: String? = null
    var created_at: String? = null
    var shift_break_out: String? = null
    var shift_break_in: String? = null
    var attendance_type: String? = null
    var user_name: String? = null
    val id_no: String? = null
    var button_status: String? = null
    var action_type: String? = null
    var total_working_hour: TotalWorkingHour? = null
}

class AttendanceViewAllModel : Serializable{
    val id: String? = null
    val attendance_date: String? = null
    val check_in: String? = null
}

class TotalWorkingHour : Serializable{
    var total_hour: String? = null
    var total_minute: String? = null
}