package com.eazy.daikou.model.utillity_tracking.model;

import java.io.Serializable;

public class UtilNoCommonAreaModel implements Serializable {

    private String id;
    private String name;
    private boolean isAlreadyHaveTrack;
    private LastTrackData lastTrackDataWater;
    private LastTrackData lastTrackDataElectricity;
    private String total_recorded_amount_water;
    private String total_recorded_amount_electric;

    public LastTrackData getLastTrackDataWater() {
        return lastTrackDataWater;
    }

    public void setLastTrackDataWater(LastTrackData lastTrackDataWater) {
        this.lastTrackDataWater = lastTrackDataWater;
    }

    public LastTrackData getLastTrackDataElectricity() {
        return lastTrackDataElectricity;
    }

    public void setLastTrackDataElectricity(LastTrackData lastTrackDataElectricity) {
        this.lastTrackDataElectricity = lastTrackDataElectricity;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name.replaceAll("^\"+|\"+$", "");
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isAlreadyHaveTrack() {
        return isAlreadyHaveTrack;
    }

    public void setAlreadyHaveTrack(boolean alreadyHaveTrack) {
        isAlreadyHaveTrack = alreadyHaveTrack;
    }

    public String getTotal_recorded_amount_electric() {
        return total_recorded_amount_electric;
    }

    public void setTotal_recorded_amount_electric(String total_recorded_amount_electric) {
        this.total_recorded_amount_electric = total_recorded_amount_electric;
    }

    public String getTotal_recorded_amount_water() {
        return total_recorded_amount_water;
    }

    public void setTotal_recorded_amount_water(String total_recorded_amount_water) {
        this.total_recorded_amount_water = total_recorded_amount_water;
    }
}