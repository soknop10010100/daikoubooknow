package com.eazy.daikou.model.floor_plan;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class SpacePolyDetailModel implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("space_name")
    @Expose
    private String spaceName;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("num_people")
    @Expose
    private String numPeople;
    @SerializedName("note")
    @Expose
    private String note;
    @SerializedName("file_path")
    @Expose
    private List<String> filePath = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSpaceName() {
        return spaceName;
    }

    public void setSpaceName(String spaceName) {
        this.spaceName = spaceName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNumPeople() {
        return numPeople;
    }

    public void setNumPeople(String numPeople) {
        this.numPeople = numPeople;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public List<String> getFilePath() {
        return filePath;
    }

    public void setFilePath(List<String> filePath) {
        this.filePath = filePath;
    }

}
