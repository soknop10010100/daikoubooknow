package com.eazy.daikou.model.buy_sell

import java.io.Serializable
import java.util.*
import kotlin.collections.ArrayList

class CreateBuySellModel : Serializable{
    var user_id: String = ""
    var property_type : String = ""
    var property_name : String = ""
    var property_location_name : String = ""
    var property_province : String = ""
    var indication_purpose : String = ""
    var property_coord_lat : String = ""
    var property_coord_long : String = ""
    var title_deed_type : String = ""
    var map_sheet_number : String = ""
    var plot_number: String = ""
    var title_deed_total_area : String = ""
    var title_deed_date_issue : String = ""
    var additional_information : String = ""
    var payment_description : String = ""
    var move_in_condition : String = ""

    var price : String = ""
    var rent_condition : String = ""
    var furniture: ArrayList<String> = ArrayList()
    var property_images : ArrayList<ImageProperty> = ArrayList()
    var title_deed_images : ArrayList<ImageProperty> = ArrayList()
    lateinit  var land_info  : LandInformation
    lateinit  var building_info : BuildingInformation
    var area = ""
    var property_id = ""
    var coord_lat = ""
    var coord_long = ""
    var author_user_id_fk = ""
}