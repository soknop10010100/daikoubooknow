package com.eazy.daikou.model.my_property.service_property_employee

import java.io.Serializable

class ServicePropertyHistoryEmployeeModel : Serializable {
    var work_schedule_id : String? = null
    var account_id : String? = null
    var prop_bill_item_id_fk : String? = null
    var work_schedule_reg_id : String? = null
    var unit_id : String? = null
    var operation_part : String? = null
    var schedule_start_date : String? = null
    var schedule_end_date : String? = null
    var schedule_start_time : String? = null
    var schedule_end_time : String? = null
    var working_date : String? = null
    var schedule_status : String? = null
    var description : String? = null
    var created_date : String? = null
    var sv_name : String? = null
    var sv_term : String? = null
    var unit_no : String? = null
    var floor_no : String? = null
    var zone_name : String? = null

    var space_name: String? = null
    var is_common_area : Boolean = false
}