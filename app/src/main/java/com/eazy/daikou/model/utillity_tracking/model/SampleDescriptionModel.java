package com.eazy.daikou.model.utillity_tracking.model;

import android.content.Context;

import com.eazy.daikou.R;
import com.eazy.daikou.helper.Utils;

import java.util.ArrayList;

public class SampleDescriptionModel {

    private String name;

    public SampleDescriptionModel(String name){
        this.name = name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getName(){
        return  name;
    }

    public static ArrayList<SampleDescriptionModel> getString(Context context){
        ArrayList<SampleDescriptionModel> list = new ArrayList<>();
        list.add(new SampleDescriptionModel(context.getResources().getString(R.string.please_help_me_I_am_stuck_in_elevator)));
        list.add(new SampleDescriptionModel(context.getResources().getString(R.string.please_help_me_my_room_got_problem)));
        list.add(new SampleDescriptionModel(context.getResources().getString(R.string.sos_someone_got_sick)));
        list.add(new SampleDescriptionModel(context.getResources().getString(R.string.please_come_there_is_stranger_in_my_room)));
        list.add(new SampleDescriptionModel(context.getResources().getString(R.string.sos_there_is_fire)));
        Utils.saveArrayList(list, "des_emergency", context);
        return list;
    }

}
