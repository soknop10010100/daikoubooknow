package com.eazy.daikou.model.contact;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ContactAPIModel implements Serializable {
    @SerializedName("eid")
    @Expose
    private String eid;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("e_designation")
    @Expose
    private String eDesignation;
    @SerializedName("e_email")
    @Expose
    private String eEmail;
    @SerializedName("phonecode")
    @Expose
    private String phonecode;
    @SerializedName("e_contact")
    @Expose
    private String eContact;

    public String getEid() {
        return eid;
    }

    public void setEid(String eid) {
        this.eid = eid;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEDesignation() {
        return eDesignation;
    }

    public void setEDesignation(String eDesignation) {
        this.eDesignation = eDesignation;
    }

    public String getEEmail() {
        return eEmail;
    }

    public void setEEmail(String eEmail) {
        this.eEmail = eEmail;
    }

    public String getPhonecode() {
        return phonecode;
    }

    public void setPhonecode(String phonecode) {
        this.phonecode = phonecode;
    }

    public String getEContact() {
        return eContact;
    }

    public void setEContact(String eContact) {
        this.eContact = eContact;
    }

    @Override
    public String toString() {
        return "ContactAPIModel{" +
                "eid='" + eid + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", eDesignation='" + eDesignation + '\'' +
                ", eEmail='" + eEmail + '\'' +
                ", phonecode='" + phonecode + '\'' +
                ", eContact='" + eContact + '\'' +
                '}';
    }
}
