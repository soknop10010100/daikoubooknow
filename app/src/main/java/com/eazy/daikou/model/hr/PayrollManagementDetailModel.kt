package com.eazy.daikou.model.hr

import java.io.Serializable

class PayrollManagementDetailModel : Serializable{

    lateinit var employee_info : BasicEmployeeInfo
    lateinit var salary_detail : SalaryDetail
    lateinit var salary_items : ArrayList<SalaryItem>
    var currency_code : String? = null
}

class SalaryItem : Serializable{
    val id : String? = null
    val item_name : String? = null
    val amount : String? = null
    val status : String? = null
    val currency_code : String? = null
}

class SalaryDetail : Serializable{
    val provident_fund : String = ""
    val gross_salary : String = ""
    val total_deduction : String = ""
    val net_salary : String = ""
}
