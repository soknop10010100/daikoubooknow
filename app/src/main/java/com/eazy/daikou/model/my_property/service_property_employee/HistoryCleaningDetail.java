package com.eazy.daikou.model.my_property.service_property_employee;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class HistoryCleaningDetail implements Serializable {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("cleaning_schedule_id")
    @Expose
    private String cleaningScheduleId;
    @SerializedName("start_date")
    @Expose
    private String startDate;
    @SerializedName("end_date")
    @Expose
    private String endDate;
    @SerializedName("start_time")
    @Expose
    private String startTime;
    @SerializedName("end_time")
    @Expose
    private String endTime;
    @SerializedName("unit_id")
    @Expose
    private String unitId;
    @SerializedName("unit_no")
    @Expose
    private String unitNo;
    @SerializedName("service_type")
    @Expose
    private String serviceType;
    @SerializedName("service_term")
    @Expose
    private String serviceTerm;
    @SerializedName("cleaning_service_name")
    @Expose
    private String cleaningServiceName;
    @SerializedName("cleaner_schedule_status")
    @Expose
    private String cleanerScheduleStatus;
    @SerializedName("order_no")
    @Expose
    private String orderNo;
    @SerializedName("cleaning_schedule_image")
    @Expose
    private CleaningScheduleImage cleaningScheduleImage;
    @SerializedName("cleaner_check_in_out_info")
    @Expose
    private CleanerCheckInOutInfo cleanerCheckInOutInfo;
    @SerializedName("employee_who_assigned_you")
    @Expose
    private EmployeeWhoAssignedYou employeeWhoAssignedYou;

    @SerializedName("work_space_id")
    @Expose
    private Object workSpaceId;
    @SerializedName("work_zone_id")
    @Expose
    private Object workZoneId;
    @SerializedName("work_floor_id")
    @Expose
    private Object workFloorId;
    @SerializedName("cleaning_category")
    @Expose
    private Object workSpaceCategory;

    @SerializedName("work_space_name")
    @Expose
    private Object workSpaceName;
    @SerializedName("work_zone_name")
    @Expose
    private Object workZoneName;
    @SerializedName("work_floor_name")
    @Expose
    private Object workFloorName;

    @SerializedName("employee_assigned_detail")
    @Expose
    private ArrayList<CleaningService.EmployeeAssignedDetail> employeeAssignedDetail;

    public ArrayList<CleaningService.EmployeeAssignedDetail> getEmployeeAssignedDetail() {
        return employeeAssignedDetail;
    }

    public void setEmployeeAssignedDetail(ArrayList<CleaningService.EmployeeAssignedDetail> employeeAssignedDetail) {
        this.employeeAssignedDetail = employeeAssignedDetail;
    }

    public Object getWorkSpaceId() {
        return workSpaceId;
    }

    public void setWorkSpaceId(String workSpaceId) {
        this.workSpaceId = workSpaceId;
    }

    public Object getWorkZoneId() {
        return workZoneId;
    }

    public void setWorkZoneId(String workZoneId) {
        this.workZoneId = workZoneId;
    }

    public Object getWorkFloorId() {
        return workFloorId;
    }

    public void setWorkFloorId(String workFloorId) {
        this.workFloorId = workFloorId;
    }

    public Object getWorkSpaceCategory() {
        return workSpaceCategory;
    }

    public void setWorkSpaceCategory(String workSpaceCategory) {
        this.workSpaceCategory = workSpaceCategory;
    }

    public Object getWorkSpaceName() {
        return workSpaceName;
    }

    public void setWorkSpaceName(String workSpaceName) {
        this.workSpaceName = workSpaceName;
    }

    public Object getWorkZoneName() {
        return workZoneName;
    }

    public void setWorkZoneName(String workZoneName) {
        this.workZoneName = workZoneName;
    }

    public Object getWorkFloorName() {
        return workFloorName;
    }

    public void setWorkFloorName(String workFloorName) {
        this.workFloorName = workFloorName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCleaningScheduleId() {
        return cleaningScheduleId;
    }

    public void setCleaningScheduleId(String cleaningScheduleId) {
        this.cleaningScheduleId = cleaningScheduleId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getUnitNo() {
        return unitNo;
    }

    public void setUnitNo(String unitNo) {
        this.unitNo = unitNo;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getServiceTerm() {
        return serviceTerm;
    }

    public void setServiceTerm(String serviceTerm) {
        this.serviceTerm = serviceTerm;
    }

    public String getCleaningServiceName() {
        return cleaningServiceName;
    }

    public void setCleaningServiceName(String cleaningServiceName) {
        this.cleaningServiceName = cleaningServiceName;
    }

    public String getCleanerScheduleStatus() {
        return cleanerScheduleStatus;
    }

    public void setCleanerScheduleStatus(String cleanerScheduleStatus) {
        this.cleanerScheduleStatus = cleanerScheduleStatus;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public CleaningScheduleImage getCleaningScheduleImage() {
        return cleaningScheduleImage;
    }

    public void setCleaningScheduleImage(CleaningScheduleImage cleaningScheduleImage) {
        this.cleaningScheduleImage = cleaningScheduleImage;
    }

    public CleanerCheckInOutInfo getCleanerCheckInOutInfo() {
        return cleanerCheckInOutInfo;
    }

    public void setCleanerCheckInOutInfo(CleanerCheckInOutInfo cleanerCheckInOutInfo) {
        this.cleanerCheckInOutInfo = cleanerCheckInOutInfo;
    }

    public EmployeeWhoAssignedYou getEmployeeWhoAssignedYou() {
        return employeeWhoAssignedYou;
    }

    public void setEmployeeWhoAssignedYou(EmployeeWhoAssignedYou employeeWhoAssignedYou) {
        this.employeeWhoAssignedYou = employeeWhoAssignedYou;
    }

    public static class CleanerCheckInOutInfo implements Serializable {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("check_in_date")
        @Expose
        private String checkInDate;
        @SerializedName("check_out_date")
        @Expose
        private Object checkOutDate;
        @SerializedName("check_in_late_reason")
        @Expose
        private Object checkInLateReason;
        @SerializedName("check_out_late_reason")
        @Expose
        private Object checkOutLateReason;
        @SerializedName("check_out_early_reason")
        @Expose
        private Object checkOutEarlyReason;
        @SerializedName("check_in_status")
        @Expose
        private String checkInStatus;
        @SerializedName("check_out_status")
        @Expose
        private Object checkOutStatus;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCheckInDate() {
            return checkInDate;
        }

        public void setCheckInDate(String checkInDate) {
            this.checkInDate = checkInDate;
        }

        public Object getCheckOutDate() {
            return checkOutDate;
        }

        public void setCheckOutDate(Object checkOutDate) {
            this.checkOutDate = checkOutDate;
        }

        public Object getCheckInLateReason() {
            return checkInLateReason;
        }

        public void setCheckInLateReason(Object checkInLateReason) {
            this.checkInLateReason = checkInLateReason;
        }

        public Object getCheckOutLateReason() {
            return checkOutLateReason;
        }

        public void setCheckOutLateReason(Object checkOutLateReason) {
            this.checkOutLateReason = checkOutLateReason;
        }

        public Object getCheckOutEarlyReason() {
            return checkOutEarlyReason;
        }

        public void setCheckOutEarlyReason(Object checkOutEarlyReason) {
            this.checkOutEarlyReason = checkOutEarlyReason;
        }

        public String getCheckInStatus() {
            return checkInStatus;
        }

        public void setCheckInStatus(String checkInStatus) {
            this.checkInStatus = checkInStatus;
        }

        public Object getCheckOutStatus() {
            return checkOutStatus;
        }

        public void setCheckOutStatus(Object checkOutStatus) {
            this.checkOutStatus = checkOutStatus;
        }

    }
    public static class CleaningScheduleImage implements Serializable{

        @SerializedName("before_clean_image")
        @Expose
        private ArrayList<String> beforeCleanImage = null;
        @SerializedName("after_clean_image")
        @Expose
        private ArrayList<String> afterCleanImage = null;

        public ArrayList<String> getBeforeCleanImage() {
            return beforeCleanImage;
        }

        public void setBeforeCleanImage(ArrayList<String> beforeCleanImage) {
            this.beforeCleanImage = beforeCleanImage;
        }

        public ArrayList<String> getAfterCleanImage() {
            return afterCleanImage;
        }

        public void setAfterCleanImage(ArrayList<String> afterCleanImage) {
            this.afterCleanImage = afterCleanImage;
        }

    }

    public static class EmployeeWhoAssignedYou implements Serializable {

        @SerializedName("user_id")
        @Expose
        private String userId;
        @SerializedName("employee_id")
        @Expose
        private String employeeId;
        @SerializedName("employee_full_name")
        @Expose
        private String employeeFullName;
        @SerializedName("employee_phone_number")
        @Expose
        private String employeePhoneNumber;
        @SerializedName("employee_profile_photo")
        @Expose
        private String employeeProfilePhoto;

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getEmployeeId() {
            return employeeId;
        }

        public void setEmployeeId(String employeeId) {
            this.employeeId = employeeId;
        }

        public String getEmployeeFullName() {
            return employeeFullName;
        }

        public void setEmployeeFullName(String employeeFullName) {
            this.employeeFullName = employeeFullName;
        }

        public String getEmployeePhoneNumber() {
            return employeePhoneNumber;
        }

        public void setEmployeePhoneNumber(String employeePhoneNumber) {
            this.employeePhoneNumber = employeePhoneNumber;
        }

        public String getEmployeeProfilePhoto() {
            return employeeProfilePhoto;
        }

        public void setEmployeeProfilePhoto(String employeeProfilePhoto) {
            this.employeeProfilePhoto = employeeProfilePhoto;
        }

    }

    public static class EmployeeAssignedDetail implements Serializable{

        @SerializedName("schedule_id")
        @Expose
        private String scheduleId;
        @SerializedName("cleaning_date")
        @Expose
        private String cleaningDate;
        @SerializedName("user_id")
        @Expose
        private String userId;
        @SerializedName("employee_id")
        @Expose
        private String employeeId;
        @SerializedName("photo")
        @Expose
        private String photo;
        @SerializedName("employee_full_name")
        @Expose
        private String employeeFullName;
        @SerializedName("employee_phone_number")
        @Expose
        private String employeePhoneNumber;
        @SerializedName("profile_photo")
        @Expose
        private String profilePhoto;

        public String getScheduleId() {
            return scheduleId;
        }

        public void setScheduleId(String scheduleId) {
            this.scheduleId = scheduleId;
        }

        public String getCleaningDate() {
            return cleaningDate;
        }

        public void setCleaningDate(String cleaningDate) {
            this.cleaningDate = cleaningDate;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getEmployeeId() {
            return employeeId;
        }

        public void setEmployeeId(String employeeId) {
            this.employeeId = employeeId;
        }

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }

        public String getEmployeeFullName() {
            return employeeFullName;
        }

        public void setEmployeeFullName(String employeeFullName) {
            this.employeeFullName = employeeFullName;
        }

        public String getEmployeePhoneNumber() {
            return employeePhoneNumber;
        }

        public void setEmployeePhoneNumber(String employeePhoneNumber) {
            this.employeePhoneNumber = employeePhoneNumber;
        }

        public String getProfilePhoto() {
            return profilePhoto;
        }

        public void setProfilePhoto(String profilePhoto) {
            this.profilePhoto = profilePhoto;
        }

    }
}
