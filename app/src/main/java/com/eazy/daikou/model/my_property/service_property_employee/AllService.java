package com.eazy.daikou.model.my_property.service_property_employee;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AllService  implements Serializable {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("bill_item_id")
    @Expose
    private String billItemId;
    @SerializedName("cleaning_service_name")
    @Expose
    private String cleaningServiceName;
    @SerializedName("cleaning_service_type")
    @Expose
    private String cleaningServiceType;
    @SerializedName("property_id")
    @Expose
    private String propertyId;
    @SerializedName("cleaning_service_fee_id")
    @Expose
    private String cleaningServiceFeeId;
    @SerializedName("service_code")
    @Expose
    private String serviceCode;
    @SerializedName("service_term")
    @Expose
    private String serviceTerm;
    @SerializedName("service_fee")
    @Expose
    private String serviceFee;
    @SerializedName("service_tax")
    @Expose
    private String serviceTax;
    @SerializedName("start_time")
    @Expose
    private String startTime;
    @SerializedName("end_time")
    @Expose
    private String endTime;
    @SerializedName("total_cleaner")
    @Expose
    private String totalCleaner;
    @SerializedName("cleaning_period")
    @Expose
    private int cleaningPeriod;
    @SerializedName("mon")
    @Expose
    private boolean mon;
    @SerializedName("tue")
    @Expose
    private boolean tue;
    @SerializedName("wed")
    @Expose
    private boolean wed;
    @SerializedName("thu")
    @Expose
    private boolean thu;
    @SerializedName("fri")
    @Expose
    private boolean fri;
    @SerializedName("sat")
    @Expose
    private boolean sat;
    @SerializedName("sun")
    @Expose
    private boolean sun;
    @SerializedName("available_day")
    @Expose
    private List<String> availableDay = null;
    @SerializedName("start_time_in_minute")
    @Expose
    private int startTimeInMinute;
    @SerializedName("end_time_in_minute")
    @Expose
    private int endTimeInMinute;
    @SerializedName("cleaning_service_image")
    @Expose
    private ArrayList<String> cleaningServiceImage = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBillItemId() {
        return billItemId;
    }

    public void setBillItemId(String billItemId) {
        this.billItemId = billItemId;
    }

    public String getCleaningServiceName() {
        return cleaningServiceName;
    }

    public void setCleaningServiceName(String cleaningServiceName) {
        this.cleaningServiceName = cleaningServiceName;
    }

    public String getCleaningServiceType() {
        return cleaningServiceType;
    }

    public void setCleaningServiceType(String cleaningServiceType) {
        this.cleaningServiceType = cleaningServiceType;
    }

    public String getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(String propertyId) {
        this.propertyId = propertyId;
    }

    public String getCleaningServiceFeeId() {
        return cleaningServiceFeeId;
    }

    public void setCleaningServiceFeeId(String cleaningServiceFeeId) {
        this.cleaningServiceFeeId = cleaningServiceFeeId;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getServiceTerm() {
        return serviceTerm;
    }

    public void setServiceTerm(String serviceTerm) {
        this.serviceTerm = serviceTerm;
    }

    public String getServiceFee() {
        return serviceFee;
    }

    public void setServiceFee(String serviceFee) {
        this.serviceFee = serviceFee;
    }

    public String getServiceTax() {
        return serviceTax;
    }

    public void setServiceTax(String serviceTax) {
        this.serviceTax = serviceTax;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getTotalCleaner() {
        return totalCleaner;
    }

    public void setTotalCleaner(String totalCleaner) {
        this.totalCleaner = totalCleaner;
    }

    public int getCleaningPeriod() {
        return cleaningPeriod;
    }

    public void setCleaningPeriod(int cleaningPeriod) {
        this.cleaningPeriod = cleaningPeriod;
    }

    public boolean isMon() {
        return mon;
    }

    public void setMon(boolean mon) {
        this.mon = mon;
    }

    public boolean isTue() {
        return tue;
    }

    public void setTue(boolean tue) {
        this.tue = tue;
    }

    public boolean isWed() {
        return wed;
    }

    public void setWed(boolean wed) {
        this.wed = wed;
    }

    public boolean isThu() {
        return thu;
    }

    public void setThu(boolean thu) {
        this.thu = thu;
    }

    public boolean isFri() {
        return fri;
    }

    public void setFri(boolean fri) {
        this.fri = fri;
    }

    public boolean isSat() {
        return sat;
    }

    public void setSat(boolean sat) {
        this.sat = sat;
    }

    public boolean isSun() {
        return sun;
    }

    public void setSun(boolean sun) {
        this.sun = sun;
    }

    public List<String> getAvailableDay() {
        return availableDay;
    }

    public void setAvailableDay(List<String> availableDay) {
        this.availableDay = availableDay;
    }

    public int getStartTimeInMinute() {
        return startTimeInMinute;
    }

    public void setStartTimeInMinute(int startTimeInMinute) {
        this.startTimeInMinute = startTimeInMinute;
    }

    public int getEndTimeInMinute() {
        return endTimeInMinute;
    }

    public void setEndTimeInMinute(int endTimeInMinute) {
        this.endTimeInMinute = endTimeInMinute;
    }

    public ArrayList<String> getCleaningServiceImage() {
        return cleaningServiceImage;
    }

    public void setCleaningServiceImage(ArrayList<String> cleaningServiceImage) {
        this.cleaningServiceImage = cleaningServiceImage;
    }

}
