package com.eazy.daikou.model.my_property.cleaning

import java.io.Serializable

class PropertyServiceProviderDetailModel : Serializable {
    var id: String? = null
    var reg_no: String? = null
    var unit_no: String? = null
    var reg_start_date: String? = null
    var reg_end_date: String? = null
    var reg_start_time: String? = null
    var reg_end_time: String? = null
    var sv_name: String? = null
    var sv_term: String? = null
    var sv_period: String? = null
    var category_name: String? = null
    var reg_price: String? = null
    var reg_qty: String? = null
    var reg_tax_amount: String? = null
    var reg_total_amount: String? = null
    var subtotal: String? = null
    var reg_status: String? = null
    var ref_no: String? = null

    var available_days: ArrayList<String> = ArrayList()
    var reg_history_logs: ArrayList<String> = ArrayList()

}