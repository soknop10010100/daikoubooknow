package com.eazy.daikou.model.my_property.service_provider;

import com.eazy.daikou.model.law.LawsDetailModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class LatLngServiceProvider implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("contractor_name")
    @Expose
    private String contractorName;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("website")
    @Expose
    private String website;
    @SerializedName("company_contact")
    @Expose
    private String companyContact;
    @SerializedName("deleted_dt")
    @Expose
    private Object deletedDt;
    @SerializedName("company_logo")
    @Expose
    private String companyLogo;
    @SerializedName("coord_lat")
    @Expose
    private String  coordLat;
    @SerializedName("coord_long")
    @Expose
    private String coordLong;
    @SerializedName("main_category_id")
    @Expose
    private String main_category_id;
    @SerializedName("category")
    @Expose
    private LawsDetailModel.Category category;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContractorName() {
        return contractorName;
    }

    public void setContractorName(String contractorName) {
        this.contractorName = contractorName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getCompanyContact() {
        return companyContact;
    }

    public void setCompanyContact(String companyContact) {
        this.companyContact = companyContact;
    }

    public Object getDeletedDt() {
        return deletedDt;
    }

    public void setDeletedDt(Object deletedDt) {
        this.deletedDt = deletedDt;
    }

    public String getCompanyLogo() {
        return companyLogo;
    }

    public void setCompanyLogo(String companyLogo) {
        this.companyLogo = companyLogo;
    }

    public String getCoordLat() {
        return coordLat;
    }

    public void setCoordLat(String coordLat) {
        this.coordLat = coordLat;
    }

    public String getCoordLong() {
        return coordLong;
    }

    public void setCoordLong(String coordLong) {
        this.coordLong = coordLong;
    }

    public LawsDetailModel.Category getCategory() {
        return category;
    }

    public void setCategory(LawsDetailModel.Category category) {
        this.category = category;
    }

    public String getMain_category_id() {
        return main_category_id;
    }

    public void setMain_category_id(String main_category_id) {
        this.main_category_id = main_category_id;
    }
}
