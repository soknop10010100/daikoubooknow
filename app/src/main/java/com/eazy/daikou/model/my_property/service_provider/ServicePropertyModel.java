package com.eazy.daikou.model.my_property.service_provider;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ServicePropertyModel implements Serializable {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("contractor_id_fk")
    @Expose
    private String contractorIdFk;
    @SerializedName("property_id_fk")
    @Expose
    private String propertyIdFk;
    @SerializedName("service_start_dt")
    @Expose
    private String serviceStartDt;
    @SerializedName("service_end_dt")
    @Expose
    private String serviceEndDt;
    @SerializedName("service_charge_type")
    @Expose
    private String serviceChargeType;
    @SerializedName("total_service_charge")
    @Expose
    private Object totalServiceCharge;
    @SerializedName("contractor_name")
    @Expose
    private String contractorName;
    @SerializedName("business_type_name")
    @Expose
    private String businessTypeName;
    @SerializedName("company_contact")
    @Expose
    private String companyContact;
    @SerializedName("company_contact_phonecode")
    @Expose
    private String companyContactPhonecode;
    @SerializedName("website")
    @Expose
    private String website;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("category_name")
    @Expose
    private String categoryName;
    @SerializedName("product_desc")
    @Expose
    private String productDesc;
    @SerializedName("scope_of_work")
    @Expose
    private List<String> scopeOfWork = null;
    @SerializedName("company_logo")
    @Expose
    private String companyLogo;
    @SerializedName("company_stamp")
    @Expose
    private String companyStamp;
    @SerializedName("contract_status")
    @Expose
    private String contractStatus;
    @SerializedName("file_path")
    @Expose
    private ArrayList<String> filePath = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContractorIdFk() {
        return contractorIdFk;
    }

    public void setContractorIdFk(String contractorIdFk) {
        this.contractorIdFk = contractorIdFk;
    }

    public String getPropertyIdFk() {
        return propertyIdFk;
    }

    public void setPropertyIdFk(String propertyIdFk) {
        this.propertyIdFk = propertyIdFk;
    }

    public String getServiceStartDt() {
        return serviceStartDt;
    }

    public void setServiceStartDt(String serviceStartDt) {
        this.serviceStartDt = serviceStartDt;
    }

    public String getServiceEndDt() {
        return serviceEndDt;
    }

    public void setServiceEndDt(String serviceEndDt) {
        this.serviceEndDt = serviceEndDt;
    }

    public String getServiceChargeType() {
        return serviceChargeType;
    }

    public void setServiceChargeType(String serviceChargeType) {
        this.serviceChargeType = serviceChargeType;
    }

    public Object getTotalServiceCharge() {
        return totalServiceCharge;
    }

    public void setTotalServiceCharge(Object totalServiceCharge) {
        this.totalServiceCharge = totalServiceCharge;
    }

    public String getContractorName() {
        return contractorName;
    }

    public void setContractorName(String contractorName) {
        this.contractorName = contractorName;
    }

    public String getBusinessTypeName() {
        return businessTypeName;
    }

    public void setBusinessTypeName(String businessTypeName) {
        this.businessTypeName = businessTypeName;
    }

    public String getCompanyContact() {
        return companyContact;
    }

    public void setCompanyContact(String companyContact) {
        this.companyContact = companyContact;
    }

    public String getCompanyContactPhonecode() {
        return companyContactPhonecode;
    }

    public void setCompanyContactPhonecode(String companyContactPhonecode) {
        this.companyContactPhonecode = companyContactPhonecode;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getProductDesc() {
        return productDesc;
    }

    public void setProductDesc(String productDesc) {
        this.productDesc = productDesc;
    }

    public List<String> getScopeOfWork() {
        return scopeOfWork;
    }

    public void setScopeOfWork(List<String> scopeOfWork) {
        this.scopeOfWork = scopeOfWork;
    }

    public String getCompanyLogo() {
        return companyLogo;
    }

    public void setCompanyLogo(String companyLogo) {
        this.companyLogo = companyLogo;
    }

    public String getCompanyStamp() {
        return companyStamp;
    }

    public void setCompanyStamp(String companyStamp) {
        this.companyStamp = companyStamp;
    }

    public String getContractStatus() {
        return contractStatus;
    }

    public void setContractStatus(String contractStatus) {
        this.contractStatus = contractStatus;
    }

    public ArrayList<String> getFilePath() {
        return filePath;
    }

    public void setFilePath(ArrayList<String> filePath) {
        this.filePath = filePath;
    }


    @SerializedName("contractor_id")
    @Expose
    private String contractorId;
    @SerializedName("service_name")
    @Expose
    private String serviceName;
    @SerializedName("service_address")
    @Expose
    private String serviceAddress;
    @SerializedName("service_website")
    @Expose
    private String serviceWebsite;
    @SerializedName("service_email")
    @Expose
    private Object serviceEmail;
    @SerializedName("service_facebook")
    @Expose
    private Object serviceFacebook;
    @SerializedName("service_we_chat")
    @Expose
    private Object serviceWeChat;
    @SerializedName("service_telegram")
    @Expose
    private Object serviceTelegram;
    @SerializedName("service_whatsapp")
    @Expose
    private Object serviceWhatsapp;
    @SerializedName("service_desc")
    @Expose
    private String serviceDesc;
    @SerializedName("service_logo")
    @Expose
    private String serviceLogo;
    @SerializedName("service_phone_number")
    @Expose
    private String servicePhoneNumber;

    public String getContractorId() {
        return contractorId;
    }

    public void setContractorId(String contractorId) {
        this.contractorId = contractorId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getServiceAddress() {
        return serviceAddress;
    }

    public void setServiceAddress(String serviceAddress) {
        this.serviceAddress = serviceAddress;
    }

    public String getServiceWebsite() {
        return serviceWebsite;
    }

    public void setServiceWebsite(String serviceWebsite) {
        this.serviceWebsite = serviceWebsite;
    }

    public Object getServiceEmail() {
        return serviceEmail;
    }

    public void setServiceEmail(Object serviceEmail) {
        this.serviceEmail = serviceEmail;
    }

    public Object getServiceFacebook() {
        return serviceFacebook;
    }

    public void setServiceFacebook(Object serviceFacebook) {
        this.serviceFacebook = serviceFacebook;
    }

    public Object getServiceWeChat() {
        return serviceWeChat;
    }

    public void setServiceWeChat(Object serviceWeChat) {
        this.serviceWeChat = serviceWeChat;
    }

    public Object getServiceTelegram() {
        return serviceTelegram;
    }

    public void setServiceTelegram(Object serviceTelegram) {
        this.serviceTelegram = serviceTelegram;
    }

    public Object getServiceWhatsapp() {
        return serviceWhatsapp;
    }

    public void setServiceWhatsapp(Object serviceWhatsapp) {
        this.serviceWhatsapp = serviceWhatsapp;
    }

    public String getServiceDesc() {
        return serviceDesc;
    }

    public void setServiceDesc(String serviceDesc) {
        this.serviceDesc = serviceDesc;
    }

    public String getServiceLogo() {
        return serviceLogo;
    }

    public void setServiceLogo(String serviceLogo) {
        this.serviceLogo = serviceLogo;
    }

    public String getServicePhoneNumber() {
        return servicePhoneNumber;
    }

    public void setServicePhoneNumber(String servicePhoneNumber) {
        this.servicePhoneNumber = servicePhoneNumber;
    }
}
