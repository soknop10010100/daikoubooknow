package com.eazy.daikou.model.hr

import java.io.Serializable

class ItemOvertimeModel : Serializable {
    var user_business_key: String? = null
    var user_name: String? = null

    var id: String? = null
    var name: String? = null
}