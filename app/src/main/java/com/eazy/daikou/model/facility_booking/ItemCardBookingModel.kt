package com.eazy.daikou.model.facility_booking

import java.io.Serializable

class ItemCardBookingModel : Serializable{

    var property_id : String? = null
    var property_name : String? = null
    var property_logo : String? = null
    var cart_items : ArrayList<CardItemByProperty> = ArrayList()
    var isClick : Boolean = false
}

class CardItemByProperty : Serializable{
    var cart_item_id : String? = null
    var booking_id : String? = null
    var property_id : String? = null
    var property_name : String? = null
    var space_id : String? = null
    var space_name : String? = null
    var booking_date : String? = null

    var from_time : String? = null
    var to_time : String? = null
    var created_date : String? = null
    var total_amount : String? = null
    var quantity : String? = null
    var tax_vat_percent : String? = null
    var tax_lighting_percent : String? = null
    var price_per_one_quantity : String? = null
    var tax_specific_percent: String? = null
    var space_image : String? = null

    // create to use no server key
    var isClick : Boolean = false
    var taxTotal : String? = null
}