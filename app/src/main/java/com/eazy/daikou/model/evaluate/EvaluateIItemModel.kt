package com.eazy.daikou.model.evaluate

import com.eazy.daikou.model.CustomImageModel
import com.eazy.daikou.model.booking_hotel.UserH
import com.eazy.daikou.model.complaint.AccountUnitModel
import com.eazy.daikou.model.complaint.Comment
import java.io.Serializable

data class EvaluateIItemModel(
    var id: String?,
    var satisfaction: String?,
    var comment: String?,
    var user: UserModel?,
    var created_dt: String?,
    var images: ArrayList<String> = ArrayList(),

    var total_readers: String?,
    var total_comments: String?,
    var object_type: String?,
    var subject: String?,
    var description: String?,

    var unit: AccountUnitModel?,
    var account: AccountUnitModel,
    var files: ArrayList<CustomImageModel> = ArrayList(),
    var uploader_user: UserH?,
    var comments: ArrayList<Comment> = ArrayList(),
    var created_at: String?
) : Serializable
