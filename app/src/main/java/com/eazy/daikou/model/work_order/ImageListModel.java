package com.eazy.daikou.model.work_order;

import android.graphics.Bitmap;

public class ImageListModel {
    private String keyImage;
    private Bitmap bitmapImage;

    private String valImage;
    private boolean isClick;

    private String actionPart;

    public ImageListModel(){}

    // Constructor image param
    public ImageListModel(String keyImage, String valImage) {
        this.keyImage = keyImage;
        this.valImage = valImage;
    }

    // Constructor bitmap param
    public ImageListModel(String keyImage,Bitmap bitmapImage) {
        this.keyImage = keyImage;
        this.bitmapImage = bitmapImage;
    }

    public String getKeyImage() {
        return keyImage;
    }

    public void setKeyImage(String keyImage) {
        this.keyImage = keyImage;
    }

    public Bitmap getBitmapImage() {
        return bitmapImage;
    }

    public void setBitmapImage(Bitmap bitmapImage) {
        this.bitmapImage = bitmapImage;
    }

    public String getValImage() {
        return valImage;
    }

    public void setValImage(String valImage) {
        this.valImage = valImage;
    }

    public boolean isClick() {
        return isClick;
    }

    public void setClick(boolean click) {
        isClick = click;
    }

    public String getActionPart() {
        return actionPart;
    }

    public void setActionPart(String actionPart) {
        this.actionPart = actionPart;
    }
}
