package com.eazy.daikou.model.hr

import java.io.Serializable

class ResignationDetailModel : Serializable{

    var id : String? = null
    var name : String? = null
    var contact : String? = null
    var designation : String? = null
    var last_date : String? = null
    var date_start_worked : String? = null
    var approved_last_date : String? = null
    var confirmed_dt : String? = null
    var type : String? = null
    var reason : String? = null
    var approved_by : String? = null
    var user_business_key : String? = null
    var status : String? = null
    var remark : String? = null
    var created_at : String? = null
    var approved_by_name : String? = null
    var approver_name : String? = null

}