package com.eazy.daikou.model.my_property.payment_schedule_my_unit

import java.io.Serializable

class PaymentSchedulesModel : Serializable {
    var unit_id: String? = null
    var unit_no: String? = null
    var sale_purchase_id: String? = null
    var purchase_date: String? = null
    var sale_price: String? = null
    var user_id: String? = null
    var user_name: String? = null
    var payment_schedule : ArrayList<SubPaymentStatusModel> = ArrayList()
    var isUnitNoClick : Boolean = false
    var generate_payment : GeneratePayment? = null
    var spa_agreement : SpaAgreement? = null
    var lease_agreement : SpaAgreement? = null
    var owner_info : OwnerInfo? = null
    var developer_info : ArrayList<DeveloperInfo> = ArrayList()
    var company_info : CompanyInfo? = null
    var tenant_info : OwnerInfo? = null
    var unit_info : UnitInfo? = null
    var file_agreement : ArrayList<String> = ArrayList()
}

class SubPaymentStatusModel : Serializable{
    var id: String? = null
    var sale_puchase_id: String? = null
    var number: String? = null
    var date_pay: String? = null
    var type_name: String? = null
    var percentage: String? = null
    var amount: String? = null
    var sale_order_item_id: String? = null
    var status: String? = null
    var interest : String? = null
    var create_dt : String? = null
    var payment_term_id: String? = null
    var payment_term_type: String? = null
    var term_name : String? = null
    var payment_option : String? = null
    var payment_status : String? = null
    var url : String? = null
    var kess_payment_url : String? = null
    var invoice_url : String? = null
}

class UnitInfo : Serializable{
    var unit_id: String? = null
    var unit_no: String? = null
    var unit_type_id: String? = null
    var unit_type_name: String? = null
    var total_bedroom: String? = null
    var total_bathroom: String? = null
    var total_living_room: String? = null
    var total_maid_room: String? = null
    var total_area: String? = null
    var total_private_area : String? = null
    var total_common_area : String? = null
    var direction: String? = null
}

class CompanyInfo : Serializable{
    var company_id: String? = null
    var company_name: String? = null
    var company_address: String? = null
    var company_city: String? = null
}

class DeveloperInfo : Serializable{
    var name: String? = null
    var email : String? = null
    var phone: String? = null
    var photo :String? = null
    var country_name : String? = null
    var contact_type : String? = null
}

class OwnerInfo : Serializable{
    var user_id: String? = null
    var date_of_birth: String? = null
    var gender: String? = null
    var email: String? = null
    var pre_address: String? = null
    var user_name: String? = null
    var nationality: String? = null
    var local_id_or_passport_no: String? = null
    var phone_number: String? = null
}

class SpaAgreement : Serializable{
    //SPA agreement
    var sale_purchase_id: String? = null
    var purchase_date: String? = null
    var original_price: String? = null
    var discount_price: String? = null
    var promotion_term: String? = null
    var bank_or_developer: String? = null
    var selling_price: String? = null
    var spa_id: String? = null
    var description: String? = null
    var payment_term_id: String? = null
    var payment_method: String? = null

    //Lease agreement
    var lease_purchase_id : String? = null
    var start_date : String? = null
    var end_date : String? = null
    var leasing_fee : String? = null
    var leasing_fee_type : String? = null
    var original_rate : String? = null
    var duration_year : String? = null
    var duration_month : String? = null
    var duration_day : String? = null
    var duration_hour : String? = null
    var original_fee : String? = null
    var lease_deposit : String? = null
    var discount_fee : String? = null
    var lease_fee : String? = null
}

class GeneratePayment : Serializable{
    var term_name: String? = null
    var payment_option: String? = null
}