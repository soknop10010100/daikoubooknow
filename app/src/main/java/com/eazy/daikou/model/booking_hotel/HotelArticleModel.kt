package com.eazy.daikou.model.booking_hotel

import java.io.Serializable

class HotelArticleModel : Serializable{
    var id : String? = null
    var title : String? = null
    var content : String? = null
    var category_name : String? = null
    var created_at : String? = null
    var image : String? = null
    var url : String? = null
    var view : String? = null

    //Detail Hotel
    var latest_news : ArrayList<HotelArticleModel> = ArrayList()
    var created_by : CreatedByUser? = null
}

class CreatedByUser : Serializable{
    var name : String? = null
    var image : String? = null
}