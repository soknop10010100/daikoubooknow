package com.eazy.daikou.model.facility_booking.my_booking

import java.io.Serializable

class MyBookingItemsModel : Serializable{
    var booking_item_id : String? = null
    var booking_id : String? = null
    var property_name : String? = null
    var property_id : String? = null
    var space_id : String? = null
    var space_name : String? = null
    var booking_date : String? = null
    var from_time : String? = null
    var created_at : String? = null
    var to_time : String? = null
    var space_image : String? = null
    var pricing : String? = null
    var total_amount : String? = null

    var type_price : String? = null
    var quantity : String? = null
    var tax_vat_amount : String? = null
    var tax_specific_amount : String? = null
    var tax_lighting_amount : String? = null
    var tax_lighting_percent : String? = null
    var tax_vat_percent : String? = null
    var payment_status : String? = null
    var expired_status : Boolean = true
    var total_tax_amount : String?= null
}