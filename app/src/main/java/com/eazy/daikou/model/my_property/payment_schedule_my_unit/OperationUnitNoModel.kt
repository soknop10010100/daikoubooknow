package com.eazy.daikou.model.my_property.payment_schedule_my_unit

import com.eazy.daikou.model.my_property.service_provider.SelectPropertyModel
import java.io.Serializable

class OperationUnitNoModel : Serializable{
    var all_sale_units : ArrayList<SelectPropertyModel> = ArrayList()
    var all_lease_units : ArrayList<SelectPropertyModel> = ArrayList()
}