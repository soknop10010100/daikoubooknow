package com.eazy.daikou.model

import java.io.Serializable

class CustomImageModel : Serializable {
    var id : String? = null
    var file_path : String? = null
    var file_type : String?= null
}