package com.eazy.daikou.model.inspection;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ImageInspectionModel implements Serializable {

    @SerializedName("document_id")
    @Expose
    private String documentId;
    @SerializedName("inspection_room_id")
    @Expose
    private String inspectionRoomId;
    @SerializedName("file_upload_id")
    @Expose
    private String fileUploadId;
    @SerializedName("file_path")
    @Expose
    private String filePath;

    @SerializedName("sub_inspection_note")
    @Expose
    private String subInspectionNote;

    private boolean isAlreadySaveImage;

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    public String getInspectionRoomId() {
        return inspectionRoomId;
    }

    public void setInspectionRoomId(String inspectionRoomId) {
        this.inspectionRoomId = inspectionRoomId;
    }

    public String getFileUploadId() {
        return fileUploadId;
    }

    public void setFileUploadId(String fileUploadId) {
        this.fileUploadId = fileUploadId;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public boolean isAlreadySaveImage() {
        return isAlreadySaveImage;
    }

    public void setAlreadySaveImage(boolean alreadySaveImage) {
        isAlreadySaveImage = alreadySaveImage;
    }

    public String getSubInspectionNote() {
        return subInspectionNote;
    }

    public void setSubInspectionNote(String subInspectionNote) {
        this.subInspectionNote = subInspectionNote;
    }
}
