package com.eazy.daikou.model.book;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class QuoteModel implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("created_dt")
    @Expose
    private String createdDt;
    @SerializedName("updated_dt")
    @Expose
    private Object updatedDt;
    @SerializedName("child")
    @Expose
    private List<Child> child = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreatedDt() {
        return createdDt;
    }

    public void setCreatedDt(String createdDt) {
        this.createdDt = createdDt;
    }

    public Object getUpdatedDt() {
        return updatedDt;
    }

    public void setUpdatedDt(Object updatedDt) {
        this.updatedDt = updatedDt;
    }

    public List<Child> getChild() {
        return child;
    }

    public void setChild(List<Child> child) {
        this.child = child;
    }

    public static class Child implements Serializable {
        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("book_cover")
        @Expose
        private String bookCover;
        @SerializedName("extension")
        @Expose
        private String extension;
        @SerializedName("file_path")
        @Expose
        private String filePath;
        @SerializedName("created_dt")
        @Expose
        private String createdDt;
        @SerializedName("updated_dt")
        @Expose
        private Object updatedDt;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getBookCover() {
            return bookCover;
        }

        public void setBookCover(String bookCover) {
            this.bookCover = bookCover;
        }

        public String getExtension() {
            return extension;
        }

        public void setExtension(String extension) {
            this.extension = extension;
        }

        public String getFilePath() {
            return filePath;
        }

        public void setFilePath(String filePath) {
            this.filePath = filePath;
        }

        public String getCreatedDt() {
            return createdDt;
        }

        public void setCreatedDt(String createdDt) {
            this.createdDt = createdDt;
        }

        public Object getUpdatedDt() {
            return updatedDt;
        }

        public void setUpdatedDt(Object updatedDt) {
            this.updatedDt = updatedDt;
        }

    }

}
