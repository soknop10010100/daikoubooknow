package com.eazy.daikou.model.my_property.service_provider

import java.io.Serializable

class DetailServiceProviderModel : Serializable{

    var id: String? = null
    var name: String? = null
    var address: String? = null
    var website: String? = null
    var coord_lat: String? = null
    var coord_long: String? = null
    var city: String? = null
    var logo: String? = null
    var phone_number: String? = null
}