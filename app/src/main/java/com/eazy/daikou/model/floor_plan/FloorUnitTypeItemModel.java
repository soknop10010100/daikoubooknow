package com.eazy.daikou.model.floor_plan;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class FloorUnitTypeItemModel implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("space_name")
    @Expose
    private String spaceName;
    @SerializedName("space_image")
    @Expose
    private String spaceImage;
    @SerializedName("description")
    @Expose
    private Object description;
    @SerializedName("storey_id")
    @Expose
    private String storeyId;
    @SerializedName("layout_canvas_json_file")
    @Expose
    private String layoutCanvasJsonFile;
    @SerializedName("fill_color")
    @Expose
    private String fillColor;
    @SerializedName("total_space")
    @Expose
    private Integer totalSpace;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSpaceName() {
        return spaceName;
    }

    public void setSpaceName(String spaceName) {
        this.spaceName = spaceName;
    }

    public String getSpaceImage() {
        return spaceImage;
    }

    public void setSpaceImage(String spaceImage) {
        this.spaceImage = spaceImage;
    }

    public Object getDescription() {
        return description;
    }

    public void setDescription(Object description) {
        this.description = description;
    }

    public String getStoreyId() {
        return storeyId;
    }

    public void setStoreyId(String storeyId) {
        this.storeyId = storeyId;
    }

    public String getLayoutCanvasJsonFile() {
        return layoutCanvasJsonFile;
    }

    public void setLayoutCanvasJsonFile(String layoutCanvasJsonFile) {
        this.layoutCanvasJsonFile = layoutCanvasJsonFile;
    }

    public String getFillColor() {
        return fillColor;
    }

    public void setFillColor(String fillColor) {
        this.fillColor = fillColor;
    }

    public Integer getTotalSpace() {
        return totalSpace;
    }

    public void setTotalSpace(Integer totalSpace) {
        this.totalSpace = totalSpace;
    }

}
