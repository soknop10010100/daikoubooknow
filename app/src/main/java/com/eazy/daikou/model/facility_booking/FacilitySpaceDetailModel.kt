package com.eazy.daikou.model.facility_booking
import java.io.Serializable

class FacilitySpaceDetailModel : Serializable {
    var space_id : String? = null
    var space_name: String? = null
    var floor: String? = null
    var description: String? = null
    var num_people: String? = null
    var note: String? = null
    var created_date: String? = null
    var venue: PropertyVenueModel? = null
    var property: PropertyVenueModel? = null
    var space_images = ArrayList<String>()
    var space_pricing : ArrayList<SpacePricing> = ArrayList()
    var space_tax_setting_percent : SpaceTaxSettingPercent? = null
    var is_free : Boolean = false
}

class SpacePricing : Serializable{
    var id : String? = null
    var type : String? = null
    var from_time : String? = null
    var to_time : String? = null
    var title : String? = null
    var time_number : String? = null
    var price : String? = null
    var unit_type : String? = null
    var operation_day = ArrayList<String>()
}

class SpaceTaxSettingPercent : Serializable {
    var vat_tax : String? = null
    var lighting_tax : String? = null
    var specific_tax : String? = null
}

