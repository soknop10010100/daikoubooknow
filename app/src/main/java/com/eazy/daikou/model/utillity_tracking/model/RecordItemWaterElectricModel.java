package com.eazy.daikou.model.utillity_tracking.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RecordItemWaterElectricModel implements Serializable {

    @SerializedName("tracking_date")
    @Expose
    private String trackingDate;
    @SerializedName("total_usage")
    @Expose
    private String totalUsage;

    public String getTrackingDate() {
        return trackingDate;
    }

    public void setTrackingDate(String trackingDate) {
        this.trackingDate = trackingDate;
    }

    public String getTotalUsage() {
        return totalUsage;
    }

    public void setTotalUsage(String totalUsage) {
        this.totalUsage = totalUsage;
    }

}
