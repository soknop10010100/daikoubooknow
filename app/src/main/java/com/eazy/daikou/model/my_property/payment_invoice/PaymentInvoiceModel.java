package com.eazy.daikou.model.my_property.payment_invoice;

import com.eazy.daikou.model.profile.User;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PaymentInvoiceModel implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("scope")
    @Expose
    private String scope;
    @SerializedName("issue_dt")
    @Expose
    private String issueDt;
    @SerializedName("due_dt")
    @Expose
    private String dueDt;
    @SerializedName("payment_status")
    @Expose
    private String paymentStatus;
    @SerializedName("inv_type")
    @Expose
    private String invType;
    @SerializedName("total")
    @Expose
    private String total;
    @SerializedName("created_dt")
    @Expose
    private String createdDt;
    @SerializedName("account")
    @Expose
    private Account account;
    @SerializedName("unit")
    @Expose
    private Unit unit;
    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("kess_payment_link")
    @Expose
    private String linkPaymentKess;
    @SerializedName("invoice_receipt_url")
    @Expose
    private String invoiceReceiptUrl;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getIssueDt() {
        return issueDt;
    }

    public void setIssueDt(String issueDt) {
        this.issueDt = issueDt;
    }

    public String getDueDt() {
        return dueDt;
    }

    public void setDueDt(String dueDt) {
        this.dueDt = dueDt;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getInvType() {
        return invType;
    }

    public void setInvType(String invType) {
        this.invType = invType;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getCreatedDt() {
        return createdDt;
    }

    public void setCreatedDt(String createdDt) {
        this.createdDt = createdDt;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public String getLinkPaymentKess() {
        return linkPaymentKess;
    }

    public void setLinkPaymentKess(String linkPaymentKess) {
        this.linkPaymentKess = linkPaymentKess;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public void setInvoiceReceiptUrl(String invoiceReceiptUrl) {
        this.invoiceReceiptUrl = invoiceReceiptUrl;
    }

    public String getInvoiceReceiptUrl() {
        return invoiceReceiptUrl;
    }

    public static class Unit implements Serializable{

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("name")
        @Expose
        private String name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    }

    public static class Account implements Serializable{

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("name")
        @Expose
        private String name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    }
}
