package com.eazy.daikou.model.home_page

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class HomePageModelV2 (

    @SerializedName("success" ) var success : Boolean? = null,
    @SerializedName("msg"     ) var msg     : String?  = null,

)

data class PropertyListing (

    @SerializedName("id"            ) var id           : String? = null,
    @SerializedName("name"          ) var name         : String? = null,
    @SerializedName("price"         ) var price        : Int?    = null,
    @SerializedName("address"       ) var address      : String? = null,
    @SerializedName("phone"         ) var phone        : String? = null,
    @SerializedName("category"      ) var category     : String? = null,
    @SerializedName("type"          ) var type         : String? = null,
    @SerializedName("created_at"    ) var createdAt    : String? = null,
    @SerializedName("price_display" ) var priceDisplay : String? = null,
    @SerializedName("image"         ) var image        : String? = null

)

data class DevelopmentProjects (

    @SerializedName("id"      ) var id      : String? = null,
    @SerializedName("name"    ) var name    : String? = null,
    @SerializedName("logo"    ) var logo    : String? = null,
    @SerializedName("city"    ) var city    : String? = null,
    @SerializedName("address" ) var address : String? = null,
    @SerializedName("image"   ) var image   : String? = null

)

data class Data (

    @SerializedName("image_sliders"        ) var imageSliders        : ArrayList<String>              = arrayListOf(),
    @SerializedName("property_listing"     ) var propertyListing     : ArrayList<PropertyListing>     = arrayListOf(),
    @SerializedName("development_projects" ) var developmentProjects : ArrayList<DevelopmentProjects> = arrayListOf()

)

data class AccountsAndContacts (

    @SerializedName("id"                   ) var id                 : String? = null,
    @SerializedName("contact_type"         ) var contactType        : String? = null,
    @SerializedName("account_id"           ) var accountId          : String? = null,
    @SerializedName("account_name"         ) var accountName        : String? = null,
    @SerializedName("contact_type_display" ) var contactTypeDisplay : String? = null

):Serializable