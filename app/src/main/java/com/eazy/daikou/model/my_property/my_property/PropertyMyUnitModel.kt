package com.eazy.daikou.model.my_property.my_property

import com.eazy.daikou.model.my_property.payment_schedule_my_unit.DeveloperInfo
import com.eazy.daikou.model.profile.PropertyItemModel
import java.io.Serializable

class PropertyMyUnitModel : Serializable{
    var id : String? = null
    var name : String? = null
    var qr_code : String? = null
    var floor_no : String? = null
    var price: String? = null
    var total_bedroom : String? = null
    var total_storey : String? = null
    var account_address : String? = null
    var type : Type? = null
    var payment_schedules : ArrayList<PropertyPaymentUnitModel> = ArrayList()
    var developers : ArrayList<DeveloperInfo> = ArrayList()

    var total_area_sqm : String? = null
    var my_images : ArrayList<ImagesModel> = ArrayList()
    var my_videos : ArrayList<ImagesModel> = ArrayList()
    var file_uploads : ArrayList<DocumentModel> = ArrayList()

    var discount : String? = null
    var sale_price : String? = null
    var voucher_amount : String? = null
    var purchase_date : String?= null
    var account : PropertyItemModel?= null

    var account_category : String? = null
    var user_name : String? = null
    var user_phone : String? = null
    var supporting_team_name : String?= null
    var channel_name : String? = null
    var broker_name : String? = null
    var agency_name : String? = null
    var payment_option : String? = null
    var payment_term_type : String? = null

}

class Type : Serializable{
    var id : String? = null
    var name : String? = null
    var category : String? = null
    var total_bedroom : String? = null
    var total_bathroom : String? = null
    var total_living_room : String? = null
    var total_maid_room : String? = null
    var total_area_sqm : String? = null
    var total_private_area : String? = null
    var total_common_area : String? = null
    var style : String? = null
    var layout_photo : String? = null
    var direction : ArrayList<String> = ArrayList()
    var images : ArrayList<ImagesModel> = ArrayList()
}

class ImagesModel : Serializable{
    var id : String? = null
    var file_path : String? = null
}

class DocumentModel : Serializable{
    var id : String? = null
    var name : String? = null
    var file_path : String? = null
    var created_at : String? = null

}
