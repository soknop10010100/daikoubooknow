package com.eazy.daikou.model.profile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class User implements Serializable {

    @SerializedName("position_name")
    @Expose
    private String positionName;
    @SerializedName("active_user_type_display")
    @Expose
    private String activeUserTypeTitle;
    @SerializedName("active_property_title")
    @Expose
    private String activePropertyTitle;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("phone_code")
    @Expose
    private String phoneCode;
    @SerializedName("phone_no")
    @Expose
    private String phoneNo;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("active_account_id")
    @Expose
    private String accountId;
    @SerializedName("active_account_name")
    @Expose
    private String accountName;
    @SerializedName("image")
    @Expose
    private String photo;
    @SerializedName("active_user_type")
    @Expose
    private String activeUserType;
    @SerializedName("cover_image")
    @Expose
    private String coverImage;
    @SerializedName("user_business_key")
    @Expose
    private String userBusinessKey;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("is_guest")
    @Expose
    private boolean isGuestUser;
    @SerializedName("phone")
    @Expose
    private String phoneNumber;
    @SerializedName("active_property_id_fk")
    @Expose
    private String activePropertyIdFk;
    @SerializedName("active_account_business_key")
    @Expose
    private String activeAccountBusinessKey;
    @SerializedName("first_user_login")
    @Expose
    private boolean firstUserLogin;
    @SerializedName("access_token")
    @Expose
    private String accessToken;
    @SerializedName("push_notification")
    @Expose
    private String pushNotification;
    @SerializedName("is_vendor")
    @Expose
    private Boolean isVendor = false;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneCode() {
        return phoneCode;
    }

    public void setPhoneCode(String phoneCode) {
        this.phoneCode = phoneCode;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getActiveUserType() {
        return activeUserType;
    }

    public void setActiveUserType(String activeUserType) {
        this.activeUserType = activeUserType;
    }

    public String getCoverImage() {
        return coverImage;
    }

    public void setCoverImage(String coverImage) {
        this.coverImage = coverImage;
    }

    public String getUserBusinessKey() {
        return userBusinessKey;
    }

    public void setUserBusinessKey(String userBusinessKey) {
        this.userBusinessKey = userBusinessKey;
    }

    public String getActiveAccountBusinessKey() {
        return activeAccountBusinessKey;
    }

    public void setActiveAccountBusinessKey(String activeAccountBusinessKey) {
        this.activeAccountBusinessKey = activeAccountBusinessKey;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public boolean isGuestUser() {
        return isGuestUser;
    }

    public void setGuestUser(boolean guestUser) {
        isGuestUser = guestUser;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getActivePropertyIdFk() {
        return activePropertyIdFk;
    }

    public void setActivePropertyIdFk(String activePropertyIdFk) {
        this.activePropertyIdFk = activePropertyIdFk;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public boolean isFirstUserLogin() {
        return firstUserLogin;
    }

    public void setFirstUserLogin(boolean firstUserLogin) {
        this.firstUserLogin = firstUserLogin;
    }

    public String getPushNotification() {
        return pushNotification;
    }

    public void setPushNotification(String pushNotification) {
        this.pushNotification = pushNotification;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public String getActiveUserTypeTitle() {
        return activeUserTypeTitle;
    }

    public void setActiveUserTypeTitle(String activeUserTypeTitle) {
        this.activeUserTypeTitle = activeUserTypeTitle;
    }

    public String getActivePropertyTitle() {
        return activePropertyTitle;
    }

    public void setActivePropertyTitle(String activePropertyTitle) {
        this.activePropertyTitle = activePropertyTitle;
    }

    public Boolean getVendor() {
        return isVendor;
    }

    public void setVendor(Boolean vendor) {
        isVendor = vendor;
    }
}
