package com.eazy.daikou.model.facility_booking.my_booking

import java.io.Serializable

class CouponBookingModel : Serializable {
    var id : String? = null
    var amount : String? = null
    var amount_type : String? = null
    var coupon_check : String? = null
    var msg : String? = null
    var minimum_purchase : String? = null
}