package com.eazy.daikou.model.my_property.service_provider;

public class ServiceProviderModel {
    String label;
    String ph;
    String imageUrl;
    String address;
    String location;

    public ServiceProviderModel(String label, String ph, String imageUrl, String address, String location) {
        this.label = label;
        this.ph = ph;
        this.imageUrl = imageUrl;
        this.address = address;
        this.location = location;
    }

    public String getLabel() {
        return label;
    }

    public String getPh() {
        return ph;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getAddress() {
        return address;
    }

    public String getLocation() {
        return location;
    }
}
