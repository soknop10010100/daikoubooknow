package com.eazy.daikou.model.booking_hotel

import java.io.Serializable

class HotelVisitDetailModel : Serializable {
    // News
    var id : String ? = null
    var title : String? = null
    var body : String? = null
    var image : String? = null  // use for news
    var name_resource : String? = null
    var link : String?= null
    var views : String?= null
    var created_at : String? = null

    // Visit
    var gallery : ArrayList<String> = ArrayList()
    var lat : String? = null
    var long : String? = null
    var contact : Contact? = null
    var location : String? = null
    var profile_photo : String? = null

    var points : ArrayList<Point> = ArrayList()
}

class Contact : Serializable {
    var phoneNumber : String? = null
    var email : String? = null
    var facebook : String? = null
    var telegram : String? = null
}

class Point : Serializable {
    var title : String? = null
    var image : String? = null
}