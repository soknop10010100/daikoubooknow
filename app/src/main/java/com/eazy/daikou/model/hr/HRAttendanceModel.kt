package com.eazy.daikou.model.hr

import java.io.Serializable
import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName

class HRAttendanceModel : Serializable{
    var today_status: String? = null
    var today_work_status: String? = null
    var today_list: ItemAttendanceModel? = null
    var is_live_photo_required: Boolean = false
    var list: List<ItemAttendanceModel> = ArrayList()
}