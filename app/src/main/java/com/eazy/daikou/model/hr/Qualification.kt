package com.eazy.daikou.model.hr

import androidx.recyclerview.widget.LinearLayoutManager
import com.eazy.daikou.ui.home.hrm.adapter.ItemQualificationAdapter
import java.io.Serializable

class Qualification : Serializable {
    val qualification: String? = null
    val major: String? = null
    val school: String? = null
    val school_addr: String? = null
    val joining_date: String? = null
    val gratuate_date: String? = null
    val issue_date: String? = null
    val action: String? = null


}