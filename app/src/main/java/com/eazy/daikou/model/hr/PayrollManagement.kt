package com.eazy.daikou.model.hr

import java.io.Serializable

class PayrollManagement : Serializable{
     lateinit var basic_employee_info : BasicEmployeeInfo
     var payroll_list : ArrayList<PayrollList> = ArrayList()
}

class BasicEmployeeInfo : Serializable{
    val user_id : String = ""
    val salary_month : String = ""
    val designation : String = ""
    val basic_salary : String = ""
    val order_no : String = ""
    val paid_date : String = ""
    val salary_payment_id : String = ""
    val confirm_payment_at : String = ""
    val joining_date : String = ""
    val employee_type : String = ""
    val payment_type : String = ""
}

class PayrollList : Serializable{

    val order_no : String = ""
    val id : String = ""
    val created_by : String = ""
    val account_id : String = ""
    val user_id : String = ""
    val gross_salary : String = ""
    val total_deduction : String = ""
    val net_salary : String = ""
    val provident_fund : String = ""
    val payment_amount : String = ""
    val payment_month : String = ""
    val payment_type : String = ""
    val note : String = ""
    val paid_date : String = ""
    val created_at : String = ""
    val updated_at : String = ""
    val item_name : String = ""
    val amount : String = ""
    val currency_symbol : String? = null
    val currency_code : String? = null
}
