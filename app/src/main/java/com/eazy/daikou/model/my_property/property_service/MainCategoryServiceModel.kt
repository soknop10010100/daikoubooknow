package com.eazy.daikou.model.my_property.property_service

import com.eazy.daikou.base.BaseModel
import com.eazy.daikou.model.facility_booking.FeeTypeModel

class MainCategoryServiceModel(
    var title : String?,
    var service_item_list : ArrayList<ServiceByCategoryModel> = ArrayList(),
    var service_type_list : ArrayList<FeeTypeModel> = ArrayList()
) : BaseModel()

class ServiceByCategoryModel(
    var name : String?,
    var image : String?,
    var price : String?,
    var description : String?
) : BaseModel()