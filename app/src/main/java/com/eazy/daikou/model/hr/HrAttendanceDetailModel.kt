package com.eazy.daikou.model.hr

import java.io.Serializable

class HrAttendanceDetailModel : Serializable {
    var total_working_hour: TotalWorkingHour? = null
    var my_attendance: Boolean = true
    var check_in: String? = null
    var check_out: String? = null
    var shift_in : String? = null
    var shift_out : String? = null
    var break_time_out : String? = null
    var break_time_in : String? = null
    var button_status: String? = null
    var show_break_out_button : Boolean = false
    var log_list: ArrayList<LogListAttendance> = ArrayList()
}

class LogListAttendance : Serializable{
    val id: String? = null
    val account_id: String? = null
    val user_id: String? = null
    val employee_attendance_id: String? = null
    val attendance_date: String? = null
    val check_datetime: String? = null
    val check_by: String? = null
    val status: String? = null
    val created_by: String? = null
    var id_no: String? = null
    var action_log_status: ActionLogStatus? = null
}

class ActionLogStatus : Serializable{
    val status: String? = null
    val total_hour: String? = null
    val total_minute: String? = null
}
