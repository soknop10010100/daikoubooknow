package com.eazy.daikou.model.my_property.cleaning

import java.io.Serializable

class ServiceProviderDetailModel : Serializable{
    var registration_info : RegistrationInfo? = null
    var reg_schedule_date: ArrayList<String> = ArrayList()
    var service_detail: ServiceDetail? = null
    var date_and_time: DateAndTime? = null
    var booking_summary: BookingSummary? = null
    var history_logs: ArrayList<HistoryLogs> = ArrayList()
    var all_schedule_employees : ArrayList<AllScheduleEmployee> = ArrayList()
}

class RegistrationInfo : Serializable{
    var id: String? = null
    var reg_no: String? = null
    var created_date: String? = null
    var payment_status: String? = null
    var status: String? = null
    var payment_info: ArrayList<PaymentInfo> = ArrayList()

}

class ServiceDetail : Serializable{
    var unit_no: String? = null
    var service_category: String? = null
    var service_term: String? = null
    var service_name: String? = null
    var service_photo: String? = null
    var available_days: ArrayList<String> = ArrayList()
    var available_start_times: ArrayList<String> = ArrayList()
}

class DateAndTime : Serializable{
    var start_date: String? = null
    var end_date: String? = null
    var start_time: String? = null
    var end_time: String? = null
    var duration: String? = null
    var sv_period: String? = null
    var reg_days: ArrayList<String> = ArrayList()
}

class BookingSummary: Serializable{
    var subtotal: String? = null
    var qty: String? = null
    var tax: String? = null
    var total: String? = null
}

class PaymentInfo: Serializable{
    var sale_order_id: String? = null
    var invoice_url: String? = null
    var receipt_url: String? = null
    var kess_payment_url: String? = null
    var from_date: String? = null
    var to_date: String? = null
    var created_dt: String? = null
    var payment_status: String? = null
}

class HistoryLogs: Serializable{
    var user_id_fk: String? = null
    var reg_start_date: String? = null
    var reg_end_date: String? = null
    var reg_start_time: String? = null
    var reg_end_time: String? = null
    var reg_status: String? = null
    var created_at: String? = null
    var user_name: String? = null
    var user_profile_image: String? = null

    var isMine : Boolean = false
}

class AllScheduleEmployee: Serializable{
    var work_schedule_employee_id: String? = null
    var working_date: String? = null
    var user_id: String? = null
    var user_title: String? = null
    var user_full_name: String? = null
    var user_email: String? = null
    var user_name: String? = null
    var user_phone: String? = null
    var user_profile_image: String? = null
    var activity_logs : ArrayList<ActivityLogs> = ArrayList()
    var image_info : ImageInfo? = null
}

class ActivityLogs : Serializable{
    var work_schedule_activity_id: String? = null
    var activity_type: String? = null
    var status: String? = null
    var remark: String? = null
    var created_date: String? = null
}

class ImageInfo : Serializable{
    var before_activity : ArrayList<ImageActivity> = ArrayList()
    var after_activity : ArrayList<ImageActivity> = ArrayList()
}

class ImageActivity : Serializable{
    var id : String? = null
    var file_path: String? = null
}

