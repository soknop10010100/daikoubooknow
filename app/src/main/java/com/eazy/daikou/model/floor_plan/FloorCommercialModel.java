package com.eazy.daikou.model.floor_plan;

import android.content.Context;

import com.eazy.daikou.R;

import java.util.ArrayList;
import java.util.List;

public class FloorCommercialModel {
    private String commercialName;

    public String getCommercialName() {
        return commercialName;
    }

    public void setCommercialName(String commercialName) {
        this.commercialName = commercialName;
    }

    public List<FloorCommercialModel> getListCommercialFloor(Context context){
        List<FloorCommercialModel> commercialList = new ArrayList<>();
        for (int i = 1; i <= 5; i++){
            FloorCommercialModel commercialModel = new FloorCommercialModel();
            commercialModel.setCommercialName(context.getResources().getString(R.string.commercial_floor)+ " " + i);
            commercialList.add(commercialModel);
        }
        return commercialList;
    }

    public List<FloorCommercialModel> getListUnitTypeFloor(){
        List<FloorCommercialModel> commercialList = new ArrayList<>();
        for (int i = 1; i <= 100; i++){
            FloorCommercialModel commercialModel = new FloorCommercialModel();
            commercialModel.setCommercialName(" " + (100 + i));
            commercialList.add(commercialModel);
        }
        return commercialList;
    }
}
