package com.eazy.daikou.model.work_order;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class WorkOrderItemModel implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("work_order_no")
    @Expose
    private String workOrderNo;
    @SerializedName("issue_date")
    @Expose
    private String issueDate;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("priority")
    @Expose
    private Integer priority;
    @SerializedName("subject")
    @Expose
    private String subject;
    @SerializedName("author_user_id")
    @Expose
    private String authorUserId;
    @SerializedName("assignee_user_id")
    @Expose
    private String assigneeUserId;
    @SerializedName("done_percent")
    @Expose
    private String donePercent;
    @SerializedName("work_order_type")
    @Expose
    private String workOrderType;
    @SerializedName("item_no")
    @Expose
    private Object itemNo;
    @SerializedName("report_type_no")
    @Expose
    private String reportTypeNo;
    @SerializedName("parent_task")
    @Expose
    private String parentTask;
    @SerializedName("author_user_info")
    @Expose
    private WorkOrderUserInfo authorUserInfo;
    @SerializedName("assignee_user_info")
    @Expose
    private WorkOrderUserInfo assigneeUserInfo;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getWorkOrderNo() {
        return workOrderNo;
    }

    public void setWorkOrderNo(String workOrderNo) {
        this.workOrderNo = workOrderNo;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getAuthorUserId() {
        return authorUserId;
    }

    public void setAuthorUserId(String authorUserId) {
        this.authorUserId = authorUserId;
    }

    public String getAssigneeUserId() {
        return assigneeUserId;
    }

    public void setAssigneeUserId(String assigneeUserId) {
        this.assigneeUserId = assigneeUserId;
    }

    public String getDonePercent() {
        return donePercent;
    }

    public void setDonePercent(String donePercent) {
        this.donePercent = donePercent;
    }

    public String getWorkOrderType() {
        return workOrderType;
    }

    public void setWorkOrderType(String workOrderType) {
        this.workOrderType = workOrderType;
    }

    public Object getItemNo() {
        return itemNo;
    }

    public void setItemNo(Object itemNo) {
        this.itemNo = itemNo;
    }

    public String getReportTypeNo() {
        return reportTypeNo;
    }

    public void setReportTypeNo(String reportTypeNo) {
        this.reportTypeNo = reportTypeNo;
    }

    public String getParentTask() {
        return parentTask;
    }

    public void setParentTask(String parentTask) {
        this.parentTask = parentTask;
    }

    public WorkOrderUserInfo getAuthorUserInfo() {
        return authorUserInfo;
    }

    public void setAuthorUserInfo(WorkOrderUserInfo authorUserInfo) {
        this.authorUserInfo = authorUserInfo;
    }

    public WorkOrderUserInfo getAssigneeUserInfo() {
        return assigneeUserInfo;
    }

    public void setAssigneeUserInfo(WorkOrderUserInfo assigneeUserInfo) {
        this.assigneeUserInfo = assigneeUserInfo;
    }

}
