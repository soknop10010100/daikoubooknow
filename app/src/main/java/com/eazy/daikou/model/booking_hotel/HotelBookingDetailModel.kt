package com.eazy.daikou.model.booking_hotel

import java.io.Serializable

class HotelBookingDetailModel : Serializable {
    var id : String? = null
    var images : ArrayList<String> = ArrayList()
    var price_display : String? = null
    var original_price_display : String? = null
    var name : String? = null
    var price : Double = 0.0
    var star_rate : String? = null
    var coord_lat : String? = null
    var coord_long : String? = null
    var address : String? = null
    var location_id : String? = null
    var description : String? = null
    var image : String? = null
    var policies : ArrayList<PoliciesModel> = ArrayList()
    var facilities : ArrayList<String> = ArrayList()
    var services : ArrayList<String> = ArrayList()
    var types : ArrayList<String> = ArrayList()
    var rates_and_reviews : ArrayList<ReviewHotelModel> = ArrayList()
    var shared_link : String? = null
    var is_wishlist = false

    var extra_price : ArrayList<ExtraPrice> = ArrayList()

    // item can book
    var booking_available : Boolean = false

    //Service fee and buyer fee
    var service_fees : ArrayList<ExtraPrice> = ArrayList()
    var buyer_fees : ArrayList<ExtraPrice> = ArrayList()

    // Space
    var max_people : String? = null
    var size : String? = null
    var beds : String? = null
    var bathrooms : String? = null

    // Event
    var start_time : String? = null
    var duration : String? = null
    var ticket_types : ArrayList<TicketType> = ArrayList()

    // Tour
    var date_form_to : String? = null
    var min_age : String? = null
    var pickup : String? = null
    var include : ArrayList<String> = ArrayList()
    var exclude : ArrayList<String> = ArrayList()
    var itinerary : ArrayList<PoliciesModel> = ArrayList()
}

class PoliciesModel : Serializable{
    var title : String? = null
    var content : String? = null
    var desc : String? = null
}

class ReviewHotelModel : Serializable{
    var id : String? = null
    var review : String? = null
    var star_rate : String? = null
    var created_at : String? = null
    var user : UserH? = null
}

class UserH : Serializable{
    var image : String? = null
    var name : String? = null
}

class ExtraPrice : Serializable{
    var name : String? = null
    var price_display : String? = null
    var type : String? = null
    var price : Double = 0.0
    var isClick : Boolean = false
}

class TicketType : Serializable{
    var code : String? = null
    var name : String? = null
    var price : Double= 0.0
    var number : String? = null
    var price_display : String? = null

    var totalAllPrice : Double = 0.0
    var totalRoom : Int = 0
    var numberRoomSelect : Int = 0

    var num : Int = 0
    var quantity : Int = 0
    var isClick : Boolean = false

    var date_form_to : String? = null
    var min_age : String? = null
}

class PaymentSuccessModel : Serializable {
    var code : String? = null
    var first_name : String? = null
    var last_name : String? = null
    var phone : String? = null
    var paid : String? = null
}