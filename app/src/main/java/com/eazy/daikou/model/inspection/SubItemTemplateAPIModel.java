package com.eazy.daikou.model.inspection;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SubItemTemplateAPIModel implements Serializable {
    @SerializedName("room_item_id")
    @Expose
    private String roomItemId;
    @SerializedName("item_no")
    @Expose
    private String itemNo;
    @SerializedName("item_name")
    @Expose
    private String itemName;
    @SerializedName("item_desc")
    @Expose
    private String itemDesc;
    @SerializedName("item_condition")
    @Expose
    private String itemCondition;
    @SerializedName("work_order_id")
    @Expose
    private String workOrderId;

    @SerializedName("option_type")
    @Expose
    private String optionType;
    @SerializedName("report_type")
    @Expose
    private String reportType;
    @SerializedName("unit_no")
    @Expose
    private String unitNo;
    @SerializedName("inspection_id")
    @Expose
    private String inspectionId;
    @SerializedName("inspection_name")
    @Expose
    private String inspectionName;
    @SerializedName("room_name")
    @Expose
    private String roomName;
    @SerializedName("inspection_type_id")
    @Expose
    private String inspectionTypeId;
    @SerializedName("work_order_no")
    @Expose
    private String workOrderNo;
    @SerializedName("work_order_subject_default")
    @Expose
    private String workOrderSubjectDefault;
    @SerializedName("work_order_no_default")
    @Expose
    private String workOrderNoDefault;

    public String getRoomItemId() {
        return roomItemId;
    }

    public void setRoomItemId(String roomItemId) {
        this.roomItemId = roomItemId;
    }

    public String getItemNo() {
        return itemNo;
    }

    public void setItemNo(String itemNo) {
        this.itemNo = itemNo;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemDesc() {
        return itemDesc;
    }

    public void setItemDesc(String itemDesc) {
        this.itemDesc = itemDesc;
    }

    public String getItemCondition() {
        return itemCondition;
    }

    public void setItemCondition(String itemCondition) {
        this.itemCondition = itemCondition;
    }

    public String getWorkOrderId() {
        return workOrderId;
    }

    public void setWorkOrderId(String workOrderId) {
        this.workOrderId = workOrderId;
    }

    public String getOptionType() {
        return optionType;
    }

    public void setOptionType(String optionType) {
        this.optionType = optionType;
    }

    public String getReportType() {
        return reportType;
    }

    public void setReportType(String reportType) {
        this.reportType = reportType;
    }

    public String getUnitNo() {
        return unitNo;
    }

    public void setUnitNo(String unitNo) {
        this.unitNo = unitNo;
    }

    public String getInspectionId() {
        return inspectionId;
    }

    public void setInspectionId(String inspectionId) {
        this.inspectionId = inspectionId;
    }

    public String getInspectionName() {
        return inspectionName;
    }

    public void setInspectionName(String inspectionName) {
        this.inspectionName = inspectionName;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public String getInspectionTypeId() {
        return inspectionTypeId;
    }

    public void setInspectionTypeId(String inspectionTypeId) {
        this.inspectionTypeId = inspectionTypeId;
    }

    public String getWorkOrderNo() {
        return workOrderNo;
    }

    public void setWorkOrderNo(String workOrderNo) {
        this.workOrderNo = workOrderNo;
    }

    public String getWorkOrderSubjectDefault() {
        return workOrderSubjectDefault;
    }

    public void setWorkOrderSubjectDefault(String workOrderSubjectDefault) {
        this.workOrderSubjectDefault = workOrderSubjectDefault;
    }

    public String getWorkOrderNoDefault() {
        return workOrderNoDefault;
    }

    public void setWorkOrderNoDefault(String workOrderNoDefault) {
        this.workOrderNoDefault = workOrderNoDefault;
    }
}
