package com.eazy.daikou.model.hr

import java.io.Serializable

class Relationship : Serializable {

    val name: String? = null
    val contact: String? = null
    val relationship: String? = null
    val created_by: String? = null
    val dob: String? = null
    val occupation: String? = null
    val company: String? = null
    val job_title: String? = null
    val address: String? = null
}