package com.eazy.daikou.model.data_record

import java.io.Serializable

class OpportunityModel : Serializable {
    var id : String? = null
    var name : String? = null
    var created_at : String? = null
    var status : String? = null
    var account : AccountModel? = null
    var interesting : ArrayList<InterestingModel> = ArrayList()
}
class AccountModel : Serializable{
    var id : String? = null
    var name : String? = null
}

class InterestingModel : Serializable{
    var id : String? = null
    var created_at :String? = null
    var price : String? = null
    var unit : UnitModel? = null
}

class UnitModel : Serializable{
    var id : String? = null
    var name : String? = null
    var floor : String? = null
    var price : String? = null
    var status : String? = null
    var type : TypeModel? = null
}

class TypeModel : Serializable{
    var id : String? = null
    var name : String? = null
    var total_bedroom : String? = null
    var total_bathroom : String? = null
    var total_living_room : String? = null
    var total_maid_room : String? = null
    var total_area_sqm : String? = null
    var total_private_area : String? = null
    var total_common_area : String? = null

}
