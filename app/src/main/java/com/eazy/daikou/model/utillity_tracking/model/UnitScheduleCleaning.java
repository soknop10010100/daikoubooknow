package com.eazy.daikou.model.utillity_tracking.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UnitScheduleCleaning implements Serializable {

    @SerializedName("id")
    @Expose
    private Object id;
    @SerializedName("cleaning_schedule_id")
    @Expose
    private String cleaningScheduleId;
    @SerializedName("unit_no")
    @Expose
    private Object unitNo;
    @SerializedName("work_space_id")
    @Expose
    private String workSpaceId;
    @SerializedName("work_zone_id")
    @Expose
    private String workZoneId;
    @SerializedName("work_floor_id")
    @Expose
    private String workFloorId;
    @SerializedName("work_space_category")
    @Expose
    private String workSpaceCategory;
    @SerializedName("unit_id")
    @Expose
    private Object unitId;
    @SerializedName("start_date")
    @Expose
    private Object startDate;
    @SerializedName("end_date")
    @Expose
    private Object endDate;
    @SerializedName("start_time")
    @Expose
    private Object startTime;
    @SerializedName("end_time")
    @Expose
    private Object endTime;
    @SerializedName("service_type")
    @Expose
    private Object serviceType;
    @SerializedName("service_term")
    @Expose
    private Object serviceTerm;
    @SerializedName("cleaning_service_name")
    @Expose
    private Object cleaningServiceName;
    @SerializedName("cleaner_schedule_status")
    @Expose
    private String cleanerScheduleStatus;
    @SerializedName("cleaning_category")
    @Expose
    private String cleaningCategory;
    @SerializedName("work_space_name")
    @Expose
    private String workSpaceName;
    @SerializedName("work_zone_name")
    @Expose
    private String workZoneName;
    @SerializedName("work_floor_name")
    @Expose
    private String workFloorName;
    @SerializedName("order_no")
    @Expose
    private String orderNo;

    public Object getId() {
        return id;
    }

    public void setId(Object id) {
        this.id = id;
    }

    public String getCleaningScheduleId() {
        return cleaningScheduleId;
    }

    public void setCleaningScheduleId(String cleaningScheduleId) {
        this.cleaningScheduleId = cleaningScheduleId;
    }

    public Object getUnitNo() {
        return unitNo;
    }

    public void setUnitNo(Object unitNo) {
        this.unitNo = unitNo;
    }

    public String getWorkSpaceId() {
        return workSpaceId;
    }

    public void setWorkSpaceId(String workSpaceId) {
        this.workSpaceId = workSpaceId;
    }

    public String getWorkZoneId() {
        return workZoneId;
    }

    public void setWorkZoneId(String workZoneId) {
        this.workZoneId = workZoneId;
    }

    public String getWorkFloorId() {
        return workFloorId;
    }

    public void setWorkFloorId(String workFloorId) {
        this.workFloorId = workFloorId;
    }

    public String getWorkSpaceCategory() {
        return workSpaceCategory;
    }

    public void setWorkSpaceCategory(String workSpaceCategory) {
        this.workSpaceCategory = workSpaceCategory;
    }

    public Object getUnitId() {
        return unitId;
    }

    public void setUnitId(Object unitId) {
        this.unitId = unitId;
    }

    public Object getStartDate() {
        return startDate;
    }

    public void setStartDate(Object startDate) {
        this.startDate = startDate;
    }

    public Object getEndDate() {
        return endDate;
    }

    public void setEndDate(Object endDate) {
        this.endDate = endDate;
    }

    public Object getStartTime() {
        return startTime;
    }

    public void setStartTime(Object startTime) {
        this.startTime = startTime;
    }

    public Object getEndTime() {
        return endTime;
    }

    public void setEndTime(Object endTime) {
        this.endTime = endTime;
    }

    public Object getServiceType() {
        return serviceType;
    }

    public void setServiceType(Object serviceType) {
        this.serviceType = serviceType;
    }

    public Object getServiceTerm() {
        return serviceTerm;
    }

    public void setServiceTerm(Object serviceTerm) {
        this.serviceTerm = serviceTerm;
    }

    public Object getCleaningServiceName() {
        return cleaningServiceName;
    }

    public void setCleaningServiceName(Object cleaningServiceName) {
        this.cleaningServiceName = cleaningServiceName;
    }

    public String getCleanerScheduleStatus() {
        return cleanerScheduleStatus;
    }

    public void setCleanerScheduleStatus(String cleanerScheduleStatus) {
        this.cleanerScheduleStatus = cleanerScheduleStatus;
    }

    public String getCleaningCategory() {
        return cleaningCategory;
    }

    public void setCleaningCategory(String cleaningCategory) {
        this.cleaningCategory = cleaningCategory;
    }

    public String getWorkSpaceName() {
        return workSpaceName;
    }

    public void setWorkSpaceName(String workSpaceName) {
        this.workSpaceName = workSpaceName;
    }

    public String getWorkZoneName() {
        return workZoneName;
    }

    public void setWorkZoneName(String workZoneName) {
        this.workZoneName = workZoneName;
    }

    public String getWorkFloorName() {
        return workFloorName;
    }

    public void setWorkFloorName(String workFloorName) {
        this.workFloorName = workFloorName;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }
}
