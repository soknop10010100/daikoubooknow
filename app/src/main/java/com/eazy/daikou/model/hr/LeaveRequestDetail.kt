package com.eazy.daikou.model.hr

import java.io.Serializable

class LeaveRequestDetail : Serializable{

    var  profile_leave : ProfileLeave = ProfileLeave()
    var info_leave : InformationLeave = InformationLeave()
    val reason_leave = ""

}

class ProfileLeave : Serializable{
    val id  : String = ""
    val user_id : String = ""
    val user_name  = ""
    val id_number  = ""
    val designation_id  = ""
    val designation  = ""
    val leave_category_id  = ""
    val leave_category  = ""
    val start_date  = ""
    val user_business_key : String = ""
    val end_date = ""
    val created_date = ""
    val approved_by_user_id = ""
    val approver_user_name : String? = null
    val approved_by_user_name = ""
    val total_user_leave_balance : String? = null
    val remark : String? = null
}
class InformationLeave : Serializable {
    val id= ""
    val during_leave= ""
    val last_leave_period= ""
    val last_leave_category_id= ""
    val last_leave_category= ""
    val leave_address= ""
    val created_date= ""
    val end_date= ""
    val status = ""
    val period = ""
}