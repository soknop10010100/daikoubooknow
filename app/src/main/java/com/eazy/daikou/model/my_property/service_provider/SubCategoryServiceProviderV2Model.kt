package com.eazy.daikou.model.my_property.service_provider

import java.io.Serializable

class SubCategoryServiceProviderV2Model : Serializable {

    var id : String? = null
    var name : String? = null
    var address : String? = null
    var logo : String? = null
}