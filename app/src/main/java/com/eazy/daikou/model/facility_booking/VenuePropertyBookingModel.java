package com.eazy.daikou.model.facility_booking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class VenuePropertyBookingModel implements Serializable {
    private Boolean isClickTitle;
    @SerializedName("facility_basic_id")
    @Expose
    private String facilityBasicId;
    @SerializedName("venue_name")
    @Expose
    private String venueName;

    public String getFacilityBasicId() {
        return facilityBasicId;
    }

    public void setFacilityBasicId(String facilityBasicId) {
        this.facilityBasicId = facilityBasicId;
    }

    public String getVenueName() {
        return venueName;
    }

    public void setVenueName(String venueName) {
        this.venueName = venueName;
    }

    public Boolean getClickTitle() {
        return isClickTitle;
    }

    public void setClickTitle(Boolean clickTitle) {
        isClickTitle = clickTitle;
    }
}
