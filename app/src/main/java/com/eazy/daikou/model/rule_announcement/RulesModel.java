package com.eazy.daikou.model.rule_announcement;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RulesModel implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("version")
    @Expose
    private String version;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("file_path")
    @Expose
    private String filePath;
    @SerializedName("extension")
    @Expose
    private String extension;
    @SerializedName("time_value")
    @Expose
    private String timeValue;
    @SerializedName("time_post_to_now")
    @Expose
    private TimePostToNow timePostToNow;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getTimeValue() {
        return timeValue;
    }

    public void setTimeValue(String timeValue) {
        this.timeValue = timeValue;
    }

    public TimePostToNow getTimePostToNow() {
        return timePostToNow;
    }

    public void setTimePostToNow(TimePostToNow timePostToNow) {
        this.timePostToNow = timePostToNow;
    }

    public static class TimePostToNow implements Serializable{

        @SerializedName("value")
        @Expose
        private Integer value;
        @SerializedName("type")
        @Expose
        private String type;

        public Integer getValue() {
            return value;
        }

        public void setValue(Integer value) {
            this.value = value;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

    }
}
