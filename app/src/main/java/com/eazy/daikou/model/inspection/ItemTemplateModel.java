package com.eazy.daikou.model.inspection;

import java.io.Serializable;

public class ItemTemplateModel implements Serializable {

    private String key;
    private String value;
    private String main_category_inspection;
    private final String type_menu;
    private boolean isAlreadySave;
    private ItemTemplateAPIModel itemTemplateAPIModel;

    public ItemTemplateModel(String key, String value, String type_menu) {
        this.type_menu = type_menu;
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getMain_category_inspection() {
        return main_category_inspection;
    }

    public void setMain_category_inspection(String main_category_inspection) {
        this.main_category_inspection = main_category_inspection;
    }

    public String getType_menu() {
        return type_menu;
    }

    public boolean isAlreadySave() {
        return isAlreadySave;
    }

    public void setAlreadySave(boolean alreadySave) {
        isAlreadySave = alreadySave;
    }

    public ItemTemplateAPIModel getItemTemplateAPIModel() {
        return itemTemplateAPIModel;
    }

    public void setItemTemplateAPIModel(ItemTemplateAPIModel itemTemplateAPIModel) {
        this.itemTemplateAPIModel = itemTemplateAPIModel;
    }

    public static class SubItemTemplateModel implements Serializable {

        private String sub_item_category;
        private String item_description;
        private String item_condition;
        private String key;
        private String value;
        private String clickClean;
        private String clickUnDamage;
        private String clickWorking;
        private final String key_menu;
        private String item_no;
        private boolean edit_item;
        private String item_room_id;

        private String reportType;
        private String workOrderSubjectDefault;
        private String workOrderNoDefault;

        public SubItemTemplateModel(String key, String value, String key_menu,String clickClean, String clickUnDamage, String clickWorking, boolean edit_item) {
            this.key = key;
            this.value = value;
            this.key_menu = key_menu;
            this.clickClean = clickClean;
            this.clickUnDamage = clickUnDamage;
            this.clickWorking = clickWorking;
            this.edit_item = edit_item;
        }

        public String getSub_item_category() {
            return sub_item_category;
        }

        public void setSub_item_category(String sub_item_category) {
            this.sub_item_category = sub_item_category;
        }

        public String getItem_description() {
            return item_description;
        }

        public void setItem_description(String item_description) {
            this.item_description = item_description;
        }

        public String getItem_condition() {
            return item_condition;
        }

        public void setItem_condition(String item_condition) {
            this.item_condition = item_condition;
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getIsClickNA() {
            return clickClean;
        }

        public void setIsClickNA(String isClickNA) {
            this.clickClean = isClickNA;
        }

        public String getClickUnDamage() {
            return clickUnDamage;
        }

        public void setClickUnDamage(String clickUnDamage) {
            this.clickUnDamage = clickUnDamage;
        }

        public String getClickWorking() {
            return clickWorking;
        }

        public void setClickWorking(String clickWorking) {
            this.clickWorking = clickWorking;
        }

        public String getKey_menu() {
            return key_menu;
        }

        public String getItem_no() {
            return item_no;
        }

        public void setItem_no(String item_no) {
            this.item_no = item_no;
        }

        public boolean isEdit_item() {
            return edit_item;
        }

        public void setEdit_item(boolean edit_item) {
            this.edit_item = edit_item;
        }

        public String getItem_room_id() {
            return item_room_id;
        }

        public void setItem_room_id(String item_room_id) {
            this.item_room_id = item_room_id;
        }

        public String getReportType() {
            return reportType;
        }

        public void setReportType(String reportType) {
            this.reportType = reportType;
        }

        public String getWorkOrderSubjectDefault() {
            return workOrderSubjectDefault;
        }

        public void setWorkOrderSubjectDefault(String workOrderSubjectDefault) {
            this.workOrderSubjectDefault = workOrderSubjectDefault;
        }

        public String getWorkOrderNoDefault() {
            return workOrderNoDefault;
        }

        public void setWorkOrderNoDefault(String workOrderNoDefault) {
            this.workOrderNoDefault = workOrderNoDefault;
        }
    }
}
