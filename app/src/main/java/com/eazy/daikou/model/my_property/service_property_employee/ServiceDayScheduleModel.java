package com.eazy.daikou.model.my_property.service_property_employee;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ServiceDayScheduleModel implements Serializable {
    @SerializedName("working_date")
    @Expose
    private String working_date;
    @SerializedName("total_works")
    @Expose
    private String total_works;
    @SerializedName("schedule_status")
    @Expose
    private String schedule_status;
    @SerializedName("day_name")
    @Expose
    private String day_name;

    public String getWorking_date() {
        return working_date;
    }

    public void setWorking_date(String working_date) {
        this.working_date = working_date;
    }

    public String getTotal_works() {
        return total_works;
    }

    public void setTotal_works(String total_works) {
        this.total_works = total_works;
    }

    public String getSchedule_status() {
        return schedule_status;
    }

    public void setSchedule_status(String schedule_status) {
        this.schedule_status = schedule_status;
    }

    public String getDay_name() {
        return day_name;
    }

    public void setDay_name(String day_name) {
        this.day_name = day_name;
    }
}
