package com.eazy.daikou.model.facility_booking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class AlertPropertyBookingModel implements Serializable {
    @SerializedName("property_id")
    @Expose
    private String propertyId;
    @SerializedName("property_name")
    @Expose
    private String propertyName;
    private boolean isClick;

    public String getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(String propertyId) {
        this.propertyId = propertyId;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    public boolean isClick() {
        return isClick;
    }

    public void setClick(boolean click) {
        isClick = click;
    }
}
