package com.eazy.daikou.model.inspection;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ItemInspectionModel implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("type_name")
    @Expose
    private String inspection_name;
    @SerializedName("main_type")
    @Expose
    private String main_category_type;
    @SerializedName("template_name")
    @Expose
    private String templateName;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("created_dt")
    @Expose
    private String createdDt;

    private boolean isClick;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public  ItemInspectionModel(){}

    public String getInspection_name() {
        return inspection_name;
    }

    public void setInspection_name(String inspection_name) {
        this.inspection_name = inspection_name;
    }

    public boolean isClick() {
        return isClick;
    }

    public void setClick(boolean click) {
        isClick = click;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreatedDt() {
        return createdDt;
    }

    public void setCreatedDt(String createdDt) {
        this.createdDt = createdDt;
    }

    public String getMain_category_type() {
        return main_category_type;
    }

    public void setMain_category_type(String main_category_type) {
        this.main_category_type = main_category_type;
    }
}
