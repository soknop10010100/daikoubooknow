package com.eazy.daikou.model.floor_plan;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class FloorSpaceItemModel implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("space_name")
    @Expose
    private String spaceName;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("space_image")
    @Expose
    private String spaceImage;
    @SerializedName("fill_color")
    @Expose
    private String fillColor;
    @SerializedName("total_space")
    @Expose
    private Integer totalSpace;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSpaceName() {
        return spaceName;
    }

    public void setSpaceName(String spaceName) {
        this.spaceName = spaceName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSpaceImage() {
        return spaceImage;
    }

    public void setSpaceImage(String spaceImage) {
        this.spaceImage = spaceImage;
    }

    public String getFillColor() {
        return fillColor;
    }

    public void setFillColor(String fillColor) {
        this.fillColor = fillColor;
    }

    public Integer getTotalSpace() {
        return totalSpace;
    }

    public void setTotalSpace(Integer totalSpace) {
        this.totalSpace = totalSpace;
    }
}
