package com.eazy.daikou.model.my_property.payment_schedule_my_unit

import java.io.Serializable

class DetailInvoiceModel : Serializable {
    var labelItem : String = ""
    var valueItem : String = ""
    constructor(labelItem: String, valueItem: String) {
        this.labelItem = labelItem
        this.valueItem = valueItem
    }
}