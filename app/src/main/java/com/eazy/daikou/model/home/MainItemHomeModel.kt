package com.eazy.daikou.model.home

import android.graphics.drawable.Drawable
import com.eazy.daikou.model.hr.ItemHR
import java.io.Serializable

class MainItemHomeModel : Serializable {
    constructor(mainAction: String?, headerTitle: String?, descriptionTitle : String?) {
        this.mainAction = mainAction
        this.headerTitle = headerTitle
        this.descriptionTitle = descriptionTitle
    }
    constructor()

    var mainAction : String? = null
    var headerTitle : String? = null
    var descriptionTitle : String? = null
    var subItemHomeModelList : ArrayList<SubItemHomeModel> = ArrayList()
    var subItemMoreModelList : ArrayList<ItemHR> = ArrayList()
}

class SubItemHomeModel : Serializable{
    var id : String? = null
    var name : String? = null
    var detailName : String? = null
    var imageUrl : String? = null
    var isFreeBooking : Boolean = false
    var action : String? = null
    var bWebsite : String? = null
    var priceVal : String? = null
    var priceOriginalBeforeDiscount : String? = null
    var icon: Int = 0

    // item can book
    var isBookingAvailable : Boolean = false

    // Hotel item
    var address : String? = null
    var rating : String? = null
    var totalHotel : String? = null
    var locationId : String? = null
    var isWishlist : Boolean = false
    var distance : String? = null

    var drawable: Drawable? = null
    var isClickCategory : Boolean = false

    // Location lat lng
    var coordLat : String? = null
    var coordLng : String? = null
    var titleCategory : String? = null

    // Visit
    var titleKh : String? = null
    var views : String? = null
}

class UserDeactivated : Serializable{
    var is_deactivated : Boolean = false
    var is_expired : Boolean = false
}