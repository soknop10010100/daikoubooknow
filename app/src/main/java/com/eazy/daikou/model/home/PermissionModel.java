package com.eazy.daikou.model.home;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PermissionModel implements Serializable {

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("read")
    @Expose
    private Boolean read;
    @SerializedName("write")
    @Expose
    private Boolean write;
    @SerializedName("update")
    @Expose
    private Boolean update;
    @SerializedName("delete")
    @Expose
    private Boolean delete;
    @SerializedName("employee_role")
    @Expose
    private Boolean employeeRole;

//    public Boolean getRead() {
//        return read;
//    }
//
//    public void setRead(Boolean read) {
//        this.read = read;
//    }
//
//    public Boolean getWrite() {
//        return write;
//    }
//
//    public void setWrite(Boolean write) {
//        this.write = write;
//    }
//
//    public Boolean getUpdate() {
//        return update;
//    }
//
//    public void setUpdate(Boolean update) {
//        this.update = update;
//    }
//
//    public Boolean getDelete() {
//        return delete;
//    }
//
//    public void setDelete(Boolean delete) {
//        this.delete = delete;
//    }
//
//    public String getTitle() {
//        return title;
//    }
//
//    public void setTitle(String title) {
//        this.title = title;
//    }
//
//    public Boolean getEmployeeRole() {
//        return employeeRole;
//    }
//
//    public void setEmployeeRole(Boolean employeeRole) {
//        this.employeeRole = employeeRole;
//    }
}
