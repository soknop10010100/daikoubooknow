package com.eazy.daikou.model.book;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class BookCategoryV2 implements Serializable {

    @SerializedName("books")
    @Expose
    private List<BookModelV2> bookModelList = null;
    @SerializedName("quotes")
    @Expose
    private List<BookModelV2> quoteModelList = null;

    public List<BookModelV2> getBookList() {
        return bookModelList;
    }

    public void setBooks(List<BookModelV2> bookModelList) {
        this.bookModelList = bookModelList;
    }

    public List<BookModelV2> getQuoteList() {
        return quoteModelList;
    }

    public void setQuotes(List<BookModelV2> quoteModelList) {
        this.quoteModelList = quoteModelList;
    }
}
