package com.eazy.daikou.model.my_property;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Child implements Serializable {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("total_service_provider")
    @Expose
    private String totalServiceProvider;

    private boolean isClick;

    public boolean isClick() {
        return isClick;
    }

    public void setClick(boolean click) {
        isClick = click;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTotalServiceProvider() {
        return totalServiceProvider;
    }

    public void setTotalServiceProvider(String totalServiceProvider) {
        this.totalServiceProvider = totalServiceProvider;
    }
}
