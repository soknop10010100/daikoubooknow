package com.eazy.daikou.model.my_property.property_service

import com.eazy.daikou.base.BaseModel

data class PropertyServiceListModel(
    var reg_no : String?,
    var operation_part : String?,
    var client_name : String?,
    var unit_no : String?,
    var service_name : String?,
    var start_date : String?,
    var end_date : String?,
    var price : String?,
    var tax : String?,
    var service_type : String?,
    var total_tax_amount : String?,
    var total_price_amount : String?,
    var status : String?,

    // client info
    var username : String?,
    var phone : String?,
    var address : String?,

    ) : BaseModel()