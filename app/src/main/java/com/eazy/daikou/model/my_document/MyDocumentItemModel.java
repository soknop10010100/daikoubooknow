package com.eazy.daikou.model.my_document;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MyDocumentItemModel implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("document_category")
    @Expose
    private String document_category;
    @SerializedName("document_name")
    @Expose
    private String document_name;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("created_dt")
    @Expose
    private String created_dt;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDocument_category() {
        return document_category;
    }

    public void setDocument_category(String document_category) {
        this.document_category = document_category;
    }

    public String getDocument_name() {
        return document_name;
    }

    public void setDocument_name(String document_name) {
        this.document_name = document_name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCreated_dt() {
        return created_dt;
    }

    public void setCreated_dt(String created_dt) {
        this.created_dt = created_dt;
    }
}
