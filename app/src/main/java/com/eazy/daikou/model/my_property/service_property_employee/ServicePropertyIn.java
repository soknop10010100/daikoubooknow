package com.eazy.daikou.model.my_property.service_property_employee;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class ServicePropertyIn implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("property_id")
    @Expose
    private String propertyId;
    @SerializedName("service_name")
    @Expose
    private String serviceName;
    @SerializedName("service_address")
    @Expose
    private String serviceAddress;
    @SerializedName("service_website")
    @Expose
    private String serviceWebsite;
    @SerializedName("service_email")
    @Expose
    private String serviceEmail;
    @SerializedName("service_facebook")
    @Expose
    private String serviceFacebook;
    @SerializedName("service_wechat")
    @Expose
    private String serviceWechat;
    @SerializedName("service_telegram")
    @Expose
    private String serviceTelegram;
    @SerializedName("service_whatsapp")
    @Expose
    private String serviceWhatsapp;
    @SerializedName("service_logo")
    @Expose
    private String serviceLogo;
    @SerializedName("service_desc")
    @Expose
    private String serviceDesc;
    @SerializedName("service_phone_number")
    @Expose
    private String servicePhoneNumber;
    @SerializedName("service_type")
    @Expose
    private String serviceType;
    @SerializedName("service_image")
    @Expose
    private ArrayList<String> serviceImage = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(String propertyId) {
        this.propertyId = propertyId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getServiceAddress() {
        return serviceAddress;
    }

    public void setServiceAddress(String serviceAddress) {
        this.serviceAddress = serviceAddress;
    }

    public String getServiceWebsite() {
        return serviceWebsite;
    }

    public void setServiceWebsite(String serviceWebsite) {
        this.serviceWebsite = serviceWebsite;
    }

    public String getServiceEmail() {
        return serviceEmail;
    }

    public void setServiceEmail(String serviceEmail) {
        this.serviceEmail = serviceEmail;
    }

    public String getServiceFacebook() {
        return serviceFacebook;
    }

    public void setServiceFacebook(String serviceFacebook) {
        this.serviceFacebook = serviceFacebook;
    }

    public String getServiceWechat() {
        return serviceWechat;
    }

    public void setServiceWechat(String serviceWechat) {
        this.serviceWechat = serviceWechat;
    }

    public String getServiceTelegram() {
        return serviceTelegram;
    }

    public void setServiceTelegram(String serviceTelegram) {
        this.serviceTelegram = serviceTelegram;
    }

    public String getServiceWhatsapp() {
        return serviceWhatsapp;
    }

    public void setServiceWhatsapp(String serviceWhatsapp) {
        this.serviceWhatsapp = serviceWhatsapp;
    }

    public String getServiceLogo() {
        return serviceLogo;
    }

    public void setServiceLogo(String serviceLogo) {
        this.serviceLogo = serviceLogo;
    }

    public String getServiceDesc() {
        return serviceDesc;
    }

    public void setServiceDesc(String serviceDesc) {
        this.serviceDesc = serviceDesc;
    }

    public String getServicePhoneNumber() {
        return servicePhoneNumber;
    }

    public void setServicePhoneNumber(String servicePhoneNumber) {
        this.servicePhoneNumber = servicePhoneNumber;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public ArrayList<String> getServiceImage() {
        return serviceImage;
    }

    public void setServiceImage(ArrayList<String> serviceImage) {
        this.serviceImage = serviceImage;
    }
}

