package com.eazy.daikou.model.home;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class HisEmergencyModel implements Serializable {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("property_id_fk")
    @Expose
    private String propertyIdFk;
    @SerializedName("unit_no")
    @Expose
    private String unitNo;
    @SerializedName("receiver_user_id")
    @Expose
    private String receiverUserIdFk;
    @SerializedName("sender_user_id")
    @Expose
    private String senderUserIdFk;
    @SerializedName("department_id_fk")
    @Expose
    private String departmentIdFk;
    @SerializedName("sender_user_name")
    @Expose
    private String senderFullName;
    @SerializedName("receiver_user_name")
    @Expose
    private String receiverFullName;
    @SerializedName("coord_lat")
    @Expose
    private String coordLat;
    @SerializedName("coord_long")
    @Expose
    private String coordLong;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("update_status_dt")
    @Expose
    private String updateStatusDt;
    @SerializedName("created_dt")
    @Expose
    private String createdDt;
    @SerializedName("property_name")
    @Expose
    private String propertyName;
    @SerializedName("emergency_type_of_user")
    @Expose
    private String emergencyType;
    @SerializedName("receiver_position_name")
    @Expose
    private String receiver_position;
    @SerializedName("receiver_user_phone")
    @Expose
    private String receiver_phone_number;
    @SerializedName("sender_user_phone")
    @Expose
    private String sender_user_phone;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPropertyIdFk() {
        return propertyIdFk;
    }

    public void setPropertyIdFk(String propertyIdFk) {
        this.propertyIdFk = propertyIdFk;
    }

    public String getUnitNo() {
        return unitNo;
    }

    public void setUnitNo(String unitNo) {
        this.unitNo = unitNo;
    }

    public String getReceiverUserIdFk() {
        return receiverUserIdFk;
    }

    public void setReceiverUserIdFk(String receiverUserIdFk) {
        this.receiverUserIdFk = receiverUserIdFk;
    }

    public String getSenderUserIdFk() {
        return senderUserIdFk;
    }

    public void setSenderUserIdFk(String senderUserIdFk) {
        this.senderUserIdFk = senderUserIdFk;
    }

    public String getCoordLat() {
        return coordLat;
    }

    public void setCoordLat(String coordLat) {
        this.coordLat = coordLat;
    }

    public String getCoordLong() {
        return coordLong;
    }

    public void setCoordLong(String coordLong) {
        this.coordLong = coordLong;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUpdateStatusDt() {
        return updateStatusDt;
    }

    public void setUpdateStatusDt(String updateStatusDt) {
        this.updateStatusDt = updateStatusDt;
    }

    public String getCreatedDt() {
        return createdDt;
    }

    public void setCreatedDt(String createdDt) {
        this.createdDt = createdDt;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    public String getDepartmentIdFk() {
        return departmentIdFk;
    }

    public void setDepartmentIdFk(String departmentIdFk) {
        this.departmentIdFk = departmentIdFk;
    }

    public String getSenderFullName() {
        return senderFullName;
    }

    public void setSenderFullName(String senderFullName) {
        this.senderFullName = senderFullName;
    }

    public String getReceiverFullName() {
        return receiverFullName;
    }

    public void setReceiverFullName(String receiverFullName) {
        this.receiverFullName = receiverFullName;
    }

    public String getEmergencyType() {
        return emergencyType;
    }

    public void setEmergencyType(String emergencyType) {
        this.emergencyType = emergencyType;
    }

    public String getReceiver_position() {
        return receiver_position;
    }

    public void setReceiver_position(String receiver_position) {
        this.receiver_position = receiver_position;
    }

    public String getReceiver_phone_number() {
        return receiver_phone_number;
    }

    public void setReceiver_phone_number(String receiver_phone_number) {
        this.receiver_phone_number = receiver_phone_number;
    }

    public String getSender_user_phone() {
        return sender_user_phone;
    }

    public void setSender_user_phone(String sender_user_phone) {
        this.sender_user_phone = sender_user_phone;
    }
}
