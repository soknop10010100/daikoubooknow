package com.eazy.daikou.model.hr

import java.io.Serializable

class Experience : Serializable {

    val name: String? = null
    val companyName: String? = null
    val department: String? = null
    val position: String? = null
    val start_date: String? = null
    val leave_date: String? = null
    val salary: String? = null
    val last_salary: String? = null
    val director_manager_name: String? = null
    val director_manager_contact: String? = null
    val reason: String? = null
    val remark: String? = null
    val actions: String? = null



}