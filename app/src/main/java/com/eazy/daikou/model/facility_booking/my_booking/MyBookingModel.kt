package com.eazy.daikou.model.facility_booking.my_booking

import java.io.Serializable

class MyBookingModel : Serializable {
    var booking_id : String? = null
    var date_time : String? = null
    var status : String? = null
    var amount_to_pay : String? = null
    var payment_status : String? = null
    var created_at : String? = null

}