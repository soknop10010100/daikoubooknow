package com.eazy.daikou.model.booking_hotel

import java.io.Serializable

open class HotelTravelTalkModel : Serializable{
    var id : String? = null
    var title : String? = null
    var description : String? = null
    var created_at : String? = null
    var total_likes : String? = null
    var total_replies : String? = null
    var total_shares : String? = null
    var category : Category? = null
    var images : ArrayList<String> = ArrayList()
    var created_by : CreateBy? = null
    var is_user_liked : Boolean = false
    var share_link : String? = null
}

class Category : Serializable{
    var id : String? = null
    var name : String? = null
}

class CreateBy : Serializable{
    var id : String? = null
    var name : String? = null
    var image : String? = null
    var email : String? = null
    var total_posts: String? = null
    var phone : String? = null
    var total_comments: String? = null
    var total_likes: String? = null
}

// Travel Talk Detail Reply
class TravelTalkReplyDetail : Serializable {
    // Info Thread Object
    var thread : HotelTravelTalkModel? = null
    var replies : ArrayList<TravelTalkReplyDetail> = ArrayList()        // Info Main Object Replies

    // Info Sub Object Replies
    var id : String? = null
    var description : String? = null
    var created_at : String? = null
    var total_likes : String? = null
    var is_user_liked : Boolean = false
    var image : String? = null
    var user : UserH? = null
}
