package com.eazy.daikou.model.home_page.home_project

import com.google.gson.annotations.SerializedName

data class NearByPropertyModel(
    @SerializedName("success" ) var success : Boolean? = null,
    @SerializedName("msg"     ) var msg     : String?  = null,
    @SerializedName("data"    ) var data    : Data    = Data()
)

data class BranchTypes (

    @SerializedName("key"   ) var key   : String? = null,
    @SerializedName("value" ) var value : String? = null,
    @SerializedName("icon"  ) var icon  : String? = null

)

data class Nearby (

    @SerializedName("id"       ) var id       : String? = null,
    @SerializedName("name"     ) var name     : String? = null,
    @SerializedName("logo"     ) var logo     : String? = null,
    @SerializedName("city"     ) var city     : String? = null,
    @SerializedName("address"  ) var address  : String? = null,
    @SerializedName("distance" ) var distance : String? = null,
    @SerializedName("image"    ) var image    : String? = null

)

data class Featured (

    @SerializedName("id"      ) var id      : String? = null,
    @SerializedName("name"    ) var name    : String? = null,
    @SerializedName("logo"    ) var logo    : String? = null,
    @SerializedName("city"    ) var city    : String? = null,
    @SerializedName("address" ) var address : String? = null,
    @SerializedName("image"   ) var image   : String? = null

)

data class AllBranches (

    @SerializedName("id"      ) var id      : String? = null,
    @SerializedName("name"    ) var name    : String? = null,
    @SerializedName("logo"    ) var logo    : String? = null,
    @SerializedName("city"    ) var city    : String? = null,
    @SerializedName("address" ) var address : String? = null,
    @SerializedName("image"   ) var image   : String? = null

)

data class Data (
    @SerializedName("image_sliders" ) var imageSliders : ArrayList<String>      = arrayListOf(),
    @SerializedName("branch_types"  ) var branchTypes  : ArrayList<BranchTypes> = arrayListOf(),
    @SerializedName("nearby"        ) var nearby       : ArrayList<Nearby>      = arrayListOf(),
    @SerializedName("featured"      ) var featured     : ArrayList<Featured>    = arrayListOf(),
    @SerializedName("all_branches"  ) var allBranches  : ArrayList<AllBranches> = arrayListOf()

)
