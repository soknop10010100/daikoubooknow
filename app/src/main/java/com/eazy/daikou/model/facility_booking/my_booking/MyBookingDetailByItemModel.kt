package com.eazy.daikou.model.facility_booking.my_booking

import com.eazy.daikou.model.facility_booking.PropertyVenueModel
import java.io.Serializable

class MyBookingDetailByItemModel : Serializable{
    var booking_date : String? = null
    var from_time : String? = null
    var to_time : String? = null
    var item_name : String? = null
    var price_id : String? = null
    var option_name : String? = null
    var quantity : String? = null
    var type_price : String? = null

    var unit_price : String? = null
    var amount : String? = null
    var tax_vat_percent : String? = null
    var tax_vat_amount : String? = null
    var tax_relax_percent : String? = null
    var total_tax_amount : String? = null
    var tax_relax_amount : String? = null
    var tax_show_amount : String? = null
    var total_amount : String? = null

    var coupon_id : String? = null
    var created_date : String? = null
    var property_info : PropertyVenueModel? = null
    var space_info : PropertyVenueModel? = null
    var available_day = ArrayList<String>()

}