package com.eazy.daikou.model.parking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class AllImage implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("file_path")
    @Expose
    private String filePath;
    @SerializedName("uploader_user_id")
    @Expose
    private String uploaderUserId;
    @SerializedName("created_date")
    @Expose
    private String createdDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getUploaderUserId() {
        return uploaderUserId;
    }

    public void setUploaderUserId(String uploaderUserId) {
        this.uploaderUserId = uploaderUserId;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }
}
