package com.eazy.daikou.model.home;

import android.graphics.drawable.Drawable;

import java.io.Serializable;

public class HomeViewModel implements Serializable {

    private transient String icon;
    private transient String name;
    private final transient Drawable drawable;
    private final transient String action;
    private transient String category_item;
    private int color;

    public HomeViewModel(String action,Drawable icon, String name) {
        this.action = action;
        this.drawable = icon;
        this.name = name;
    }

    public HomeViewModel(String category_item, String action, Drawable drawable, String name) {
        this.action = action;
        this.drawable = drawable;
        this.name = name;
        this.category_item = category_item;
    }

    public HomeViewModel(String category_item, String action, Drawable drawable, String name, int color) {
        this.action = action;
        this.drawable = drawable;
        this.name = name;
        this.category_item = category_item;
        this.color = color;
    }

    public String getAction() {
        return action;
    }

    public Drawable getDrawable() {
        return drawable;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory_item() {
        return category_item;
    }

    public int getColor() {
        return color;
    }
}
