package com.eazy.daikou.model.inspection;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class InfoInspectionModel implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("order_no")
    @Expose
    private String orderNo;
    @SerializedName("conduct_dt")
    @Expose
    private String conductDt;
    @SerializedName("created_dt")
    @Expose
    private String createdDt;
    @SerializedName("unit_id")
    @Expose
    private String unitId;
    @SerializedName("unit_no")
    @Expose
    private String unitNo;
    @SerializedName("inspector_type")
    @Expose
    private String inspectorType;
    @SerializedName("inspection_name")
    @Expose
    private String inspectionName;
    @SerializedName("conduct_time")
    @Expose
    private String conductTime;
    @SerializedName("conduct_duration")
    @Expose
    private String conductDuration;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("author_user_info")
    @Expose
    private Author_ClerkUserInfo authorUserInfo;
    @SerializedName("clerk_user_info")
    @Expose
    private Author_ClerkUserInfo clerkUserInfo;
    @SerializedName("inspector_info")
    @Expose
    private Author_ClerkUserInfo inspectorInfo;
    @SerializedName("inspection_type_id")
    @Expose
    private String inspectionTypeId;
    @SerializedName("inspection_template_id")
    @Expose
    private String inspectionTemplateId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreatedDt() {
        return createdDt;
    }

    public void setCreatedDt(String createdDt) {
        this.createdDt = createdDt;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String  getUnitNo() {
        return unitNo;
    }

    public void setUnitNo(String unitNo) {
        this.unitNo = unitNo;
    }

    public String getInspectorType() {
        return inspectorType;
    }

    public void setInspectorType(String inspectorType) {
        this.inspectorType = inspectorType;
    }

    public String getInspectionName() {
        return inspectionName;
    }

    public void setInspectionName(String inspectionName) {
        this.inspectionName = inspectionName;
    }

    public String getConductTime() {
        return conductTime;
    }

    public void setConductTime(String conductTime) {
        this.conductTime = conductTime;
    }

    public String getConductDuration() {
        return conductDuration;
    }

    public void setConductDuration(String conductDuration) {
        this.conductDuration = conductDuration;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Author_ClerkUserInfo getAuthorUserInfo() {
        return authorUserInfo;
    }

    public void setAuthorUserInfo(Author_ClerkUserInfo authorUserInfo) {
        this.authorUserInfo = authorUserInfo;
    }

    public Author_ClerkUserInfo getClerkUserInfo() {
        return clerkUserInfo;
    }

    public void setClerkUserInfo(Author_ClerkUserInfo clerkUserInfo) {
        this.clerkUserInfo = clerkUserInfo;
    }

    public Author_ClerkUserInfo getInspectorInfo() {
        return inspectorInfo;
    }

    public void setInspectorInfo(Author_ClerkUserInfo inspectorInfo) {
        this.inspectorInfo = inspectorInfo;
    }

    public String getInspectionTypeId() {
        return inspectionTypeId;
    }

    public void setInspectionTypeId(String inspectionTypeId) {
        this.inspectionTypeId = inspectionTypeId;
    }

    public String getInspectionTemplateId() {
        return inspectionTemplateId;
    }

    public void setInspectionTemplateId(String inspectionTemplateId) {
        this.inspectionTemplateId = inspectionTemplateId;
    }

    public String getConductDt() {
        return conductDt;
    }

    public void setConductDt(String conductDt) {
        this.conductDt = conductDt;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }
}
