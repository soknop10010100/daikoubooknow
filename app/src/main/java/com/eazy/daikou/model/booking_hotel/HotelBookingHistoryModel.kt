package com.eazy.daikou.model.booking_hotel

import com.eazy.daikou.model.home.SubItemHomeModel
import java.io.Serializable

//History Hotel Booking
class HotelBookingHistoryModel : Serializable{
    var id : String?= null
    var code : String?= null
    var hotel_id : String?= null
    var hotel_name : String?= null
    var start_date : String?= null
    var end_date : String?= null
    var created_at : String?= null
    var status : String?= null
    var total : String?= null
    var paid : String?= null
    var payment_method : String? = null
    var category : String? = null
}

//History Hotel Detail
class HotelBookingHistoryDetailModel : Serializable {
    var booking_id: String? = null
    var code : String? = null
    var gateway: String? = null
    var hotel_id: String? = null
    var hotel_name : String? = null
    var start_date: String? = null
    var end_date: String? = null
    var total_price: String? = null
    var total_guests: String? = null
    var status: String? = null
    var email: String? = null
    var last_name: String? = null
    var first_name: String? = null
    var category : String? = null

    var phone: String? = null
    var city: String? = null
    var state: String? = null
    var country: String? = null
    var customer_notes: String? = null
    var created_at: String? = null
    var total_price_display: String? = null
    var total_night : String? = null
    var hotel_telephone : String? = null
    var hotel_email : String? = null
    var room_booking_history: ArrayList<RoomBookingHistory> = ArrayList()
}

class RoomBookingHistory : Serializable{
    var price : String? = null
    var price_display : String? = null
    var number : String?= null
    var image : String? = null
    var room_name : String?= null
}

//My Hotel For Vendor
class HotelMyVendorModel : Serializable{
    var hotel_id : String? = null
    var hotel_name : String? = null
    var location_id : String?= null
    var price : String? = null
    var updated_at : String?= null
    var location_name : String? = null
    var status : String? = null
    var price_display : String?= null
    var image : String? = null
    var is_wishlist : Boolean = false
}

//My Room Of Hotel For Vendor
class HotelMyRoomOfHotelVendorModel : Serializable{
    var room_id : String? = null
    var room_name : String? = null
    var number : String?= null
    var price : String? = null
    var updated_at : String?= null
    var status : String? = null
    var price_display : String?= null
    var image : String? = null
}

//Dashboard Hotel
class HotelDashboardModel : Serializable{
    var total_pending : String? = null
    var total_earning : String? = null
    var total_booking : String? = null
    var total_bookable_service : String? = null
}

//Nearby location
class NearbyHotelLocationModel : Serializable{
    var id : String? = null
    var name : String? = null
    var coord_lat : String? = null
    var coord_long : String? = null
    var star_rate : String? = null
    var image : String? = null
    var original_price_display : String? = null
    var is_wishlist : Boolean = false
    var price_display : String? = null
    var distance : String? = null
    var isClick : Boolean = false
    var location : LocationModel? = null
}

//Detail location
class HotelLocationDetailModel : Serializable{
    var id : String? = null
    var name : String? = null
    var description : String? = null
}
