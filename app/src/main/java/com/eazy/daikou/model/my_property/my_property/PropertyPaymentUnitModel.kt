package com.eazy.daikou.model.my_property.my_property

import java.io.Serializable

class PropertyPaymentUnitModel : Serializable{
    var id: String? = null
    var invoice_no: String? = null
    var date_pay: String? = null
    var percentage: String? = null
    var amount: String? = null
    var interest : String? = null
    var interest_amount : String? = null
    var payment_status : String? = null
    var payment : String? = null
    var stage : String? = null
    var total_term : String? = null
    var created_at : String? = null
    var url : String? = null
    var payment_amount : String? = null
    var kess_payment_link : String? = null
    var invoice_receipt_url : String? = null
}