package com.eazy.daikou.model.hr

import java.io.Serializable

class NoticeBoardHrModel : Serializable {

    var id: String? = null
    var name: String? = null
    var notice_title: String? = null
    var created_at: String? = null
    var date: String? = null
    var expired_at: String? = null
    var description: String? = null
    var status: String? = null

}