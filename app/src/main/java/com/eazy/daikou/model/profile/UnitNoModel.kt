package com.eazy.daikou.model.profile

import java.io.Serializable

class UnitNoModel : Serializable {
    var id : String? = null
    var name : String? = null
    var property : String? = null
    var isClick : Boolean = false
}