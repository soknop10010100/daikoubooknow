package com.eazy.daikou.model.my_document;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PropertyDocument implements Serializable {
    @SerializedName("property_id")
    @Expose
    private String propertyId;

    @SerializedName("property_name")
    @Expose
    private String propertyName;

    private boolean isClick;

    public PropertyDocument(String propertyId, String propertyName, boolean isClick) {
        this.propertyId = propertyId;
        this.propertyName = propertyName;
        this.isClick = isClick;
    }

    public String getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(String propertyId) {
        this.propertyId = propertyId;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    public boolean getIsClick() {
        return isClick;
    }

    public void setIsClick(boolean isClick) {
        this.isClick = isClick;
    }
}
