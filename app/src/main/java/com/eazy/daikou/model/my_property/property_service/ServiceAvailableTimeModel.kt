package com.eazy.daikou.model.my_property.property_service

import com.eazy.daikou.base.BaseModel

class ServiceAvailableTimeModel(
    var name : String?,
    var numberOfTime : String?,
    var timeAvailableList : ArrayList<String> = ArrayList()
) : BaseModel()