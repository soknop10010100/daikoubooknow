package com.eazy.daikou.model.hr

import java.io.Serializable

class LeaveManagement : Serializable {
    val id : String = ""
    val name : String? = null
    val leave_category_id : String = ""
    val leave_category : String = ""
    val start_date : String = ""
    val end_date : String = ""
    val reason : String = ""
    val period : String = ""
    val status : String = ""
    val created_at : String = ""
}

