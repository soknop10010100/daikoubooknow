package com.eazy.daikou.model.hr

import java.io.Serializable

class ResignationHrModel : Serializable {

    var id : String? = null
    var resigner_name : String? = null
    var designation : String? = null
    var last_date : String? = null
    var contact : String? = null
    var type : String? = null
    var reason : String? = null
    var status : String? = null
    var user_id : String? = null
    var created_at: String? = null
    var my_list : String? = null





}