package com.eazy.daikou.model.work_order;

import com.eazy.daikou.model.inspection.SubItemTemplateAPIModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ParentTaskWorkOrderModel implements Serializable {
    @SerializedName("property_name")
    @Expose
    private String propertyName;
    @SerializedName("template_name")
    @Expose
    private String templateName;
    @SerializedName("inspection_id")
    @Expose
    private String inspectionId;
    @SerializedName("inspection_room_id")
    @Expose
    private String inspectionRoomId;
    @SerializedName("option_type")
    @Expose
    private String optionType;
    @SerializedName("inspection_title")
    @Expose
    private String inspectionTitle;
    @SerializedName("unit_no")
    @Expose
    private String unitNo;
    @SerializedName("inspection_no")
    @Expose
    private String inspectionNo;
    @SerializedName("inspection_sub_title")
    @Expose
    private SubItemTemplateAPIModel inspectionSubTitle;

    public String getPropertyName() {
        return propertyName;
    }

    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public String getInspectionId() {
        return inspectionId;
    }

    public void setInspectionId(String inspectionId) {
        this.inspectionId = inspectionId;
    }

    public String getInspectionRoomId() {
        return inspectionRoomId;
    }

    public void setInspectionRoomId(String inspectionRoomId) {
        this.inspectionRoomId = inspectionRoomId;
    }

    public String getOptionType() {
        return optionType;
    }

    public void setOptionType(String optionType) {
        this.optionType = optionType;
    }

    public String getInspectionTitle() {
        return inspectionTitle;
    }

    public void setInspectionTitle(String inspectionTitle) {
        this.inspectionTitle = inspectionTitle;
    }

    public String getUnitNo() {
        return unitNo;
    }

    public void setUnitNo(String unitNo) {
        this.unitNo = unitNo;
    }

    public String getInspectionNo() {
        return inspectionNo;
    }

    public void setInspectionNo(String inspectionNo) {
        this.inspectionNo = inspectionNo;
    }

    public SubItemTemplateAPIModel getInspectionSubTitle() {
        return inspectionSubTitle;
    }

    public void setInspectionSubTitle(SubItemTemplateAPIModel inspectionSubTitle) {
        this.inspectionSubTitle = inspectionSubTitle;
    }

}
