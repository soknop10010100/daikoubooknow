package com.eazy.daikou.model.utillity_tracking.model;

import java.io.Serializable;
import java.util.List;

public class FloorUtilNoModel implements Serializable {

    private String floorName;
    private String floorNoName;
    private List<UtilNoCommonAreaModel> utilNoModelList;
    private List<UtilNoCommonAreaModel> waterCommonList;
    private List<UtilNoCommonAreaModel> electricityCommonList;

    public List<UtilNoCommonAreaModel> getWaterCommonList() {
        return waterCommonList;
    }

    public void setWaterCommonList(List<UtilNoCommonAreaModel> waterCommonList) {
        this.waterCommonList = waterCommonList;
    }

    public List<UtilNoCommonAreaModel> getElectricityCommonList() {
        return electricityCommonList;
    }

    public void setElectricityCommonList(List<UtilNoCommonAreaModel> electricityCommonList) {
        this.electricityCommonList = electricityCommonList;
    }

    public List<UtilNoCommonAreaModel> getUtilNoModelList() {
        return utilNoModelList;
    }

    public void setUtilNoModelList(List<UtilNoCommonAreaModel> utilNoModelList) {
        this.utilNoModelList = utilNoModelList;
    }

    public String getFloorName() {
        return floorName;
    }

    public void setFloorName(String floorName) {
        this.floorName = floorName;
    }

    public String getFloorNoName() {
        return floorNoName;
    }

    public void setFloorNoName(String floorNoName) {
        this.floorNoName = floorNoName;
    }

}
