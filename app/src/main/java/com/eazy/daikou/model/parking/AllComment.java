package com.eazy.daikou.model.parking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class AllComment implements Serializable {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("comment")
    @Expose
    private String comment;
    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("commenter_user_name")
    @Expose
    private String commenterUserName;

    @SerializedName("commenter_user_id")
    @Expose
    private String commenterUserId;
    @SerializedName("created_date")
    @Expose
    private String createdDate;
    @SerializedName("all_images")
    @Expose
    private List<AllImage> allImages = null;

    public String getCommenterUserName() {
        return commenterUserName;
    }

    public void setCommenterUserName(String commenterUserName) {
        this.commenterUserName = commenterUserName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getCommenterUserId() {
        return commenterUserId;
    }

    public void setCommenterUserId(String commenterUserId) {
        this.commenterUserId = commenterUserId;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public List<AllImage> getAllImages() {
        return allImages;
    }

    public void setAllImages(List<AllImage> allImages) {
        this.allImages = allImages;
    }


}