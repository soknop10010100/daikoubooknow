package com.eazy.daikou.model.work_order;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class AssignWorkOrderModel implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("room_item_id")
    @Expose
    private String roomItemId;
    @SerializedName("work_order_no")
    @Expose
    private String workOrderNo;
    @SerializedName("issue_date")
    @Expose
    private String issueDate;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("item_no")
    @Expose
    private String itemNo;
    @SerializedName("work_order_type")
    @Expose
    private String workOrderType;
    @SerializedName("done_percent")
    @Expose
    private String donePercent;
    @SerializedName("start_date")
    @Expose
    private String startDate;
    @SerializedName("end_date")
    @Expose
    private String endDate;
    @SerializedName("department_name")
    @Expose
    private String departmentName;
    @SerializedName("report_type_no")
    @Expose
    private String reportTypeNo;
    @SerializedName("parent_task")
    @Expose
    private String parentTask;
    @SerializedName("assignee_user")
    @Expose
    private String assigneeUser;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRoomItemId() {
        return roomItemId;
    }

    public void setRoomItemId(String roomItemId) {
        this.roomItemId = roomItemId;
    }

    public String getWorkOrderNo() {
        return workOrderNo;
    }

    public void setWorkOrderNo(String workOrderNo) {
        this.workOrderNo = workOrderNo;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getItemNo() {
        return itemNo;
    }

    public void setItemNo(String itemNo) {
        this.itemNo = itemNo;
    }

    public String getWorkOrderType() {
        return workOrderType;
    }

    public void setWorkOrderType(String workOrderType) {
        this.workOrderType = workOrderType;
    }

    public String getDonePercent() {
        return donePercent;
    }

    public void setDonePercent(String donePercent) {
        this.donePercent = donePercent;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getReportTypeNo() {
        return reportTypeNo;
    }

    public void setReportTypeNo(String reportTypeNo) {
        this.reportTypeNo = reportTypeNo;
    }

    public String getParentTask() {
        return parentTask;
    }

    public void setParentTask(String parentTask) {
        this.parentTask = parentTask;
    }

    public String getAssigneeUser() {
        return assigneeUser;
    }

    public void setAssigneeUser(String assigneeUser) {
        this.assigneeUser = assigneeUser;
    }


}
