package com.eazy.daikou.model.front_desk

import java.io.Serializable

class SearchFrontDeskModel : Serializable{
    var id : String? = null
    var name : String? = null
    var image : String? = null
    var phone : String? = null
    var status : String? = null
    var user_id : String? = null
    var created_at : String? = null
}
