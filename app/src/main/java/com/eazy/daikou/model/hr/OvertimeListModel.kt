package com.eazy.daikou.model.hr

import java.io.Serializable

class OvertimeListModel : Serializable {
    var id: String? = null
    var date: String? = null
    var begin_time: String? = null
    var end_time: String? = null
    var user_business_key: String? = null
    var user_name: String? = null
    var description: String? = null
    var status: String? = null
    var created_at: String? = null
    var hour: String? = null
    var shift_name: String? = null
    var overtime_category: String? = null
    var approved_by: String? = null
    var approver_name: String? = null
    var approved_by_name: String? = null
    var remark:String? = null
}