package com.eazy.daikou.model.my_property.service_property_employee.common_area

import java.io.Serializable

class AdminListCleaning : Serializable {

    var space_name = ""
    var zone_name= ""
    var floor_no = ""
    var start_date= ""
    var end_date= ""
    var start_time= ""
    var end_time= ""
    var status= ""
    var work_space_id =""
    var unit_no =""
    var cleaning_schedule_id =""
    var unit_id =""
    var cleaning_category =""
    var cleaning_service_history_id = ""
    var created_date =""
}