package com.eazy.daikou.model.data_record

import java.io.Serializable


class DetailQuotationModel : Serializable {
    var id : String? = null
    var price : String? = null
    var sale_price : String? = null
    var voucher_amount : String? = null
    var email : String? = null
    var phone : String? = null
    var priority : String? = null
    var expire_date : String? = null
    var created_at : String? = null
    var no : String? = null
    var status : String? = null
    var discount : String? = null
    var channel_name : String? = null
    var supporting_team : String? = null
    var payment_term_type : String? = null
    var option : String? = null
    var total_payment_amount : String? = null
    var total_interest_amount : String? = null
    var total_amount : String? = null
    var user : User? = null
    var unit : UnitQuotation? = null
    var account : Account? = null
    var payment_schedules : ArrayList<PaymentSchedule> = ArrayList()
    var all_status : ArrayList<AllStatus> = ArrayList()
    var agency_name : String? = null
    var broker_name : String? = null
}

class User : Serializable{
    var id : String? = null
    var name : String? = null
}


class UnitQuotation : Serializable{
    var id : String? = null
    var name : String? = null
    var floor_no : String? = null
    var price : String? = null
    var sale_price : String? = null
    var direction : String? = null
    var unit_stair : String? = null
    var unit_position : String? = null
    var street_id_fk : String? = null
    var voucher_amount : String? = null
    var expire_date : String? = null
    var qr_code : String? = null
    var type : TypeQuotation? = null
}

class  TypeQuotation (): Serializable{
    var id : String? = null
    var name : String? = null
    var from_floor : String? = null
    var category : String? = null
    var total_bedroom : String? = null
    var total_bathroom : String? = null
    var total_living_room : String? = null
    var total_maid_room : String? = null
    var total_area_sqm : String? = null
    var total_private_area : String? = null
    var total_common_area : String? = null

}

class PaymentSchedule : Serializable{
    var id : String? = null
    var after_previous_display : String? = null
    var stage : String? = null
    var percentage : String? = null
    var amount : String? = null
    var interest : String? = null
    var created_at : String? = null
    var total_term : String? = null
    var payment_amount_number : String? = null
    var interest_amount_number : String? = null
    var amount_number : String? = null
    var interest_amount : String? = null
    var payment_amount : String? = null
    var payment_status: String? = null
    var invoice_receipt_url : String? = null
    var date_pay : String? = null
}
