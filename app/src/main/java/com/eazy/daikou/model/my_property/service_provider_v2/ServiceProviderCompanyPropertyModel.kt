package com.eazy.daikou.model.my_property.service_provider_v2

import java.io.Serializable

class ServiceProviderCompanyPropertyModel : Serializable{

    var id: String? = null
    var account_name: String? = null
    var contact: String? = null
    var account_address: String? = null
    var image: String? = null
    var website: String? = null
}