package com.eazy.daikou.model.complaint

import com.eazy.daikou.model.CustomImageModel
import com.eazy.daikou.model.booking_hotel.UserH
import java.io.Serializable

class ComplaintListModel : Serializable {
    var id: String? = null
    var subject: String? = null
    var description: String? = null
    var created_at: String? = null
    var object_type: String? = null
    var object_info : AccountUnitModel?= null
    var status: String? = null
    var post_type: String? = null
    var total_comments: String? = null
    var total_contributor: String? = null

    var unit: AccountUnitModel? = null
    var account: AccountUnitModel? = null
    var files: ArrayList<CustomImageModel> = ArrayList()
    var uploader_user: UserH? = null
    var comments : ArrayList<Comment> = ArrayList()

    // Front Desk
    var unit_no : String ?= null
    var user_name : String? = null
    var user_phone: String? = null
    var delivery_name: String? = null
    var delivery_phone: String? = null

}

class Comment : Serializable {
    var id: String? = null
    var comment: String? = null
    var uploader_user : UserH? = null
    var created_at : String?= null
    var image : String?= null
    var voice : String?= null
    var actionDisplay : String = ""
}

class AccountUnitModel : Serializable {
    var id: String? = null
    var name: String? = null
}