package com.eazy.daikou.model.hr

import java.io.Serializable

class LeaveTypeCategory :Serializable{

    var leave_categories = ArrayList<LeaveCategory>()
    var approved_by = ArrayList<AssignEmployee>()
}

class LeaveCategory : Serializable {
    var id = ""
    var leave_category = ""
    var isClick = false
}

class AssignEmployee : Serializable{
    val id =  ""
    val name = ""
    var isClick = false
}