package com.eazy.daikou.model.facility_booking;

public class SelectNumberModel {

    private String numberString;
    private boolean isClick;

    public String getNumberString() {
        return numberString;
    }

    public void setNumberString(String numberString) {
        this.numberString = numberString;
    }

    public boolean isClick() {
        return isClick;
    }

    public void setClick(boolean click) {
        isClick = click;
    }
}
