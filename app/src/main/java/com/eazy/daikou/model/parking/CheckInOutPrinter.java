package com.eazy.daikou.model.parking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CheckInOutPrinter implements Serializable {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("ticket_no")
    @Expose
    private String ticketNo;
    @SerializedName("order_no")
    @Expose
    private String orderNo;

    @SerializedName("property_id")
    @Expose
    private String propertyId;
    @SerializedName("property_name")
    @Expose
    private String propertyName;
    @SerializedName("parking_no")
    @Expose
    private Object parkingNo;
    @SerializedName("parking_lot_type")
    @Expose
    private Object parkingLotType;
    @SerializedName("qr_code_data")
    @Expose
    private String qrCodeData;
    @SerializedName("close_hour")
    @Expose
    private Object closeHour;
    @SerializedName("time_in")
    @Expose
    private String timeIn;
    @SerializedName("time_out")
    @Expose
    private Object timeOut;
    @SerializedName("car_image")
    @Expose
    private String carImage;
    @SerializedName("time_duration_in_minute")
    @Expose
    private Integer timeDurationInMinute;
    @SerializedName("parking_fee")
    @Expose
    private Object parkingFee;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("scan_action")
    @Expose
    private String scanAction;

    @SerializedName("payment_method")
    @Expose
    private String paymentMethod;

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTicketNo() {
        return ticketNo;
    }

    public void setTicketNo(String ticketNo) {
        this.ticketNo = ticketNo;
    }

    public String getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(String propertyId) {
        this.propertyId = propertyId;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    public Object getParkingNo() {
        return parkingNo;
    }

    public void setParkingNo(Object parkingNo) {
        this.parkingNo = parkingNo;
    }

    public Object getParkingLotType() {
        return parkingLotType;
    }

    public void setParkingLotType(Object parkingLotType) {
        this.parkingLotType = parkingLotType;
    }

    public String getQrCodeData() {
        return qrCodeData;
    }

    public void setQrCodeData(String qrCodeData) {
        this.qrCodeData = qrCodeData;
    }

    public Object getCloseHour() {
        return closeHour;
    }

    public void setCloseHour(Object closeHour) {
        this.closeHour = closeHour;
    }

    public String getTimeIn() {
        return timeIn;
    }

    public void setTimeIn(String timeIn) {
        this.timeIn = timeIn;
    }

    public Object getTimeOut() {
        return timeOut;
    }

    public void setTimeOut(Object timeOut) {
        this.timeOut = timeOut;
    }

    public String getCarImage() {
        return carImage;
    }

    public void setCarImage(String carImage) {
        this.carImage = carImage;
    }

    public Integer getTimeDurationInMinute() {
        return timeDurationInMinute;
    }

    public void setTimeDurationInMinute(Integer timeDurationInMinute) {
        this.timeDurationInMinute = timeDurationInMinute;
    }

    public Object getParkingFee() {
        return parkingFee;
    }

    public void setParkingFee(Object parkingFee) {
        this.parkingFee = parkingFee;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getScanAction() {
        return scanAction;
    }

    public void setScanAction(String scanAction) {
        this.scanAction = scanAction;
    }

}

