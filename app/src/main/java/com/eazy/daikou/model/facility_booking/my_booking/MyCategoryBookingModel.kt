package com.eazy.daikou.model.facility_booking.my_booking

import java.io.Serializable

class MyCategoryBookingModel : Serializable {
    var booking_id : String? = null
    var date_time : String? = null
    var payment_kess_link : String? = null
    var booking_items : ArrayList<MyBookingItemsModel> = ArrayList()
}