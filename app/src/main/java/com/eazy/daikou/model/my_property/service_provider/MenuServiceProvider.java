package com.eazy.daikou.model.my_property.service_provider;

import com.eazy.daikou.model.my_property.Child;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class MenuServiceProvider implements Serializable {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("child")
    @Expose
    private ArrayList<Child> child;

    private boolean isClick;

    public MenuServiceProvider(String id, String name, ArrayList<Child> child, boolean isClick) {
        this.id = id;
        this.name = name;
        this.child = child;
        this.isClick = isClick;
    }

    public boolean isClick() {
        return isClick;
    }

    public void setClick(boolean click) {
        isClick = click;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Child> getChild() {
        return child;
    }

    public void setChild(ArrayList<Child> child) {
        this.child = child;
    }
}
