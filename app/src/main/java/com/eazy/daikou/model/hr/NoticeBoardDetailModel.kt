package com.eazy.daikou.model.hr

import java.io.Serializable

class NoticeBoardDetailModel : Serializable{
    var id : String? = null
    var created_by : String? = null
    var notice_title : String? = null
    var location : String? = null
    var created_at : String? = null
    var expired_at : String? = null
    var date : String? = null
    var description : String? = null
    var status : String? = null
    var file_upload = ArrayList<String>()

}