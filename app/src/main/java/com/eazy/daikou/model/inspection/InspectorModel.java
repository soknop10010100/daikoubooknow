package com.eazy.daikou.model.inspection;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class InspectorModel implements Serializable {
    @SerializedName("owner_rent_user_id")
    @Expose
    private String ownerRentUserId;
    @SerializedName("owner_rent_id")
    @Expose
    private String ownerRentId;
    @SerializedName("nationality")
    @Expose
    private String nationality;
    @SerializedName("user_name")
    @Expose
    private String userName;
    @SerializedName("user_phone_number")
    @Expose
    private String userPhoneNumber;
    private boolean isCheckInspector;

    public String getOwnerRentUserId() {
        return ownerRentUserId;
    }

    public void setOwnerRentUserId(String ownerRentUserId) {
        this.ownerRentUserId = ownerRentUserId;
    }

    public String getOwnerRentId() {
        return ownerRentId;
    }

    public void setOwnerRentId(String ownerRentId) {
        this.ownerRentId = ownerRentId;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPhoneNumber() {
        return userPhoneNumber;
    }

    public void setUserPhoneNumber(String userPhoneNumber) {
        this.userPhoneNumber = userPhoneNumber;
    }

    public boolean isCheckInspector() {
        return isCheckInspector;
    }

    public void setCheckInspector(boolean checkInspector) {
        isCheckInspector = checkInspector;
    }
}
