package com.eazy.daikou.model.my_property.service_property_employee

import com.eazy.daikou.model.my_property.cleaning.ActivityLogs
import com.eazy.daikou.model.my_property.cleaning.ImageInfo
import java.io.Serializable

class EmployeeDetailServiceProviderModel : Serializable {
    var assigner_info: AssignerInfo?= null
    var detail_info: DetailInfo? = null
    var activity_logs: ArrayList<ActivityLogs> = ArrayList()
    var image_info: ImageInfo? = null
}

class AssignerInfo : Serializable{
    var user_id: String? = null
    var user_name: String? = null
    var phone_number: String? = null
    var user_profile_image: String? = null
}

class DetailInfo : Serializable{
    var work_schedule_id: String? = null
    var work_schedule_employee_id: String? = null
    var phone_number: String? = null
    var work_schedule_no: String? = null
    var unit_id: String? = null
    var unit_no: String? = null
    var sv_name: String? = null
    var sv_type: String? = null
    var category_name: String? = null
    var sv_term: String? = null
    var schedule_start_date: String? = null
    var schedule_end_date: String? = null
    var schedule_start_time: String? = null
    var schedule_end_time: String? = null
    var schedule_status: String? = null
    var emp_working_status: String? = null

    var space_name: String? = null
    var is_common_area : Boolean = false
}
class ActivityLogs : Serializable{
    var work_schedule_activity_id: String? = null
    var activity_type: String? = null
    var status: String? = null
    var remark: String? = null
    var created_date: String? = null
}