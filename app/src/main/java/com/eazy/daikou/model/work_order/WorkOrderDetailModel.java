package com.eazy.daikou.model.work_order;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class WorkOrderDetailModel implements Serializable {

    @SerializedName("work_order_id")
    @Expose
    private String workOrderId;
    @SerializedName("subject")
    @Expose
    private String subject;
    @SerializedName("work_order_no")
    @Expose
    private String workOrderNo;
    @SerializedName("start_date")
    @Expose
    private String startDate;
    @SerializedName("due_date")
    @Expose
    private String dueDate;
    @SerializedName("department_name")
    @Expose
    private String departmentName;
    @SerializedName("done_percent")
    @Expose
    private String donePercent;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("priority")
    @Expose
    private Integer priority;
    @SerializedName("estimate_duration")
    @Expose
    private String estimateDuration;
    @SerializedName("component_list")
    @Expose
    private String componentList;
    @SerializedName("work_order_type")
    @Expose
    private String workOrderType;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("created_date")
    @Expose
    private String createdDate;
    @SerializedName("item_images")
    @Expose
    private List<String> itemImages = null;
    @SerializedName("work_order_images")
    @Expose
    private List<String> workOrderImages = null;
    @SerializedName("work_order_in_progress")
    @Expose
    private List<WorkOrderProgressModel> workOrderInProgress = null;
    @SerializedName("assignee_user_info")
    @Expose
    private WorkOrderUserInfo assigneeUserInfo;
    @SerializedName("hide_button_add")
    @Expose
    private boolean isHideButtonAdd;
    @SerializedName("max_done_percent_selection")
    @Expose
    private List<String> maxDonePercentSelection = null;
    @SerializedName("parent_task_id")
    @Expose
    private String parent_task_id;

    public String getWorkOrderId() {
        return workOrderId;
    }

    public void setWorkOrderId(String workOrderId) {
        this.workOrderId = workOrderId;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getWorkOrderNo() {
        return workOrderNo;
    }

    public void setWorkOrderNo(String workOrderNo) {
        this.workOrderNo = workOrderNo;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getDonePercent() {
        return donePercent;
    }

    public void setDonePercent(String donePercent) {
        this.donePercent = donePercent;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public String getEstimateDuration() {
        return estimateDuration;
    }

    public void setEstimateDuration(String estimateDuration) {
        this.estimateDuration = estimateDuration;
    }

    public String getComponentList() {
        return componentList;
    }

    public void setComponentList(String componentList) {
        this.componentList = componentList;
    }

    public String getWorkOrderType() {
        return workOrderType;
    }

    public void setWorkOrderType(String workOrderType) {
        this.workOrderType = workOrderType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public List<String> getItemImages() {
        return itemImages;
    }

    public void setItemImages(List<String> itemImages) {
        this.itemImages = itemImages;
    }

    public List<String> getWorkOrderImages() {
        return workOrderImages;
    }

    public void setWorkOrderImages(List<String> workOrderImages) {
        this.workOrderImages = workOrderImages;
    }

    public List<WorkOrderProgressModel> getWorkOrderInProgress() {
        return workOrderInProgress;
    }

    public void setWorkOrderInProgress(List<WorkOrderProgressModel> workOrderInProgress) {
        this.workOrderInProgress = workOrderInProgress;
    }

    public WorkOrderUserInfo getAssigneeUserInfo() {
        return assigneeUserInfo;
    }

    public void setAssigneeUserInfo(WorkOrderUserInfo assigneeUserInfo) {
        this.assigneeUserInfo = assigneeUserInfo;
    }

    public boolean isHideButtonAdd() {
        return isHideButtonAdd;
    }

    public void setHideButtonAdd(boolean hideButtonAdd) {
        isHideButtonAdd = hideButtonAdd;
    }

    public List<String> getMaxDonePercentSelection() {
        return maxDonePercentSelection;
    }

    public void setMaxDonePercentSelection(List<String> maxDonePercentSelection) {
        this.maxDonePercentSelection = maxDonePercentSelection;
    }

    public String getParent_task_id() {
        return parent_task_id;
    }

    public void setParent_task_id(String parent_task_id) {
        this.parent_task_id = parent_task_id;
    }
}
