package com.eazy.daikou.model.my_property.cleaning

import java.io.Serializable

class ServiceTermAndCategoryModel : Serializable {
    var service_terms : ArrayList<ServiceTerms> = ArrayList()
    var service_categories : ArrayList<ServiceCategories> = ArrayList()
}

class ServiceTerms : Serializable{
    var term_name: String? = null
    var term_key: String? = null
    var isClickTitle: Boolean = false
}

class ServiceCategories : Serializable{
    var id: String? = null
    var category_name: String? = null
    var operation_part: String? = null
    var description: String? = null
    var isClick = false
}