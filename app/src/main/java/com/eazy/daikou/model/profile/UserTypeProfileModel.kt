package com.eazy.daikou.model.profile

import java.io.Serializable

class UserTypeProfileModel : Serializable {
    var user_type : String?= null
    var accounts : ArrayList<PropertyItemModel> = ArrayList()
}

class PropertyItemModel : Serializable{
    var name : String? = null
    var id : String?= null
}