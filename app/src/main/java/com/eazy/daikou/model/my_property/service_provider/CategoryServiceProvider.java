package com.eazy.daikou.model.my_property.service_provider;

public class CategoryServiceProvider {
    Object image ;
    String name;

    public CategoryServiceProvider(Object image, String name) {
        this.image = image;
        this.name = name;
    }

    public Object getImage() {
        return image;
    }

    public void setImage(Object image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
