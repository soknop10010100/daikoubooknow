package com.eazy.daikou.model.profile;

public class NationalityList {

    private String num_code;
    private String alpha_2_code;
    private String alpha_3_code;
    private String en_short_name;
    private String nationality;
    private boolean isClick;

    public String getNaum_code() {
        return num_code;
    }

    public void setNaum_code(String naum_code) {
        this.num_code = naum_code;
    }

    public String getAlpha_2_code() {
        return alpha_2_code;
    }

    public void setAlpha_2_code(String alpha_2_code) {
        this.alpha_2_code = alpha_2_code;
    }

    public String getAlpha_3_code() {
        return alpha_3_code;
    }

    public void setAlpha_3_code(String alpha_3_code) {
        this.alpha_3_code = alpha_3_code;
    }

    public String getEn_short_name() {
        return en_short_name;
    }

    public void setEn_short_name(String en_short_name) {
        this.en_short_name = en_short_name;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public boolean isClick() {
        return isClick;
    }

    public void setClick(boolean click) {
        isClick = click;
    }
}
