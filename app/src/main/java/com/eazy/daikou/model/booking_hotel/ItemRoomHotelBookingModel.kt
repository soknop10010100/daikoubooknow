package com.eazy.daikou.model.booking_hotel

import java.io.Serializable

class ItemRoomHotelBookingModel : Serializable{
    var room_id : String? = null
    var isClick : Boolean = false
    var price : Double= 0.0
    var beds : String? = null
    var adults : String? = null
    var children : String? = null
    var size : String? = null
    var room_image : String? = null
    var price_display : String? = null
    var original_price_display : String?= null
    var room_name : String? = null
    var number : Int = 0

    var totalAllPrice : Double = 0.0
    var totalRoom : Int = 0
    var full_status : String? = null
    var numberRoomSelect : Int = 0

    var quantity : Int = 0
}