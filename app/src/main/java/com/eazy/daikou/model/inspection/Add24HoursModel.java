package com.eazy.daikou.model.inspection;

import java.io.Serializable;
import java.util.List;

public class Add24HoursModel implements Serializable {

    private String hour;
    private List<InfoInspectionModel> hourList;

    public Add24HoursModel(String hour, List<InfoInspectionModel> hourList) {
        this.hour = hour;
        this.hourList = hourList;
    }

    public List<InfoInspectionModel> getHourList() {
        return hourList;
    }

    public void setHourList(List<InfoInspectionModel> hourList) {
        this.hourList = hourList;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }
}
