package com.eazy.daikou.model.my_unit_info

import java.io.Serializable

class MyUnitScanInfoModel : Serializable{
    var part : String? = null
    var action : String? = null
    var parking : ParkingInfo? = null
    var unit : UnitModel? = null
    var service_provider_info : ServiceProviderInfo? = null
    var options : ArrayList<String> = ArrayList()
}

class UnitModel : Serializable{
    var unit_id : String? = null
    var unit_no : String? = null
    var unit_insp_type_id : String? = null
    var current_owner_tenant : String? = null
    var unit_qr : String? = null
}

class ParkingInfo : Serializable{
    var parking_id : String? = null
}

class ServiceProviderInfo : Serializable{
    var action : String? = null
    var work_schedule_id : String? = null
    var success : Boolean = false
    var msg : String? = null
    var action_status : String? = null
}


