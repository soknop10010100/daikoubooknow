package com.eazy.daikou.model.my_property.payment_schedule_my_unit

import java.io.Serializable

class OperationPaymentInfoModel : Serializable {
    var sale_order_id : String? = null
    var sale_order_item_id : String? = null
    var unit_id : String? = null
    var unit_no : String? = null
    var item_name : String? = null
    var client_type : String? = null
    var total_management_service : String? = null
    var total_electricity : String? = null
    var total_water : String? = null
    var total_maintenance : String? = null
    var total_topup_reader : String? = null
    var total_common_electricity : String? = null
    var total_common_water : String? = null
    var total_parking : String? = null
    var total_cleaning : String? = null
    var total_pest_control : String? = null
    var total_cable_tv : String? = null
    var total_internet : String? = null
    var total_laundry : String? = null
    var total_security : String? = null
    var total_other : String? = null
    var issue_dt : String? = null
    var due_dt : String? = null
    var vat : String? = null
    var total : String? = null
    var outstanding : String? = null
    var collection : String? = null
    var payment_status : String? = null
    var payment_date : String? = null
    var payment_method : String? = null
    var invoice_no : String? = null
    var created_dt : String? = null
}