package com.eazy.daikou.model.facility_booking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class FacilityBookingModel implements Serializable {
    @SerializedName("space_id")
    @Expose
    private String spaceId;
    @SerializedName("space_name")
    @Expose
    private String spaceName;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("space_images")
    @Expose
    private String spaceImages;
    @SerializedName("property")
    @Expose
    private PropertyVenueModel property;
    @SerializedName("venue")
    @Expose
    private PropertyVenueModel venue;
    @SerializedName("space_price")
    @Expose
    private SpacePricing spacePricing;
    @SerializedName("is_free")
    @Expose
    private boolean isFree;

    public String getSpaceId() {
        return spaceId;
    }

    public void setSpaceId(String spaceId) {
        this.spaceId = spaceId;
    }

    public String getSpaceName() {
        return spaceName;
    }

    public void setSpaceName(String spaceName) {
        this.spaceName = spaceName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSpaceImages() {
        return spaceImages;
    }

    public void setSpaceImages(String spaceImages) {
        this.spaceImages = spaceImages;
    }

    public PropertyVenueModel getProperty() {
        return property;
    }

    public void setProperty(PropertyVenueModel property) {
        this.property = property;
    }

    public PropertyVenueModel getVenue() {
        return venue;
    }

    public void setVenue(PropertyVenueModel venue) {
        this.venue = venue;
    }

    public SpacePricing getSpacePricing() {
        return spacePricing;
    }

    public void setSpacePricing(SpacePricing spacePricing) {
        this.spacePricing = spacePricing;
    }

    public boolean isFree() {
        return isFree;
    }

    public void setFree(boolean free) {
        isFree = free;
    }
}
