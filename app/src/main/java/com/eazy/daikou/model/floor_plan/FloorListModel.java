package com.eazy.daikou.model.floor_plan;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class FloorListModel implements Serializable {

    @SerializedName("floor_no")
    @Expose
    private String floorNo;
    @SerializedName("storey_id")
    @Expose
    private String storeyId;
    @SerializedName("layout_canvas_json_file")
    @Expose
    private String layoutCanvasJsonFile;
    @SerializedName("layout_canvas_size")
    @Expose
    private String layoutCanvasSize;
    @SerializedName("background_img")
    @Expose
    private String backgroundImg;
    private boolean isClick;

    public boolean isClick() {
        return isClick;
    }

    public void setClick(boolean click) {
        isClick = click;
    }

    public String getFloorNo() {
        return floorNo;
    }

    public void setFloorNo(String floorNo) {
        this.floorNo = floorNo;
    }

    public String getStoreyId() {
        return storeyId;
    }

    public void setStoreyId(String storeyId) {
        this.storeyId = storeyId;
    }

    public String getLayoutCanvasJsonFile() {
        return layoutCanvasJsonFile;
    }

    public void setLayoutCanvasJsonFile(String layoutCanvasJsonFile) {
        this.layoutCanvasJsonFile = layoutCanvasJsonFile;
    }

    public String getLayoutCanvasSize() {
        return layoutCanvasSize;
    }

    public void setLayoutCanvasSize(String layoutCanvasSize) {
        this.layoutCanvasSize = layoutCanvasSize;
    }


    public String getBackgroundImg() {
        return backgroundImg;
    }

    public void setBackgroundImg(String backgroundImg) {
        this.backgroundImg = backgroundImg;
    }
}
