package com.eazy.daikou.model.home;

import android.graphics.drawable.Drawable;

import java.io.Serializable;

public class HomeGuestModel implements Serializable {
    private Drawable image;
    private final String title;
    private final String action;
    private boolean isClick;

    public HomeGuestModel(String title, String action) {
        this.title = title;
        this.action = action;
    }

    public String getTitle() {
        return title;
    }


    public String getAction() {
        return action;
    }

    public boolean isClick() {
        return isClick;
    }

    public void setClick(boolean click) {
        isClick = click;
    }

    public Drawable getImage() {
        return image;
    }
}
