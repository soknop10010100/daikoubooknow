package com.eazy.daikou.model.utillity_tracking.model;

import android.graphics.Bitmap;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class UnitImgModel implements Serializable {

    private Boolean isClickTitle = false;
    @SerializedName("unit_id")
    @Expose
    private String unitId;
    @SerializedName("unit_no")
    @Expose
    private String unitNo;
    @SerializedName("floor_no")
    @Expose
    private String floorNo;
    @SerializedName("direction")
    @Expose
    private String direction;
    @SerializedName("property_unit_type_id_fk")
    @Expose
    private String propertyUnitTypeIdFk;
    @SerializedName("property_unit_type_name")
    @Expose
    private String propertyUnitTypeName;
    @SerializedName("qr_code")
    @Expose
    private String qrCode;
    @SerializedName("buyer_type")
    @Expose
    private String buyerType;
    @SerializedName("my_upload_image")
    @Expose
    private List<MyUploadImage> myUploadImage = null;
    @SerializedName("my_upload_video")
    @Expose
    private List<MyUploadVideo> myUploadVideo = null;
    @SerializedName("gallery")
    @Expose
    private List<String> gallery = null;
    @SerializedName("layout")
    @Expose
    private String layout;
    @SerializedName("unit_type_info")
    @Expose
    private UnitTypeInfo unitTypeInfo;

    public Boolean getClickTitle() {
        return isClickTitle;
    }

    public void setClickTitle(Boolean clickTitle) {
        isClickTitle = clickTitle;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getUnitNo() {
        return unitNo;
    }

    public void setUnitNo(String unitNo) {
        this.unitNo = unitNo;
    }

    public String getFloorNo() {
        return floorNo;
    }

    public void setFloorNo(String floorNo) {
        this.floorNo = floorNo;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getPropertyUnitTypeIdFk() {
        return propertyUnitTypeIdFk;
    }

    public void setPropertyUnitTypeIdFk(String propertyUnitTypeIdFk) {
        this.propertyUnitTypeIdFk = propertyUnitTypeIdFk;
    }

    public String getPropertyUnitTypeName() {
        return propertyUnitTypeName;
    }

    public void setPropertyUnitTypeName(String propertyUnitTypeName) {
        this.propertyUnitTypeName = propertyUnitTypeName;
    }

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    public String getBuyerType() {
        return buyerType;
    }

    public void setBuyerType(String buyerType) {
        this.buyerType = buyerType;
    }

    public List<MyUploadImage> getMyUploadImage() {
        return myUploadImage;
    }

    public void setMyUploadImage(List<MyUploadImage> myUploadImage) {
        this.myUploadImage = myUploadImage;
    }

    public List<MyUploadVideo> getMyUploadVideo() {
        return myUploadVideo;
    }

    public void setMyUploadVideo(List<MyUploadVideo> myUploadVideo) {
        this.myUploadVideo = myUploadVideo;
    }

    public List<String> getGallery() {
        return gallery;
    }

    public void setGallery(List<String> gallery) {
        this.gallery = gallery;
    }

    public String getLayout() {
        return layout;
    }

    public void setLayout(String layout) {
        this.layout = layout;
    }

    public UnitTypeInfo getUnitTypeInfo() {
        return unitTypeInfo;
    }

    public void setUnitTypeInfo(UnitTypeInfo unitTypeInfo) {
        this.unitTypeInfo = unitTypeInfo;
    }

    public static class MyUploadImage implements Serializable {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("file_path")
        @Expose
        private String filePath;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getFilePath() {
            return filePath;
        }

        public void setFilePath(String filePath) {
            this.filePath = filePath;
        }
    }
    public static class MyUploadVideo implements Serializable {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("file_path")
        @Expose
        private String filePath;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getFilePath() {
            return filePath;
        }

        public void setFilePath(String filePath) {
            this.filePath = filePath;
        }
    }
    public static class UnitTypeInfo implements Serializable{

        @SerializedName("total_bedroom")
        @Expose
        private String totalBedroom;
        @SerializedName("total_bathroom")
        @Expose
        private String totalBathroom;
        @SerializedName("total_living_room")
        @Expose
        private String totalLivingRoom;
        @SerializedName("style")
        @Expose
        private String style;
        @SerializedName("total_maid_room")
        @Expose
        private String totalMaidRoom;
        @SerializedName("total_area_sqm")
        @Expose
        private String totalAreaSqm;
        @SerializedName("total_private_area")
        @Expose
        private String totalPrivateArea;
        @SerializedName("total_common_area")
        @Expose
        private String totalCommonArea;
        @SerializedName("total_balcony")
        @Expose
        private String totalBalcony;

        public String getTotalBedroom() {
            return totalBedroom;
        }

        public void setTotalBedroom(String totalBedroom) {
            this.totalBedroom = totalBedroom;
        }

        public String getTotalBathroom() {
            return totalBathroom;
        }

        public void setTotalBathroom(String totalBathroom) {
            this.totalBathroom = totalBathroom;
        }

        public String getTotalLivingRoom() {
            return totalLivingRoom;
        }

        public void setTotalLivingRoom(String totalLivingRoom) {
            this.totalLivingRoom = totalLivingRoom;
        }

        public String getStyle() {
            return style;
        }

        public void setStyle(String style) {
            this.style = style;
        }

        public String getTotalMaidRoom() {
            return totalMaidRoom;
        }

        public void setTotalMaidRoom(String totalMaidRoom) {
            this.totalMaidRoom = totalMaidRoom;
        }

        public String getTotalAreaSqm() {
            return totalAreaSqm;
        }

        public void setTotalAreaSqm(String totalAreaSqm) {
            this.totalAreaSqm = totalAreaSqm;
        }

        public String getTotalPrivateArea() {
            return totalPrivateArea;
        }

        public void setTotalPrivateArea(String totalPrivateArea) {
            this.totalPrivateArea = totalPrivateArea;
        }

        public String getTotalCommonArea() {
            return totalCommonArea;
        }

        public void setTotalCommonArea(String totalCommonArea) {
            this.totalCommonArea = totalCommonArea;
        }

        public String getTotalBalcony() {
            return totalBalcony;
        }

        public void setTotalBalcony(String totalBalcony) {
            this.totalBalcony = totalBalcony;
        }

    }
}


