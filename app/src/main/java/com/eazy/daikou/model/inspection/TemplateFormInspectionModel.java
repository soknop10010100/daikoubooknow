package com.eazy.daikou.model.inspection;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class TemplateFormInspectionModel implements Serializable {

    @SerializedName("inspection_id")
    @Expose
    private String inspectionId;
    @SerializedName("inspection_room_id")
    @Expose
    private String inspectionRoomId;
    @SerializedName("option_type")
    @Expose
    private String optionType;
    @SerializedName("inspection_title")
    @Expose
    private String inspectionTitle;
    @SerializedName("inspection_sub_title")
    @Expose
    private String inspectionSubTitle;

    public String getInspectionId() {
        return inspectionId;
    }

    public void setInspectionId(String inspectionId) {
        this.inspectionId = inspectionId;
    }

    public String getInspectionRoomId() {
        return inspectionRoomId;
    }

    public void setInspectionRoomId(String inspectionRoomId) {
        this.inspectionRoomId = inspectionRoomId;
    }

    public String getOptionType() {
        return optionType;
    }

    public void setOptionType(String optionType) {
        this.optionType = optionType;
    }

    public String getInspectionTitle() {
        return inspectionTitle;
    }

    public void setInspectionTitle(String inspectionTitle) {
        this.inspectionTitle = inspectionTitle;
    }

    public String getInspectionSubTitle() {
        return inspectionSubTitle;
    }

    public void setInspectionSubTitle(String inspectionSubTitle) {
        this.inspectionSubTitle = inspectionSubTitle;
    }

}
