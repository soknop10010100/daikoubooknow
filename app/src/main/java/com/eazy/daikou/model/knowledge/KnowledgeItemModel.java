package com.eazy.daikou.model.knowledge;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class KnowledgeItemModel implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("category_id_fk")
    @Expose
    private String categoryIdFk;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("credit_title")
    @Expose
    private String creditTitle;
    @SerializedName("created_dt")
    @Expose
    private String createdDt;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategoryIdFk() {
        return categoryIdFk;
    }

    public void setCategoryIdFk(String categoryIdFk) {
        this.categoryIdFk = categoryIdFk;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCreditTitle() {
        return creditTitle;
    }

    public void setCreditTitle(String creditTitle) {
        this.creditTitle = creditTitle;
    }

    public String getCreatedDt() {
        return createdDt;
    }

    public void setCreatedDt(String createdDt) {
        this.createdDt = createdDt;
    }

}
