package com.eazy.daikou.model.booking_hotel;

import android.graphics.drawable.Drawable;

import java.io.Serializable;

public class DashboardModel implements Serializable {

    private transient String action;
    private transient String name;
    private final transient Drawable drawable;
    private transient String price;

    public DashboardModel(String action, Drawable drawable, String name, String price) {
        this.action = action;
        this.drawable = drawable;
        this.name = name;
        this.price = price;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Drawable getDrawable() {
        return drawable;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
