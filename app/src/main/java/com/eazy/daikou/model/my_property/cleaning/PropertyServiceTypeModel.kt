package com.eazy.daikou.model.my_property.cleaning

import java.io.Serializable

class PropertyServiceTypeModel : Serializable {

    var id: String? = null
    var sv_name: String? =null
    var sv_photo: String? = null
    var sv_group_description: String? = null
    var price: String? = null
    var sv_period: String? = null
    var sv_start_time: String? = null
    var sv_end_time: String? = null

    var available_days: ArrayList<String> = ArrayList()
    var available_start_times: ArrayList<String> = ArrayList()
}

