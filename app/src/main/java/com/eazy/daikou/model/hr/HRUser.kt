package com.eazy.daikou.model.hr

import java.io.Serializable

class HRUser : Serializable {

    var profile_detail: ProfileDetail? = null
    var bank_detail: BankDetail? = null
    var qualification_details : ArrayList<Qualification> = ArrayList()
    var experience = ArrayList<Experience>()
    var relationship = ArrayList<Relationship>()
    var document_detail = ArrayList<Document>()

}

class ProfileDetail : Serializable {
    val id: String? = null
    val created_by: String? = null
    val employee_id: String? = null
    val name: String? = null
    val father_name: String? = null
    val mother_name: String? = null
    val spouse_name: String? = null
    val email: String? = null
    val password: String? = null
    val present_address: String? = null
    val permanent_address: String? = null
    val home_district: String? = null
    val academic_qualification: String? = null
    val professional_qualification: String? = null
    val joining_date: String? = null
    val experience: String? = null
    val reference: String? = null
    val id_name: String? = null
    val id_number: String? = null
    val contact_no_one: String? = null
    val contact_no_two: String? = null
    val emergency_contact: String? = null
    val web: String? = null
    val gender: String? = null
    val date_of_birth: String? = null
    val marital_status: String? = null
    val avatar: String? = null
    val client_type_id: String? = null
    val designation_id: String? = null
    val joining_position: String? = null
    val access_label: String? = null
    val role: String? = null
    val activation_status: String? = null
    val account_id: String? = null
    val active_account_id: String? = null
    val deletion_status: String? = null
    val remember_token: String? = null
    val created_at: String? = null
    val updated_at: String? = null
    val currency: String? = null
    val tax_bare: String? = null
    val wechat: String? = null
    val telegram: String? = null
    val whatsapp: String? = null
    val direct_manager_id: String? = null
    val salary_per: String? = null
    val working_area: String? = null
    val visa_type: String? = null
    val payment_method: String? = null
    val employee_condition: String? = null
    val direct_manager: String? = null
    val shift: String? = null
    val foreign_title: String? = null
    val phone_code_one: String? = null
    val visa_expiry_date: String? = null
    val nationality_id: String? = null
    val end_condition_date: String? = null
    val nssf_no: String? = null
    val mother_language: String? = null
    val religion: String? = null
    val pob: String? = null
    val id_issue_date: String? = null
    val id_expiry_date: String? = null
    val id_image: String? = null
    val middle_name_native: String? = null
    val last_name_native: String? = null
    val first_name_native: String? = null
    val last_name: String? = null
    val middle_name: String? = null
    val first_name: String? = null
    val title: String? = null
    val daikou_property_key: String? = null
    val working_hour: String? = null
    val designation: String? = null
}

class BankDetail : Serializable {
    val id: String? = null
    val created_by: String? = null
    val user_id: String? = null
    val account_no: String? = null
    val account_name: String? = null
    val bank: String? = null
    val branch: String? = null
    val branch_addr: String? = null
    val iban: String? = null
    val swift_code: String? = null
    val created_at: String? = null
    val updated_at: String? = null
}