package com.eazy.daikou.model.my_property.service_provider_v2

import java.io.Serializable

class ServiceProviderTypeModel : Serializable {

    var service_key: String? = null
    var service_name: String? = null
    var operation_part: String? = null

    var isClick: Boolean = false

}