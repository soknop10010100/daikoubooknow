package com.eazy.daikou.model.parking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Parking implements Serializable {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("order_no")
    @Expose
    private String orderNo;
    @SerializedName("property_id")
    @Expose
    private String propertyId;
    @SerializedName("property_name")
    @Expose
    private String propertyName;
    @SerializedName("car_owner_user_id")
    @Expose
    private String carOwnerUserId;
    @SerializedName("entrance_date")
    @Expose
    private String entranceDate;
    @SerializedName("exit_date")
    @Expose
    private Object exitDate;
    @SerializedName("parking_no_id")
    @Expose
    private String parkingNoId;
    @SerializedName("parking_no_name")
    @Expose
    private String parkingNoName;
    @SerializedName("available")
    @Expose
    private String available;
    @SerializedName("parking_lot_type_id")
    @Expose
    private String parkingLotTypeId;
    @SerializedName("parking_lot_type_name")
    @Expose
    private String parkingLotTypeName;
    @SerializedName("parking_zone_id")
    @Expose
    private String parkingZoneId;
    @SerializedName("parking_zone_name")
    @Expose
    private String parkingZoneName;
    @SerializedName("car_plate_no")
    @Expose
    private String carPlateNo;
    @SerializedName("created_date")
    @Expose
    private String createdDate;
    @SerializedName("employee_who_update_info_user_id")
    @Expose
    private String employeeWhoUpdateInfoUserId;
    @SerializedName("total_parking_fee")
    @Expose
    private Object totalParkingFee;
    @SerializedName("payment_status")
    @Expose
    private String paymentStatus;
    @SerializedName("total_paid")
    @Expose
    private Object totalPaid;
    @SerializedName("paid_date")
    @Expose
    private Object paidDate;
    @SerializedName("pay_by_cash_status")
    @Expose
    private String payByCashStatus;
    @SerializedName("payment_method")
    @Expose
    private String paymentMethod;
    @SerializedName("car_color")
    @Expose
    private String carColor;
    @SerializedName("car_model")
    @Expose
    private String carModel;
    @SerializedName("open_hour")
    @Expose
    private String openHour;
    @SerializedName("close_hour")
    @Expose
    private String closeHour;
    @SerializedName("employee_receiver_user_id")
    @Expose
    private Object employeeReceiverUserId;
    @SerializedName("employee_receiver_user_name")
    @Expose
    private Object employeeReceiverUserName;
    @SerializedName("parking_type")
    @Expose
    private String parkingType;
    @SerializedName("parking_duration_in_minute")
    @Expose
    private int parkingDurationInMinute;
    @SerializedName("parking_duration_in_hour")
    @Expose
    private double parkingDurationInHour;
    @SerializedName("parking_duration_in_day")
    @Expose
    private double parkingDurationInDay;
    @SerializedName("parking_duration_in_date")
    @Expose
    private String parkingDurationInDate;
    @SerializedName("parking_duration_in_day_today")
    @Expose
    private String parkingDurationInDayToday;
    @SerializedName("parking_duration_in_time")
    @Expose
    private String parkingDurationInTime;
    @SerializedName("parking_entrance_in_day")
    @Expose
    private String parkingEntranceInDay;
    @SerializedName("current_date_time")
    @Expose
    private String currentDateTime;

    @SerializedName("received_amount")
    @Expose
    private String receivedAmount;

    @SerializedName("changed_amount")
    @Expose
    private String changedAmount;

    @SerializedName("all_documents")
    @Expose
    private List<AllComment> allDocuments = null;
    @SerializedName("parking_fee")
    @Expose
    private double parkingFee;
    @SerializedName("parking_alert_history")
    @Expose
    private List<ParkingAlertHistory> parkingAlertHistory = null;
    @SerializedName("parking_fee_currency")
    @Expose
    private String parkingFeeCurrency;

    @SerializedName("car_plate_image")
    @Expose
    private String carImagePlate;

    @SerializedName("ticket_no")
    @Expose
    private String ticketNo;
    @SerializedName("parking_with_machine")
    @Expose
    private String parking_with_machine;

    public String getParking_with_machine() {
        return parking_with_machine;
    }

    public void setParking_with_machine(String parking_with_machine) {
        this.parking_with_machine = parking_with_machine;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getCarImagePlate() {
        return carImagePlate;
    }

    public void setCarImagePlate(String carImagePlate) {
        this.carImagePlate = carImagePlate;
    }

    public String getTicketNo() {
        return ticketNo;
    }

    public void setTicketNo(String ticketNo) {
        this.ticketNo = ticketNo;
    }

    public String getReceivedAmount() {
        return receivedAmount;
    }

    public void setReceivedAmount(String receivedAmount) {
        this.receivedAmount = receivedAmount;
    }

    public String getChangedAmount() {
        return changedAmount;
    }

    public void setChangedAmount(String changedAmount) {
        this.changedAmount = changedAmount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(String propertyId) {
        this.propertyId = propertyId;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    public String getCarOwnerUserId() {
        return carOwnerUserId;
    }

    public void setCarOwnerUserId(String carOwnerUserId) {
        this.carOwnerUserId = carOwnerUserId;
    }

    public String getEntranceDate() {
        return entranceDate;
    }

    public void setEntranceDate(String entranceDate) {
        this.entranceDate = entranceDate;
    }

    public Object getExitDate() {
        return exitDate;
    }

    public void setExitDate(Object exitDate) {
        this.exitDate = exitDate;
    }

    public String getParkingNoId() {
        return parkingNoId;
    }

    public void setParkingNoId(String parkingNoId) {
        this.parkingNoId = parkingNoId;
    }

    public String getParkingNoName() {
        return parkingNoName;
    }

    public void setParkingNoName(String parkingNoName) {
        this.parkingNoName = parkingNoName;
    }

    public String getAvailable() {
        return available;
    }

    public void setAvailable(String available) {
        this.available = available;
    }

    public String getParkingLotTypeId() {
        return parkingLotTypeId;
    }

    public void setParkingLotTypeId(String parkingLotTypeId) {
        this.parkingLotTypeId = parkingLotTypeId;
    }

    public String getParkingLotTypeName() {
        return parkingLotTypeName;
    }

    public void setParkingLotTypeName(String parkingLotTypeName) {
        this.parkingLotTypeName = parkingLotTypeName;
    }

    public String getParkingZoneId() {
        return parkingZoneId;
    }

    public void setParkingZoneId(String parkingZoneId) {
        this.parkingZoneId = parkingZoneId;
    }

    public String getParkingZoneName() {
        return parkingZoneName;
    }

    public void setParkingZoneName(String parkingZoneName) {
        this.parkingZoneName = parkingZoneName;
    }

    public String getCarPlateNo() {
        return carPlateNo;
    }

    public void setCarPlateNo(String carPlateNo) {
        this.carPlateNo = carPlateNo;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getEmployeeWhoUpdateInfoUserId() {
        return employeeWhoUpdateInfoUserId;
    }

    public void setEmployeeWhoUpdateInfoUserId(String employeeWhoUpdateInfoUserId) {
        this.employeeWhoUpdateInfoUserId = employeeWhoUpdateInfoUserId;
    }

    public Object getTotalParkingFee() {
        return totalParkingFee;
    }

    public void setTotalParkingFee(Object totalParkingFee) {
        this.totalParkingFee = totalParkingFee;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public Object getTotalPaid() {
        return totalPaid;
    }

    public void setTotalPaid(Object totalPaid) {
        this.totalPaid = totalPaid;
    }

    public Object getPaidDate() {
        return paidDate;
    }

    public void setPaidDate(Object paidDate) {
        this.paidDate = paidDate;
    }

    public String getPayByCashStatus() {
        return payByCashStatus;
    }

    public void setPayByCashStatus(String payByCashStatus) {
        this.payByCashStatus = payByCashStatus;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getCarColor() {
        return carColor;
    }

    public void setCarColor(String carColor) {
        this.carColor = carColor;
    }

    public String getCarModel() {
        return carModel;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public String getOpenHour() {
        return openHour;
    }

    public void setOpenHour(String openHour) {
        this.openHour = openHour;
    }

    public String getCloseHour() {
        return closeHour;
    }

    public void setCloseHour(String closeHour) {
        this.closeHour = closeHour;
    }

    public Object getEmployeeReceiverUserId() {
        return employeeReceiverUserId;
    }

    public void setEmployeeReceiverUserId(Object employeeReceiverUserId) {
        this.employeeReceiverUserId = employeeReceiverUserId;
    }

    public Object getEmployeeReceiverUserName() {
        return employeeReceiverUserName;
    }

    public void setEmployeeReceiverUserName(Object employeeReceiverUserName) {
        this.employeeReceiverUserName = employeeReceiverUserName;
    }

    public String getParkingType() {
        return parkingType;
    }

    public void setParkingType(String parkingType) {
        this.parkingType = parkingType;
    }

    public int getParkingDurationInMinute() {
        return parkingDurationInMinute;
    }

    public void setParkingDurationInMinute(int parkingDurationInMinute) {
        this.parkingDurationInMinute = parkingDurationInMinute;
    }

    public double getParkingDurationInHour() {
        return parkingDurationInHour;
    }

    public void setParkingDurationInHour(double parkingDurationInHour) {
        this.parkingDurationInHour = parkingDurationInHour;
    }

    public double getParkingDurationInDay() {
        return parkingDurationInDay;
    }

    public void setParkingDurationInDay(double parkingDurationInDay) {
        this.parkingDurationInDay = parkingDurationInDay;
    }

    public String getParkingDurationInDate() {
        return parkingDurationInDate;
    }

    public void setParkingDurationInDate(String parkingDurationInDate) {
        this.parkingDurationInDate = parkingDurationInDate;
    }

    public String getParkingDurationInDayToday() {
        return parkingDurationInDayToday;
    }

    public void setParkingDurationInDayToday(String parkingDurationInDayToday) {
        this.parkingDurationInDayToday = parkingDurationInDayToday;
    }

    public String getParkingDurationInTime() {
        return parkingDurationInTime;
    }

    public void setParkingDurationInTime(String parkingDurationInTime) {
        this.parkingDurationInTime = parkingDurationInTime;
    }

    public String getParkingEntranceInDay() {
        return parkingEntranceInDay;
    }

    public void setParkingEntranceInDay(String parkingEntranceInDay) {
        this.parkingEntranceInDay = parkingEntranceInDay;
    }

    public String getCurrentDateTime() {
        return currentDateTime;
    }

    public void setCurrentDateTime(String currentDateTime) {
        this.currentDateTime = currentDateTime;
    }

    public List<AllComment> getAllDocuments() {
        return allDocuments;
    }

    public void setAllDocuments(List<AllComment> allDocuments) {
        this.allDocuments = allDocuments;
    }

    public double getParkingFee() {
        return parkingFee;
    }

    public void setParkingFee(double parkingFee) {
        this.parkingFee = parkingFee;
    }

    public List<ParkingAlertHistory> getParkingAlertHistory() {
        return parkingAlertHistory;
    }

    public void setParkingAlertHistory(List<ParkingAlertHistory> parkingAlertHistory) {
        this.parkingAlertHistory = parkingAlertHistory;
    }

    public String getParkingFeeCurrency() {
        return parkingFeeCurrency;
    }

    public void setParkingFeeCurrency(String parkingFeeCurrency) {
        this.parkingFeeCurrency = parkingFeeCurrency;
    }

}




