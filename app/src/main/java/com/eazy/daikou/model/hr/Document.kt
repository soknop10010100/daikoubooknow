package com.eazy.daikou.model.hr

import java.io.Serializable

class Document : Serializable {
    val name: String? = null
    val contact: String? = null
    val issue_dt: String? = null
    val start_dt: String? = null
    val filename: String? = null
}