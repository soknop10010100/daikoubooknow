package com.eazy.daikou.model.facility_booking;

public class FeeTypeModel {

    private String id;
    private String freeTypeName;
    private boolean isClick;
    private boolean isEnableItem;

    public FeeTypeModel(String id, String freeTypeName) {
        this.id = id;
        this.freeTypeName = freeTypeName;
    }

    public FeeTypeModel(String id, String freeTypeName, boolean isClick) {
        this.id = id;
        this.freeTypeName = freeTypeName;
        this.isClick = isClick;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFreeTypeName() {
        return freeTypeName;
    }

    public void setFreeTypeName(String freeTypeName) {
        this.freeTypeName = freeTypeName;
    }

    public boolean isClick() {
        return isClick;
    }

    public void setClick(boolean click) {
        isClick = click;
    }

    public boolean isEnableItem() {
        return isEnableItem;
    }

    public void setEnableItem(boolean enableItem) {
        isEnableItem = enableItem;
    }
}
