package com.eazy.daikou.model.hr

import java.io.Serializable

class ItemResignationModel : Serializable {
    var resignation_types : ArrayList<ApproveResignType> = ArrayList()
    var approver : ArrayList<ApproveResignType> = ArrayList()
}

class ApproveResignType : Serializable{
    var id : String? = null
    var name : String? = null
    var user_business_key : String? = null
    var isClick : Boolean = false
}