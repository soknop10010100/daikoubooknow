package com.eazy.daikou.model.utillity_tracking.model;

import java.io.Serializable;

public class LastTrackData implements Serializable {

    private String lastTrackingDate;
    private String lastCurrentUsage;
    private boolean isAlreadyHaveTrack;
    private final String current_usage;
    private String current_date;

    public LastTrackData(String lastTrackingDate, String lastCurrentUsage, String current_usage, boolean isAlreadyHaveTrack, String current_date) {
        this.lastTrackingDate = lastTrackingDate;
        this.lastCurrentUsage = lastCurrentUsage;
        this.isAlreadyHaveTrack = isAlreadyHaveTrack;
        this.current_usage = current_usage;
        this.current_date = current_date;
    }


    public boolean getIsAlreadyHaveTrack() {
        return isAlreadyHaveTrack;
    }

    public void setIsAlreadyHaveTrack(boolean isAlreadyHaveTrack) {
        this.isAlreadyHaveTrack = isAlreadyHaveTrack;
    }

    public String getLastTrackingDate() {
        return lastTrackingDate;
    }

    public void setLastTrackingDate(String lastTrackingDate) {
        this.lastTrackingDate = lastTrackingDate;
    }

    public String getLastCurrentUsage() {
        return lastCurrentUsage;
    }

    public void setLastCurrentUsage(String lastCurrentUsage) {
        this.lastCurrentUsage = lastCurrentUsage;
    }

    public String getCurrent_usage() {
        return current_usage;
    }

    public String getCurrent_date() {
        return current_date;
    }

    public void setCurrent_date(String current_date) {
        this.current_date = current_date;
    }
}
