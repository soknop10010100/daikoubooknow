package com.eazy.daikou.model.service_provider

import java.io.Serializable

class ListAllServiceProviderModel : Serializable{
    var operation_part : String? = null
    var name : String? = null
    var icon : String? = null
    var account_id : String? = null
    var city : String? = null
    var service_id : String? = null
    var image : String? = null
    var price : String? = null
    var price_display : String? = null
    var company_info : CompanyInfo? = null
}
class CompanyInfo : Serializable{
    var account_id : String? = null
    var name : String? = null
    var image : String? = null
}