package com.eazy.daikou.model.my_property.service_provider

import com.eazy.daikou.model.hr.Relationship
import java.io.Serializable

class ServiceProviderCategoryModel : Serializable {

    var category_keyword: String? = null
    var category_name: String? = null
}