package com.eazy.daikou.model.my_property.service_property_employee;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class CleanScheduleInfoCleaner implements Serializable{
    @SerializedName("clean_schedule_info")
    @Expose
    private CleanScheduleInfo cleanScheduleInfo;
    @SerializedName("employee_info")
    @Expose
    private EmployeeInfo employeeInfo;
    @SerializedName("before_clean_image")
    @Expose
    private ArrayList<String> beforeCleanImage = null;
    @SerializedName("after_clean_image")
    @Expose
    private ArrayList<String> afterCleanImage = null;
    @SerializedName("cleaner_rating_info")
    @Expose
    private CleanerRatingInfo cleanerRatingInfo;
    @SerializedName("assigner_user_info")
    @Expose
    private AssignerUserInfo assignerUserInfo;


    public AssignerUserInfo getAssignerUserInfo() {
        return assignerUserInfo;
    }

    public void setAssignerUserInfo(AssignerUserInfo assignerUserInfo) {
        this.assignerUserInfo = assignerUserInfo;
    }

    public CleanerRatingInfo getCleanerRatingInfo() {
        return cleanerRatingInfo;
    }

    public void setCleanerRatingInfo(CleanerRatingInfo cleanerRatingInfo) {
        this.cleanerRatingInfo = cleanerRatingInfo;
    }

    public CleanScheduleInfo getCleanScheduleInfo() {
        return cleanScheduleInfo;
    }

    public void setCleanScheduleInfo(CleanScheduleInfo cleanScheduleInfo) {
        this.cleanScheduleInfo = cleanScheduleInfo;
    }

    public EmployeeInfo getEmployeeInfo() {
        return employeeInfo;
    }

    public void setEmployeeInfo(EmployeeInfo employeeInfo) {
        this.employeeInfo = employeeInfo;
    }

    public ArrayList<String> getBeforeCleanImage() {
        return beforeCleanImage;
    }

    public void setBeforeCleanImage(ArrayList<String> beforeCleanImage) {
        this.beforeCleanImage = beforeCleanImage;
    }

    public ArrayList<String> getAfterCleanImage() {
        return afterCleanImage;
    }

    public void setAfterCleanImage(ArrayList<String> afterCleanImage) {
        this.afterCleanImage = afterCleanImage;
    }
    public static class CleanScheduleInfo implements Serializable{

        @SerializedName("cleaning_schedule_id")
        @Expose
        private String cleaningScheduleId;
        @SerializedName("start_time")
        @Expose
        private String startTime;
        @SerializedName("end_time")
        @Expose
        private String endTime;
        @SerializedName("cleaning_date")
        @Expose
        private String cleaningDate;
        @SerializedName("cleaning_start_dt")
        @Expose
        private String cleaningStartDt;
        @SerializedName("cleaning_end_dt")
        @Expose
        private String cleaningEndDt;
        @SerializedName("cleaner_schedule_status")
        @Expose
        private String cleanerScheduleStatus;
        @SerializedName("unit_no")
        @Expose
        private String unitNo;

        public String getCleaningScheduleId() {
            return cleaningScheduleId;
        }

        public void setCleaningScheduleId(String cleaningScheduleId) {
            this.cleaningScheduleId = cleaningScheduleId;
        }

        public String getStartTime() {
            return startTime;
        }

        public void setStartTime(String startTime) {
            this.startTime = startTime;
        }

        public String getEndTime() {
            return endTime;
        }

        public void setEndTime(String endTime) {
            this.endTime = endTime;
        }

        public String getCleaningDate() {
            return cleaningDate;
        }

        public void setCleaningDate(String cleaningDate) {
            this.cleaningDate = cleaningDate;
        }

        public String getCleaningStartDt() {
            return cleaningStartDt;
        }

        public void setCleaningStartDt(String cleaningStartDt) {
            this.cleaningStartDt = cleaningStartDt;
        }

        public String getCleaningEndDt() {
            return cleaningEndDt;
        }

        public void setCleaningEndDt(String cleaningEndDt) {
            this.cleaningEndDt = cleaningEndDt;
        }

        public String getCleanerScheduleStatus() {
            return cleanerScheduleStatus;
        }

        public void setCleanerScheduleStatus(String cleanerScheduleStatus) {
            this.cleanerScheduleStatus = cleanerScheduleStatus;
        }

        public String getUnitNo() {
            return unitNo;
        }

        public void setUnitNo(String unitNo) {
            this.unitNo = unitNo;
        }

    }

    public static class EmployeeInfo implements Serializable{

        @SerializedName("employee_id")
        @Expose
        private String employeeId;
        @SerializedName("user_id")
        @Expose
        private String userId;
        @SerializedName("employee_full_name")
        @Expose
        private String employeeFullName;
        @SerializedName("employee_phone_number")
        @Expose
        private String employeePhoneNumber;
        @SerializedName("employee_profile_photo")
        @Expose
        private String employeeProfilePhoto;

        public String getEmployeeId() {
            return employeeId;
        }

        public void setEmployeeId(String employeeId) {
            this.employeeId = employeeId;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getEmployeeFullName() {
            return employeeFullName;
        }

        public void setEmployeeFullName(String employeeFullName) {
            this.employeeFullName = employeeFullName;
        }

        public String getEmployeePhoneNumber() {
            return employeePhoneNumber;
        }

        public void setEmployeePhoneNumber(String employeePhoneNumber) {
            this.employeePhoneNumber = employeePhoneNumber;
        }

        public String getEmployeeProfilePhoto() {
            return employeeProfilePhoto;
        }

        public void setEmployeeProfilePhoto(String employeeProfilePhoto) {
            this.employeeProfilePhoto = employeeProfilePhoto;
        }

    }

    public static class CleanerRatingInfo implements Serializable {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("rating_star")
        @Expose
        private String ratingStar;
        @SerializedName("comment")
        @Expose
        private String comment;
        @SerializedName("cleaner_rating_images")
        @Expose
        private ArrayList<String> cleanerRatingImages = null;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getRatingStar() {
            return ratingStar;
        }

        public void setRatingStar(String ratingStar) {
            this.ratingStar = ratingStar;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        public ArrayList<String> getCleanerRatingImages() {
            return cleanerRatingImages;
        }

        public void setCleanerRatingImages(ArrayList<String> cleanerRatingImages) {
            this.cleanerRatingImages = cleanerRatingImages;
        }

    }


    public static class AssignerUserInfo implements Serializable{

        @SerializedName("user_id")
        @Expose
        private String userId;
        @SerializedName("employee_id")
        @Expose
        private String employeeId;
        @SerializedName("employee_full_name")
        @Expose
        private String employeeFullName;
        @SerializedName("employee_phone_number")
        @Expose
        private String employeePhoneNumber;
        @SerializedName("employee_profile_photo")
        @Expose
        private String employeeProfilePhoto;

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getEmployeeId() {
            return employeeId;
        }

        public void setEmployeeId(String employeeId) {
            this.employeeId = employeeId;
        }

        public String getEmployeeFullName() {
            return employeeFullName;
        }

        public void setEmployeeFullName(String employeeFullName) {
            this.employeeFullName = employeeFullName;
        }

        public String getEmployeePhoneNumber() {
            return employeePhoneNumber;
        }

        public void setEmployeePhoneNumber(String employeePhoneNumber) {
            this.employeePhoneNumber = employeePhoneNumber;
        }

        public String getEmployeeProfilePhoto() {
            return employeeProfilePhoto;
        }

        public void setEmployeeProfilePhoto(String employeeProfilePhoto) {
            this.employeeProfilePhoto = employeeProfilePhoto;
        }

    }
}
