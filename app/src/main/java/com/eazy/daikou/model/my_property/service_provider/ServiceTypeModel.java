package com.eazy.daikou.model.my_property.service_provider;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ServiceTypeModel implements Serializable {
    @SerializedName("service_category")
    @Expose
    private String serviceCategory;
    @SerializedName("service_category_key")
    @Expose
    private String serviceCategoryKey;

    private boolean isClick;

    public boolean isClick() {
        return isClick;
    }

    public void setClick(boolean click) {
        isClick = click;
    }


    public String getServiceCategory() {
        return serviceCategory;
    }

    public void setServiceCategory(String serviceCategory) {
        this.serviceCategory = serviceCategory;
    }

    public String getServiceCategoryKey() {
        return serviceCategoryKey;
    }

    public void setServiceCategoryKey(String serviceCategoryKey) {
        this.serviceCategoryKey = serviceCategoryKey;
    }
}

