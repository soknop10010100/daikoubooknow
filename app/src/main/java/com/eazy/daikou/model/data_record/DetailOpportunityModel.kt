package com.eazy.daikou.model.data_record

import java.io.Serializable

class DetailOpportunityModel : Serializable {
    var id: String? = null
    var name: String? = null
    var created_at: String? = null
    var status : String? = null
    var account : Account? = null
    var recommends : ArrayList<Interesting> = ArrayList()
    var interestings : ArrayList<Interesting> = ArrayList()
    var quotations : ArrayList<Quotation>  = ArrayList()
    var all_status : ArrayList<AllStatus> = ArrayList()
}

class Account : Serializable{
    var id : String? = null
    var name : String? = null
}

class Unit : Serializable{
    var id: String? = null
    var name: String? = null
    var floor_no: String? = null
    var price: String? = null
    var status : String? = null
    var type : Type? = null
}

class Type : Serializable{
    var id: String? = null
    var name: String? = null
    var total_bedroom: String? = null
    var total_bathroom: String? = null
    var total_living_room: String? = null
    var total_maid_room: String? = null
    var total_area_sqm: String? = null
    var total_private_area: String? = null
    var total_common_area: String? = null
}

class Interesting : Serializable{
    var id: String? = null
    var no : String? = null
    var create_at: String? = null
    var status: String? = null
    var price: String? = null
    var unit : Unit? = null

}

class Quotation : Serializable{
    var id: String? = null
    var no : String? = null
    var created_at: String? = null
    var price: String? = null
    var status: String? = null
    var unit : Unit? = null

}

class AllStatus : Serializable{
    var status : String? = null
    var status_display : String? = null
    var is_active: Boolean = false
}