package com.eazy.daikou.model.buy_sell

import java.io.Serializable

class BuySaleRentModel : Serializable {
    var properties : ArrayList<SaleAndRentProperties> = ArrayList()
}
class SaleAndRentProperties : Serializable{
    var id : String? = null
    var title : String? = null
    var address : String? = null
    var total_bedrooms : String? = null
    var total_bathrooms : String? = null
    var width : String? = null
    var length : String? = null
    var size : String? = null
    var price : String? = null
    var category : String? = null
    var property_type : String? = null
    var image : String? = null

}