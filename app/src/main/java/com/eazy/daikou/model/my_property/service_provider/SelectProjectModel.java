package com.eazy.daikou.model.my_property.service_provider;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SelectProjectModel implements Serializable {
    @SerializedName("branch_id")
    @Expose
    private String branchId;
    @SerializedName("branch_name")
    @Expose
    private String branchName;
    @SerializedName("file_path")
    @Expose
    private String filePath;
    @SerializedName("location")
    @Expose
    private Location location;

    @SerializedName("property_id")
    @Expose
    private String propertyId;
    @SerializedName("building_name")
    @Expose
    private String buildingName;


    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getBuildingName() {
        return buildingName;
    }

    public void setBuildingName(String buildingName) {
        this.buildingName = buildingName;
    }

    public String getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(String propertyId) {
        this.propertyId = propertyId;
    }

    public static class Location implements Serializable {

        @SerializedName("coord_lat")
        @Expose
        private double coordLat;
        @SerializedName("coord_long")
        @Expose
        private double coordLong;

        public double getCoordLat() {
            return coordLat;
        }

        public void setCoordLat(double coordLat) {
            this.coordLat = coordLat;
        }

        public double getCoordLong() {
            return coordLong;
        }

        public void setCoordLong(double coordLong) {
            this.coordLong = coordLong;
        }
    }
}
