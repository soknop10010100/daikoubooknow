package com.eazy.daikou.model.contact;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ContactProperty implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("department_name")
    @Expose
    private String department_name;
    @SerializedName("telegram")
    @Expose
    private String telegram;
    @SerializedName("whatsapp")
    @Expose
    private String whatsapp;
    @SerializedName("wechat")
    @Expose
    private String wechat;
    @SerializedName("phone_number")
    @Expose
    private String phone_number;

    public String getId() {
        return id;
    }

    public String getDepartment_name() {
        return department_name;
    }

    public String getTelegram() {
        return telegram;
    }

    public String getWhatsapp() {
        return whatsapp;
    }

    public String getWechat() {
        return wechat;
    }

    public String getPhone_number() {
        return phone_number;
    }
}
