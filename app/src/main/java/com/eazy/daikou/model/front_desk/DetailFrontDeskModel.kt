package com.eazy.daikou.model.front_desk

import java.io.Serializable

class DetailFrontDeskModel : Serializable {

    var id : String? = null
    var status : String? = null
    var full_phone : String? = null
    var description : String? = null
    var qr_code_data : String? = null
    var created_at : String? = null
    var delivery_name : String? = null
    var delivery_phone : String? = null
    var no : String? = null
    val images : ArrayList<String> = ArrayList()
    var user : User? = null
    var creator_user : CreateUser? = null
    var account : Account? = null
    var pickers : ArrayList<Pickers> = ArrayList()

}
class User : Serializable{
    var id : String? = null
    var name : String? = null
    var image : String? = null
}
class CreateUser : Serializable{
    var id  : String? = null
    var name : String? = null
    var image : String? = null
}
class Account : Serializable{
    var id : String? = null
    var name : String? = null
}
class Pickers : Serializable{
    var id : String? = null
    var user_id : String? = null
    var status : String? = null
    var user_name : String? = null
    var created_at : String? = null
    var image : String? = null
    var phone : String? = null
}
