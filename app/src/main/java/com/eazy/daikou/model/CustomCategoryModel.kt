package com.eazy.daikou.model

import java.io.Serializable

class CustomCategoryModel : Serializable {
    constructor(id: String?, name: String?, isClick: Boolean) {
        this.id = id
        this.name = name
        this.isClick = isClick
    }

    constructor(id: String?, name: String?) {
        this.id = id
        this.name = name
    }

    constructor()

    var id : String?= null
    var name : String?= null
    var isClick : Boolean = false
}