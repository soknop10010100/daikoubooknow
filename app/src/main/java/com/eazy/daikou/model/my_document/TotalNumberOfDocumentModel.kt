package com.eazy.daikou.model.my_document

import java.io.Serializable

class TotalNumberOfDocumentModel : Serializable {
    var document_category : String ?= null
    var total_amount : String ?= null
}