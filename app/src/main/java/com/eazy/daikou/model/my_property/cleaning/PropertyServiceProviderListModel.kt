package com.eazy.daikou.model.my_property.cleaning

class PropertyServiceProviderListModel {
    var id: String? = null
    var reg_no: String? = null
    var unit_id_fk: String? = null
    var unit_no: String? = null
    var reg_start_date: String? = null
    var reg_end_date: String? = null
    var reg_start_time: String? = null
    var reg_end_time: String? = null
    var reg_tax_amount: String? = null
    var reg_total_amount: String? = null
    var reg_status: String? = null
    var sv_name: String? = null

}
