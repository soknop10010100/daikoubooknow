package com.eazy.daikou.model.data_record

class DetailHoldAndReservedModel : java.io.Serializable{
    var id : String? = null
    var spa_no : String? = null
    var code : String? = null
    var amount : String? = null
    var discount : String? = null
    var voucherAmountTv : String? = null
    var type : String? = null
    var start_date : String? = null
    var end_date : String? = null
    var end_time : String? = null
    var status : String? = null
    var created_at : String? = null
    var invoice_receipt_url : String? = null
    var payment_kess_url : String? = null
    var total_payment_amount : String? = null
    var total_interest_amount : String? = null
    var total_amount : String? = null
    var purchase_date : String? = null
    var payment_option : String? = null

    var account : Account? = null
    var unit : UnitHold? = null
    var quotation : QuotationModel? = null
    var payment_schedules : ArrayList<PaymentSchedule> = ArrayList()
    var all_status : ArrayList<AllStatus> = ArrayList()
    var booking : BookingModel? = null
    var customer : CustomerModel? = null
    var hold_and_reserve : HoldAndReservedModel? = null
}
class HoldAndReservedModel : java.io.Serializable{
    var id : String? = null
    var code : String? = null
    var amount : String? = null
    var type : String? = null
}

class UnitHold : java.io.Serializable{
    var id : String? = null
    var name : String? = null
    var floor_no : String? = null
    var price : String? = null
    var direction : String? = null
    var unit_stair : String? = null
    var unit_position : String? = null
    var street_id_fk : String? = null
    var qr_code : String? = null
    var type : TypeHold? = null
}

class  TypeHold : java.io.Serializable{
    var id : String? = null
    var name : String? = null
    var from_floor : String? = null
    var category : String? = null
    var total_bedroom : String? = null
    var total_bathroom : String? = null
    var total_living_room : String? = null
    var total_maid_room : String? = null
    var total_area_sqm : String? = null
    var total_private_area : String? = null
    var total_common_area : String? = null
}

class QuotationModel : java.io.Serializable{
    var id : String? = null
    var price : String? = null
    var sale_price : String? = null
    var email : String? = null
    var phone : String? = null
    var priority : String? = null
    var voucher_amount : String? = null
    var lead_source : String? = null
    var expire_date : String? = null
    var discount : String? = null
    var property_unit_no_id : String? = null
    var payment_term_id : String? = null
    var created_at : String? = null
    var no : String? = null
    var channel_name : String? = null
    var supporting_team : String? = null
    var payment_term_type : String? = null
    var option : String? = null
    var account : Account? = null
}

class BookingModel : java.io.Serializable{
    var id : String? = null
    var code : String? = null
    var discount : String? = null
    var voucher_amount : String? = null
    var amount : String? = null
    var sale_price : String? = null
    var start_date : String? = null
    var end_date : String? = null
    var end_time : String? = null
    var channel : ChannelModel? = null
    var broker : BrokerModel? = null
    var agency : AgencyModel? = null
    var customer : CustomerModel? = null
    var unit : UnitHold? = null
    var supporting_team : SupportingTeam? = null
}

class AgencyModel : java.io.Serializable{
    var id : String? = null
    var name : String? = null
}

class BrokerModel : java.io.Serializable{
    var id : String? = null
    var name : String? = null
}

class ChannelModel : java.io.Serializable{
    var id : String? = null
    var name : String? = null
}

class SupportingTeam : java.io.Serializable{
    var id : String? = null
    var name : String? = null
}

class CustomerModel : java.io.Serializable{
    var id : String? = null
    var name : String? = null
}
