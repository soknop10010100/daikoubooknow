package com.eazy.daikou.model.evaluate

class EvaluateModel : java.io.Serializable{

    var id : String? = null
    var satisfaction : String? = null
    var comment : String? = null
    var unit : UnitModel? = null
    var account : AccountModel? = null
    var user : UserModel? = null
    var created_dt : String? = null
    var images : ArrayList<String> = ArrayList()
}

class UserModel : java.io.Serializable{
    var id : String? = null
    var name : String? = null
}

class UnitModel : java.io.Serializable{
    var id : String? = null
    var name : String? = null
}

class AccountModel : java.io.Serializable{
    var id : String? = null
    var name : String? = null
}
