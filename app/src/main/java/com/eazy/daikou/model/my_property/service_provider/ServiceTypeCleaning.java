package com.eazy.daikou.model.my_property.service_provider;

import com.eazy.daikou.model.my_property.service_property_employee.AllService;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ServiceTypeCleaning implements Serializable {

    @SerializedName("service_type")
    @Expose
    private String serviceType;
    @SerializedName("all_services")
    @Expose
    private List<AllService> allServices = null;

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public List<AllService> getAllServices() {
        return allServices;
    }

    public void setAllServices(List<AllService> allServices) {
        this.allServices = allServices;
    }


}
