package com.eazy.daikou.model.utillity_tracking.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class W_ETrackingModel implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("tracking_date")
    @Expose
    private String trackingDate;
    @SerializedName("created_date")
    @Expose
    private String createdDate;
    @SerializedName("total_usage")
    @Expose
    private Object totalUsage;
    @SerializedName("previous_date")
    @Expose
    private Object previousDate;
    @SerializedName("recorded_number")
    @Expose
    private String recordedNumber;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTrackingDate() {
        return trackingDate;
    }

    public void setTrackingDate(String trackingDate) {
        this.trackingDate = trackingDate;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public Object getTotalUsage() {
        return totalUsage;
    }

    public void setTotalUsage(Object totalUsage) {
        this.totalUsage = totalUsage;
    }

    public Object getPreviousDate() {
        return previousDate;
    }

    public void setPreviousDate(Object previousDate) {
        this.previousDate = previousDate;
    }

    public String getRecordedNumber() {
        return recordedNumber;
    }

    public void setRecordedNumber(String recordedNumber) {
        this.recordedNumber = recordedNumber;
    }


}
