package com.eazy.daikou.model.service_provider

import java.io.Serializable

class MainItemModel : Serializable{
    constructor(action: String?, titleHeader: String?) {
        this.action = action
        this.titleHeader = titleHeader
        this.description = description
    }

    var action: String? = null
    var titleHeader: String? = null
    var description: String? = null
    var subCategoryList: ArrayList<SubCategory> = ArrayList()
}

class SubCategory : Serializable{
    var action: String? = null
    // category part
    var id: String? = null
    var title: String? = null
    var urlImage: String? = null
    var city: String? = null
    var complyInfo : SubCompanyInfo? = null
    var priceDisplay : String? = null
}

class SubCompanyInfo : Serializable{
    var id: String? = null
    var title: String? = null
    var urlImage: String? = null
}

class ImageSliderModel: Serializable{
    var account_id: String? = null
    var image: String? = null
    var url: String? = null
}