package com.eazy.daikou.model.hr

import com.eazy.daikou.model.my_property.service_provider.MenuServiceProvider
import java.io.Serializable

class ItemHR :Serializable {
    var id : Int = 0
    var action: String? = null
    var image : Int =0
    var nameItem : String? = null
    var descriptionItem : String? = null
    var color : Int = 0

    constructor(id: Int, image: Int, nameItem: String, descriptionItem: String) {
        this.id = id
        this.image = image
        this.nameItem = nameItem
        this.descriptionItem = descriptionItem
    }

    constructor(action: String, image: Int, nameItem: String, descriptionItem: String, color : Int) {
        this.action = action
        this.image = image
        this.nameItem = nameItem
        this.descriptionItem = descriptionItem
        this.color = color
    }
}