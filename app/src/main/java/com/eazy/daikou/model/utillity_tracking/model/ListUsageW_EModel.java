package com.eazy.daikou.model.utillity_tracking.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ListUsageW_EModel implements Serializable {

    @SerializedName("tracking_data")
    @Expose
    private List<TrackingDatum> trackingData = null;
    @SerializedName("total_usage")
    @Expose
    private TotalUsage totalUsage;

    public List<TrackingDatum> getTrackingData() {
        return trackingData;
    }

    public void setTrackingData(List<TrackingDatum> trackingData) {
        this.trackingData = trackingData;
    }

    public TotalUsage getTotalUsage() {
        return totalUsage;
    }

    public void setTotalUsage(TotalUsage totalUsage) {
        this.totalUsage = totalUsage;
    }

    public class TotalUsage implements Serializable{

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("from_date")
        @Expose
        private String fromDate;
        @SerializedName("to_date")
        @Expose
        private String toDate;
        @SerializedName("total_price")
        @Expose
        private String totalPrice;
        @SerializedName("total_usage")
        @Expose
        private String totalUsage;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getFromDate() {
            return fromDate;
        }

        public void setFromDate(String fromDate) {
            this.fromDate = fromDate;
        }

        public String getToDate() {
            return toDate;
        }

        public void setToDate(String toDate) {
            this.toDate = toDate;
        }

        public String getTotalPrice() {
            return totalPrice;
        }

        public void setTotalPrice(String totalPrice) {
            this.totalPrice = totalPrice;
        }

        public String getTotalUsage() {
            return totalUsage;
        }

        public void setTotalUsage(String totalUsage) {
            this.totalUsage = totalUsage;
        }
    }

    public class TrackingDatum implements Serializable{

        @SerializedName("total_usage")
        @Expose
        private String totalUsage;
        @SerializedName("tracking_date")
        @Expose
        private String trackingDate;

        public String getTotalUsage() {
            return totalUsage;
        }

        public void setTotalUsage(String totalUsage) {
            this.totalUsage = totalUsage;
        }

        public String getTrackingDate() {
            return trackingDate;
        }

        public void setTrackingDate(String trackingDate) {
            this.trackingDate = trackingDate;
        }

    }

}
