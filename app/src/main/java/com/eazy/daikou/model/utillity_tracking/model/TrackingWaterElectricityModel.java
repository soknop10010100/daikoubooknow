package com.eazy.daikou.model.utillity_tracking.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class TrackingWaterElectricityModel implements Serializable {

    @SerializedName("current_usage")
    @Expose
    private String currentUsage;
    @SerializedName("total_usage")
    @Expose
    private String totalUsage;
    @SerializedName("utility_photo_file")
    @Expose
    private String trackingImage;
    @SerializedName("previous_usage")
    @Expose
    private String previousUsage;

    public String getPreviousUsage() {
        return previousUsage;
    }

    public void setPreviousUsage(String previousUsage) {
        this.previousUsage = previousUsage;
    }

    public String getCurrentUsage() {
        return currentUsage;
    }

    public void setCurrentUsage(String currentUsage) {
        this.currentUsage = currentUsage;
    }

    public String getTotalUsage() {
        return totalUsage;
    }

    public void setTotalUsage(String totalUsage) {
        this.totalUsage = totalUsage;
    }

    public String getTrackingImage() {
        return trackingImage;
    }

    public void setTrackingImage(String trackingImage) {
        this.trackingImage = trackingImage;
    }
}
