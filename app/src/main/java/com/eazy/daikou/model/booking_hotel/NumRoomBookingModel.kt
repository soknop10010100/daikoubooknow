package com.eazy.daikou.model.booking_hotel

import java.io.Serializable

class NumRoomBookingModel(var numValues: String?, var action: String?) : Serializable {
    var room : String? = null
    var adults : String? = null
    var children : String? = null
}
