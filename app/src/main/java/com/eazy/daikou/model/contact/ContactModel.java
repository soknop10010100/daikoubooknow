package com.eazy.daikou.model.contact;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ContactModel implements Serializable {

    int id;
    String Email;
    int useId;

    @SerializedName("eid")
    @Expose
    private String eid;
    @SerializedName("department_id")
    @Expose
    private String departmentId;
    @SerializedName("department")
    @Expose
    private String department;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("e_job_title")
    @Expose
    private String eJobTitle;
    @SerializedName("work_time")
    @Expose
    private String workTime;
    @SerializedName("e_email")
    @Expose
    private String eEmail;
    @SerializedName("phonecode")
    @Expose
    private String phonecode;
    @SerializedName("e_contact")
    @Expose
    private String eContact;
    @SerializedName("e_telegram_addr")
    @Expose
    private String eTelegramAddr;
    @SerializedName("e_we_chat")
    @Expose
    private Object eWeChat;
    @SerializedName("e_whatsapp_addr")
    @Expose
    private Object eWhatsappAddr;
    @SerializedName("working_hour_from")
    @Expose
    private String workingHourFrom;
    @SerializedName("working_hour_to")
    @Expose
    private String workingHourTo;
    @SerializedName("working_days")
    @Expose
    private String workingDays;


    public void setId(int id) {

        this.id = id;
    }

    public void setUseId(int useId) {
        this.useId = useId;
    }

    public String getEid() {
        return eid;
    }

    public void setEid(String eid) {
        this.eid = eid;
    }

    public String geteJobTitle() {
        return eJobTitle;
    }

    public void seteJobTitle(String eJobTitle) {
        this.eJobTitle = eJobTitle;
    }

    public String getWorkTime() {
        return workTime;
    }

    public void setWorkTime(String workTime) {
        this.workTime = workTime;
    }

    public String geteEmail() {
        return eEmail;
    }

    public void seteEmail(String eEmail) {
        this.eEmail = eEmail;
    }

    public String getPhonecode() {
        return phonecode;
    }

    public void setPhonecode(String phonecode) {
        this.phonecode = phonecode;
    }

    public String geteContact() {
        return eContact;
    }

    public void seteContact(String eContact) {
        this.eContact = eContact;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String geteTelegramAddr() {
        return eTelegramAddr;
    }

    public void seteTelegramAddr(String eTelegramAddr) {
        this.eTelegramAddr = eTelegramAddr;
    }

    public Object geteWeChat() {
        return eWeChat;
    }

    public void seteWeChat(Object eWeChat) {
        this.eWeChat = eWeChat;
    }

    public Object geteWhatsappAddr() {
        return eWhatsappAddr;
    }

    public void seteWhatsappAddr(Object eWhatsappAddr) {
        this.eWhatsappAddr = eWhatsappAddr;
    }

    public String getWorkingHourFrom() {
        return workingHourFrom;
    }

    public void setWorkingHourFrom(String workingHourFrom) {
        this.workingHourFrom = workingHourFrom;
    }

    public String getWorkingHourTo() {
        return workingHourTo;
    }

    public void setWorkingHourTo(String workingHourTo) {
        this.workingHourTo = workingHourTo;
    }

    public String getWorkingDays() {
        return workingDays;
    }

    public void setWorkingDays(String workingDays) {
        this.workingDays = workingDays;
    }

    public ContactModel(int id, String firstName, String lastName, String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        Email = email;
        this.id = id;
    }

    public ContactModel(String firstName, String lastName, String email, int userId) {
        this.firstName = firstName;
        this.lastName = lastName;
        Email = email;
        this.useId = userId;
    }

    public ContactModel() {
    }

    public int getUseId() {
        return useId;
    }

    @Override
    public String toString() {
        return "ContactModel{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", Email='" + Email + '\'' +
                '}';
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public int getId() {
        return id;
    }
}
