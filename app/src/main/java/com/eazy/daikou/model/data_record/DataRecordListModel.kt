package com.eazy.daikou.model.data_record

class SubListDataRecord : java.io.Serializable{
    var id : String? = null
    var no : String? = null
    var projectName : String? = null
    var createdDate : String? = null
    var opportunityName : String? = null
    var status : String? = null
    var unitType : String? = null
    var unitNo : String? = null
    var userName : String? = null
    var price : String? = null
    var amount : String? = null
    var type : String? = null
    var startDate : String? = null
    var endDate : String? = null
    var endTime : String? = null
    var streetFloor : String? = null
    var expire_date : String? = null
    //Spa
    var discount : String? = null
    var payment_tern_type : String? = null
    var purchase_date : String? = null
}