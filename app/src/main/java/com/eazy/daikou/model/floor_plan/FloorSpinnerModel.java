package com.eazy.daikou.model.floor_plan;

import java.io.Serializable;

public class FloorSpinnerModel implements Serializable{
    private final String floorName;
    private final String numberRecord;
    private boolean isClick;

    public FloorSpinnerModel(String floorName, String numberRecord) {
        this.floorName = floorName;
        this.numberRecord = numberRecord;
    }

    public String getNumberRecord() {
        return numberRecord;
    }

    public String getFloorName() {
        return floorName;
    }

    public boolean isClick() {
        return isClick;
    }

    public void setClick(boolean click) {
        isClick = click;
    }
}
