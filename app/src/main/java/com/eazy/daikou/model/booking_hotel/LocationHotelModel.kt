package com.eazy.daikou.model.booking_hotel

import java.io.Serializable

class LocationHotelModel : Serializable {
    var location_id : String? = null
    var location_name : String? = null
    var image : String? = null
    var total_hotels : String? = null
}

class LocationModel : Serializable {
    var id : String? = null
    var name : String? = null
}