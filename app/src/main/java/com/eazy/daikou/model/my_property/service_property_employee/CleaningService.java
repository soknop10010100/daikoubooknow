package com.eazy.daikou.model.my_property.service_property_employee;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CleaningService implements Serializable {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("property_id")
    @Expose
    private String propertyId;
    @SerializedName("unit_id")
    @Expose
    private String unitId;
    @SerializedName("start_date")
    @Expose
    private String startDate;
    @SerializedName("end_date")
    @Expose
    private String endDate;
    @SerializedName("start_time")
    @Expose
    private String startTime;
    @SerializedName("service_type")
    @Expose
    private String serviceType;
    @SerializedName("service_term")
    @Expose
    private String serviceTerm;
    @SerializedName("service_fee")
    @Expose
    private String serviceFee;
    @SerializedName("service_tax")
    @Expose
    private String serviceTax;
    @SerializedName("total_price")
    @Expose
    private String totalPrice;
    @SerializedName("reschedule_status")
    @Expose
    private String rescheduleStatus;
    @SerializedName("service_id")
    @Expose
    private String serviceId;
    @SerializedName("bill_item_id")
    @Expose
    private String billItemId;
    @SerializedName("created_date")
    @Expose
    private String createdDate;
    @SerializedName("unit_no")
    @Expose
    private String unitNo;
    @SerializedName("cleaning_service_name")
    @Expose
    private String cleaningServiceName;
    @SerializedName("cleaning_service_type")
    @Expose
    private String cleaningServiceType;
    @SerializedName("sale_order_id")
    @Expose
    private String saleOrderId;
    @SerializedName("total_week")
    @Expose
    private String totalWeek;
    @SerializedName("service_fee_id")
    @Expose
    private String serviceFeeId;
    @SerializedName("payment_status")
    @Expose
    private String paymentStatus;
    @SerializedName("payment_method")
    @Expose
    private Object paymentMethod;
    @SerializedName("start_time_working")
    @Expose
    private String startTimeWorking;
    @SerializedName("end_time_working")
    @Expose
    private String endTimeWorking;

    @SerializedName("schedule_cancelled_reason")
    @Expose
    private Object schedule_cancelled_reason;
    @SerializedName("cleaning_period")
    @Expose
    private int cleaningPeriod;
    @SerializedName("available_day")
    @Expose
    private ArrayList<String> availableDay = null;
    @SerializedName("start_time_in_minute")
    @Expose
    private int startTimeInMinute;
    @SerializedName("end_time_in_minute")
    @Expose
    private int endTimeInMinute;
    @SerializedName("creater_user_detail")
    @Expose
    private CreaterUserDetail createrUserDetail;
    @SerializedName("employee_user_detail")
    @Expose
    private EmployeeUserDetail employeeUserDetail;
    @SerializedName("employee_assigned_detail")
    @Expose
    private ArrayList<EmployeeAssignedDetail> employeeAssignedDetail = null;
    @SerializedName("reschedule_history_list")
    @Expose
    private ArrayList<RescheduleHistory> rescheduleHistoryList = null;

    public Object getSchedule_cancelled_reason() {
        return schedule_cancelled_reason;
    }

    public void setSchedule_cancelled_reason(Object schedule_cancelled_reason) {
        this.schedule_cancelled_reason = schedule_cancelled_reason;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(String propertyId) {
        this.propertyId = propertyId;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getServiceTerm() {
        return serviceTerm;
    }

    public void setServiceTerm(String serviceTerm) {
        this.serviceTerm = serviceTerm;
    }

    public String getServiceFee() {
        return serviceFee;
    }

    public void setServiceFee(String serviceFee) {
        this.serviceFee = serviceFee;
    }

    public String getServiceTax() {
        return serviceTax;
    }

    public void setServiceTax(String serviceTax) {
        this.serviceTax = serviceTax;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getRescheduleStatus() {
        return rescheduleStatus;
    }

    public void setRescheduleStatus(String rescheduleStatus) {
        this.rescheduleStatus = rescheduleStatus;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getBillItemId() {
        return billItemId;
    }

    public void setBillItemId(String billItemId) {
        this.billItemId = billItemId;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getUnitNo() {
        return unitNo;
    }

    public void setUnitNo(String unitNo) {
        this.unitNo = unitNo;
    }

    public String getCleaningServiceName() {
        return cleaningServiceName;
    }

    public void setCleaningServiceName(String cleaningServiceName) {
        this.cleaningServiceName = cleaningServiceName;
    }

    public String getCleaningServiceType() {
        return cleaningServiceType;
    }

    public void setCleaningServiceType(String cleaningServiceType) {
        this.cleaningServiceType = cleaningServiceType;
    }

    public String getSaleOrderId() {
        return saleOrderId;
    }

    public void setSaleOrderId(String saleOrderId) {
        this.saleOrderId = saleOrderId;
    }

    public String getTotalWeek() {
        return totalWeek;
    }

    public void setTotalWeek(String totalWeek) {
        this.totalWeek = totalWeek;
    }

    public String getServiceFeeId() {
        return serviceFeeId;
    }

    public void setServiceFeeId(String serviceFeeId) {
        this.serviceFeeId = serviceFeeId;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public Object getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(Object paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getStartTimeWorking() {
        return startTimeWorking;
    }

    public void setStartTimeWorking(String startTimeWorking) {
        this.startTimeWorking = startTimeWorking;
    }

    public String getEndTimeWorking() {
        return endTimeWorking;
    }

    public void setEndTimeWorking(String endTimeWorking) {
        this.endTimeWorking = endTimeWorking;
    }

    public int getCleaningPeriod() {
        return cleaningPeriod;
    }

    public void setCleaningPeriod(int cleaningPeriod) {
        this.cleaningPeriod = cleaningPeriod;
    }

    public ArrayList<String> getAvailableDay() {
        return availableDay;
    }

    public void setAvailableDay(ArrayList<String> availableDay) {
        this.availableDay = availableDay;
    }

    public int getStartTimeInMinute() {
        return startTimeInMinute;
    }

    public void setStartTimeInMinute(int startTimeInMinute) {
        this.startTimeInMinute = startTimeInMinute;
    }

    public int getEndTimeInMinute() {
        return endTimeInMinute;
    }

    public void setEndTimeInMinute(int endTimeInMinute) {
        this.endTimeInMinute = endTimeInMinute;
    }

    public CreaterUserDetail getCreaterUserDetail() {
        return createrUserDetail;
    }

    public void setCreaterUserDetail(CreaterUserDetail createrUserDetail) {
        this.createrUserDetail = createrUserDetail;
    }

    public EmployeeUserDetail getEmployeeUserDetail() {
        return employeeUserDetail;
    }

    public void setEmployeeUserDetail(EmployeeUserDetail employeeUserDetail) {
        this.employeeUserDetail = employeeUserDetail;
    }

    public ArrayList<EmployeeAssignedDetail> getEmployeeAssignedDetail() {
        return employeeAssignedDetail;
    }

    public void setEmployeeAssignedDetail(ArrayList<EmployeeAssignedDetail> employeeAssignedDetail) {
        this.employeeAssignedDetail = employeeAssignedDetail;
    }

    public ArrayList<RescheduleHistory> getRescheduleHistoryList() {
        return rescheduleHistoryList;
    }

    public void setRescheduleHistoryList(ArrayList<RescheduleHistory> rescheduleHistoryList) {
        this.rescheduleHistoryList = rescheduleHistoryList;
    }
    public static class EmployeeAssignedDetail implements Serializable {

        @SerializedName("schedule_id")
        @Expose
        private String scheduleId;
        @SerializedName("cleaning_date")
        @Expose
        private String cleaningDate;
        @SerializedName("all_cleaners_detail")
        @Expose
        private ArrayList<AllCleanersDetail> allCleanersDetail = null;

        public String getScheduleId() {
            return scheduleId;
        }

        public void setScheduleId(String scheduleId) {
            this.scheduleId = scheduleId;
        }

        public String getCleaningDate() {
            return cleaningDate;
        }

        public void setCleaningDate(String cleaningDate) {
            this.cleaningDate = cleaningDate;
        }

        public ArrayList<AllCleanersDetail> getAllCleanersDetail() {
            return allCleanersDetail;
        }

        public void setAllCleanersDetail(ArrayList<AllCleanersDetail> allCleanersDetail) {
            this.allCleanersDetail = allCleanersDetail;
        }
    }

    public static class AllCleanersDetail implements Serializable{

        @SerializedName("user_id")
        @Expose
        private String userId;
        @SerializedName("employee_id")
        @Expose
        private String employeeId;
        @SerializedName("employee_full_name")
        @Expose
        private String employeeFullName;
        @SerializedName("employee_phone_number")
        @Expose
        private String employeePhoneNumber;
        @SerializedName("employee_profile_photo")
        @Expose
        private String employeeProfilePhoto;
        @SerializedName("cleaning_date")
        @Expose
        private String cleaningDate;
        @SerializedName("schedule_id")
        @Expose
        private String scheduleId;

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getEmployeeId() {
            return employeeId;
        }

        public void setEmployeeId(String employeeId) {
            this.employeeId = employeeId;
        }

        public String getEmployeeFullName() {
            return employeeFullName;
        }

        public void setEmployeeFullName(String employeeFullName) {
            this.employeeFullName = employeeFullName;
        }

        public String getEmployeePhoneNumber() {
            return employeePhoneNumber;
        }

        public void setEmployeePhoneNumber(String employeePhoneNumber) {
            this.employeePhoneNumber = employeePhoneNumber;
        }

        public String getEmployeeProfilePhoto() {
            return employeeProfilePhoto;
        }

        public void setEmployeeProfilePhoto(String employeeProfilePhoto) {
            this.employeeProfilePhoto = employeeProfilePhoto;
        }

        public String getCleaningDate() {
            return cleaningDate;
        }

        public void setCleaningDate(String cleaningDate) {
            this.cleaningDate = cleaningDate;
        }

        public String getScheduleId() {
            return scheduleId;
        }

        public void setScheduleId(String scheduleId) {
            this.scheduleId = scheduleId;
        }

    }
    public static class EmployeeUserDetail implements Serializable{

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("full_name")
        @Expose
        private String fullName;
        @SerializedName("phone_number")
        @Expose
        private String phoneNumber;
        @SerializedName("profile_photo")
        @Expose
        private String profilePhoto;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getFullName() {
            return fullName;
        }

        public void setFullName(String fullName) {
            this.fullName = fullName;
        }

        public String getPhoneNumber() {
            return phoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        public String getProfilePhoto() {
            return profilePhoto;
        }

        public void setProfilePhoto(String profilePhoto) {
            this.profilePhoto = profilePhoto;
        }

    }

    public static class RescheduleHistory implements Serializable {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("rescheduler_user_id")
        @Expose
        private String reschedulerUserId;
        @SerializedName("reason")
        @Expose
        private String reason;
        @SerializedName("created_date")
        @Expose
        private String createdDate;
        @SerializedName("start_date")
        @Expose
        private String startDate;
        @SerializedName("end_date")
        @Expose
        private String endDate;
        @SerializedName("start_time")
        @Expose
        private String startTime;
        @SerializedName("end_time")
        @Expose
        private String endTime;
        @SerializedName("rescheduler_user_name")
        @Expose
        private String reschedulerUserName;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getReschedulerUserId() {
            return reschedulerUserId;
        }

        public void setReschedulerUserId(String reschedulerUserId) {
            this.reschedulerUserId = reschedulerUserId;
        }

        public String getReason() {
            return reason;
        }

        public void setReason(String reason) {
            this.reason = reason;
        }

        public String getCreatedDate() {
            return createdDate;
        }

        public void setCreatedDate(String createdDate) {
            this.createdDate = createdDate;
        }

        public String getStartDate() {
            return startDate;
        }

        public void setStartDate(String startDate) {
            this.startDate = startDate;
        }

        public String getEndDate() {
            return endDate;
        }

        public void setEndDate(String endDate) {
            this.endDate = endDate;
        }

        public String getStartTime() {
            return startTime;
        }

        public void setStartTime(String startTime) {
            this.startTime = startTime;
        }

        public String getEndTime() {
            return endTime;
        }

        public void setEndTime(String endTime) {
            this.endTime = endTime;
        }

        public String getReschedulerUserName() {
            return reschedulerUserName;
        }

        public void setReschedulerUserName(String reschedulerUserName) {
            this.reschedulerUserName = reschedulerUserName;
        }

    }

    public static class CreaterUserDetail implements Serializable{

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("first_name")
        @Expose
        private String firstName;
        @SerializedName("last_name")
        @Expose
        private String lastName;
        @SerializedName("phonecode")
        @Expose
        private String phonecode;
        @SerializedName("phone_no")
        @Expose
        private String phoneNo;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("user_profile_photo")
        @Expose
        private String userProfilePhoto;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getPhonecode() {
            return phonecode;
        }

        public void setPhonecode(String phonecode) {
            this.phonecode = phonecode;
        }

        public String getPhoneNo() {
            return phoneNo;
        }

        public void setPhoneNo(String phoneNo) {
            this.phoneNo = phoneNo;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getUserProfilePhoto() {
            return userProfilePhoto;
        }

        public void setUserProfilePhoto(String userProfilePhoto) {
            this.userProfilePhoto = userProfilePhoto;
        }

    }
    public static class CleanerRatingInfo implements Serializable {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("rating_star")
        @Expose
        private String ratingStar;
        @SerializedName("comment")
        @Expose
        private String comment;
        @SerializedName("cleaner_rating_images")
        @Expose
        private List<Object> cleanerRatingImages = null;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getRatingStar() {
            return ratingStar;
        }

        public void setRatingStar(String ratingStar) {
            this.ratingStar = ratingStar;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        public List<Object> getCleanerRatingImages() {
            return cleanerRatingImages;
        }

        public void setCleanerRatingImages(List<Object> cleanerRatingImages) {
            this.cleanerRatingImages = cleanerRatingImages;
        }

    }

}
