package com.eazy.daikou.model.home_page.home_project

import com.google.gson.annotations.SerializedName

data class PropertyDetailModel (

    @SerializedName("success" ) var success : Boolean? = null,
    @SerializedName("msg"     ) var msg     : String?  = null,
    @SerializedName("data"    ) var data    : DataDetail?    = DataDetail()

)

data class RelatedBranches (

    @SerializedName("id"      ) var id      : String? = null,
    @SerializedName("name"    ) var name    : String? = null,
    @SerializedName("logo"    ) var logo    : String? = null,
    @SerializedName("city"    ) var city    : String? = null,
    @SerializedName("address" ) var address : String? = null,
    @SerializedName("image"   ) var image   : String? = null

)

data class DataDetail(

    @SerializedName("id"               ) var id              : Int?                       = null,
    @SerializedName("name"             ) var name            : String?                    = null,
    @SerializedName("email"            ) var email           : String?                    = null,
    @SerializedName("district"         ) var district        : String?                    = null,
    @SerializedName("city"             ) var city            : String?                    = null,
    @SerializedName("country"          ) var country         : String?                    = null,
    @SerializedName("address"          ) var address         : String?                    = null,
    @SerializedName("coord_lat"        ) var coordLat        : String?                    = null,
    @SerializedName("coord_long"       ) var coordLong       : String?                    = null,
    @SerializedName("website"          ) var website         : String?                    = null,
    @SerializedName("logo"             ) var logo            : String?                    = null,
    @SerializedName("created_at"       ) var createdAt       : String?                    = null,
    @SerializedName("phone"            ) var phone           : String?                    = null,
    @SerializedName("images"           ) var images          : ArrayList<String>          = arrayListOf(),
    @SerializedName("related_branches" ) var relatedBranches : ArrayList<RelatedBranches> = arrayListOf()

)