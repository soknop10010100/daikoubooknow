package com.eazy.daikou.base;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public abstract class BaseAdapter extends RecyclerView.Adapter<BaseAdapter.MyViewHolder> {

    public int layout_id;
    protected List<?> dataList;
    Context BASE_CONTEXT;
    public View itemView;
    public int count;

    public BaseAdapter(Context context , int layout_id , List<?> dataList) {
        this.BASE_CONTEXT = context;
        this.layout_id = layout_id;
        this.dataList = dataList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(layout_id, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, int position) {
        if (dataList.size() >= position + 1){
            onBindViewHold(position, dataList.get(position));
        }

    }

    public abstract View getView(View view);

    @Override
    public int getItemCount() {
        return dataList.size() + count;
    }

    public abstract void onBindViewHold(int position, Object itemView);

    class MyViewHolder extends RecyclerView.ViewHolder {

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            BaseAdapter.this.itemView = itemView;
            getView(itemView);
        }
    }

    public <T extends View> T bind(int id) {
        return itemView.findViewById(id);
    }

}
