package com.eazy.daikou.base;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;

import androidx.activity.result.ActivityResult;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.eazy.daikou.repository_ws.Constant;
import com.eazy.daikou.helper.CheckIsAppActive;
import com.eazy.daikou.helper.UserSessionManagement;
import com.eazy.daikou.helper.Utils;

public class BaseActivity extends AppCompatActivity {

    public static double latitude;
    public static double longitude;
    public static String country_code;
    public static String DEVICE_ID;
    public static boolean isBookNowApp;
    protected final BetterActivityResult<Intent, ActivityResult> activityLauncher = BetterActivityResult.registerActivityForResult(this);

    @SuppressLint("SourceLockedOrientationActivity")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        ActionBar mActionBar = getSupportActionBar();
        if (mActionBar != null){
            getSupportActionBar().hide();
        }

        String lang = Utils.getString("language", this);
        if (lang.equals("")){
            lang = "en";
        }
        Utils.changeLanguage(this, lang);

        // change status bar text color book now
        if (!CheckIsAppActive.Companion.is_daikou_active()) {
            isBookNowApp = true;
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        UserSessionManagement.saveString(Constant.OUT_IN_APP, "true", getApplicationContext());
    }

    @Override
    protected void onPause() {
        super.onPause();
        UserSessionManagement.saveString(Constant.OUT_IN_APP, "false", getApplicationContext());
    }
}