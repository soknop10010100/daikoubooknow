package com.eazy.daikou.base;

import android.annotation.SuppressLint;
import android.content.Context;

import com.eazy.daikou.R;
import com.eazy.daikou.helper.UserSessionManagement;
import com.eazy.daikou.helper.Utils;
import com.eazy.daikou.model.parking.ColorCar;
import com.eazy.daikou.model.hr.ItemHR;
import com.eazy.daikou.model.inspection.ItemTemplateModel;
import com.eazy.daikou.model.notification.Payment;
import com.eazy.daikou.model.my_property.service_provider.ServicePropertyModel;
import com.eazy.daikou.model.profile.User;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MockUpData {
    public static Boolean UPDATE_COMPLAINT = false;
    public static Boolean ROLE_EMERGENCY = false;
    public static Boolean READ_EMERGENCY = false;
    public static Boolean ROLE_FRONT_DEST = false;
    public static Boolean ROLE_PARKING = false;
    public static Boolean ROLE_CLEANER = false;
    public static Boolean ROLE_INSPECTION_WORK_ORDER = false;
    public static boolean isRemove = false;
    public static boolean isFirstHomeScreen = true;
    public static Integer stat;
    public static Integer statPayment;
    public static Integer statColor;
    public static Integer statProperty;
    public static int SWITCH_VIEW = 2;
    public static String textPay;
    public static Integer imagePay;
    public static Boolean IS_CHECK = true;
    public static Boolean IS_CLICK = true;
    public static Boolean isCLick = true;
    public static Boolean IS_CLICK_HIDE = false;
    public static ServicePropertyModel serviceProperty;

    public static HashMap<String,Object> hashMapAccount = new HashMap<>();
    public static HashMap<String, Object> listImage = new HashMap<>();
    public static HashMap<String, List<ItemTemplateModel.SubItemTemplateModel>> listHashMap = new HashMap<>();

    //User for hotel get from home page;
    public static Boolean isDeactivated = false;

    public static Boolean getIsCheck() {
        return IS_CHECK;
    }

    public static void setIsCheck(Boolean isCheck) {
        IS_CHECK = isCheck;
    }

    public static Boolean getIsCLick() {
        return isCLick;
    }

    public static void setIsCLick(Boolean isCLick) {
        MockUpData.isCLick = isCLick;
    }

    public static Boolean getIsClick() {
        return IS_CLICK;
    }

    public static void setIsClick(Boolean isClick) {
        IS_CLICK = isClick;
    }

    public static HashMap<String, Object> getHashMapAccount() {
        return hashMapAccount;
    }

    public static void setHashMapAccount(HashMap<String, Object> hashMapAccount) { MockUpData.hashMapAccount = hashMapAccount; }

    public static Integer getStat() {
        return stat;
    }

    public static void setStat(Integer stat) {
        MockUpData.stat = stat;
    }

    public static void setStatProperty(Integer statProperty) {
        MockUpData.statProperty = statProperty;
    }

    public static String getTextPay() {
        return textPay;
    }

    public static void setTextPay(String textPay) {
        MockUpData.textPay = textPay;
    }

    public static Integer getImagePay() {
        return imagePay;
    }

    public static void setImagePay(Integer imagePay) {
        MockUpData.imagePay = imagePay;
    }

    public static Integer getStatColor() {
        return statColor;
    }

    public static void setStatColor(Integer statColor) {
        MockUpData.statColor = statColor;
    }

    public static Integer getStatPayment() {
        return statPayment;
    }

    public static void setStatPayment(Integer statPayment) {
        MockUpData.statPayment = statPayment;
    }

    public static boolean isUserAsGuest(UserSessionManagement sessionManagement) {
        return new Gson().fromJson(sessionManagement.getUserDetail(), User.class).isGuestUser();
    }

    public static User getUserItem(UserSessionManagement sessionManagement) {
        return new Gson().fromJson(sessionManagement.getUserDetail(), User.class);
    }

    public static String userBusinessKey(UserSessionManagement sessionManagement) {
        return Utils.validateNullValue(new Gson().fromJson(sessionManagement.getUserDetail(), User.class).getUserBusinessKey());
    }

    public static String activeAccountBusinessKey(UserSessionManagement sessionManagement){
        return Utils.validateNullValue(new Gson().fromJson(sessionManagement.getUserDetail(), User.class).getActiveAccountBusinessKey());
    }

    public static String getEazyHotelUserId(UserSessionManagement sessionManagement){
        return Utils.validateNullValue(new Gson().fromJson(sessionManagement.getUserDetail(), User.class).getId());
    }

    public static String getAccountUserId(UserSessionManagement sessionManagement){
        return Utils.validateNullValue(new Gson().fromJson(sessionManagement.getUserDetail(), User.class).getAccountId());
    }

    public static void setListSubInspection(HashMap<String, List<ItemTemplateModel.SubItemTemplateModel>> listHashMap) {
        MockUpData.listHashMap = listHashMap;
    }

    public static HashMap<String, List<ItemTemplateModel.SubItemTemplateModel>> getListSubInspection(){
        return MockUpData.listHashMap;
    }

    public static ArrayList<String> listTypeParking(){
        ArrayList<String> list = new ArrayList<>();
        list.add("Hourly");
        list.add("Daily");
        return list;
    }
    public static ArrayList<String> listServiceTerm(){
        ArrayList<String> list = new ArrayList<>();
        list.add("Time");
        list.add("Weekly");
        return list;
    }


    public static ArrayList<ColorCar> listColor(){
        ArrayList<ColorCar> list = new ArrayList<>();
        list.add(new ColorCar(R.color.light_grey,"White"));
        list.add(new ColorCar(R.color.blue,"Sky"));
        list.add(new ColorCar(R.color.colorPrimary,"Blue"));
        list.add(new ColorCar(R.color.red,"Red"));
        list.add(new ColorCar(R.color.black,"Black"));
        list.add(new ColorCar(R.color.gray,"Gray"));
        list.add(new ColorCar(R.color.green,"Green"));
        list.add(new ColorCar(R.color.calendar_pink,"Pink"));
        list.add(new ColorCar(R.color.brown,"Brown"));
        list.add(new ColorCar(R.color.gold,"Gold"));
        list.add(new ColorCar(R.color.purple,"Purple"));
        return list;
    }

    public static ArrayList<Payment> listPaymentMethod(){
        ArrayList<Payment> list = new ArrayList<>();
        list.add(new Payment(R.drawable.kess_logo_new,"PAY BY KESS"));
        list.add(new Payment(R.drawable.more_cash,"PAY BY CASH"));
        return list;
    }

    @SuppressLint("StringFormatInvalid")
    public static ArrayList<ItemHR> listItem(Context context){
        ArrayList<ItemHR> list = new ArrayList<>();
        list.add(new ItemHR(1,R.drawable.ic_home_attendance_v2,context.getString(R.string.report_attendance), context.getString(R.string.employee_attendance_report)));
        list.add(new ItemHR(2,R.drawable.ic_home_leave_v2,context.getString(R.string.leave_magement),context.getString(R.string.annual_leave_sick_special)));
        list.add(new ItemHR(3,R.drawable.ic_home_payroll_v2,context.getString(R.string.payroll_management), context.getString(R.string.you_can_manage_your_payroll_with_our_wallet)));
        list.add(new ItemHR(5,R.drawable.ic_home_overtime_v2,context.getString(R.string.overtime_management), context.getString(R.string.you_can_apply_for_overtime_approve_by_here)));
        list.add(new ItemHR(6,R.drawable.ic_home_resignation_v2,context.getString(R.string.resignation), context.getString(R.string.you_can_ask_for_resignation_with_this_function)));
        list.add(new ItemHR(7,R.drawable.profile_assign_work_order,context.getString(R.string.my_rool), context.getString(R.string.employee_permission_responsibility)));
        list.add(new ItemHR(4,R.drawable.ic_home_profile_v2,context.getString(R.string.my_profile), context.getString(R.string.employee_data_record)));
        return  list;
    }
}
