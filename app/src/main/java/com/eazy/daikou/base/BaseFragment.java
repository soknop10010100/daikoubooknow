package com.eazy.daikou.base;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;

import androidx.activity.result.ActivityResult;
import androidx.fragment.app.Fragment;

import com.eazy.daikou.helper.CheckIsAppActive;
import com.eazy.daikou.helper.UserSessionManagement;
import com.eazy.daikou.helper.Utils;

import org.jetbrains.annotations.NotNull;

public class BaseFragment extends Fragment {

    public Activity mActivity;

    public BaseFragment() {}

    protected final BetterActivityResult<Intent, ActivityResult> activityLauncher = BetterActivityResult.registerActivityForResult(this);

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        if (mActivity == null && context instanceof BaseActivity){
            mActivity = (BaseActivity) context;
        }

        assert mActivity != null;
        String lang = Utils.getString("language", mActivity);
        if (lang.equals("")){
            lang = "en";
        }
        Utils.changeLanguage(mActivity, lang);

        // change status bar text color book now
        if (!CheckIsAppActive.Companion.is_daikou_active()) {
            mActivity.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
    }
}
