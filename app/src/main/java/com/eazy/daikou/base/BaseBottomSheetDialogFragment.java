package com.eazy.daikou.base;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;

import com.eazy.daikou.R;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import org.jetbrains.annotations.NotNull;

public class BaseBottomSheetDialogFragment extends BottomSheetDialogFragment {

    public Activity mActivity;

    public BaseBottomSheetDialogFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_FRAME, R.style.CustomBottomSheetDialogFragment);
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        if (mActivity == null && context instanceof BaseActivity){
            mActivity = (BaseActivity) context;
        }
    }
}
