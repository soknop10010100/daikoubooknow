package com.eazy.daikou.base;

import static com.eazy.daikou.helper.Utils.logDebug;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.eazy.daikou.R;
import com.eazy.daikou.helper.Utils;
import com.eazy.daikou.helper.adapter.AlertDialogAdapter;
import com.eazy.daikou.helper.image.AlbumStorageDirFactory;
import com.eazy.daikou.helper.image.FroyoAlbumDirFactory;
import com.eazy.daikou.model.home.LanguageModel;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class BaseCameraActivity extends BaseActivity {

    private String mCameraFileName;
    private AlbumStorageDirFactory mAlbumStorageDirFactory = null;
    private static final int REQUEST_CODE_GALLERY = 220;
    private static final int REQUEST_CODE_CAMERA = 120;
    private static int REQUEST_CODE_ACTION = 1;
    private String goneSelectImage = "";
    private String action = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_custom_dialog);

        findViewById(R.id.txtCancel).setOnClickListener(view -> finish());
        RecyclerView recyclerViewLang = findViewById(R.id.recyclerViewPic);

        mAlbumStorageDirFactory = new FroyoAlbumDirFactory();

        // Init action
        if (getIntent().hasExtra("no_select")) {
            goneSelectImage = getIntent().getStringExtra("no_select");
        }

        if (getIntent() != null && getIntent().hasExtra("action")){
            action = getIntent().getStringExtra("action");
        }

        if (goneSelectImage.equals("yes")) {    //when want to open only camera to take photo
          openCamera();
        } else {
            recyclerViewLang.setLayoutManager(new LinearLayoutManager(this));
            List<LanguageModel> changeImageList = LanguageModel.getListChangeImage(this, action.equalsIgnoreCase("video") ? 2 : 1, goneSelectImage);

            AlertDialogAdapter alertDialogAdapter = new AlertDialogAdapter(this, changeImageList, clickCallBackListener);
            recyclerViewLang.setAdapter(alertDialogAdapter);
        }

        LinearLayout mainLayout = findViewById(R.id.mainScroll);
        mainLayout.setPadding(5,5,10,10);

    }
    private void openCamera(){
        if (ContextCompat.checkSelfPermission(getBaseContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(getBaseContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CODE_CAMERA);
        } else {
            takePhotoFromCamera(REQUEST_CODE_CAMERA);
        }
    }

    private final AlertDialogAdapter.ClickCallBackListener clickCallBackListener = position -> {
        switch (position) {
            case 0:
                if (ContextCompat.checkSelfPermission(getBaseContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                        || ContextCompat.checkSelfPermission(getBaseContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CODE_CAMERA);
                } else {
                    takePhotoFromCamera(REQUEST_CODE_CAMERA);
                }
                break;
            case 1:
                if (ContextCompat.checkSelfPermission(getBaseContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_CODE_GALLERY);
                } else {
                    choosePhotoFromGallery(REQUEST_CODE_GALLERY);
                }
                break;
        }
    };

    @SuppressLint("IntentReset")
    private void choosePhotoFromGallery(int requestCode) {
        REQUEST_CODE_ACTION = requestCode;
        if (action.equalsIgnoreCase("video")){
            Intent intent = new Intent(Intent.ACTION_PICK,MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
            intent.setType("video/*");
            resultLauncher.launch(intent);
        } else {
            Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            resultLauncher.launch(galleryIntent);
        }
    }

    private void takePhotoFromCamera(int request) {
        REQUEST_CODE_ACTION = request;
        if (action.equalsIgnoreCase("video")){
            Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
            File outFile = setUpPhotoFile();
            if (outFile == null) return;
            mCameraFileName = outFile.getAbsolutePath();
            Uri ouTuri = FileProvider.getUriForFile(getApplicationContext(), getApplicationContext().getPackageName() + ".provider", outFile);

            takeVideoIntent.putExtra(MediaStore.EXTRA_OUTPUT, ouTuri);
            if (takeVideoIntent.resolveActivity(getPackageManager()) != null) {
                resultLauncher.launch(takeVideoIntent);
            }
        } else {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            // Ensure that there's a camera activity to handle the intent
            if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                // Create the File where the photo should go
                File photoFile = null;
                try {
                    photoFile = createImageFile();
                } catch (IOException ex) {
                    // Error occurred while creating the File
                }
                // Continue only if the File was successfully created
                if (photoFile != null) {
                    Uri photoURI = FileProvider.getUriForFile(this, this.getPackageName()+".fileprovider", photoFile);
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                    resultLauncher.launch(takePictureIntent);
                }
            }
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCameraFileName = image.getAbsolutePath();
        return image;
    }
    private File setUpPhotoFile() {
        String fileName = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date());
        return createImageFile(fileName);
    }

    private File createImageFile(String fileName) {
        File albumF = getAlbumDir();
        return new File(albumF, fileName + ".jpg");
    }

    private File getAlbumDir() {
        File storageDir = null;
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            storageDir = mAlbumStorageDirFactory.getAlbumStorageDir(getString(R.string.app_name));
            if (storageDir != null) {
                if (!storageDir.mkdirs()) {
                    if (!storageDir.exists()) {
                        logDebug("CameraSample", "failed to create directory");
                        return null;
                    }
                }
            }
        } else {
            logDebug(getString(R.string.app_name), getString(R.string.external_storage_is_not_mount));
        }
        return storageDir;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE_CAMERA) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(BaseCameraActivity.this, R.string.camera_was_granteds, Toast.LENGTH_LONG).show();
                takePhotoFromCamera(requestCode);
            } else {
                Toast.makeText(BaseCameraActivity.this, R.string.camera_was_not_granted, Toast.LENGTH_LONG).show();
            }
        }
        if (requestCode == REQUEST_CODE_GALLERY) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(BaseCameraActivity.this, getString(R.string.gallery_was_granted), Toast.LENGTH_LONG).show();
                choosePhotoFromGallery(requestCode);
            } else {
                Toast.makeText(BaseCameraActivity.this, getString(R.string.gallery_was_not_granted), Toast.LENGTH_LONG).show();
            }
        }
    }

    private final ActivityResultLauncher<Intent> resultLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            result -> {
                if (result.getResultCode() == Activity.RESULT_OK) {
                    Intent data = result.getData();
                    if (action.equalsIgnoreCase("video")){
                        if (REQUEST_CODE_ACTION == REQUEST_CODE_GALLERY) {
                            if (data != null) {
                                Uri contentURI = data.getData();    // When I convert here it not finish class, so I give only Uri
                                // File actualImage = Utils.from(this, contentURI);
                                // String base = videoToBase64(actualImage);

                                Intent intent = getIntent();
                                intent.putExtra("select_video", contentURI.toString()); // pathVideo is file uri
                                setResult(RESULT_OK, intent);
                                BaseCameraActivity.this.finish();
                            }
                        } else if (REQUEST_CODE_ACTION == REQUEST_CODE_CAMERA) {
                            if (mCameraFileName == null) return;    // When I convert here it not finish class, so I give only mCameraFileName
                            // File actualFile = new File(mCameraFileName);
                            // String base = videoToBase64(actualFile);

                            Intent intent = getIntent();
                            intent.putExtra("video_path", mCameraFileName);   // pathVideo is file url
                            setResult(RESULT_OK, intent);
                            BaseCameraActivity.this.finish();
                        }
                    } else {
                        if (REQUEST_CODE_ACTION == REQUEST_CODE_GALLERY) {
                            if (data != null) {
                                Uri contentURI = data.getData();
                                Bitmap loadedBitmap = Utils.getImageGallery(data.getData(), this);
                                loadedBitmap = getResizedBitmap(loadedBitmap, 700);
                                String pathImage = Utils.convert(loadedBitmap);
                                Intent intent = new Intent();
                                intent.putExtra("select_image", pathImage); // pathImage is base64 url
                                intent.putExtra("select_image_uri", contentURI.toString());
                                setResult(RESULT_OK, intent);
                                BaseCameraActivity.this.finish();
                            }
                        } else if (REQUEST_CODE_ACTION == REQUEST_CODE_CAMERA) {
                            if (mCameraFileName == null) return;
                            File actualFile = new File(mCameraFileName);
                            Bitmap bitmap = BitmapFactory.decodeFile(mCameraFileName);
                            try {
                                Bitmap bitmaps = Utils.getImageCamera(actualFile, this);
                                String pathImage = Utils.convert(bitmaps);
                                Intent intent = new Intent();
                                intent.putExtra("image_path", pathImage);   // pathImage is base64 url
                                intent.putExtra("image_path_uri", mCameraFileName);
                                setResult(RESULT_OK, intent);
                                finish();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            });

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 0) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    public static Bitmap convertByteToBitmap(String fileString) {
        if (fileString != null) {
            byte[] imageAsBytes = Base64.decode(fileString, Base64.DEFAULT);
            return BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length);
        }
        return null;
    }

    private String getPath(Uri uri, String str) {
        String[] projection = { MediaStore.Video.Media.DATA, MediaStore.Video.Media.SIZE, MediaStore.Video.Media.DURATION};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        cursor.moveToFirst();
        String filePath = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA));
        //      int fileSize = cursor.getInt(cursor.getColumnIndexOrThrow(MediaStore.Video.Media.SIZE));
        //      long duration = Time.MILLISECONDS.toSeconds(cursor.getInt(cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DURATION)));


        //some extra potentially useful data to help with filtering if necessary
        //System.out.println("size: " + fileSize);
        System.out.println("path: " + filePath);
        // System.out.println("duration: " + duration);

        return filePath;
    }

    public String getPath(Uri uri) {
        String[] projection = { MediaStore.Video.Media.DATA };
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            // HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
            // THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } else
            return null;
    }

    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = { MediaStore.Images.Media.DATA };
            cursor = context.getContentResolver().query(contentUri,  proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "filename1", null);
        return Uri.parse(path);
    }

    public static String convertUriToVideoBase64(Context context, String fileString) {
        if (fileString == null) return null;
         File actualImage = Utils.from(context, Uri.parse(fileString));
        return Utils.videoToBase64(actualImage);
    }

    public static String convertFileNameToVideoBase64(String mCameraFileName) {
        if (mCameraFileName == null) return null;
        File actualFile = new File(mCameraFileName);
        return Utils.videoToBase64(actualFile);
    }

    private void deleteFileRecord(String mFileName) {
        File videoFiles = new File(mFileName);
        if (videoFiles.exists()) {
            videoFiles.delete();
        }
    }

}
