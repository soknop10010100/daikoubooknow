package com.eazy.daikou.repository_ws;

import com.eazy.daikou.ui.home.calendar.calendar_custom.CalendarCellView;

import java.util.Date;

public interface CalendarCellDecorator {
  void decorate(CalendarCellView cellView, Date date);
}
