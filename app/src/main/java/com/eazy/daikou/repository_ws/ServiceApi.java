package com.eazy.daikou.repository_ws;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.HashMap;
import java.util.Map;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface ServiceApi {

    // Main home page
    @GET("api/homepage/get_homepage_data")
    Call<JsonElement> getMainHomePage(@Query("page") String page, @Query("limit") String limit);

    // New sign up
    @POST("api/user/create_guest_user")
    Call<JsonElement> newSignUp(@Body HashMap body);

    // Complaint Solutions
    @GET("api/posts/fetch_posts")
    Call<JsonElement> getResComplaintSolution(
            @Query("page") int page,
            @Query("limit") int limit,
            @Query("list_type") String list_type,
            @Query("post_type") String unitId,
            @Query("account_id") String account_id
    );

    @POST("api/posts/post")
    Call<JsonElement> postComplaint(@Body HashMap hashMap);

    @GET("api/posts/get_post")
    Call<JsonElement> getDetailCompliant(@Query("id") String id);

    @POST("api/posts/comment")
    Call<JsonElement> replyCommentComplaint(@Body HashMap hashMap);

    @POST("api/notification/register_notification")
    Call<JsonElement> registerNotification(@Body HashMap<String, String> hashMap);

    @POST("api/notification/register_notification")
    Call<JsonElement> registerNotificationBookNow(@Body HashMap<String, String> hashMap);

    @GET("api/fetch_department_contact.php?")
    Call<JsonElement> getPropertyContact(@Query("property_id") String propertyId, @Query("page") int page, @Query("limit") int limit);

    @POST("api/friend_list/add_friend.php")
    Call<JsonElement> addFriendContact(@Body HashMap hashMap);

    @GET("api/friend_list/fetch_friend_by_user.php")
    Call<JsonElement> getListAddFriendContact(@Query("user_id") String user_id, @Query("page") int page, @Query("limit") int limit);

    // =============== Project Development ========
    @GET("api/branch/get_homepage")
    Call<JsonElement> getHomePageItem(
            @Query("page") String page,
            @Query("limit") String limit,
            @Query("coord_lat") String X,
            @Query("coord_long") String Y
    );

    //API for project detail
    @GET("api/branch/get_branch_detail")
    Call<JsonElement> getPropertyDetail(@Query("id") int id);

    //API for category list
    @GET("api/branch/fetch_branches_by_type")
    Call<JsonElement> getListByCategory(
            @Query("type_key") String key,
            @Query("page") String page,
            @Query("limit") String limit
    );

    // api get list featured
    @GET("api/branch/fetch_featured_branches")
    Call<JsonElement> getListFeatured(
            @Query("page") String page,
            @Query("limit") String limit
    );

    // Api get list NearBy
    @GET("api/branch/fetch_nearby_branches")
    Call<JsonElement> getListNearBy(
            @Query("page") String page,
            @Query("limit") String limit,
            @Query("coord_lat") String x,
            @Query("coord_long") String y
    );

    // Api for update location Property of Project
    @PUT("api/update_property_coord.php")
    Call<JsonObject> updateLocationProject(@Body HashMap hashMap);

    // Dis / Active Acc User
    @PUT("api/user_api/activate_and_deactivate_user.php")
    Call<JsonElement> deleteUserAccount(@Body HashMap hashMap);

    @POST("api/user_api/send_and_verify_code.php")
    Call<JsonElement> confirmSMSCodeDeleteAcc(@Body HashMap hashMap);

    // API for Rule and News
    @GET("api/rules_and_news.php")
    Call<JsonElement> rule_and_news(@Query("page") int page,
                                    @Query("limit") int limit,
                                    @Query("type") String type,
                                    @Query("property_id") String property_id);

    // API for Laws Detail
    @GET("api/law_list_by_category.php")
    Call<JsonElement> lawsDetail(@Query("page") int page,
                                 @Query("limit") int limit,
                                 @Query("category_id") String category_id);

    // API for Laws Detail
    @GET("api/law_category.php")
    Call<JsonElement> laws(@Query("page") int page,
                           @Query("limit") int limit);

    //@GET("api/books_and_quotes.php")
    @GET("api/fetch_books_and_quotes_list.php")
    Call<JsonElement> quote_book(@Query("limit") int limit,
                                 @Query("page") int page);

    // Api for Quote Book
    @GET("api/fetch_books_and_quotes_all.php")
    Call<JsonElement> quote_book_all(@Query("page") int page,
                                     @Query("limit") int limit,
                                     @Query("item_type") String item_type,
                                     @Query("category_id") String category_id);

    @POST("api/parking_register_user_card.php")
    Call<JsonElement> setQrCodeProfileParking(@Body HashMap hashMap);


    // Api for my Unit list
    @GET("api/my_unit.php")
    Call<JsonElement> getMyUnitList(@Query("account_id") String account, @Query("property_id") String property_id, @Query("page") int page, @Query("limit") int limit, @Query("user_login_id") String user_login_id);

    @GET("api/my_unit/fetch_all_unit_user.php")
    Call<JsonElement> getOperationUnitNo(@Query("user_id") String userId);

    //Sale operation payment
    @GET("api/my_unit/fetch_unit_operation_payment.php")
    Call<JsonElement> getOperationPaymentSaleList(@Query("user_id") String userId, @Query("unit_id") String unit_id);

    //Lease operation payment
    @GET("api/my_unit/fetch_unit_lease_operation_payment.php")
    Call<JsonElement> getOperationPaymentLeaseList(@Query("user_id") String userId, @Query("unit_id") String unit_id);

    // ================ API for Floor Plan List =====================
    @GET("api/floor_plan_floor_list_v1.php")
    Call<JsonElement> getFloorList(@Query("page") int page, @Query("limit") int limit, @Query("property_id") String property_id);

    @GET("api/floor_plan_space_category.php")
    Call<JsonElement> getSpaceCategory(@Query("storey_id") String storey_id, @Query("floor_no") String floor_no);

    @GET("/api/floor_plan_space_list_v1.php")
    Call<JsonElement> getSpaceItemSpace(@Query("category_type") String category_type,
                                        @Query("space_category_id") String space_category_id,
                                        @Query("storey_id") String storey_id,
                                        @Query("page") int page,
                                        @Query("limit") int limit);

    @GET("/api/floor_plan_space_list_v1.php")
    Call<JsonElement> getSpaceItemUnit(@Query("category_type") String category_type,
                                       @Query("property_id") String property_id,
                                       @Query("floor_no") String floor_no,
                                       @Query("page") int page,
                                       @Query("limit") int limit);

    @GET("api/floor_plan_polyline_v2.php")
    Call<JsonElement> getSpacePolyline(@Query("category_type") String category_type,
                                       @Query("property_id") String property_id,
                                       @Query("space_id") String space_id,
                                       @Query("storey_id") String storey_id);

    @GET("api/floor_plan_polyline_v2.php")
    Call<JsonElement> getUnitPolyline(@Query("category_type") String category_type,
                                      @Query("property_id") String property_id,
                                      @Query("unit_id") String unit_id);

    @GET("api/floor_plan_polyline_v2.php")
    Call<JsonElement> getUnitTypePolyline(@Query("category_type") String category_type,
                                          @Query("property_id") String property_id,
                                          @Query("unit_type_id") String unit_type_id,
                                          @Query("floor_no") String floor_no);

    @GET("api/property_security_option.php")
    Call<JsonElement> getFloorSecurity(@Query("property_id") String property_id,
                                       @Query("page") int page,
                                       @Query("limit") int limit);

    @GET("api/space_by_id.php")
    Call<JsonElement> getSpaceByIdPolylineDetail(@Query("space_id") String space_id);

    @GET("api/unit_by_id.php")
    Call<JsonElement> getUnitByIdPolylineDetail(@Query("unit_id") String unit_id);

    // Api for Reset Password
    @GET("api/reset_new_password.php")
    Call<JsonElement> verifyCode(@QueryMap Map<String, Object> filter);

    // *** Track Water Electricity ***

    @GET("api/electric_dashboard.php?")
    Call<JsonElement> getElectricOwnerUsage(@Query("property_id") String property_id, @Query("unit_id") String unit_id, @Query("month") String month, @Query("year") String year);

    @GET("api/water_dashboard.php")
    Call<JsonElement> getWaterOwnerUsage(@Query("property_id") String property_id, @Query("unit_id") String unit_id, @Query("month") String month, @Query("year") String year);

    @GET("api/fetch_all_utility_floor.php")
    Call<JsonElement> getWaterElectricAllFloor(@Query("property_id") String property_id, @Query("tracking_date") String tracking_date);

    @GET("api/fetch_utility_record_list.php")
    Call<JsonElement> getWaterElectricAllRecords(@Query("page") int page, @Query("limit") int limit, @Query("property_id") String property_id, @Query("utility_type") String utility_type, @Query("utility_area") String utility_area);

    @GET("api/fetch_all_utility_floor_unit.php")
    Call<JsonElement> getAllFloorAndUnit(@Query("property_id") String property_id, @Query("tracking_date") String tracking_date, @Query("utility_type") String utility_type, @Query("utility_area") String utility_area);

    @GET("api/fetch_utility_info_by_unit.php")
    Call<JsonElement> getInfoByUnit(@Query("property_id") String property_id, @Query("tracking_date") String tracking_date, @Query("utility_type") String utility_type, @Query("utility_area") String utility_area, @Query("unit_id") String unit_id);

    @POST("api/save_utility_record.php")
    Call<JsonElement> saveUnitWaterElectric(@Body HashMap hashMap);

    // ServiceProvider
    @GET("api/service_provider_list_city.php")
    Call<JsonElement> getSeeMoreServiceProviders(@Query("page") int page, @Query("limit") int limit, @Query("sub_category_id") String sub_category_id, @Query("city") String city);

    // SearchServiceProvider
    @GET("api/service_provider_search.php")
    Call<JsonElement> searchServiceProvider(@Query("page") int page, @Query("limit") int limit, @Query("keysearch") String key);

    @POST("api/logout")
    Call<JsonElement> logout();

    //API for Send Emergency
    @POST("api/emergency/create_emergency.php")
    Call<JsonElement> getSendEmergency(@Body HashMap hashMap);

    //API for History Emergency
    @GET("api/emergency/fetch_emergency_history_list.php")
    Call<JsonElement> getHistoryEmergency(@QueryMap Map<String, Object> filter);

    //API for History Emergency Detail
    @GET("api/emergency/get_emergency_history_detail.php")
    Call<JsonElement> getHistoryEmergencyDetail(@Query("emergency_history_id") String emergency_list_id);


    //API for Show Status Emergency
    @PUT("api/emergency/update_emergency.php")
    Call<JsonElement> statusEmergency(@Body HashMap hashMap);

    //API for Permission
    @GET("api/permission_menu_v2.php")
    Call<JsonElement> getUserPermission(@QueryMap Map<String, Object> maps);

    //============= Api get list notification ===================
    @GET("api/notification/fetch_user_notification_list.php")
    Call<JsonElement> getListAllNotification(@Query("page") int page, @Query("limit") int limit, @Query("user_id") String userId);

    @PUT("api/notification/read_user_notification.php")
    Call<JsonElement> readNotification(@Body HashMap hashMap);

    @PUT("api/notification/read_user_all_notification.php")
    Call<JsonElement> readAllNotification(@Body HashMap hashMap);

    @GET("api/notification/fetch_badge_number.php")
    Call<JsonElement> readNumberNotification(@Query("user_id") String userId);

    @PUT("api/notification/delete_user_notification.php")
    Call<JsonElement> deleteNotification(@Body HashMap hashMap);

    //================= Complaint_Solution ================
    //GetListServiceProvider
    @GET("api/service_provider_menu.php")
    Call<JsonElement> getListServiceProvider();

    //Get List service provider Category V2
    @GET("api/service_provider/fetch_service_provider_categories.php")
    Call<JsonElement> ListServiceProviderCategoryItem();

    //Get Sub List category service provider V2
    @GET("api/service_provider/fetch_service_provider_by_category.php")
    Call<JsonElement> getSubListCategoryServiceProvider(@Query("page") int page, @Query("limit") int limit, @Query("category_keyword") String categoryKeyword);

    //Get Detail List Service Provider V2
    @GET("api/service_provider/get_service_provider_detail.php")
    Call<JsonElement> getListDetailServiceProvider(@Query("id") String id);

    @GET("api/service_provider_list_test.php")
    Call<JsonElement> getLatLongListServiceProvider(@Query("page") int page, @Query("limit") int limit, @Query("sub_category_id") String subCategoryId);

    @GET("api/service_provider_all_location.php")
    Call<JsonElement> getLatLongListServiceProviderV2(@Query("page") int page, @Query("limit") int limit, @Query("sub_category_id") String subCategoryId);

    @GET("api/service_provider_detail_by_id.php")
    Call<JsonElement> getDetailLatLongById(@Query("service_provider_id") String service_provider_id);

    //******************* Service Provider New View V3 -*********************
    @GET("api/service_provider/get_service_provider_homepage.php")
    Call<JsonElement> getHomePageListProvider(@Query("page") String page, @Query("limit") String limit, @Query("user_id") String userId);

    @GET("api/service_provider/fetch_service_provider_list_all.php")
    Call<JsonElement> getListAllServiceProvider(@Query("page") int page, @Query("limit") int limit, @Query("user_id") String userId, @Query("list_type") String listType);

    ///Signup_guest
    @GET("api/check_email_or_phone_if_existed.php")
    Call<JsonObject> validateEmail(@QueryMap Map<String, Object> hashMap);

    @POST("api/send_verify_phone_number.php")
    Call<JsonObject> sendPin(@Body HashMap user);

    @POST("api/create_guest_user.php")
    Call<JsonObject> signUp(@Body HashMap user);

    //Parking V1
    @GET("api/fetch_parking_history_list.php")
    Call<JsonElement> getListParking(@Query("page") int page, @Query("limit") int limit, @Query("property_id") String propertyId, @Query("user_type") String userType, @Query("user_id") String userId);

    //parking_detail
    @GET("api/parking_detail_by_id.php")
    Call<JsonElement> getParkingDetail(@Query("parking_list_id") String parkingId);

    //create parking
    @POST("api/parking_scan_check_in.php")
    Call<JsonElement> createParking(@Body HashMap<String, Object> user);

    @POST("api/parking_smart_scan.php")
    Call<JsonElement> createParkingV2(@Body HashMap<String, Object> user);

    ///employee Scan
    @PUT("api/parking_scan_add_info.php")
    Call<JsonElement> staffUpdate(@Body HashMap<String, Object> user);

    //pay by kess
    @GET("api/parking_payment_kess_url.php")
    Call<JsonElement> getPaymentParking(@Query("parking_list_id") String parkingListId, @Query("scanqr_code_info") String scanqr_info);

    //Kess Pay Printer
    @GET("api/parking_payment_kess_url_with_machine.php")
    Call<JsonElement> paymentParkingKESS(@Query("parking_list_id") String parkingListId);

    //pay by cash
    @POST("api/parking_payment_by_cash.php")
    Call<JsonElement> paymentParkingByCash(@Body HashMap<String, Object> hashMap);

    //Accept
    @PUT("api/employee_accept_payment_cash_v1.php")
    Call<JsonElement> acceptPaymentParking(@Body HashMap<String, Object> hashMap);

    //Accept
    @PUT("api/parking_alert_update_status_v1.php")
    Call<JsonElement> acceptNotificationParking(@Body HashMap<String, Object> hashMap);

    @PUT("api/parking_car_owner_cancel_check_out.php")
    Call<JsonElement> canCelDetailParking(@Body HashMap<String, Object> hashMap);

    // Print Check In parking
    @POST("api/parking_with_machine_smart_scan.php")
    Call<JsonElement> pintCheckInt(@Body HashMap<String, Object> hashMap);

    //Print Check Out Parking
    @PUT("api/parking_scan_check_out.php")
    Call<JsonElement> checkOut(@Body HashMap<String, Object> hashMap);

    // Detail On Lat Long
    @GET("api/branch_by_id_small_detail.php?branch_id=29")
    Call<JsonElement> detailOnLatLong(@Query("branch_id") String branch_id);

    ///GetServiceType
    @GET("api/cleaning_service_type_info_v1.php")
    Call<JsonElement> getServiceType(@Query("page") int page, @Query("limit") int limit, @Query("property_id") String propertyId, @Query("unit_type_id") String unitTypeId, @Query("service_term") String serviceTerm);

    ///GetServiceType
    @GET("api/cleaning_service_type_info_more.php")
    Call<JsonElement> getServiceNameType(@Query("page") int page, @Query("limit") int limit, @Query("property_id") String propertyId, @Query("unit_type_id") String unitTypeId, @Query("service_type") String service_type, @Query("service_term") String serviceTerm);

    //create cleaning
    @POST("api/cleaning_create_service.php")
    Call<JsonElement> createCleaningService(@Body HashMap<String, Object> hashMap);

    ///Accept and Reschedule
    @PUT("api/cleaning_reschedule_service_v1.php")
    Call<JsonElement> acceptAndReschedule(@Body HashMap<String, Object> hashMap);

    //AcceptPayment
    @PUT("api/cleaning_service_payment_v1.php")
    Call<JsonElement> acceptPaymentCleaning(@Body HashMap<String, Object> hashMap);

    // ====================== Work Schedule Employee Service Property ==============================
    // Service Property Status Schedule Cleaner
    @GET("api/property_service_provider_employee/fetch_property_service_schedule.php")
    Call<JsonElement> getListStatusWorkSchedule(@QueryMap Map<String, Object> mHashMap);

    // Service Property History Work Scheduled
    @GET("api/property_service_provider_employee/fetch_property_service_schedule_list_by_date.php")
    Call<JsonElement> getListHistoryWorkSchedule(@QueryMap Map<String, Object> mHashMap);

    // List History Schedule Cleaner
    @GET("api/cleaner/fetch_cleaning_service_history_of_cleaner_by_user.php")
    Call<JsonElement> getListHistoryCleaner(@Query("property_id") String propertyId, @Query("user_id") String userId, @Query("page") int page, @Query("limit") int limit);

    @POST("api/cleaner/set_cleaner_rating_by_owner.php")
    Call<JsonElement> writeAndReview(@Body HashMap<String, Object> hashMap);

    // List Detail Employee Service Provider
    @GET("api/property_service_provider_employee/get_property_service_schedule_detail.php")
    Call<JsonElement> getDetailEmployeeServiceProvider(@Query("user_id") String user_id, @Query("active_user_type") String activeUserType, @Query("work_schedule_id") String workScheduleId);

    //List History Schedule Cleaner
    @GET("api/cleaner/fetch_cleaning_service_history_detail_by_id.php")
    Call<JsonElement> getDetailCleaner(@Query("cleaning_schedule_id") String historyId, @Query("user_id") String userId);

    //Scan Unit Service Provider
    @POST("api/property_service_provider_employee/property_service_employee_activity.php")
    Call<JsonElement> getListServiceScanUnit(@Body HashMap<String, Object> hashMap);

    //CheckIn Cleaning Room
    @PUT("api/cleaner/cleaner_scan_check_in_out_v1.php")
    Call<JsonElement> scanCheckInClean(@Body HashMap<String, Object> hashMap);

    @PUT("api/cleaner/cleaner_check_in_out_reason.php")
    Call<JsonElement> writeReasonWhenCheckInOutLate(@Body HashMap<String, Object> hashMap);

    //Take Photo Activity V2
    @POST("api/property_service_provider_employee/upload_schedule_employee_activity_image.php")
    Call<JsonElement> uploadActivityPhoto(@Body HashMap<String, Object> hashMap);

    //call people help cleaning
    @POST("api/cleaner/cleaner_alert_for_helping.php")
    Call<JsonElement> callHelp(@Body HashMap<String, Object> hashMap);

    // ***** Property Service Provider ***** //

    @GET("api/property_service_provider/fetch_property_service_registration_list.php")
    Call<JsonElement> getListPropertyServiceProvider(@Query("page") int page, @Query("limit") int limit, @Query("operation_part") String operationPart, @Query("user_id") String userid, @Query("account_id") String accountID);

    @GET("api/property_service_provider/get_property_service_registration_detail.php")
    Call<JsonElement> getListDetailPropertyServiceProvider(@Query("id") String id);

    @PUT("api/property_service_provider/reschedule_and_accept_property_service_registration.php")
    Call<JsonElement> getRescheduleAndAcceptPropertyProvider(@Body HashMap hashMap);

    @GET("api/property_service/fetch_property_service_list.php")
    Call<JsonElement> getListServiceIn(@Query("page") int page, @Query("limit") int limit, @Query("property_id") String propertyId, @Query("service_type") String serviceType, @Query("service_category_key") String service_category_key);

    @GET("api/property_service/fetch_property_service_category.php")
    Call<JsonElement> getListCategoryServiceIn(@Query("property_id") String propertyId, @Query("service_type") String service_type);

    // category Type Service provider Property
    @GET("api/property_service_provider/fetch_property_service_provider_category.php")
    Call<JsonElement> listTypeServiceProviderProperty();

    // company Service Provider company
    @GET("api/property_service_provider/fetch_property_service_provider_company.php")
    Call<JsonElement> listServiceProviderCompany(@Query("page") int page, @Query("limit") int limit, @Query("active_account_id") String activeAccountId, @Query("service_key") String serviceKey, @Query("service_name") String serviceName, @Query("operation_part") String operationPart);

    // # Get Property Service Category and Term
    @GET("api/property_service_provider/get_property_service_category_and_term.php")
    Call<JsonElement> listServiceCategoryAndTerm(@Query("company_account_id") String companyAccountId, @Query("active_account_id") String activeAccountId, @Query("operation_part") String operationPart);

    // Get List Service Type
    @GET("api/property_service_provider/fetch_property_service_by_category_and_term.php")
    Call<JsonElement> listPropertyServiceType(@Query("page") int page, @Query("limit") int limit, @Query("active_account_id") String activeAccountId, @Query("unit_id") String unitId, @Query("term_key") String termKey, @Query("category_id") String categoryId, @Query("company_account_id") String companyAccountId);

    // Create List Service Provider
    @POST("api/property_service_provider/confirm_and_submit_property_service.php")
    Call<JsonElement> listCreteServiceProvider(@Body HashMap hashMap);

    @GET("api/fetch_property_indoor_service_detail_by_id.php")
    Call<JsonElement> getListServiceInDetail(@Query("service_id") String serviceId, @Query("service_type") String serviceType);

    //Search Service Cleaning
    @GET("api/cleaner/search_cleaning_service_history.php")
    Call<JsonElement> searchServiceCleaning(@Query("page") int page, @Query("limit") int limit, @Query("property_id") String propertyId,
                                            @Query("user_id") String userId, @Query("no_order") String no_order, @Query("unit_no") String unit_no,
                                            @Query("start_time") String start_time, @Query("end_time") String end_time, @Query("status") String status, @Query("service_term") String service_term);

    @PUT("api/switch_language.php")
    Call<JsonElement> changLanguage(@Body HashMap<String, Object> hashMap);

    // =========== Inspection Api ========================

    @GET("api/inspection_api/fetch_inspection_type.php")
    Call<JsonElement> getListInspectionType();

    @GET("api/inspection_api/fetch_inspection_list.php")
    Call<JsonElement> getListAllInspection(@Query("page") int page, @Query("limit") int limit, @Query("property_id") String property_id, @Query("inspection_type_id") String inspection_type_id);

    @GET("api/inspection_api/fetch_inspection_template.php")
    Call<JsonElement> getListTemplateType(@Query("page") int page, @Query("limit") int limit, @Query("property_id") String property_id, @Query("inspection_type_id") String inspection_type_id);

    @GET("api/inspection_api/fetch_all_unit_no_by_property.php")
    Call<JsonElement> getUnitNoForInspection(@QueryMap Map<String, Object> filter);

    @GET("api/inspection_api/fetch_all_employee_by_property.php")
    Call<JsonElement> getEmployeeDepartment(@QueryMap Map<String, Object> filter);

    @GET("api/property_api/fetch_owner_tenant_of_unit.php")
    Call<JsonElement> getInspectors(@Query("unit_id") String page, @Query("user_type") String limit);

    @POST("api/inspection_api/save_inspection_schedule.php")
    Call<JsonElement> saveTemplateInspection(@Body HashMap hashMap);

    @POST("api/inspection_api/save_inspection_form_all.php")
    Call<JsonElement> saveInspectionList(@Body HashMap hashMap);

    @GET("api/inspection_api/fetch_inspection_form.php")
    Call<JsonElement> getListInspectionTemplateForm(@Query("page") int page, @Query("limit") int limit, @Query("inspection_id") String inspection_id);

    @PUT("api/inspection_api/update_inspection_form_all.php")
    Call<JsonElement> updateInspectionList(@Body HashMap hashMap);

    @PUT("api/inspection_api/delete_main_and_sub_inspection.php")
    Call<JsonElement> deleteInspectionList(@Body HashMap hashMap);

    @GET("api/inspection_api/fetch_inspection_image_and_note.php")
    Call<JsonElement> getListImageInspection(@Query("part_type") String part_type, @Query("inspection_level") String inspection_level, @Query("inspection_room_id") String inspection_room_id, @Query("inspection_room_item_id") String inspection_room_item_id);

    @POST("api/inspection_api/upload_image_and_create_note_inspection.php")
    Call<JsonElement> upload_DeleteImageInspection(@Body HashMap hashMap);

    @GET("api/inspection_api/fetch_inspection_template_form.php")
    Call<JsonElement> getTemplateInspection(@Query("template_id") String template_id);

    @GET("api/inspection_api/fetch_inspection_list_by_schedule.php")
    Call<JsonElement> getInspectionListByCalendar(@QueryMap Map<String, Object> filter);

    // ============================ Work Order ================================
    @GET("api/work_order/fetch_work_order_list.php")
    Call<JsonElement> getWorkOrderList(@Query("page") int page, @Query("limit") int limit, @Query("work_order_type") String work_order_type,
                                       @Query("property_id") String property_id, @Query("user_id") String user_id, @Query("user_role") String user_role, @Query("active_account_id") String activeAccountId);

    @GET("api/work_order/fetch_work_order_detail.php")
    Call<JsonElement> getWorkOrderDetail(@Query("work_order_id") String work_order_id);

    @PUT("api/work_order/delete_image_of_work_order_and_progress.php")
    Call<JsonElement> deleteImageWorkOrder(@Body HashMap hashMap);

    @GET("api/work_order/fetch_work_order_progress_detail.php")
    Call<JsonElement> detailWorkProgress(@Query("work_progress_id") String work_progress_id);

    @PUT("api/work_order/update_work_order_detail.php")
    Call<JsonElement> updateWorkOrderDetail(@Body HashMap hashMap);

    @POST("api/work_order/add_and_update_work_progress_detail.php")
    Call<JsonElement> updateAndAddWorkProgressDetail(@Body HashMap hashMap);

    @GET("api/work_order/fetch_parent_task_form.php")
    Call<JsonElement> getParentTaskList(@Query("parent_task_id") String parent_task_id);

    @POST("api/maintenance_api/save_maintenance_schedule.php")
    Call<JsonElement> saveTemplateMaintenance(@Body HashMap hashMap);

    @GET("api/work_order/fetch_work_order_by_sub_inspection_id.php")
    Call<JsonElement> getListAssignWorkOrder(@Query("room_item_id") String room_item_id);

    @POST("api/work_order/create_work_order.php")
    Call<JsonElement> createWorkOrder(@Body HashMap hashMap);

    @GET("api/inspection_api/fetch_all_department_by_property.php")
    Call<JsonElement> getDepartmentName(@Query("property_id") String property_id);

    @GET("api/inspection_api/fetch_employee_and_position_by_department.php")
    Call<JsonElement> getEmployeeByDepartmentId(@Query("page") int page, @Query("limit") int limit, @Query("property_id") String property_id, @Query("department_id") String department_id);

    // ===================== Maintenance =====================

    @GET("api/maintenance_api/fetch_maintenance_type.php")
    Call<JsonElement> getMaintenanceType();

    @GET("api/maintenance_api/fetch_maintenance_list.php")
    Call<JsonElement> getListAllMaintenance(@Query("page") int page, @Query("limit") int limit, @Query("property_id") String property_id, @Query("maintenance_type_id") String inspection_type_id);

    @GET("api/maintenance_api/fetch_maintenance_template.php")
    Call<JsonElement> getListTemplateTypeMaintenance(@Query("page") int page, @Query("limit") int limit, @Query("property_id") String property_id, @Query("maintenance_type_id") String inspection_type_id);

    @GET("api/maintenance_api/fetch_maintenance_form.php")
    Call<JsonElement> getListMaintenanceForm(@Query("page") int page, @Query("limit") int limit, @Query("inspection_id") String inspection_id);

    // ================ HR Management ===========================

    //===> 1. Profile Management
    @GET("api/my_profile/my_profile_detail")
    Call<JsonElement> getMyProfileHRM(@Query("user_business_key") String user_id);

    //===> 2. Leave Management
    @GET("api/leave_application/fetch_leave_application_list_by_user")
    Call<JsonElement> getLeaveManagement(@Query("user_business_key") String userBusinessKey, @Query("page") int page, @Query("limit") int limit, @Query("list_type") String listType, @Query("account_business_key") String accountBusinessKey);

    @GET("api/leave_application/fetch_user_leave_balance")
    Call<JsonElement> getLeaveCategoryManagement(@Query("user_business_key") String user_id);

    @GET("api/leave_application/fetch_leave_request_data_create")
    Call<JsonElement> getLeaveTypeCategory(@Query("user_business_key") String userId, @Query("search_approver") String searchApprover);

    @POST("api/leave_application/leave_request_create")
    Call<JsonElement> createLeaveManagement(@Body HashMap<String, Object> hashMap);

    @GET("api/leave_application/leave_application_detail")
    Call<JsonElement> getLeaveDetail(@Query("id") String userId);

    @POST("api/leave_application/update_leave")
    Call<JsonElement> getActionApproveAndReject(@Body HashMap<String, Object> hashMap);

    //===> 3. PayRoll
    @GET("api/payroll/fetch_payroll_list_by_user")
    Call<JsonElement> getPayrollManagement(@Query("page") int page, @Query("limit") int limit, @Query("user_business_key") String user_id);

    @GET("api/payroll/payroll_detail")
    Call<JsonElement> getPayrollManagementDetailById(@Query("id") String id, @Query("user_business_key") String user_id);

    // Comfirm Receive Salary
    @POST("api/payroll/confirm_payment")
    Call<JsonElement> getConfirmReceiveSalary(@Body HashMap hashMap);

    //===> 4. List My Attendance
    @GET("api/attendance/fetch_attendance_list_by_user")
    Call<JsonElement> getListMyAttendance(@Query("page") int page, @Query("limit") int limit, @Query("user_business_key") String user_id);

    @GET("api/attendance/fetch_attendance_log")
    Call<JsonElement> getListMyAttendanceDetail(@Query("employee_attendance_id") String employeeAttendanceId, @Query("attendance_type") String attendanceType);

    @POST("api/attendance/employee_check_in_and_out")
    Call<JsonElement> checkOutAttendance(@Body HashMap hashMap);

    // View all list by date
    @GET("api/attendance/fetch_all_attendance_list_by_date")
    Call<JsonElement> getListAttendanceViewAllList(@Query("page") int page, @Query("limit") int limit, @Query("date") String date);

    //===> 5. Overtime****************

    //List EmployeeName
    @GET("api/over_time/fetch_all_employees")
    Call<JsonElement> getListEmployeeName(@Query("account_business_key") String accountBusinessKey, @Query("search") String search);

    //List Catgory Overtime
    @GET("api/over_time/fetch_all_overtimes_category")
    Call<JsonElement> getListCategoryOvertime(@Query("account_business_key") String accountBusinessKey);

    //List Catgory Overtime
    @GET("api/over_time/fetch_all_shift")
    Call<JsonElement> getListAllShift(@Query("account_business_key") String accountBusinessKey);

    //List Overtime List
    @GET("api/over_time/fetch_overtimes_list")
    Call<JsonElement> getListOvertimeList(@Query("page") int page, @Query("limit") int limit, @Query("user_business_key") String userBusinessKey, @Query("list_category") String listCategory, @Query("account_business_key") String accountBusinessKey, @Query("month") String month, @Query("year") String year);

    //List Overtime Detail List
    @GET("api/over_time/fetch_overtimes_list_detail")
    Call<JsonElement> getListDetailOvertime(@Query("overtime_list_id") String overtimeListId);

    //Create List Overtime
    @POST("api/over_time/create_overtime_request")
    Call<JsonElement> getCreateListOvertime(@Body HashMap hashMap);

    //Action Button Approve and Reject
    @POST("api/over_time/update_overtime")
    Call<JsonElement> getActionApproveReject(@Body HashMap hashMap);

    //===> 5. NoticeBoard****************
    //NoticeBoard
    @GET("api/noticeboard/fetch_all_noticeboard_list")
    Call<JsonElement> getNoticeBoard(@Query("page") int page, @Query("limit") int limit, @Query("account_business_key") String account_business_key);

    //List NoticeBoard Detail
    @GET("api/noticeboard/fetch_all_noticeboard_detail")
    Call<JsonElement> getNoticeBoardDetail(@Query("notice_list_id") String noticeListId);

    //===> 5. Resignation****************

    //List Resignation
    @GET("api/resignation/fetch_all_resignation_list")
    Call<JsonElement> getListResignation(@Query("page") int page, @Query("limit") int limit, @Query("active_account_business_key") String accountBusinessKey, @Query("user_business_key") String userBusinessKey);

    //List Detail Resignation
    @GET("api/resignation/fetch_resignation_detail")
    Call<JsonElement> getListDetailResignation(@Query("resign_list_id") String resignListId);

    //List Category Item Resignation
    @GET("api/resignation/fetch_resignation_data")
    Call<JsonElement> getCategoryItemResign(@Query("user_business_key") String userBusinessKey, @Query("search_approver") String searchApprover);

    //List Create Resignation
    @POST("api/resignation/create_resignation_request")
    Call<JsonElement> getCreateResignation(@Body HashMap hashMap);

    //List Action Button Resignation
    @POST("api/resignation/update_resignation_status")
    Call<JsonElement> getActionButtonResignation(@Body HashMap hashMap);

    //================= Facility Booking ======================
    @GET("api/facility_booking/fetch_facility_space.php")
    Call<JsonElement> getSpaceFacilityBooking(@Query("page") int page, @Query("limit") int limit, @Query("property_id") String property_id, @Query("facility_basic_id") String facility_basic_id, @Query("date_time") String date_time);

    @GET("api/facility_booking/fetch_facility_space_detail.php")
    Call<JsonElement> getSpaceFacilityBookingDetail(@Query("space_id") String space_id, @Query("user_id") String userId);

    @GET("api/facility_booking/fetch_facility_property.php")
    Call<JsonElement> getPropertyFacilityBooking();

    @GET("/api/facility_booking/search_facility_property.php")
    Call<JsonElement> getListSearchProperty(@Query("key_search") String key_search);

    @GET("api/facility_booking/fetch_facility_venue.php")
    Call<JsonElement> getVenueFacilityBooking(@Query("list_by") String list_by, @Query("property_id") String property_id);

    @GET("api/facility_booking/search_facility.php")
    Call<JsonElement> getSearchFacilityBooking(@Query("page") int page, @Query("limit") int limit, @Query("key_search") String key_search, @Query("property_id") String property_id, @Query("facility_basic_id") String facility_basic_id);

    @GET("api/facility_booking/fetch_facility_booking_item.php")
    Call<JsonElement> getListMyBookingDetail(@Query("page") int page, @Query("limit") int limit, @Query("user_id") String user_id, @Query("current_datetime") String current_datetime, @Query("booking_status") String booking_status);

    @GET("api/facility_booking/fetch_facility_booking_item_detail.php")
    Call<JsonElement> getListMyBookingDetailByItem(@Query("booking_item_id") String booking_item_id);

    @GET("api/facility_booking/check_coupon.php")
    Call<JsonElement> applyCouponBooking(@Query("space_id") String space_id, @Query("code") String code, @Query("date") String date, @Query("day_name") String day_name, @Query("total_amount") String total_amount);

    @POST("api/facility_booking/add_item_to_cart.php")
    Call<JsonElement> addToCardBooking(@Body HashMap hashMap);

    @GET("api/facility_booking/fetch_facility_cart_item.php")
    Call<JsonElement> getMyCardBooking(@Query("user_id") String user_id);

    @PUT("api/facility_booking/delete_facility_cart_item.php")
    Call<JsonElement> deleteCardBookingItem(@Body HashMap hashMap);

    @POST("api/facility_booking/check_out_cart_item.php")
    Call<JsonElement> checkOutPayBooking(@Body HashMap hashMap);

    @PUT("api/facility_booking/cancel_facility_booking_item.php")
    Call<JsonElement> cancelBookingItem(@Body HashMap hashMap);

    @PUT("api/facility_booking/update_facility_cart_item.php")
    Call<JsonElement> updateBookingItem(@Body HashMap hashMap);

    // ==== My Document ===========
    @GET("api/my_document/get_total_document_amount.php")
    Call<JsonElement> getNumberOfDocument(@Query("user_id") String userId);

    @GET("api/my_document/fetch_my_document_list.php")
    Call<JsonElement> getListMyDocument(@Query("page") int page, @Query("limit") int limit, @Query("document_category") String documentCategory, @Query("property_id") String propertyId, @Query("user_id") String userId);

    @GET("api/my_document/fetch_property_of_my_document.php")
    Call<JsonElement> getListPropertyDocument(@Query("document_category") String propertyId, @Query("user_id") String userId);

    // Scan info from qr code my unit
    @GET("api/smart_scan/smart_scan_validator.php")
    Call<JsonElement> getInfoFromMyUnit(@Query("user_id") String unitId, @Query("scanqr_info") String scanQrInfo, @Query("active_user_type") String active_user_type, @Query("active_account_id") String active_account_id);

    @POST("api/evaluation/evaluation_insert.php")
    Call<JsonElement> saveEvaluate(@Body HashMap hashMap);

    // Get Home item have property list and booking
    @GET("api/mobile_home_api/fetch_mobile_home_data.php")
    Call<JsonElement> getItemHomeMenu(@Query("current_datetime") String current_datetime, @Query("user_id") String userId);

    @GET("api/user/fetch_user_types_and_accounts")
    Call<JsonElement> getUserAccount();

    @POST("api/user/switch_user_active_account")
    Call<JsonElement> switchProfileUser(@Body HashMap hashMap);

    //************************* Sale Buy Rent (New) *****************
    @POST("api/property_for_sale_and_rent/create_property_for_sale_and_rent.php")
    Call<JsonElement> getListCreateSaleAndRent(@Body HashMap hashMap);

    @GET("api/property_for_sale_and_rent/fetch_property_for_sale_and_rent_list.php")
    Call<JsonElement> getListSaleAndRentProperty(@Query("page") int page, @Query("limit") int limit, @Query("category") String category, @Query("min_price") String min_price, @Query("max_price") String max_price);

    //Detail Sale and Rent
    @GET("api/property_for_sale_and_rent/get_property_for_sale_and_rent_detail.php")
    Call<JsonElement> getListDetailSaleAndRentProperty(@Query("id") String id);

    //**** Hotel Booking ****
    @GET("api/locations/fetch_all_locations")
    Call<JsonElement> getLocationHotel();

    @GET("api/homepage/get_homepage_by_category")
    Call<JsonElement> getHotelBookingV2All(@QueryMap Map<String, Object> map);

    @GET("api/places/fetch_places_by_category")
    Call<JsonElement> getHotelBookingSeeAll(@QueryMap Map<String, Object> map);

    // @GET("api/hotels/search_hotel")
    @GET("api/places/search_place")
    Call<JsonElement> getHotelBookingSearchLocation(@QueryMap Map<String, Object> filter);

    @GET("api/locations/get_location_detail")
    Call<JsonElement> getHotelBookingLocationDetail(@QueryMap Map<String, Object> filter);

    // @GET("api/hotels/get_place_detail_by_category")
    @GET("api/places/get_place_detail_by_category")
    Call<JsonElement> getHotelBookingLocationDetail(@Query("id") String hotelId, @Query("user_id") String userId, @Query("category") String category);

    // @GET("api/hotel/availability/{hotelId}")      // Use Path form like : https://booking.kesspay.com/api/hotel/availability/ 579
    @GET("api/rooms/check_room_availability")
    Call<JsonElement> getUnitBookingHotel(@Query("hotel_id") String hotelId, @Query("adult") String adult, @Query("child") String child, @Query("start_date") String start_date, @Query("end_date") String end_date);

    @POST("api/bookings/booking_draft")
    Call<JsonElement> bookingRoom(@Body HashMap hashMap);

    @POST("api/bookings/booking_submit")
    Call<JsonElement> bookingSubmitPayment(@Body HashMap hashMap);

    @GET("api/bookings/fetch_booking_history")
    Call<JsonElement> getHistoryHotelBooking(@QueryMap Map<String, Object> filter);

    @GET("api/bookings/fetch_booking_history_of_vendor")
    Call<JsonElement> getHistoryHotelBookingVendor(@QueryMap Map<String, Object> filter);

    @GET("api/bookings/get_booking_history_detail")
    Call<JsonElement> getHistoryDetailHotelBooking(@Query("booking_id") String hotel_id);

    @GET("api/users/get_vendor_dashboard")
    Call<JsonElement> getDashboardHotel(@QueryMap Map<String, Object> maps);

    @GET("api/hotels/fetch_vendor_hotel")
    Call<JsonElement> getHotelVendu(@Query("page") int page, @Query("limit") int limit, @Query("eazyhotel_user_id") String id);

    @GET("api/rooms/fetch_all_room_by_hotel")
    Call<JsonElement> getRoomOfHotelVendu(@Query("page") int page, @Query("limit") int limit, @Query("hotel_id") String hotel_id);

    @GET("api/places/fetch_wishlist_by_place")
    Call<JsonElement> getWishlistHotel(@Query("category") String category, @Query("page") int page, @Query("limit") int limit, @Query("user_id") String id);

    @POST("api/rooms/edit_room_status")
    Call<JsonElement> editRoomHotelVendor(@Body HashMap hashMap);

    @POST("api/hotels/edit_hotel_status")
    Call<JsonElement> editHotelVendor(@Body HashMap hashMap);

    // @POST("api/hotels/update_hotel_wishlist")
    @POST("api/places/update_wishlist_by_place")
    Call<JsonElement> editHotelVendorWishlist(@Body HashMap hashMap);

    @POST("api/places/rate_and_review_by_place")
    Call<JsonElement> addReviewHotel(@Body HashMap hashMap);

    @GET("api/places/fetch_nearby_places")
    Call<JsonElement> nearbyHotelLocation(@Query("category") String category, @Query("user_id") String eazyhotel_user_id, @Query("coord_lat") String user_lat, @Query("coord_long") String user_long);

    @GET("api/bookings/apply_coupon")
    Call<JsonElement> applyCoupon(@QueryMap Map<String, Object> maps);

    @GET("api/places/check_place_availability")
    Call<JsonElement> checkAvailableItem(@QueryMap Map<String, Object> map);

    // == Account User ==
    @GET("api/locations/get_location_detail_by_category")
    Call<JsonElement> locationDetailWs(@Query("id") String id, @Query("category") String category, @Query("coord_lat") String user_lat, @Query("coord_long") String user_long);

    @POST("api/login")
    Call<JsonObject> loginHotel(@Body HashMap user);

    @POST("api/users/register_social_media_user")
    Call<JsonObject> loginByFbGoogle(@Body HashMap user);

    @POST("api/users/send_and_verify_code")
    Call<JsonObject> getVerifyCodeHotel(@Body HashMap user);

    @GET("api/users/check_user_existed")
    Call<JsonObject> validatePhoneEmailExist(@QueryMap Map<String, Object> maps);

    @POST("api/users/register_user")
    Call<JsonObject> doSignUpAccountHotel(@Body HashMap user);

    @POST("api/users/change_user_password")
    Call<JsonElement> doResetPassword(@Body HashMap user);

    // == Travel Article ==
    @GET("api/_news/fetch_news_list")
    Call<JsonElement> getArticleListt(@Query("page") int page, @Query("limit") int limit);

    @GET("api/_news/get_news_detail")
    Call<JsonElement> getArticleDetail(@Query("id") String id);

    // == Travel Talk ==
    @GET("api/thread/fetch_threads")
    Call<JsonElement> getTravelTalkList(@Query("page") int page, @Query("limit") int limit, @Query("user_id") String user_id);

    @POST("api/thread/create_thread")
    Call<JsonElement> createPostTravelTalk(@Body HashMap hashMap);

    @POST("api/thread/like_toggle")
    Call<JsonElement> doLikeTravelTalk(@Body HashMap hashMap);

    @GET("api/thread/fetch_replies_by_thread")
    Call<JsonElement> getReplyCommentListTravelTalk(@Query("page") int page, @Query("limit") int limit, @Query("user_id") String user_id, @Query("thread_id") String thread_id);

    @POST("api/thread/reply")
    Call<JsonElement> doReplyTravelTalk(@Body HashMap hashMap);

    @GET("api/thread/fetch_categories")
    Call<JsonElement> getCategoryPost();

    @POST("api/thread/share")
    Call<JsonElement> getSharePost(@Body HashMap hashMap);

    @GET("api/thread/fetch_threads_by_user")
    Call<JsonElement> getLikeSharePostByUser(@Query("page") int page, @Query("limit") int limit, @Query("user_id") String user_id, @Query("type") String type);

    // Discount item hotel
    @GET("api/places/fetch_discounts_by_place")
    Call<JsonElement> getListDiscountItem(@Query("page") int page, @Query("limit") int limit, @Query("category") String category);

    // ** Visit Booking **
    @GET("api/community/visits")
    Call<JsonElement> getHomePageVisitItem();

    @GET("api/community/shows_api/{id}")
    Call<JsonElement> getItemDetailByMenus(@Path("id") String id);

    @GET("api/community/shows_api/{id}/filter/{item_id}")
    Call<JsonElement> getItemSearchByMenus(@Path("id") String id, @Path("item_id") String item_id);

    @GET("api/community/area_api/{id}")
    Call<JsonElement> getItemDetailByVisitLocation(@Path("id") String id);

    @GET("api/community/show_api/{id}")
    Call<JsonElement> visitItemDetail(@Path("id") String id);

    @GET("api/community/news_api/{id}")
    Call<JsonElement> visitNewsDetail(@Path("id") String id);

    @GET("api/community/ads_api/{id}")
    Call<JsonElement> visitSlideBanerDetail(@Path("id") String id);

    // ======= New login daikou =======
    @POST("api/login")
    Call<JsonObject> login(@Body HashMap user);

    @POST("api/user/send_verify_code")
    Call<JsonElement> sendVerifyCodeEmailPhone(@Body HashMap hashMap);

    @PUT("api/edit_first_login_on_mobile_status.php")
    Call<JsonElement> editFirstLoginStatus(@Body HashMap hashMap);

    @GET("api/user/get_user_detail")
    Call<JsonElement> getViewProfile(@Query("device_token") String deviceToken, @Query("app_name") String AppName);

    @POST("api/user/change_user_profile_or_cover_image")
    Call<JsonElement> getUpdateCoverProfileImage(@Body HashMap hashMap);

    @POST("api/user/change_user_email_or_phone")
    Call<JsonElement> editEmailOrPhone(@Body HashMap hashMap);

    @GET("api/forget_password.php")
    Call<JsonElement> getFilterPassword(@QueryMap Map<String, Object> filter);

    @POST("api/user/change_user_password")
    Call<JsonElement> changeAndForgetPassword(@Body HashMap hashMap);

    @POST("api/user/delete_user")
    Call<JsonElement> deleteAccountUser(@Body HashMap hashMap);

    //check account is exist or not
    @GET("api/user/can_create_user")
    Call<JsonElement> checkAccountExist(@Query("username") String userName);

    // == Notification ==
    @POST("api/notification/enable_or_disable_notification")
    Call<JsonElement> pushNotification(@Body HashMap hashMap);

    // == My Unit ==
    @GET("api/user/fetch_units_of_user")
    Call<JsonElement> getListMyUnit(@Query("page") int page, @Query("limit") int limit, @Query("type") String type);

    @GET("api/user/get_unit_of_user_detail")
    Call<JsonElement> getDetailMyUnit(@Query("id") String id);

    @POST("api/user/upload_my_unit_document")
    Call<JsonElement> uploadImgVideoMyUnit(@Body HashMap hashMap);

    @POST("api/user/upload_my_unit_document")
    Call<JsonElement> uploadVideoMyUnit(@Body RequestBody signup);

    @POST("api/user/delete_my_unit_document")
    Call<JsonElement> deleteImgVideoMyUnit(@Body HashMap hashMap);

    @GET("api/account/search_account")
    Call<JsonElement> getListItemPropertyComplaint(@Query("page") int page, @Query("limit") int limit, @Query("search") String search);

    @GET("api/unit/search_unit")
    Call<JsonElement> getListUnitNoByAccount(@Query("page") int page, @Query("limit") int limit, @Query("account_id") String accountId, @Query("search") String search);

    @GET("api/user/fetch_invoices_by_user")
    Call<JsonElement> getPaymentInvoice(@Query("page") int page, @Query("limit") int limit, @Query("scope") String scope, @Query("account_id") String id);

    // FrontDesk
    @GET("api/frontdesks/search")
    Call<JsonElement> searchFrontDesk(@Query("page") int page, @Query("limit") int limit, @Query("account_id") String accountId, @Query("search") String search);

    @GET("api/frontdesks/get_detail")
    Call<JsonElement> detailFrontDesk(@Query("id") String id);

    @POST("api/frontdesks/assign_picker")
    Call<JsonElement> getAssignPickerComplaint(@Body HashMap hashMap);
    @GET("api/opportunities/fetch_opportunities_by_user")
    Call<JsonElement> listOpportunity(@Query("page") int page, @Query("limit") int limit);

    @GET("api/opportunities/get_opportunity_detail")
    Call<JsonElement> detailOpportunity(@Query("id") String id);

    @GET("api/quotations/fetch_quotations_by_user")
    Call<JsonElement> listQuotation(@Query("page") int page, @Query("limit") int limit);

    @GET("api/quotations/get_quotation_detail")
    Call<JsonElement> detailQuotation(@Query("id") String id);

    @GET("api/holds_and_reserves/fetch_holds_and_reserves_by_user")
    Call<JsonElement> listHoleAndReserved(@Query("page") int page, @Query("limit") int limit);

    @GET("api/holds_and_reserves/get_hold_and_reserve_detail")
    Call<JsonElement> detailHoldAndReserved(@Query("id") String id);

    @GET("api/bookings/fetch_bookings_by_user")
    Call<JsonElement> listBookingDR(@Query("page") int page, @Query("limit") int limit);

    @GET("api/sale_agreements/fetch_sale_agreement_list")
    Call<JsonElement> listSaleAgreementDR(@Query("page") int page, @Query("limit") int limit);

    @GET("api/bookings/get_booking_detail")
    Call<JsonElement> detailBookingDR(@Query("id") String id);

    @GET("api/sale_agreements/get_sale_agreement_detail")
    Call<JsonElement> detailSaleAgreementDR(@Query("id") String id);

    @GET("api/unit/get_unit_detail")
    Call<JsonElement> detailUnitInterestingAndRecommended(@Query("id") String id);

    @GET("api/user/fetch_user_roles")
    Call<JsonElement> getUserMyRolePermission(@QueryMap Map<String, Object> maps);

    @POST("api/evaluate/evaluate")
    Call<JsonElement> getEvaluate(@Body HashMap hashMap);

    @GET("api/evaluate/fetch_evaluations")
    Call<JsonElement> getListEvaluate(@Query("page") int page, @Query("limit") int limit, @Query("list_by") String listBy, @Query("account_id") String accountId);

    @GET("api/evaluate/get_evaluation")
    Call<JsonElement> getDetailEvaluate(@Query("id") String id);
}

