package com.eazy.daikou.repository_ws;

import android.content.Context;

import com.eazy.daikou.helper.UserSessionManagement;

public class Constant {

//    public static final String baseUrl = "https://eazy.daikou.asia/";    //pro
//    public static final String baseUrlHr = "https://hrm.daikou.asia/";   //pro
//
//    public static final String baseUrlHotel = "https://booknow.asia/";    //pro

    public static final String baseUrlHr = "https://sandbox.daikouhrm.asia/"; //dev
    public static final String baseUrl = "https://sandboxeazy.daikou.asia/";  //dev

    public static final String baseUrlHotel = "https://dev.booknow.asia/";   //dev

    public static String kess_url = "https://kesspay.io";
    public static String kess_url_dev = "devwebpayment.kesspay.io";
    public static String kessChatDeepLink = "io.kessinnovation.kesschat";

    public static String url_beetube = "https://beetube.live/";
    public static String url_beetube_movies = "https://beetube.live/movies";
    public static String url_beetube_news = "https://news.beetube.live";
    public static String url_property_website = "https://www.propertyarea.asia";

    public final static String FIREBASE_TOKEN = "fToken";
    public final static String OUT_IN_APP = "";
    public final static String SHARE_PREFERENCE_NAME = "Daikou_Eazy";
    public final static String FIRST_SCAN_QR = "true";

    public final static String TAG = "daikou_log_debug";

    // ======================= Check user login =====================
    public static boolean isLoggedIn(Context context){
        return new UserSessionManagement(context).IsUserLoggedIn("IsUserLoggedIn");
    }

    // ======================= Language Code ==================
    public final static String LANG_KH = "kh";
    public final static String LANG_CN = "cn";
    public final static String LANG_EN = "en";
}