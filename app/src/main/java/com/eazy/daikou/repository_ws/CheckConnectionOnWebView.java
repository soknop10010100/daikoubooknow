package com.eazy.daikou.repository_ws;

public interface CheckConnectionOnWebView {
    void onConnected(boolean isConnected);
    void urlWebView(String url);
}
