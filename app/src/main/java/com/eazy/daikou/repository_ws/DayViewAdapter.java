package com.eazy.daikou.repository_ws;

import com.eazy.daikou.ui.home.calendar.calendar_custom.CalendarCellView;

/** Adapter used to provide a layout for {@link CalendarCellView}.*/
public interface DayViewAdapter {
  void makeCellView(CalendarCellView parent);
}
