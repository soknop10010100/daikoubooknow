package com.eazy.daikou.repository_ws;

import static com.eazy.daikou.helper.Utils.logDebug;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.widget.Button;

import androidx.core.content.res.ResourcesCompat;

import com.eazy.daikou.R;
import com.eazy.daikou.helper.CheckIsAppActive;
import com.eazy.daikou.helper.InternetConnection;
import com.eazy.daikou.helper.UserSessionManagement;
import com.eazy.daikou.helper.Utils;
import com.eazy.daikou.ui.LoginActivity;
import com.eazy.daikou.ui.home.book_now.booking_account.LoginBookNowActivity;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CustomCallback implements Callback<JsonElement> {
    private final CustomResponseListener customResponseListener;
    private final Context mContext;

    public CustomCallback(Context context, CustomResponseListener listener) {
        customResponseListener = listener;
        // Obtain the FirebaseAnalytics instance.
        mContext = context;
    }

    @Override
    public void onResponse(@NotNull Call<JsonElement> call, Response<JsonElement> response) {
        Utils.logDebug(Constant.TAG, response.toString());
        if (response.isSuccessful()) {
            JsonElement jsonElement = response.body();
            if (jsonElement != null && !jsonElement.isJsonNull() && jsonElement instanceof JsonObject) {
                logDebug("logcallbacksucceess", "c: "+ jsonElement.getAsJsonObject());
                try {
                    customResponseListener.onSuccess(jsonElement.getAsJsonObject());
                } catch (Exception exception){
                    customResponseListener.onError(exception.getMessage(), response.code());
                }
            } else {
                customResponseListener.onError("Wrong Json Type!",response.code());
            }
        } else {
            if (response.errorBody() != null) {
                ResponseBody errorString = response.errorBody();
                logDebug("logcallbackerorr",response.errorBody().toString());
                String message = "";
                try {
                    String json = errorString.string();
                    logDebug("dfkj",errorString.toString());
                    logDebug("error", json);
                    JSONObject jsonOb = new JSONObject(json);
                    if (response.code() == 401) {
                        if(jsonOb.has("message")){
                            message = jsonOb.getString("message");
                            customResponseListener.onError(message,response.code());
                        } else if (jsonOb.has("error") && !jsonOb.getString("error").equalsIgnoreCase("Unauthenticated access")) {
                            message = jsonOb.getString("error");
                            customResponseListener.onError(message,response.code());
                        } else if (jsonOb.has("msg")){
                            popupSessionExpired(mContext);
                        }
                    } else if(response.code() == 405){
                        if (jsonOb.has("msg")){
                            message = jsonOb.getString("msg");
                            customResponseListener.onError(message,response.code());
                        }
                    }
                    else if (jsonOb.has("msg")) {
                        message = jsonOb.getString("msg");
                        customResponseListener.onError(message,response.code());
                    } else if (jsonOb.has("error")) {
                        message = jsonOb.getString("error");
                        customResponseListener.onError(message,response.code());
                    }
                    else if (jsonOb.has("message")){
                        message = jsonOb.getString("message");
                        customResponseListener.onError(message,response.code());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("jfgdf"+e.getClass().getName(), e.getMessage() + "");
                    message = e.getMessage();
                    customResponseListener.onError(message,response.code());
                } catch (IOException e2) {
                    Log.e(e2.getClass().getName(), e2.getMessage() + "");
                    message = e2.getMessage();
                    customResponseListener.onError(message,response.code());
                }
            }
        }
    }

    @Override
    public void onFailure(@NotNull Call<JsonElement> call, @NotNull Throwable t ) {
        if (!InternetConnection.checkInternetConnectionActivity(mContext)) {
            customResponseListener.onError(mContext.getString(R.string.please_check_your_internet_connection), 500);
        } else  {
            customResponseListener.onError(mContext.getString(R.string.something_went_wrong), 500);
        }
    }

    @SuppressLint("SetTextI18n")
    public static void popupSessionExpired(final Context context) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog
                .setTitle(context.getResources().getString(R.string.confirm))
                .setMessage(context.getString(R.string.unauthorized_access_you_must_go_to_login_agin))
                .setPositiveButton(context.getResources().getString(R.string.yes), (dialog, which) -> {
                    new UserSessionManagement(context).logoutUser();
                    Intent intent = new Intent(context, CheckIsAppActive.Companion.is_daikou_active() ? LoginActivity.class : LoginBookNowActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    context.startActivity(intent);
                    dialog.dismiss();
                })
                .setCancelable(false)
                .setIcon(ResourcesCompat.getDrawable(context.getResources(), R.drawable.ic_report_problem, null));

        AlertDialog alert = alertDialog.create();
        alert.show();
        Button okButton = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
        okButton.setTextColor(Utils.getColor(context, R.color.gray));
        Button cancelButton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        cancelButton.setTextColor(Utils.getColor(context, CheckIsAppActive.Companion.is_daikou_active() ? R.color.appBarColorOld : R.color.book_now_secondary));
    }

}
