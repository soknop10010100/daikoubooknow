package com.eazy.daikou.repository_ws;

import com.google.gson.JsonObject;

public interface CustomResponseListener {
    void onSuccess(JsonObject resObj);
    void onError(String error,int resCode);
}
