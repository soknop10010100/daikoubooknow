package com.eazy.daikou.repository_ws;

import android.view.View;

import com.eazy.daikou.helper.Utils;

public class CustomSetOnClickViewListener implements View.OnClickListener {
    private final CustomResponseOnClickListener customResponseOnClickListener;

    public CustomSetOnClickViewListener(CustomResponseOnClickListener customResponseOnClickListener){
        this.customResponseOnClickListener = customResponseOnClickListener;
    }

    @Override
    public void onClick(View v) {
        Utils.checkEnableView(v);
        customResponseOnClickListener.onClick(v);
    }
}
