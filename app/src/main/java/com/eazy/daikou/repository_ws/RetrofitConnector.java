package com.eazy.daikou.repository_ws;

import static com.eazy.daikou.helper.Utils.logDebug;

import android.content.Context;

import com.eazy.daikou.helper.Utils;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class RetrofitConnector {

    public static final int TIME_OUT = 30;

    public static Retrofit getRetrofit(Context context) {
        final String authToken = "Bearer " + Utils.getString("token", context);
        logDebug("authToken", authToken);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(chain -> {
            Request original = chain.request();
            Request.Builder builder = original.newBuilder()
                    .header("Content-Type", "application/json")
                    .header("Accept", "application/json")
                    .header("Authorization", authToken)
                    .header("Request-Type", "mobile")
                    // .header("Cookie", "acci=" + userId + "; acct=" + userCookie)
                    // .header("Cookie", "acci=" + "1999" + "; acct=" + "$2y$10$PD3jZnwilccjUj7fKE7LkO39I4VNmMUkZujXxsMBaY/kzmqGqAvRe")
                    .method(original.method(), original.body());

            Request request = builder.build();
            return chain.proceed(request);
        });
        httpClient.readTimeout(TIME_OUT, TimeUnit.SECONDS).connectTimeout(TIME_OUT, TimeUnit.SECONDS);
        return new Retrofit.Builder()
                .baseUrl(Constant.baseUrl)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().setLenient().create()))
                .client(httpClient.build())
                .build();
    }

    public static Retrofit getFormDataRetrofit(Context context) {
        final String authToken = "Bearer " + Utils.getString("token", context);
        logDebug("authToken", authToken);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(chain -> {
//            RequestBody body = new MultipartBody.Builder().setType(MultipartBody.FORM)
//                    .addFormDataPart("file","/C:/Users/User/Pictures/new_logo.png",
//                            RequestBody.create(MediaType.parse("application/octet-stream"),
//                                    new File("/C:/Users/User/Pictures/new_logo.png")))
//                    .addFormDataPart("file_type","video")
//                    .addFormDataPart("unit_id","30102")
//                    .build();
//
            Request original = chain.request();
            Request.Builder builder = original.newBuilder()
                    .header("Content-Type", "application/json")
                    .header("Accept", "application/json")
                    .header("Authorization", authToken)
                    .header("Request-Type", "mobile")
                    // .header("Cookie", "acci=" + userId + "; acct=" + userCookie)
                    // .header("Cookie", "acci=" + "1999" + "; acct=" + "$2y$10$PD3jZnwilccjUj7fKE7LkO39I4VNmMUkZujXxsMBaY/kzmqGqAvRe")
                    .method(original.method(), original.body());

            Request request = builder.build();
            return chain.proceed(request);
        });
        httpClient.readTimeout(TIME_OUT, TimeUnit.SECONDS).connectTimeout(TIME_OUT, TimeUnit.SECONDS);
        return new Retrofit.Builder()
                .baseUrl(Constant.baseUrl)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().setLenient().create()))
                .client(httpClient.build())
                .build();
    }

    // USE THIS METHOD FOR GET RETROFIT LINK CLIENT WITH SERVER
    public static ServiceApi createService(Context context){
        return getRetrofit( context).create(ServiceApi.class);
    }

}
