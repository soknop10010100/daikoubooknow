package com.eazy.daikou.repository_ws;

import static com.eazy.daikou.helper.Utils.logDebug;

import android.content.Context;

import com.eazy.daikou.helper.Utils;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitGenerator {

    public static ServiceApi createServiceWithHotelBooking(Context context) {
        final String authToken = "Bearer " + Utils.getString("token", context);
        logDebug("authToken", authToken);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(chain -> {
            Request original = chain.request();
            Request.Builder builder = original.newBuilder()
                    .header("Authorization", authToken)
                    .header("Request-Type", "mobile")
                    .method(original.method(), original.body());
            Request request = builder.build();
            return chain.proceed(request);
        });

        httpClient.readTimeout(60, TimeUnit.SECONDS).connectTimeout(60, TimeUnit.SECONDS);
        Retrofit.Builder builder = new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create()).baseUrl(Constant.baseUrlHotel);
        builder.client(httpClient.build());
        Retrofit retrofit = builder.build();
        return retrofit.create(ServiceApi.class);
    }

    public static ServiceApi createServiceWithHR(Context activity) {
        final String authToken = "Bearer " + Utils.getString("token", activity);
        logDebug("authToken", authToken);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(chain -> {
            Request original = chain.request();
            Request.Builder builder = original.newBuilder()
                    .header("Authorization", authToken)
                    .method(original.method(), original.body());
            Request request = builder.build();
            return chain.proceed(request);
        });

        httpClient.readTimeout(60, TimeUnit.SECONDS).connectTimeout(60, TimeUnit.SECONDS);
        Retrofit.Builder builder = new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create()).baseUrl(Constant.baseUrlHr);
        builder.client(httpClient.build());
        Retrofit retrofit = builder.build();
        return retrofit.create(ServiceApi.class);
    }


    public static ServiceApi createCategoryService(boolean isBookNowApp){
        int timeout = 30;
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.readTimeout(timeout, TimeUnit.SECONDS).connectTimeout(timeout, TimeUnit.SECONDS);
        Retrofit.Builder builder = new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create()).baseUrl(isBookNowApp ? Constant.baseUrlHotel : Constant.baseUrl);
        builder.client(httpClient.build());
        Retrofit retrofit = builder.build();
        return retrofit.create(ServiceApi.class);
    }
}
