package com.eazy.daikou.request_data.request.profile_ws;

import android.content.Context;

import com.eazy.daikou.repository_ws.CustomCallback;
import com.eazy.daikou.repository_ws.CustomResponseListener;
import com.eazy.daikou.repository_ws.RetrofitGenerator;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.eazy.daikou.R;
import com.eazy.daikou.repository_ws.RetrofitConnector;
import com.eazy.daikou.repository_ws.ServiceApi;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PasswordWs {

    public void changeAndForgetPasswordWS(Context context, HashMap<String, String> hashMapPass, PasswordCallBackListener passwordCallBackListener){
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = serviceApi.changeAndForgetPassword(hashMapPass);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("success") && resObj.get("success").getAsString().equals("true")){
                    passwordCallBackListener.onSuccess(resObj.get("msg").getAsString());
                } else {
                    passwordCallBackListener.onWrong(resObj.get("msg").getAsString());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                passwordCallBackListener.onFailed(error);
            }
        }));
    }

    public void forgotPassword(Context context, Map<String, Object> hashMapPass, String phone_mail,ForgotPasswordCallBackListener forgotPasswordCallBackListener){
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = serviceApi.getFilterPassword(hashMapPass);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("success") && resObj.get("success").getAsString().equals("true")){
                    forgotPasswordCallBackListener.onSuccess(phone_mail);
                } else {
                    forgotPasswordCallBackListener.onSuccessFalse(resObj.get("msg").getAsString());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                forgotPasswordCallBackListener.onFailed(error);
            }
        }));

    }

    public void verifyCode(Context context, Map<String, Object> hashMapPass, String getPinCode, VerifyCodeListener verifyCodeListener) {
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = serviceApi.verifyCode(hashMapPass);
        res.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(@NotNull Call<JsonElement> call, @NotNull Response<JsonElement> response) {
                if (response.isSuccessful() && response.body() instanceof JsonObject) {
                    JsonObject resObj = response.body ().getAsJsonObject ();
                    if (resObj.has ( "success" ) && resObj.get("success").getAsString().equals("true")) {
                        verifyCodeListener.onSuccess(getPinCode);
                    } else {
                        verifyCodeListener.onWrong();
                    }
                }else {
                    verifyCodeListener.onWrong();
                }
            }

            @Override
            public void onFailure(@NotNull Call<JsonElement> call, @NotNull Throwable t) {
                verifyCodeListener.onFailed(context.getResources().getString(R.string.something_went_wrong));
            }
        });
    }

    public void resetNewPassword(Context context, HashMap<String, Object> hashMapPass, boolean isBookNowApp, ResetNewPassCallBack resetNewPassCallBack) {
        ServiceApi serviceApi = isBookNowApp ? RetrofitGenerator.createCategoryService(true) : RetrofitConnector.createService(context);    //Check Book Now
        Call<JsonElement> res = isBookNowApp ? serviceApi.doResetPassword(hashMapPass) : serviceApi.verifyCode(hashMapPass);    //Check Book Now

        res.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(@NotNull Call<JsonElement> call, @NotNull Response<JsonElement> response) {
                if (response.isSuccessful() && response.body() instanceof JsonObject) {
                    JsonObject resObj = response.body ().getAsJsonObject ();
                    if (resObj.has ( "success" ) && resObj.get("success").getAsString().equals("true")) {
                        resetNewPassCallBack.onSuccess();
                    } else {
                        resetNewPassCallBack.onFailed(resObj.get("msg").getAsString());
                    }
                }else {
                    resetNewPassCallBack.onFailed(context.getResources().getString(R.string.something_went_wrong));
                }
            }

            @Override
            public void onFailure(@NotNull Call<JsonElement> call, @NotNull Throwable t) {
                resetNewPassCallBack.onFailed(context.getResources().getString(R.string.something_went_wrong));
            }
        });
    }

    public interface ResetNewPassCallBack{
        void onSuccess();
        void onWrong();
        void onFailed(String mess);
    }
    public interface PasswordCallBackListener{
        void onSuccess(String msgSuccess);
        void onWrong(String msg);
        void onFailed(String mess);
    }

    public interface ForgotPasswordCallBackListener{
        void onSuccess(String phone_email);
        void onSuccessFalse(String status);
        void onWrong(String msg);
        void onFailed(String mess);
    }

    public interface VerifyCodeListener{
        void onSuccess(String getPinCode);
        void onWrong();
        void onFailed(String error);
    }
}
