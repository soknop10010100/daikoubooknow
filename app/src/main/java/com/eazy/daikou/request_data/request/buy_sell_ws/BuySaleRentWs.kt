package com.eazy.daikou.request_data.request.buy_sell_ws

import android.content.Context
import com.eazy.daikou.repository_ws.RetrofitConnector
import com.eazy.daikou.repository_ws.CustomCallback
import com.eazy.daikou.repository_ws.CustomResponseListener
import com.eazy.daikou.model.buy_sell.DetailBuyAndSaleRentModel
import com.eazy.daikou.model.buy_sell.SaleAndRentProperties
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.google.gson.JsonObject

class BuySaleRentWs {

    fun createListSaleAndRentPropertyWS(context: Context, hashMap: HashMap<String, Any?>, callBackCreate : OnCallBackCreateSaleAndRentListener){
        val serviceApi = RetrofitConnector.createService(context)
        val res = serviceApi.getListCreateSaleAndRent(hashMap)
        res.enqueue(CustomCallback(context, object : CustomResponseListener{
            override fun onSuccess(resObj: JsonObject?) {
                if (resObj != null) {
                    if (resObj.has("success") && resObj["success"].asString == "true" ){
                        if (resObj.has("msg")){
                            callBackCreate.onSuccess(resObj["msg"].asString)
                        }
                    } else{
                        if (resObj.has("msg")){
                            callBackCreate.onFailed(resObj["msg"].asString)
                        }
                    }
                }
            }
            override fun onError(error: String, resCode: Int) {
               callBackCreate.onFailed(error)
            }
        }))
    }

    fun getListSaleAndRentPropertyWs(context: Context, page: Int, limit: Int, category: String, min_price: String, max_price: String, callBackList : OnCallBackListSaleRentListener){
        val serviceApi = RetrofitConnector.createService(context)
        val res = serviceApi.getListSaleAndRentProperty(page, limit, category, min_price, max_price)
        res.enqueue(CustomCallback(context, object : CustomResponseListener{
            override fun onSuccess(resObj: JsonObject?) {
               if (resObj != null){
                   if (resObj.has("data")){
                       val listProperty : ArrayList<SaleAndRentProperties> = ArrayList()
                       val data = resObj["data"].asJsonArray
                       for (i in 0 until data.size()){
                           val item : JsonElement = data[i].asJsonObject
                           val saleAndRentProperty : SaleAndRentProperties = Gson().fromJson(item, SaleAndRentProperties::class.java)
                           listProperty.add(saleAndRentProperty)
                       }
                       callBackList.onSuccess(listProperty)
                   } else{
                       callBackList.onFailed(resObj["msg"].asString)
                   }
               }
            }
            override fun onError(error: String, resCode: Int) {
                callBackList.onFailed(error)
            }
        }))
    }
    fun getListDetailSaleAndRent(context: Context, id: String,  callBackList: OnDetailCallBackListener){
        val serviceApi = RetrofitConnector.createService(context)
        val res = serviceApi.getListDetailSaleAndRentProperty(id)
        res.enqueue(CustomCallback(context, object : CustomResponseListener{
            override fun onSuccess(resObj: JsonObject?) {
               if (resObj != null){
                   if (resObj.has("data") && !resObj["data"].isJsonNull){
                       val data = resObj["data"].asJsonObject
                       val detailBuyAndSale : DetailBuyAndSaleRentModel = Gson().fromJson(data.asJsonObject, DetailBuyAndSaleRentModel::class.java)
                       callBackList.onSuccess(detailBuyAndSale)
                   } else{
                      callBackList.onFailed(resObj["msg"].asString)
                   }
               }
            }
            override fun onError(error: String, resCode: Int) {
                callBackList.onFailed(error)
            }
        }))
    }

    interface OnCallBackCreateSaleAndRentListener{
        fun onSuccess(message: String)
        fun onFailed(error: String)
    }
    interface OnCallBackListSaleRentListener{
        fun onSuccess( listProperty : ArrayList<SaleAndRentProperties>)
        fun onFailed(error: String)
    }
    interface OnDetailCallBackListener{
        fun onSuccess( detailBuySaleAndRentModel : DetailBuyAndSaleRentModel)
        fun onFailed(error: String)
    }
}