package com.eazy.daikou.request_data.request.data_record

import android.content.Context
import com.eazy.daikou.repository_ws.CustomCallback
import com.eazy.daikou.repository_ws.CustomResponseListener
import com.eazy.daikou.repository_ws.RetrofitConnector
import com.eazy.daikou.model.data_record.DetailHoldAndReservedModel
import com.eazy.daikou.model.data_record.DetailOpportunityModel
import com.eazy.daikou.model.data_record.DetailQuotationModel
import com.eazy.daikou.model.data_record.SubListDataRecord
import com.eazy.daikou.model.my_property.my_property.PropertyMyUnitModel
import com.google.gson.Gson
import com.google.gson.JsonObject

class DataRecordWS {
    val subListDataRecord : ArrayList<SubListDataRecord> = ArrayList()

    fun getListOpportunityWS(context: Context, page : Int, limit : Int, callBackOpportunity : OnCallBackOpportunityListener){
        val serviceApi = RetrofitConnector.createService(context)
        val res = serviceApi.listOpportunity(page, limit)
        res.enqueue(CustomCallback(context, object : CustomResponseListener{
            override fun onSuccess(resObj: JsonObject?) {
                if (resObj != null){
                    if (resObj.has("data")){
                        val data = resObj["data"].asJsonArray
                        if (data.size() > 0){
                            for (item in data){
                                val opportunity  = SubListDataRecord()
                                opportunity.id = validateData(item.asJsonObject, "id")
                                opportunity.createdDate = validateData(item.asJsonObject, "created_at")
                                opportunity.opportunityName = validateData(item.asJsonObject, "name")
                                opportunity.status  = validateData(item.asJsonObject, "status")

                                if (item.asJsonObject.has("account")){
                                    val unitJsonObject = item.asJsonObject.get("account").asJsonObject
                                    opportunity.projectName = validateData(unitJsonObject, "name")
                                }
                                subListDataRecord.add(opportunity)
                            }

                        }
                        callBackOpportunity.onSuccess(subListDataRecord)
                    } else{
                        callBackOpportunity.onFailed(resObj["msg"].asString)
                    }
                }
            }
            override fun onError(error: String, resCode: Int) {
                callBackOpportunity.onFailed(error)
            }
        }))
    }

    fun getListQuotationWs(context: Context, page: Int, limit : Int, callBackQuotation : OnCallBackOpportunityListener){
        val serviceApi = RetrofitConnector.createService(context)
        val res = serviceApi.listQuotation(page, limit)
        res.enqueue(CustomCallback(context, object : CustomResponseListener{
            override fun onSuccess(resObj: JsonObject?) {
                if (resObj != null){
                    if (resObj.has("data")){
                        val data = resObj["data"].asJsonArray
                        if (data.size() > 0 ){
                            for (item in data){
                                val quotation  = SubListDataRecord()
                                quotation.id = validateData(item.asJsonObject, "id")
                                quotation.no = validateData(item.asJsonObject, "no")
                                quotation.createdDate = validateData(item.asJsonObject, "created_at")
                                quotation.expire_date = validateData(item.asJsonObject, "expire_date")
                                quotation.price = validateData(item.asJsonObject, "price")
                                quotation.status = validateData(item.asJsonObject, "status")

                                if (item.asJsonObject.has("account")){
                                    val unitJsonObject = item.asJsonObject.get("account").asJsonObject
                                    quotation.projectName = validateData(unitJsonObject, "name")
                                }

                                if (item.asJsonObject.has("unit")){
                                    val unitJsonObject = item.asJsonObject.get("unit").asJsonObject
                                    quotation.unitNo = validateData(unitJsonObject, "name")
                                    quotation.streetFloor = validateData(unitJsonObject, "floor_no")
                                    // Type
                                    val typeJsonObject = unitJsonObject.get("type").asJsonObject
                                    quotation.unitType = validateData(typeJsonObject, "name")
                                }
                                subListDataRecord.add(quotation)
                            }
                        }
                        callBackQuotation.onSuccess(subListDataRecord)
                    } else{
                        callBackQuotation.onFailed(resObj["msg"].asString)
                    }
                }
            }

            override fun onError(error: String, resCode: Int) {
                callBackQuotation.onFailed(error)
            }

        }))
    }

    fun getListHoldWithReservedWs(context: Context, page: Int, limit : Int, callBackHoldWithReserved : OnCallBackOpportunityListener){
        val serviceApi = RetrofitConnector.createService(context)
        val res = serviceApi.listHoleAndReserved(page, limit)
        res.enqueue(CustomCallback(context, object : CustomResponseListener{
            override fun onSuccess(resObj: JsonObject?) {
                if (resObj != null){
                    if (resObj.has("data")){
                        val data = resObj["data"].asJsonArray
                        if (data.size() > 0 ){
                            for (item in data){
                                val holdWithReserves  = SubListDataRecord()
                                holdWithReserves.id = validateData(item.asJsonObject, "id")
                                holdWithReserves.no = validateData(item.asJsonObject, "code")
                                holdWithReserves.amount = validateData(item.asJsonObject, "amount")
                                holdWithReserves.type = validateData(item.asJsonObject, "type")
                                holdWithReserves.startDate = validateData(item.asJsonObject, "start_date")
                                holdWithReserves.endDate = validateData(item.asJsonObject, "end_date")
                                holdWithReserves.endTime = validateData(item.asJsonObject,"end_time")
                                holdWithReserves.status = validateData(item.asJsonObject, "status")

                                if (item.asJsonObject.has("account")){
                                    val unitJsonObject = item.asJsonObject.get("account").asJsonObject
                                    holdWithReserves.projectName = validateData(unitJsonObject, "name")
                                }

                                if (item.asJsonObject.has("unit")){
                                    val unitJsonObject = item.asJsonObject.get("unit").asJsonObject
                                    holdWithReserves.unitNo = validateData(unitJsonObject, "name")
                                }
                                subListDataRecord.add(holdWithReserves)
                            }
                        }
                        callBackHoldWithReserved.onSuccess(subListDataRecord)
                    } else{
                        callBackHoldWithReserved.onFailed(resObj["msg"].asString)
                    }
                }
            }

            override fun onError(error: String, resCode: Int) {
                callBackHoldWithReserved.onFailed(error)
            }

        }))
    }

    fun getListBookingWS(context: Context, page: Int, limit: Int, callBackBooking : OnCallBackOpportunityListener){
        val serviceApi = RetrofitConnector.createService(context)
        val res = serviceApi.listBookingDR(page, limit)
        res.enqueue(CustomCallback(context, object : CustomResponseListener{
            override fun onSuccess(resObj: JsonObject?) {
               if (resObj != null){
                   if (resObj.has("data")){
                       val data = resObj["data"].asJsonArray
                       if (data.size() > 0 ){
                           for (item in data){
                               val booking  = SubListDataRecord()
                               booking.id = validateData(item.asJsonObject, "id")
                               booking.no = validateData(item.asJsonObject, "code")
                               booking.amount = validateData(item.asJsonObject, "amount")
                               booking.startDate = validateData(item.asJsonObject, "start_date")
                               booking.endDate = validateData(item.asJsonObject, "end_date")
                               booking.endTime = validateData(item.asJsonObject,"end_time")
                               booking.status = validateData(item.asJsonObject, "status")

                               if (item.asJsonObject.has("account")){
                                   val unitJsonObject = item.asJsonObject.get("account").asJsonObject
                                   booking.projectName = validateData(unitJsonObject, "name")
                               }

                               if (item.asJsonObject.has("unit")){
                                   val unitJsonObject = item.asJsonObject.get("unit").asJsonObject
                                   booking.unitNo = validateData(unitJsonObject, "name")
                               }

                               subListDataRecord.add(booking)
                           }
                       }
                       callBackBooking.onSuccess(subListDataRecord)
                   } else {
                       callBackBooking.onFailed(resObj["msg"].asString)
                   }
               }
            }

            override fun onError(error: String, resCode: Int) {
                callBackBooking.onFailed(error)
            }
        }))
    }

    fun getListSaleAgreementsWS(context: Context, page: Int, limit: Int, callBackBooking : OnCallBackOpportunityListener){
        val serviceApi = RetrofitConnector.createService(context)
        val res = serviceApi.listSaleAgreementDR(page, limit)
        res.enqueue(CustomCallback(context, object : CustomResponseListener{
            override fun onSuccess(resObj: JsonObject?) {
                if (resObj != null){
                    if (resObj.has("data")){
                        val data = resObj["data"].asJsonArray
                        if (data.size() > 0 ){
                            for (item in data){
                                val saleAgreement  = SubListDataRecord()
                                saleAgreement.id = validateData(item.asJsonObject, "id")
                                saleAgreement.no = validateData(item.asJsonObject, "spa_no")
                                saleAgreement.discount = validateData(item.asJsonObject, "discount")
                                saleAgreement.payment_tern_type = validateData(item.asJsonObject, "payment_term_type")
                                saleAgreement.purchase_date = validateData(item.asJsonObject, "purchase_date")
                                saleAgreement.status = validateData(item.asJsonObject, "status")
                                if (item.asJsonObject.has("account")){
                                    val unitJsonObject = item.asJsonObject.get("account").asJsonObject
                                    saleAgreement.projectName = validateData(unitJsonObject, "name")
                                }

                                if (item.asJsonObject.has("unit")){
                                    val unitJsonObject = item.asJsonObject.get("unit").asJsonObject
                                    saleAgreement.unitNo = validateData(unitJsonObject, "name")
                                }

                                if (item.asJsonObject.has("customer")){
                                    val unitJsonObject = item.asJsonObject.get("customer").asJsonObject
                                    saleAgreement.userName = validateData(unitJsonObject, "name")
                                }

                                subListDataRecord.add(saleAgreement)
                            }
                        }
                        callBackBooking.onSuccess(subListDataRecord)
                    } else {
                        callBackBooking.onFailed(resObj["msg"].asString)
                    }
                }
            }

            override fun onError(error: String, resCode: Int) {
                callBackBooking.onFailed(error)
            }
        }))
    }

    fun getDetailOpportunityWS(context: Context, id : String, callBackDetailOpportunity : OnCallBackDetailOpportunityListener){
        val serviceApi = RetrofitConnector.createService(context)
        val res = serviceApi.detailOpportunity(id)
        res.enqueue(CustomCallback(context, object : CustomResponseListener{
            override fun onSuccess(resObj: JsonObject?) {
                if (resObj != null){
                    if (resObj.has("data")){
                        val data = resObj["data"].asJsonObject
                        val detailOpportunity : DetailOpportunityModel = Gson().fromJson(data.asJsonObject, DetailOpportunityModel::class.java)
                        callBackDetailOpportunity.onSuccess(detailOpportunity)
                    } else {
                        callBackDetailOpportunity.onFailed(resObj["msg"].asString)
                    }
                }
            }

            override fun onError(error: String, resCode: Int) {
                callBackDetailOpportunity.onFailed(error)
            }

        }))
    }

    fun getQuotationDetailWS(context: Context, id :String, callBackQuotation : OnCallBackDetailQuotationListener){
        val serviceAPI = RetrofitConnector.createService(context)
        val res = serviceAPI.detailQuotation(id)
        res.enqueue(CustomCallback(context, object : CustomResponseListener{
            override fun onSuccess(resObj: JsonObject?) {
                if (resObj != null){
                    if (resObj.has("data")){
                        val data = resObj["data"].asJsonObject
                        val detailQuotation : DetailQuotationModel = Gson().fromJson(data.asJsonObject, DetailQuotationModel::class.java)
                        callBackQuotation.onSuccess(detailQuotation)
                    } else {
                        callBackQuotation.onFailed(resObj["msg"].asString)
                    }
                }
            }

            override fun onError(error: String, resCode: Int) {
                callBackQuotation.onFailed(error)
            }

        }))
    }

    fun getHoldAndReservedDetailWS(context: Context, id: String, callBackHoldWithReserved : OnCallBackDetailHoldAndReservedListener){
        val serviceAPI = RetrofitConnector.createService(context)
        val res = serviceAPI.detailHoldAndReserved(id)
        res.enqueue(CustomCallback(context, object : CustomResponseListener{
            override fun onSuccess(resObj: JsonObject?) {
                if (resObj != null){
                    if (resObj.has("data")){
                        val data = resObj["data"].asJsonObject
                        val detailQuotation : DetailHoldAndReservedModel = Gson().fromJson(data.asJsonObject, DetailHoldAndReservedModel::class.java)
                        callBackHoldWithReserved.onSuccess(detailQuotation)
                    } else {
                        callBackHoldWithReserved.onFailed(resObj["msg"].asString)
                    }
                }
            }

            override fun onError(error: String, resCode: Int) {
                callBackHoldWithReserved.onFailed(error)
            }

        }))
    }

    fun getBookingDetailWS(context: Context, id: String, callBackBooing : OnCallBackDetailHoldAndReservedListener){
        val serviceAPI = RetrofitConnector.createService(context)
        val res = serviceAPI.detailBookingDR(id)
        res.enqueue(CustomCallback(context, object : CustomResponseListener{
            override fun onSuccess(resObj: JsonObject?) {
                if (resObj != null){
                    if (resObj.has("data")){
                        val data = resObj["data"].asJsonObject
                        val detailQuotation : DetailHoldAndReservedModel = Gson().fromJson(data.asJsonObject, DetailHoldAndReservedModel::class.java)
                        callBackBooing.onSuccess(detailQuotation)
                    } else {
                        callBackBooing.onFailed(resObj["msg"].asString)
                    }
                }
            }

            override fun onError(error: String, resCode: Int) {
                callBackBooing.onFailed(error)
            }
        }))
    }

    fun getSaleAgreementDetailWS(context: Context, id: String, callBackBooing : OnCallBackDetailHoldAndReservedListener){
        val serviceAPI = RetrofitConnector.createService(context)
        val res = serviceAPI.detailSaleAgreementDR(id)
        res.enqueue(CustomCallback(context, object : CustomResponseListener{
            override fun onSuccess(resObj: JsonObject?) {
                if (resObj != null){
                    if (resObj.has("data")){
                        val data = resObj["data"].asJsonObject
                        val detailQuotation : DetailHoldAndReservedModel = Gson().fromJson(data.asJsonObject, DetailHoldAndReservedModel::class.java)
                        callBackBooing.onSuccess(detailQuotation)
                    } else {
                        callBackBooing.onFailed(resObj["msg"].asString)
                    }
                }
            }

            override fun onError(error: String, resCode: Int) {
                callBackBooing.onFailed(error)
            }
        }))
    }

    fun getDetailUnitInterestingAndRecommendedWS(context: Context, id: String , callBackItemMyUnit : OnClickBackDetailUnitListener){
        val serviceApi = RetrofitConnector.createService(context)
        val res =serviceApi.detailUnitInterestingAndRecommended(id)
        res.enqueue(CustomCallback(context, object : CustomResponseListener{
            override fun onSuccess(resObj: JsonObject?) {
                if (resObj != null){
                    if (resObj.has("data")){
                        val data = resObj["data"].asJsonObject
                        val unitDetail : PropertyMyUnitModel = Gson().fromJson(data.asJsonObject, PropertyMyUnitModel::class.java)
                        callBackItemMyUnit.onSuccess(unitDetail)
                    } else{
                        callBackItemMyUnit.onFailed(resObj["msg"].asString)
                    }
                }
            }
            override fun onError(error: String, resCode: Int) {
               callBackItemMyUnit.onFailed(error)
            }
        }))
    }

    // method for check validate data
    private fun validateData(jsonObject : JsonObject, key : String) : String? {
        if (!jsonObject.isJsonNull){
            if (jsonObject.has(key)) {
                if (!jsonObject[key].isJsonNull) {
                    return jsonObject[key].asString
                }
            }
        }
        return null
    }

    interface OnCallBackOpportunityListener{
        fun onSuccess( listOpportunityDR : ArrayList<SubListDataRecord>)
        fun onFailed(error: String)
    }

    interface OnCallBackDetailOpportunityListener{
        fun onSuccess(detailOpportunity : DetailOpportunityModel)
        fun onFailed(error: String)
    }

    interface OnCallBackDetailQuotationListener{
        fun onSuccess(detailOpportunity : DetailQuotationModel)
        fun onFailed(error: String)
    }

    interface OnCallBackDetailHoldAndReservedListener{
        fun onSuccess(detailOpportunity : DetailHoldAndReservedModel)
        fun onFailed(error: String)
    }

    interface OnClickBackDetailUnitListener{
        fun onSuccess(detailPropertyMyUnit : PropertyMyUnitModel)
        fun onFailed(error: String)
    }
}