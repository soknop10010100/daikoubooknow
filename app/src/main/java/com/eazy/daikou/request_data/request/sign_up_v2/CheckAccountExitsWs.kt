package com.eazy.daikou.request_data.request.sign_up_v2

import android.content.Context
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.RetrofitConnector
import com.eazy.daikou.repository_ws.CustomCallback
import com.eazy.daikou.repository_ws.CustomResponseListener
import com.eazy.daikou.helper.InternetConnection
import com.google.gson.JsonObject

class CheckAccountExitsWs {

    fun checkUserNameExist(context: Context, userName:String, callBack:OnResponseUserNameExist) {
        val api = RetrofitConnector.createService(context).checkAccountExist(userName)
        api.enqueue(CustomCallback(context, object : CustomResponseListener{
            override fun onSuccess(resObj: JsonObject?) {
                if ( resObj != null){
                    if (resObj.has("success") && resObj["success"].asBoolean) {
                        if (resObj.has("msg")){
                            callBack.onSuccessFull(resObj["msg"].asString)
                        }
                    } else {
                        callBack.onFailed(resObj["msg"].asString)
                    }
                }
            }

            override fun onError(error: String, resCode: Int) {
                if (!InternetConnection.checkInternetConnectionActivity(context)) {
                    callBack.onInternetConnection(context.getString(R.string.please_check_your_internet_connection))
                } else {
                    callBack.onFailed(error)
                }
            }
        })
        )
    }
    interface OnResponseUserNameExist{
        fun onSuccessFull(message : String)
        fun onFailed(msg : String)
        fun onInternetConnection( mess : String)
    }
}

