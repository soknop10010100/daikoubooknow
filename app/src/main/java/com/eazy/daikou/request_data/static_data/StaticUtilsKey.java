package com.eazy.daikou.request_data.static_data;

public class StaticUtilsKey {

    public static String action_key_scanner = "scanner_shortcut";
    public static String employee_type = "employee";

    public static String user_permission_key = "user_permission";
    public static String water_record_permission = "water";
    public static String electric_record_permission = "electric";
    public static String complaint_solutions_permission = "complaint";
    public static String frontdesk_permission = "front_desk";
    public static String emergency_permission = "emergency";
    public static String parking_permission = "parking";
    public static String cleaning_and_service_permission = "cleaning";
    public static String inspection_permission = "inspection";
    public static String work_order_permission = "work_order";
    public static String maintenance_permission = "maintenance";

    public static String sale_key = "sale";
    public static String lease_key = "lease";
    public static String operation_key = "operation";

    //====================== define key Inspection Maintenance ==================
    public static String inspection_type = "inspection_type";   // Step 1 Select Inspection Type
    public static String template_type = "template_type";   // Step 2 Select Template Type And Schedule
    public static String form_template_type = "form_template_type"; // Step 3 Create Inspection Form

    public static String detail_button_key = "detailed";
    public static String simplify_button_key = "simplified";
    public static String question_button_key = "question";
    public static String is_check_key = "";
    public static String noteId = "";


    public static String unit_no_action = "unit_no_action"; // Part Inspection
    public static String department_action = "department_action";   // Part Inspection
    public static String assignee_employee_action = "assignee_employee_action";   // Part Work Order
    public static String department_employee_action = "department_employee_action";   // Part Work Order


    public static String image_key = "image";
    public static String note_key = "note";
    public static String main_key = "main";
    public static String sub_key = "sub";
    public static String insert_key = "insert";
    public static String delete_key = "delete";
    public static boolean isActionSavedTemplate = true;
    public static String isActionMenuHome = "";

    //====================== define key update email or phone ==================
    public static String first_action = "first_action";
    public static String second_action = "second_action";
    public static String verify_code_action = "verify_code";
    public static boolean is_email_address = true;

    public static String work_order_action = "work_order";
    public static String inspection_action = "inspection_action";
    public static String maintenance_action = "maintenance_action";
    public static String pre_handover_action = "pre_handover_action";

    //====================== define work progress ==================
    public static String work_order_image = "work_order_image";
    public static String work_progress_image = "work_progress_image";

    public static Boolean is_check_click_category = true;

    public static String leave_category_action = "leave_category_action";
    public static String leave_approved_by_action = "leave_approved_action";
    public static String leave_period_action = "leave_period_action";

    //=================== define key for fee type facility ================
    public static String hourly = "hourly";
    public static String daily = "daily";
    public static String time_membership = "time_member";
    public static String schedule_membership = "schedule_member";
    public static String free = "free";
    public static String no_item_found = "no_item_found";

    public static String action_category_payment = "";
    public static String action_operation_payment_lease = "operation_payment_lease";

    public static String category = "hotel";    // Part hotel

    public static String operation_part = "cleaning"; //service cleaning employee
}
