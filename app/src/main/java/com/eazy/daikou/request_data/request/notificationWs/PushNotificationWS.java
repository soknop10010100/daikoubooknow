package com.eazy.daikou.request_data.request.notificationWs;

import android.content.Context;

import com.eazy.daikou.repository_ws.RetrofitConnector;
import com.eazy.daikou.repository_ws.CustomCallback;
import com.eazy.daikou.repository_ws.CustomResponseListener;
import com.eazy.daikou.repository_ws.ServiceApi;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.HashMap;

import retrofit2.Call;

public class PushNotificationWS {
    public void push_notification(Context context, HashMap<String, String> hashMapPass, PushNotificationCallBackListener notificationCallBackListener){
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = serviceApi.pushNotification(hashMapPass);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("success") && resObj.get("success").getAsString().equals("true")){
                    notificationCallBackListener.onSuccess(resObj.get("msg").getAsString());
                } else {
                    notificationCallBackListener.onFailed(resObj.get("msg").getAsString());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                notificationCallBackListener.onFailed(error);
            }
        }));
    }

    public interface PushNotificationCallBackListener{
        void onSuccess(String msgSuccess);
        void onFailed(String mess);
    }
}
