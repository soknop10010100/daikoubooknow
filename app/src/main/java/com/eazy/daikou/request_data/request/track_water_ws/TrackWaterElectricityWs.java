package com.eazy.daikou.request_data.request.track_water_ws;

import android.content.Context;

import com.eazy.daikou.repository_ws.RetrofitConnector;
import com.eazy.daikou.repository_ws.CustomCallback;
import com.eazy.daikou.repository_ws.CustomResponseListener;
import com.eazy.daikou.request_data.sql_database.TrackWaterElectricityDatabase;
import com.eazy.daikou.request_data.static_data.TypeTrackingWaterElectricity;
import com.eazy.daikou.helper.Utils;
import com.eazy.daikou.model.utillity_tracking.model.W_ETrackingModel;
import com.eazy.daikou.model.utillity_tracking.model.FloorUtilNoModel;
import com.eazy.daikou.model.utillity_tracking.model.LastTrackData;
import com.eazy.daikou.model.utillity_tracking.model.ListUsageW_EModel;
import com.eazy.daikou.model.utillity_tracking.model.TrackingWaterElectricityModel;
import com.eazy.daikou.model.utillity_tracking.model.UtilNoCommonAreaModel;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import retrofit2.Call;

public class TrackWaterElectricityWs {

    public void getAllFloorRecord(String propertyID, String tracking_date, String action, Context activity, OnTrackWaterCallBack callback) {
        Call<JsonElement> res = RetrofitConnector.createService(activity).getWaterElectricAllFloor(propertyID, tracking_date);
        res.enqueue(new CustomCallback(activity, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data")){

                    JsonObject data = resObj.get("data").getAsJsonObject();
                    List<FloorUtilNoModel> floorUtilNoModels = new ArrayList<>();
                    HashMap<String, FloorUtilNoModel> hashMap = new LinkedHashMap<>();
                    //ELECTRICITY DATA
                    JsonArray dataElectric = data.get("electricity").getAsJsonArray();
                    for (int i = 0 ; i < dataElectric.size(); i++){
                        FloorUtilNoModel utilNoModel = new FloorUtilNoModel();

                        JsonObject jsonObject = dataElectric.get(i).getAsJsonObject();
                        // get electricity util
                        List<UtilNoCommonAreaModel> utilNoList = new ArrayList<>();
                        for (int j = 0 ; j < jsonObject.get("unit").getAsJsonArray().size() ; j++){
                            UtilNoCommonAreaModel utilNoCommonAreaModel = new UtilNoCommonAreaModel();
                            String lastTrackDateElectricity = "", lastCurrentUsageElectricity = "", current_usage_Electricity = "";
                            if (jsonObject.get("unit").getAsJsonArray().get(j).getAsJsonObject().has("previous_usage") && !jsonObject.get("unit").getAsJsonArray().get(j).getAsJsonObject().get("previous_usage").isJsonNull()) {
                                JsonObject jsonElectricity = jsonObject.get("unit").getAsJsonArray().get(j).getAsJsonObject().get("previous_usage").getAsJsonObject();
                                if (!jsonElectricity.isJsonNull()) {
                                    if (!jsonElectricity.get("previous_tracking_date").isJsonNull()) {
                                        lastTrackDateElectricity = jsonElectricity.get("previous_tracking_date").getAsString();
                                    }
                                    if (!jsonElectricity.get("previous_usage").isJsonNull()) {
                                        lastCurrentUsageElectricity = jsonElectricity.get("previous_usage").getAsString();
                                    }
                                }
                            }

                            if (!jsonObject.get("unit").getAsJsonArray().get(j).getAsJsonObject().get("current_usage").isJsonNull()){
                                current_usage_Electricity = jsonObject.get("unit").getAsJsonArray().get(j).getAsJsonObject().get("current_usage").getAsString();
                            }

                            utilNoCommonAreaModel.setLastTrackDataElectricity(new LastTrackData(lastTrackDateElectricity, lastCurrentUsageElectricity,current_usage_Electricity,jsonObject.get("unit").getAsJsonArray().get(j).getAsJsonObject().get("have_previous_record").getAsBoolean(), tracking_date));
                            utilNoCommonAreaModel.setId(jsonObject.get("unit").getAsJsonArray().get(j).getAsJsonObject().get("unit_id").getAsString());
                            utilNoCommonAreaModel.setName(jsonObject.get("unit").getAsJsonArray().get(j).getAsJsonObject().get("unit_no").getAsString());
                            utilNoCommonAreaModel.setAlreadyHaveTrack(jsonObject.get("unit").getAsJsonArray().get(j).getAsJsonObject().get("have_previous_record").getAsBoolean());
                            utilNoCommonAreaModel.setTotal_recorded_amount_electric(jsonObject.get("total_recorded_amount_in_floor_of_unit").getAsString());
                            utilNoList.add(utilNoCommonAreaModel);
                        }

                        // get common electricity
                        List<UtilNoCommonAreaModel> electricityCommonAreaModelList = new ArrayList<>();
                        for (int l = 0 ; l < jsonObject.get("common_area").getAsJsonArray().size() ; l++) {
                            UtilNoCommonAreaModel utilNoCommonAreaModel = new UtilNoCommonAreaModel();
                            String lastTrackDateElectricity = "", lastCurrentUsageElectricity="", current_usage_Electrictity = "";
                            if (jsonObject.get("common_area").getAsJsonArray().get(l).getAsJsonObject().has("previous_usage") && !jsonObject.get("common_area").getAsJsonArray().get(l).getAsJsonObject().get("previous_usage").isJsonNull()) {
                                JsonObject jsonElectricity = jsonObject.get("common_area").getAsJsonArray().get(l).getAsJsonObject().get("previous_usage").getAsJsonObject();
                                if (!jsonElectricity.isJsonNull()) {
                                    if (!jsonElectricity.get("previous_tracking_date").isJsonNull()) {
                                        lastTrackDateElectricity = jsonElectricity.get("previous_tracking_date").getAsString();
                                    }
                                    if (!jsonElectricity.get("previous_usage").isJsonNull()) {
                                        lastCurrentUsageElectricity = jsonElectricity.get("previous_usage").getAsString();
                                    }
                                }
                            }

                            utilNoCommonAreaModel.setId(jsonObject.get("common_area").getAsJsonArray().get(l).getAsJsonObject().get("unit_id").getAsString());
                            if (!jsonObject.get("common_area").getAsJsonArray().get(l).getAsJsonObject().get("unit_no").isJsonNull()){
                                utilNoCommonAreaModel.setName(jsonObject.get("common_area").getAsJsonArray().get(l).getAsJsonObject().get("unit_no").getAsString());
                            } else {
                                utilNoCommonAreaModel.setName("No Name");
                            }
                            if (!jsonObject.get("common_area").getAsJsonArray().get(l).getAsJsonObject().get("current_usage").isJsonNull()){
                                current_usage_Electrictity = jsonObject.get("common_area").getAsJsonArray().get(l).getAsJsonObject().get("current_usage").getAsString();
                            }

                            utilNoCommonAreaModel.setLastTrackDataElectricity(new LastTrackData(lastTrackDateElectricity,lastCurrentUsageElectricity,current_usage_Electrictity ,jsonObject.get("common_area").getAsJsonArray().get(l).getAsJsonObject().get("have_previous_record").getAsBoolean(), tracking_date));
                            utilNoCommonAreaModel.setAlreadyHaveTrack(jsonObject.get("common_area").getAsJsonArray().get(l).getAsJsonObject().get("have_previous_record").getAsBoolean());
                            utilNoCommonAreaModel.setTotal_recorded_amount_electric(jsonObject.get("total_recorded_amount_in_floor_of_common_area").getAsString());
                            electricityCommonAreaModelList.add(utilNoCommonAreaModel);

                        }

                        utilNoModel.setFloorName(jsonObject.get("floor_no").getAsString());
                        utilNoModel.setFloorNoName(jsonObject.get("floor_name").getAsString());
                        utilNoModel.setUtilNoModelList(utilNoList);
                        utilNoModel.setElectricityCommonList(electricityCommonAreaModelList);
                        hashMap.put(jsonObject.get("floor_name").getAsString(),utilNoModel);

                        // get electricity EDC
                        List<UtilNoCommonAreaModel> EDCList = new ArrayList<>();
                        FloorUtilNoModel utilNoModelEDC = new FloorUtilNoModel();
                        for (int j = 0 ; j < jsonObject.get("edc").getAsJsonArray().size() ; j++){
                            UtilNoCommonAreaModel utilNoCommonAreaModel = new UtilNoCommonAreaModel();
                            utilNoCommonAreaModel.setId(String.valueOf(jsonObject.get("edc").getAsJsonArray().get(j).getAsJsonObject().get("unit_id").getAsString()));
                            utilNoCommonAreaModel.setName(String.valueOf(jsonObject.get("edc").getAsJsonArray().get(j).getAsJsonObject().get("unit_no").getAsString()));

                            String lastTrackDateElectricity = "", lastCurrentUsageElectricity = "", current_usage_Electricity = "";
                            if (jsonObject.get("edc").getAsJsonArray().get(j).getAsJsonObject().has("previous_usage") && !jsonObject.get("edc").getAsJsonArray().get(j).getAsJsonObject().get("previous_usage").isJsonNull()) {
                                JsonObject jsonElectricity = jsonObject.get("edc").getAsJsonArray().get(j).getAsJsonObject().get("previous_usage").getAsJsonObject();
                                if (!jsonElectricity.isJsonNull()) {
                                    if (!jsonElectricity.get("previous_tracking_date").isJsonNull()) {
                                        lastTrackDateElectricity = jsonElectricity.get("previous_tracking_date").getAsString();
                                    }
                                    if (!jsonElectricity.get("previous_usage").isJsonNull()) {
                                        lastCurrentUsageElectricity = jsonElectricity.get("previous_usage").getAsString();
                                    }
                                }
                            }
                            if (!jsonObject.get("edc").getAsJsonArray().get(j).getAsJsonObject().get("current_usage").isJsonNull()){
                                current_usage_Electricity = jsonObject.get("edc").getAsJsonArray().get(j).getAsJsonObject().get("current_usage").getAsString();
                            }

                            utilNoCommonAreaModel.setTotal_recorded_amount_electric(jsonObject.get("total_recorded_amount_in_floor_of_edc").getAsString());
                            utilNoCommonAreaModel.setLastTrackDataElectricity(new LastTrackData(lastTrackDateElectricity, lastCurrentUsageElectricity, current_usage_Electricity,jsonObject.get("edc").getAsJsonArray().get(j).getAsJsonObject().get("have_previous_record").getAsBoolean(), tracking_date));
                            utilNoCommonAreaModel.setAlreadyHaveTrack(jsonObject.get("edc").getAsJsonArray().get(j).getAsJsonObject().get("have_previous_record").getAsBoolean());
                            EDCList.add(utilNoCommonAreaModel);
                        }

                        utilNoModelEDC.setFloorName(TypeTrackingWaterElectricity.EDC_FLOOR); // if floor -1 is EDC
                        utilNoModelEDC.setElectricityCommonList(EDCList);
                        if (i == 0){
                            hashMap.put(TypeTrackingWaterElectricity.EDC_FLOOR, utilNoModelEDC);
                        } else {
                            FloorUtilNoModel utilNoModel1 = hashMap.get(TypeTrackingWaterElectricity.EDC_FLOOR);
                            assert utilNoModel1 != null;
                            utilNoModel1.getElectricityCommonList().addAll(EDCList);
                            hashMap.put(TypeTrackingWaterElectricity.EDC_FLOOR,utilNoModel1);
                        }

                    }

                   // WATER DATA
                    JsonArray dataWater = data.get("water").getAsJsonArray();
                    for (int i = 0 ; i < dataWater.size(); i++){
                        FloorUtilNoModel utilNoModel = new FloorUtilNoModel();
                        JsonObject jsonObject = dataWater.get(i).getAsJsonObject();

                        List<UtilNoCommonAreaModel> utilNoList = new ArrayList<>();
                        for (int j = 0 ; j < jsonObject.get("unit").getAsJsonArray().size() ; j++){
                            UtilNoCommonAreaModel utilNoCommonAreaModel = new UtilNoCommonAreaModel();

                            // get water util
                            String lastTrackDateWater = "", lastCurrentUsageWater = "", current_usage_Water = "";
                            if (jsonObject.get("unit").getAsJsonArray().get(j).getAsJsonObject().has("previous_usage") && !jsonObject.get("unit").getAsJsonArray().get(j).getAsJsonObject().get("previous_usage").isJsonNull()) {
                                JsonObject jsonWater = jsonObject.get("unit").getAsJsonArray().get(j).getAsJsonObject().get("previous_usage").getAsJsonObject();
                                if (!jsonWater.isJsonNull()) {
                                    if (!jsonWater.get("previous_tracking_date").isJsonNull()) {
                                        lastTrackDateWater = jsonWater.get("previous_tracking_date").getAsString();
                                    }
                                    if (!jsonWater.get("previous_usage").isJsonNull()) {
                                        lastCurrentUsageWater = jsonWater.get("previous_usage").getAsString();
                                    }
                                }
                            }
                            if (jsonObject.get("unit").getAsJsonArray().get(j).getAsJsonObject().has("current_usage") && !jsonObject.get("unit").getAsJsonArray().get(j).getAsJsonObject().get("current_usage").isJsonNull()){
                                current_usage_Water = jsonObject.get("unit").getAsJsonArray().get(j).getAsJsonObject().get("current_usage").getAsString();
                            }
                            utilNoCommonAreaModel.setTotal_recorded_amount_water(jsonObject.get("total_recorded_amount_in_floor_of_unit").getAsString());
                            utilNoCommonAreaModel.setLastTrackDataWater(new LastTrackData(lastTrackDateWater,lastCurrentUsageWater,current_usage_Water ,jsonObject.get("unit").getAsJsonArray().get(j).getAsJsonObject().get("have_previous_record").getAsBoolean(), tracking_date));
                            utilNoCommonAreaModel.setId(jsonObject.get("unit").getAsJsonArray().get(j).getAsJsonObject().get("unit_id").getAsString());
                            utilNoCommonAreaModel.setName(jsonObject.get("unit").getAsJsonArray().get(j).getAsJsonObject().get("unit_no").getAsString());
                            utilNoCommonAreaModel.setAlreadyHaveTrack(jsonObject.get("unit").getAsJsonArray().get(j).getAsJsonObject().get("have_previous_record").getAsBoolean());
                            utilNoList.add(utilNoCommonAreaModel);
                        }

                        List<UtilNoCommonAreaModel> waterCommonAreaModelList = new ArrayList<>();
                        for (int k = 0 ; k < jsonObject.get("common_area").getAsJsonArray().size() ; k++){
                            UtilNoCommonAreaModel utilNoCommonAreaModel = new UtilNoCommonAreaModel();

                            // get common water
                            String lastTrackDateWater = "", lastCurrentUsageWater="", current_usage_Water = "";
                            if (jsonObject.get("common_area").getAsJsonArray().get(k).getAsJsonObject().has("previous_usage") && !jsonObject.get("common_area").getAsJsonArray().get(k).getAsJsonObject().get("previous_usage").isJsonNull()) {
                                JsonObject jsonWater = jsonObject.get("common_area").getAsJsonArray().get(k).getAsJsonObject().get("previous_usage").getAsJsonObject();
                                if (!jsonWater.isJsonNull()) {
                                    if (!jsonWater.get("previous_tracking_date").isJsonNull()) {
                                        lastTrackDateWater = jsonWater.get("previous_tracking_date").getAsString();
                                    }
                                    if (!jsonWater.get("previous_usage").isJsonNull()) {
                                        lastCurrentUsageWater = jsonWater.get("previous_usage").getAsString();
                                    }
                                }
                            }
                            if (!jsonObject.get("common_area").getAsJsonArray().get(k).getAsJsonObject().get("current_usage").isJsonNull()){
                                current_usage_Water = jsonObject.get("common_area").getAsJsonArray().get(k).getAsJsonObject().get("current_usage").getAsString();
                            }

                            utilNoCommonAreaModel.setTotal_recorded_amount_water(jsonObject.get("total_recorded_amount_in_floor_of_common_area").getAsString());
                            utilNoCommonAreaModel.setId(jsonObject.get("common_area").getAsJsonArray().get(k).getAsJsonObject().get("unit_id").getAsString());
                            utilNoCommonAreaModel.setName(jsonObject.get("common_area").getAsJsonArray().get(k).getAsJsonObject().get("unit_no").getAsString());
                            utilNoCommonAreaModel.setLastTrackDataWater(new LastTrackData(lastTrackDateWater,lastCurrentUsageWater,current_usage_Water ,jsonObject.get("common_area").getAsJsonArray().get(k).getAsJsonObject().get("have_previous_record").getAsBoolean(), tracking_date));
                            utilNoCommonAreaModel.setAlreadyHaveTrack(jsonObject.get("common_area").getAsJsonArray().get(k).getAsJsonObject().get("have_previous_record").getAsBoolean());
                            waterCommonAreaModelList.add(utilNoCommonAreaModel);

                        }

                        FloorUtilNoModel utilNoModel1 = hashMap.get(jsonObject.get("floor_name").getAsString());

                        List<UtilNoCommonAreaModel> utilNoCommonAreaModels = new ArrayList<>();

                        for (UtilNoCommonAreaModel utilNoCommonAreaModel : utilNoModel1.getUtilNoModelList()){
                            for (UtilNoCommonAreaModel utilNoCommonAreaModel1 : utilNoList){
                                if (utilNoCommonAreaModel.getId().equals(utilNoCommonAreaModel1.getId())){
                                    UtilNoCommonAreaModel newData = new UtilNoCommonAreaModel();
                                    newData.setId(utilNoCommonAreaModel.getId());
                                    newData.setName(utilNoCommonAreaModel.getName());
                                    newData.setTotal_recorded_amount_electric(utilNoCommonAreaModel.getTotal_recorded_amount_electric());
                                    newData.setLastTrackDataElectricity(utilNoCommonAreaModel.getLastTrackDataElectricity());
                                    newData.setTotal_recorded_amount_water(utilNoCommonAreaModel1.getTotal_recorded_amount_water());
                                    newData.setLastTrackDataWater(utilNoCommonAreaModel1.getLastTrackDataWater());
                                    utilNoCommonAreaModels.add(newData);
                                    break;
                                }
                            }
                        }

                        utilNoModel1.setUtilNoModelList(utilNoCommonAreaModels);
                        utilNoModel1.setWaterCommonList(waterCommonAreaModelList);

                        hashMap.put(jsonObject.get("floor_name").getAsString(),utilNoModel1);

//                        get PPWSA
                        FloorUtilNoModel utilNoModelPPWSA = new FloorUtilNoModel();
                        List<UtilNoCommonAreaModel> PPSWAList = new ArrayList<>();
                        for (int j = 0 ; j < jsonObject.get("ppwsa").getAsJsonArray().size() ; j++){
                            UtilNoCommonAreaModel utilNoCommonAreaModel = new UtilNoCommonAreaModel();
                            utilNoCommonAreaModel.setId(String.valueOf(jsonObject.get("ppwsa").getAsJsonArray().get(j).getAsJsonObject().get("unit_id").getAsString()));
                            utilNoCommonAreaModel.setName(String.valueOf(jsonObject.get("ppwsa").getAsJsonArray().get(j).getAsJsonObject().get("unit_no").getAsString()));
                            String lastTrackDateWater = "", lastCurrentUsageWater = "", current_usage_Water = "";
                            if (jsonObject.get("ppwsa").getAsJsonArray().get(j).getAsJsonObject().has("previous_usage") && !jsonObject.get("ppwsa").getAsJsonArray().get(j).getAsJsonObject().get("previous_usage").isJsonNull()) {
                                JsonObject jsonWater = jsonObject.get("ppwsa").getAsJsonArray().get(j).getAsJsonObject().get("previous_usage").getAsJsonObject();
                                if (!jsonWater.isJsonNull()) {
                                    if (!jsonWater.get("previous_tracking_date").isJsonNull()) {
                                        lastTrackDateWater = jsonWater.get("previous_tracking_date").getAsString();
                                    }
                                    if (!jsonWater.get("previous_usage").isJsonNull()) {
                                        lastCurrentUsageWater = jsonWater.get("previous_usage").getAsString();
                                    }
                                }
                            }
                            if (!jsonObject.get("ppwsa").getAsJsonArray().get(j).getAsJsonObject().get("current_usage").isJsonNull()){
                                current_usage_Water = jsonObject.get("ppwsa").getAsJsonArray().get(j).getAsJsonObject().get("current_usage").getAsString();
                            }

                            utilNoCommonAreaModel.setTotal_recorded_amount_water(jsonObject.get("total_recorded_amount_in_floor_of_ppwsa").getAsString());
                            utilNoCommonAreaModel.setLastTrackDataWater(new LastTrackData(lastTrackDateWater,lastCurrentUsageWater,current_usage_Water ,jsonObject.get("ppwsa").getAsJsonArray().get(j).getAsJsonObject().get("have_previous_record").getAsBoolean(), tracking_date));
                            utilNoCommonAreaModel.setAlreadyHaveTrack(jsonObject.get("ppwsa").getAsJsonArray().get(j).getAsJsonObject().get("have_previous_record").getAsBoolean());
                            PPSWAList.add(utilNoCommonAreaModel);
                        }
                        utilNoModelPPWSA.setFloorName(TypeTrackingWaterElectricity.PPWSA_FLOOR); // if floor -1 is PPWSA
                        utilNoModelPPWSA.setWaterCommonList(PPSWAList);

                        if (i == 0){
                            hashMap.put(TypeTrackingWaterElectricity.PPWSA_FLOOR, utilNoModelPPWSA);
                        } else {
                            FloorUtilNoModel utilNoModel2 = hashMap.get(TypeTrackingWaterElectricity.PPWSA_FLOOR);
                            utilNoModel2.getWaterCommonList().addAll(PPSWAList);
                            hashMap.put(TypeTrackingWaterElectricity.PPWSA_FLOOR,utilNoModel2);
                        }
                    }

                    TrackWaterElectricityDatabase database = new TrackWaterElectricityDatabase(activity);

                    for (Map.Entry map : hashMap.entrySet()) {
                        if (!map.getKey().equals(TypeTrackingWaterElectricity.EDC_FLOOR) && !map.getKey().equals(TypeTrackingWaterElectricity.PPWSA_FLOOR) ){
                            floorUtilNoModels.add(hashMap.get(map.getKey()));
                        }
                    }

                    floorUtilNoModels.add(hashMap.get(TypeTrackingWaterElectricity.EDC_FLOOR));
                    floorUtilNoModels.add(hashMap.get(TypeTrackingWaterElectricity.PPWSA_FLOOR));

                    String storeDate = new Gson().toJson(floorUtilNoModels);
                    if (action.equals("create")){
                        database.insertFloor(storeDate);
                    } else {
                        database.updateFloor(storeDate);
                    }

                    callback.onSuccess("success",floorUtilNoModels);
                } else {
                    callback.onFail(resObj.get("msg").getAsString());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                callback.onFail(error);
            }
        }));
    }

    public void getAllFloorAndUnit(String propertyID, String tracking_date, String action_type, Context activity, OnTrackWaterCallBack callback) {
        Call<JsonElement> res = RetrofitConnector.createService(activity).getAllFloorAndUnit(propertyID, tracking_date, TypeTrackingWaterElectricity.getTypeOfUtility().get(action_type), TypeTrackingWaterElectricity.getUtilityType().get(action_type));
        res.enqueue(new CustomCallback(activity, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data")){
                    JsonArray jsonArray = resObj.get("data").getAsJsonArray();

                    List<FloorUtilNoModel> floorUtilNoModels = new ArrayList<>();

                    if (action_type.equalsIgnoreCase("water_ppwsa") || action_type.equalsIgnoreCase("electricity_edc")) {
                        FloorUtilNoModel utilNoModel = new FloorUtilNoModel();
                        List<UtilNoCommonAreaModel> waterPpwsaEDCList = new ArrayList<>();
                        for (int i = 0; i < jsonArray.size(); i++) {
                            JsonObject jsonObject = jsonArray.get(i).getAsJsonObject();
                            UtilNoCommonAreaModel utilNoCommonAreaModel = new UtilNoCommonAreaModel();
                            utilNoCommonAreaModel.setId(jsonObject.get("floor_no").getAsString());
                            utilNoCommonAreaModel.setName(jsonObject.get("floor_name").getAsString());
                            if (action_type.equalsIgnoreCase("water_ppwsa")){
                                utilNoCommonAreaModel.setTotal_recorded_amount_water(jsonObject.get("total_recorded_amount").getAsString());
                                utilNoCommonAreaModel.setLastTrackDataWater(new LastTrackData("", "", "",jsonObject.get("have_previous_record").getAsBoolean(), ""));
                            } else {
                                utilNoCommonAreaModel.setTotal_recorded_amount_electric(jsonObject.get("total_recorded_amount").getAsString());
                                utilNoCommonAreaModel.setLastTrackDataElectricity(new LastTrackData("", "", "",jsonObject.get("have_previous_record").getAsBoolean(), ""));
                            }
                            utilNoCommonAreaModel.setLastTrackDataWater(new LastTrackData("", "", "",jsonObject.get("have_previous_record").getAsBoolean(), ""));
                            utilNoCommonAreaModel.setAlreadyHaveTrack(jsonObject.get("have_previous_record").getAsBoolean());
                            waterPpwsaEDCList.add(utilNoCommonAreaModel);
                        }

                        if (action_type.equalsIgnoreCase("water_ppwsa")){
                            utilNoModel.setWaterCommonList(waterPpwsaEDCList);
                            utilNoModel.setFloorName(TypeTrackingWaterElectricity.PPWSA_FLOOR);
                        } else {
                            utilNoModel.setElectricityCommonList(waterPpwsaEDCList);
                            utilNoModel.setFloorName(TypeTrackingWaterElectricity.EDC_FLOOR);
                        }

                        floorUtilNoModels.add(utilNoModel);
                    } else {
                        for (int i = 0 ; i < jsonArray.size() ; i++){
                            FloorUtilNoModel utilNoModel = new FloorUtilNoModel();
                            JsonObject jsonObject = jsonArray.get(i).getAsJsonObject();
                            List<UtilNoCommonAreaModel> utilNoList = new ArrayList<>();
                            if (jsonObject.has("unit") && !jsonObject.get("unit").isJsonNull()){
                                for (int j = 0 ; j < jsonObject.get("unit").getAsJsonArray().size() ; j++){
                                    UtilNoCommonAreaModel utilNoCommonAreaModel = new UtilNoCommonAreaModel();
                                    switch (action_type) {
                                        case "water_track":
                                            utilNoCommonAreaModel.setTotal_recorded_amount_water(jsonObject.get("total_recorded_amount").getAsString());
                                            utilNoCommonAreaModel.setId(jsonObject.get("unit").getAsJsonArray().get(j).getAsJsonObject().get("unit_id").getAsString());
                                            utilNoCommonAreaModel.setName(jsonObject.get("unit").getAsJsonArray().get(j).getAsJsonObject().get("unit_no").getAsString());
                                            utilNoCommonAreaModel.setLastTrackDataWater(new LastTrackData("", "", "",jsonObject.get("unit").getAsJsonArray().get(j).getAsJsonObject().get("have_previous_record").getAsBoolean(), ""));
                                            utilNoCommonAreaModel.setAlreadyHaveTrack(jsonObject.get("unit").getAsJsonArray().get(j).getAsJsonObject().get("have_previous_record").getAsBoolean());
                                            utilNoList.add(utilNoCommonAreaModel);
                                            utilNoModel.setUtilNoModelList(utilNoList);
                                            break;
                                        case "electricity_track":
                                            utilNoCommonAreaModel.setTotal_recorded_amount_electric(jsonObject.get("total_recorded_amount").getAsString());
                                            utilNoCommonAreaModel.setId(jsonObject.get("unit").getAsJsonArray().get(j).getAsJsonObject().get("unit_id").getAsString());
                                            utilNoCommonAreaModel.setName(jsonObject.get("unit").getAsJsonArray().get(j).getAsJsonObject().get("unit_no").getAsString());
                                            utilNoCommonAreaModel.setLastTrackDataElectricity(new LastTrackData("", "", "",jsonObject.get("unit").getAsJsonArray().get(j).getAsJsonObject().get("have_previous_record").getAsBoolean(), ""));
                                            utilNoCommonAreaModel.setAlreadyHaveTrack(jsonObject.get("unit").getAsJsonArray().get(j).getAsJsonObject().get("have_previous_record").getAsBoolean());
                                            utilNoModel.setUtilNoModelList(utilNoList);
                                            utilNoList.add(utilNoCommonAreaModel);
                                            break;
                                        case "electricity_common":
                                            utilNoCommonAreaModel.setTotal_recorded_amount_electric(jsonObject.get("total_recorded_amount").getAsString());
                                            utilNoCommonAreaModel.setId(jsonObject.get("unit").getAsJsonArray().get(j).getAsJsonObject().get("unit_id").getAsString());
                                            utilNoCommonAreaModel.setName(jsonObject.get("unit").getAsJsonArray().get(j).getAsJsonObject().get("unit_no").getAsString());
                                            utilNoCommonAreaModel.setLastTrackDataElectricity(new LastTrackData("", "","", jsonObject.get("unit").getAsJsonArray().get(j).getAsJsonObject().get("have_previous_record").getAsBoolean(), ""));
                                            utilNoCommonAreaModel.setAlreadyHaveTrack(jsonObject.get("unit").getAsJsonArray().get(j).getAsJsonObject().get("have_previous_record").getAsBoolean());
                                            utilNoModel.setElectricityCommonList(utilNoList);
                                            utilNoList.add(utilNoCommonAreaModel);
                                            break;
                                        case "water_common":
                                            utilNoCommonAreaModel.setTotal_recorded_amount_water(jsonObject.get("total_recorded_amount").getAsString());
                                            utilNoCommonAreaModel.setId(jsonObject.get("unit").getAsJsonArray().get(j).getAsJsonObject().get("unit_id").getAsString());
                                            utilNoCommonAreaModel.setName(jsonObject.get("unit").getAsJsonArray().get(j).getAsJsonObject().get("unit_no").getAsString());
                                            utilNoCommonAreaModel.setLastTrackDataWater(new LastTrackData("","", "", jsonObject.get("unit").getAsJsonArray().get(j).getAsJsonObject().get("have_previous_record").getAsBoolean(), ""));
                                            utilNoCommonAreaModel.setAlreadyHaveTrack(jsonObject.get("unit").getAsJsonArray().get(j).getAsJsonObject().get("have_previous_record").getAsBoolean());
                                            utilNoModel.setWaterCommonList(utilNoList);
                                            utilNoList.add(utilNoCommonAreaModel);
                                            break;
                                    }
                                }

                                switch (action_type) {
                                    case "water_track":
                                    case "electricity_track":
                                        utilNoModel.setUtilNoModelList(utilNoList);
                                        break;
                                    case "electricity_common":
                                        utilNoModel.setElectricityCommonList(utilNoList);
                                        break;
                                    case "water_common":
                                        utilNoModel.setWaterCommonList(utilNoList);
                                        break;
                                }

                                utilNoModel.setFloorName(jsonObject.get("floor_no").getAsString());
                                utilNoModel.setFloorNoName(jsonObject.get("floor_name").getAsString());
                                floorUtilNoModels.add(utilNoModel);
                            }
                        }
                    }

                    callback.onSuccess("success", floorUtilNoModels);

                } else {callback.onFail(resObj.get("msg").getAsString());}
            }

            @Override
            public void onError(String error, int resCode) {
                callback.onFail(error);
            }
        }));
    }

    public void getAllListTracking(int page, int limit, String propertyID, Context activity, String action, OnRecordTrackWaterCallBack callback) {
        Utils.logDebug("jteiheueeheheeeje", TypeTrackingWaterElectricity.getTypeOfUtility().get(action)+ "     " + TypeTrackingWaterElectricity.getUtilityType().get(action));
        Call<JsonElement> res = RetrofitConnector.createService(activity).getWaterElectricAllRecords(page, limit, propertyID, TypeTrackingWaterElectricity.getTypeOfUtility().get(action), TypeTrackingWaterElectricity.getUtilityType().get(action));
        res.enqueue(new CustomCallback(activity, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data")) {
                    List<W_ETrackingModel> listSub = new ArrayList<>();
                    JsonArray data = resObj.get("data").getAsJsonArray();
                    for (int i = 0; i < data.size(); i++) {
                        JsonObject jsonObject = data.get(i).getAsJsonObject();
                        W_ETrackingModel w_eTrackingModel = new Gson().fromJson(jsonObject, W_ETrackingModel.class);
                        listSub.add(w_eTrackingModel);
                    }
                    callback.onSuccessSubApi("success", listSub);
                } else {
                    callback.onFail(resObj.get("msg").getAsString());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                callback.onFail(error);
            }
        }));
    }

    public void createWaterElectricUtility(Context context, HashMap<String,Object> hashMap, OnShowingMessage onShowingMessage ){
        String id = "";
        if (hashMap.containsKey("id")){
            id = Objects.requireNonNull(hashMap.get("id")).toString();
            hashMap.remove("id");
        }
        Call<JsonElement> res = RetrofitConnector.createService(context).saveUnitWaterElectric(hashMap);
        String finalId = id;
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("success") && resObj.get("success").getAsString().equals("true")){
                    if (resObj.has("msg")) {
                        onShowingMessage.onSuccess(resObj.get("msg").getAsString(), finalId);
                    }
                } else {
                    if (resObj.has("msg")) {
                     onShowingMessage.onFail(resObj.get("msg").getAsString());
                    }
                }
            }

            @Override
            public void onError(String error, int resCode) {
                onShowingMessage.onFail(error);
            }
        }));
    }

    public void getUtilityRecordDetail(Context activity, String propertyId, String tracking_date, String action, String unit_id, OnCallBackUtility onCallBackUtility){
        Call<JsonElement> res = RetrofitConnector.createService(activity).getInfoByUnit(propertyId, tracking_date, TypeTrackingWaterElectricity.getTypeOfUtility().get(action), TypeTrackingWaterElectricity.getUtilityType().get(action), unit_id);
        res.enqueue(new CustomCallback(activity, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (!resObj.get("data").isJsonNull()){
                    TrackingWaterElectricityModel trackingWaterElectricityModel = new Gson().fromJson(resObj.get("data").getAsJsonObject(), TrackingWaterElectricityModel.class);
                    onCallBackUtility.onSuccess(trackingWaterElectricityModel);
                } else {
                    onCallBackUtility.onFail("false");
                }
            }

            @Override
            public void onError(String error, int resCode) {
                onCallBackUtility.onFail(error);
            }
        }));
    }

    public void getWaterElectricOwnerUsage(Context activity, String type, String property_id, String unit_no, String month, String year, OnCallBackUsageElectricWater onCallBackUsageElectricWater){
        Call<JsonElement> res;
        if (type.equalsIgnoreCase("water")){
            res = RetrofitConnector.createService(activity).getWaterOwnerUsage(property_id, unit_no, month, year);
        } else {
            res = RetrofitConnector.createService(activity).getElectricOwnerUsage(property_id, unit_no, month, year);
        }
        res.enqueue(new CustomCallback(activity, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data")){
                    if (!resObj.get("data").isJsonNull()) {
                        JsonElement item = resObj.get("data").getAsJsonObject();
                        ListUsageW_EModel listUsageW_eModel = new Gson().fromJson(item, ListUsageW_EModel.class);
                        onCallBackUsageElectricWater.onSuccess(listUsageW_eModel);
                    }
                } else {
                    onCallBackUsageElectricWater.onFail(resObj.get("msg").getAsString());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                onCallBackUsageElectricWater.onFail(error);
            }
        }));
    }

    public interface OnTrackWaterCallBack{
        void onSuccess(String message, List<FloorUtilNoModel> list);
        void onFail(String message);
    }

    public interface OnRecordTrackWaterCallBack{
        void onSuccessSubApi(String message, List<W_ETrackingModel> list);
        void onFail(String message);
    }

    public interface OnShowingMessage{
        void onSuccess(String message , String dataId);
        void onFail(String message);
    }

    public interface OnCallBackUtility{
        void onSuccess(TrackingWaterElectricityModel trackingWaterElectricityModel);
        void onFail(String message);
    }

    public interface OnCallBackUsageElectricWater{
        void onSuccess(ListUsageW_EModel listUsageW_eModels);
        void onFail(String message);
    }
}
