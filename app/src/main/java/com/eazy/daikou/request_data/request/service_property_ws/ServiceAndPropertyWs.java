package com.eazy.daikou.request_data.request.service_property_ws;

import android.content.Context;

import com.eazy.daikou.repository_ws.RetrofitConnector;
import com.eazy.daikou.repository_ws.CustomCallback;
import com.eazy.daikou.repository_ws.CustomResponseListener;
import com.eazy.daikou.repository_ws.ServiceApi;
import com.eazy.daikou.model.my_property.service_property_employee.ServicePropertyIn;
import com.eazy.daikou.model.my_property.service_provider.ServicePropertyModel;
import com.eazy.daikou.model.my_property.service_provider.ServiceTypeModel;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;

import retrofit2.Call;

public class ServiceAndPropertyWs {
    public void getListServiceCategory(Context context, String propertyId, String serviceType, OnCallBackServiceInListener callBackServiceIn){
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = serviceApi.getListCategoryServiceIn(propertyId, serviceType);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if(resObj.has("success")){
                    if(resObj.has("data")){
                        ArrayList<ServiceTypeModel> serviceTypeModelList = new ArrayList<>();
                        JsonArray data = resObj.get("data").getAsJsonArray();
                        for (int i = 0; i < data.size(); i++) {
                            JsonObject jsonObject = data.get(i).getAsJsonObject();
                            ServiceTypeModel serviceTypeModel = new Gson().fromJson(jsonObject.toString(), ServiceTypeModel.class);
                            serviceTypeModelList.add(serviceTypeModel);
                        }
                        callBackServiceIn.onLoadSuccess(serviceTypeModelList);
                    }else {
                        callBackServiceIn.onLoadFail(resObj.get("msg").getAsString());
                    }
                }
            }

            @Override
            public void onError(String error, int resCode) {
                callBackServiceIn.onLoadFail(error);
            }
        }));
    }

    public void getListServiceProperty(Context context,String propertyId, String serviceType, String categoryId,int page, OnCallBackServiceInListener callBackServiceIn){
        ArrayList<ServicePropertyIn> listService = new ArrayList<>();
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = serviceApi.getListServiceIn(page,10,propertyId, serviceType, categoryId);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if(resObj.has("success")){
                    boolean isSuccess = resObj.get("success").getAsBoolean();
                    if(isSuccess){
                        if(resObj.has("data")){
                            JsonArray jsonArray = resObj.get("data").getAsJsonArray();
                            for (int i = 0; i <jsonArray.size() ; i++) {
                                JsonObject item = jsonArray.get(i).getAsJsonObject();
                                ServicePropertyIn parking = new Gson().fromJson(item.toString(),ServicePropertyIn.class);
                                listService.add(parking);
                            }
                            callBackServiceIn.onLoadSuccessFull(listService);
                        }
                    }else {
                        callBackServiceIn.onLoadFail(resObj.get("msg").getAsString());
                    }
                }
            }

            @Override
            public void onError(String error, int resCode) {
                callBackServiceIn.onLoadFail(error);
            }
        }));
    }

    public void getListOutDoorServiceProperty(Context context,String propertyId, String serviceType, String categoryId,int page, OnCallBackServiceInListener callBackServiceIn){
        ArrayList<ServicePropertyModel> serviceModels = new ArrayList<>();
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = serviceApi.getListServiceIn(page,10,propertyId, serviceType, categoryId);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data")){
                    JsonArray data = resObj.get("data").getAsJsonArray();
                    for (int i = 0; i <data.size() ; i++) {
                        JsonObject item = data.get(i).getAsJsonObject();
                        ServicePropertyModel serviceModel = new Gson().fromJson(item.toString(), ServicePropertyModel.class);
                        serviceModels.add(serviceModel);
                    }
                    callBackServiceIn.onLoadGetServiceOutDoor(serviceModels);
                } else {
                    callBackServiceIn.onLoadFail(resObj.get("msg").getAsString());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                callBackServiceIn.onLoadFail(error);
            }
        }));
    }

    public void getDetailServiceProperty(Context context,String serviceType,String serviceId,OnCallBackServiceIn callBackServiceIn){
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = serviceApi.getListServiceInDetail(serviceId,serviceType);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if(resObj.has("success")){
                    boolean isSuccess = resObj.get("success").getAsBoolean();
                    if(isSuccess){
                        if(resObj.has("data")){
                            JsonElement data = resObj.get("data");
                            ServicePropertyIn service = new Gson().fromJson(data, ServicePropertyIn.class);
                            callBackServiceIn.onLoadDetailSuccessFull(service);
                        }
                    }else {
                        callBackServiceIn.onLoadFail(resObj.get("msg").getAsString());
                    }
                }
            }

            @Override
            public void onError(String error, int resCode) {
                callBackServiceIn.onLoadFail(error);
            }
        }));
    }

    public interface OnCallBackServiceIn{
        void onLoadDetailSuccessFull( ServicePropertyIn servicePropertyIn);
        void onLoadFail(String message);
    }

    public interface OnCallBackServiceInListener{
        void onLoadSuccessFull(ArrayList<ServicePropertyIn> listService);
        void onLoadGetServiceOutDoor(ArrayList<ServicePropertyModel> serviceModels);
        void onLoadSuccess(ArrayList<ServiceTypeModel> listService);
        void onLoadFail(String message);
    }

}
