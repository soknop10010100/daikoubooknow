package com.eazy.daikou.request_data.request.inspection_ws;

import android.content.Context;

import com.eazy.daikou.repository_ws.RetrofitConnector;
import com.eazy.daikou.repository_ws.CustomCallback;
import com.eazy.daikou.repository_ws.CustomResponseListener;
import com.eazy.daikou.repository_ws.ServiceApi;
import com.eazy.daikou.request_data.static_data.StaticUtilsKey;
import com.eazy.daikou.helper.Utils;
import com.eazy.daikou.model.inspection.InspectorModel;
import com.eazy.daikou.model.my_property.service_provider.SelectPropertyModel;
import com.eazy.daikou.model.inspection.Author_ClerkUserInfo;
import com.eazy.daikou.model.inspection.ImageInspectionModel;
import com.eazy.daikou.model.inspection.InfoInspectionModel;
import com.eazy.daikou.model.inspection.ItemInspectionModel;
import com.eazy.daikou.model.inspection.ItemTemplateAPIModel;
import com.eazy.daikou.model.inspection.ItemTemplateModel;
import com.eazy.daikou.model.inspection.TemplateFormInspectionModel;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;

public class InspectionWS {

    public void getInspection_MaintenanceType(Context context, String action, CallBackItemInspectionListener callBackItemInspectionListener){
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = action.equalsIgnoreCase(StaticUtilsKey.inspection_action) ? serviceApi.getListInspectionType() : serviceApi.getMaintenanceType();
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data")){
                    List<ItemInspectionModel> list = new ArrayList<>();
                    JsonArray data = resObj.getAsJsonArray("data");
                    for (int i=0; i<data.size();i++) {
                        JsonElement item = data.get(i).getAsJsonObject();
                        ItemInspectionModel hisEmergencyModel = new Gson().fromJson(item, ItemInspectionModel.class);
                        list.add(hisEmergencyModel);
                    }
                    callBackItemInspectionListener.onSuccess(list);
                } else {
                    callBackItemInspectionListener.onFailed(resObj.get("msg").getAsString());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                callBackItemInspectionListener.onFailed(error);
            }
        }));
    }

    public void getInspection_MaintenanceItemAll(Context context, String action, int page, int limit, String property_id, String inspection_type_id, CallBackItemInspectionInfoListener callBackItemInspectionListener) {
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = action.equalsIgnoreCase(StaticUtilsKey.inspection_action) ? serviceApi.getListAllInspection(page, limit, property_id, inspection_type_id) : serviceApi.getListAllMaintenance(page, limit, property_id, inspection_type_id);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data")) {
                    JsonArray data = resObj.getAsJsonArray("data");
                    List<InfoInspectionModel> list = new ArrayList<>();
                    for (int i = 0; i < data.size(); i++) {
                        JsonElement item = data.get(i).getAsJsonObject();
                        InfoInspectionModel hisEmergencyModel = new Gson().fromJson(item, InfoInspectionModel.class);
                        list.add(hisEmergencyModel);
                    }
                    callBackItemInspectionListener.onSuccess(list);
                } else {
                    callBackItemInspectionListener.onFailed(resObj.get("msg").getAsString());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                callBackItemInspectionListener.onFailed(error);
            }
        }));
    }

    public void getTemplateTypeInspection(Context context, String action, int page, int limit, String property_id, String inspection_type_id, CallBackItemInspectionListener callBackItemInspectionListener) {
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement>   res = action.equals(StaticUtilsKey.inspection_action) ? serviceApi.getListTemplateType(page, limit, property_id, inspection_type_id) : serviceApi.getListTemplateTypeMaintenance(page, limit, property_id, inspection_type_id);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data")) {
                    JsonArray data = resObj.getAsJsonArray("data");
                    List<ItemInspectionModel> templateInspectionModelList = new ArrayList<>();
                    for (int i = 0; i < data.size(); i++) {
                        JsonElement item = data.get(i).getAsJsonObject();
                        ItemInspectionModel hisEmergencyModel = new Gson().fromJson(item, ItemInspectionModel.class);
                        templateInspectionModelList.add(hisEmergencyModel);
                    }
                    callBackItemInspectionListener.onSuccess(templateInspectionModelList);
                } else {
                    callBackItemInspectionListener.onFailed(resObj.get("msg").getAsString());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                callBackItemInspectionListener.onFailed(error);
            }
        }));
    }

    public void getUnitNoInspection(Context context, HashMap<String, Object> hashMap, CallBackUnitNoListener callBackItemInspectionListener) {
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = serviceApi.getUnitNoForInspection(hashMap);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data")) {
                    JsonArray data = resObj.getAsJsonArray("data");
                    List<SelectPropertyModel> templateInspectionModelList = new ArrayList<>();
                    for (int i = 0; i < data.size(); i++) {
                        JsonElement item = data.get(i).getAsJsonObject();
                        SelectPropertyModel hisEmergencyModel = new Gson().fromJson(item, SelectPropertyModel.class);
                        templateInspectionModelList.add(hisEmergencyModel);
                    }
                    callBackItemInspectionListener.onSuccessUnitList(templateInspectionModelList);
                } else {
                    callBackItemInspectionListener.onFailed(resObj.get("msg").getAsString());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                callBackItemInspectionListener.onFailed(error);
            }
        }));
    }

    public void selectEmployeeByDepartment(Context context, HashMap<String, Object> hashMap, CallBackUnitNoListener callBackItemInspectionListener) {
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement>   res = serviceApi.getEmployeeDepartment(hashMap);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data")) {
                    JsonArray data = resObj.getAsJsonArray("data");
                    List<Author_ClerkUserInfo> templateInspectionModelList = new ArrayList<>();
                    for (int i = 0; i < data.size(); i++) {
                        JsonElement item = data.get(i).getAsJsonObject();
                        Author_ClerkUserInfo hisEmergencyModel = new Gson().fromJson(item, Author_ClerkUserInfo.class);
                        templateInspectionModelList.add(hisEmergencyModel);
                    }
                    callBackItemInspectionListener.onSuccessDepartmentEmployee(templateInspectionModelList);
                } else {
                    callBackItemInspectionListener.onFailed(resObj.get("msg").getAsString());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                callBackItemInspectionListener.onFailed(error);
            }
        }));
    }

    public void getInspectors(Context context, String user_id, String user_type, CallBackItemInspectionListener callBackItemInspectionListener) {
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement>   res = serviceApi.getInspectors(user_id, user_type);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data")) {
                    JsonArray data = resObj.getAsJsonArray("data");
                    List<InspectorModel> templateInspectionModelList = new ArrayList<>();
                    for (int i = 0; i < data.size(); i++) {
                        JsonElement item = data.get(i).getAsJsonObject();
                        InspectorModel hisEmergencyModel = new Gson().fromJson(item, InspectorModel.class);
                        templateInspectionModelList.add(hisEmergencyModel);
                    }
                    callBackItemInspectionListener.onSuccessGetInspectorList(templateInspectionModelList);
                } else {
                    callBackItemInspectionListener.onFailed(resObj.get("msg").getAsString());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                callBackItemInspectionListener.onFailed(error);
            }
        }));
    }

    public void saveTemplateInspectionMaintenanceSchedule(Context context, String action, HashMap<String, Object> hashMap, CallBackItemInspectionListener callBackItemInspectionListener) {
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Utils.logDebug("Jeheejjjjjjjjjjjjjjjje", Utils.getStringGson(hashMap));
        Call<JsonElement>  res = action.equalsIgnoreCase(StaticUtilsKey.inspection_action) ? serviceApi.saveTemplateInspection(hashMap) : serviceApi.saveTemplateMaintenance(hashMap);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data")) {
                    if (!resObj.isJsonNull()){
                        JsonObject data = resObj.getAsJsonObject("data");
                        TemplateFormInspectionModel templateInspectionModelList = new Gson().fromJson(data, TemplateFormInspectionModel.class);
                        callBackItemInspectionListener.onSuccessSaveTemplate(templateInspectionModelList);
                    }
                } else {
                    callBackItemInspectionListener.onFailed(resObj.get("msg").getAsString());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                callBackItemInspectionListener.onFailed(error);
            }
        }));
    }

    public void saveInspectionList(Context context, HashMap<String, Object> hashMap, ItemTemplateModel itemTemplateModel, CallBackItemInspectionListener callBackItemInspectionListener) {
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        String mMap = Utils.getStringGson(hashMap);
        Utils.logDebug("jeejeehejhehej", mMap);
        Call<JsonElement>   res = serviceApi.saveInspectionList(hashMap);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data")) {
                    if (!resObj.isJsonNull()){
                        JsonObject data = resObj.getAsJsonObject("data");
                        ItemTemplateAPIModel templateInspectionModelList = new Gson().fromJson(data, ItemTemplateAPIModel.class);
                        callBackItemInspectionListener.onSuccessSaveInspectionList("Success", itemTemplateModel, templateInspectionModelList);
                    }
                } else {
                    callBackItemInspectionListener.onFailed(resObj.get("msg").getAsString());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                callBackItemInspectionListener.onFailed(error);
            }
        }));
    }

    public void getListInspection_MaintenanceForm(Context context, String action, int page, int limit, String inspection_id, CallBackItemInspectionListener callBackItemInspectionListener) {
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement>   res = action.equalsIgnoreCase(StaticUtilsKey.inspection_action) ? serviceApi.getListInspectionTemplateForm(page, limit, inspection_id) : serviceApi.getListMaintenanceForm(page, limit, inspection_id);
        Utils.logDebug("jeejeehejhehej", page + " " + limit + " " + inspection_id);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data")) {
                    JsonArray data = resObj.getAsJsonArray("data");
                    List<ItemTemplateAPIModel> inspectionModelList = new ArrayList<>();
                    for (int i = 0; i < data.size(); i++) {
                        ItemTemplateAPIModel templateInspectionModelList = new Gson().fromJson(data.get(i).getAsJsonObject(), ItemTemplateAPIModel.class);
                        inspectionModelList.add(templateInspectionModelList);
                    }
                    callBackItemInspectionListener.onSuccessGetInspectionFormList(inspectionModelList);
                } else {
                    callBackItemInspectionListener.onFailed(resObj.get("msg").getAsString());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                callBackItemInspectionListener.onFailed(error);
            }
        }));
    }

    public void updateInspectionList(Context context, HashMap<String, Object> hashMap, CallBackUpdateInspectionListener callBackItemInspectionListener) {
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement>   res = serviceApi.updateInspectionList(hashMap);
        String storeDate = new Gson().toJson(hashMap);
        Utils.logDebug("Jeheeejejee", storeDate);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data")) {
                    if (!resObj.get("data").isJsonNull()){
                        JsonObject data = resObj.getAsJsonObject("data");
                        ItemTemplateAPIModel templateInspectionModelList = new Gson().fromJson(data, ItemTemplateAPIModel.class);
                        callBackItemInspectionListener.onSuccessSaveInspectionList("Success", templateInspectionModelList);
                    }
                } else {
                    callBackItemInspectionListener.onFailed(resObj.get("msg").getAsString());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                callBackItemInspectionListener.onFailed(error);
            }
        }));
    }

    public void DeleteInspectionList(Context context, HashMap<String, Object> hashMap, CallBackUpdateInspectionListener callBackItemInspectionListener) {
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement>   res = serviceApi.deleteInspectionList(hashMap);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("success") && resObj.get("success").getAsString().equals("true")){
                    if (resObj.has("msg")) {
                        callBackItemInspectionListener.onSuccessDeleteInspectionList(resObj.get("msg").getAsString());
                    }
                } else {
                    if (resObj.has("msg")) {
                        callBackItemInspectionListener.onFailed(resObj.get("msg").getAsString());
                    }
                }
            }

            @Override
            public void onError(String error, int resCode) {
                callBackItemInspectionListener.onFailed(error);
            }
        }));
    }

    public void UploadDeleteImageList(Context context, HashMap<String, Object> hashMap, String actionDel_Upload, CallBackImageInspectionInfoListener callBackItemInspectionListener) {
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement>   res = serviceApi.upload_DeleteImageInspection(hashMap);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data")) {
                    JsonArray data = resObj.getAsJsonArray("data");
                    List<ImageInspectionModel> inspectionModelList = new ArrayList<>();
                    for (int i = 0; i < data.size(); i++) {
                        ImageInspectionModel templateInspectionModelList = new Gson().fromJson(data.get(i).getAsJsonObject(), ImageInspectionModel.class);
                        inspectionModelList.add(templateInspectionModelList);
                    }
                    callBackItemInspectionListener.onSuccessDeleteInspectionList(inspectionModelList, actionDel_Upload, resObj.get("msg").getAsString());
                }  else {
                    if (resObj.has("msg")) {
                        callBackItemInspectionListener.onFailed(resObj.get("msg").getAsString());
                    }
                }
            }

            @Override
            public void onError(String error, int resCode) {
                callBackItemInspectionListener.onFailed(error);
            }
        }));
    }

    public void getListImageInspection(Context context, String part_type, String inspection_level, String inspection_room_id, String inspection_room_item_id, CallBackImageInspectionInfoListener callBackItemInspectionListener) {
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement>   res = serviceApi.getListImageInspection(part_type, inspection_level, inspection_room_id,inspection_room_item_id);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data")) {
                    List<ImageInspectionModel> imageInspectionModelList = new ArrayList<>();
                    JsonArray data = resObj.get("data").getAsJsonArray();
                    for (int i = 0; i < data.size(); i++) {
                        ImageInspectionModel imageInspectionModel = new Gson().fromJson(data.get(i).getAsJsonObject(), ImageInspectionModel.class);
                        imageInspectionModel.setAlreadySaveImage(true);
                        imageInspectionModelList.add(imageInspectionModel);
                    }
                    callBackItemInspectionListener.onSuccessListImageInspection(imageInspectionModelList);
                } else {
                    callBackItemInspectionListener.onFailed(resObj.get("msg").getAsString());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                callBackItemInspectionListener.onFailed(error);
            }
        }));
    }

    public void getListTemplateInspection(Context context, String inspection_id, CallBackItemInspectionListener callBackItemInspectionListener) {
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement>   res = serviceApi.getTemplateInspection(inspection_id);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data")) {
                    JsonArray data = resObj.getAsJsonArray("data");
                    List<ItemTemplateAPIModel> inspectionModelList = new ArrayList<>();
                    for (int i = 0; i < data.size(); i++) {
                        ItemTemplateAPIModel templateInspectionModelList = new Gson().fromJson(data.get(i).getAsJsonObject(), ItemTemplateAPIModel.class);
                        inspectionModelList.add(templateInspectionModelList);
                    }
                    callBackItemInspectionListener.onSuccessGetInspectionFormList(inspectionModelList);
                } else {
                    callBackItemInspectionListener.onFailed(resObj.get("msg").getAsString());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                callBackItemInspectionListener.onFailed(error);
            }
        }));
    }

    public void getListInspectionByCalendar(Context context, HashMap<String, Object> hashMap, CallBackItemInspectionInfoListener callBackItemInspectionListener) {
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = serviceApi.getInspectionListByCalendar(hashMap);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data")) {
                    JsonArray data = resObj.getAsJsonArray("data");
                    List<InfoInspectionModel> list = new ArrayList<>();
                    for (int i = 0; i < data.size(); i++) {
                        JsonElement item = data.get(i).getAsJsonObject();
                        InfoInspectionModel hisEmergencyModel = new Gson().fromJson(item, InfoInspectionModel.class);
                        list.add(hisEmergencyModel);
                    }
                    callBackItemInspectionListener.onSuccess(list);
                } else {
                    callBackItemInspectionListener.onFailed(resObj.get("msg").getAsString());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                callBackItemInspectionListener.onFailed(error);
            }
        }));
    }

    public interface CallBackUnitNoListener{
        void onSuccessUnitList(List<SelectPropertyModel> itemInspectionModels);
        void onSuccessDepartmentEmployee(List<Author_ClerkUserInfo> itemInspectionModels);
        void onFailed(String msg);
    }

    public interface CallBackItemInspectionListener{
        void onSuccess(List<ItemInspectionModel> itemInspectionModels);
        void onSuccessGetInspectionFormList(List<ItemTemplateAPIModel> itemInspectionModels);
        void onSuccessSaveTemplate(TemplateFormInspectionModel itemInspectionModels);
        void onSuccessSaveInspectionList(String msg, ItemTemplateModel itemTemplateModel, ItemTemplateAPIModel modelList);
        void onSuccessGetInspectorList(List<InspectorModel> inspectorModelList);
        void onFailed(String msg);
    }

    public interface CallBackUpdateInspectionListener{
        void onSuccessSaveInspectionList(String msg, ItemTemplateAPIModel modelList);
        void onSuccessDeleteInspectionList(String msg);
        void onFailed(String msg);
    }

    public interface CallBackItemInspectionInfoListener{
        void onSuccess(List<InfoInspectionModel> itemInspectionModels);
        void onFailed(String msg);
    }

    public interface CallBackImageInspectionInfoListener{
        void onSuccessListImageInspection(List<ImageInspectionModel> itemInspectionModels);
        void onSuccessDeleteInspectionList(List<ImageInspectionModel> list, String action, String msg);
        void onFailed(String msg);
    }

}
