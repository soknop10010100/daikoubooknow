package com.eazy.daikou.request_data.request.home_ws;

import android.content.Context;

import com.eazy.daikou.R;
import com.eazy.daikou.repository_ws.CustomCallback;
import com.eazy.daikou.repository_ws.CustomResponseListener;
import com.eazy.daikou.helper.UserSessionManagement;
import com.eazy.daikou.model.home.MainItemHomeModel;
import com.eazy.daikou.model.home.MyRolePermissionModel;
import com.eazy.daikou.model.home.PermissionModel;
import com.eazy.daikou.model.home.SubItemHomeModel;
import com.eazy.daikou.model.home.UserDeactivated;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.eazy.daikou.repository_ws.RetrofitConnector;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;

public class HomeWs {

    public void getUserPermission(Context context, HashMap<String, Object> hashMap,  UserPermissionCallBack permissionCallBack){
        Call<JsonElement> res = RetrofitConnector.createService(context).getUserPermission(hashMap);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data")){
                    JsonArray jsonArray = resObj.get("data").getAsJsonArray();
                    List<PermissionModel> list = new ArrayList<>();
                    for (int i = 0; i < jsonArray.size(); i++) {
                        PermissionModel userPermissionModel = new Gson().fromJson(jsonArray.get(i).getAsJsonObject(), PermissionModel.class);
                        list.add(userPermissionModel);
                    }
                    permissionCallBack.onSuccess(list);
                } else {
                    permissionCallBack.onFailure(resObj.get("msg").getAsString());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                permissionCallBack.onFailure(error);
            }
        }));
    }

    public void getUserRolePermission(Context context, HashMap<String, Object> hashMap,  UserPermissionCallBack permissionCallBack){
        Call<JsonElement> res = RetrofitConnector.createService(context).getUserMyRolePermission(hashMap);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data")){
                    JsonArray jsonArray = resObj.get("data").getAsJsonArray();
                    ArrayList<MyRolePermissionModel> list = new ArrayList<>();
                    for (int i = 0; i < jsonArray.size(); i++) {
                        MyRolePermissionModel userPermissionModel = new Gson().fromJson(jsonArray.get(i).getAsJsonObject(), MyRolePermissionModel.class);
                        list.add(userPermissionModel);
                    }
                    permissionCallBack.onSuccessUserRolePermission(list);
                } else {
                    permissionCallBack.onFailure(resObj.get("msg").getAsString());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                permissionCallBack.onFailure(error);
            }
        }));
    }

    public void getItemHomeList(Context context, String date,  HomeItemCallBack permissionCallBack){
        Call<JsonElement> res = RetrofitConnector.createService(context).getItemHomeMenu(date, new UserSessionManagement(context).getUserId());
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data")){
                    JsonObject jsonArray = resObj.get("data").getAsJsonObject();
                    List<MainItemHomeModel> mainItemHomeModelLIst = new ArrayList<>();
                    UserDeactivated userDeactivated = new UserDeactivated();

                    if(jsonArray.has("hotel_listing_booking")){     //we not show hotel listing in daikou so this condition false
                        JsonArray developmentData = jsonArray.get("hotel_listing_booking").getAsJsonArray();
                        if (developmentData.size() > 0){
                            ArrayList<SubItemHomeModel> subItemHomeModelList = new ArrayList<>();
                            MainItemHomeModel mainItemHomeModel = new MainItemHomeModel();
                            mainItemHomeModel.setHeaderTitle(context.getString(R.string.hotel_booking));
                            mainItemHomeModel.setMainAction("hotel_property");
                            for (int i = 0; i < developmentData.size(); i++) {
                                SubItemHomeModel subItemHomeModel = new SubItemHomeModel();
                                JsonObject jsonObject = developmentData.get(i).getAsJsonObject();
                                if (jsonObject.has("id")) {
                                    if (!jsonObject.get("id").isJsonNull()) {
                                        subItemHomeModel.setId(jsonObject.get("id").getAsString());
                                    }
                                }
                                if (jsonObject.has("hotel_name")) {
                                    if (!jsonObject.get("hotel_name").isJsonNull()) {
                                        subItemHomeModel.setName(jsonObject.get("hotel_name").getAsString());
                                    }
                                }
                                if (jsonObject.has("hotel_location")) {
                                    if (!jsonObject.get("hotel_location").isJsonNull()) {
                                        subItemHomeModel.setDetailName(jsonObject.get("hotel_location").getAsString());
                                    }
                                }
                                if (jsonObject.has("hotel_image")) {
                                    if (!jsonObject.get("hotel_image").isJsonNull()) {
                                        subItemHomeModel.setImageUrl(jsonObject.get("hotel_image").getAsString());
                                    }
                                }
                                subItemHomeModel.setAction("hotel_property");
                                subItemHomeModelList.add(subItemHomeModel);
                            }
                            mainItemHomeModel.setSubItemHomeModelList(subItemHomeModelList);
                            mainItemHomeModelLIst.add(mainItemHomeModel);
                        }
                    }

                    if(jsonArray.has("property_listing_sale")){
                        JsonArray developmentData = jsonArray.get("property_listing_sale").getAsJsonArray();
                        if (developmentData.size() > 0){
                            ArrayList<SubItemHomeModel> subItemHomeModelList = new ArrayList<>();
                            MainItemHomeModel mainItemHomeModel = new MainItemHomeModel();
                            mainItemHomeModel.setHeaderTitle(context.getString(R.string.buy_property));
                            mainItemHomeModel.setMainAction("buy_property");
                            for (int i = 0; i < developmentData.size(); i++) {
                                SubItemHomeModel subItemHomeModel = new SubItemHomeModel();
                                JsonObject jsonObject = developmentData.get(i).getAsJsonObject();
                                if (jsonObject.has("id")) {
                                    if (!jsonObject.get("id").isJsonNull()) {
                                        subItemHomeModel.setId(jsonObject.get("id").getAsString());
                                    }
                                }
                                if (jsonObject.has("property_name")) {
                                    if (!jsonObject.get("property_name").isJsonNull()) {
                                        subItemHomeModel.setName(jsonObject.get("property_name").getAsString());
                                    }
                                }
                                if (jsonObject.has("property_location_name")) {
                                    if (!jsonObject.get("property_location_name").isJsonNull()) {
                                        subItemHomeModel.setDetailName(jsonObject.get("property_location_name").getAsString());
                                    }
                                }
                                if (jsonObject.has("property_image")) {
                                    if (!jsonObject.get("property_image").isJsonNull()) {
                                        subItemHomeModel.setImageUrl(jsonObject.get("property_image").getAsString());
                                    }
                                }
                                subItemHomeModel.setAction("buy_property");
                                subItemHomeModelList.add(subItemHomeModel);
                            }
                            mainItemHomeModel.setSubItemHomeModelList(subItemHomeModelList);
                            mainItemHomeModelLIst.add(mainItemHomeModel);
                        }
                    }

                    if(jsonArray.has("property_listing_rent")){
                        JsonArray developmentData = jsonArray.get("property_listing_rent").getAsJsonArray();
                        if (developmentData.size() > 0){
                            ArrayList<SubItemHomeModel> subItemHomeModelList = new ArrayList<>();
                            MainItemHomeModel mainItemHomeModel = new MainItemHomeModel();
                            mainItemHomeModel.setHeaderTitle(context.getString(R.string.rent_property));
                            mainItemHomeModel.setMainAction("rent_property");
                            for (int i = 0; i < developmentData.size(); i++) {
                                SubItemHomeModel subItemHomeModel = new SubItemHomeModel();
                                JsonObject jsonObject = developmentData.get(i).getAsJsonObject();
                                if (jsonObject.has("id")) {
                                    if (!jsonObject.get("id").isJsonNull()) {
                                        subItemHomeModel.setId(jsonObject.get("id").getAsString());
                                    }
                                }
                                if (jsonObject.has("property_name")) {
                                    if (!jsonObject.get("property_name").isJsonNull()) {
                                        subItemHomeModel.setName(jsonObject.get("property_name").getAsString());
                                    }
                                }
                                if (jsonObject.has("property_location_name")) {
                                    if (!jsonObject.get("property_location_name").isJsonNull()) {
                                        subItemHomeModel.setDetailName(jsonObject.get("property_location_name").getAsString());
                                    }
                                }
                                if (jsonObject.has("property_image")) {
                                    if (!jsonObject.get("property_image").isJsonNull()) {
                                        subItemHomeModel.setImageUrl(jsonObject.get("property_image").getAsString());
                                    }
                                }
                                subItemHomeModel.setAction("rent_property");
                                subItemHomeModelList.add(subItemHomeModel);
                            }
                            mainItemHomeModel.setSubItemHomeModelList(subItemHomeModelList);
                            mainItemHomeModelLIst.add(mainItemHomeModel);
                        }
                    }

                    if(jsonArray.has("development_project_list")){
                        JsonArray developmentData = jsonArray.get("development_project_list").getAsJsonArray();
                        if (developmentData.size() > 0){
                            ArrayList<SubItemHomeModel> subItemHomeModelList = new ArrayList<>();
                            MainItemHomeModel mainItemHomeModel = new MainItemHomeModel();
                            mainItemHomeModel.setHeaderTitle(context.getString(R.string.project_development));
                            mainItemHomeModel.setMainAction("property");
                            for (int i = 0; i < developmentData.size(); i++) {
                                SubItemHomeModel subItemHomeModel = new SubItemHomeModel();
                                JsonObject jsonObject = developmentData.get(i).getAsJsonObject();
                                if (jsonObject.has("id")) {
                                    if (!jsonObject.get("id").isJsonNull()) {
                                        subItemHomeModel.setId(jsonObject.get("id").getAsString());
                                    }
                                }
                                if (jsonObject.has("building_name")) {
                                    if (!jsonObject.get("building_name").isJsonNull()) {
                                        subItemHomeModel.setName(jsonObject.get("building_name").getAsString());
                                    }
                                }
                                if (jsonObject.has("b_address")) {
                                    if (!jsonObject.get("b_address").isJsonNull()) {
                                        subItemHomeModel.setDetailName(jsonObject.get("b_address").getAsString());
                                    }
                                }
                                if (jsonObject.has("file_path")) {
                                    if (!jsonObject.get("file_path").isJsonNull()) {
                                        subItemHomeModel.setImageUrl(jsonObject.get("file_path").getAsString());
                                    }
                                }
                                if (jsonObject.has("b_website")) {
                                    if (!jsonObject.get("b_website").isJsonNull()) {
                                        subItemHomeModel.setBWebsite(jsonObject.get("b_website").getAsString());
                                    }
                                }
                                subItemHomeModel.setAction("property");
                                subItemHomeModelList.add(subItemHomeModel);
                            }
                            mainItemHomeModel.setSubItemHomeModelList(subItemHomeModelList);
                            mainItemHomeModelLIst.add(mainItemHomeModel);
                        }
                    }

                    if(jsonArray.has("facility_booking_space_list")){
                        JsonArray developmentData = jsonArray.get("facility_booking_space_list").getAsJsonArray();
                        if (developmentData.size() > 0){
                            ArrayList<SubItemHomeModel> subItemHomeModelList = new ArrayList<>();
                            MainItemHomeModel mainItemHomeModel = new MainItemHomeModel();
                            mainItemHomeModel.setHeaderTitle(context.getString(R.string.facility_booking));
                            mainItemHomeModel.setMainAction("facility_booking");
                            for (int i = 0; i < developmentData.size(); i++) {
                                SubItemHomeModel subItemHomeModel = new SubItemHomeModel();
                                JsonObject jsonObject = developmentData.get(i).getAsJsonObject();
                                if (jsonObject.has("space_id")) {
                                    if (!jsonObject.get("space_id").isJsonNull()) {
                                        subItemHomeModel.setId(jsonObject.get("space_id").getAsString());
                                    }
                                }
                                if (jsonObject.has("space_name")) {
                                    if (!jsonObject.get("space_name").isJsonNull()) {
                                        subItemHomeModel.setName(jsonObject.get("space_name").getAsString());
                                    }
                                }
                                if (jsonObject.has("space_price")) {
                                    if (!jsonObject.get("space_price").isJsonNull()) {
                                        if (jsonObject.get("space_price").getAsJsonObject().has("price")) {
                                            if (!jsonObject.get("space_price").getAsJsonObject().get("price").isJsonNull()) {
                                                subItemHomeModel.setDetailName(jsonObject.get("space_price").getAsJsonObject().get("price").getAsString());
                                            }
                                        }
                                    }
                                }

                                if (jsonObject.has("is_free")) {
                                    subItemHomeModel.setFreeBooking(jsonObject.get("is_free").getAsBoolean());
                                }
                                if (jsonObject.has("space_images")) {
                                    if (!jsonObject.get("space_images").isJsonNull()) {
                                        subItemHomeModel.setImageUrl(jsonObject.get("space_images").getAsString());
                                    }
                                }
                                subItemHomeModel.setAction("facility_booking");
                                subItemHomeModelList.add(subItemHomeModel);
                            }
                            mainItemHomeModel.setSubItemHomeModelList(subItemHomeModelList);
                            mainItemHomeModelLIst.add(mainItemHomeModel);
                        }
                    }

                    if(jsonArray.has("user_deactivated")){
                        JsonObject developmentData = jsonArray.get("user_deactivated").getAsJsonObject();
                        if (developmentData.has("is_deactivated")) {
                            userDeactivated.set_deactivated(developmentData.get("is_deactivated").getAsBoolean());
                        }
                        if (developmentData.has("is_expired")) {
                            userDeactivated.set_expired(developmentData.get("is_expired").getAsBoolean());
                        }
                    }

                    permissionCallBack.onSuccess(mainItemHomeModelLIst, userDeactivated);
                } else {
                    permissionCallBack.onFailure(resObj.get("msg").getAsString());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                permissionCallBack.onFailure(error);
            }
        }));
    }

    public interface UserPermissionCallBack{
        void onSuccess(List<PermissionModel> userPermissionModels);
        void onSuccessUserRolePermission(List<MyRolePermissionModel> userPermissionModels);
        void onFailure(String errorMessage);
    }

    public interface HomeItemCallBack{
        void onSuccess(List<MainItemHomeModel> listUserDeactivated, UserDeactivated userDeactivated);
        void onFailure(String errorMessage);
    }

}
