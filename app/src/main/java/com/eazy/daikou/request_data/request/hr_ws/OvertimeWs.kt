package com.eazy.daikou.request_data.request.hr_ws

import android.content.Context
import com.eazy.daikou.repository_ws.CustomCallback
import com.eazy.daikou.repository_ws.CustomResponseListener
import com.eazy.daikou.repository_ws.RetrofitGenerator
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.hr.ItemOvertimeModel
import com.eazy.daikou.model.hr.OvertimeListModel
import com.eazy.daikou.model.hr.OvertimeMainModel
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class OvertimeWs {

    fun getOvertimeListWs(context: Context, page: Int, limit: Int, userBusinessKey: String, listCategory: String, accountBusinessKey: String, month: String, year: String, onCallBackOvertimeListener: OnCallBackOvertimeListener) {
        val serviceMenuApi = RetrofitGenerator.createServiceWithHR(context)
        val res = serviceMenuApi.getListOvertimeList(page, limit, userBusinessKey, listCategory, accountBusinessKey, month, year)
        res.enqueue(CustomCallback(context, object : CustomResponseListener {
            override fun onSuccess(resObj: JsonObject?) {
                if (resObj != null) {
                    if (resObj.has("data")) {
                        val data = resObj["data"].asJsonObject
                        val overtimeListModel: OvertimeMainModel = Gson().fromJson(data, OvertimeMainModel::class.java)
                        onCallBackOvertimeListener.onLoadSuccessFull(overtimeListModel)
                    } else {
                        onCallBackOvertimeListener.onLoadFailed(resObj.get("msg").asString)
                    }
                }
            }

            override fun onError(error: String, resCode: Int) {
                onCallBackOvertimeListener.onLoadFailed(error)
            }
        }))
    }

    fun getDetailOvertimeWs(context: Context, overtime_list_id: String, onCallBackDetailOvertimeListener: OnCallBackDetailOvertimeListener) {
        val serviceMenuApi = RetrofitGenerator.createServiceWithHR(context)
        val res = serviceMenuApi.getListDetailOvertime(overtime_list_id)
        res.enqueue(CustomCallback(context, object : CustomResponseListener{
            override fun onSuccess(resObj: JsonObject?) {
                if (resObj != null) {
                    if (resObj.has("data")) {
                        if (resObj.has("data") && !resObj["data"].isJsonNull) {
                            val item: JsonElement = resObj["data"].asJsonObject
                            val overtimeListModel = Gson().fromJson(item, OvertimeListModel::class.java)
                            onCallBackDetailOvertimeListener.onLoadSuccessFull(overtimeListModel)
                        } else {
                            onCallBackDetailOvertimeListener.onLoadFailed(resObj.get("msg").asString)
                        }
                    }
                }
            }

            override fun onError(error: String, resCode: Int) {
                onCallBackDetailOvertimeListener.onLoadFailed(error)
            }
        }))
    }

    fun getEmployeeNameListWs(context: Context, account_business_key: String, search: String, onCallBackEmployeeNameListener: OnCallBackEmployeeNameListener) {
        val serviceMenuApi = RetrofitGenerator.createServiceWithHR(context)
        val res = serviceMenuApi.getListEmployeeName(account_business_key, search)
        res.enqueue(CustomCallback(context, object : CustomResponseListener {
            override fun onSuccess(resObj: JsonObject?) {
                if (resObj != null) {
                    if (resObj.has("data")) {
                        val list: ArrayList<ItemOvertimeModel> = ArrayList()
                        val data = resObj["data"].asJsonArray
                        for (i in 0 until data.size()) {
                            val item: JsonElement = data[i].asJsonObject
                            val itemOvertimeModel: ItemOvertimeModel =
                                Gson().fromJson(item, ItemOvertimeModel::class.java)
                            list.add(itemOvertimeModel)
                        }
                        onCallBackEmployeeNameListener.onLoadSuccessFull(list)
                    } else {
                        onCallBackEmployeeNameListener.onLoadFailed(resObj.get("msg").asString)
                    }
                }
            }

            override fun onError(error: String, resCode: Int) {
                onCallBackEmployeeNameListener.onLoadFailed(error)
            }
        }))
    }

    fun getCategoryOvertimeWs(context: Context,account_business_key: String ,onCallBackEmployeeNameListener: OnCallBackEmployeeNameListener){
        val serviceMenuApi = RetrofitGenerator.createServiceWithHR(context)
        val res = serviceMenuApi.getListCategoryOvertime(account_business_key)
        res.enqueue(CustomCallback(context,object :CustomResponseListener{
            override fun onSuccess(resObj: JsonObject?) {
                if (resObj != null) {
                    if (resObj.has("data")) {
                        val listCategory: ArrayList<ItemOvertimeModel> = ArrayList()
                        val data = resObj["data"].asJsonArray
                        for (i in 0 until data.size()) {
                            val item: JsonElement = data[i].asJsonObject
                            val itemOvertimeModel: ItemOvertimeModel = Gson().fromJson(item, ItemOvertimeModel::class.java)
                            listCategory.add(itemOvertimeModel)
                        }
                        onCallBackEmployeeNameListener.onLoadSuccessFull(listCategory)
                    }else{
                        onCallBackEmployeeNameListener.onLoadFailed(resObj.get("mag").asString)
                    }
                }
            }

            override fun onError(error: String, resCode: Int) {
                onCallBackEmployeeNameListener.onLoadFailed(error)
            }
        }))
    }

    fun getAllShiftListWs(context: Context, account_business_key: String, onCallBackEmployeeNameListener: OnCallBackEmployeeNameListener) {
        val serviceMenuApi = RetrofitGenerator.createServiceWithHR(context)
        val res = serviceMenuApi.getListAllShift(account_business_key)
        res.enqueue(CustomCallback(context, object : CustomResponseListener {
            override fun onSuccess(resObj: JsonObject?) {
                if (resObj != null) {
                    if (resObj.has("data")) {
                        val listShift: ArrayList<ItemOvertimeModel> = ArrayList()
                        val data = resObj["data"].asJsonArray
                        for (i in 0 until data.size()) {
                            val item: JsonElement = data[i].asJsonObject
                            val itemOvertimeModel: ItemOvertimeModel =
                                Gson().fromJson(item, ItemOvertimeModel::class.java)
                            listShift.add(itemOvertimeModel)
                        }
                        onCallBackEmployeeNameListener.onLoadSuccessFull(listShift)
                    } else {
                        onCallBackEmployeeNameListener.onLoadFailed(resObj.get("mag").asString)
                    }
                }
            }

            override fun onError(error: String, resCode: Int) {
                onCallBackEmployeeNameListener.onLoadFailed(error)
            }
        }))
    }

    fun getCreateListOvertimeWs(context: Context, hashMap: HashMap<String, Any>, callBack: OnCallBackCreateOvertime){
        val serviceMenuApi = RetrofitGenerator.createServiceWithHR(context)
        val res = serviceMenuApi.getCreateListOvertime(hashMap)
        Utils.logDebug("jehehejeeejejej", Utils.getStringGson(hashMap))
        res.enqueue(CustomCallback(context, object : CustomResponseListener{
            override fun onSuccess(resObj: JsonObject?) {
                if (resObj != null) {
                    if (resObj.has("success")){
                        val isSuccess = resObj.get("success").asBoolean
                        if (isSuccess){
                            if (resObj.has("msg")){
                                callBack.onLoadSuccessFull(resObj["msg"].asString)
                            }
                        }else{
                            callBack.onLoadFailed(resObj["msg"].asString)
                        }
                    }
                }
            }

            override fun onError(error: String, resCode: Int) {
                callBack.onLoadFailed(error)
            }

        }))
    }

    fun getActionApproveRejectWS(context: Context, hashMap: HashMap<String, Any>, callBack: OnCallBackActionOvertime){
        val serviceApi = RetrofitGenerator.createServiceWithHR(context)
        val res = serviceApi.getActionApproveReject(hashMap)
        res.enqueue(CustomCallback(context,object : CustomResponseListener{
            override fun onSuccess(resObj: JsonObject?) {
               if (resObj != null){
                   if (resObj.has("success")){
                       val isSuccess = resObj.get("success").asBoolean
                       if (isSuccess){
                           if (resObj.has("msg")){
                               callBack.onLoadSuccessFull(resObj["msg"].asString)
                           }else{
                               callBack.onLoadFailed(resObj["msg"].asString)
                           }
                       }
                   }
               }
            }

            override fun onError(error: String, resCode: Int) {
                callBack.onLoadFailed(error)
            }
        }))
    }

    interface OnCallBackOvertimeListener{
        fun onLoadSuccessFull(overtimeListModel: OvertimeMainModel)
        fun onLoadFailed(message: String)
    }
    interface OnCallBackDetailOvertimeListener{
        fun onLoadSuccessFull(overtimeListModel: OvertimeListModel)
        fun onLoadFailed(message: String)
    }

    interface OnCallBackEmployeeNameListener{
        fun  onLoadSuccessFull(itemOvertimeModel: ArrayList<ItemOvertimeModel>)
        fun  onLoadFailed(message: String)
    }

    interface OnCallBackCreateOvertime{
        fun onLoadSuccessFull(message: String)
        fun onLoadFailed(message: String)
    }

    interface OnCallBackActionOvertime{
        fun onLoadSuccessFull(message: String)
        fun onLoadFailed(message: String)
    }
}