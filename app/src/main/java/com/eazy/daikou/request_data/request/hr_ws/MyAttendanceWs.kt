package com.eazy.daikou.request_data.request.hr_ws

import android.content.Context
import com.eazy.daikou.repository_ws.CustomCallback
import com.eazy.daikou.repository_ws.CustomCallback.popupSessionExpired
import com.eazy.daikou.repository_ws.CustomResponseListener
import com.eazy.daikou.repository_ws.RetrofitGenerator
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.hr.*
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.google.gson.JsonObject

class MyAttendanceWs {

    fun getListMyAttendance(context: Context, page: Int, userId : String, callBackListener: OnCallBackListener){
        val api = RetrofitGenerator.createServiceWithHR(context)
        val res = api.getListMyAttendance(page, 10, userId)
        res.enqueue(CustomCallback(context, object : CustomResponseListener {
            override fun onSuccess(resObj: JsonObject) {
                if (resObj.has("data")) {
                    val data = resObj["data"].asJsonObject
                    val hrAttendanceModel = Gson().fromJson(data.asJsonObject, HRAttendanceModel::class.java)
                    callBackListener.onLoadListSuccess(hrAttendanceModel)

                } else {
                    if(resObj["msg"].asString == "Unauthenticated."){
                        popupSessionExpired(context)
                    }else{
                        callBackListener.onLoadFail(resObj["msg"].asString)
                    }

                }
            }

            override fun onError(error: String, resCode: Int) {
                callBackListener.onLoadFail(error)
            }
        }))
    }

    fun checkOutAttendance(context: Context, hashMap: HashMap<String, Any?>, callBackListener: OnAttendanceCallBackListener){
        val api = RetrofitGenerator.createServiceWithHR(context)
        val res = api.checkOutAttendance(hashMap)
        Utils.logDebug("eheheheejhejej", Utils.getStringGson(hashMap))
        res.enqueue(CustomCallback(context, object : CustomResponseListener {
            override fun onSuccess(resObj: JsonObject) {
                if (resObj.has("success") && resObj["success"].asString == "true") {
                    if (resObj.has("msg")) {
                        callBackListener.onLoadListSuccess(resObj["msg"].asString)
                    } else {
                        callBackListener.onLoadFail("error")
                    }
                } else {
                    callBackListener.onLoadFail(resObj["msg"].asString)
                }
            }

            override fun onError(error: String, resCode: Int) {
                callBackListener.onLoadFail(error)
            }
        }))
    }

    fun getListAttendanceDetail(context: Context, employeeAttendanceId : String, attendanceType : String, callBackListener: OnCallBackListener){
        val api = RetrofitGenerator.createServiceWithHR(context)
        val res = api.getListMyAttendanceDetail(employeeAttendanceId, attendanceType)
        res.enqueue(CustomCallback(context, object : CustomResponseListener {
            override fun onSuccess(resObj: JsonObject?) {
                if (resObj != null) {
                    if (resObj.has("data") && !resObj["data"].isJsonNull) {
                        val data: JsonElement = resObj["data"].asJsonObject
                        val attendanceDetail = Gson().fromJson(data, HrAttendanceDetailModel::class.java)
                        callBackListener.onLoadListDetailSuccess(attendanceDetail)
                    } else {
                        callBackListener.onLoadFail(resObj["msg"].asString)
                    }
                }
            }

            override fun onError(error: String, resCode: Int) {
                callBackListener.onLoadFail(error)
            }
        }))
    }
    fun getAttendanceViewAllListWs(context: Context, page: Int, limit: Int, date: String, callBackAllListener : OnCallBackListAllListener){
        val serviceApi = RetrofitGenerator.createServiceWithHR(context)
        val res = serviceApi.getListAttendanceViewAllList(page, limit, date)
        res.enqueue(CustomCallback(context, object : CustomResponseListener{
            override fun onSuccess(resObj: JsonObject?) {
                if (resObj != null){
                    if (resObj.has("data")){
                        val listAll : ArrayList<ItemAttendanceModel> = ArrayList()
                        val data = resObj["data"].asJsonArray
                        for (i in 0 until data.size()){
                            val item: JsonElement = data[i].asJsonObject
                            val itemAllModel : ItemAttendanceModel = Gson().fromJson(item, ItemAttendanceModel::class.java)
                            listAll.add(itemAllModel)
                        }
                        callBackAllListener.onSuccessFul(listAll)
                    } else {
                        callBackAllListener.onFailed(resObj.get("msg").asString)
                    }
                }
            }

            override fun onError(error: String?, resCode: Int) {
                if (error != null) {
                    callBackAllListener.onFailed(error)
                }
            }

        }))
    }

    interface OnCallBackListener {
        fun onLoadListSuccess(hrAttendanceModel : HRAttendanceModel)
        fun onLoadListDetailSuccess(hrAttendanceModel : HrAttendanceDetailModel)
        fun onLoadFail(message: String)
    }

    interface OnCallBackListAllListener{
        fun onSuccessFul( listAll : ArrayList<ItemAttendanceModel>)
        fun onFailed(error : String)

    }

    interface OnAttendanceCallBackListener {
        fun onLoadListSuccess(msg : String)
        fun onLoadFail(message: String)
    }
}