package com.eazy.daikou.request_data.request.sign_up_v2

import android.content.Context
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.RetrofitConnector
import com.eazy.daikou.repository_ws.CustomCallback
import com.eazy.daikou.repository_ws.CustomResponseListener
import com.eazy.daikou.helper.InternetConnection
import com.google.gson.JsonObject

class SignUpV2Ws {

    companion object{
        fun createUser(context: Context,body: HashMap<String,Any>,callBack: OnCallBackSignUP)
        {
            val api = RetrofitConnector.createService(context).newSignUp(body)

            api.enqueue(object :CustomCallback(context,object :CustomResponseListener{
                override fun onSuccess(resObj: JsonObject?) {
                    val jsonObject = resObj!!.asJsonObject

                    val msg = jsonObject["msg"].toString()

                    if (msg == "\"This email is already taken.\"")
                    {
                        callBack.onError(context.getString(R.string.this_account_already_exist))
                    }
                    else
                    {
                      callBack.onSuccess(context.getString(R.string.create_account_success))
                    }
                }

                override fun onError(error: String?, resCode: Int) {
                    if (!InternetConnection.checkInternetConnectionActivity(context))
                    {
                        callBack.onError(context.getString(R.string.please_check_your_internet_connection))
                    }
                    else if (resCode == 500)
                    {
                        callBack.onError(context.getString(R.string.something_went_wrong))
                    }
                }

            }){})
        }
    }

    interface OnCallBackSignUP{
        fun onSuccess(msg :String)
        fun onError(msg:String)
    }
}