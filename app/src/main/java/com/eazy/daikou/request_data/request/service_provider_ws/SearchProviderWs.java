package com.eazy.daikou.request_data.request.service_provider_ws;

import android.content.Context;

import com.eazy.daikou.repository_ws.RetrofitConnector;
import com.eazy.daikou.repository_ws.CustomCallback;
import com.eazy.daikou.repository_ws.CustomResponseListener;
import com.eazy.daikou.repository_ws.ServiceApi;
import com.eazy.daikou.model.my_property.service_provider.LatLngServiceProvider;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

public class SearchProviderWs {
    public void searchServiceProvider(Context context, String text, int page, int limit, SearchProviderWs.RequestSearchServiceProvider request){
        List<LatLngServiceProvider> serviceList = new ArrayList<>();
        ServiceApi serviceApi = RetrofitConnector.createService( context);
        Call<JsonElement> res = serviceApi.searchServiceProvider(page,limit,text);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data")){
                    JsonArray jsonArray = resObj.get("data").getAsJsonArray();
                    for (int i = 0; i <jsonArray.size() ; i++) {
                        JsonObject item = jsonArray.get(i).getAsJsonObject();
                        LatLngServiceProvider serviceModel = new Gson().fromJson(item.toString(), LatLngServiceProvider.class);
                        serviceList.add(serviceModel);
                    }
                    request.onLoadServiceProvider(serviceList);
                }
            }

            @Override
            public void onError(String error, int resCode) {
                request.OnLoadFail(error);
            }
        }));
    }

    public interface RequestSearchServiceProvider{
        void onLoadServiceProvider(List<LatLngServiceProvider> serviceProvider);
        void OnLoadFail(String message);
    }
}
