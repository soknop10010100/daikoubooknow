package com.eazy.daikou.request_data.sql_database.reader;

import android.provider.BaseColumns;

public class TrackUtilityReader {

    private TrackUtilityReader(){}

    public static class Floor implements BaseColumns {
        public static final String TABLE_NAME = "floor";
        public static final String UTIL_LOCATION_ID = "util_location_id";
        public static final String SAVE_DATE = "save_date";
    }

    public static class SaveFloorData implements BaseColumns {
        public static final String TABLE_NAME = "floor_detail";
        public static final String PROPERTY_ID = "property_id";
        public static final String UTILITY_TYPE = "utility_type";
        public static final String TRACKING_DATE = "tracking_date";
        public static final String CURRENT_USAGE = "current_usage";
        public static final String TOTAL_USAGE = "total_usage";
        public static final String UTILITY_PHOTO = "utility_photo";
        public static final String PURPOSE = "purpose";
        public static final String LOCATION_ID = "location_id";
        public static final String DATA_TYPE = "data_type";
    }


}
