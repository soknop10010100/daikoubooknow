package com.eazy.daikou.request_data.request.book_now_ws

import android.content.Context
import android.content.res.Resources
import android.graphics.drawable.Drawable
import android.view.View
import android.widget.ProgressBar
import androidx.core.content.res.ResourcesCompat
import com.eazy.daikou.R
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.booking_hotel.*
import com.eazy.daikou.model.home.HomeViewModel
import com.eazy.daikou.model.home.MainItemHomeModel
import com.eazy.daikou.model.home.SubItemHomeModel

class RequestHashMapData {
    companion object {
        // == Add List for category ==
        fun addCategoryItemList(resources : Resources) : ArrayList<SubItemHomeModel>{
            val subHomeModelCategoryList: ArrayList<SubItemHomeModel> = ArrayList()
            for (i in 0 until 5) {
                var name =""
                var id = ""
                var isClicked = false
                var drawable = ResourcesCompat.getDrawable(resources, R.drawable.home_ic_accessibility, null)
                when (i) {
                    0 -> {
                        name = resources.getString(R.string.hotels)
                        id = "hotel"
                        isClicked = true
                        drawable = ResourcesCompat.getDrawable(resources, R.drawable.hotel_home_ic_hotel, null)
                    }
                    1 -> {
                        name = resources.getString(R.string.space_)
                        id = "space"
                        isClicked = false
                        drawable = ResourcesCompat.getDrawable(resources, R.drawable.hotel_home_ic_space, null)
                    }
                    2 ->{
                        name = resources.getString(R.string.activity)
                        id = "activity"
                        isClicked = false
                        drawable = ResourcesCompat.getDrawable(resources, R.drawable.hotel_home_ic_activity, null)
                    }
                    3 ->{
                        name = resources.getString(R.string.tour)
                        id = "tour"
                        isClicked = false
                        drawable = ResourcesCompat.getDrawable(resources, R.drawable.hotel_home_ic_tour, null)
                    }
                    4 ->{
                        name = resources.getString(R.string.event)
                        id = "event"
                        isClicked = false
                        drawable = ResourcesCompat.getDrawable(resources, R.drawable.hotel_home_ic_event, null)
                    }

                }
                subHomeModelCategoryList.add(getItemCategoryBooking(name, id, isClicked, drawable!!, "booking_category"))
            }
            return subHomeModelCategoryList
        }

        fun categoryTitle(context: Context?): HashMap<String, String> {
            val hashMap = HashMap<String, String>()
            hashMap["hotel"] = Utils.getText(context, R.string.hotels)
            hashMap["space"] = Utils.getText(context, R.string.space_)
            hashMap["activity"] = Utils.getText(context, R.string.activity)
            hashMap["tour"] = Utils.getText(context, R.string.tour)
            hashMap["event"] = Utils.getText(context, R.string.event)
            return hashMap
        }

        fun getItemCategoryBooking(name: String, id: String, isClick : Boolean, drawable: Drawable, action: String) : SubItemHomeModel{
            val subItemHomeModel = SubItemHomeModel()
            subItemHomeModel.action = action
            subItemHomeModel.name = name
            subItemHomeModel.id = id
            subItemHomeModel.isClickCategory = isClick
            subItemHomeModel.drawable = drawable
            return subItemHomeModel
        }

        // == Payment ==
        fun getListOnlinePayment(context: Context): ArrayList<HomeViewModel> {
            val homeViewModels: ArrayList<HomeViewModel> =ArrayList()
            homeViewModels.add(
                HomeViewModel(
                    "aba_pay",
                    ResourcesCompat.getDrawable(context.resources, R.drawable.aba_logo, null),
                    "ABA Pay"
                )
            )
            homeViewModels.add(
                HomeViewModel(
                    "acleda_pay",
                    ResourcesCompat.getDrawable(context.resources, R.drawable.acleda_logo, null),
                    "ACLEDA Bank"
                )
            )
            homeViewModels.add(
                HomeViewModel(
                    "emoney_pay",
                    ResourcesCompat.getDrawable(
                        context.resources,
                        R.drawable.e_money_logo,
                        null
                    ),
                    "eMoney"
                )
            )
            homeViewModels.add(
                HomeViewModel(
                    "kess_pay",
                    ResourcesCompat.getDrawable(context.resources, R.drawable.kesspay_logo, null),
                    "Kess Pay"
                )
            )
            homeViewModels.add(
                HomeViewModel(
                    "sathapana_pay",
                    ResourcesCompat.getDrawable(
                        context.resources,
                        R.drawable.pay_sathapana_logo,
                        null
                    ),
                    "Sathapana"
                )
            )
            homeViewModels.add(
                HomeViewModel(
                    "visa_card_pay",
                    ResourcesCompat.getDrawable(context.resources, R.drawable.visa_master_card_logo, null),
                    "Visa Card Pay"
                )
            )
            homeViewModels.add(
                HomeViewModel(
                    "wing_pay",
                    ResourcesCompat.getDrawable(
                        context.resources,
                        R.drawable.wing_bank_logo,
                        null
                    ),
                    "Wing Bank"
                )
            )
            homeViewModels.add(
                HomeViewModel(
                    "true_money",
                    ResourcesCompat.getDrawable(context.resources, R.drawable.pay_truemoneywallet_logo, null),
                    "TrueMoney Wallet"
                )
            )

            homeViewModels.add(
                HomeViewModel(
                    "u_pay",
                    ResourcesCompat.getDrawable(context.resources, R.drawable.u_pay_logo, null),
                    "U-Pay"
                )
            )

            return homeViewModels
        }

        // === Request Item List ===

        var hotelHomePageHashMap : HashMap<String, Any> = HashMap()
        var locationHotelModelList: ArrayList<LocationHotelModel> = ArrayList()
        var subHotelList: ArrayList<SubItemHomeModel> = ArrayList()
        //Hotel List Booking
        fun hashMapHomePageData(currentPage: Int, category: String, userId: String, coord_lat : String, coord_long : String): HashMap<String, Any> {
            val hashMap: HashMap<String, Any> = HashMap()
            hashMap["page"] = currentPage
            hashMap["limit"] = 10
            hashMap["category"] = category
            hashMap["coord_lat"] = coord_lat
            hashMap["coord_long"] = coord_long
            hashMap["user_id"] = userId
            return hashMap
        }
        fun hashMapData(currentPage: Int, category: String, locationId: String, userEasyId : String): HashMap<String, Any> {
            val hashMap: HashMap<String, Any> = HashMap()
            hashMap["page"] = currentPage
            hashMap["limit"] = 10
            hashMap["location_id"] = locationId
            hashMap["category"] = category
            hashMap["user_id"] = userEasyId
            return hashMap
        }
        //Hotel List Booking History
        fun hashMapHistoryData(currentPage: Int, hotelUserId: String): HashMap<String, Any> {
            val hashMap: HashMap<String, Any> = HashMap()
            hashMap["page"] = currentPage
            hashMap["limit"] = 10
            hashMap["hotel_user_id"] = hotelUserId
            return hashMap
        }

        //Edit Status Hotel Vendor
        fun editStatusHotelVendor(hotelId: String, status : String, hotelUserId: String, action: String, category: String, isRoom : Boolean): HashMap<String, Any> {
            val hashMap: HashMap<String, Any> = HashMap()
            if (isRoom) hashMap["room_id"] = hotelId
            else hashMap["hotel_id"] = hotelId

            hashMap["id"] = hotelId     // Wishlist
            hashMap["status"] = status
            if (action != "") { //edit wishlist favourite hotel
                hashMap["user_id"] = hotelUserId
                hashMap["action"] = action
                hashMap["category"] = category
            }

            return hashMap
        }


        //Store data home page hotel booking
        fun storeHotelItemData(putHashMapDate : HashMap<String, Any>): HashMap<String, Any> {
            hotelHomePageHashMap = putHashMapDate
            return hotelHomePageHashMap
        }
        //Get data home page hotel from store
        fun getStoreHotelData(): HashMap<String, Any> {
            return hotelHomePageHashMap
        }

        //Store data location hotel
        fun storeLocationHotelData(locationList : ArrayList<LocationHotelModel>): ArrayList<LocationHotelModel> {
            locationHotelModelList =locationList
            return locationHotelModelList
        }
        //Get data home page hotel from store
        fun getStoreLocationHotelData(): ArrayList<LocationHotelModel> {
            return locationHotelModelList
        }

        //Store hotel home page
        fun storeHotelHomepageData(locationList : ArrayList<SubItemHomeModel>): ArrayList<SubItemHomeModel> {
            subHotelList = locationList
            return subHotelList
        }
        //Get data home page hotel from store
        fun getStoreHotelHomepageData(): ArrayList<SubItemHomeModel> {
            return subHotelList
        }

        //Request date active user
        fun hashMapUserActiveAccount(action: String, userId: String): HashMap<String, Any> {
            val hashMap : HashMap<String, Any> = HashMap()
            hashMap["action"] = action
            hashMap["user_id"] = userId
            return hashMap
        }

        //Edit status wish list hotel
        fun editStatusHotelWs(context: Context, progressBar : ProgressBar, hotelId : String, isWishList : Boolean, status : String, eazyhotelUserId : String, category: String, onCallBack : OnSuccessEditStatusListener){
            progressBar.visibility = View.VISIBLE
            BookingHotelWS().doEditStatus(context, "wishlist", editStatusHotelVendor(hotelId, status, eazyhotelUserId, if (isWishList) "remove" else "add", category,false), object : BookingHotelWS.OnCallBackHotelVendorListener{
                override fun onSuccessShowWishlistHotel(locationHotelModel: ArrayList<MainItemHomeModel>) {}

                override fun onSuccessHotelOfVendor(hotelHistoryList: ArrayList<HotelMyVendorModel>) {}

                override fun onSuccessRoomHotel(hotelHistory: ArrayList<HotelMyRoomOfHotelVendorModel>) {}

                override fun onSuccessEditStatus(successMsg: String) {
                    progressBar.visibility = View.GONE
                    // Utils.customToastMsgError(context, successMsg, true)
                    onCallBack.onSuccess(successMsg)
                }

                override fun onFailed(message: String) {
                    progressBar.visibility = View.GONE
                    Utils.customToastMsgError(context, message, false)
                }
            })
        }

        interface OnSuccessEditStatusListener{
            fun onSuccess(msg : String)
        }

        //Travel Talk Like / Unlike
        fun requestServiceLikeUnLike(context: Context, progressBar: ProgressBar, action: String, hashMap: HashMap<String, Any>, onCallBackListener : OnSuccessEditStatusListener){
            BookingHotelWS().createPostTravelTalkWs(context,action, hashMap, object : BookingHotelWS.OnCallBackTravelTalkListener{
                override fun onSuccessTravelTalkList(travelTalkModelList: ArrayList<HotelTravelTalkModel>) {}
                override fun onSuccessTravelTalkReplyDetail(travelTalkModelList: TravelTalkReplyDetail) {}

                override fun onCreatePostSuccess(message: String) {
                    progressBar.visibility = View.GONE
                    onCallBackListener.onSuccess(message)
                    Utils.logDebug("debougkelelogie", message)
                }

                override fun onFailed(message: String) {
                    progressBar.visibility = View.GONE
                    Utils.logDebug("debougkelelogie", message)
                }
            })
        }

        // Body like unlike
        fun requestDataLike(userId: String, type : String, postId : String, action: String) : HashMap<String, Any>{
            val hashMap = HashMap<String, Any>()
            hashMap["user_id"] = userId
            hashMap["type"] = type
            hashMap["id"] = postId
            hashMap["action"] = action
            return hashMap
        }

        // Body like unlike
        fun requestDataReply(userId: String, type : String, threadId : String, replySubId : String, descriptionReply : String, urlImage : String) : HashMap<String, Any>{
            val hashMap = HashMap<String, Any>()
            hashMap["type"] = type
            hashMap["thread_id"] = threadId
            hashMap["user_id"] = userId
            hashMap["description"] = descriptionReply
            if (urlImage != ""){
                hashMap["image"] = urlImage
            }
            if (type == "reply"){
                hashMap["reply_id"] = replySubId
            }
            return hashMap
        }

        fun requestDataShare(type: String, id: String, userId: String) : HashMap<String, Any> {
            val hashMap = HashMap<String, Any>()
            hashMap["type"] = type
            hashMap["id"] = id
            hashMap["user_id"] = userId
            return hashMap
        }

        //Check Available
        fun checkAvailableItem(context: Context, hashMap: HashMap<String, Any>, progressBar: ProgressBar, callBack : BookingHotelWS.OnCallBackCheckAvailableListener){
            BookingHotelWS().checkAvailableBooking(context, hashMap, object : BookingHotelWS.OnCallBackCheckAvailableListener{
                override fun onSuccess(price: Double, message: Boolean) {
                    progressBar.visibility = View.GONE
                    callBack.onSuccess(price, message)
                }

                override fun onFailed(message: String) {
                    progressBar.visibility = View.GONE
                    Utils.customToastMsgError(context, message, false)
                }

            })
        }

        fun requestDataCheckAvailableItem(action: String, id: String, startDate: String, endDate: String) : HashMap<String, Any> {
            val hashMap = HashMap<String, Any>()
            hashMap["category"] = action
            hashMap["id"] = id
            hashMap["start_date"] = startDate
            hashMap["end_date"] = endDate
            return hashMap
        }
    }
}