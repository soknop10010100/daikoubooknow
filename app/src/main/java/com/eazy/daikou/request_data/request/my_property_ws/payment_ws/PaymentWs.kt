package com.eazy.daikou.request_data.request.my_property_ws.payment_ws

import android.content.Context
import com.eazy.daikou.repository_ws.CustomCallback
import com.eazy.daikou.repository_ws.CustomResponseListener
import com.eazy.daikou.repository_ws.RetrofitConnector
import com.eazy.daikou.request_data.request.book_now_ws.GenerateRespondWs
import com.eazy.daikou.model.my_property.payment_invoice.PaymentInvoiceModel
import com.google.gson.JsonObject

class PaymentWs {

    fun getListPaymentWs(context: Context, limit: Int, page: Int, scope : String, propertyId : String, onCalBackListener : CallBackListener) {
        val serviceApi = RetrofitConnector.createService(context)
        val res = serviceApi.getPaymentInvoice(limit, page, scope, propertyId)
        res.enqueue(CustomCallback(context, object : CustomResponseListener {
            override fun onSuccess(resObj: JsonObject) {
                if (resObj.has("data")) {
                    onCalBackListener.onSuccess(GenerateRespondWs.onGenerateRespondDataListWs(resObj, PaymentInvoiceModel::class.java))
                } else {
                    onCalBackListener.onFailed(resObj["msg"].asString)
                }
            }
            override fun onError(error: String, resCode: Int) {
                onCalBackListener.onFailed(error)
            }
        }))
    }

    interface CallBackListener {
        fun onSuccess(paymentList : ArrayList<PaymentInvoiceModel>)
        fun onFailed(error: String)
    }

}