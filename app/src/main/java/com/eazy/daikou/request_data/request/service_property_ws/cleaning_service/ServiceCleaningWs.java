package com.eazy.daikou.request_data.request.service_property_ws.cleaning_service;

import android.content.Context;
import androidx.annotation.NonNull;

import com.eazy.daikou.repository_ws.RetrofitConnector;
import com.eazy.daikou.repository_ws.CustomCallback;
import com.eazy.daikou.repository_ws.CustomResponseListener;
import com.eazy.daikou.repository_ws.ServiceApi;
import com.eazy.daikou.model.my_property.service_property_employee.AllService;
import com.eazy.daikou.model.my_property.service_provider.ServiceTypeCleaning;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ServiceCleaningWs {

    //getServiceName
    public void getServiceNameCleaning(Context context, int page, int limit, String unitId, String propertyId, String serviceTerm, OnLoadRequestCallBack loadRequestCallBack) {
        List<ServiceTypeCleaning> listService = new ArrayList<>();
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = serviceApi.getServiceType(page, limit, propertyId, unitId, serviceTerm);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data")) {
                    JsonArray jsonArray = resObj.get("data").getAsJsonArray();
                    for (int i = 0; i < jsonArray.size(); i++) {
                        JsonObject item = jsonArray.get(i).getAsJsonObject();
                        ServiceTypeCleaning serviceModel = new Gson().fromJson(item.toString(), ServiceTypeCleaning.class);
                        listService.add(serviceModel);
                    }
                    loadRequestCallBack.onLoadSuccessFull(listService);
                }

            }

            @Override
            public void onError(String error, int resCode) {
                loadRequestCallBack.onLoadFail(error);
            }
        }));
    }

    //getServiceNameType
    public void getServiceName(Context context, int page, String unitId, String propertyId,String serviceType, String serviceTerm, OnLoadRequestAllServiceCallListener loadRequestCallBack) {
        List<AllService> listService = new ArrayList<>();
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = serviceApi.getServiceNameType(page, 10, propertyId, unitId, serviceType,serviceTerm);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data")) {
                    JsonArray jsonArray = resObj.get("data").getAsJsonArray();
                    for (int i = 0; i < jsonArray.size(); i++) {
                        JsonObject item = jsonArray.get(i).getAsJsonObject();
                        AllService serviceModel = new Gson().fromJson(item.toString(), AllService.class);
                        listService.add(serviceModel);
                    }
                    loadRequestCallBack.onLoadSuccessFull(listService);
                }

            }

            @Override
            public void onError(String error, int resCode) {
                loadRequestCallBack.onLoadFail(error);
            }
        }));
    }

    //create service cleaning

    public void createServiceCleaning(Context context, HashMap<String, Object> hashMap, OnLoadRequestCreateCallBack createCallBack) {
        ServiceApi api = RetrofitConnector.createService(context);
        Call<JsonElement> res = api.createCleaningService(hashMap);
        res.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(@NonNull Call<JsonElement> call, @NonNull Response<JsonElement> response) {
                if (response.isSuccessful()) {
                    assert response.body() != null;
                    JsonObject object = response.body().getAsJsonObject();
                    if (object.has("success")) {
                        boolean isSuccess = object.get("success").getAsBoolean();
                        if (isSuccess) {
                            if (object.has("msg")) {
                                String message = object.get("msg").getAsString();
                                createCallBack.onLoadSuccessFull(message);
                            }
                        } else {
                            if (object.has("msg")) {
                                String message = object.get("msg").getAsString();
                                createCallBack.onLoadFail(message);
                            }
                        }
                    } else {
                        createCallBack.onLoadFail("Something went wrong.");
                    }
                }else {
                    createCallBack.onLoadFail("Something went wrong.");
                }

            }

            @Override
            public void onFailure(@NonNull Call<JsonElement> call, @NonNull Throwable t) {
                createCallBack.onLoadFail(t.getMessage());
            }
        });
    }

    public interface OnLoadRequestCallBack {
        void onLoadSuccessFull(List<ServiceTypeCleaning> listService);
        void onLoadFail(String message);
    }

    public interface OnLoadRequestAllServiceCallListener {
        void onLoadSuccessFull(List<AllService> listService);
        void onLoadFail(String message);
    }

    public interface OnLoadRequestCreateCallBack {
        void onLoadSuccessFull(String message);
        void onLoadFail(String message);
    }
}
