package com.eazy.daikou.request_data.request.front_desk_ws

import android.content.Context
import com.eazy.daikou.repository_ws.CustomCallback
import com.eazy.daikou.repository_ws.CustomResponseListener
import com.eazy.daikou.repository_ws.RetrofitConnector
import com.eazy.daikou.model.front_desk.DetailFrontDeskModel
import com.eazy.daikou.model.front_desk.SearchFrontDeskModel
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.google.gson.JsonObject

class FrontDeskWS {
    fun getAssignPickerFrontDeskWS(context: Context, hashMap: HashMap<String, Any?>, callBackAssignPicker : CallBackFrontDeskCreateListener ){
        val serviceApi = RetrofitConnector.createService(context)
        val res = serviceApi.getAssignPickerComplaint(hashMap)
        res.enqueue(CustomCallback(context, object : CustomResponseListener{
            override fun onSuccess(resObj: JsonObject?) {
                if (resObj != null){
                    if (resObj.has("success")){
                        val isSuccess = resObj.get("success").asBoolean
                        if (isSuccess){
                            if (resObj.has("msg")){
                                callBackAssignPicker.onSuccessFul(resObj["msg"].asString)
                            }
                        } else {
                            callBackAssignPicker.onFailed(resObj["msg"].asString)
                        }
                    }
                }
            }
            override fun onError(error: String, resCode: Int) {
                callBackAssignPicker.onFailed(error)
            }
        }))
    }

    fun searchFrontDeskWS(context: Context, page: Int, limit: Int , accountId : String, keySearch : String, callBackSearchFrontDesk : CallBackSearchFrontDeskListener){
        val serviceApi = RetrofitConnector.createService(context)
        val res = serviceApi.searchFrontDesk(page, limit, accountId, keySearch)
        res.enqueue(CustomCallback(context, object :  CustomResponseListener{
            override fun onSuccess(resObj: JsonObject?) {
                if (resObj != null){
                    if (resObj.has("data")){
                        val listSearch : ArrayList<SearchFrontDeskModel> = ArrayList()
                        val data = resObj["data"].asJsonArray
                        for (i in 0 until data.size()){
                            val item : JsonElement = data[i].asJsonObject
                            val search = Gson().fromJson(item, SearchFrontDeskModel::class.java)
                            listSearch.add(search)
                        }
                        callBackSearchFrontDesk.onSuccessFul(listSearch)
                    } else{
                        callBackSearchFrontDesk.onFailed(resObj["msg"].asString)
                    }
                }
            }

            override fun onError(error: String, resCode: Int) {
                callBackSearchFrontDesk.onFailed(error)
            }

        }))
    }

    fun getDetailFrontDeskWS(context: Context, id : String, callBackDetail : CallBackDetailFrontDesk){
        val serviceApi = RetrofitConnector.createService(context)
        val res = serviceApi.detailFrontDesk(id)
        res.enqueue(CustomCallback(context, object : CustomResponseListener{
            override fun onSuccess(resObj: JsonObject?) {
                if (resObj != null){
                    if (resObj.has("data")&& resObj.isJsonObject){
                        val data = resObj.getAsJsonObject("data")
                        val detailFrontDesk = Gson().fromJson(data, DetailFrontDeskModel::class.java)
                        callBackDetail.onSuccessful(detailFrontDesk)
                    } else {
                        callBackDetail.onFailed(resObj["msg"].asString)
                    }
                }
            }

            override fun onError(error: String, resCode: Int) {
                callBackDetail.onFailed(error)
            }
        }))
    }

    interface CallBackFrontDeskCreateListener{
        fun onSuccessFul(message : String)
        fun onFailed(mess : String)
    }
    interface CallBackSearchFrontDeskListener{
        fun onSuccessFul( listSearch : ArrayList<SearchFrontDeskModel>)
        fun onFailed(mess : String)
    }
    interface CallBackDetailFrontDesk{
        fun onSuccessful( detail : DetailFrontDeskModel)
        fun onFailed(error : String)
    }
}