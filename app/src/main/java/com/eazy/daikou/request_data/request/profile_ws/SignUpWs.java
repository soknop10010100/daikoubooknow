package com.eazy.daikou.request_data.request.profile_ws;

import static com.eazy.daikou.helper.Utils.logDebug;

import android.util.Log;

import androidx.annotation.NonNull;

import com.eazy.daikou.repository_ws.RetrofitGenerator;
import com.eazy.daikou.repository_ws.ServiceApi;
import com.eazy.daikou.helper.Utils;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpWs {

    public void signUp(HashMap<String, Object> user, boolean isBookNow, SignUpInterface callback){
        ServiceApi serviceApi = RetrofitGenerator.createCategoryService(isBookNow);
        Call<JsonObject> res = isBookNow ? serviceApi.doSignUpAccountHotel(user) : serviceApi.signUp(user);
        res.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if(response.isSuccessful() && response.body() != null){
                    JsonObject jsonObject = response.body();
                    if(jsonObject.has("success")){
                        boolean isSuccess = jsonObject.get("success").getAsBoolean();
                        String mess="";
                        if(jsonObject.has("msg")){
                            mess = jsonObject.get("msg").getAsString();
                        }
                        if(isSuccess){
                            callback.onSuccess(mess, "", "");
                        }else {
                            callback.onFailed(mess);
                        }
                    } else {
                        callback.onFailed(jsonObject.get("msg").getAsString());
                    }
                } else {
                    if (response.errorBody() != null) {
                        ResponseBody errorString = response.errorBody();
                        logDebug("erorr",response.errorBody().toString());
                        String message = "";
                        try {
                            String json = errorString.string();
                            logDebug("dfkj",errorString.toString());
                            logDebug("error", json);
                            JSONObject jsonOb = new JSONObject(json);
                            if(jsonOb.has("msg")){
                                message = jsonOb.getString("msg");
                            }
                            callback.onFailed(message);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("jfgdf"+e.getClass().getName(), e.getMessage() + "");
                            callback.onError(message,response.code());
                        } catch (IOException e2) {
                            Log.e(e2.getClass().getName(), e2.getMessage() + "");
                            callback.onError(message,response.code());
                        }
                    }
                }

            }

            @Override
            public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
                callback.onFailed(t.getMessage());
            }
        });

    }

    public void sendVerifyPinCode( HashMap<String, Object> user, boolean isBookNow, SignUpInterface callback){
        ServiceApi serviceApi = RetrofitGenerator.createCategoryService(isBookNow);
        Call<JsonObject> res = isBookNow ? serviceApi.getVerifyCodeHotel(user) : serviceApi.sendPin(user);     //Check Book Now
        Utils.logDebug("jeeeeeeeeeeeeeeeeee", Utils.getStringGson(user));
        res.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if(response.isSuccessful() && response.body() != null){
                    JsonObject jsonObject = response.body();
                    if(jsonObject.has("success")){
                        boolean isSuccess = jsonObject.get("success").getAsBoolean();
                        String mess="";
                        if(jsonObject.has("msg")){
                            mess = jsonObject.get("msg").getAsString();
                        }
                        if(isSuccess){
                            String pinCode = "", userId = "";
                            //Book now have pin code, not yet daikou
                            if (jsonObject.has("data") && !jsonObject.get("data").isJsonNull()) {
                                JsonElement jsonElement = jsonObject.get("data").getAsJsonObject();
                                if (!jsonElement.isJsonNull() && !jsonElement.getAsJsonObject().get("verify_code").isJsonNull()) {
                                    pinCode = jsonElement.getAsJsonObject().get("verify_code").getAsString();
                                }
                                if (!jsonElement.isJsonNull() && !jsonElement.getAsJsonObject().get("eazyhotel_user_id").isJsonNull()) {
                                    userId = jsonElement.getAsJsonObject().get("eazyhotel_user_id").getAsString();
                                }
                            }

                            callback.onSuccess(mess, pinCode, userId);
                        } else {
                            callback.onFailed(mess);
                        }
                    } else {
                        callback.onFailed(jsonObject.get("msg").getAsString());
                    }
                } else {
                    if (response.errorBody() != null) {
                        ResponseBody errorString = response.errorBody();
                        logDebug("erorr",response.errorBody().toString());
                        String message = "";
                        try {
                            String json = errorString.string();
                            logDebug("dfkj",errorString.toString());
                            logDebug("error", json);
                            JSONObject jsonOb = new JSONObject(json);
                            if(jsonOb.has("msg")){
                                message = jsonOb.getString("msg");
                            }
                            callback.onFailed(message);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("jfgdf"+e.getClass().getName(), e.getMessage() + "");
                            callback.onError(e.getMessage(),response.code());
                        } catch (IOException e2) {
                            Log.e(e2.getClass().getName(), e2.getMessage() + "");
                            callback.onError(e2.getMessage(),response.code());
                        }
                    }
                }

            }

            @Override
            public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
                callback.onFailed(t.getMessage());
            }
        });


    }

    public void validateEmail(HashMap<String, Object> hashMap, boolean isBookNow, SignUpInterface callback){
        ServiceApi serviceApi = RetrofitGenerator.createCategoryService(isBookNow);
        Call<JsonObject> res = isBookNow ? serviceApi.validatePhoneEmailExist(hashMap) : serviceApi.validateEmail(hashMap);     //Check Book Now

        res.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if(response.isSuccessful()  && response.body() != null){
                    JsonObject jsonObject = response.body();
                    if(jsonObject.has("success")){
                        boolean isSuccess = jsonObject.get("success").getAsBoolean();
                        String mess="";
                        if(isSuccess){
                            callback.onSuccess(mess, "", "");
                        }else {
                            if(jsonObject.has("msg")){
                                mess = jsonObject.get("msg").getAsString();
                            }
                            callback.onFailed(mess);
                        }
                    } else {
                        callback.onFailed(jsonObject.get("msg").getAsString());
                    }
                } else {
                    if (response.errorBody() != null) {
                        ResponseBody errorString = response.errorBody();
                        logDebug("erorr",response.errorBody().toString());
                        String message = "";
                        try {
                            String json = errorString.string();
                            logDebug("dfkj",errorString.toString());
                            logDebug("error", json);
                            JSONObject jsonOb = new JSONObject(json);
                            if(jsonOb.has("msg")){
                                message = jsonOb.getString("msg");
                            }
                            callback.onFailed(message);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("jfgdf"+e.getClass().getName(), e.getMessage() + "");
                            callback.onError(message,response.code());
                        } catch (IOException e2) {
                            Log.e(e2.getClass().getName(), e2.getMessage() + "");
                            callback.onError(message,response.code());
                        }
                    }
                }

            }

            @Override
            public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
                callback.onFailed(t.getMessage());
            }
        });

    }

    public interface SignUpInterface{
        void onSuccess(String message, String pin_code, String user_id);
        void onFailed(String mess);
        void onError(String mess,int code);
    }
}
