package com.eazy.daikou.request_data.request.hr_ws

import android.content.Context
import com.eazy.daikou.repository_ws.*
//import com.eazy.daikou.api.ServiceMenuApi
import com.eazy.daikou.repository_ws.CustomCallback
import com.eazy.daikou.repository_ws.CustomResponseListener
import com.eazy.daikou.model.hr.*
import com.google.gson.Gson
import com.google.gson.JsonObject
import java.util.ArrayList

class MyProfileHRWs {


    fun getMyProfileEmployeeHr(context: Context, user_id:String, onCallBackMyProfileListener: OnCallBackMyProfileListener) {

        val hrUser = HRUser()
        val serviceMenuApi = RetrofitGenerator.createServiceWithHR(context)
        val res = serviceMenuApi.getMyProfileHRM(user_id)
        res.enqueue(CustomCallback(context, object : CustomResponseListener{
            override fun onSuccess(resObj: JsonObject) {
                if (resObj.has("data")){
                    val obj = resObj.getAsJsonObject("data")
                    if(obj.has("profile_detail") && !obj.get("profile_detail").isJsonNull){
                        val profile = Gson().fromJson(obj.get("profile_detail").asJsonObject, ProfileDetail::class.java)
                        hrUser.profile_detail = profile
                    }
                    if (obj.has("bank_detail") && !obj.get("bank_detail").isJsonNull){
                        val bankDetail = Gson().fromJson(obj.get("bank_detail").asJsonObject,BankDetail::class.java)
                        hrUser.bank_detail = bankDetail
                    }
                    if(obj.has("qualification_details")){
                        val listQualification = ArrayList<Qualification>()
                        val jsonArray = obj.getAsJsonArray("qualification_details")
                        for (i in 0 until jsonArray.size()) {
                            val item = jsonArray[i].asJsonObject
                            val qualification = Gson().fromJson(item.toString(), Qualification::class.java)
                            listQualification.add(qualification)
                            hrUser.qualification_details = listQualification
                        }
                    }
                    if(obj.has("experience_details")){
                        val listExperience = ArrayList<Experience>()
                        val jsonArray = obj.getAsJsonArray("experience_details")
                        for (i in 0 until jsonArray.size()) {
                            val item = jsonArray[i].asJsonObject
                            val experience = Gson().fromJson(item.toString(), Experience::class.java)
                            listExperience.add(experience)
                            hrUser.experience = listExperience
                        }
                    }
                    if(obj.has("relationship_detail")){
                        val listRelationship = ArrayList<Relationship>()
                        val jsonArray = obj.getAsJsonArray("relationship_detail")
                        for (i in 0 until jsonArray.size()) {
                            val item = jsonArray[i].asJsonObject
                            val relationship = Gson().fromJson(item.toString(), Relationship::class.java)
                            listRelationship.add(relationship)
                            hrUser.relationship = listRelationship
                        }
                    }
                    if(obj.has("document_detail")){
                        val listDocument = ArrayList<Document>()
                        val jsonArray = obj.getAsJsonArray("document_detail")
                        for (i in 0 until jsonArray.size()) {
                            val item = jsonArray[i].asJsonObject
                            val document_detail = Gson().fromJson(item.toString(), Document::class.java)
                            listDocument.add(document_detail)
                            hrUser.document_detail = listDocument
                        }
                    }

                    onCallBackMyProfileListener.onLoadListSuccessFull(hrUser)

                } else {
                    onCallBackMyProfileListener.onLoadFail(resObj.get("msg").asString)
                }
            }
            override fun onError(error: String, resCode: Int) {
                onCallBackMyProfileListener.onLoadFail(error)
            }
        }))
    }


    interface OnCallBackMyProfileListener{
        fun onLoadListSuccessFull(hrUser : HRUser)
        fun onLoadFail(message:String)

    }
}