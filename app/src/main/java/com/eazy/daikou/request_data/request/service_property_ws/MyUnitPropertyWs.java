package com.eazy.daikou.request_data.request.service_property_ws;

import android.content.Context;

import com.eazy.daikou.repository_ws.RetrofitConnector;
import com.eazy.daikou.repository_ws.CustomCallback;
import com.eazy.daikou.repository_ws.CustomResponseListener;
import com.eazy.daikou.repository_ws.ServiceApi;
import com.eazy.daikou.request_data.static_data.StaticUtilsKey;
import com.eazy.daikou.model.my_property.payment_schedule_my_unit.OperationPaymentInfoModel;
import com.eazy.daikou.model.my_property.payment_schedule_my_unit.OperationUnitNoModel;
import com.eazy.daikou.model.my_property.payment_schedule_my_unit.PaymentSchedulesModel;
import com.eazy.daikou.model.utillity_tracking.model.UnitImgModel;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

public class MyUnitPropertyWs {

    public void getServiceMyUnitV2(Context context, String accountId, String property_id, int page, int limit, String user_login_id, MyUnitCallBackListener unitPropertyListener){
        Call<JsonElement> res = RetrofitConnector.createService(context).getMyUnitList(accountId, property_id, page,limit, user_login_id);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data")){
                    ArrayList<UnitImgModel> unitImgModelList = new ArrayList<>();
                    JsonArray data = resObj.getAsJsonArray("data");
                    for (int i = 0; i < data.size() ; i++) {
                        JsonElement item = data.get(i).getAsJsonObject();
                        UnitImgModel unitImgModel = new Gson().fromJson(item.toString(), UnitImgModel.class);
                        unitImgModelList.add(unitImgModel);
                    }
                    unitPropertyListener.getServiceUnit(unitImgModelList);
                } else {
                    unitPropertyListener.failed(resObj.get("msg").getAsString());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                unitPropertyListener.failed(error);
            }
        }));
    }


    public void getUnitNoOperation(Context context, String userId, UnitPaymentScheduleListener unitPaymentSchedule){
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = serviceApi.getOperationUnitNo(userId);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data")){
                    if (!resObj.get("data").isJsonNull()){
                        OperationUnitNoModel paymentModel = new Gson().fromJson(resObj.get("data").getAsJsonObject(),OperationUnitNoModel.class);
                        unitPaymentSchedule.onSuccessUnitNo(paymentModel);
                    } else {
                        unitPaymentSchedule.onFailed("");
                    }
                } else {
                    unitPaymentSchedule.onFailed(resObj.get("msg").getAsString());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                unitPaymentSchedule.onFailed(error);
            }
        }));
    }

    public void getOperationPayment(Context context, String action, String userId, String unitId, UnitPaymentScheduleListener unitPaymentSchedule){
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = action.equals(StaticUtilsKey.action_operation_payment_lease) ? serviceApi.getOperationPaymentLeaseList(userId, unitId) : serviceApi.getOperationPaymentSaleList(userId, unitId);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data")){
                    List<OperationPaymentInfoModel> paymentSchedulesModelList = new ArrayList<>();
                    JsonArray dataPayment = resObj.getAsJsonArray("data").getAsJsonArray();
                    for (int i = 0; i < dataPayment.size(); i++){
                        JsonElement itemPayment = dataPayment.get(i).getAsJsonObject();
                        OperationPaymentInfoModel paymentModel = new Gson().fromJson(itemPayment.getAsJsonObject(),OperationPaymentInfoModel.class);
                        paymentSchedulesModelList.add(paymentModel);
                    }
                    unitPaymentSchedule.onSuccessOperationPayment(paymentSchedulesModelList);
                } else {
                    unitPaymentSchedule.onFailed(resObj.get("msg").getAsString());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                unitPaymentSchedule.onFailed(error);
            }
        }));
    }

    public interface MyUnitCallBackListener{
        void getServiceUnit(ArrayList<UnitImgModel> unitImgModels);
        void failed(String error);
    }

    public interface UnitPaymentScheduleListener{
        void onSuccessful(List<PaymentSchedulesModel> paymentSchedulesModelList);
        void onSuccessUnitNo(OperationUnitNoModel operationUnitNoModel);
        void onSuccessOperationPayment(List<OperationPaymentInfoModel> paymentSchedulesModelList);
        void onFailed(String error);
    }

}
