package com.eazy.daikou.request_data.request.book_now_ws

import android.content.Context
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.RetrofitGenerator
import com.eazy.daikou.request_data.request.profile_ws.LoginWs.OnRecives
import com.eazy.daikou.helper.InternetConnection
import com.eazy.daikou.model.profile.User
import com.google.gson.Gson
import com.google.gson.JsonObject
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException

class BookingHotelAccountWS {

    fun loginWs(context: Context, user: HashMap<String, Any>, onCallBackListener: OnRecives) {
        val serviceApi = RetrofitGenerator.createCategoryService(true)
        val res = serviceApi.loginHotel(user)
        res.enqueue(object : Callback<JsonObject?> {
            override fun onResponse(call: Call<JsonObject?>, response: Response<JsonObject?>) {
                if (response.isSuccessful && response.body() != null) {
                    val msg = response.body()!!["msg"].asString
                    val status = response.body()!!["success"].asString
                    if (status == "true") {
                        val jsonObject = response.body()!!.getAsJsonObject("data")
                        onCallBackListener.onSuccess(Gson().fromJson(jsonObject, User::class.java))
                    } else {
                        onCallBackListener.onWrong(msg)
                    }
                } else {
                    if (response.errorBody() != null) {
                        val errorString = response.errorBody()
                        try {
                            val json = errorString!!.string()
                            val jsonObject1 = JSONObject(json)
                            if (jsonObject1.has("msg")) {
                                val message = jsonObject1.getString("msg")
                                onCallBackListener.onWrong(message)
                            }
                        } catch (e: IOException) {
                            e.printStackTrace()
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                    }
                }
            }

            override fun onFailure(call: Call<JsonObject?>, t: Throwable) {
                if (!InternetConnection.checkInternetConnectionActivity(context)) {
                    onCallBackListener.onFailed(context.getString(R.string.please_check_your_internet_connection))
                } else {
                    onCallBackListener.onFailed(context.getString(R.string.something_went_wrong))
                }
            }
        })
    }

    fun loginFBGoogleWs(context: Context, action: String, user: HashMap<String, Any?>, onRegisterFbGoogleListener: OnRegisterFbGoogleListener) {
        val serviceApi = RetrofitGenerator.createServiceWithHotelBooking(context)
        val res = serviceApi.loginByFbGoogle(user)
        res.enqueue(object : Callback<JsonObject?> {
            override fun onResponse(call: Call<JsonObject?>, response: Response<JsonObject?>) {
                if (response.isSuccessful && response.body() != null) {
                    val msg = response.body()!!["msg"].asString
                    val status = response.body()!!["success"].asString
                    if (status == "true") {
                        if (!response.body()!!.getAsJsonObject("data").isJsonNull) {
                            val jsonObject = response.body()!!.getAsJsonObject("data")
                            onRegisterFbGoogleListener.onSuccess(
                                action, Gson().fromJson(jsonObject, User::class.java),
                                user
                            )
                        }
                    } else {
                        onRegisterFbGoogleListener.onFailed(msg)
                    }
                } else {
                    if (response.errorBody() != null) {
                        val errorString = response.errorBody()
                        try {
                            val json = errorString!!.string()
                            val jsonObject1 = JSONObject(json)
                            if (jsonObject1.has("msg")) {
                                val message = jsonObject1.getString("msg")
                                onRegisterFbGoogleListener.onFailed(message)
                            }
                        } catch (e: IOException) {
                            e.printStackTrace()
                            onRegisterFbGoogleListener.onFailed(e.message)
                        } catch (e: JSONException) {
                            e.printStackTrace()
                            onRegisterFbGoogleListener.onFailed(e.message)
                        }
                    }
                }
            }

            override fun onFailure(call: Call<JsonObject?>, t: Throwable) {
                if (!InternetConnection.checkInternetConnectionActivity(context)) {
                    onRegisterFbGoogleListener.onFailed(context.getString(R.string.please_check_your_internet_connection))
                } else {
                    onRegisterFbGoogleListener.onFailed(context.getString(R.string.something_went_wrong))
                }
            }
        })
    }

    interface OnRegisterFbGoogleListener {
        fun onSuccess(action: String?, user: User?, hashMap: HashMap<String, Any?>)
        fun onFailed(mess: String?)
    }

}