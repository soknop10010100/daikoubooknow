package com.eazy.daikou.request_data.request.book_quote_ws;

import android.content.Context;

import com.eazy.daikou.repository_ws.CustomCallback;
import com.eazy.daikou.repository_ws.CustomResponseListener;
import com.eazy.daikou.model.book.BookCategoryV2;
import com.eazy.daikou.model.book.BookSellAllV2;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.eazy.daikou.repository_ws.RetrofitConnector;
import com.eazy.daikou.repository_ws.ServiceApi;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

public class QuoteWs {

    public void getQuoteBookNew(Context context, int limit, int page , QuoteBookNewCallBackListener quoteBookNewCallBackListener){
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = serviceApi.quote_book(limit,page);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data")) {
                    JsonElement jsonElement = resObj.get("data").getAsJsonObject();
                    BookCategoryV2 quoteAndBookModelV2 = new Gson().fromJson(jsonElement, BookCategoryV2.class);
                    quoteBookNewCallBackListener.getQuoteBook(quoteAndBookModelV2);
                } else{
                    quoteBookNewCallBackListener.onFailed(resObj.get("msg").getAsString());
                }

            }
            @Override
            public void onError(String error, int resCode) {
                quoteBookNewCallBackListener.onFailed(error);
            }
        }));
    }
    
    public void getQuoteBookAll(Context context, int page, int limit,String item_type ,String category_id , QuoteBookAllCallBackListener quoteBookAllCallBackListener){
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = serviceApi.quote_book_all( page , limit ,item_type,category_id);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {

            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data")){
                    List<BookSellAllV2> list = new ArrayList<>();
                    JsonArray data = resObj.getAsJsonArray("data");
                    for (int i=0; i<data.size();i++) {
                        JsonElement item = data.get(i).getAsJsonObject();
                        BookSellAllV2 bookSellAllV2 = new Gson().fromJson(item, BookSellAllV2.class);
                        list.add(bookSellAllV2);
                    }
                    quoteBookAllCallBackListener.getQuoteBookAll(list);
                }
            }

            @Override
            public void onError(String error, int resCode) {
                quoteBookAllCallBackListener.onFailed(error);
            }
        }));
    }

    public interface QuoteBookNewCallBackListener{
        void getQuoteBook(BookCategoryV2 quoteAndBookModelV2);
        void onFailed(String error);
    }
    public interface QuoteBookAllCallBackListener{
        void getQuoteBookAll(List<BookSellAllV2> quotesList);
        void onFailed(String error);
    }
}
