package com.eazy.daikou.request_data.sql_database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DataBaseHelper extends SQLiteOpenHelper {


    private static final String CONTACT_TABLE = "CONTACT_TABLE";
    private static final String ID4 = "ID";
    private static final String ID3 = ID4;
    private static final String ID2 = ID3;
    private static final String ID1 = ID2;
    private static final String ID = ID1;
    private static final String FIRST_NAME = "FIRST_NAME";
    private static final String LAST_NAME = "LAST_NAME";
    private static final String EMAIL = "EMAIL";
    private static final String CHAT_TABLE = "CHAT_TABLE";
    private static final String CONTACT_ID = "CONTACT_" + ID2;
    private static final String ISSENDER = "ISSENDER";
    private static final String DATE = "DATE";
    private static final String MESSAGES = "MESSAGES";
    private static final String MESSAGES_TABLE = MESSAGES + "_TABLE";
    private static final String CHAT_ID = "CHAT_" + ID4;
    private static final String ACCOUNT_TABLE = "ACCOUNT_TABLE";
    private static final String ACC_EMAIL = "ACC_EMAIL";
    private static final String ACC_ID = "ACC_ID";
    private static final String IMAGE = "IMAGE";
    private static final String TYPE = "TYPE";
    private static final String IS_SEEN = "isSeen";
    private static final String AUDIO = "AUDIO";
    private static final String DURATION = "DURATION";
    private static final String USER_ID = "USER_ID";
    private static final String NAME = "";
    private static final String ID_NAME = "";
    private static final String ACCOUNT_SAMPLE = "ACCOUNT_SAMPLE";
    private final Context context;


    public DataBaseHelper(@Nullable Context context) {
        super(context, "contact.db", null, 1);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createAccountTable = "CREATE TABLE " + ACCOUNT_TABLE + "(" + ID4 + " INTEGER PRIMARY KEY, " + ACC_EMAIL + ")";
        String createContactTable = "CREATE TABLE " + CONTACT_TABLE + " (" + ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + FIRST_NAME + ", " + LAST_NAME + ", " + EMAIL + ", "+USER_ID+ " INTEGER, " + ACC_ID + " INTEGER , FOREIGN KEY (" + ACC_ID + ") REFERENCES " + ACCOUNT_TABLE + "(" + ID4 + ")" + ");";
        String createChatTable = "CREATE TABLE " + CHAT_TABLE + " (" + ID2 + " INTEGER PRIMARY KEY, " + CONTACT_ID + " INTEGER UNIQUE, FOREIGN KEY (" + CONTACT_ID + ") REFERENCES " + CONTACT_TABLE + "(" + ID + "))";
        String createMessageTable = "CREATE TABLE " + MESSAGES_TABLE + " (" + ID3 + " PRIMARY KEY, " + CHAT_ID + " INTEGER, " + MESSAGES + ", " + IMAGE + ", " + AUDIO + ", " + DURATION + ", " + ISSENDER + "," + DATE + ", " + TYPE + ", " + IS_SEEN + ", FOREIGN KEY (" + CHAT_ID + ") REFERENCES " + CHAT_TABLE + "(" + ID2 + "))";

        db.execSQL(createAccountTable);
        db.execSQL(createContactTable);
        db.execSQL(createChatTable);
        db.execSQL(createMessageTable);

        String createSampleTable = "CREATE TABLE " + ACCOUNT_SAMPLE + " (" + ID_NAME + " INTEGER PRIMARY KEY, " + NAME + "TEXT NOT NULL)" + ");";

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
    }

    public void addOneAcc(String acc, String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();

        cv.put(ACC_EMAIL, acc);
        cv.put(ID4, id);

        db.insert(ACCOUNT_TABLE, null, cv);
    }

    public boolean isAccountExist(String email) {
        String queryString = "SELECT COUNT(ID) FROM " + ACCOUNT_TABLE + " WHERE " + ACC_EMAIL + "= \"" + email + "\"";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(queryString, null);
        cursor.moveToFirst();
        return cursor.getInt(0) > 0;
    }

}
