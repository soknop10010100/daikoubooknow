package com.eazy.daikou.request_data.request.my_document

import android.content.Context
import com.eazy.daikou.repository_ws.RetrofitConnector
import com.eazy.daikou.repository_ws.CustomCallback
import com.eazy.daikou.repository_ws.CustomResponseListener
import com.eazy.daikou.model.my_document.MyDocumentItemModel
import com.eazy.daikou.model.my_document.PropertyDocument
import com.eazy.daikou.model.my_document.TotalNumberOfDocumentModel
import com.google.gson.Gson
import com.google.gson.JsonObject

class MyDocumentWs {

    fun getNumberOfDocument(context: Context, userId : String, callBackListener: NumberDocumentOnCallBackListener){
        val serviceApi = RetrofitConnector.createService(context)
        val res = serviceApi.getNumberOfDocument(userId)
        res.enqueue(CustomCallback(context, object : CustomResponseListener {
            override fun onSuccess(resObj: JsonObject) {
                if(resObj.has("success")){
                    val isSuccess = resObj.get("success").asBoolean
                    if(isSuccess){
                        if(resObj.has("data")){
                            val numberOfDocumentModelList : ArrayList<TotalNumberOfDocumentModel> = ArrayList()
                            val jsonArray = resObj["data"].asJsonArray
                            for (i in 0 until jsonArray.size()) {
                                val item = jsonArray[i].asJsonObject
                                val property = Gson().fromJson(item.toString(), TotalNumberOfDocumentModel::class.java)
                                numberOfDocumentModelList.add(property)
                            }
                            callBackListener.onLoadPropertySuccessFull(numberOfDocumentModelList)
                        }
                    }else{
                        callBackListener.onLoadPropertyFail(resObj["msg"].toString())
                    }
                }

            }

            override fun onError(error: String, resCode: Int) {
                callBackListener.onLoadPropertyFail(error)
            }
        }))
    }

    fun getListProperty(context: Context, userId:String, documentCategory: String, callBackListener:PropertyDocumentOnCallBackListener){
        val listProperty = java.util.ArrayList<PropertyDocument>()
        val serviceApi = RetrofitConnector.createService(context)
        val res = serviceApi.getListPropertyDocument(documentCategory, userId)
        res.enqueue(CustomCallback(context, object : CustomResponseListener {
            override fun onSuccess(resObj: JsonObject) {
                if(resObj.has("success")){
                    val isSuccess = resObj.get("success").asBoolean
                    if(isSuccess){
                        if(resObj.has("data")){
                            val jsonArray = resObj["data"].asJsonArray
                            for (i in 0 until jsonArray.size()) {
                                val item = jsonArray[i].asJsonObject
                                val property = Gson().fromJson(item.toString(), PropertyDocument::class.java)
                                listProperty.add(property)
                            }
                            callBackListener.onLoadPropertySuccessFull(listProperty)
                        }
                    }else{
                        callBackListener.onLoadPropertyFail(resObj["msg"].toString())
                    }
                }

            }

            override fun onError(error: String, resCode: Int) {
                callBackListener.onLoadPropertyFail(error)
            }
        }))
    }

    fun getListMyDocument(context: Context, page : Int,document_category:String, userId:String, propertyId: String, callBackListener:ListMyDocumentOnCallBackListener){
        val listMyDocument = java.util.ArrayList<MyDocumentItemModel>()
        val serviceApi = RetrofitConnector.createService(context)
        val res = serviceApi.getListMyDocument(page,10,document_category,propertyId, userId)
        res.enqueue(CustomCallback(context, object : CustomResponseListener {
            override fun onSuccess(resObj: JsonObject) {
                if(resObj.has("success")){
                    val isSuccess = resObj.get("success").asBoolean
                    if(isSuccess){
                        if(resObj.has("data")){
                            val jsonArray = resObj["data"].asJsonArray
                            for (i in 0 until jsonArray.size()) {
                                val item = jsonArray[i].asJsonObject
                                val myDocument = Gson().fromJson(item.toString(), MyDocumentItemModel::class.java)
                                listMyDocument.add(myDocument)
                            }
                            callBackListener.onLoadListMyDocumentSuccessFull(listMyDocument)
                        }
                    }else{
                        callBackListener.onLoadPMyDocumentFail(resObj["msg"].toString())
                    }
                }

            }

            override fun onError(error: String, resCode: Int) {
                callBackListener.onLoadPMyDocumentFail(error)
            }
        }))
    }

    interface NumberDocumentOnCallBackListener{
        fun onLoadPropertySuccessFull(listProperty : ArrayList<TotalNumberOfDocumentModel>)
        fun onLoadPropertyFail(message : String)
    }

    interface PropertyDocumentOnCallBackListener{
        fun onLoadPropertySuccessFull(listProperty : ArrayList<PropertyDocument>)
        fun onLoadPropertyFail(message : String)
    }


    interface ListMyDocumentOnCallBackListener{
        fun onLoadListMyDocumentSuccessFull(listMyDocument : ArrayList<MyDocumentItemModel>)
        fun onLoadPMyDocumentFail(message : String)
    }
}