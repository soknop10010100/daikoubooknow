package com.eazy.daikou.request_data.request.book_now_ws

import android.content.Context
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomCallback
import com.eazy.daikou.repository_ws.CustomResponseListener
import com.eazy.daikou.repository_ws.RetrofitGenerator
import com.eazy.daikou.request_data.request.book_now_ws.GenerateRespondWs.Companion.validateSt
import com.eazy.daikou.model.booking_hotel.*
import com.eazy.daikou.model.home.MainItemHomeModel
import com.eazy.daikou.model.home.SubItemHomeModel
import com.eazy.daikou.model.work_order.ImageListModel
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.google.gson.JsonObject

class BookingVisitWS {

    fun getHomePageVisitAllWs(context: Context, onCallBackHomePageItemListener: OnCallBackHomePageItemListener) {
        val serviceApi = RetrofitGenerator.
        createServiceWithHotelBooking(context)
        val res = serviceApi.homePageVisitItem
        res.enqueue(CustomCallback(context, object : CustomResponseListener {
            override fun onSuccess(resObj: JsonObject) {
                val mainItemHomeModelLIst: ArrayList<MainItemHomeModel> = ArrayList()
                val listBannerSlider: ArrayList<ImageListModel> = ArrayList()
                val subHomeModelCategoryList: ArrayList<SubItemHomeModel> = ArrayList()

                // Image Banner Slider
                if (resObj.has("advertise")) {
                    val imgList = resObj["advertise"].asJsonArray
                    for (i in 0 until imgList.size()) {
                        val imageListModel = ImageListModel()
                        imageListModel.keyImage = validateSt(imgList[i].asJsonObject, "id")
                        imageListModel.valImage = validateSt(imgList[i].asJsonObject, "image_ads")
                        listBannerSlider.add(imageListModel)
                    }
                }

                // Menus Category Visit
                if (resObj.has("menus")) {
                    val developmentData = resObj["menus"].asJsonArray
                    if (developmentData.size() > 0) {
                        for (i in 0 until developmentData.size()) {
                            subHomeModelCategoryList.add(getItemHotel(developmentData[i].asJsonObject, "menus_visit_category"))
                        }
                    }
                }

                // Visits Place
                if (resObj.has("visits_sub")) {
                    val developmentData = resObj["visits_sub"].asJsonArray
                    if (developmentData.size() > 0) {
                        val subItemHomeModelList = ArrayList<SubItemHomeModel>()
                        val mainItemHomeModel = MainItemHomeModel("visits_sub", context.resources.getString(R.string.visit_place), "")

                        for (i in 0 until developmentData.size()) {
                            subItemHomeModelList.add(getItemHotel(developmentData[i].asJsonObject, "visits_sub"))
                        }
                        mainItemHomeModel.subItemHomeModelList = subItemHomeModelList
                        mainItemHomeModelLIst.add(mainItemHomeModel)
                    }
                }

                // Visit Popular
                if (resObj.has("popular")) {
                    val popularArr = resObj["popular"].asJsonArray
                    for (i in 0 until popularArr.size()) {
                        val jsonObject = popularArr[i].asJsonObject
                        val title = validateSt(jsonObject, "name")

                        if (jsonObject.has("visits")){
                            val visitArr = jsonObject["visits"].asJsonArray
                            if (visitArr.size() > 0) {
                                val subItemHomeModelList = ArrayList<SubItemHomeModel>()
                                val mainItemHomeModel = MainItemHomeModel("popular", context.resources.getString(R.string.popular) + " ( $title )", "")

                                for (j in 0 until visitArr.size()) {
                                    subItemHomeModelList.add(getItemHotel(visitArr[j].asJsonObject, "popular",))
                                }

                                mainItemHomeModel.subItemHomeModelList = subItemHomeModelList
                                mainItemHomeModelLIst.add(mainItemHomeModel)
                            }
                        }
                    }
                }

                // Food Visit
                if (resObj.has("food")) {
                    val developmentData = resObj["food"].asJsonArray
                    if (developmentData.size() > 0) {
                        val subItemHomeModelList = ArrayList<SubItemHomeModel>()
                        val mainItemHomeModel = MainItemHomeModel("food", context.resources.getString(R.string.food), "")

                        for (i in 0 until developmentData.size()) {
                            subItemHomeModelList.add(getItemHotel(developmentData[i].asJsonObject, "food"))
                        }
                        mainItemHomeModel.subItemHomeModelList = subItemHomeModelList
                        mainItemHomeModelLIst.add(mainItemHomeModel)
                    }
                }

                // Activity Visit
                if (resObj.has("activity")) {
                    val developmentData = resObj["activity"].asJsonArray
                    if (developmentData.size() > 0) {
                        val subItemHomeModelList = ArrayList<SubItemHomeModel>()
                        val mainItemHomeModel = MainItemHomeModel("activity", context.resources.getString(R.string.activity), "")

                        for (i in 0 until developmentData.size()) {
                            subItemHomeModelList.add(getItemHotel(developmentData[i].asJsonObject, "activity"))
                        }
                        mainItemHomeModel.subItemHomeModelList = subItemHomeModelList
                        mainItemHomeModelLIst.add(mainItemHomeModel)
                    }
                }

                // Activity Visit
                if (resObj.has("news")) {
                    val developmentData = resObj["news"].asJsonArray
                    if (developmentData.size() > 0) {
                        val subItemHomeModelList = ArrayList<SubItemHomeModel>()
                        val mainItemHomeModel = MainItemHomeModel("news", context.resources.getString(R.string.news), "")

                        for (i in 0 until developmentData.size()) {
                            subItemHomeModelList.add(getItemHotel(developmentData[i].asJsonObject, "news"))
                        }
                        mainItemHomeModel.subItemHomeModelList = subItemHomeModelList
                        mainItemHomeModelLIst.add(mainItemHomeModel)
                    }
                }

                onCallBackHomePageItemListener.onSuccess(listBannerSlider, subHomeModelCategoryList, mainItemHomeModelLIst)
            }

            override fun onError(error: String, resCode: Int) {
                onCallBackHomePageItemListener.onFailed(error)
            }
        }))
    }

    private fun getItemHotel(jsonObject: JsonObject, action: String, ): SubItemHomeModel {
        val subItemHomeModel = SubItemHomeModel()
        when (action) {
            "menus_visit_category" -> {
                subItemHomeModel.action = action
                subItemHomeModel.id = validateSt(jsonObject, "id")
                subItemHomeModel.name = validateSt(jsonObject, "name")
                subItemHomeModel.imageUrl = validateSt(jsonObject, "icon")
            }
            "visits_sub", "visits_sub_detail" -> {
                subItemHomeModel.action = action
                subItemHomeModel.id = validateSt(jsonObject, "id")
                if (action == "visits_sub"){
                    subItemHomeModel.name = validateSt(jsonObject, "title_eng")
                } else {
                    subItemHomeModel.name = validateSt(jsonObject, "title")
                    subItemHomeModel.views = validateSt(jsonObject, "views")
                }
                subItemHomeModel.titleKh = validateSt(jsonObject, "title_kh")
                subItemHomeModel.imageUrl = validateSt(jsonObject, "profile_photo")
            }
            else -> {
                subItemHomeModel.action = action
                subItemHomeModel.id = validateSt(jsonObject, "id")
                subItemHomeModel.name = validateSt(jsonObject, "title")
                subItemHomeModel.address = validateSt(jsonObject, "body")
                subItemHomeModel.views = validateSt(jsonObject, "views")
                subItemHomeModel.imageUrl = validateSt(jsonObject, if (action == "news") "image" else "profile_photo")
            }
        }
        return subItemHomeModel
    }

    fun getLocationDetail(context: Context, action: String, locationId: String, id: String, OnCallBackDetailLocationListener: OnCallBackDetailLocationListener){
        val serviceApi = RetrofitGenerator.createServiceWithHotelBooking(context)
        val res = when(action) {
            "menu_category" -> {
                serviceApi.getItemDetailByMenus(id)
            }
            "search_item_by_menu" -> { // Search by location in category item
                serviceApi.getItemSearchByMenus(id, locationId)
            }
            else -> {
                serviceApi.getItemDetailByVisitLocation(id)
            }
        }
        res.enqueue(CustomCallback(context, object : CustomResponseListener {
            override fun onSuccess(resObj: JsonObject) {
                val locationHotelModelList: ArrayList<LocationModel> = ArrayList()
                val subItemHomeModelList = ArrayList<SubItemHomeModel>()
                if (resObj.has("visits")) {
                    val jsonObject = resObj["visits"].asJsonArray
                    if (jsonObject.size() > 0) {
                        for (i in 0 until jsonObject.size()) {
                            subItemHomeModelList.add(getItemHotel(jsonObject[i].asJsonObject, "visits_sub_detail"))
                        }
                    }
                }
                if (resObj.has("locations")) {
                    val data = resObj["locations"].asJsonArray
                    for (i in 0 until data.size()) {
                        val item: JsonElement = data[i].asJsonObject
                        val locationHotelModel: LocationModel = Gson().fromJson(item, LocationModel::class.java)
                        locationHotelModelList.add(locationHotelModel)
                    }
                }
                OnCallBackDetailLocationListener.onSuccess(subItemHomeModelList, locationHotelModelList)
            }

            override fun onError(error: String, resCode: Int) {
                OnCallBackDetailLocationListener.onFailed(error)
            }
        }))
    }

    fun getVisitShowDetail(context: Context, action: String, id: String, OnCallBackDetailLocationListener: OnCallBackDetailLocationListener){
        val serviceApi = RetrofitGenerator.createServiceWithHotelBooking(context)
        val res = when(action) {
            "news" -> {
                serviceApi.visitNewsDetail(id)
            }
            "advertise_banner" -> {
                serviceApi.visitSlideBanerDetail(id)
            }
            else -> {
                serviceApi.visitItemDetail(id)
            }
        }
        res.enqueue(CustomCallback(context, object : CustomResponseListener {
            override fun onSuccess(resObj: JsonObject) {
                val subItemHomeModelList = ArrayList<SubItemHomeModel>()
                if (resObj.has("nearplace")) {
                    val jsonObject = resObj["nearplace"].asJsonArray
                    if (jsonObject.size() > 0) {
                        for (i in 0 until jsonObject.size()) {
                            subItemHomeModelList.add(getItemHotel(jsonObject[i].asJsonObject, "visits_sub_detail"))
                        }
                    }
                }

                var locationHotelModel = HotelVisitDetailModel()
                if (resObj.has("news")) {
                    val data = resObj["news"].asJsonObject
                    if (!data.isJsonNull){
                        locationHotelModel = Gson().fromJson(data.asJsonObject, HotelVisitDetailModel::class.java)
                    }
                }

                if (resObj.has("visits")){
                    val data = resObj["visits"].asJsonObject
                    if (!data.isJsonNull){
                        locationHotelModel = Gson().fromJson(data.asJsonObject, HotelVisitDetailModel::class.java)
                    }
                }

                OnCallBackDetailLocationListener.onSuccessDetail(locationHotelModel, subItemHomeModelList)
            }

            override fun onError(error: String, resCode: Int) {
                OnCallBackDetailLocationListener.onFailed(error)
            }
        }))
    }

    interface OnCallBackHomePageItemListener {
        fun onSuccess(listImageSlider: ArrayList<ImageListModel>, subHomeModelCategoryList: ArrayList<SubItemHomeModel>, mainItemHomeModelLIst: ArrayList<MainItemHomeModel>)
        fun onFailed(message: String)
    }

    interface OnCallBackDetailLocationListener {
        fun onSuccess(subHomeModelCategoryList: ArrayList<SubItemHomeModel>, locationModel: ArrayList<LocationModel>)
        fun onSuccessDetail(hotelVisitDetailModel: HotelVisitDetailModel, nearByList: ArrayList<SubItemHomeModel>)
        fun onFailed(message: String)
    }

}