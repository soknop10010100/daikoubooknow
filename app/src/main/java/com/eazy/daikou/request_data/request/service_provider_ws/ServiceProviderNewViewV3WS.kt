package com.eazy.daikou.request_data.request.service_provider_ws

import android.content.Context
import com.eazy.daikou.repository_ws.RetrofitConnector
import com.eazy.daikou.repository_ws.CustomCallback
import com.eazy.daikou.repository_ws.CustomResponseListener
import com.eazy.daikou.model.service_provider.*
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.google.gson.JsonObject

class ServiceProviderNewViewV3WS {

    fun getListHomePageProviderWS(context: Context, page: String, limit: String, userId: String, callBackHomePageListener: CallBackHomePageListener){
        val serviceApi = RetrofitConnector.createService(context)
        val res = serviceApi.getHomePageListProvider(page, limit, userId)
        res.enqueue(CustomCallback(context, object : CustomResponseListener{
            override fun onSuccess(resObj: JsonObject?) {
                if (resObj != null){
                    if (resObj.has("data")) {
                        val dataObj = resObj.getAsJsonObject("data")
                        val mainItemList : ArrayList<MainItemModel> = ArrayList()
                        val imageSlider : ArrayList<ImageSliderModel> = ArrayList()
                        val subCategoryItemList : ArrayList<SubCategory> = ArrayList()

                        //slider image
                        if (dataObj.has("image_sliders")){
                            val imgList = dataObj["image_sliders"].asJsonArray
                            for (i in 0 until imgList.size()) {
                                val item: JsonElement = imgList[i].asJsonObject
                                val sliderModel : ImageSliderModel = Gson().fromJson(item, ImageSliderModel::class.java)
                                imageSlider.add(sliderModel)
                            }
                        }

                        //category
                        if (dataObj.has("categories")) {
                            val companyArray = dataObj["categories"].asJsonArray
                            if (companyArray.size() > 0){
                                for (item in companyArray){
                                    val company  = SubCategory()
                                    company.action = "companies"
                                    company.id = validateData(item.asJsonObject, "operation_part")
                                    company.title = validateData(item.asJsonObject, "name")
                                    company.urlImage = validateData(item.asJsonObject, "icon")
                                    subCategoryItemList.add(company)
                                }
                            }
                        }

                        //Companies
                        if (dataObj.has("companies")) {
                            val companyArray = dataObj["companies"].asJsonArray
                            if (companyArray.size() > 0){
                                val mainItemModel = MainItemModel("companies", "Company")
                                val subCategoryList : ArrayList<SubCategory> = ArrayList()
                                for (item in companyArray){
                                    val company  = SubCategory()
                                    company.action = "companies"
                                    company.id = validateData(item.asJsonObject, "account_id")
                                    company.title = validateData(item.asJsonObject, "name")
                                    company.urlImage = validateData(item.asJsonObject, "logo")
                                    company.city = validateData(item.asJsonObject, "city")
                                    subCategoryList.add(company)
                                }
                                mainItemModel.subCategoryList = subCategoryList
                                mainItemList.add(mainItemModel)
                            }
                        }

                        //Popular
                        if (dataObj.has("companies")) {
                            val companyArray = dataObj["companies"].asJsonArray
                            if (companyArray.size() > 0){
                                val mainItemModel = MainItemModel("companies", "Special offer")
                                val subCategoryList : ArrayList<SubCategory> = ArrayList()
                                for (item in companyArray){
                                    val company  = SubCategory()
                                    company.action = "companies"
                                    company.id = validateData(item.asJsonObject, "account_id")
                                    company.title = validateData(item.asJsonObject, "name")
                                    company.urlImage = validateData(item.asJsonObject, "logo")
                                    company.city = validateData(item.asJsonObject, "city")
                                    subCategoryList.add(company)
                                }
                                mainItemModel.subCategoryList = subCategoryList
                                mainItemList.add(mainItemModel)
                            }
                        }

                        //Near By Service
                        if (dataObj.has("companies")) {
                            val companyArray = dataObj["companies"].asJsonArray
                            if (companyArray.size() > 0){
                                val mainItemModel = MainItemModel("companies", "Service Near By")
                                val subCategoryList : ArrayList<SubCategory> = ArrayList()
                                for (item in companyArray){
                                    val company  = SubCategory()
                                    company.action = "companies"
                                    company.id = validateData(item.asJsonObject, "account_id")
                                    company.title = validateData(item.asJsonObject, "name")
                                    company.urlImage = validateData(item.asJsonObject, "logo")
                                    company.city = validateData(item.asJsonObject, "city")
                                    subCategoryList.add(company)
                                }
                                mainItemModel.subCategoryList = subCategoryList
                                mainItemList.add(mainItemModel)
                            }
                        }

                        //Product
                        if (dataObj.has("featured_services")) {
                            val companyArray = dataObj["featured_services"].asJsonArray
                            if (companyArray.size() > 0){
                                val mainItemModel = MainItemModel("featured_services", "Product")
                                val subCategoryList : ArrayList<SubCategory> = ArrayList()
                                for (item in companyArray){
                                    val company  = SubCategory()
                                    company.action = "featured_services"
                                    company.id = validateData(item.asJsonObject, "service_id")
                                    company.title = validateData(item.asJsonObject, "name")
                                    company.urlImage = validateData(item.asJsonObject, "image")
                                    //companyInfo
                                    var companyInfo : SubCompanyInfo? = null
                                    if (item.asJsonObject.has("company_info")){
                                        if (!item.asJsonObject["company_info"].isJsonNull){
                                            companyInfo = SubCompanyInfo()
                                            val companyInfoObj = item.asJsonObject["company_info"].asJsonObject
                                            companyInfo.id = validateData(companyInfoObj.asJsonObject, "account_id")
                                            companyInfo.title = validateData(companyInfoObj.asJsonObject, "name")
                                            companyInfo.urlImage = validateData(companyInfoObj.asJsonObject, "image")
                                        }
                                    }
                                    company.complyInfo = companyInfo
                                    subCategoryList.add(company)
                                }
                                mainItemModel.subCategoryList = subCategoryList
                                mainItemList.add(mainItemModel)
                            }
                        }

                        //All Services
                        if (dataObj.has("all_services")) {
                            val companyArray = dataObj["all_services"].asJsonArray
                            if (companyArray.size() > 0){
                                val mainItemModel = MainItemModel("all_services", "Service")
                                val subCategoryList : ArrayList<SubCategory> = ArrayList()
                                for (item in companyArray){
                                    val company  = SubCategory()
                                    company.action = "all_services"
                                    company.id = validateData(item.asJsonObject, "service_id")
                                    company.title = validateData(item.asJsonObject, "name")
                                    company.urlImage = validateData(item.asJsonObject, "image")
                                    company.priceDisplay = validateData(item.asJsonObject, "price_display")
                                    //companyInfo
                                    var companyInfo : SubCompanyInfo? = null
                                    if (item.asJsonObject.has("company_info")){
                                        companyInfo = SubCompanyInfo()
                                        if (!item.asJsonObject["company_info"].isJsonNull){
                                            val companyInfoObj = item.asJsonObject["company_info"].asJsonObject
                                            companyInfo.id = validateData(companyInfoObj.asJsonObject, "account_id")
                                            companyInfo.title = validateData(companyInfoObj.asJsonObject, "name")
                                            companyInfo.urlImage = validateData(companyInfoObj.asJsonObject, "image")
                                        }
                                    }
                                    company.complyInfo = companyInfo
                                    subCategoryList.add(company)
                                }
                                mainItemModel.subCategoryList = subCategoryList
                                mainItemList.add(mainItemModel)
                            }
                        }

                        callBackHomePageListener.onSuccessFul(mainItemList, subCategoryItemList, imageSlider)
                    }
                }
            }

            override fun onError(error: String, resCode: Int) {
                callBackHomePageListener.onLoadFailed(error)
            }
        }))
    }

    // List all of service provider
    fun getListAllOfServiceProviderWS(context: Context, page : Int, limit: Int, userID: String, listType: String, callBackListAll : CallBackListAllProviderListener){
        val serviceApi = RetrofitConnector.createService(context)
        val res = serviceApi.getListAllServiceProvider(page, limit, userID,listType)
        res.enqueue(CustomCallback(context, object : CustomResponseListener{
            override fun onSuccess(resObj: JsonObject?) {
               if (resObj != null){
                   if (resObj.has("data")){
                       val allServiceList: ArrayList<ListAllServiceProviderModel> = ArrayList()
                       val data = resObj["data"].asJsonArray
                       for (i in 0 until data.size()){
                           val item: JsonElement = data[i].asJsonObject
                           val listAllModel : ListAllServiceProviderModel = Gson().fromJson(item, ListAllServiceProviderModel::class.java)
                           allServiceList.add(listAllModel)
                       }
                       callBackListAll.onSuccessFul(allServiceList)
                   } else {
                       callBackListAll.onLoadFailed(resObj["msg"].asString)
                   }
               }
            }

            override fun onError(error: String, resCode: Int) {
                callBackListAll.onLoadFailed(error)
            }

        }))
    }

    // method for check validate data
    private fun validateData(jsonObject : JsonObject, key : String) : String? {
        if (jsonObject.has(key)) {
            if (!jsonObject[key].isJsonNull) {
                return jsonObject[key].asString
            }
        }
        return null
    }

    interface CallBackHomePageListener{
        fun onSuccessFul(homePageProvider : ArrayList<MainItemModel>, subCategoryList : ArrayList<SubCategory>, sliderImage : ArrayList<ImageSliderModel>)
        fun onLoadFailed(error : String)
    }

    interface CallBackListAllProviderListener{
        fun onSuccessFul(allServiceProviderList: ArrayList<ListAllServiceProviderModel>)
        fun onLoadFailed(error : String)
    }
}