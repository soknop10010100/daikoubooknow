package com.eazy.daikou.request_data.request.service_property_ws.cleaner;

import android.content.Context;

import com.eazy.daikou.repository_ws.RetrofitConnector;
import com.eazy.daikou.repository_ws.CustomCallback;
import com.eazy.daikou.repository_ws.CustomResponseListener;
import com.eazy.daikou.repository_ws.ServiceApi;
import com.eazy.daikou.model.my_property.service_property_employee.EmployeeDetailServiceProviderModel;
import com.eazy.daikou.model.my_property.service_property_employee.HistoryCleaningDetail;
import com.eazy.daikou.model.my_property.service_property_employee.ServiceDayScheduleModel;
import com.eazy.daikou.model.my_property.service_property_employee.ServicePropertyHistoryEmployeeModel;
import com.eazy.daikou.model.utillity_tracking.model.UnitScheduleCleaning;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;

public class ServiceScheduleEmployeeWs {

    // Schedule Work This Month Cleaner
    public void getListScheduleCleaner(Context context, HashMap<String, Object> mHashMap, ScheduleCleanerCallBackListener callBackSchedule){
        ArrayList<ServiceDayScheduleModel> listSchedule = new ArrayList<>();
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = serviceApi.getListStatusWorkSchedule(mHashMap);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if(resObj.has("success")){
                    if(resObj.get("success").getAsBoolean()){
                        if(resObj.has("data")){
                            JsonArray jsonArray = resObj.get("data").getAsJsonArray();
                            for (int i = 0; i < jsonArray.size(); i++) {
                                JsonObject item = jsonArray.get(i).getAsJsonObject();
                                ServiceDayScheduleModel schedule = new Gson().fromJson(item.toString(), ServiceDayScheduleModel.class);
                                listSchedule.add(schedule);
                            }
                            callBackSchedule.onLoadSuccessFull(listSchedule);
                        }
                    } else {
                        String message = resObj.get("msg").getAsString();
                        callBackSchedule.onLoadFail(message);
                    }
                }
            }

            @Override
            public void onError(String error, int resCode) {
                callBackSchedule.onLoadFail(error);
            }
        }));
    }

    //unit schedule
    public void getHistoryWorkSchedule(Context context, HashMap<String, Object> hashMap, UnitScheduleCleanerCallBackListener callBackSchedule){
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = serviceApi.getListHistoryWorkSchedule(hashMap);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if(resObj.has("success")){
                    if(resObj.get("success").getAsBoolean()){
                        if(resObj.has("data")){
                            JsonArray jsonArray = resObj.get("data").getAsJsonArray();
                            ArrayList<ServicePropertyHistoryEmployeeModel> listSchedule = new ArrayList<>();
                            for (int i = 0; i < jsonArray.size(); i++) {
                                JsonObject item = jsonArray.get(i).getAsJsonObject();
                                ServicePropertyHistoryEmployeeModel schedule = new Gson().fromJson(item.toString(), ServicePropertyHistoryEmployeeModel.class);
                                listSchedule.add(schedule);
                            }
                            callBackSchedule.onLoadSuccess(listSchedule);
                        }
                    }else {
                        String message = resObj.get("msg").getAsString();
                        callBackSchedule.onLoadFail(message);
                    }
                }
            }

            @Override
            public void onError(String error, int resCode) {
                callBackSchedule.onLoadFail(error);
            }
        }));
    }

    //ListCleaner
    public void getListHistoryCleaner(Context context, String propertyId, String userId, int page, HistoryCleanerCallBackListener callBackSchedule){
        ArrayList<UnitScheduleCleaning> listSchedule = new ArrayList<>();
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = serviceApi.getListHistoryCleaner(propertyId,userId,page,10);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if(resObj.has("success")){
                    boolean isSuccess = resObj.get("success").getAsBoolean();
                    if(isSuccess){
                        if(resObj.has("data")){
                            JsonArray jsonArray = resObj.get("data").getAsJsonArray();
                            for (int i = 0; i < jsonArray.size(); i++) {
                                JsonObject item = jsonArray.get(i).getAsJsonObject();
                                UnitScheduleCleaning schedule = new Gson().fromJson(item.toString(), UnitScheduleCleaning.class);
                                listSchedule.add(schedule);
                            }
                            callBackSchedule.onLoadSuccessFull(listSchedule);
                        }
                    }else {
                        String message = resObj.get("msg").getAsString();
                        callBackSchedule.onLoadFail(message);
                    }
                }
            }

            @Override
            public void onError(String error, int resCode) {
                callBackSchedule.onLoadFail(error);
            }
        }));
    }
    // detail Employee Service provider

    public void getListDetailEmployeeServiceProviderWs(Context context, String userId, String activeUserType, String workScheduleId, CallBackDetailEmployeeServiceListener callbackDetailEmployee){
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = serviceApi.getDetailEmployeeServiceProvider(userId, activeUserType, workScheduleId);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data")){
                    JsonObject object = resObj.get("data").getAsJsonObject();
                    EmployeeDetailServiceProviderModel detailEmployeeModel = new Gson().fromJson(object, EmployeeDetailServiceProviderModel.class);
                    callbackDetailEmployee.onSuccessful(detailEmployeeModel);
                } else {
                    callbackDetailEmployee.onLoadFail(resObj.get("msg").getAsString());
                }
            }
            @Override
            public void onError(String error, int resCode) {
                callbackDetailEmployee.onLoadFail(error);
            }
        }));
    }


    //Detail Cleaner
    public void getListHistoryDetailCleaner(Context context, String historyId, String userId, DetailHistoryCleanerCallBackListener callBackSchedule){
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = serviceApi.getDetailCleaner(historyId,userId);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if(resObj.has("success")){
                    boolean isSuccess = resObj.get("success").getAsBoolean();
                    if(isSuccess){
                        if(resObj.has("data")){
                            JsonObject object = resObj.get("data").getAsJsonObject();
                            HistoryCleaningDetail schedule = new Gson().fromJson(object, HistoryCleaningDetail.class);
                            callBackSchedule.onLoadSuccessFull(schedule);
                        }
                    }else {
                        String message = resObj.get("msg").getAsString();
                        callBackSchedule.onLoadFail(message);
                    }
                }
            }

            @Override
            public void onError(String error, int resCode) {
                callBackSchedule.onLoadFail(error);
            }
        }));
    }

    //check In Service Provider V2
    public void getServiceScanUnitWS(Context context, HashMap<String,Object> hashMap, CallBackScanUnitListener callBackScanUnit){
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = serviceApi.getListServiceScanUnit(hashMap);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if(resObj.has("success")){
                    if(resObj.get("success").getAsBoolean()){
                        if(resObj.has("data") && !resObj.get("data").isJsonNull()){
                            JsonObject jsonObject = resObj.get("data").getAsJsonObject();
                            if (jsonObject.has("action_status")){
                                if (!jsonObject.get("action_status").isJsonNull()){
                                    callBackScanUnit.onSuccessful(jsonObject.get("action_status").getAsString());
                                } else {
                                    callBackScanUnit.onLoadFail("No data");
                                }
                            } else {
                                callBackScanUnit.onLoadFail("No data");
                            }
                        }
                    } else {
                        callBackScanUnit.onLoadFail(resObj.get("msg").getAsString());
                    }
                }
            }

            @Override
            public void onError(String error, int resCode) {
                callBackScanUnit.onLoadFail(error);
            }
        }));
    }

    //Check In
    public void checkInToClean(Context context, HashMap<String,Object> hashMap, CheckInCleanerCallBackListener callBack){
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = serviceApi.scanCheckInClean(hashMap);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if(resObj.has("success")){
                    boolean isSuccess = resObj.get("success").getAsBoolean();
                    if(isSuccess){
                        if(resObj.has("data")){
                            JsonObject object = resObj.getAsJsonObject("data");
                            String message = "",orderId ="";
                            if(object.has("id")){
                                orderId = object.get("id").getAsString();
                            }
                            if(object.has("check_in_out_status")){
                                message = object.get("check_in_out_status").getAsString();
                            }
                            callBack.onLoadSuccessFully(message,orderId);
                        } else {
                            callBack.onLoadFail("error");
                        }

                    }else {
                        String message = resObj.get("msg").getAsString();
                       callBack.onLoadFail(message);
                    }
                }
            }

            @Override
            public void onError(String error, int resCode) {
                callBack.onLoadFail(error);
            }
        }));
    }

    //Upload photo
    public void uploadActivityPhotoWS(Context context, HashMap<String,Object> hashMap, CallBackActivityPhotoListener callbackPhoto){
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = serviceApi.uploadActivityPhoto(hashMap);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("success") && resObj.get("success").getAsString().equals("true")){
                    if (resObj.has("msg")) {
                        callbackPhoto.onSuccessful(resObj.get("msg").getAsString());
                    }
                } else {
                    if (resObj.has("msg")) {
                        callbackPhoto.onLoadFail(resObj.get("msg").getAsString());
                    }
                }
            }

            @Override
            public void onError(String error, int resCode) {
                callbackPhoto.onLoadFail(error);
            }
        }));
    }

    //Help In
    public void alertHelp(Context context, HashMap<String,Object> hashMap, CheckInCleanerCallBackListener callBack){
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = serviceApi.callHelp(hashMap);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if(resObj.has("success")){
                    boolean isSuccess = resObj.get("success").getAsBoolean();
                    if(isSuccess){
                        if(resObj.has("msg")){
                            String message = resObj.get("msg").getAsString();
                            callBack.onLoadSuccessFull(message);
                        }
                    }else {
                        String message = resObj.get("msg").getAsString();
                        callBack.onLoadFail(message);
                    }
                }
            }

            @Override
            public void onError(String error, int resCode) {
                callBack.onLoadFail(error);
            }
        }));
    }

    public void checkInOutLate(Context context, HashMap<String,Object> hashMap, CheckInCleanerCallBackListener callBack){
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = serviceApi.writeReasonWhenCheckInOutLate(hashMap);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if(resObj.has("success")){
                    boolean isSuccess = resObj.get("success").getAsBoolean();
                    if(isSuccess){
                        if(resObj.has("msg")){
                            String message = resObj.get("msg").getAsString();
                            callBack.onLoadSuccessFull(message);
                        }
                    }else {
                        String message = resObj.get("msg").getAsString();
                        callBack.onLoadFail(message);
                    }
                }
            }

            @Override
            public void onError(String error, int resCode) {
                callBack.onLoadFail(error);
            }
        }));
    }


    public void searchHistoryCleaner(Context context, int page, String propertyId, String userId, String unitNo, String orderNo, String startTime, String endTime, String status, String serviceTerm, HistoryCleanerCallBackListener callBackSchedule){
        ArrayList<UnitScheduleCleaning> listSchedule = new ArrayList<>();
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = serviceApi.searchServiceCleaning(page,10,propertyId,userId,orderNo,unitNo,startTime,endTime,status,serviceTerm);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if(resObj.has("success")){
                    boolean isSuccess = resObj.get("success").getAsBoolean();
                    if(isSuccess){
                        if(resObj.has("data")){
                            JsonArray jsonArray = resObj.get("data").getAsJsonArray();
                            for (int i = 0; i < jsonArray.size(); i++) {
                                JsonObject item = jsonArray.get(i).getAsJsonObject();
                                UnitScheduleCleaning schedule = new Gson().fromJson(item.toString(), UnitScheduleCleaning.class);
                                listSchedule.add(schedule);
                            }
                            callBackSchedule.onLoadSuccessFull(listSchedule);
                        }
                    }else {
                        String message = resObj.get("msg").getAsString();
                        callBackSchedule.onLoadFail(message);
                    }
                }
            }

            @Override
            public void onError(String error, int resCode) {
                callBackSchedule.onLoadFail(error);
            }
        }));
    }


    public interface ScheduleCleanerCallBackListener {
        void onLoadSuccessFull(ArrayList<ServiceDayScheduleModel> listSchedule);
        void onLoadFail(String message);
    }

    public interface UnitScheduleCleanerCallBackListener {
        void onLoadSuccess(ArrayList<ServicePropertyHistoryEmployeeModel> listSchedule);
        void onLoadFail(String message);
    }

    public interface HistoryCleanerCallBackListener {
        void onLoadSuccessFull(ArrayList<UnitScheduleCleaning> listSchedule);
        void onLoadFail(String message);
    }
    public interface DetailHistoryCleanerCallBackListener {
        void onLoadSuccessFull(HistoryCleaningDetail historyCleaner);
        void onLoadFail(String message);
    }

    public interface CheckInCleanerCallBackListener {
        void onLoadSuccessFully(String message, String orderId);
        void onLoadSuccessFull(String message);
        void onLoadFail(String message);
    }

    public interface CallBackScanUnitListener{
        void onSuccessful(String action);
        void onLoadFail(String error);
    }

    public interface CallBackDetailEmployeeServiceListener{
        void onSuccessful(EmployeeDetailServiceProviderModel employeeDetailServiceProviderModel);
        void onLoadFail(String error);
    }

    public interface CallBackActivityPhotoListener{
        void onSuccessful(String message);
        void onLoadFail(String error);
    }

}
