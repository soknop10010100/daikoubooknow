package com.eazy.daikou.request_data.request.service_property_ws;

import android.content.Context;
import android.widget.RelativeLayout;

import com.eazy.daikou.repository_ws.CustomCallback;
import com.eazy.daikou.repository_ws.CustomResponseListener;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.eazy.daikou.repository_ws.RetrofitConnector;
import com.eazy.daikou.repository_ws.ServiceApi;
import com.eazy.daikou.model.floor_plan.FloorListModel;
import com.eazy.daikou.model.floor_plan.FloorSecureModel;
import com.eazy.daikou.model.floor_plan.FloorSpaceCategoryModel;
import com.eazy.daikou.model.floor_plan.FloorSpaceItemModel;
import com.eazy.daikou.model.floor_plan.FloorSpacePolyline;
import com.eazy.daikou.model.floor_plan.FloorUnitItemModel;
import com.eazy.daikou.model.floor_plan.FloorUnitTypeItemModel;
import com.eazy.daikou.model.floor_plan.SpacePolyDetailModel;
import com.eazy.daikou.model.utillity_tracking.model.UnitPolyDetailModel;


import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

public class FloorListWs {

    public void getFloorList(Context context, int page , int limit, String property_id, FloorPlanCallBackListener floorPlanCallBackListener){
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = serviceApi.getFloorList( page, limit, property_id);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data")){
                    List<FloorListModel> list = new ArrayList<>();
                    JsonArray data = resObj.getAsJsonArray("data");
                    for (int i=0; i<data.size();i++) {
                        JsonElement item = data.get(i).getAsJsonObject();
                        FloorListModel floorListModel = new Gson().fromJson(item, FloorListModel.class);
                        list.add(floorListModel);
                    }
                    floorPlanCallBackListener.getFloorList(list);
                } else {
                    floorPlanCallBackListener.onFailed(resObj.get("msg").getAsString());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                floorPlanCallBackListener.onFailed(error);
            }
        }));
    }

    public void getSpaceCategory(Context context,String storey_id, String floor_no, FloorPlanCallBackListener floorPlanCallBackListener){
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = serviceApi.getSpaceCategory(storey_id, floor_no);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data")){
                    List<FloorSpaceCategoryModel> list = new ArrayList<>();
                    JsonArray data = resObj.getAsJsonArray("data");
                    for (int i=0; i<data.size();i++) {
                        JsonElement item = data.get(i).getAsJsonObject();
                        FloorSpaceCategoryModel spaceCategoryModel = new Gson().fromJson(item, FloorSpaceCategoryModel.class);
                        list.add(spaceCategoryModel);
                    }
                    floorPlanCallBackListener.getSpaceCategory(list);
                } else {
                    floorPlanCallBackListener.onFailed(resObj.get("msg").getAsString());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                floorPlanCallBackListener.onFailed(error);
            }
        }));
    }

    public void getSpaceItemSpaceList(Context context, String category_type, String space_category_id, String storey_id, int page, int limit, FloorSpaceList floorSpaceList){
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = serviceApi.getSpaceItemSpace(category_type, space_category_id, storey_id, page, limit);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data")){
                    List<FloorSpaceItemModel> list = new ArrayList<>();
                    JsonArray data = resObj.getAsJsonArray("data");
                    for (int i=0; i<data.size();i++) {
                        JsonElement item = data.get(i).getAsJsonObject();
                        FloorSpaceItemModel spaceItemModel = new Gson().fromJson(item, FloorSpaceItemModel.class);
                        list.add(spaceItemModel);
                    }
                    floorSpaceList.getSpaceList(list);
                } else {
                    floorSpaceList.onFailed(resObj.get("msg").getAsString());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                floorSpaceList.onFailed(error);
            }
        }));
    }

    public void getSpaceItemUnitList(Context context, String category_type, String property_id, String floor_no, int page, int limit, FloorUnitList floorUnitList) {
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = serviceApi.getSpaceItemUnit(category_type, property_id, floor_no, page, limit);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data")){
                    JsonArray data = resObj.getAsJsonArray("data");
                    List<FloorUnitItemModel> list = new ArrayList<>();
                    for (int i = 0; i < data.size(); i++) {
                        JsonElement item = data.get(i).getAsJsonObject();
                        FloorUnitItemModel spaceItemModel = new Gson().fromJson(item, FloorUnitItemModel.class);
                        list.add(spaceItemModel);
                    }
                    floorUnitList.getUnitList(list);
                } else {
                    floorUnitList.onFailed(resObj.get("msg").getAsString());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                floorUnitList.onFailed(error);
            }
        }));
    }

    public void getSpaceItemUnitTypeList(Context context,String category_type, String property_id, String floor_no, int page, int limit, FloorUnitTypeList floorUnitTypeList) {
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = serviceApi.getSpaceItemUnit( category_type, property_id, floor_no, page, limit);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data")){
                    JsonArray data = resObj.getAsJsonArray("data");
                    List<FloorUnitTypeItemModel> list = new ArrayList<>();
                    for (int i = 0; i < data.size(); i++) {
                        JsonElement item = data.get(i).getAsJsonObject();
                        FloorUnitTypeItemModel spaceItemModel = new Gson().fromJson(item, FloorUnitTypeItemModel.class);
                        list.add(spaceItemModel);
                    }
                    floorUnitTypeList.getUnitTypeList(list);
                } else {
                    floorUnitTypeList.onFailed(resObj.get("msg").getAsString());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                floorUnitTypeList.onFailed(error);
            }
        }));
    }

    public void getSpacePolyline(RelativeLayout layout, Context context, String category_type, String property_id, String space_id, String storey_id,  String fullNameSpace ,  FloorPlanCallBackListener floorPlanCallBackListener) {
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = serviceApi.getSpacePolyline(category_type, property_id, space_id, storey_id);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data") && !resObj.get("data").isJsonNull()){
                    JsonArray dataArray = resObj.get("data").getAsJsonArray();
                    List<FloorSpacePolyline> floorSpacePolylineList = new ArrayList<>();
                    for (int i = 0; i < dataArray.size(); i++) {
                        FloorSpacePolyline spacePolyline = new Gson().fromJson(dataArray.get(i).getAsJsonObject(), FloorSpacePolyline.class);
                        floorSpacePolylineList.add(spacePolyline);
                    }
                    floorPlanCallBackListener.getSpacePolyline(layout, floorSpacePolylineList, category_type,fullNameSpace);
                } else {
                    floorPlanCallBackListener.onFailed(resObj.get("msg").getAsString());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                floorPlanCallBackListener.onFailed(error);
            }
        }));
    }

    public void getUnitPolyline(RelativeLayout layout, Context context, String category_type, String property_id, String unit_id, String fullNameSpace, FloorPlanCallBackListener floorPlanCallBackListener) {
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> elementCall = serviceApi.getUnitPolyline(category_type, property_id, unit_id);
        elementCall.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data") && !resObj.get("data").isJsonNull()){
                    JsonArray dataArray = resObj.get("data").getAsJsonArray();
                    List<FloorSpacePolyline> floorSpacePolylineList = new ArrayList<>();
                    for (int i = 0; i < dataArray.size(); i++) {
                        FloorSpacePolyline spacePolyline = new Gson().fromJson(dataArray.get(i).getAsJsonObject(), FloorSpacePolyline.class);
                        floorSpacePolylineList.add(spacePolyline);
                    }
                    floorPlanCallBackListener.getUnitPolyline(layout, floorSpacePolylineList, category_type, fullNameSpace);
                } else {
                    floorPlanCallBackListener.onFailed(resObj.get("msg").getAsString());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                floorPlanCallBackListener.onFailed(error);
            }
        }));
    }

    public void getUnitTypePolyline(RelativeLayout layout, Context context,String category_type, String property_id, String unit_type_id, String floor_no,  String fullNameSpace, FloorPlanCallBackListener floorPlanCallBackListener) {
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> elementCall = serviceApi.getUnitTypePolyline(category_type, property_id, unit_type_id, floor_no);
        elementCall.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data") && !resObj.get("data").isJsonNull()){
                    JsonArray dataArray = resObj.get("data").getAsJsonArray();
                    List<FloorSpacePolyline> floorSpacePolylineList = new ArrayList<>();
                    for (int i = 0; i < dataArray.size(); i++) {
                        FloorSpacePolyline spacePolyline = new Gson().fromJson(dataArray.get(i).getAsJsonObject(), FloorSpacePolyline.class);
                        floorSpacePolylineList.add(spacePolyline);
                    }
                    floorPlanCallBackListener.getUnitTypePolyline(layout, floorSpacePolylineList, category_type, fullNameSpace);
                } else {
                    floorPlanCallBackListener.onFailed(resObj.get("msg").getAsString());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                floorPlanCallBackListener.onFailed(error);
            }
        }));
    }

    public void getFloorSecurity(Context context, String property_id, int page, int limit,FloorPlanCallBackListener floorPlanCallBackListener ){
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = serviceApi.getFloorSecurity(property_id, page, limit);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data")){
                    List<FloorSecureModel> list = new ArrayList<>();
                    JsonArray data = resObj.getAsJsonArray("data");
                    for (int i=0; i<data.size();i++) {
                        JsonElement item = data.get(i).getAsJsonObject();
                        FloorSecureModel secureModel = new Gson().fromJson(item, FloorSecureModel.class);
                        list.add(secureModel);
                    }
                    floorPlanCallBackListener.getFloorSecurity(list);
                } else {
                    floorPlanCallBackListener.onFailed(resObj.get("msg").getAsString());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                floorPlanCallBackListener.onFailed(error);
            }
        }));
    }

    public void getDetailSpacePolyline(Context context, String space_id, FloorDetailPolyline floorDetailPolyline ){
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = serviceApi.getSpaceByIdPolylineDetail(space_id);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data")){
                    JsonObject data = resObj.getAsJsonObject("data");
                    SpacePolyDetailModel spacePolyDetailModel = new Gson().fromJson(data, SpacePolyDetailModel.class);
                    floorDetailPolyline.getSpaceByIdPolyline(spacePolyDetailModel);
                } else {
                    floorDetailPolyline.onFailed(resObj.get("msg").getAsString());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                floorDetailPolyline.onFailed(error);
            }
        }));
    }

    public void getDetailUnitPolyline(Context context, String unit_id, FloorDetailPolyline floorDetailPolyline ){
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = serviceApi.getUnitByIdPolylineDetail(unit_id);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data")){
                    JsonObject data = resObj.get("data").getAsJsonObject();
                    UnitPolyDetailModel unitPolyDetailModel = new Gson().fromJson(data, UnitPolyDetailModel.class);
                    floorDetailPolyline.getUnitByIdPolyline(unitPolyDetailModel);
                } else {
                    floorDetailPolyline.onFailed(resObj.get("msg").getAsString());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                floorDetailPolyline.onFailed(error);
            }
        }));
    }


    public interface FloorSpaceList {
        void getSpaceList(List<FloorSpaceItemModel> floorList);
        void onFailed(String error);
    }
    public interface FloorUnitList {
        void getUnitList(List<FloorUnitItemModel> floorList);
        void onFailed(String error);
    }
    public interface FloorUnitTypeList {
        void getUnitTypeList(List<FloorUnitTypeItemModel> floorList);
        void onFailed(String error);
    }
    public interface FloorPlanCallBackListener{
        void getFloorList(List<FloorListModel> floorList);
        void getSpaceCategory(List<FloorSpaceCategoryModel> spaceCategoryList);
        void getFloorSecurity(List<FloorSecureModel> secureModelList);

        void getSpacePolyline(RelativeLayout layout, List<FloorSpacePolyline> spacePolyline, String category_type, String fullNameSpace);
        void getUnitPolyline(RelativeLayout layout, List<FloorSpacePolyline> spacePolyline, String category_type , String fullNameSpace);
        void getUnitTypePolyline(RelativeLayout layout, List<FloorSpacePolyline> spacePolyline, String category_type,  String fullNameSpace);
        void onFailed(String error);
    }

    public interface FloorDetailPolyline {
        void getSpaceByIdPolyline(SpacePolyDetailModel spacePolylineModels);
        void getUnitByIdPolyline(UnitPolyDetailModel unitPolylineModels);
        void onFailed(String error);
    }

}
