package com.eazy.daikou.request_data.request.book_now_ws

import com.eazy.daikou.model.CustomImageModel
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.google.gson.JsonObject

class GenerateRespondWs {
    companion object {
        // On Custom Respond
        fun onGenerateRespondWs(resObj: JsonObject): Boolean {
            if (resObj.has("success")) {
                if (resObj["success"].asBoolean) { // success = true
                    if (resObj.has("msg")) {
                        return true
                    }
                } else {    // success = false
                    if (resObj.has("msg")) {
                        return false
                    }
                }
            } else { // not found success
                if (resObj.has("msg")) {
                    return false
                }
            }
            return false
        }

        // On Custom Respond Data List
        fun <T> onGenerateRespondDataListWs(resObj: JsonObject, clazz: Class<T>): ArrayList<T> {
            // Already check has data in request
            val dataList = ArrayList<T>()
            val data = resObj["data"].asJsonArray
            for (i in 0 until data.size()) {
                val item: JsonElement = data[i].asJsonObject
                val itemTalkModel = Gson().fromJson(item, clazz)
                dataList.add(itemTalkModel)
            }
            return dataList
        }

        // On Custom Respond Data Class Model
        fun <T> onGenerateRespondDataObjWs(resObj: JsonObject, clazz: Class<T>): T {
            // Already check has data in request
            val data = resObj["data"].asJsonObject
            return Gson().fromJson(data, clazz)
        }

        // Check validate data
        fun validateSt(jsonObject: JsonObject, key: String): String? {
            if (jsonObject.has(key)) {
                if (!jsonObject[key].isJsonNull) {
                    return jsonObject[key].asString
                }
            }
            return null
        }

        fun convertListStringImage(list: ArrayList<CustomImageModel>): ArrayList<String> {
            val imgList = ArrayList<String>()
            for (item in list) {
                if (item.file_type == "image") {
                    imgList.add(item.file_path!!)
                }
            }
            return imgList
        }

    }
}