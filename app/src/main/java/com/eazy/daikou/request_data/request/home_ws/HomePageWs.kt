package com.eazy.daikou.request_data.request.home_ws

import android.content.Context
import com.eazy.daikou.repository_ws.RetrofitConnector
import com.eazy.daikou.repository_ws.CustomCallback
import com.eazy.daikou.repository_ws.CustomResponseListener
import com.eazy.daikou.base.MockUpData
import com.eazy.daikou.helper.UserSessionManagement
import com.eazy.daikou.model.home_page.DevelopmentProjects
import com.eazy.daikou.model.home_page.PropertyListing
import com.eazy.daikou.model.profile.PropertyItemModel
import com.eazy.daikou.model.profile.UserTypeProfileModel
import com.google.gson.Gson
import com.google.gson.JsonObject

class HomePageWs {

    companion object {
        // Get Property Listing
        fun getPropertyListing(context: Context, page: String, limit: String, callBack: OnRespondPropertyListing) {
            val api = RetrofitConnector.createService(context).getMainHomePage(page, limit)
            api.enqueue(CustomCallback(context, object : CustomResponseListener {
                override fun onSuccess(resObj: JsonObject) {
                    if (resObj.has("data")) {
                        val jsonData = resObj.get("data").asJsonObject
                        val listPropertyListing = ArrayList<PropertyListing>()
                        val listContactsAccount = ArrayList<UserTypeProfileModel>()

                        if (!jsonData.isJsonNull) {
                            // Property Listener
                            if (jsonData.has("property_listing")){
                                val jsonArrayPropertyListing = jsonData.get("property_listing").asJsonArray
                                for (index in 0 until jsonArrayPropertyListing.size()) {
                                    listPropertyListing.add(Gson().fromJson(jsonArrayPropertyListing[index].asJsonObject, PropertyListing::class.java))
                                }
                            }

                            // Account Contact
                            if (jsonData.has("accounts_and_contacts")){
                                val jsonArrayContacts = jsonData.getAsJsonArray("accounts_and_contacts")
                                for (index in 0 until jsonArrayContacts.size()) {
                                    listContactsAccount.add(
                                        Gson().fromJson(
                                            jsonArrayContacts[index],
                                            UserTypeProfileModel::class.java
                                        )
                                    )
                                }
                            }
                        }

                        // Active account property
                        val userSessionManagement = UserSessionManagement(context)
                        val user = MockUpData.getUserItem(userSessionManagement)
                        if (jsonData.has("active_account")){
                            val jsonObjectAccount = jsonData.getAsJsonObject("active_account")
                            val activeAccount = Gson().fromJson(jsonObjectAccount.asJsonObject, PropertyItemModel::class.java)
                            user.accountId = activeAccount.id
                            user.accountName = activeAccount.name
                        }

                        // Active user type
                        if (jsonData.has("active_user_type")){
                            val activeUserType = jsonData.get("active_user_type").asString
                            user.activeUserType = activeUserType
                        }

                        // == Store Data Local ==
                        userSessionManagement.saveData(user, true)

                        callBack.onSuccess(listContactsAccount, listPropertyListing)
                    } else {
                        callBack.onGetItemFiled(resObj.get("msg").toString())
                    }
                }

                override fun onError(error: String, resCode: Int) {
                    callBack.onGetItemFiled(error)
                }

            })
            )
        }

        // Get Project Development
        fun getProjectDevelopment(context: Context, page: String, limit: String, callBack: OnRespondProjectDevelopment) {
            val api = RetrofitConnector.createService(context).getMainHomePage(page, limit)
            val listProjectDevelopment = ArrayList<DevelopmentProjects>()
            api.enqueue(CustomCallback(context, object : CustomResponseListener {
                override fun onSuccess(resObj: JsonObject) {
                    if (resObj.has("data")) {
                        val jsonData = resObj.get("data") as JsonObject
                        if (!jsonData.isJsonNull){
                            // Project Development
                            if (jsonData.has("development_projects")){
                                val jsonArrayProjectDevelopment = jsonData.getAsJsonArray("development_projects")
                                for (index in 0 until jsonArrayProjectDevelopment.size()) {
                                    listProjectDevelopment.add(
                                        Gson().fromJson(
                                            jsonArrayProjectDevelopment[index],
                                            DevelopmentProjects::class.java
                                        )
                                    )
                                }
                            }
                        }

                        callBack.onSuccess(listProjectDevelopment)

                    } else {
                        callBack.onGetItemFiled(resObj.get("msg").toString())
                    }
                }

                override fun onError(error: String, resCode: Int) {
                    callBack.onGetItemFiled(error)
                }
            }))
        }

    }

    interface OnRespondPropertyListing {
        fun onSuccess(listAccountsAndContacts: List<UserTypeProfileModel>, list: List<PropertyListing>)
        fun onGetItemFiled(msg: String)

    }

    interface OnRespondProjectDevelopment {
        fun onSuccess(list: List<DevelopmentProjects>)
        fun onGetItemFiled(msg: String)
    }

}