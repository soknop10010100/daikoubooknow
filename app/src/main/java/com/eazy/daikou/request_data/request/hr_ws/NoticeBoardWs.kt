package com.eazy.daikou.request_data.request.hr_ws

import android.content.Context
import com.eazy.daikou.repository_ws.CustomCallback
import com.eazy.daikou.repository_ws.CustomResponseListener
import com.eazy.daikou.repository_ws.RetrofitGenerator
import com.eazy.daikou.model.hr.NoticeBoardDetailModel
import com.eazy.daikou.model.hr.NoticeBoardHrModel
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.google.gson.JsonObject

class NoticeBoardWs {

    fun getNoticeBoardWs(context: Context, page: Int, limit: Int, accountBusinessKey: String, onCallBackNoticeBoardListener: OnCallBackNoticeBoardListener ){
        val serviceMenuApi = RetrofitGenerator.createServiceWithHR(context)
        val res = serviceMenuApi.getNoticeBoard(page, limit, accountBusinessKey)
        res.enqueue(CustomCallback(context, object : CustomResponseListener{
            override fun onSuccess(resObj: JsonObject?) {
                if (resObj != null){
                    if (resObj.has("data")){
                        val listNoticeBoard: ArrayList<NoticeBoardHrModel> = ArrayList()
                        val data = resObj["data"].asJsonArray
                        for (i in 0 until data.size()){
                            val item: JsonElement = data[i].asJsonObject
                            val noticeBoardHrModel: NoticeBoardHrModel = Gson().fromJson(item, NoticeBoardHrModel::class.java)
                            listNoticeBoard.add(noticeBoardHrModel)
                        }
                        onCallBackNoticeBoardListener.onLoadSuccessFull(listNoticeBoard)
                    }else {
                        onCallBackNoticeBoardListener.onLoadFailed(resObj.get("msg").asString)
                    }
                }
            }

            override fun onError(error: String, resCode: Int) {
                onCallBackNoticeBoardListener.onLoadFailed(error)
            }
        }))
    }

    fun getNoticeBoardDetail(context: Context, noticeListId : String, onCallBackDetail : OnCallBackNoticeBoardDetail){
        val serviceMenuApi = RetrofitGenerator.createServiceWithHR(context)
        val res = serviceMenuApi.getNoticeBoardDetail(noticeListId)
        res.enqueue(CustomCallback(context, object : CustomResponseListener{
            override fun onSuccess(resObj: JsonObject?) {
                if (resObj != null) {
                    if (resObj.has("data") && !resObj["data"].isJsonNull){
                        val data: JsonElement = resObj["data"].asJsonObject
                        val noticeDetail: NoticeBoardDetailModel = Gson().fromJson(data, NoticeBoardDetailModel::class.java)
                        onCallBackDetail.onLoadSuccessFull(noticeDetail)
                    } else {
                        onCallBackDetail.onLoadFailed(resObj.get("msg").asString)
                    }
                }
            }

            override fun onError(error: String, resCode: Int) {
                onCallBackDetail.onLoadFailed(error)
            }

        }))
    }
    interface OnCallBackNoticeBoardListener{
        fun onLoadSuccessFull(noticeBoardHrModel: ArrayList<NoticeBoardHrModel>)
        fun onLoadFailed(message: String)
    }

    interface OnCallBackNoticeBoardDetail{
        fun onLoadSuccessFull(noticeBoardDetail: NoticeBoardDetailModel)
        fun onLoadFailed(message: String)
    }
}