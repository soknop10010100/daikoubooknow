package com.eazy.daikou.request_data.request.parking_ws;

import android.content.Context;

import androidx.annotation.NonNull;

import com.eazy.daikou.repository_ws.RetrofitConnector;
import com.eazy.daikou.repository_ws.ServiceApi;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PaymentParkingWs {

    public void getPayment(Context context, String propertyListId, String scanQr, OnRequestPaymentCallBack paymentCallBack){
        ServiceApi api = RetrofitConnector.createService(context);
        Call<JsonElement> res = api.getPaymentParking(propertyListId,scanQr);
        res.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(@NonNull Call<JsonElement> call, @NonNull Response<JsonElement> response) {
                if(response.isSuccessful()){
                    assert response.body() != null;
                    JsonObject jsonObject = response.body().getAsJsonObject();
                    if(jsonObject.has("success")){
                        boolean isSuccess = jsonObject.get("success").getAsBoolean();
                        if(isSuccess){
                            if(jsonObject.has("data")){
                                JsonObject object = jsonObject.getAsJsonObject("data");
                                String link="";
                                if(object.has("payment_link")){
                                    link = object.get("payment_link").getAsString();
                                }
                                paymentCallBack.onLoadSuccess(link);
                            }
                        } else {
                            if(jsonObject.has("error_des")){
                                paymentCallBack.onLoadFail(jsonObject.get("error_des").getAsString());
                            } else {
                                paymentCallBack.onLoadFail(jsonObject.get("msg").getAsString());
                            }

                        }
                    }
                } else {
                    paymentCallBack.onLoadFail("error");
                }
            }

            @Override
            public void onFailure(@NonNull Call<JsonElement> call, @NonNull Throwable t) {
                paymentCallBack.onLoadFail("error");
            }
        });

    }

    public void getPaymentPayByCash(Context context, HashMap<String,Object> hashMap, OnRequestPaymentCallBack paymentCallBack){
        ServiceApi api = RetrofitConnector.createService(context);
        Call<JsonElement> res = api.paymentParkingByCash(hashMap);
        res.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(@NonNull Call<JsonElement> call, @NonNull Response<JsonElement> response) {
                if(response.isSuccessful()){
                    assert response.body() != null;
                    JsonObject jsonObject = response.body().getAsJsonObject();
                    if(jsonObject.has("success")){
                        boolean isSuccess = jsonObject.get("success").getAsBoolean();
                        if(isSuccess){
                            if(jsonObject.has("msg")){
                                String message = jsonObject.get("msg").getAsString();
                                paymentCallBack.onLoadSuccess(message);
                            }
                        }else {
                            paymentCallBack.onLoadFail(jsonObject.get("msg").getAsString());
                        }
                    }
                }else {
                    paymentCallBack.onLoadFail("error");
                }
            }

            @Override
            public void onFailure(@NonNull Call<JsonElement> call, @NonNull Throwable t) {
                paymentCallBack.onLoadFail("error");
            }
        });

    }
    public void acceptPaymentPayByCash(Context context, HashMap<String,Object> hashMap, OnRequestPaymentCallBack paymentCallBack){
        ServiceApi api = RetrofitConnector.createService(context);
        Call<JsonElement> res = api.acceptPaymentParking(hashMap);
        res.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(@NonNull Call<JsonElement> call, @NonNull Response<JsonElement> response) {
                if(response.isSuccessful()){
                    assert response.body() != null;
                    JsonObject jsonObject = response.body().getAsJsonObject();
                    if(jsonObject.has("success")){
                        boolean isSuccess = jsonObject.get("success").getAsBoolean();
                        if(isSuccess){
                            if(jsonObject.has("msg")){
                                String message = jsonObject.get("msg").getAsString();
                                paymentCallBack.onLoadSuccess(message);
                            }
                        }else {
                            paymentCallBack.onLoadFail(jsonObject.get("msg").getAsString());
                        }
                    }
                }else {
                    paymentCallBack.onLoadFail("error");
                }
            }

            @Override
            public void onFailure(@NonNull Call<JsonElement> call, @NonNull Throwable t) {
                paymentCallBack.onLoadFail("error");
            }
        });

    }

    public void getPaymentKessPrinter(Context context, String propertyListId, OnRequestPaymentCallBack paymentCallBack){
        ServiceApi api = RetrofitConnector.createService(context);
        Call<JsonElement> res = api.paymentParkingKESS(propertyListId);
        res.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(@NonNull Call<JsonElement> call, @NonNull Response<JsonElement> response) {
                if(response.body() != null && response.isSuccessful()){
                    JsonObject jsonObject = response.body().getAsJsonObject();
                    if(jsonObject.has("success")){
                        boolean isSuccess = jsonObject.get("success").getAsBoolean();
                        if(isSuccess){
                            if(jsonObject.has("data")){
                                JsonObject object = jsonObject.getAsJsonObject("data");
                                String link="";
                                if(object.has("payment_link")){
                                    link = object.get("payment_link").getAsString();
                                }
                                paymentCallBack.onLoadSuccess(link);
                            }
                        }else {
                            if(jsonObject.has("error_des")){
                                paymentCallBack.onLoadFail(jsonObject.get("error_des").getAsString());
                            } else {
                                paymentCallBack.onLoadFail(jsonObject.get("msg").getAsString());
                            }

                        }
                    }
                }else {
                    paymentCallBack.onLoadFail("error");
                }
            }

            @Override
            public void onFailure(@NonNull Call<JsonElement> call, @NonNull Throwable t) {
                paymentCallBack.onLoadFail("error");
            }
        });

    }

    public interface OnRequestPaymentCallBack {
        void onLoadSuccess(String linkKESSPay);
        void onLoadFail(String message);
    }
}
