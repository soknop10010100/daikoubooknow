package com.eazy.daikou.request_data.request.notificationWs;

import android.content.Context;

import com.eazy.daikou.repository_ws.RetrofitConnector;
import com.eazy.daikou.repository_ws.CustomCallback;
import com.eazy.daikou.repository_ws.CustomResponseListener;
import com.eazy.daikou.model.notification.ListNotification;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;

public class ListAllNotificationWs {

    public interface OnCallBackListNotification{
        void onSuccess(List<ListNotification> listNotifications);
        void onSuccessMsg(ListNotification listNotification, boolean isReadAll);
        void onSuccessDelete(ListNotification listNotification);
        void onFail(String message);
    }

    public interface OnCallBackListNumberNotification{
        void onSuccess(String number);
        void onFailed(String message);
    }

    public void getListAllNotification(Context activity, int page, int limit , String userId , OnCallBackListNotification onCallBackListNotification ){
        Call<JsonElement> res = RetrofitConnector.createService(activity).getListAllNotification(page, limit, userId);
        res.enqueue(new CustomCallback(activity, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("success") && resObj.get("success").getAsString().equals("true")){
                    JsonArray list = resObj.get("data").getAsJsonArray();
                    List<ListNotification> stringList = new ArrayList<>();
                    for (int i = 0 ; i < list.size() ; i++){
                        JsonElement item = list.get(i).getAsJsonObject();
                        ListNotification frontDesk = new Gson().fromJson(item, ListNotification.class);
                        stringList.add(frontDesk);
                    }
                    onCallBackListNotification.onSuccess(stringList);
                } else {
                    onCallBackListNotification.onFail(resObj.get("msg").getAsString());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                onCallBackListNotification.onFail(error);
            }
        }));
    }

    public void readItemNotification(Context activity, HashMap<String, Object> hashMap, boolean isReadAll, ListNotification notification, OnCallBackListNotification onCallBackListNotification){
        Call<JsonElement> res = isReadAll ? RetrofitConnector.createService(activity).readAllNotification(hashMap) : RetrofitConnector.createService(activity).readNotification(hashMap);
        res.enqueue(new CustomCallback(activity, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("success") && resObj.get("success").getAsString().equals("true")){
                    if (resObj.has("msg")) {
                        onCallBackListNotification.onSuccessMsg(notification, isReadAll);
                    }
                } else {
                    if (resObj.has("msg")) {
                        onCallBackListNotification.onFail(resObj.get("msg").getAsString());
                    }
                }
            }

            @Override
            public void onError(String error, int resCode) {
                onCallBackListNotification.onFail(error);
            }
        }));
    }

    public void deleteNotification(Context activity, HashMap<String, Object> hashMap, ListNotification notification, OnCallBackListNotification onCallBackListNotification){
        Call<JsonElement> res = RetrofitConnector.createService(activity).deleteNotification(hashMap);
        res.enqueue(new CustomCallback(activity, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("success") && resObj.get("success").getAsString().equals("true")){
                    if (resObj.has("msg")) {
                        onCallBackListNotification.onSuccessDelete(notification);
                    }
                } else {
                    if (resObj.has("msg")) {
                        onCallBackListNotification.onFail(resObj.get("msg").getAsString());
                    }
                }
            }

            @Override
            public void onError(String error, int resCode) {
                onCallBackListNotification.onFail(error);
            }
        }));
    }

    public void readNumberNotification(Context activity, String userId, OnCallBackListNumberNotification onCallBackListNotification){
        Call<JsonElement> res = RetrofitConnector.createService(activity).readNumberNotification(userId);
        res.enqueue(new CustomCallback(activity, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data")){
                    if (!resObj.get("data").isJsonNull()){
                        JsonElement jsonElement = resObj.get("data").getAsJsonObject();
                        if (!jsonElement.isJsonNull() && !jsonElement.getAsJsonObject().get("notification_badges").isJsonNull()){
                            String number = jsonElement.getAsJsonObject().get("notification_badges").getAsString();
                            onCallBackListNotification.onSuccess(number);
                        }

                    }
                } else {
                    onCallBackListNotification.onFailed(resObj.get("msg").getAsString());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                onCallBackListNotification.onFailed(error);
            }
        }));
    }
}
