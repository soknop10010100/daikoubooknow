package com.eazy.daikou.request_data.request.book_quote_ws;

import android.content.Context;

import com.eazy.daikou.repository_ws.CustomCallback;
import com.eazy.daikou.repository_ws.CustomResponseListener;
import com.eazy.daikou.model.rule_announcement.RulesModel;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.eazy.daikou.repository_ws.RetrofitConnector;
import com.eazy.daikou.repository_ws.ServiceApi;
import com.eazy.daikou.model.law.LawsModel;
import com.eazy.daikou.model.law.LawsDetailModel;
import com.eazy.daikou.model.law.RuleNewModel;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

public class RuleAndNewsWs {

    public void getRules(Context context, int page, int limit, String type, String property_id, RuleAndNewsWs.RulesCallBackListener rulesCallBackListener) {
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = serviceApi.rule_and_news(page, limit, type, property_id);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data")) {
                    ArrayList<RulesModel> rulesModelArrayList = new ArrayList<>();
                    JsonArray data = resObj.getAsJsonArray("data");
                    for (int i = 0; i < data.size(); i++) {
                        RulesModel rulesModel = new Gson().fromJson(data.get(i).getAsJsonObject(), RulesModel.class);
                        rulesModelArrayList.add(rulesModel);
                    }
                    rulesCallBackListener.getRules(rulesModelArrayList);
                } else {
                    rulesCallBackListener.onFailed(resObj.get("msg").getAsString());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                rulesCallBackListener.onFailed(error);
            }
        }));
    }

    public void getLaws(Context context, int page, int limit, RuleAndNewsWs.RuleNewsCallBackListener ruleNewsCallBackListener){
        ServiceApi serviceApi = RetrofitConnector.createService( context);
        Call<JsonElement> res = serviceApi.laws(page, limit);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data")){
                    List<LawsModel> list = new ArrayList<>();
                    JsonArray data = resObj.get("data").getAsJsonArray();
                    for (int i=0; i<data.size();i++) {
                        JsonElement item = data.get(i).getAsJsonObject();
                        LawsModel ruleNewsModel = new Gson().fromJson(item, LawsModel.class);
                        list.add(ruleNewsModel);
                    }
                    ruleNewsCallBackListener.getLaws(list);
                }
            }

            @Override
            public void onError(String error, int resCode) {
                ruleNewsCallBackListener.onFailed(error);
            }
        }));
    }

    public void getLawsDetail(Context context, int page, int limit, String category_id, LawsCallBackListener lawsCallBackListener){
        ServiceApi serviceApi = RetrofitConnector.createService( context);
        Call<JsonElement> res = serviceApi.lawsDetail(page, limit, category_id);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data")){
                    List<LawsDetailModel> list = new ArrayList<>();
                    JsonArray data = resObj.get("data").getAsJsonArray();
                    for (int i=0; i<data.size();i++) {
                        JsonElement item = data.get(i).getAsJsonObject();
                        LawsDetailModel ruleNewsModel = new Gson().fromJson(item, LawsDetailModel.class);
                        list.add(ruleNewsModel);
                    }
                    lawsCallBackListener.getLawsDetail(list);
                }
            }

            @Override
            public void onError(String error, int resCode) {
                lawsCallBackListener.onFailed(error);
            }
        }));
    }

    public interface RuleNewsCallBackListener{
        void getRuleNewsModel(List<RuleNewModel> ruleNewsModels);
        void getLaws(List<LawsModel> lawsModelList);
        void onFailed(String error);
    }

    public interface RulesCallBackListener{
        void getRules(List<RulesModel> rulesModels);
        void onFailed(String error);
    }

    public interface LawsCallBackListener{
        void getLawsDetail(List<LawsDetailModel> lawsModelDetailList);
        void onFailed(String error);
    }

}
