package com.eazy.daikou.request_data.request.service_property_ws_v2

import android.content.Context
import com.eazy.daikou.repository_ws.RetrofitConnector
import com.eazy.daikou.repository_ws.CustomCallback
import com.eazy.daikou.repository_ws.CustomResponseListener
import com.eazy.daikou.model.my_property.cleaning.*
import com.eazy.daikou.model.my_property.service_provider_v2.ServiceProviderCompanyPropertyModel
import com.eazy.daikou.model.my_property.service_provider_v2.ServiceProviderTypeModel
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.google.gson.JsonObject

class ServiceProviderPropertyWs {

    fun getListPropertyServiceProviderWs(
        context: Context,
        page: Int,
        limit: Int,
        operationPart: String,
        userid: String,
        accountID: String,
        callBackList: OnCallBackListServiceListener) {
        val serviceApi = RetrofitConnector.createService(context)
        val res = serviceApi.getListPropertyServiceProvider(page, limit, operationPart, userid, accountID)
        res.enqueue(CustomCallback(context, object : CustomResponseListener {
            override fun onSuccess(resObj: JsonObject?) {
                if (resObj != null) {
                    if (resObj.has("data")) {
                        val listService: ArrayList<PropertyServiceProviderListModel> = ArrayList()
                        val dataService = resObj["data"].asJsonArray
                        for (i in 0 until dataService.size()) {
                            val item: JsonElement = dataService[i].asJsonObject
                            val providerListModel: PropertyServiceProviderListModel =
                                Gson().fromJson(item, PropertyServiceProviderListModel::class.java)
                            listService.add(providerListModel)
                        }
                        callBackList.onLoadSuccessFull(listService)
                    } else {
                        callBackList.onLoadFailed(resObj["msg"].asString)
                    }
                }
            }

            override fun onError(error: String, resCode: Int) {
                callBackList.onLoadFailed(error)
            }
        }))
    }

    fun getCategoryTabServiceProviderWs(context: Context, callBackType: OnCallBackTypeListener) {
        val serviceApi = RetrofitConnector.createService(context)
        val res = serviceApi.listTypeServiceProviderProperty()
        res.enqueue(CustomCallback(context, object : CustomResponseListener {
            override fun onSuccess(resObj: JsonObject?) {
                if (resObj != null) {
                    if (resObj.has("data")) {
                        val listProviderCategory: ArrayList<ServiceProviderTypeModel> = ArrayList()
                        val data = resObj["data"].asJsonArray
                        for (i in 0 until data.size()) {
                            val item: JsonElement = data[i].asJsonObject
                            val serviceCategoryModel: ServiceProviderTypeModel =
                                Gson().fromJson(item, ServiceProviderTypeModel::class.java)
                            listProviderCategory.add(serviceCategoryModel)
                        }
                        callBackType.onLoadSuccessFull(listProviderCategory)
                    } else {
                        callBackType.onLoadFailed(resObj.get("msg").asString)
                    }
                }
            }

            override fun onError(error: String, resCode: Int) {
                callBackType.onLoadFailed(error)
            }
        }))

    }

    fun getListCompanyServiceProviderWS(
        context: Context,
        page: Int,
        limit: Int,
        activeAccountId: String,
        serviceKey: String,
        serviceName: String,
        operationPart: String,
        callBackCompany: OnCallBackCompanyListener) {
        val serviceApi = RetrofitConnector.createService(context)
        val res = serviceApi.listServiceProviderCompany(
            page,
            limit,
            activeAccountId,
            serviceKey,
            serviceName,
            operationPart
        )
        res.enqueue(CustomCallback(context, object : CustomResponseListener {
            override fun onSuccess(resObj: JsonObject?) {
                if (resObj != null) {
                    if (resObj.has("data")) {
                        val listServiceCompanyList: ArrayList<ServiceProviderCompanyPropertyModel> =
                            ArrayList()
                        val data = resObj["data"].asJsonArray
                        for (i in 0 until data.size()) {
                            val item: JsonElement = data[i].asJsonObject
                            val serviceCategoryModel: ServiceProviderCompanyPropertyModel =
                                Gson().fromJson(
                                    item,
                                    ServiceProviderCompanyPropertyModel::class.java
                                )
                            listServiceCompanyList.add(serviceCategoryModel)
                        }
                        callBackCompany.onLoadSuccessFull(listServiceCompanyList)
                    } else {
                        callBackCompany.onLoadFailed(resObj.get("msg").asString)
                    }
                }
            }

            override fun onError(error: String, resCode: Int) {
                callBackCompany.onLoadFailed(error)
            }

        }))
    }

    fun getServiceTermAndCategoryWs(
        context: Context,
        companyAccountId : String,
        activeAccountId: String,
        operationPart: String,
        callbackService: OnCallBackServiceTermAndCategory) {
        val serviceApi = RetrofitConnector.createService(context)
        val res = serviceApi.listServiceCategoryAndTerm(companyAccountId, activeAccountId, operationPart)
        res.enqueue(CustomCallback(context, object : CustomResponseListener {
            override fun onSuccess(resObj: JsonObject?) {
                if (resObj != null) {
                    if (resObj.has("data")) {
                        val data = resObj["data"].asJsonObject
                        val serviceTermAndCategory: ServiceTermAndCategoryModel = Gson().fromJson(
                            data.asJsonObject,
                            ServiceTermAndCategoryModel::class.java
                        )
                        callbackService.onLoadSuccessFull(serviceTermAndCategory)
                    } else {
                        callbackService.onLoadFailed(resObj["msg"].asString)
                    }
                }
            }

            override fun onError(error: String, resCode: Int) {
                callbackService.onLoadFailed(error)
            }

        }))
    }

    fun getListPropertyServiceTypeWs(
        context: Context,
        page: Int,
        limit: Int,
        activeAccountId: String,
        unitId: String,
        termKey: String,
        categoryId: String,
        companyAccountId: String,
        callBackPropertyServiceType: OnCallBackPropertyServiceType
    ) {
        val serviceApi = RetrofitConnector.createService(context)
        val res = serviceApi.listPropertyServiceType(
            page,
            limit,
            activeAccountId,
            unitId,
            termKey,
            categoryId,
            companyAccountId
        )
        res.enqueue(CustomCallback(context, object : CustomResponseListener {
            override fun onSuccess(resObj: JsonObject?) {
                if (resObj != null) {
                    if (resObj.has("data")) {
                        val propertyServiceTypeList: ArrayList<PropertyServiceTypeModel> =
                            ArrayList()
                        val data = resObj["data"].asJsonArray
                        for (i in 0 until data.size()) {
                            val item: JsonElement = data[i].asJsonObject
                            val propertyServiceTypeModel: PropertyServiceTypeModel =
                                Gson().fromJson(item, PropertyServiceTypeModel::class.java)
                            propertyServiceTypeList.add(propertyServiceTypeModel)
                        }
                        callBackPropertyServiceType.onLoadSuccessFull(propertyServiceTypeList)
                    } else {
                        callBackPropertyServiceType.onLoadFailed(resObj["msg"].asString)
                    }
                }
            }

            override fun onError(error: String, resCode: Int) {
                callBackPropertyServiceType.onLoadFailed(error)
            }

        }))
    }

    fun createListRequestServiceProviderWs(
        context: Context,
        action: String,
        serviceId: String,
        hashMap: HashMap<String, Any>,
        callBackCreate: OnCallBackCreateServiceProvider
    ) {
        val serviceApi = RetrofitConnector.createService(context)
        val res = if (action == "detail_display") serviceApi.getListDetailPropertyServiceProvider(
            serviceId
        ) else serviceApi.listCreteServiceProvider(hashMap)
        res.enqueue(CustomCallback(context, object : CustomResponseListener {
            override fun onSuccess(resObj: JsonObject?) {
                if (resObj != null)
                    if (resObj.has("data")) {
                        val dataDetail = resObj["data"].asJsonObject
                        val serviceDetail: ServiceProviderDetailModel = Gson().fromJson(dataDetail.asJsonObject, ServiceProviderDetailModel::class.java
                        )
                        callBackCreate.onSuccessGetData(serviceDetail)
                    } else if (resObj.has("success")) {
                        if (resObj["success"].asBoolean) {
                            if (resObj.has("msg")) {
                                callBackCreate.onLoadSuccessFull(resObj["msg"].asString)
                            }
                        }
                    } else {
                        callBackCreate.onLoadFailed(resObj["msg"].asString)
                    }
            }

            override fun onError(error: String, resCode: Int) {
                callBackCreate.onLoadFailed(error)
            }

        }))
    }

    fun rescheduleAndAcceptServiceProviderWs(
        context: Context,
        hashMap: HashMap<String, Any>,
        callbackReschedule: OnCallBackRescheduleAndAccept
    ) {
        val serviceApi = RetrofitConnector.createService(context)
        val res = serviceApi.getRescheduleAndAcceptPropertyProvider(hashMap)
        res.enqueue(CustomCallback(context, object : CustomResponseListener {
            override fun onSuccess(resObj: JsonObject?) {
                if (resObj != null) {
                    if (resObj.has("success")) {
                        if (resObj["success"].asBoolean) {
                            if (resObj.has("msg")) {
                                callbackReschedule.onLoadSuccessFull(resObj["msg"].asString)
                            }
                        } else {
                            callbackReschedule.onLoadFailed(resObj["msg"].asString)
                        }
                    } else {
                        callbackReschedule.onLoadFailed(resObj["msg"].asString)
                    }
                }
            }

            override fun onError(error: String, resCode: Int) {
                callbackReschedule.onLoadFailed(error)
            }

        }))
    }

    interface OnCallBackListServiceListener {
        fun onLoadSuccessFull(listServiceProvider: ArrayList<PropertyServiceProviderListModel>)
        fun onLoadFailed(message: String)
    }

    interface OnCallBackDetailServiceListener {
        fun onLoadSuccessFull(detailServiceProvider: PropertyServiceProviderDetailModel)
        fun onLoadFailed(message: String)
    }

    interface OnCallBackTypeListener {
        fun onLoadSuccessFull(listServiceProvider: ArrayList<ServiceProviderTypeModel>)
        fun onLoadFailed(message: String)
    }

    interface OnCallBackCompanyListener {
        fun onLoadSuccessFull(listServiceProvider: ArrayList<ServiceProviderCompanyPropertyModel>)
        fun onLoadFailed(message: String)
    }

    interface OnCallBackServiceTermAndCategory {
        fun onLoadSuccessFull(serviceTermAndCategory: ServiceTermAndCategoryModel)
        fun onLoadFailed(message: String)
    }

    interface OnCallBackPropertyServiceType {
        fun onLoadSuccessFull(propertyServiceTypeList: ArrayList<PropertyServiceTypeModel>)
        fun onLoadFailed(message: String)
    }

    interface OnCallBackCreateServiceProvider {
        fun onSuccessGetData(serviceDetail: ServiceProviderDetailModel)
        fun onLoadSuccessFull(message: String)
        fun onLoadFailed(error: String)
    }

    interface OnCallBackRescheduleAndAccept {
        fun onLoadSuccessFull(message: String)
        fun onLoadFailed(error: String)
    }
}