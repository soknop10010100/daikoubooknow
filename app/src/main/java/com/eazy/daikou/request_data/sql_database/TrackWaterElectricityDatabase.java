package com.eazy.daikou.request_data.sql_database;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import androidx.annotation.Nullable;

import com.eazy.daikou.request_data.sql_database.reader.TrackUtilityReader;
import com.eazy.daikou.request_data.static_data.TypeTrackingWaterElectricity;
import com.eazy.daikou.model.utillity_tracking.model.FloorUtilNoModel;
import com.eazy.daikou.model.utillity_tracking.model.LastTrackData;
import com.eazy.daikou.model.utillity_tracking.model.TrackingWaterElectricityModel;
import com.eazy.daikou.model.utillity_tracking.model.UtilNoCommonAreaModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import static com.eazy.daikou.request_data.sql_database.reader.TrackUtilityReader.Floor.TABLE_NAME;
import static com.eazy.daikou.request_data.sql_database.reader.TrackUtilityReader.Floor.UTIL_LOCATION_ID;
import static com.eazy.daikou.helper.Utils.logDebug;

public class TrackWaterElectricityDatabase extends SQLiteOpenHelper {

    private final Context context;

    public TrackWaterElectricityDatabase(@Nullable Context context) {
        super(context, "tracking_water.db", null, 1);
        this.context = context;
    }

    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + TrackUtilityReader.Floor.TABLE_NAME
                    + " (" + TrackUtilityReader.Floor._ID
                    + " INTEGER PRIMARY KEY," + UTIL_LOCATION_ID
                    + " TEXT , " + TrackUtilityReader.Floor.SAVE_DATE
                    + " DATETIME DEFAULT CURRENT_TIMESTAMP)";

    private static final String SQL_CREATE_TABLE =
            "CREATE TABLE " + TrackUtilityReader.SaveFloorData.TABLE_NAME
                    + " (" + TrackUtilityReader.SaveFloorData._ID + " INTEGER PRIMARY KEY,"
                    + TrackUtilityReader.SaveFloorData.PROPERTY_ID + " TEXT , "
                    + TrackUtilityReader.SaveFloorData.UTILITY_TYPE + " TEXT , "
                    + TrackUtilityReader.SaveFloorData.TRACKING_DATE + " DATETIME DEFAULT CURRENT_TIMESTAMP , "
                    + TrackUtilityReader.SaveFloorData.CURRENT_USAGE + " TEXT ,"
                    + TrackUtilityReader.SaveFloorData.TOTAL_USAGE + " TEXT ,"
                    + TrackUtilityReader.SaveFloorData.UTILITY_PHOTO + " TEXT ,"
                    + TrackUtilityReader.SaveFloorData.LOCATION_ID + " TEXT ,"
                    + TrackUtilityReader.SaveFloorData.DATA_TYPE + " TEXT ,"
                    + TrackUtilityReader.SaveFloorData.PURPOSE + " TEXT)";

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_TABLE);
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    public void insertFloorDetail(HashMap<String, Object> hashMap){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(TrackUtilityReader.SaveFloorData.PROPERTY_ID, Objects.requireNonNull(hashMap.get("property_id")).toString());
        cv.put(TrackUtilityReader.SaveFloorData.PURPOSE, Objects.requireNonNull(hashMap.get("purpose")).toString());
        cv.put(TrackUtilityReader.SaveFloorData.UTILITY_TYPE, Objects.requireNonNull(hashMap.get("utility_type")).toString());
        cv.put(TrackUtilityReader.SaveFloorData.LOCATION_ID, Objects.requireNonNull(hashMap.get("location_id")).toString());
        cv.put(TrackUtilityReader.SaveFloorData.CURRENT_USAGE, Objects.requireNonNull(hashMap.get("current_usage")).toString());
        cv.put(TrackUtilityReader.SaveFloorData.TOTAL_USAGE, Objects.requireNonNull(hashMap.get("total_usage")).toString());
        cv.put(TrackUtilityReader.SaveFloorData.UTILITY_PHOTO, Objects.requireNonNull(hashMap.get("utility_photo_file")).toString());
        cv.put(TrackUtilityReader.SaveFloorData.DATA_TYPE, Objects.requireNonNull(hashMap.get("data_type")).toString());
        cv.put(TrackUtilityReader.SaveFloorData.TRACKING_DATE, Objects.requireNonNull(hashMap.get("tracking_date")).toString());

        db.insert(TrackUtilityReader.SaveFloorData.TABLE_NAME, null, cv);
    }

    public List<HashMap<String,String>> getFloorDetailList(String type){
        List<HashMap<String,String>> mapList = new ArrayList<>();
        String queryString = "SELECT * FROM " + TrackUtilityReader.SaveFloorData.TABLE_NAME + " WHERE data_type= '" + type +"' ";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(queryString, null);
        if (cursor.moveToFirst()) {
            do {
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("id",cursor.getString(0));
                hashMap.put("property_id",cursor.getString(1));
                hashMap.put("utility_type",cursor.getString(2));
                hashMap.put("tracking_date",cursor.getString(3));
                hashMap.put("current_usage",cursor.getString(4));
                hashMap.put("total_usage",cursor.getString(5));
                hashMap.put("utility_photo",cursor.getString(6));
                hashMap.put("location_id",cursor.getString(7));
                hashMap.put("data_type",cursor.getString(8));
                hashMap.put("purpose",cursor.getString(9));
                mapList.add(hashMap);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return mapList;
    }

    public TrackingWaterElectricityModel getFloorDetail(String type, String id){
        TrackingWaterElectricityModel trackingWaterElectricityModel = new TrackingWaterElectricityModel();
        String queryString = "SELECT * from " + TrackUtilityReader.SaveFloorData.TABLE_NAME + " WHERE location_id= '" + id + "' ";

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery(queryString, null);

        if (cursor.moveToFirst()) {
            do {

                if (type.equals(cursor.getString(8))) {
                    trackingWaterElectricityModel.setCurrentUsage(cursor.getString(4));
                    trackingWaterElectricityModel.setTotalUsage(cursor.getString(5));
                    trackingWaterElectricityModel.setTrackingImage(cursor.getString(6));
                }
            } while (cursor.moveToNext());
        }

        //close
        cursor.close();
        db.close();
        return trackingWaterElectricityModel;
    }

    public String getFloorSaveDate(){
        String date = "";
        String queryString = "SELECT * from " + TABLE_NAME + " WHERE _id= '" + TypeTrackingWaterElectricity.ID_FLOOR+ "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(queryString, null);
        if (cursor.moveToFirst()) {
            do {
                date = cursor.getString(2);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return date;
    }

    public List<UtilNoCommonAreaModel> searchUnitOrLocation(String floor,String name,String action) throws JSONException {
        List<UtilNoCommonAreaModel> floorUtilNoModels = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        String queryString = "SELECT * from " + TABLE_NAME;
        Cursor cursor = db.rawQuery(queryString, null);

        if (cursor.moveToFirst()) {
            do {
                String util_location = cursor.getString(1);
                JSONArray jsonArray = new JSONArray(util_location);
                for (int i = 0 ; i < jsonArray.length() - 2 ; i++){
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    String floorName = jsonObject.getString("floor_no");

                    if(floor.equals(floorName)) {
                        List<UtilNoCommonAreaModel> utilNoList = new ArrayList<>();

                        //get unit_no
                        if (action.equals(TypeTrackingWaterElectricity.WATER_TRACK) || action.equals(TypeTrackingWaterElectricity.ELECTRICITY_TRACK)){
                            for (int j = 0 ; j < jsonObject.getJSONArray("unit_no").length() ; j++){
                                UtilNoCommonAreaModel utilNoCommonAreaModel = new UtilNoCommonAreaModel();

                                String lastTrackDateWater = "", lastCurrentUsageWater="";
                                String lastTrackDateElectricity = "", lastCurrentUsageElectricity="", current_usage = "";
                                if (action.equals(TypeTrackingWaterElectricity.WATER_TRACK)){
                                    // get water util
                                    JSONObject jsonWater = jsonObject.getJSONArray("unit_no").getJSONObject(j).getJSONObject("previous_water");

                                    if (jsonWater.get("last_tracking_date") != null){
                                        lastTrackDateWater = jsonWater.getString("last_tracking_date");
                                    }
                                    if (jsonWater.get("last_current_usage") != null){
                                        lastCurrentUsageWater = jsonWater.getString("last_current_usage");
                                    }
                                } else {
                                    // get electricity util
                                    JSONObject jsonElectricity = jsonObject.getJSONArray("unit_no").getJSONObject(j).getJSONObject("previous_electric");
                                    if (jsonElectricity.get("last_tracking_date") != null){
                                        lastTrackDateElectricity = jsonElectricity.getString("last_tracking_date");
                                    }
                                    if (jsonElectricity.get("last_current_usage") != null){
                                        lastCurrentUsageElectricity = jsonElectricity.getString("last_current_usage");
                                    }
                                }

                                if (jsonObject.getJSONArray("unit_no").getJSONObject(j).get("current_usage") != null){
                                    current_usage = jsonObject.getJSONArray("unit_no").getJSONObject(j).get("current_usage").toString();
                                }
//                                utilNoCommonAreaModel.setLastTrackDataElectricity(new LastTrackData(lastTrackDateElectricity,lastCurrentUsageElectricity, jsonObject.getJSONArray("unit_no").getJSONObject(j).getBoolean("previous_electric")));
//                                utilNoCommonAreaModel.setLastTrackDataWater(new LastTrackData(lastTrackDateWater,lastCurrentUsageWater, jsonObject.getJSONArray("unit_no").getJSONObject(j).getBoolean("isAlreadyHaveTrack")));
                                utilNoCommonAreaModel.setId(jsonObject.getJSONArray("unit_no").getJSONObject(j).getString("unit_id"));
                                utilNoCommonAreaModel.setName(jsonObject.getJSONArray("unit_no").getJSONObject(j).getString("unit_no"));
                                utilNoList.add(utilNoCommonAreaModel);
                                if (jsonObject.getJSONArray("unit_no").getJSONObject(j).getString("unit_no").toLowerCase().contains(name.toLowerCase()))
                                    floorUtilNoModels.add(utilNoCommonAreaModel);
                            }
                        } else if (action.equals(TypeTrackingWaterElectricity.WATER_COMMON)) {
                            List<UtilNoCommonAreaModel> waterCommonAreaModelList = new ArrayList<>();
                            for (int k = 0 ; k < jsonObject.getJSONArray("water_common_location").length() ; k++){
                                UtilNoCommonAreaModel utilNoCommonAreaModel = new UtilNoCommonAreaModel();

                                // get common water
                                JSONObject jsonWater = jsonObject.getJSONArray("water_common_location").getJSONObject(k).getJSONObject("previous_common_water");
                                String lastTrackDateWater = "", lastCurrentUsageWater="", current_usage = "";
                                if (jsonWater.get("last_tracking_date") != null){
                                    lastTrackDateWater = jsonWater.getString("last_tracking_date");
                                }
                                if (jsonWater.get("last_current_usage") != null){
                                    lastCurrentUsageWater = jsonWater.getString("last_current_usage");
                                }

                                if (jsonObject.getJSONArray("unit_no").getJSONObject(k).get("current_usage") != null){
                                    current_usage = jsonObject.getJSONArray("unit_no").getJSONObject(k).get("current_usage").toString();
                                }
                                utilNoCommonAreaModel.setId(jsonObject.getJSONArray("water_common_location").getJSONObject(k).getString("id"));
                                utilNoCommonAreaModel.setName(jsonObject.getJSONArray("water_common_location").getJSONObject(k).getString("location"));
//                                utilNoCommonAreaModel.setLastTrackDataWater(new LastTrackData(lastTrackDateWater,lastCurrentUsageWater, jsonObject.getJSONArray("water_common_location").getJSONObject(k).getBoolean("isAlreadyHaveTrack")));
                                waterCommonAreaModelList.add(utilNoCommonAreaModel);
                                if (jsonObject.getJSONArray("water_common_location").getJSONObject(k).getString("location").toLowerCase().contains(name.toLowerCase()))
                                    floorUtilNoModels.add(utilNoCommonAreaModel);
                            }
                        } else if (action.equals(TypeTrackingWaterElectricity.ELECTRICITY_COMMON)) {
                            List<UtilNoCommonAreaModel> electricityCommonAreaModelList = new ArrayList<>();
                            for (int l = 0 ; l < jsonObject.getJSONArray("electric_common_location").length() ; l++){
                                UtilNoCommonAreaModel utilNoCommonAreaModel = new UtilNoCommonAreaModel();

                                // get common electricity
                                JSONObject jsonElectricity = jsonObject.getJSONArray("electric_common_location").getJSONObject(l).getJSONObject("previous_common_electric");
                                String lastTrackDateElectricity = "", lastCurrentUsageElectricity="", current_usage = "";
                                if (jsonElectricity.get("last_tracking_date") != null){
                                    lastTrackDateElectricity = jsonElectricity.getString("last_tracking_date");
                                }
                                if (jsonElectricity.get("last_current_usage") != null){
                                    lastCurrentUsageElectricity = jsonElectricity.getString("last_current_usage");
                                }

                                if (jsonObject.getJSONArray("unit_no").getJSONObject(l).get("current_usage") != null){
                                    current_usage = jsonObject.getJSONArray("unit_no").getJSONObject(l).get("current_usage").toString();
                                }
                                utilNoCommonAreaModel.setId(jsonObject.getJSONArray("electric_common_location").getJSONObject(l).getString("id"));
                                utilNoCommonAreaModel.setName(jsonObject.getJSONArray("electric_common_location").getJSONObject(l).getString("location"));
//                                utilNoCommonAreaModel.setLastTrackDataElectricity(new LastTrackData(lastTrackDateElectricity,lastCurrentUsageElectricity, jsonObject.getJSONArray("electric_common_location").getJSONObject(l).getBoolean("isAlreadyHaveTrack")));
                                electricityCommonAreaModelList.add(utilNoCommonAreaModel);
                                if (jsonObject.getJSONArray("electric_common_location").getJSONObject(l).getString("location").toLowerCase().contains(name.toLowerCase()))
                                    floorUtilNoModels.add(utilNoCommonAreaModel);
                            }
                        }

                    }
                }
            } while (cursor.moveToNext());
        }
        return floorUtilNoModels;
    }

    public List<FloorUtilNoModel> getAllFloor() throws JSONException {
        List<FloorUtilNoModel> floorUtilNoModels = new ArrayList<>();
        String queryString = "SELECT * from " + TABLE_NAME + " WHERE _id= '" + TypeTrackingWaterElectricity.ID_FLOOR+ "' ";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(queryString, null);

        if (cursor.moveToFirst()) {
            do {
                String util_location = cursor.getString(1);
                JSONArray jsonArray = new JSONArray(util_location);
                for (int i = 0 ; i < jsonArray.length() - 2 ; i++){
                    FloorUtilNoModel utilNoModel = new FloorUtilNoModel();
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    List<UtilNoCommonAreaModel> utilNoList = new ArrayList<>();
                    for (int j = 0 ; j < jsonObject.getJSONArray("utilNoModelList").length() ; j++){
                        UtilNoCommonAreaModel utilNoCommonAreaModel = new UtilNoCommonAreaModel();

                        // get water util
                        String lastTrackDateWater = "", lastCurrentUsageWater = "", current_usage_Water = "", totalWaterRecord = "";
                        boolean isAlreadyWater = false;
                        if (jsonObject.getJSONArray("utilNoModelList").length() > 0) {
                            if (jsonObject.getJSONArray("utilNoModelList").getJSONObject(j).has("lastTrackDataWater")) {
                                JSONObject jsonWater = jsonObject.getJSONArray("utilNoModelList").getJSONObject(j).getJSONObject("lastTrackDataWater");
                                if (jsonWater != null) {
                                    if (jsonWater.get("lastTrackingDate") != null) {
                                        lastTrackDateWater = jsonWater.getString("lastTrackingDate");
                                    }
                                    if (jsonWater.get("lastCurrentUsage") != null) {
                                        lastCurrentUsageWater = jsonWater.getString("lastCurrentUsage");
                                    }
                                    if (jsonWater.get("current_usage") != null){
                                        current_usage_Water = jsonWater.getString("current_usage");
                                    }
                                    if (jsonWater.getBoolean("isAlreadyHaveTrack")){
                                        isAlreadyWater = jsonWater.getBoolean("isAlreadyHaveTrack");
                                    }
                                }
                            }
                            if (jsonObject.getJSONArray("utilNoModelList").getJSONObject(j).has("total_recorded_amount_water")){
                                totalWaterRecord = jsonObject.getJSONArray("utilNoModelList").getJSONObject(j).getString("total_recorded_amount_water");
                            }
                        }

                        // get electricity util
                        String lastTrackDateElectricity = "", lastCurrentUsageElectricity="",  current_usage_Electricity = "", totalElectricRecord = "";
                        boolean isAlreadyElectric = false;
                        if (jsonObject.getJSONArray("utilNoModelList").length() > 0) {
                            if (jsonObject.getJSONArray("utilNoModelList").getJSONObject(j).has("lastTrackDataElectricity")) {
                                JSONObject jsonElectricity = jsonObject.getJSONArray("utilNoModelList").getJSONObject(j).getJSONObject("lastTrackDataElectricity");
                                if (jsonElectricity != null) {
                                    if (jsonElectricity.get("lastTrackingDate") != null) {
                                        lastTrackDateElectricity = jsonElectricity.getString("lastTrackingDate");
                                    }
                                    if (jsonElectricity.get("lastCurrentUsage") != null) {
                                        lastCurrentUsageElectricity = jsonElectricity.getString("lastCurrentUsage");
                                    }
                                    if (jsonElectricity.get("current_usage") != null){
                                        current_usage_Electricity = jsonElectricity.getString("current_usage");
                                    }
                                    if (jsonElectricity.getBoolean("isAlreadyHaveTrack")){
                                        isAlreadyElectric = jsonElectricity.getBoolean("isAlreadyHaveTrack");
                                    }
                                }
                            }
                            if (jsonObject.getJSONArray("utilNoModelList").getJSONObject(j).has("total_recorded_amount_electric")){
                                totalElectricRecord = jsonObject.getJSONArray("utilNoModelList").getJSONObject(j).getString("total_recorded_amount_electric");
                            }

                        }
                        utilNoCommonAreaModel.setTotal_recorded_amount_water(totalWaterRecord);
                        utilNoCommonAreaModel.setTotal_recorded_amount_electric(totalElectricRecord);
                        utilNoCommonAreaModel.setAlreadyHaveTrack(jsonObject.getJSONArray("utilNoModelList").getJSONObject(j).getBoolean("isAlreadyHaveTrack"));
                        utilNoCommonAreaModel.setLastTrackDataElectricity(new LastTrackData(lastTrackDateElectricity,lastCurrentUsageElectricity, current_usage_Electricity, isAlreadyElectric, ""));
                        utilNoCommonAreaModel.setLastTrackDataWater(new LastTrackData(lastTrackDateWater,lastCurrentUsageWater, current_usage_Water, isAlreadyWater, ""));
                        utilNoCommonAreaModel.setId(jsonObject.getJSONArray("utilNoModelList").getJSONObject(j).getString("id"));
                        utilNoCommonAreaModel.setName(jsonObject.getJSONArray("utilNoModelList").getJSONObject(j).getString("name"));
                        utilNoList.add(utilNoCommonAreaModel);
                    }

                    List<UtilNoCommonAreaModel> waterCommonAreaModelList = new ArrayList<>();
                    for (int k = 0 ; k < jsonObject.getJSONArray("waterCommonList").length() ; k++){
                        UtilNoCommonAreaModel utilNoCommonAreaModel = new UtilNoCommonAreaModel();

                        // get common water
                        JSONObject jsonWater = jsonObject.getJSONArray("waterCommonList").getJSONObject(k).getJSONObject("lastTrackDataWater");
                        String lastTrackDateWater = "", lastCurrentUsageWater="", current_usage_Water = "", totalWaterRecord = "";
                        boolean isAlreadyWater = false;
                        if (jsonWater != null) {
                            if (jsonWater.get("lastTrackingDate") != null) {
                                lastTrackDateWater = jsonWater.getString("lastTrackingDate");
                            }
                            if (jsonWater.get("lastCurrentUsage") != null) {
                                lastCurrentUsageWater = jsonWater.getString("lastCurrentUsage");
                            }

                            if (jsonWater.get("current_usage") != null) {
                                current_usage_Water = jsonWater.getString("current_usage");
                            }

                            if (jsonWater.getBoolean("isAlreadyHaveTrack")){
                                isAlreadyWater = jsonWater.getBoolean("isAlreadyHaveTrack");
                            }
                        }
                        if (jsonObject.getJSONArray("waterCommonList").getJSONObject(k).has("total_recorded_amount_water")){
                            totalWaterRecord = jsonObject.getJSONArray("waterCommonList").getJSONObject(k).getString("total_recorded_amount_water");
                        }
                        utilNoCommonAreaModel.setTotal_recorded_amount_water(totalWaterRecord);
                        utilNoCommonAreaModel.setAlreadyHaveTrack(jsonObject.getJSONArray("waterCommonList").getJSONObject(k).getBoolean("isAlreadyHaveTrack"));
                        utilNoCommonAreaModel.setId(jsonObject.getJSONArray("waterCommonList").getJSONObject(k).getString("id"));
                        utilNoCommonAreaModel.setName(jsonObject.getJSONArray("waterCommonList").getJSONObject(k).getString("name"));
                        utilNoCommonAreaModel.setLastTrackDataWater(new LastTrackData(lastTrackDateWater,lastCurrentUsageWater, current_usage_Water, isAlreadyWater, ""));
                        waterCommonAreaModelList.add(utilNoCommonAreaModel);
                    }

                    List<UtilNoCommonAreaModel> electricityCommonAreaModelList = new ArrayList<>();
                    for (int l = 0 ; l < jsonObject.getJSONArray("electricityCommonList").length() ; l++){
                        UtilNoCommonAreaModel utilNoCommonAreaModel = new UtilNoCommonAreaModel();

                        // get common electricity
                        JSONObject jsonElectricity = jsonObject.getJSONArray("electricityCommonList").getJSONObject(l).getJSONObject("lastTrackDataElectricity");
                        String lastTrackDateElectricity = "", lastCurrentUsageElectricity="", current_usage_Electricity = "", totalElectricRecord = "";
                        boolean isAlreadyElectric = false;
                        if (jsonElectricity != null) {
                            if (jsonElectricity.get("lastTrackingDate") != null) {
                                lastTrackDateElectricity = jsonElectricity.getString("lastTrackingDate");
                            }
                            if (jsonElectricity.get("lastCurrentUsage") != null) {
                                lastCurrentUsageElectricity = jsonElectricity.getString("lastCurrentUsage");
                            }
                            if (jsonElectricity.get("current_usage") != null) {
                                current_usage_Electricity = jsonElectricity.getString("current_usage");
                            }

                            if (jsonElectricity.getBoolean("isAlreadyHaveTrack")){
                                isAlreadyElectric = jsonElectricity.getBoolean("isAlreadyHaveTrack");
                            }
                        }
                        if (jsonObject.getJSONArray("electricityCommonList").getJSONObject(l).has("total_recorded_amount_electric")){
                            totalElectricRecord = jsonObject.getJSONArray("electricityCommonList").getJSONObject(l).getString("total_recorded_amount_electric");
                        }
                        utilNoCommonAreaModel.setTotal_recorded_amount_electric(totalElectricRecord);
                        utilNoCommonAreaModel.setAlreadyHaveTrack(jsonObject.getJSONArray("electricityCommonList").getJSONObject(l).getBoolean("isAlreadyHaveTrack"));
                        utilNoCommonAreaModel.setId(jsonObject.getJSONArray("electricityCommonList").getJSONObject(l).getString("id"));
                        utilNoCommonAreaModel.setName(jsonObject.getJSONArray("electricityCommonList").getJSONObject(l).getString("name"));
                        utilNoCommonAreaModel.setLastTrackDataElectricity(new LastTrackData(lastTrackDateElectricity,lastCurrentUsageElectricity, current_usage_Electricity, isAlreadyElectric, ""));
                        electricityCommonAreaModelList.add(utilNoCommonAreaModel);
                    }

                    utilNoModel.setFloorName(jsonObject.getString("floorName"));
                    utilNoModel.setFloorNoName(jsonObject.getString("floorNoName"));
                    utilNoModel.setUtilNoModelList(utilNoList);
                    utilNoModel.setWaterCommonList(waterCommonAreaModelList);
                    utilNoModel.setElectricityCommonList(electricityCommonAreaModelList);

                    floorUtilNoModels.add(utilNoModel);
                }

                // get PPWSA
                FloorUtilNoModel utilNoModel1 = new FloorUtilNoModel();
                JSONObject jsonObject1 = jsonArray.getJSONObject(jsonArray.length() - 1);
                List<UtilNoCommonAreaModel> PPSWAList = new ArrayList<>();
                for (int j = 0 ; j < jsonObject1.getJSONArray("waterCommonList").length() ; j++){
                    UtilNoCommonAreaModel utilNoCommonAreaModel = new UtilNoCommonAreaModel();
                    utilNoCommonAreaModel.setId(jsonObject1.getJSONArray("waterCommonList").getJSONObject(j).getString("id"));
                    utilNoCommonAreaModel.setName(jsonObject1.getJSONArray("waterCommonList").getJSONObject(j).getString("name"));

                    JSONObject jsonWater = jsonObject1.getJSONArray("waterCommonList").getJSONObject(j).getJSONObject("lastTrackDataWater");
                    String lastTrackDateWater = "", lastCurrentUsageWater="", current_usage_Water = "", totalWaterRecord = "";
                    boolean isAlreadyWater = false;
                    if (jsonWater != null) {
                        if (jsonWater.get("lastTrackingDate") != null) {
                            lastTrackDateWater = jsonWater.getString("lastTrackingDate");
                        }
                        if (jsonWater.get("lastCurrentUsage") != null) {
                            lastCurrentUsageWater = jsonWater.getString("lastCurrentUsage");
                        }
                        if (jsonWater.get("current_usage") != null){
                            current_usage_Water = jsonWater.getString("current_usage");
                        }
                        if (jsonWater.getBoolean("isAlreadyHaveTrack")){
                            isAlreadyWater = jsonWater.getBoolean("isAlreadyHaveTrack");
                        }
                    }
                    if (jsonObject1.getJSONArray("waterCommonList").getJSONObject(j).has("total_recorded_amount_water")){
                        totalWaterRecord = jsonObject1.getJSONArray("waterCommonList").getJSONObject(j).getString("total_recorded_amount_water");
                    }
                    utilNoCommonAreaModel.setTotal_recorded_amount_water(totalWaterRecord);
                    utilNoCommonAreaModel.setAlreadyHaveTrack(jsonObject1.getJSONArray("waterCommonList").getJSONObject(j).getBoolean("isAlreadyHaveTrack"));
                    utilNoCommonAreaModel.setLastTrackDataWater(new LastTrackData(lastTrackDateWater,lastCurrentUsageWater, current_usage_Water, isAlreadyWater, ""));
                    PPSWAList.add(utilNoCommonAreaModel);
                }
                utilNoModel1.setFloorName(TypeTrackingWaterElectricity.PPWSA_FLOOR); // if floor -1 is PPWSA
                utilNoModel1.setWaterCommonList(PPSWAList);

                // get EDC
                FloorUtilNoModel utilNoModel = new FloorUtilNoModel();
                JSONObject jsonObject = jsonArray.getJSONObject(jsonArray.length() - 2);
                List<UtilNoCommonAreaModel> EDCList = new ArrayList<>();
                for (int j = 0 ; j < jsonObject.getJSONArray("electricityCommonList").length() ; j++){
                    UtilNoCommonAreaModel utilNoCommonAreaModel = new UtilNoCommonAreaModel();
                    utilNoCommonAreaModel.setId(jsonObject.getJSONArray("electricityCommonList").getJSONObject(j).getString("id"));
                    utilNoCommonAreaModel.setName(jsonObject.getJSONArray("electricityCommonList").getJSONObject(j).getString("name"));

                    JSONObject jsonElectricity = jsonObject.getJSONArray("electricityCommonList").getJSONObject(j).getJSONObject("lastTrackDataElectricity");

                    String lastTrackDateElectricity = "", lastCurrentUsageElectricity="", current_usage_Electricity = "", totalElectricRecord = "";
                    boolean isAlreadyElectric = false;
                    if (jsonElectricity != null) {
                        if (jsonElectricity.get("lastTrackingDate") != null) {
                            lastTrackDateElectricity = jsonElectricity.getString("lastTrackingDate");
                        }
                        if (jsonElectricity.get("lastCurrentUsage") != null) {
                            lastCurrentUsageElectricity = jsonElectricity.getString("lastCurrentUsage");
                        }
                        if (jsonElectricity.get("current_usage") != null){
                            current_usage_Electricity = jsonElectricity.getString("current_usage");
                        }

                        if (jsonElectricity.getBoolean("isAlreadyHaveTrack")){
                            isAlreadyElectric = jsonElectricity.getBoolean("isAlreadyHaveTrack");
                        }
                    }

                    if (jsonObject.getJSONArray("electricityCommonList").getJSONObject(j).has("total_recorded_amount_electric")){
                        totalElectricRecord = jsonObject.getJSONArray("electricityCommonList").getJSONObject(j).getString("total_recorded_amount_electric");
                    }

                    utilNoCommonAreaModel.setTotal_recorded_amount_electric(totalElectricRecord);
                    utilNoCommonAreaModel.setAlreadyHaveTrack(jsonObject.getJSONArray("electricityCommonList").getJSONObject(j).getBoolean("isAlreadyHaveTrack"));
                    utilNoCommonAreaModel.setLastTrackDataElectricity(new LastTrackData(lastTrackDateElectricity,lastCurrentUsageElectricity, current_usage_Electricity, isAlreadyElectric, ""));
                    EDCList.add(utilNoCommonAreaModel);
                }
                utilNoModel.setFloorName(TypeTrackingWaterElectricity.EDC_FLOOR); // if floor -1 is EDC
                utilNoModel.setElectricityCommonList(EDCList);

                floorUtilNoModels.add(utilNoModel);
                floorUtilNoModels.add(utilNoModel1);

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        return floorUtilNoModels;
    }

    public List<FloorUtilNoModel> getAllFloorHaveDataV2(String action) throws JSONException {
        List<FloorUtilNoModel> floorUtilNoModels = new ArrayList<>();
        String queryString = "SELECT * from " + TABLE_NAME + " WHERE _id= '" + TypeTrackingWaterElectricity.STORE_FLOOR_ID(action)+ "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(queryString, null);

        if (cursor.moveToFirst()) {
            do {
                String util_location = cursor.getString(1);
                JSONArray jsonArray = new JSONArray(util_location);
                for (int i = 0 ; i < jsonArray.length() - 2 ; i++){
                    FloorUtilNoModel utilNoModel = new FloorUtilNoModel();
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    List<UtilNoCommonAreaModel> utilNoList = new ArrayList<>();
                    for (int j = 0 ; j < jsonObject.getJSONArray("utilNoModelList").length() ; j++){
                        UtilNoCommonAreaModel utilNoCommonAreaModel = new UtilNoCommonAreaModel();

                        // get water util
                        String lastTrackDateWater = "", lastCurrentUsageWater = "", current_usage_Water = "", totalWaterRecord = "";
                        boolean isAlreadyWater = false;
                        if (jsonObject.getJSONArray("utilNoModelList").length() > 0) {
                            if (jsonObject.getJSONArray("utilNoModelList").getJSONObject(j).has("lastTrackDataWater")) {
                                JSONObject jsonWater = jsonObject.getJSONArray("utilNoModelList").getJSONObject(j).getJSONObject("lastTrackDataWater");
                                if (jsonWater != null) {
                                    if (jsonWater.get("lastTrackingDate") != null) {
                                        lastTrackDateWater = jsonWater.getString("lastTrackingDate");
                                    }
                                    if (jsonWater.get("lastCurrentUsage") != null) {
                                        lastCurrentUsageWater = jsonWater.getString("lastCurrentUsage");
                                    }
                                    if (jsonWater.get("current_usage") != null){
                                        current_usage_Water = jsonWater.getString("current_usage");
                                    }
                                    if (jsonWater.getBoolean("isAlreadyHaveTrack")){
                                        isAlreadyWater = jsonWater.getBoolean("isAlreadyHaveTrack");
                                    }
                                }
                            }
                            if (jsonObject.getJSONArray("utilNoModelList").getJSONObject(j).has("total_recorded_amount_water")){
                                totalWaterRecord = jsonObject.getJSONArray("utilNoModelList").getJSONObject(j).getString("total_recorded_amount_water");
                            }

                        }

                        // get electricity util
                        String lastTrackDateElectricity = "", lastCurrentUsageElectricity="", current_usage_Electricity = "", totalElectricRecord = "";
                        boolean isAlreadyElectric = false;
                        if (jsonObject.getJSONArray("utilNoModelList").length() > 0) {
                            if (jsonObject.getJSONArray("utilNoModelList").getJSONObject(j).has("lastTrackDataElectricity")) {
                                JSONObject jsonElectricity = jsonObject.getJSONArray("utilNoModelList").getJSONObject(j).getJSONObject("lastTrackDataElectricity");
                                if (jsonElectricity != null) {
                                    if (jsonElectricity.get("lastTrackingDate") != null) {
                                        lastTrackDateElectricity = jsonElectricity.getString("lastTrackingDate");
                                    }
                                    if (jsonElectricity.get("lastCurrentUsage") != null) {
                                        lastCurrentUsageElectricity = jsonElectricity.getString("lastCurrentUsage");
                                    }
                                    if (jsonElectricity.get("current_usage") != null){
                                        current_usage_Electricity = jsonElectricity.getString("current_usage");
                                    }
                                    if (jsonElectricity.getBoolean("isAlreadyHaveTrack")){
                                        isAlreadyElectric = jsonElectricity.getBoolean("isAlreadyHaveTrack");
                                    }
                                }
                            }
                            if (jsonObject.getJSONArray("utilNoModelList").getJSONObject(j).has("total_recorded_amount_electric")){
                                totalElectricRecord = jsonObject.getJSONArray("utilNoModelList").getJSONObject(j).getString("total_recorded_amount_electric");
                            }
                        }

                        utilNoCommonAreaModel.setTotal_recorded_amount_water(totalWaterRecord);
                        utilNoCommonAreaModel.setTotal_recorded_amount_electric(totalElectricRecord);
                        utilNoCommonAreaModel.setAlreadyHaveTrack(jsonObject.getJSONArray("utilNoModelList").getJSONObject(j).getBoolean("isAlreadyHaveTrack"));
                        utilNoCommonAreaModel.setLastTrackDataElectricity(new LastTrackData(lastTrackDateElectricity,lastCurrentUsageElectricity, current_usage_Electricity, isAlreadyElectric, ""));
                        utilNoCommonAreaModel.setLastTrackDataWater(new LastTrackData(lastTrackDateWater,lastCurrentUsageWater, current_usage_Water, isAlreadyWater, ""));
                        utilNoCommonAreaModel.setId(jsonObject.getJSONArray("utilNoModelList").getJSONObject(j).getString("id"));
                        utilNoCommonAreaModel.setName(jsonObject.getJSONArray("utilNoModelList").getJSONObject(j).getString("name"));
                        utilNoList.add(utilNoCommonAreaModel);
                    }

                    List<UtilNoCommonAreaModel> waterCommonAreaModelList = new ArrayList<>();
                    for (int k = 0 ; k < jsonObject.getJSONArray("waterCommonList").length() ; k++){
                        UtilNoCommonAreaModel utilNoCommonAreaModel = new UtilNoCommonAreaModel();

                        // get common water
                        JSONObject jsonWater = jsonObject.getJSONArray("waterCommonList").getJSONObject(k).getJSONObject("lastTrackDataWater");
                        String lastTrackDateWater = "", lastCurrentUsageWater="", current_usage_Water = "", totalWaterRecord = "";
                        boolean isAlreadyWater = false;
                        if (jsonWater.get("lastTrackingDate") != null){
                            lastTrackDateWater = jsonWater.getString("lastTrackingDate");
                        }
                        if (jsonWater.get("lastCurrentUsage") != null){
                            lastCurrentUsageWater = jsonWater.getString("lastCurrentUsage");
                        }

                        if (jsonWater.get("current_usage") != null){
                            current_usage_Water = jsonWater.getString("current_usage");
                        }

                        if (jsonWater.getBoolean("isAlreadyHaveTrack")){
                            isAlreadyWater = jsonWater.getBoolean("isAlreadyHaveTrack");
                        }
                        if (jsonObject.getJSONArray("waterCommonList").getJSONObject(k).has("total_recorded_amount_water")){
                            totalWaterRecord = jsonObject.getJSONArray("waterCommonList").getJSONObject(k).getString("total_recorded_amount_water");
                        }

                        utilNoCommonAreaModel.setTotal_recorded_amount_water(totalWaterRecord);
                        utilNoCommonAreaModel.setAlreadyHaveTrack(jsonObject.getJSONArray("waterCommonList").getJSONObject(k).getBoolean("isAlreadyHaveTrack"));
                        utilNoCommonAreaModel.setId(jsonObject.getJSONArray("waterCommonList").getJSONObject(k).getString("id"));
                        utilNoCommonAreaModel.setName(jsonObject.getJSONArray("waterCommonList").getJSONObject(k).getString("name"));
                        utilNoCommonAreaModel.setLastTrackDataWater(new LastTrackData(lastTrackDateWater,lastCurrentUsageWater, current_usage_Water, isAlreadyWater, ""));
                        waterCommonAreaModelList.add(utilNoCommonAreaModel);
                    }

                    List<UtilNoCommonAreaModel> electricityCommonAreaModelList = new ArrayList<>();
                    for (int l = 0 ; l < jsonObject.getJSONArray("electricityCommonList").length() ; l++){
                        UtilNoCommonAreaModel utilNoCommonAreaModel = new UtilNoCommonAreaModel();

                        // get common electricity
                        JSONObject jsonElectricity = jsonObject.getJSONArray("electricityCommonList").getJSONObject(l).getJSONObject("lastTrackDataElectricity");
                        String lastTrackDateElectricity = "", lastCurrentUsageElectricity="", current_usage_Electricity = "", totalElectricRecord = "";
                        boolean isAlreadyElectric = false;
                        if (jsonElectricity.get("lastTrackingDate") != null){
                            lastTrackDateElectricity = jsonElectricity.getString("lastTrackingDate");
                        }
                        if (jsonElectricity.get("lastCurrentUsage") != null){
                            lastCurrentUsageElectricity = jsonElectricity.getString("lastCurrentUsage");
                        }

                        if (jsonElectricity.get("current_usage") != null){
                            current_usage_Electricity = jsonElectricity.getString("current_usage");
                        }

                        if (jsonElectricity.getBoolean("isAlreadyHaveTrack")){
                            isAlreadyElectric = jsonElectricity.getBoolean("isAlreadyHaveTrack");
                        }
                        if (jsonObject.getJSONArray("electricityCommonList").getJSONObject(l).has("total_recorded_amount_electric")){
                            totalElectricRecord = jsonObject.getJSONArray("electricityCommonList").getJSONObject(l).getString("total_recorded_amount_electric");
                        }
                        utilNoCommonAreaModel.setTotal_recorded_amount_electric(totalElectricRecord);
                        utilNoCommonAreaModel.setAlreadyHaveTrack(jsonObject.getJSONArray("electricityCommonList").getJSONObject(l).getBoolean("isAlreadyHaveTrack"));
                        utilNoCommonAreaModel.setId(jsonObject.getJSONArray("electricityCommonList").getJSONObject(l).getString("id"));
                        utilNoCommonAreaModel.setName(jsonObject.getJSONArray("electricityCommonList").getJSONObject(l).getString("name"));
                        utilNoCommonAreaModel.setLastTrackDataElectricity(new LastTrackData(lastTrackDateElectricity,lastCurrentUsageElectricity, current_usage_Electricity, isAlreadyElectric, ""));
                        electricityCommonAreaModelList.add(utilNoCommonAreaModel);
                    }

                    utilNoModel.setFloorName(jsonObject.getString("floorName"));
                    utilNoModel.setFloorNoName(jsonObject.getString("floorNoName"));
                    utilNoModel.setUtilNoModelList(utilNoList);
                    utilNoModel.setWaterCommonList(waterCommonAreaModelList);
                    utilNoModel.setElectricityCommonList(electricityCommonAreaModelList);

                    floorUtilNoModels.add(utilNoModel);
                }

                // get PPWSA
                FloorUtilNoModel utilNoModel1 = new FloorUtilNoModel();
                JSONObject jsonObject1 = jsonArray.getJSONObject(jsonArray.length() - 1);
                List<UtilNoCommonAreaModel> PPSWAList = new ArrayList<>();
                for (int j = 0 ; j < jsonObject1.getJSONArray("waterCommonList").length() ; j++){
                    UtilNoCommonAreaModel utilNoCommonAreaModel = new UtilNoCommonAreaModel();
                    utilNoCommonAreaModel.setId(jsonObject1.getJSONArray("waterCommonList").getJSONObject(j).getString("id"));
                    utilNoCommonAreaModel.setName(jsonObject1.getJSONArray("waterCommonList").getJSONObject(j).getString("name"));

                    JSONObject jsonWater = jsonObject1.getJSONArray("waterCommonList").getJSONObject(j).getJSONObject("lastTrackDataWater");
                    String lastTrackDateWater = "", lastCurrentUsageWater="", current_usage_Water = "", totalWaterRecord = "";
                    boolean isAlreadyWater = false;
                    if (jsonWater.get("lastTrackingDate") != null){
                        lastTrackDateWater = jsonWater.getString("lastTrackingDate");
                    }
                    if (jsonWater.get("lastCurrentUsage") != null){
                        lastCurrentUsageWater = jsonWater.getString("lastCurrentUsage");
                    }

                    if (jsonWater.get("current_usage") != null){
                        current_usage_Water = jsonWater.getString("current_usage");
                    }

                    if (jsonWater.getBoolean("isAlreadyHaveTrack")){
                        isAlreadyWater = jsonWater.getBoolean("isAlreadyHaveTrack");
                    }
                    if (jsonObject1.getJSONArray("waterCommonList").getJSONObject(j).has("total_recorded_amount_water")){
                        totalWaterRecord = jsonObject1.getJSONArray("waterCommonList").getJSONObject(j).getString("total_recorded_amount_water");
                    }
                    utilNoCommonAreaModel.setTotal_recorded_amount_water(totalWaterRecord);
                    utilNoCommonAreaModel.setAlreadyHaveTrack(jsonObject1.getJSONArray("waterCommonList").getJSONObject(j).getBoolean("isAlreadyHaveTrack"));
                    utilNoCommonAreaModel.setLastTrackDataWater(new LastTrackData(lastTrackDateWater,lastCurrentUsageWater, current_usage_Water, isAlreadyWater, ""));
                    PPSWAList.add(utilNoCommonAreaModel);
                }
                utilNoModel1.setFloorName(TypeTrackingWaterElectricity.PPWSA_FLOOR); // if floor -1 is PPWSA
                utilNoModel1.setWaterCommonList(PPSWAList);

                // get EDC
                FloorUtilNoModel utilNoModel = new FloorUtilNoModel();
                JSONObject jsonObject = jsonArray.getJSONObject(jsonArray.length() - 2);
                List<UtilNoCommonAreaModel> EDCList = new ArrayList<>();
                for (int j = 0 ; j < jsonObject.getJSONArray("electricityCommonList").length() ; j++){
                    UtilNoCommonAreaModel utilNoCommonAreaModel = new UtilNoCommonAreaModel();
                    utilNoCommonAreaModel.setId(jsonObject.getJSONArray("electricityCommonList").getJSONObject(j).getString("id"));
                    utilNoCommonAreaModel.setName(jsonObject.getJSONArray("electricityCommonList").getJSONObject(j).getString("name"));

                    JSONObject jsonElectricity = jsonObject.getJSONArray("electricityCommonList").getJSONObject(j).getJSONObject("lastTrackDataElectricity");

                    String lastTrackDateElectricity = "", lastCurrentUsageElectricity="", current_usage_Electricity = "", totalElectricRecord = "";
                    boolean isAlreadyElectric = false;
                    if (jsonElectricity.get("lastTrackingDate") != null){
                        lastTrackDateElectricity = jsonElectricity.getString("lastTrackingDate");
                    }
                    if (jsonElectricity.get("lastCurrentUsage") != null){
                        lastCurrentUsageElectricity = jsonElectricity.getString("lastCurrentUsage");
                    }

                    if (jsonElectricity.get("current_usage") != null){
                        current_usage_Electricity = jsonElectricity.getString("current_usage");
                    }
                    if (jsonElectricity.getBoolean("isAlreadyHaveTrack")){
                        isAlreadyElectric = jsonElectricity.getBoolean("isAlreadyHaveTrack");
                    }
                    if (jsonObject.getJSONArray("electricityCommonList").getJSONObject(j).has("total_recorded_amount_electric")){
                        totalElectricRecord = jsonObject.getJSONArray("electricityCommonList").getJSONObject(j).getString("total_recorded_amount_electric");
                    }
                    utilNoCommonAreaModel.setTotal_recorded_amount_electric(totalElectricRecord);
                    utilNoCommonAreaModel.setAlreadyHaveTrack(jsonObject.getJSONArray("electricityCommonList").getJSONObject(j).getBoolean("isAlreadyHaveTrack"));
                    utilNoCommonAreaModel.setLastTrackDataElectricity(new LastTrackData(lastTrackDateElectricity,lastCurrentUsageElectricity, current_usage_Electricity, isAlreadyElectric, ""));
                    EDCList.add(utilNoCommonAreaModel);
                }
                utilNoModel.setFloorName(TypeTrackingWaterElectricity.EDC_FLOOR); // if floor -1 is EDC
                utilNoModel.setElectricityCommonList(EDCList);

                floorUtilNoModels.add(utilNoModel);
                floorUtilNoModels.add(utilNoModel1);

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        return floorUtilNoModels;
    }

    public void deleteAllRecordInFloorDetail(String type){
        SQLiteDatabase db = this.getReadableDatabase();
        db.execSQL("delete from "+ TrackUtilityReader.SaveFloorData.TABLE_NAME + " WHERE data_type= '" + type +"' ");
        db.close();
    }

    public void deleteRecordInFloorDetail(String id){
        SQLiteDatabase database = this.getReadableDatabase();
        database.execSQL("delete from "+ TrackUtilityReader.SaveFloorData.TABLE_NAME + " WHERE _id= '" + id + "'");
        database.close();
    }

    public void deleteAllFloorRecord(){
        SQLiteDatabase database = this.getReadableDatabase();
        database.execSQL("delete from "+ TrackUtilityReader.Floor.TABLE_NAME);
        database.close();
    }

    public void deleteAllFloorHaveRecord(String action){
        SQLiteDatabase database = this.getReadableDatabase();
        database.execSQL("delete from "+ TrackUtilityReader.Floor.TABLE_NAME + " WHERE _id= '" + TypeTrackingWaterElectricity.STORE_FLOOR_ID(action)+ "'");
        database.close();
    }

    public void deleteAllFloorRecordFloorDetail(){
        SQLiteDatabase database = this.getReadableDatabase();
        database.execSQL("delete from "+ TrackUtilityReader.SaveFloorData.TABLE_NAME);
        database.close();
    }

    public void updateFloor(String utilLocation) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(TrackUtilityReader.Floor._ID, TypeTrackingWaterElectricity.ID_FLOOR);
        cv.put(UTIL_LOCATION_ID, utilLocation);

        @SuppressLint("SimpleDateFormat") SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        cv.put(TrackUtilityReader.Floor.SAVE_DATE, formatter.format(new Date()));
        db.update(TABLE_NAME, cv, "_id = ?", new String[]{TypeTrackingWaterElectricity.ID_FLOOR});
        logDebug("logcallback", "updateFloor: ");
    }

    public void insertFloor(String utilLocation) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(TrackUtilityReader.Floor._ID, TypeTrackingWaterElectricity.ID_FLOOR);
        cv.put(UTIL_LOCATION_ID, utilLocation);

        @SuppressLint("SimpleDateFormat") SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        cv.put(TrackUtilityReader.Floor.SAVE_DATE, formatter.format(new Date()));
        db.insert(TABLE_NAME, null, cv);
        logDebug("logcallback", "createFloor: ");
    }

    public void insertFloorHaveData(String utilLocation, String action) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(TrackUtilityReader.Floor._ID, TypeTrackingWaterElectricity.STORE_FLOOR_ID(action));
        cv.put(UTIL_LOCATION_ID, utilLocation);

        @SuppressLint("SimpleDateFormat") SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        cv.put(TrackUtilityReader.Floor.SAVE_DATE, formatter.format(new Date()));
        db.insert(TABLE_NAME, null, cv);
    }
    
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
}
