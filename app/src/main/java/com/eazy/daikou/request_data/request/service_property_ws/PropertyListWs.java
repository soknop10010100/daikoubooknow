package com.eazy.daikou.request_data.request.service_property_ws;

import android.content.Context;

import com.eazy.daikou.R;
import com.eazy.daikou.repository_ws.CustomCallback;
import com.eazy.daikou.repository_ws.CustomResponseListener;
import com.eazy.daikou.model.my_property.service_provider.LatLongPropertyModel;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.eazy.daikou.repository_ws.RetrofitConnector;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PropertyListWs {

    public void updateLocationProject(Context context, HashMap<String, Object> hashMap, UpdateLocationCallBack updateLocationCallBack){
        Call<JsonObject> res = RetrofitConnector.createService(context).updateLocationProject(hashMap);
        res.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NotNull Call<JsonObject> call, @NotNull Response<JsonObject> response) {
                if (response.isSuccessful() && response.body() != null){
                    String msg = response.body().get("msg").getAsString();
                    String status = response.body().get("success").getAsString();
                    if (status.equals("true")){
                        updateLocationCallBack.onStatusSuccess(msg);
                    } else {
                        updateLocationCallBack.onStatusFalse(msg);
                    }

                } else {
                    if (response.errorBody() != null) {
                        ResponseBody errorString = response.errorBody();
                        try {
                            String json = errorString.string();
                            JSONObject jsonObject1 = new JSONObject(json);
                            if (jsonObject1.has("msg")){
                                String message = jsonObject1.getString("msg");
                                updateLocationCallBack.onStatusFalse(message);
                            }
                        } catch (IOException | JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<JsonObject> call, @NotNull Throwable t) {
                updateLocationCallBack.onLoadingFailed(context.getString(R.string.please_check_your_internet_connection));
            }
        });
    }

    public void detailByLatLong(Context context, String branchId, CallBackDetailListLatLongListener callBackListLatLongListener){
        Call<JsonElement> res = RetrofitConnector.createService(context).detailOnLatLong(branchId);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data")){
                    if (!resObj.get("data").isJsonNull()) {
                        JsonObject jsonObject = resObj.get("data").getAsJsonObject();
                        LatLongPropertyModel latLongPropertyModel = new Gson().fromJson(jsonObject, LatLongPropertyModel.class);
                        callBackListLatLongListener.getDetailListLatLong(latLongPropertyModel);
                    }
                } else {
                    callBackListLatLongListener.onLoadingFailed(resObj.get("msg").getAsString());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                callBackListLatLongListener.onLoadingFailed(error);
            }
        }));
    }

    public interface CallBackDetailListLatLongListener{
        void getDetailListLatLong(LatLongPropertyModel latLongPropertyModels);
        void onLoadingFailed(String msg);
    }

    public interface UpdateLocationCallBack{
        void onStatusSuccess(String msgSuccess);
        void onStatusFalse(String msgFailed);
        void onLoadingFailed(String msg);
    }
}
