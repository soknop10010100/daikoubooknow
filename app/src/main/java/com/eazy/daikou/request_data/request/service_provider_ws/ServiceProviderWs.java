package com.eazy.daikou.request_data.request.service_provider_ws;

import android.content.Context;

import com.eazy.daikou.repository_ws.RetrofitConnector;
import com.eazy.daikou.repository_ws.CustomCallback;
import com.eazy.daikou.repository_ws.CustomResponseListener;
import com.eazy.daikou.repository_ws.ServiceApi;
import com.eazy.daikou.model.my_property.service_provider.LatLngServiceProvider;
import com.eazy.daikou.model.my_property.service_provider.MenuServiceProvider;
import com.eazy.daikou.model.my_property.service_provider.ServiceProvider;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

public class ServiceProviderWs {

    public void getServiceProviderTitle(Context context, RequestServiceProvider request) {
        List<MenuServiceProvider> serviceList = new ArrayList<>();
        ServiceApi serviceApi = RetrofitConnector.createService( context);
        Call<JsonElement> res = serviceApi.getListServiceProvider();
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data")) {
                    JsonArray jsonArray = resObj.get("data").getAsJsonArray();
                    for (int i = 0; i < jsonArray.size(); i++) {
                        JsonObject item = jsonArray.get(i).getAsJsonObject();
                        MenuServiceProvider serviceModel = new Gson().fromJson(item.toString(), MenuServiceProvider.class);
                        serviceList.add(serviceModel);
                    }
                    request.onLoadSuccessFullCategory(serviceList);
                }
            }

            @Override
            public void onError(String error, int resCode) {
                request.OnLoadFail(error);
            }
        }));
    }

    public void getLatLngServiceProvider(Context context,int page,int size,String id, RequestLatLngServiceProvider request){
        List<LatLngServiceProvider> serviceList = new ArrayList<>();
        ServiceApi serviceApi = RetrofitConnector.createService( context);
        Call<JsonElement> res = serviceApi.getLatLongListServiceProvider(page,size,id);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data")) {
                    JsonArray jsonArray = resObj.get("data").getAsJsonArray();
                    for (int i = 0; i < jsonArray.size(); i++) {
                        JsonObject item = jsonArray.get(i).getAsJsonObject();
                        LatLngServiceProvider serviceModel = new Gson().fromJson(item.toString(), LatLngServiceProvider.class);
                        serviceList.add(serviceModel);
                    }
                    request.onLoadServiceProvider(serviceList);
                }
            }

            @Override
            public void onError(String error, int resCode) {
                request.OnLoadFail(error);
            }
        }));
    }

    public void getLatLngServiceProviderV2(Context context, int page, int size, String id, RequestLatLngServiceProvider request){
        List<LatLngServiceProvider> serviceList = new ArrayList<>();
        ServiceApi serviceApi = RetrofitConnector.createService( context);
        Call<JsonElement> res = serviceApi.getLatLongListServiceProviderV2(page, size, id);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data")) {
                    JsonArray jsonArray = resObj.get("data").getAsJsonArray();
                    for (int i = 0; i < jsonArray.size(); i++) {
                        JsonObject item = jsonArray.get(i).getAsJsonObject();
                        LatLngServiceProvider serviceModel = new Gson().fromJson(item.toString(), LatLngServiceProvider.class);
                        serviceList.add(serviceModel);
                    }
                    request.onLoadServiceProvider(serviceList);
                }
            }

            @Override
            public void onError(String error, int resCode) {
                request.OnLoadFail(error);
            }
        }));
    }

    public void getDetailByIdLatLong(Context context,String id, DetailByIdServiceProvider request){
        ServiceApi serviceApi = RetrofitConnector.createService( context);
        Call<JsonElement> res = serviceApi.getDetailLatLongById(id);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data")){
                    if (!resObj.get("data").isJsonNull()) {
                        JsonObject item = resObj.get("data").getAsJsonObject();
                        LatLngServiceProvider serviceModel = new Gson().fromJson(item.toString(), LatLngServiceProvider.class);
                        request.onLoadServiceProvider(serviceModel);
                    }
                } else {
                    request.OnLoadFail(resObj.get("msg").getAsString());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                request.OnLoadFail(error);
            }
        }));
    }

    public void getSeeMoreServiceProvider(Context context ,String categoryId, String city, int page, int limit,RequestMoreServiceProvider request){
        List<ServiceProvider> serviceList = new ArrayList<>();
        Call<JsonElement> res = RetrofitConnector.createService(context).getSeeMoreServiceProviders(page,limit,categoryId, city);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data")){
                    JsonArray jsonArray = resObj.get("data").getAsJsonArray();
                    for (int i = 0; i <jsonArray.size() ; i++) {
                        JsonObject item = jsonArray.get(i).getAsJsonObject();
                        ServiceProvider serviceModel = new Gson().fromJson(item.toString(), ServiceProvider.class);
                        serviceList.add(serviceModel);
                    }
                    request.onLoadServiceProvider(serviceList);
                }
            }

            @Override
            public void onError(String error, int resCode) {
                request.OnLoadFail(error);
            }
        }));
    }



    public interface RequestServiceProvider{
        void onLoadSuccessFullCategory(List<MenuServiceProvider> serviceProviderList);
        void OnLoadFail(String message);
    }

    public interface RequestMoreServiceProvider{
        void onLoadServiceProvider(List<ServiceProvider> serviceProvider);
        void OnLoadFail(String message);
    }
    public interface RequestLatLngServiceProvider{
        void onLoadServiceProvider(List<LatLngServiceProvider> serviceProvider);
        void OnLoadFail(String message);
    }

    public interface DetailByIdServiceProvider{
        void onLoadServiceProvider(LatLngServiceProvider serviceProvider);
        void OnLoadFail(String message);
    }
}
