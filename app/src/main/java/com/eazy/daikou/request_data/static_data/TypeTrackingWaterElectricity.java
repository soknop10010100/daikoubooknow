package com.eazy.daikou.request_data.static_data;

import java.util.HashMap;

public class TypeTrackingWaterElectricity {

    public static String EDC_FLOOR = "CODE_EDC_FLOOR";
    public static String PPWSA_FLOOR = "CODE_PPWSA_FLOOR";
    public static String ID_FLOOR = "1";
    public static String ID_FLOOR_HAVE_DATA = "2";

    // utility type
    public static String WATER_TRACK = "water_track";
    public static String WATER_COMMON = "water_common";
    public static String WATER_PPWSA = "water_ppwsa";
    public static String ELECTRICITY_TRACK = "electricity_track";
    public static String ELECTRICITY_COMMON = "electricity_common";
    public static String ELECTRICITY_EDC = "electricity_edc";

    // utility purpose
    public static String UTILITY_PURPOSE_CREATE = "create";
    public static String UTILITY_PURPOSE_UPDATE = "update";

    public static HashMap<String,String> getTypeOfUtility(){
        HashMap<String,String> hashMap = new HashMap<>();
        // key is action and value is for api
        hashMap.put("water_track","water");
        hashMap.put("water_common","water");
        hashMap.put("water_ppwsa","water");
        hashMap.put("electricity_track","electricity");
        hashMap.put("electricity_common","electricity");
        hashMap.put("electricity_edc","electricity");
        return hashMap;
    }

    public static HashMap<String,String> getUtilityType(){
        HashMap<String,String> hashMap = new HashMap<>();
        // key is action and value is for api
        hashMap.put("water_track","unit");
        hashMap.put("water_common","common_area");
        hashMap.put("water_ppwsa","ppwsa");
        hashMap.put("electricity_track","unit");
        hashMap.put("electricity_common","common_area");
        hashMap.put("electricity_edc","edc");
        return hashMap;
    }

    public static String STORE_FLOOR_ID(String action){
        String id = "1";
        switch (action) {
            case "water_track":
                return "2";
            case "water_common":
                return "3";
            case "water_ppwsa":
                return "4";
            case "electricity_track":
                return "5";
            case "electricity_common":
                return "6";
            case "electricity_edc":
                return "7";
        }
        return id;
    }
}
