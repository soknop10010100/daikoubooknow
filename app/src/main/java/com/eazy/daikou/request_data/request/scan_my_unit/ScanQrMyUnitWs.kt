package com.eazy.daikou.request_data.request.scan_my_unit

import android.content.Context
import com.eazy.daikou.repository_ws.RetrofitConnector
import com.eazy.daikou.repository_ws.CustomCallback
import com.eazy.daikou.repository_ws.CustomResponseListener
import com.eazy.daikou.model.my_unit_info.MyUnitScanInfoModel
import com.google.gson.Gson
import com.google.gson.JsonObject

class ScanQrMyUnitWs {
    fun getItemMyUnitFromQrCode(context: Context, userId : String, qrCode: String, activeUserType : String, accountId : String, callBackListener: OnCallBackListener){
        val api = RetrofitConnector.createService(context)
        val res = api.getInfoFromMyUnit(userId, qrCode, activeUserType, accountId)
        res.enqueue(CustomCallback(context, object : CustomResponseListener {
            override fun onSuccess(resObj: JsonObject) {
                if (resObj.has("data")) {
                    val data = resObj["data"].asJsonObject
                    val myUnitInfoModel = Gson().fromJson(data.asJsonObject, MyUnitScanInfoModel::class.java)
                    callBackListener.onSuccess(myUnitInfoModel)
                } else {
                    callBackListener.onLoadFail(resObj.get("msg").asString)
                }
            }

            override fun onError(error: String, resCode: Int) {
                callBackListener.onLoadFail(error)
            }
        }))
    }

    interface OnCallBackListener {
        fun onSuccess(myUnitInfo : MyUnitScanInfoModel)
        fun onLoadFail(message: String)
    }
}