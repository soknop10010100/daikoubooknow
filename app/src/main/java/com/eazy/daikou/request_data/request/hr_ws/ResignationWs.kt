package com.eazy.daikou.request_data.request.hr_ws

import android.content.Context
import com.eazy.daikou.repository_ws.CustomCallback
import com.eazy.daikou.repository_ws.CustomResponseListener
import com.eazy.daikou.repository_ws.RetrofitGenerator
import com.eazy.daikou.model.hr.*
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.google.gson.JsonObject

class ResignationWs {

    fun getResignationWs(context : Context, page :  Int, limit : Int, accountBusinessKey : String , userBusinessKey : String, resignationCallBack : OnResignationCallBackListener ){
        val serviceApi = RetrofitGenerator.createServiceWithHR(context)
        val res = serviceApi.getListResignation(page, limit, accountBusinessKey, userBusinessKey)
        res.enqueue(CustomCallback (context, object : CustomResponseListener{
            override fun onSuccess(resObj: JsonObject?) {
                if (resObj != null) {
                    if (resObj.has("data")){
                        val listResignation: ArrayList<ResignationHrModel> = ArrayList()
                        val data = resObj["data"].asJsonArray
                        for (i in 0 until data.size()){
                            val item: JsonElement = data[i].asJsonObject
                            val resignationHrModel : ResignationHrModel = Gson().fromJson(item, ResignationHrModel::class.java)
                            listResignation.add(resignationHrModel)
                        }
                        resignationCallBack.onSuccessful(listResignation)
                    } else {
                        resignationCallBack.onFailed(resObj.get("msg").asString)
                    }
                }
            }

            override fun onError(error: String, resCode: Int) {
                resignationCallBack.onFailed(error)
            }

        }))
    }

    fun getResignationDetailWs(context: Context, resignListId : String, onDetailResignation : OnResignationDetailCallBackListener){
        val serviceApi = RetrofitGenerator.createServiceWithHR(context)
        val res = serviceApi.getListDetailResignation(resignListId)
        res.enqueue(CustomCallback(context, object : CustomResponseListener{
            override fun onSuccess(resObj: JsonObject?) {
                if (resObj != null){
                    if (resObj.has("data")){
                        val data = resObj["data"].asJsonObject
                        val resignationDetailModel : ResignationDetailModel = Gson().fromJson(data, ResignationDetailModel::class.java)
                        onDetailResignation.onSuccessful(resignationDetailModel)
                    } else{
                        onDetailResignation.onFailed(resObj.get("msg").asString)
                    }
                }
            }

            override fun onError(error: String?, resCode: Int) {
                if (error != null) {
                    onDetailResignation.onFailed(error)
                }
            }

        }))
    }

    fun getCategoryItemResignWs(context: Context, userBusinessKey: String, searchApprove : String,callBackCategoryResign : OnListItemResignCallBackListener){
        val serviceApi = RetrofitGenerator.createServiceWithHR(context)
        val res = serviceApi.getCategoryItemResign(userBusinessKey, searchApprove)
        res.enqueue(CustomCallback(context, object : CustomResponseListener{
            override fun onSuccess(resObj: JsonObject?) {
                if (resObj != null) {
                    if (resObj.has("data")) {
                        val data = resObj["data"].asJsonObject
                        val itemResignation : ItemResignationModel = Gson().fromJson(data.asJsonObject, ItemResignationModel::class.java)
                        callBackCategoryResign.onSuccessful(itemResignation)
                    } else {
                        callBackCategoryResign.onFailed(resObj.get("msg").asString)
                    }
                }
            }

            override fun onError(error: String, resCode: Int) {
                callBackCategoryResign.onFailed(error)
            }

        }))
    }
    fun getCreateListResignationWs(context : Context, hashMap: HashMap<String, Any>, onCallBackResignation : OnCallBackCreateResignationListener){
        val serviceApi = RetrofitGenerator.createServiceWithHR(context)
        val res = serviceApi.getCreateResignation(hashMap)
        res.enqueue(CustomCallback(context , object : CustomResponseListener{
            override fun onSuccess(resObj: JsonObject?) {
                if (resObj != null){
                    if (resObj.has("success")){
                        val isSuccess = resObj.get("success").asBoolean
                            if (isSuccess){
                                if (resObj.has("msg")){
                                    onCallBackResignation.onLoadSuccessFull(resObj["msg"].asString)
                                }
                            } else {
                                onCallBackResignation.onLoadFailed(resObj["msg"].asString)
                            }
                    }
                }
            }

            override fun onError(error: String, resCode: Int) {
                    onCallBackResignation.onLoadFailed(error)
            }

        }))
    }

    fun getActionApproveRejectWS(context: Context, hashMap: HashMap<String, Any>, callBackAction : OnCallBackCreateResignationListener){
        val serviceApi = RetrofitGenerator.createServiceWithHR(context)
        val res = serviceApi.
        getActionButtonResignation(hashMap)
        res.enqueue(CustomCallback(context, object : CustomResponseListener{
            override fun onSuccess(resObj: JsonObject?) {
                if (resObj != null){
                    if (resObj.has("success")){
                        val isSuccess = resObj.get("success").asBoolean
                        if (isSuccess){
                            if (resObj.has("msg")){
                                callBackAction.onLoadSuccessFull(resObj["msg"].asString)
                            }
                        } else{
                            callBackAction.onLoadFailed(resObj["msg"].asString)
                        }
                    }
                }
            }

            override fun onError(error: String?, resCode: Int) {
                if (error != null) {
                    callBackAction.onLoadFailed(error)
                }
            }


        }))
    }

    interface OnResignationCallBackListener{
        fun onSuccessful(listResignation: ArrayList<ResignationHrModel>)
        fun onFailed(error : String)
    }
    interface OnResignationDetailCallBackListener{
        fun onSuccessful(resignationDetailModel: ResignationDetailModel)
        fun onFailed(error : String)
    }
    interface OnListItemResignCallBackListener{
        fun onSuccessful(itemResignationModel: ItemResignationModel)
        fun onFailed(error : String)
    }
    interface OnCallBackCreateResignationListener{
        fun onLoadSuccessFull(message: String)
        fun onLoadFailed(message: String)
    }
}