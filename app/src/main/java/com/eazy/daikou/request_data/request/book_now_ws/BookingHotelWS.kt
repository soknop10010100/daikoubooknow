package com.eazy.daikou.request_data.request.book_now_ws

import android.content.Context
import android.widget.ProgressBar
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomCallback
import com.eazy.daikou.repository_ws.CustomResponseListener
import com.eazy.daikou.repository_ws.RetrofitGenerator
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.model.booking_hotel.*
import com.eazy.daikou.model.home.MainItemHomeModel
import com.eazy.daikou.model.home.SubItemHomeModel
import com.eazy.daikou.model.home.UserDeactivated
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import retrofit2.Call

class BookingHotelWS {

    fun getLocationHotelBooking(
        context: Context,
        onCallBackMyProfileListener: OnCallBackMyProfileListener
    ) {
        val serviceApi = RetrofitGenerator.createServiceWithHotelBooking(context)
        val res = serviceApi.locationHotel
        res.enqueue(CustomCallback(context, object : CustomResponseListener {
            override fun onSuccess(resObj: JsonObject) {
                if (resObj.has("data")) {
                    val locationHotelModelList: ArrayList<LocationHotelModel> = ArrayList()
                    val data = resObj["data"].asJsonArray
                    for (i in 0 until data.size()) {
                        val item: JsonElement = data[i].asJsonObject
                        val locationHotelModel: LocationHotelModel =
                            Gson().fromJson(item, LocationHotelModel::class.java)
                        locationHotelModelList.add(locationHotelModel)
                    }
                    onCallBackMyProfileListener.onSuccessLocation(locationHotelModelList)

                } else {
                    onCallBackMyProfileListener.onFailed(resObj.get("msg").asString)
                }
            }

            override fun onError(error: String, resCode: Int) {
                onCallBackMyProfileListener.onFailed(error)
            }
        }))
    }

    fun getHotelBookingByCategoryAll(
        context: Context,
        action: String,
        category: String,
        hashMap: HashMap<String, Any>,
        onCallBackHomePageItemListener: OnCallBackHomePageItemListener
    ) {
        val serviceApi = RetrofitGenerator.createServiceWithHotelBooking(context)
        val res = serviceApi.getHotelBookingV2All(hashMap)
        res.enqueue(CustomCallback(context, object : CustomResponseListener {
            override fun onSuccess(resObj: JsonObject) {
                if (resObj.has("data")) {
                    val mainItemHomeModelLIst: ArrayList<MainItemHomeModel> = ArrayList()
                    val userDeactivated = UserDeactivated()
                    val listImageSlider: ArrayList<String> = ArrayList()
                    val discountBanner: ArrayList<String> = ArrayList()
                    val travelTalkBanner: ArrayList<String> = ArrayList()

                    val jsonArray = resObj["data"].asJsonObject

                    // Image Slider Image
                    if (jsonArray.has("image_sliders")) {
                        val imgList = jsonArray["image_sliders"].asJsonArray
                        for (i in 0 until imgList.size()) {
                            val itemImg = imgList[i].asString
                            listImageSlider.add(itemImg)
                        }
                    }

                    // Discount Banner
                    if (jsonArray.has("discount_banners")) {
                        val imgList = jsonArray["discount_banners"].asJsonArray
                        for (i in 0 until imgList.size()) {
                            val itemImg = imgList[i].asString
                            discountBanner.add(itemImg)
                        }
                    }

                    // Travel Talk Banner
                    if (jsonArray.has("travel_talk_images")) {
                        val imgList = jsonArray["travel_talk_images"].asJsonArray
                        for (i in 0 until imgList.size()) {
                            val itemImg = imgList[i].asString
                            travelTalkBanner.add(itemImg)
                        }
                    }

                    // User Deactivated
                    if (jsonArray.has("user_deactivated")) {
                        val developmentData = jsonArray["user_deactivated"].asJsonObject
                        if (developmentData.has("is_deactivated")) {
                            userDeactivated.is_deactivated =
                                developmentData["is_deactivated"].asBoolean
                        }
                        if (developmentData.has("is_expired")) {
                            userDeactivated.is_expired =
                                developmentData["is_expired"].asBoolean
                        }
                    }

                    //top destination
                    if (jsonArray.has("location")) {
                        val developmentData = jsonArray["location"].asJsonArray
                        if (developmentData.size() > 0) {
                            val subItemHomeModelList = ArrayList<SubItemHomeModel>()
                            val mainItemHomeModel = MainItemHomeModel(
                                "top_destination",
                                context.resources.getString(R.string.top_destination),
                                context.getString(R.string.your_holiday_and_vocation_are_here)
                            )

                            for (i in 0 until developmentData.size()) {
                                subItemHomeModelList.add(
                                    getItemHotel(
                                        developmentData[i].asJsonObject,
                                        "top_destination",
                                        category
                                    )
                                )
                            }

                            mainItemHomeModel.subItemHomeModelList = subItemHomeModelList
                            mainItemHomeModelLIst.add(mainItemHomeModel)
                        }
                    }

                    //Near by location
                    if (jsonArray.has("nearby")) {
                        val developmentData = jsonArray["nearby"].asJsonArray
                        if (developmentData.size() > 0) {
                            val subItemHomeModelList = ArrayList<SubItemHomeModel>()
                            val mainItemHomeModel = MainItemHomeModel(
                                "all_nearby_hotels",
                                context.resources.getString(R.string.near_you),
                                context.resources.getString(R.string.please_check_us_we_are_around_you)
                            )

                            for (i in 0 until developmentData.size()) {
                                subItemHomeModelList.add(
                                    getItemHotel(
                                        developmentData[i].asJsonObject,
                                        "all_nearby_hotels",
                                        category
                                    )
                                )
                            }
                            mainItemHomeModel.subItemHomeModelList = subItemHomeModelList
                            mainItemHomeModelLIst.add(mainItemHomeModel)
                        }
                    }

                    //Featuring hotel
                    if (jsonArray.has("featured")) {
                        val developmentData = jsonArray["featured"].asJsonArray
                        if (developmentData.size() > 0) {
                            val subItemHomeModelList = ArrayList<SubItemHomeModel>()
                            val mainItemHomeModel = MainItemHomeModel(
                                "featuring_hotel",
                                context.resources.getString(R.string.featuring),
                                context.resources.getString(R.string.best_offer_and_great_experience)
                            )

                            for (i in 0 until developmentData.size()) {
                                subItemHomeModelList.add(
                                    getItemHotel(
                                        developmentData[i].asJsonObject,
                                        "featuring_hotel",
                                        category
                                    )
                                )
                            }

                            mainItemHomeModel.subItemHomeModelList = subItemHomeModelList
                            mainItemHomeModelLIst.add(mainItemHomeModel)
                        }
                    }

                    //bestseller listing
                    var itemHomeModelList = ArrayList<SubItemHomeModel>()    // Just init
                    if (jsonArray.has("recommendation") || jsonArray.has("all_hotels")) {
                        val developmentData =
                            if (action == "hotel_location_detail") jsonArray["all_hotels"].asJsonArray  // from click location (top destination)
                            else jsonArray["recommendation"].asJsonArray

                        if (developmentData.size() > 0) {
                            val subItemHomeModelList = ArrayList<SubItemHomeModel>()
                            val mainItemHomeModel = MainItemHomeModel(
                                "bestseller_listing",
                                context.resources.getString(R.string.recommended_for_you),
                                context.resources.getString(R.string.lets_enjoy_here)
                            )

                            for (i in 0 until developmentData.size()) {
                                subItemHomeModelList.add(
                                    getItemHotel(
                                        developmentData[i].asJsonObject,
                                        "bestseller_listing",
                                        category
                                    )
                                )
                            }

                            mainItemHomeModel.subItemHomeModelList = subItemHomeModelList
                            itemHomeModelList = subItemHomeModelList
                            mainItemHomeModelLIst.add(mainItemHomeModel)

                        }
                    }

                    onCallBackHomePageItemListener.onSuccessShowHotel(
                        travelTalkBanner,
                        discountBanner,
                        listImageSlider,
                        mainItemHomeModelLIst,
                        itemHomeModelList,
                        userDeactivated
                    )

                } else {
                    onCallBackHomePageItemListener.onFailed(resObj.get("msg").toString())
                }
            }

            override fun onError(error: String, resCode: Int) {
                onCallBackHomePageItemListener.onFailed(error)
            }
        }))
    }

    fun getHotelBookingAll(
        context: Context,
        action: String,
        category: String,
        hashMap: HashMap<String, Any>,
        onCallBackMyProfileListener: OnCallBackMyProfileListener
    ) {
        val serviceApi = RetrofitGenerator.createServiceWithHotelBooking(context)
        Utils.logDebug("jjjjjjjjjjjjjjjjjjjkeeeeeee", Utils.getStringGson(hashMap))
        val res = when (action) {
            "search_list" -> {
                serviceApi.getHotelBookingSearchLocation(hashMap)
            }
            "hotel_location_detail" -> {
                serviceApi.getHotelBookingLocationDetail(hashMap)
            }
            "hotel_home_page" -> {
                serviceApi.getHotelBookingV2All(hashMap)
            }
            else -> {
                serviceApi.getHotelBookingV2All(hashMap)
            }
        }
        res.enqueue(CustomCallback(context, object : CustomResponseListener {
            override fun onSuccess(resObj: JsonObject) {
                if (resObj.has("data")) {
                    val mainItemHomeModelLIst: ArrayList<MainItemHomeModel> = ArrayList()
                    val userDeactivated = UserDeactivated()
                    val listImageSlider: ArrayList<String> = ArrayList()

                    when (action) {
                        "search_list" -> {  // Search item by location
                            val jsonArray = resObj["data"].asJsonArray

                            val subItemHomeModelList = ArrayList<SubItemHomeModel>()
                            val mainItemHomeModel = MainItemHomeModel(
                                "bestseller_listing",
                                context.resources.getString(R.string.recommended_for_you),
                                context.getString(R.string.you_can_see_list_of_recommended_hotels_fo_you)
                            )

                            for (i in 0 until jsonArray.size()) {
                                subItemHomeModelList.add(
                                    getItemHotel(
                                        jsonArray[i].asJsonObject,
                                        "bestseller_listing",
                                        category
                                    )
                                )
                            }

                            mainItemHomeModel.subItemHomeModelList = subItemHomeModelList
                            mainItemHomeModelLIst.add(mainItemHomeModel)

                            onCallBackMyProfileListener.onSuccessShowHotel(
                                action,
                                listImageSlider,
                                mainItemHomeModelLIst,
                                subItemHomeModelList,
                                userDeactivated
                            )
                        }

                        "home_images_first_page" -> {    // Item List
                            val jsonArray = resObj["data"].asJsonObject

                            if (jsonArray.has("image_sliders")) {
                                val imgList = jsonArray["image_sliders"].asJsonArray
                                for (i in 0 until imgList.size()) {
                                    val itemImg = imgList[i].asString
                                    listImageSlider.add(itemImg)
                                }
                            }

                            if (jsonArray.has("user_deactivated")) {
                                val developmentData = jsonArray["user_deactivated"].asJsonObject
                                if (developmentData.has("is_deactivated")) {
                                    userDeactivated.is_deactivated =
                                        developmentData["is_deactivated"].asBoolean
                                }
                                if (developmentData.has("is_expired")) {
                                    userDeactivated.is_expired =
                                        developmentData["is_expired"].asBoolean
                                }
                            }

                            val subItemHomeModelList = ArrayList<SubItemHomeModel>()    // Just init
                            onCallBackMyProfileListener.onSuccessShowHotel(
                                action,
                                listImageSlider,
                                mainItemHomeModelLIst,
                                subItemHomeModelList,
                                userDeactivated
                            )
                        }

                        "hotel_home_page", "hotel_location_detail" -> {   // Item List
                            val jsonArray = resObj["data"].asJsonObject

                            if (jsonArray.has("image_sliders")) {
                                val imgList = jsonArray["image_sliders"].asJsonArray
                                for (i in 0 until imgList.size()) {
                                    val itemImg = imgList[i].asString
                                    listImageSlider.add(itemImg)
                                }
                            }

                            if (jsonArray.has("user_deactivated")) {
                                val developmentData = jsonArray["user_deactivated"].asJsonObject
                                if (developmentData.has("is_deactivated")) {
                                    userDeactivated.is_deactivated =
                                        developmentData["is_deactivated"].asBoolean
                                }
                                if (developmentData.has("is_expired")) {
                                    userDeactivated.is_expired =
                                        developmentData["is_expired"].asBoolean
                                }
                            }

                            //top destination
                            if (jsonArray.has("location")) {
                                val developmentData = jsonArray["location"].asJsonArray
                                if (developmentData.size() > 0) {
                                    val subItemHomeModelList = ArrayList<SubItemHomeModel>()
                                    val mainItemHomeModel = MainItemHomeModel(
                                        "top_destination",
                                        context.resources.getString(R.string.top_destination),
                                        context.getString(R.string.more_things_useful_recomment_for_you)
                                    )

                                    for (i in 0 until developmentData.size()) {
                                        subItemHomeModelList.add(
                                            getItemHotel(
                                                developmentData[i].asJsonObject,
                                                "top_destination",
                                                category
                                            )
                                        )
                                    }

                                    mainItemHomeModel.subItemHomeModelList = subItemHomeModelList
                                    mainItemHomeModelLIst.add(mainItemHomeModel)
                                }
                            }

                            //Near by location
                            if (jsonArray.has("nearby")) {
                                val developmentData = jsonArray["nearby"].asJsonArray
                                if (developmentData.size() > 0) {
                                    val subItemHomeModelList = ArrayList<SubItemHomeModel>()
                                    val mainItemHomeModel = MainItemHomeModel(
                                        "all_nearby_hotels",
                                        context.resources.getString(R.string.near_you),
                                        context.resources.getString(R.string.hotel_near_by_your_current_location)
                                    )

                                    for (i in 0 until developmentData.size()) {
                                        subItemHomeModelList.add(
                                            getItemHotel(
                                                developmentData[i].asJsonObject,
                                                "all_nearby_hotels",
                                                category
                                            )
                                        )
                                    }
                                    mainItemHomeModel.subItemHomeModelList = subItemHomeModelList
                                    mainItemHomeModelLIst.add(mainItemHomeModel)
                                }
                            }

                            //Featuring hotel
                            if (jsonArray.has("featured")) {
                                val developmentData = jsonArray["featured"].asJsonArray
                                if (developmentData.size() > 0) {
                                    val subItemHomeModelList = ArrayList<SubItemHomeModel>()
                                    val mainItemHomeModel = MainItemHomeModel(
                                        "featuring_hotel",
                                        context.resources.getString(R.string.featuring),
                                        context.resources.getString(R.string.best_selected_hotels)
                                    )

                                    for (i in 0 until developmentData.size()) {
                                        subItemHomeModelList.add(
                                            getItemHotel(
                                                developmentData[i].asJsonObject,
                                                "featuring_hotel",
                                                category
                                            )
                                        )
                                    }

                                    mainItemHomeModel.subItemHomeModelList = subItemHomeModelList
                                    mainItemHomeModelLIst.add(mainItemHomeModel)
                                }
                            }

                            //bestseller listing
                            var itemHomeModelList = ArrayList<SubItemHomeModel>()    // Just init
                            if (jsonArray.has("recommendation") || jsonArray.has("all_hotels")) {
                                val developmentData =
                                    if (action == "hotel_location_detail") jsonArray["all_hotels"].asJsonArray  // from click location (top destination)
                                    else jsonArray["recommendation"].asJsonArray

                                if (developmentData.size() > 0) {
                                    val subItemHomeModelList = ArrayList<SubItemHomeModel>()
                                    val mainItemHomeModel = MainItemHomeModel(
                                        "bestseller_listing",
                                        context.resources.getString(R.string.recommended_for_you),
                                        context.getString(R.string.you_can_see_list_of_recommended_hotels_fo_you)
                                    )

                                    for (i in 0 until developmentData.size()) {
                                        subItemHomeModelList.add(
                                            getItemHotel(
                                                developmentData[i].asJsonObject,
                                                "bestseller_listing",
                                                category
                                            )
                                        )
                                    }

                                    mainItemHomeModel.subItemHomeModelList = subItemHomeModelList
                                    itemHomeModelList = subItemHomeModelList
                                    mainItemHomeModelLIst.add(mainItemHomeModel)

                                }
                            }

                            onCallBackMyProfileListener.onSuccessShowHotel(
                                action,
                                listImageSlider,
                                mainItemHomeModelLIst,
                                itemHomeModelList,
                                userDeactivated
                            )
                        }
                    }
                } else {
                    onCallBackMyProfileListener.onFailed(resObj.get("msg").toString())
                }
            }

            override fun onError(error: String, resCode: Int) {
                onCallBackMyProfileListener.onFailed(error)
            }
        }))
    }

    fun getSeeAllBookingByCategoryAll(
        context: Context,
        action: String,
        category: String,
        hashMap: HashMap<String, Any>,
        onCallBackHomePageItemListener: OnCallBackHomePageItemListener
    ) {
        val serviceApi = RetrofitGenerator.createServiceWithHotelBooking(context)
        val res = serviceApi.getHotelBookingSeeAll(hashMap)
        res.enqueue(CustomCallback(context, object : CustomResponseListener {
            override fun onSuccess(resObj: JsonObject) {
                if (resObj.has("data")) {
                    val jsonArray = resObj["data"].asJsonArray

                    val subItemHomeModelList = ArrayList<SubItemHomeModel>()

                    for (i in 0 until jsonArray.size()) {
                        subItemHomeModelList.add(
                            getItemHotel(
                                jsonArray[i].asJsonObject,
                                action,
                                category
                            )
                        )
                    }

                    onCallBackHomePageItemListener.onSuccessShowHotelAll(subItemHomeModelList)

                } else {
                    onCallBackHomePageItemListener.onFailed(resObj.get("msg").toString())
                }
            }

            override fun onError(error: String, resCode: Int) {
                onCallBackHomePageItemListener.onFailed(error)
            }
        }))
    }

    fun getLocationDetailWs(
        context: Context,
        id: String,
        category: String,
        coord_lat: String,
        coord_long: String,
        onCallBackTravelArticleListener: OnCallBackLocationDetailListener
    ) {
        val serviceApi = RetrofitGenerator.createServiceWithHotelBooking(context)
        val res = serviceApi.locationDetailWs(id, category, coord_lat, coord_long)
        res.enqueue(CustomCallback(context, object : CustomResponseListener {
            override fun onSuccess(resObj: JsonObject) {
                if (resObj.has("data")) {
                    val jsonArray = resObj["data"].asJsonObject
                    val hotelLocationDetailModel = HotelLocationDetailModel()
                    val hashMap: HashMap<String, ArrayList<SubItemHomeModel>> = HashMap()

                    //data model
                    hotelLocationDetailModel.id = validateData(jsonArray, "id")
                    hotelLocationDetailModel.name = validateData(jsonArray, "name")
                    hotelLocationDetailModel.description = validateData(jsonArray, "description")

                    //bestseller listing
                    if (jsonArray.has("total_places")) {
                        val developmentData = jsonArray["total_places"].asJsonArray

                        if (developmentData.size() > 0) {
                            val subItemHomeModelList = ArrayList<SubItemHomeModel>()
                            for (i in 0 until developmentData.size()) {
                                subItemHomeModelList.add(
                                    getItemHotel(
                                        developmentData[i].asJsonObject,
                                        "bestseller_listing",
                                        category
                                    )
                                )
                            }
                            hashMap["total_places"] = subItemHomeModelList
                        }
                    }

                    //Near by location
                    if (jsonArray.has("nearby")) {
                        val developmentData = jsonArray["nearby"].asJsonArray
                        if (developmentData.size() > 0) {
                            val subItemHomeModelList = ArrayList<SubItemHomeModel>()
                            for (i in 0 until developmentData.size()) {
                                subItemHomeModelList.add(
                                    getItemHotel(
                                        developmentData[i].asJsonObject,
                                        "all_nearby_hotels",
                                        category
                                    )
                                )
                            }
                            hashMap["nearby"] = subItemHomeModelList
                        }
                    }

                    onCallBackTravelArticleListener.onSuccess(hotelLocationDetailModel, hashMap)

                } else {
                    onCallBackTravelArticleListener.onFailed(resObj.get("msg").toString())
                }
            }

            override fun onError(error: String, resCode: Int) {
                onCallBackTravelArticleListener.onFailed(error)
            }
        }))
    }

    private fun getItemHotel(
        jsonObject: JsonObject,
        action: String,
        category: String
    ): SubItemHomeModel {
        val subItemHomeModel = SubItemHomeModel()
        if (action == "top_destination") {
            subItemHomeModel.action = action
            subItemHomeModel.titleCategory = category
            subItemHomeModel.id = validateData(jsonObject, "id")
            subItemHomeModel.name = validateData(jsonObject, "name")
            subItemHomeModel.imageUrl = validateData(jsonObject, "image")
            subItemHomeModel.totalHotel = validateData(jsonObject, "total_places")
        } else {
            subItemHomeModel.action = action
            subItemHomeModel.titleCategory = category
            subItemHomeModel.id = validateData(jsonObject, "id")
            subItemHomeModel.name = validateData(jsonObject, "name")
            subItemHomeModel.rating = validateData(jsonObject, "star_rate")
            subItemHomeModel.priceVal = validateData(jsonObject, "price_display")
            subItemHomeModel.priceOriginalBeforeDiscount =
                validateData(jsonObject, "original_price_display")
            subItemHomeModel.imageUrl = validateData(jsonObject, "image")
            subItemHomeModel.distance = validateData(jsonObject, "distance")

            //location lat lng
            subItemHomeModel.coordLat = validateData(jsonObject, "coord_lat")
            subItemHomeModel.coordLng = validateData(jsonObject, "coord_long")

            if (jsonObject.has("location")) {
                if (!jsonObject["location"].isJsonNull) {
                    subItemHomeModel.address =
                        validateData(jsonObject["location"].asJsonObject, "name")
                    subItemHomeModel.locationId =
                        validateData(jsonObject["location"].asJsonObject, "id")
                }
            }

            if (jsonObject.has("is_wishlist")) {
                subItemHomeModel.isWishlist = jsonObject["is_wishlist"].asBoolean
            }

            if (jsonObject.has("booking_available")) {
                subItemHomeModel.isBookingAvailable = jsonObject["booking_available"].asBoolean
            }
        }
        return subItemHomeModel
    }

    fun getHotelBookingDetail(
        context: Context,
        hotelId: String,
        userId: String,
        category: String,
        onCallBackMyProfileListener: OnCallBackMyProfileListener
    ) {
        val serviceApi = RetrofitGenerator.createServiceWithHotelBooking(context)
        val res = serviceApi.getHotelBookingLocationDetail(hotelId, userId, category)
        res.enqueue(CustomCallback(context, object : CustomResponseListener {
            override fun onSuccess(resObj: JsonObject) {
                if (resObj.has("data")) {
                    val data = resObj["data"].asJsonObject
                    val locationHotelModel: HotelBookingDetailModel =
                        Gson().fromJson(data.asJsonObject, HotelBookingDetailModel::class.java)

                    val subItemHomeModelList = ArrayList<SubItemHomeModel>()
                    if (data.has("related")) {
                        val jsonArray = data["related"].asJsonArray
                        for (i in 0 until jsonArray.size()) {
                            subItemHomeModelList.add(
                                getItemHotel(
                                    jsonArray[i].asJsonObject,
                                    "related_hotel_detail",
                                    category
                                )
                            )
                        }

                    }

                    val ticketTypeList: ArrayList<TicketType> = ArrayList()      // Tour
                    if (data.has("person_types")) {
                        val jsonArray = data["person_types"].asJsonArray
                        for (i in 0 until jsonArray.size()) {
                            val jsonObject = jsonArray[i].asJsonObject
                            val ticketType = TicketType()
                            if (jsonObject.has("price")) {
                                ticketType.price = jsonObject["price"].asDouble
                            }
                            ticketType.price_display = validateData(jsonObject, "price_display")
                            ticketType.code = validateData(jsonObject, "id")
                            ticketType.name = validateData(jsonObject, "name")
                            ticketType.number = validateData(jsonObject, "max")
                            if (jsonObject.has("max")) {
                                ticketType.num =
                                    if (validateData(jsonObject, "max") != null) validateData(
                                        jsonObject,
                                        "max"
                                    )!!.toInt() else 0
                            }
                            ticketType.date_form_to = validateData(jsonObject, "date_form_to")
                            ticketType.min_age = validateData(jsonObject, "min_age")
                            ticketTypeList.add(ticketType)
                        }
                        locationHotelModel.ticket_types = ticketTypeList
                    }

                    onCallBackMyProfileListener.onSuccessShowHotelDetail(
                        locationHotelModel,
                        subItemHomeModelList
                    )
                } else {
                    onCallBackMyProfileListener.onFailed(resObj.get("msg").asString)
                }
            }

            override fun onError(error: String, resCode: Int) {
                onCallBackMyProfileListener.onFailed(error)
            }
        }))
    }

    fun getRoomAll(
        context: Context,
        hotelId: String,
        adults: String,
        children: String,
        startDate: String,
        endDate: String,
        onCallBackMyProfileListener: OnCallBackRoomHotelListener
    ) {
        val serviceApi = RetrofitGenerator.createServiceWithHotelBooking(context)
        val res = serviceApi.getUnitBookingHotel(hotelId, adults, children, startDate, endDate)
        res.enqueue(CustomCallback(context, object : CustomResponseListener {
            override fun onSuccess(resObj: JsonObject) {
                if (resObj.has("data")) {
                    val locationHotelModelList: ArrayList<ItemRoomHotelBookingModel> = ArrayList()
                    val data = resObj["data"].asJsonArray
                    for (i in 0 until data.size()) {
                        val item: JsonElement = data[i].asJsonObject
                        val locationHotelModel: ItemRoomHotelBookingModel =
                            Gson().fromJson(item, ItemRoomHotelBookingModel::class.java)
                        locationHotelModelList.add(locationHotelModel)
                    }
                    onCallBackMyProfileListener.onSuccessRoomHotel(locationHotelModelList)
                } else {
                    onCallBackMyProfileListener.onFailed(resObj["msg"].toString())
                }
            }

            override fun onError(error: String, resCode: Int) {
                onCallBackMyProfileListener.onFailed(error)
            }
        }))
    }

    fun bookingRoomDraft(
        context: Context,
        hashMap: HashMap<String, Any>,
        onCallBackMyProfileListener: OnCallBackRoomHotelListener
    ) {
        val serviceApi = RetrofitGenerator.createServiceWithHotelBooking(context)
        val res = serviceApi.bookingRoom(hashMap)
        res.enqueue(CustomCallback(context, object : CustomResponseListener {
            override fun onSuccess(resObj: JsonObject) {
                if (resObj.has("data")) {
                    if (!resObj.get("data").isJsonNull) {
                        val data = resObj["data"].asJsonObject
                        var bookingId = ""
                        var bookingCode = ""
                        var kessPayment = ""
                        if (data.has("booking_id")) {
                            if (!data["booking_id"].isJsonNull) {
                                bookingId = data["booking_id"].asString
                            }
                        }
                        if (data.has("booking_code")) {
                            if (!data["booking_code"].isJsonNull) {
                                bookingCode = data["booking_code"].asString
                            }
                        }
                        if (data.has("kess_payment_link")) {
                            if (!data["kess_payment_link"].isJsonNull) {
                                kessPayment = data["kess_payment_link"].asString
                            }
                        }

                        onCallBackMyProfileListener.onSuccessBookingRoom(
                            bookingId,
                            bookingCode,
                            kessPayment
                        )
                    }
                } else {
                    onCallBackMyProfileListener.onFailed(resObj["msg"].toString())
                }
            }

            override fun onError(error: String, resCode: Int) {
                onCallBackMyProfileListener.onFailed(error)
            }
        }))
    }

    fun getPayment(
        context: Context,
        hashMap: HashMap<String, Any>,
        paymentCallBack: OnRequestPaymentCallBack
    ) {
        val serviceApi = RetrofitGenerator.createServiceWithHotelBooking(context)
        val res = serviceApi.bookingSubmitPayment(hashMap)
        Utils.logDebug("Jjekkkkkkkkkkkk", Utils.getStringGson(hashMap))
        res.enqueue(CustomCallback(context, object : CustomResponseListener {
            override fun onSuccess(resObj: JsonObject) {
                if (resObj.has("data")) {
                    paymentCallBack.onLoadSuccess(
                        GenerateRespondWs.onGenerateRespondDataObjWs(
                            resObj,
                            PaymentSuccessModel::class.java
                        )
                    )
                } else {
                    paymentCallBack.onLoadFail(resObj["msg"].asString)
                }
            }

            override fun onError(error: String, resCode: Int) {
                paymentCallBack.onLoadFail(error)
            }
        }))
    }

    fun getHistoryHotelBooking(
        context: Context,
        action: String,
        hashMap: HashMap<String, Any>,
        paymentCallBack: OnCallBackHistoryHotelListener
    ) {
        val serviceApi = RetrofitGenerator.createServiceWithHotelBooking(context)
        val res =
            if (action == "hotel_history_hotel") serviceApi.getHistoryHotelBooking(hashMap) else serviceApi.getHistoryHotelBookingVendor(
                hashMap
            )
        Utils.logDebug("Jjekkkkkkkkkkkk", Utils.getStringGson(hashMap))
        res.enqueue(CustomCallback(context, object : CustomResponseListener {
            override fun onSuccess(resObj: JsonObject) {
                if (resObj.has("data")) {
                    val list: ArrayList<HotelBookingHistoryModel> = ArrayList()
                    val data = resObj["data"].asJsonArray
                    for (i in 0 until data.size()) {
                        val item: JsonElement = data[i].asJsonObject
                        val historyModel: HotelBookingHistoryModel =
                            Gson().fromJson(item, HotelBookingHistoryModel::class.java)
                        list.add(historyModel)
                    }
                    paymentCallBack.onSuccessHistoryHotel(list)
                } else {
                    paymentCallBack.onFailed(resObj["msg"].asString)
                }
            }

            override fun onError(error: String, resCode: Int) {
                paymentCallBack.onFailed(error)
            }
        }))
    }

    fun getHistoryDetailHotelBookingWs(
        context: Context,
        hotelId: String,
        paymentCallBack: OnCallBackHistoryHotelListener
    ) {
        val serviceApi = RetrofitGenerator.createServiceWithHotelBooking(context)
        val res = serviceApi.getHistoryDetailHotelBooking(hotelId)
        res.enqueue(CustomCallback(context, object : CustomResponseListener {
            override fun onSuccess(resObj: JsonObject) {
                if (resObj.has("data") && !resObj["data"].isJsonNull) {
                    val data = resObj["data"].asJsonObject
                    val historyModel: HotelBookingHistoryDetailModel = Gson().fromJson(
                        data.asJsonObject,
                        HotelBookingHistoryDetailModel::class.java
                    )
                    paymentCallBack.onSuccessHistoryDetailHotel(historyModel)
                } else {
                    paymentCallBack.onFailed(resObj["msg"].asString)
                }
            }

            override fun onError(error: String, resCode: Int) {
                paymentCallBack.onFailed(error)
            }
        }))
    }

    fun getDashboardHotelWs(
        context: Context,
        hashMap: HashMap<String, Any>,
        onCallBack: OnCallBackDashboardListener
    ) {
        val serviceApi = RetrofitGenerator.createServiceWithHotelBooking(context)
        val res = serviceApi.getDashboardHotel(hashMap)
        res.enqueue(CustomCallback(context, object : CustomResponseListener {
            override fun onSuccess(resObj: JsonObject) {
                if (resObj.has("data") && !resObj["data"].isJsonNull) {
                    val data = resObj["data"].asJsonObject
                    val historyModel: HotelDashboardModel =
                        Gson().fromJson(data.asJsonObject, HotelDashboardModel::class.java)
                    onCallBack.onSuccessDataDashboard(historyModel)
                } else {
                    onCallBack.onFailed(resObj["msg"].asString)
                }
            }

            override fun onError(error: String, resCode: Int) {
                onCallBack.onFailed(error)
            }
        }))
    }

    fun getHotelVendorWs(
        context: Context,
        action: String,
        category: String,
        eazyhotelUserId: String,
        page: Int,
        limit: Int,
        paymentCallBack: OnCallBackHotelVendorListener
    ) {
        val serviceApi = RetrofitGenerator.createServiceWithHotelBooking(context)
        val res = if (action == "hotel_hotel_control_admin") serviceApi.getHotelVendu(
            page,
            limit,
            eazyhotelUserId
        ) else serviceApi.getWishlistHotel(category, page, limit, eazyhotelUserId)
        res.enqueue(CustomCallback(context, object : CustomResponseListener {
            override fun onSuccess(resObj: JsonObject) {
                if (resObj.has("data")) {
                    if (action == "hotel_hotel_control_admin") {      //manage hotel
                        val list: ArrayList<HotelMyVendorModel> = ArrayList()
                        val data = resObj["data"].asJsonArray
                        for (i in 0 until data.size()) {
                            val item: JsonElement = data[i].asJsonObject
                            val historyModel: HotelMyVendorModel =
                                Gson().fromJson(item, HotelMyVendorModel::class.java)
                            list.add(historyModel)
                        }
                        paymentCallBack.onSuccessHotelOfVendor(list)
                    }
                    //wishlist hotel
                    else {
                        val mainItemHomeModelLIst: ArrayList<MainItemHomeModel> = ArrayList()
                        val jsonArray = resObj["data"].asJsonArray
                        val subItemHomeModelList = ArrayList<SubItemHomeModel>()
                        val mainItemHomeModel = MainItemHomeModel(
                            "bestseller_listing",
                            "Bestseller Listing",
                            "You can see list of recommended hotels fo you"
                        )

                        for (i in 0 until jsonArray.size()) {
                            subItemHomeModelList.add(
                                getItemHotel(
                                    jsonArray[i].asJsonObject,
                                    "bestseller_listing",
                                    category
                                )
                            )
                        }

                        mainItemHomeModel.subItemHomeModelList = subItemHomeModelList
                        mainItemHomeModelLIst.add(mainItemHomeModel)
                        paymentCallBack.onSuccessShowWishlistHotel(mainItemHomeModelLIst)
                    }
                } else {
                    paymentCallBack.onFailed(resObj["msg"].asString)
                }
            }

            override fun onError(error: String, resCode: Int) {
                paymentCallBack.onFailed(error)
            }
        }))
    }

    fun getRoomOfHotelVendorWs(
        context: Context,
        page: Int,
        limit: Int,
        hotelId: String,
        paymentCallBack: OnCallBackHotelVendorListener
    ) {
        val serviceApi = RetrofitGenerator.createServiceWithHotelBooking(context)
        val res = serviceApi.getRoomOfHotelVendu(page, limit, hotelId)
        res.enqueue(CustomCallback(context, object : CustomResponseListener {
            override fun onSuccess(resObj: JsonObject) {
                if (resObj.has("data")) {
                    val list: ArrayList<HotelMyRoomOfHotelVendorModel> = ArrayList()
                    val data = resObj["data"].asJsonArray
                    for (i in 0 until data.size()) {
                        val item: JsonElement = data[i].asJsonObject
                        val historyModel: HotelMyRoomOfHotelVendorModel =
                            Gson().fromJson(item, HotelMyRoomOfHotelVendorModel::class.java)
                        list.add(historyModel)
                    }
                    paymentCallBack.onSuccessRoomHotel(list)
                } else {
                    paymentCallBack.onFailed(resObj["msg"].asString)
                }
            }

            override fun onError(error: String, resCode: Int) {
                paymentCallBack.onFailed(error)
            }
        }))
    }

    //Edit status hotel or room
    fun doEditStatus(
        context: Context,
        action: String,
        hashMap: HashMap<String, Any>,
        paymentCallBack: OnCallBackHotelVendorListener
    ) {
        val serviceApi = RetrofitGenerator.createServiceWithHotelBooking(context)
        val res: Call<JsonElement>? = when (action) {
            "edit_hotel" -> {
                serviceApi.editHotelVendor(hashMap)
            }
            "status_room" -> {
                serviceApi.editRoomHotelVendor(hashMap)
            }
            else -> {
                serviceApi.editHotelVendorWishlist(hashMap)
            }
        }
        res?.enqueue(CustomCallback(context, object : CustomResponseListener {
            override fun onSuccess(resObj: JsonObject) {
                if (resObj.has("success") && resObj["success"].asString == "true") {
                    if (resObj.has("msg")) {
                        paymentCallBack.onSuccessEditStatus(resObj["msg"].asString)
                    }
                } else {
                    if (resObj.has("msg")) {
                        paymentCallBack.onSuccessEditStatus(resObj["msg"].asString)
                    }
                }
            }

            override fun onError(error: String, resCode: Int) {
                paymentCallBack.onFailed(error)
            }
        }))
    }

    fun getNearbyLocationWs(
        context: Context,
        category: String,
        hotelId: String,
        lat: String,
        lng: String,
        paymentCallBack: OnCallBackNearbyListener
    ) {
        val serviceApi = RetrofitGenerator.createServiceWithHotelBooking(context)
        val res = serviceApi.nearbyHotelLocation(category, hotelId, lat, lng)
        res.enqueue(CustomCallback(context, object : CustomResponseListener {
            override fun onSuccess(resObj: JsonObject) {
                if (resObj.has("data")) {
                    val list: ArrayList<NearbyHotelLocationModel> = ArrayList()
                    val data = resObj["data"].asJsonArray
                    for (i in 0 until data.size()) {
                        val item: JsonElement = data[i].asJsonObject
                        val historyModel: NearbyHotelLocationModel =
                            Gson().fromJson(item, NearbyHotelLocationModel::class.java)
                        list.add(historyModel)
                    }
                    paymentCallBack.onSuccessNearbyLocation(list)
                } else {
                    paymentCallBack.onFailed(resObj["msg"].asString)
                }
            }

            override fun onError(error: String, resCode: Int) {
                paymentCallBack.onFailed(error)
            }
        }))
    }

    fun doReviewHotel(
        context: Context,
        hashMap: HashMap<String, Any>,
        paymentCallBack: OnCallBackHotelVendorListener
    ) {
        val serviceApi = RetrofitGenerator.createServiceWithHotelBooking(context)
        val res: Call<JsonElement>? = serviceApi.addReviewHotel(hashMap)
        res?.enqueue(CustomCallback(context, object : CustomResponseListener {
            override fun onSuccess(resObj: JsonObject) {
                if (resObj.has("success") && resObj["success"].asString == "true") {
                    if (resObj.has("msg")) {
                        paymentCallBack.onSuccessEditStatus(resObj["msg"].asString)
                    }
                } else {
                    if (resObj.has("msg")) {
                        paymentCallBack.onSuccessEditStatus(resObj["msg"].asString)
                    }
                }
            }

            override fun onError(error: String, resCode: Int) {
                paymentCallBack.onFailed(error)
            }
        }))
    }

    fun applyCouponCode(
        context: Context,
        progressBar: ProgressBar,
        hashMap: HashMap<String, Any>,
        onRequestCouponCallBackListener: OnRequestCouponCallBackListener
    ) {
        val serviceApi = RetrofitGenerator.createServiceWithHotelBooking(context)
        val res: Call<JsonElement> = serviceApi.applyCoupon(hashMap)
        res.enqueue(CustomCallback(context, object : CustomResponseListener {
            override fun onSuccess(resObj: JsonObject) {
                if (resObj.has("data") && !resObj["data"].isJsonNull) {
                    val jsonElement: JsonObject = resObj["data"].asJsonObject
                    if (!jsonElement.isJsonNull) {
                        val priceCoupon = validateData(jsonElement, "total_coupon_price_display")
                        val totalPrice = validateData(jsonElement, "total_price_display")
                        if (priceCoupon != null && totalPrice != null) {
                            onRequestCouponCallBackListener.onLoadSuccess(
                                resObj["msg"].asString,
                                hashMap["action"].toString(),
                                hashMap["coupon_code"].toString(),
                                priceCoupon,
                                totalPrice,
                                progressBar
                            )
                        } else {
                            onRequestCouponCallBackListener.onLoadFail(
                                context.getString(R.string.something_went_wrong),
                                progressBar
                            )
                        }
                    } else {
                        onRequestCouponCallBackListener.onLoadFail(
                            context.getString(R.string.something_went_wrong),
                            progressBar
                        )
                    }
                } else {
                    onRequestCouponCallBackListener.onLoadFail(resObj["msg"].asString, progressBar)
                }
            }

            override fun onError(error: String, resCode: Int) {
                onRequestCouponCallBackListener.onLoadFail(error, progressBar)
            }
        }))
    }

    fun checkAvailableBooking(
        context: Context,
        hashMap: HashMap<String, Any>,
        onCallBackTravelArticleListener: OnCallBackCheckAvailableListener
    ) {
        val serviceApi = RetrofitGenerator.createServiceWithHotelBooking(context)
        val res = serviceApi.checkAvailableItem(hashMap)
        res.enqueue(CustomCallback(context, object : CustomResponseListener {
            override fun onSuccess(resObj: JsonObject) {
                if (resObj.has("data")) {
                    if (!resObj["data"].isJsonNull) {
                        val data = resObj["data"].asJsonObject
                        if (data.has("can_book")) {
                            var price = 0.0
                            if (data.has("price")) {
                                if (!data["price"].isJsonNull) {
                                    price = data["price"].asDouble
                                }
                            }
                            onCallBackTravelArticleListener.onSuccess(
                                price,
                                data["can_book"].asBoolean
                            )
                        } else {
                            onCallBackTravelArticleListener.onFailed("Cannot Book !")
                        }
                    }
                } else {
                    onCallBackTravelArticleListener.onFailed(resObj["msg"].asString)
                }
            }

            override fun onError(error: String, resCode: Int) {
                onCallBackTravelArticleListener.onFailed(error)
            }
        }))
    }

    private fun validateData(jsonObject: JsonObject, key: String): String? {
        if (jsonObject.has(key)) {
            if (!jsonObject[key].isJsonNull) {
                return jsonObject[key].asString
            }
        }
        return null
    }

    interface OnRequestPaymentCallBack {
        fun onLoadSuccess(paymentSuccessModel: PaymentSuccessModel)
        fun onLoadFail(message: String)
    }

    interface OnCallBackCheckAvailableListener {
        fun onSuccess(price: Double, message: Boolean)
        fun onFailed(message: String)
    }


    interface OnCallBackLocationDetailListener {
        fun onSuccess(
            locationDetail: HotelLocationDetailModel,
            hashMap: HashMap<String, ArrayList<SubItemHomeModel>>
        )

        fun onFailed(message: String)
    }

    interface OnRequestCouponCallBackListener {
        fun onLoadSuccess(
            message: String,
            action: String,
            couponCode: String,
            couponPrice: String,
            price: String,
            progressBar: ProgressBar
        )

        fun onLoadFail(message: String, progressBar: ProgressBar)
    }

    interface OnCallBackHomePageItemListener {
        fun onSuccessShowHotel(
            travelTalkBanner: ArrayList<String>,
            discountBanner: ArrayList<String>,
            listImageSlider: MutableList<String>,
            mainItemHomeModelLIst: ArrayList<MainItemHomeModel>,
            itemHomeList: ArrayList<SubItemHomeModel>,
            userDeactivated: UserDeactivated
        )

        fun onSuccessShowHotelAll(itemHomeList: ArrayList<SubItemHomeModel>)

        fun onFailed(message: String)
    }

    interface OnCallBackMyProfileListener {
        fun onSuccessLocation(locationHotelModel: ArrayList<LocationHotelModel>)
        fun onSuccessShowHotel(
            action: String,
            listImageSlider: MutableList<String>,
            locationHotelModel: ArrayList<MainItemHomeModel>,
            itemHomeList: ArrayList<SubItemHomeModel>,
            userDeactivated: UserDeactivated
        )

        fun onSuccessShowHotelDetail(
            hotelDetailItemModel: HotelBookingDetailModel,
            mainItemHomeModelLIst: ArrayList<SubItemHomeModel>
        )

        fun onFailed(message: String)
    }

    interface OnCallBackRoomHotelListener {
        fun onSuccessRoomHotel(locationHotelModel: ArrayList<ItemRoomHotelBookingModel>)
        fun onSuccessBookingRoom(bookingId: String, bookingCode: String, kessPayment: String)
        fun onFailed(message: String)
    }

    interface OnCallBackNearbyListener {
        fun onSuccessNearbyLocation(hotelDashboardModel: ArrayList<NearbyHotelLocationModel>)
        fun onFailed(message: String)
    }

    interface OnCallBackDashboardListener {
        fun onSuccessDataDashboard(hotelDashboardModel: HotelDashboardModel)
        fun onFailed(message: String)
    }

    interface OnCallBackHistoryHotelListener {
        fun onSuccessHistoryHotel(hotelHistoryList: ArrayList<HotelBookingHistoryModel>)
        fun onSuccessHistoryDetailHotel(hotelHistory: HotelBookingHistoryDetailModel)
        fun onFailed(message: String)
    }

    interface OnCallBackHotelVendorListener {
        fun onSuccessShowWishlistHotel(locationHotelModel: ArrayList<MainItemHomeModel>)
        fun onSuccessHotelOfVendor(hotelHistoryList: ArrayList<HotelMyVendorModel>)
        fun onSuccessRoomHotel(hotelHistory: ArrayList<HotelMyRoomOfHotelVendorModel>)
        fun onSuccessEditStatus(successMsg: String)
        fun onFailed(message: String)
    }

    // ** Travel Article **
    fun getTravelArticleWs(
        context: Context,
        page: Int,
        limit: Int,
        onCallBackTravelArticleListener: OnCallBackTravelArticleListener
    ) {
        val serviceApi = RetrofitGenerator.createServiceWithHotelBooking(context)
        val res = serviceApi.getArticleListt(page, limit)
        res.enqueue(CustomCallback(context, object : CustomResponseListener {
            override fun onSuccess(resObj: JsonObject) {
                if (resObj.has("data")) {
                    val list: ArrayList<HotelArticleModel> = ArrayList()
                    val data = resObj["data"].asJsonArray
                    for (i in 0 until data.size()) {
                        val item: JsonElement = data[i].asJsonObject
                        val historyModel: HotelArticleModel =
                            Gson().fromJson(item, HotelArticleModel::class.java)
                        list.add(historyModel)
                    }
                    onCallBackTravelArticleListener.onSuccessArticleList(list)
                } else {
                    onCallBackTravelArticleListener.onFailed(resObj["msg"].asString)
                }
            }

            override fun onError(error: String, resCode: Int) {
                onCallBackTravelArticleListener.onFailed(error)
            }
        }))
    }

    fun getTravelArticleDetailWs(
        context: Context,
        id: String,
        onCallBackTravelArticleListener: OnCallBackTravelArticleListener
    ) {
        val serviceApi = RetrofitGenerator.createServiceWithHotelBooking(context)
        val res = serviceApi.getArticleDetail(id)
        res.enqueue(CustomCallback(context, object : CustomResponseListener {
            override fun onSuccess(resObj: JsonObject) {
                if (resObj.has("data")) {
                    if (!resObj["data"].isJsonNull) {
                        val item: JsonElement = resObj["data"].asJsonObject
                        val historyModel: HotelArticleModel =
                            Gson().fromJson(item, HotelArticleModel::class.java)
                        onCallBackTravelArticleListener.onSuccessArticleDetail(historyModel)
                    } else {
                        onCallBackTravelArticleListener.onFailed(resObj["msg"].toString())
                    }
                } else {
                    onCallBackTravelArticleListener.onFailed(resObj["msg"].asString)
                }
            }

            override fun onError(error: String, resCode: Int) {
                onCallBackTravelArticleListener.onFailed(error)
            }
        }))
    }

    // ** Travel Talk **
    fun getTravelTalkWs(
        context: Context,
        page: Int,
        limit: Int,
        userId: String,
        onCallBackTravelArticleListener: OnCallBackTravelTalkListener
    ) {
        val serviceApi = RetrofitGenerator.createServiceWithHotelBooking(context)
        val res = serviceApi.getTravelTalkList(page, limit, userId)
        res.enqueue(CustomCallback(context, object : CustomResponseListener {
            override fun onSuccess(resObj: JsonObject) {
                if (resObj.has("data")) {
                    onCallBackTravelArticleListener.onSuccessTravelTalkList(
                        GenerateRespondWs.onGenerateRespondDataListWs(
                            resObj,
                            HotelTravelTalkModel::class.java
                        )
                    )
                } else {
                    onCallBackTravelArticleListener.onFailed(resObj["msg"].asString)
                }
            }

            override fun onError(error: String, resCode: Int) {
                onCallBackTravelArticleListener.onFailed(error)
            }
        }))
    }

    fun getDetailReplyTravelTalkWs(
        context: Context,
        page: Int,
        limit: Int,
        userId: String,
        id: String,
        onCallBackTravelArticleListener: OnCallBackTravelTalkListener
    ) {
        val serviceApi = RetrofitGenerator.createServiceWithHotelBooking(context)
        val res = serviceApi.getReplyCommentListTravelTalk(page, limit, userId, id)
        res.enqueue(CustomCallback(context, object : CustomResponseListener {
            override fun onSuccess(resObj: JsonObject) {
                if (resObj.has("data")) {
                    onCallBackTravelArticleListener.onSuccessTravelTalkReplyDetail(
                        GenerateRespondWs.onGenerateRespondDataObjWs(
                            resObj,
                            TravelTalkReplyDetail::class.java
                        )
                    )
                } else {
                    onCallBackTravelArticleListener.onFailed(resObj["msg"].asString)
                }
            }

            override fun onError(error: String, resCode: Int) {
                onCallBackTravelArticleListener.onFailed(error)
            }
        }))
    }

    fun createPostTravelTalkWs(
        context: Context,
        action: String,
        hashMap: HashMap<String, Any>,
        onCallBackTravelArticleListener: OnCallBackTravelTalkListener
    ) {
        val serviceApi = RetrofitGenerator.createServiceWithHotelBooking(context)
        val res = when (action) {
            "do_post" -> {
                serviceApi.createPostTravelTalk(hashMap)
            }
            "do_like" -> {
                serviceApi.doLikeTravelTalk(hashMap)
            }
            "do_reply_comment" -> {
                serviceApi.doReplyTravelTalk(hashMap)
            }
            "do_share" -> {
                serviceApi.getSharePost(hashMap)
            }
            else -> {
                serviceApi.createPostTravelTalk(hashMap)
            }
        }
        res.enqueue(CustomCallback(context, object : CustomResponseListener {
            override fun onSuccess(resObj: JsonObject) {
                if (GenerateRespondWs.onGenerateRespondWs(resObj)) {
                    onCallBackTravelArticleListener.onCreatePostSuccess(resObj["msg"].asString)
                } else {
                    onCallBackTravelArticleListener.onFailed(resObj["msg"].asString)
                }
            }

            override fun onError(error: String, resCode: Int) {
                onCallBackTravelArticleListener.onFailed(error)
            }
        }))
    }

    fun getCategoryPostWs(context: Context, onCallBackListener: OnCallBackCategoryListener) {
        val serviceApi = RetrofitGenerator.createServiceWithHotelBooking(context)
        val res = serviceApi.categoryPost
        res.enqueue(CustomCallback(context, object : CustomResponseListener {
            override fun onSuccess(resObj: JsonObject) {
                if (resObj.has("data")) {
                    onCallBackListener.onSuccessCategoryPostList(
                        GenerateRespondWs.onGenerateRespondDataListWs(
                            resObj,
                            Category::class.java
                        )
                    )
                } else {
                    onCallBackListener.onFailed(resObj["msg"].asString)
                }
            }

            override fun onError(error: String, resCode: Int) {
                onCallBackListener.onFailed(error)
            }
        }))
    }

    fun getLikeSharePostByUserWs(
        context: Context,
        page: Int,
        limit: Int,
        userId: String,
        type: String,
        onCallBackTravelArticleListener: OnCallBackTravelTalkListener
    ) {
        val serviceApi = RetrofitGenerator.createServiceWithHotelBooking(context)
        val res = serviceApi.getLikeSharePostByUser(page, limit, userId, type)
        res.enqueue(CustomCallback(context, object : CustomResponseListener {
            override fun onSuccess(resObj: JsonObject) {
                if (resObj.has("data")) {
                    onCallBackTravelArticleListener.onSuccessTravelTalkList(
                        GenerateRespondWs.onGenerateRespondDataListWs(
                            resObj,
                            HotelTravelTalkModel::class.java
                        )
                    )
                } else {
                    onCallBackTravelArticleListener.onFailed(resObj["msg"].asString)
                }
            }

            override fun onError(error: String, resCode: Int) {
                onCallBackTravelArticleListener.onFailed(error)
            }
        }))
    }

    interface OnCallBackTravelArticleListener {
        fun onSuccessArticleList(hotelDashboardModel: ArrayList<HotelArticleModel>)
        fun onSuccessArticleDetail(hotelDashboardModel: HotelArticleModel)
        fun onFailed(message: String)
    }

    interface OnCallBackTravelTalkListener {
        fun onSuccessTravelTalkList(travelTalkModelList: ArrayList<HotelTravelTalkModel>)
        fun onSuccessTravelTalkReplyDetail(travelTalkModelList: TravelTalkReplyDetail)
        fun onCreatePostSuccess(message: String)
        fun onFailed(message: String)
    }

    interface OnCallBackCategoryListener {
        fun onSuccessCategoryPostList(travelTalkModelList: ArrayList<Category>)
        fun onFailed(message: String)
    }

    // ** Discount Item **
    fun getDiscountItemWs(
        context: Context,
        page: Int,
        limit: Int,
        category: String,
        onCallBackDiscountListener: OnCallBackDiscountListener
    ) {
        val serviceApi = RetrofitGenerator.createServiceWithHotelBooking(context)
        val res = serviceApi.getListDiscountItem(page, limit, category)
        res.enqueue(CustomCallback(context, object : CustomResponseListener {
            override fun onSuccess(resObj: JsonObject) {
                if (resObj.has("data")) {
                    val jsonObject = resObj["data"].asJsonObject

                    val listImageSlider: ArrayList<String> = ArrayList()
                    val subItemHomeModelList = ArrayList<SubItemHomeModel>()

                    // Image slider
                    if (jsonObject.has("banners")) {
                        val imgList = jsonObject["banners"].asJsonArray
                        for (i in 0 until imgList.size()) {
                            val itemImg = imgList[i].asString
                            listImageSlider.add(itemImg)
                        }
                    }

                    // Item discount
                    if (jsonObject.has("places")) {
                        val dataArray = jsonObject["places"].asJsonArray
                        for (i in 0 until dataArray.size()) {
                            subItemHomeModelList.add(
                                getItemHotel(
                                    dataArray[i].asJsonObject,
                                    "bestseller_listing",
                                    category
                                )
                            )
                        }
                    }

                    onCallBackDiscountListener.onSuccessDiscountList(
                        listImageSlider,
                        subItemHomeModelList
                    )

                } else {
                    onCallBackDiscountListener.onFailed(resObj["msg"].asString)
                }
            }

            override fun onError(error: String, resCode: Int) {
                onCallBackDiscountListener.onFailed(error)
            }
        }))
    }

    interface OnCallBackDiscountListener {
        fun onSuccessDiscountList(
            sliderBanner: ArrayList<String>,
            discountList: ArrayList<SubItemHomeModel>
        )

        fun onFailed(message: String)
    }
}