package com.eazy.daikou.request_data.request.project_development_ws

import android.content.Context
import com.eazy.daikou.R
import com.eazy.daikou.repository_ws.CustomCallback
import com.eazy.daikou.repository_ws.CustomResponseListener
import com.eazy.daikou.repository_ws.RetrofitConnector
import com.eazy.daikou.model.home_page.home_project.*
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class ProjectDevelopmentWs {

    companion object {

        fun getHomePage(
            context: Context,
            page: String,
            limit: String,
            late: String,
            lng: String,
            callBackListener: CallBackGetNearBy
        ) {
            val res = RetrofitConnector.createService(context).getHomePageItem(page, limit, late, lng)

            val listImageSlider = ArrayList<String>()
            val listCategory = ArrayList<BranchTypes>()
            val listNearby = ArrayList<Nearby>()
            val listFeatured = ArrayList<Featured>()
            val listAllBranches = ArrayList<AllBranches>()

            res.enqueue(CustomCallback(context, object : CustomResponseListener {
                override fun onSuccess(resObj: JsonObject) {
                    if (resObj.has("data")) {
                        val jsonObject = resObj["data"].asJsonObject
                        val jsonArrayImages = jsonObject.getAsJsonArray("image_sliders")
                        val jsonArrayCategory = jsonObject.getAsJsonArray("branch_types")
                        val jsonArrayNearby = jsonObject.getAsJsonArray("nearby")
                        val jsonArrayFeatured = jsonObject.getAsJsonArray("featured")
                        val jsonArrayAllList = jsonObject.getAsJsonArray("all_branches")

                        // add image slider to list
                        for (index in 0 until jsonArrayImages.size()) {
                            listImageSlider.add(
                                jsonArrayImages[index].toString()
                            )
                        }

                        // add to category list
                        for (index in 0 until jsonArrayCategory.size()) {
                            listCategory.add(
                                Gson().fromJson(
                                    jsonArrayCategory[index],
                                    BranchTypes::class.java
                                )
                            )
                        }

                        // add near by
                        for (index in 0 until jsonArrayNearby.size()) {
                            listNearby.add(
                                Gson().fromJson(jsonArrayNearby[index], Nearby::class.java)
                            )
                        }

                        // add feature
                        for (index in 0 until jsonArrayFeatured.size()) {
                            listFeatured.add(
                                Gson().fromJson(jsonArrayFeatured[index], Featured::class.java)
                            )
                        }

                        // add all
                        for (index in 0 until jsonArrayAllList.size()) {
                            listAllBranches.add(
                                Gson().fromJson(jsonArrayAllList[index], AllBranches::class.java)
                            )
                        }


                        callBackListener.onResponeListCategory(listCategory)
                        callBackListener.onResponeListImage(listImageSlider)
                        callBackListener.onResponeListNearBy(listNearby)
                        callBackListener.onResponeListFeatured(listFeatured)
                        callBackListener.onResponeAllListProperty(listAllBranches)
                    } else {
                        callBackListener.onLoadingFailed(resObj.get("msg").toString())
                    }
                }

                override fun onError(error: String, resCode: Int) {
                    callBackListener.onLoadingFailed(error)
                }
            }))
        }

        // get item for recommend
        fun getMoreRecommend(
            context: Context,
            page: String,
            limit: String,
            x: String,
            y: String,
            callBackGetNearBy: CallBackGetNearBy
        ) {
            val getMoreRecommend = RetrofitConnector.createService(context).getHomePageItem(
                page,
                limit,
                x,
                y
            )

            getMoreRecommend.enqueue(object : Callback<JsonElement> {
                override fun onResponse(call: Call<JsonElement>, response: Response<JsonElement>) {
                    val listAllBranches = ArrayList<AllBranches>()
                    if (response.isSuccessful && response.body() is JsonObject) {
                        val jsonObject = (response.body() as JsonObject).getAsJsonObject("data")
                        val jsonArrayAllBranches = jsonObject.getAsJsonArray("all_branches")

                        for (index in 0 until jsonArrayAllBranches.size()) {
                            listAllBranches.add(
                                Gson().fromJson(
                                    jsonArrayAllBranches[index],
                                    AllBranches::class.java
                                )
                            )
                        }

                        callBackGetNearBy.onResponeAllListProperty(listAllBranches)
                    } else {
                        callBackGetNearBy.onLoadingFailed(context.getString(R.string.something_went_wrong))
                    }
                }

                override fun onFailure(call: Call<JsonElement>, t: Throwable) {
                    callBackGetNearBy.onGettingItemFailed(context.getString(R.string.please_check_your_internet_connection))
                }

            })
        }


        // get list by category
        fun getListByCategory(
            context: Context,
            key: String,
            page: String,
            limit: String,
            callBackGetNearBy: CallBackGetNearBy
        ) {
            val getListCategory = RetrofitConnector.createService(context).getListByCategory(
                key,
                page,
                limit
            )

            val listProperty = ArrayList<AllBranches>()
            getListCategory.enqueue(object : Callback<JsonElement> {
                override fun onResponse(call: Call<JsonElement>, response: Response<JsonElement>) {
                    if (response.isSuccessful && response.body() is JsonObject) {

                        val jsonArray = (response.body() as JsonObject).getAsJsonArray("data")
                        for (index in 0 until jsonArray.size()) {
                            listProperty.add(
                                Gson().fromJson(jsonArray[index], AllBranches::class.java)
                            )
                        }

                        callBackGetNearBy.onResponeAllListProperty(listProperty)
                    } else {
                        callBackGetNearBy.onLoadingFailed(context.getString(R.string.something_went_wrong))
                    }
                }

                override fun onFailure(call: Call<JsonElement>, t: Throwable) {
                    callBackGetNearBy.onGettingItemFailed(context.getString(R.string.please_check_your_internet_connection))
                }

            })
        }


        //get feature list
        fun getListFeature(
            context: Context,
            page: String,
            limit: String,
            callBackGetNearBy: CallBackGetNearBy
        ) {
            val getListFeatured =
                RetrofitConnector.createService(context).getListFeatured(page, limit)
            val listFeatured = ArrayList<Featured>()
            getListFeatured.enqueue(object : Callback<JsonElement> {
                override fun onResponse(call: Call<JsonElement>, response: Response<JsonElement>) {
                    if (response.isSuccessful && response.body() is JsonObject) {
                        val jsonObject = (response.body() as JsonObject)
                        val jsonArray = jsonObject.getAsJsonArray("data")
                        for (index in jsonArray) {
                            listFeatured.add(
                                Gson().fromJson(index, Featured::class.java)
                            )
                        }
                        callBackGetNearBy.onResponeListFeatured(listFeatured)
                    } else {
                        callBackGetNearBy.onLoadingFailed(context.getString(R.string.something_went_wrong))
                    }
                }

                override fun onFailure(call: Call<JsonElement>, t: Throwable) {
                    callBackGetNearBy.onGettingItemFailed(context.getString(R.string.please_check_your_internet_connection))
                }
            })
        }

        //get nearby list
        fun getListNearBy(
            context: Context,
            page: String,
            limit: String,
            x: String,
            y: String,
            callBackGetNearBy: CallBackGetNearBy
        ) {
            val getListNearBy =
                RetrofitConnector.createService(context).getListNearBy(page, limit, x, y)
            val listNearBy = ArrayList<Nearby>()
            getListNearBy.enqueue(object : Callback<JsonElement> {
                override fun onResponse(call: Call<JsonElement>, response: Response<JsonElement>) {
                    if (response.isSuccessful && response.body() is JsonObject) {
                        val jsonObject = response.body() as JsonObject
                        val jsonArray = jsonObject.getAsJsonArray("data")
                        for (index in jsonArray) {
                            listNearBy.add(
                                Gson().fromJson(index, Nearby::class.java)
                            )
                        }
                        callBackGetNearBy.onResponeListNearBy(listNearBy)
                    } else {
                        callBackGetNearBy.onLoadingFailed(context.getString(R.string.something_went_wrong))
                    }
                }

                override fun onFailure(call: Call<JsonElement>, t: Throwable) {
                    callBackGetNearBy.onGettingItemFailed(context.getString(R.string.please_check_your_internet_connection))
                }

            })
        }

        fun getDetail(context: Context,id:Int,callback: OnRespondListener){
            val getDetailsCall=
                RetrofitConnector.createService(context).getPropertyDetail(id)
            val listRelatedBranches = ArrayList<RelatedBranches>()
            val listImages = ArrayList<String>()
            getDetailsCall.enqueue(object :Callback<JsonElement>{
                override fun onResponse(call: Call<JsonElement>, response: Response<JsonElement>) {
                    if (response.isSuccessful && response.body() is JsonObject)
                    {
                        if ((response.body() as JsonObject).get("success").asBoolean){
                            val jsonObject = (response.body() as JsonObject).getAsJsonObject("data")
                            val jsonArrayRelatedBranches = jsonObject.getAsJsonArray("related_branches")
                            val jsonArrayImage = jsonObject.getAsJsonArray("images")
                            // add list related
                            for (index in 0 until jsonArrayRelatedBranches.size()){
                                listRelatedBranches.add(
                                    Gson().fromJson(jsonArrayRelatedBranches[index],RelatedBranches::class.java)
                                )
                            }

                            for (index in 0 until jsonArrayImage.size()){
                                listImages.add(
                                    jsonArrayImage[index].toString()
                                )
                            }
                            callback.onImage(listImages)
                            callback.onSuccess(Gson().fromJson(jsonObject,DataDetail::class.java))
                            callback.onRelatedProperty(listRelatedBranches)
                        }else
                        {
                            callback.onError((response.body() as JsonObject).get("msg").toString())
                        }

                    }
                    else
                    {
                        callback.onError(context.getString(R.string.something_went_wrong))
                    }
                }

                override fun onFailure(call: Call<JsonElement>, t: Throwable) {
                    callback.onInternet(context.getString(R.string.please_check_your_internet_connection))
                }

            })
        }

    }

    interface CallBackGetNearBy {
        fun onResponeListImage(listImageSlider: ArrayList<String>)
        fun onResponeListCategory(listCategory: ArrayList<BranchTypes>)
        fun onResponeListNearBy(listNearBy: ArrayList<Nearby>)
        fun onResponeListFeatured(listFeatured: ArrayList<Featured>)
        fun onResponeAllListProperty(listAllProperty: ArrayList<AllBranches>)
        fun onLoadingFailed(msg: String?)
        fun onGettingItemFailed(msg: String?)
    }

    interface OnRespondListener {
        fun onSuccess(propertyDetailModel: DataDetail)
        fun onRelatedProperty(listRelatedBranches:ArrayList<RelatedBranches>)
        fun onImage(listImage:List<String>)
        fun onError(msg:String)
        fun onInternet(msg: String)
    }
}