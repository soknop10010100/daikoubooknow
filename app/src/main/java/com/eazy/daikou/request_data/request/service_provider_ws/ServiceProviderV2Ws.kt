package com.eazy.daikou.request_data.request.service_provider_ws

import android.content.Context
import com.eazy.daikou.repository_ws.RetrofitConnector
import com.eazy.daikou.repository_ws.CustomCallback
import com.eazy.daikou.repository_ws.CustomResponseListener
import com.eazy.daikou.model.my_property.service_provider.DetailServiceProviderModel
import com.eazy.daikou.model.my_property.service_provider.SubCategoryServiceProviderV2Model
import com.eazy.daikou.model.my_property.service_provider.ServiceProviderCategoryModel
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.google.gson.JsonObject

class ServiceProviderV2Ws {

    fun getServiceProviderCategoryWS(context: Context, callBackCategory : OnCallBackCategoryListener){
        val serviceApi = RetrofitConnector.createService(context)
        val res = serviceApi.ListServiceProviderCategoryItem()
        res.enqueue(CustomCallback (context, object : CustomResponseListener{
            override fun onSuccess(resObj: JsonObject?) {
                if(resObj != null){
                    if (resObj.has("data")){
                        val listProviderCategory : ArrayList<ServiceProviderCategoryModel> = ArrayList()
                        val data = resObj["data"].asJsonArray
                        for (i in 0 until data.size()){
                            val item: JsonElement = data[i].asJsonObject
                            val serviceCategoryModel : ServiceProviderCategoryModel = Gson().fromJson(item, ServiceProviderCategoryModel::class.java)
                            listProviderCategory.add(serviceCategoryModel)
                        }

                        callBackCategory.onLoadSuccessFull(listProviderCategory)
                    } else {
                        callBackCategory.onLoadFailed(resObj.get("msg").asString)
                    }
                }
            }

            override fun onError(error: String, resCode: Int) {
                callBackCategory.onLoadFailed(error)
            }
        }))
    }

    fun getSubCategoryServiceProviderWs(context: Context, page: Int, limit: Int, categoryKeyword: String, callBackSub : OnCallBackSubCategory){
        val serviceApi = RetrofitConnector.createService(context)
        val res = serviceApi.getSubListCategoryServiceProvider(page, limit, categoryKeyword)
        res.enqueue(CustomCallback (context, object : CustomResponseListener{
            override fun onSuccess(resObj: JsonObject?) {
                if(resObj != null){
                    if (resObj.has("data")){
                        val listSub : ArrayList<SubCategoryServiceProviderV2Model> = ArrayList()
                        val data = resObj["data"].asJsonArray
                        for (i in 0 until data.size()){
                            val item: JsonElement = data[i].asJsonObject
                            val serviceCategoryModel : SubCategoryServiceProviderV2Model = Gson().fromJson(item, SubCategoryServiceProviderV2Model::class.java)
                            listSub.add(serviceCategoryModel)
                        }
                        callBackSub.onLoadSuccessFull(listSub)
                    } else {
                        callBackSub.onLoadFailed(resObj.get("msg").asString)
                    }
                }
            }

            override fun onError(error: String, resCode: Int) {
                callBackSub.onLoadFailed(error)
            }

        }))
    }

    fun getListDetailServiceProviderWs(context: Context,id: String, callBackDetail : OnCallBackListDetail){
        val serviceSApi = RetrofitConnector.createService(context)
        val res = serviceSApi.getListDetailServiceProvider(id)
        res.enqueue(CustomCallback( context, object : CustomResponseListener{
            override fun onSuccess(resObj: JsonObject?) {
               if (resObj != null){
                   if (resObj.has("data")){
                       val data = resObj["data"]
                       val detailService = Gson().fromJson(data, DetailServiceProviderModel::class.java)
                       callBackDetail.onLoadSuccessFull(detailService)
                   } else{
                       callBackDetail.onLoadFailed(resObj["msg"].asString)
                   }
               }
            }
            override fun onError(error: String, resCode: Int) {
               callBackDetail.onLoadFailed(error)
            }

        }))

    }

    interface OnCallBackCategoryListener{
        fun onLoadSuccessFull(listServiceProvider : ArrayList<ServiceProviderCategoryModel>)
        fun onLoadFailed(message: String)
    }
    interface OnCallBackSubCategory{
        fun onLoadSuccessFull(listSubServiceProvider : ArrayList<SubCategoryServiceProviderV2Model>)
        fun onLoadFailed(message: String)
    }
    interface OnCallBackListDetail{
        fun onLoadSuccessFull(detailServiceProvider: DetailServiceProviderModel)
        fun onLoadFailed(message: String)
    }
}