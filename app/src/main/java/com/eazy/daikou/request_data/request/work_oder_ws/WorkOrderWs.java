package com.eazy.daikou.request_data.request.work_oder_ws;

import android.content.Context;

import com.eazy.daikou.repository_ws.RetrofitConnector;
import com.eazy.daikou.repository_ws.CustomCallback;
import com.eazy.daikou.repository_ws.CustomResponseListener;
import com.eazy.daikou.repository_ws.ServiceApi;
import com.eazy.daikou.request_data.static_data.StaticUtilsKey;
import com.eazy.daikou.model.my_property.service_provider.SelectPropertyModel;
import com.eazy.daikou.model.inspection.Author_ClerkUserInfo;
import com.eazy.daikou.model.work_order.AssignWorkOrderModel;
import com.eazy.daikou.model.work_order.ParentTaskWorkOrderModel;
import com.eazy.daikou.model.work_order.WorkOrderDetailModel;
import com.eazy.daikou.model.work_order.WorkOrderItemModel;
import com.eazy.daikou.model.work_order.WorkOrderProgressModel;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;

public class WorkOrderWs {

    public void getListWorkOrder(Context context, int page, String workOrderType, String propertyId, String userId, String userRole, String activeAccountId,CallBackListener callBackListener){
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = serviceApi.getWorkOrderList(page, 10, workOrderType, propertyId, userId, userRole, activeAccountId);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data")){
                    List<WorkOrderItemModel> workOrderItemModelList = new ArrayList<>();
                    JsonArray data = resObj.get("data").getAsJsonArray();
                    for (int i = 0; i < data.size(); i++) {
                        JsonElement jsonElement = data.get(i).getAsJsonObject();
                        WorkOrderItemModel workOrderItemModel = new Gson().fromJson(jsonElement, WorkOrderItemModel.class);
                        workOrderItemModelList.add(workOrderItemModel);
                    }
                    callBackListener.onSuccessWorkOrderList(workOrderItemModelList);
                } else {
                    callBackListener.onFailed(resObj.get("msg").getAsString());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                callBackListener.onFailed(error);
            }
        }));
    }

    public void getDetailWorkOrderById(Context context, String work_order_id, CallBackListener callBackListener){
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = serviceApi.getWorkOrderDetail(work_order_id);

        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data") && !resObj.get("data").isJsonNull()){
                    JsonObject jsonObject = resObj.get("data").getAsJsonObject();
                    WorkOrderDetailModel workOrderDetailModel = new Gson().fromJson(jsonObject, WorkOrderDetailModel.class);
                    callBackListener.onSuccessWorkOrderDetail(workOrderDetailModel);
                } else {
                    callBackListener.onFailed(resObj.get("msg").getAsString());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                callBackListener.onFailed(error);
            }
        }));
    }

    public void getDetailWorkProgressById(Context context, String work_order_id, CallBackWorkProgressListener callBackListener){
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = serviceApi.detailWorkProgress(work_order_id);

        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data") && !resObj.get("data").isJsonNull()){
                    JsonObject jsonObject = resObj.get("data").getAsJsonObject();
                    WorkOrderProgressModel workOrderDetailModel = new Gson().fromJson(jsonObject, WorkOrderProgressModel.class);
                    callBackListener.onSuccessWorkOrderDetail(workOrderDetailModel);
                } else {
                    callBackListener.onFailed(resObj.get("msg").getAsString());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                callBackListener.onFailed(error);
            }
        }));
    }

    public void deleteImageWorkOrder(Context context, HashMap<String, Object> hashMap, CallBackWorkOrderListener callBackListener){
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = serviceApi.deleteImageWorkOrder(hashMap);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("success") && resObj.get("success").getAsString().equals("true")){
                    if (resObj.has("msg")) {
                        callBackListener.onSuccessDeleteImageWorkOrder(resObj.get("msg").getAsString());
                    }
                } else {
                    if (resObj.has("msg")) {
                        callBackListener.onFailed(resObj.get("msg").getAsString());
                    }
                }
            }

            @Override
            public void onError(String error, int resCode) {
                callBackListener.onFailed(error);
            }
        }));
    }

    public void saveWorkOrderDetail(Context context, String doAction,HashMap<String, Object> hashMap, CallBackWorkOrderListener callBackListener){
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = doAction.equalsIgnoreCase("create_new_work_order") || doAction.equalsIgnoreCase("update_work_order") ?
                serviceApi.updateAndAddWorkProgressDetail(hashMap) : serviceApi.updateWorkOrderDetail(hashMap);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("success") && resObj.get("success").getAsString().equals("true")){
                    if (resObj.has("msg")) {
                        callBackListener.onSuccessDeleteImageWorkOrder(resObj.get("msg").getAsString());
                    }
                } else {
                    if (resObj.has("msg")) {
                        callBackListener.onFailed(resObj.get("msg").getAsString());
                    }
                }
            }

            @Override
            public void onError(String error, int resCode) {
                callBackListener.onFailed(error);
            }
        }));
    }

    public void getParentTaskList(Context context, String parent_task_id, CallBackParenTaskListener callBackParenTaskListener){
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = serviceApi.getParentTaskList(parent_task_id);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data")){
                    if (!resObj.get("data").isJsonNull()) {
                        JsonObject jsonObject = resObj.get("data").getAsJsonObject();
                        ParentTaskWorkOrderModel itemTemplateAPIModel = new Gson().fromJson(jsonObject, ParentTaskWorkOrderModel.class);
                        callBackParenTaskListener.onSuccessParentTask(itemTemplateAPIModel);
                    } else {
                        callBackParenTaskListener.onFailed(StaticUtilsKey.no_item_found);
                    }
                } else {
                    callBackParenTaskListener.onFailed(resObj.get("msg").getAsString());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                callBackParenTaskListener.onFailed(error);
            }
        }));
    }

    public void getListAssignWorkOrder(Context context, String room_item_id, CallBackAssignWorkOrderListener callBackParenTaskListener){
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = serviceApi.getListAssignWorkOrder(room_item_id);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data")){
                    List<AssignWorkOrderModel> workOrderItemModelList = new ArrayList<>();
                    JsonArray data = resObj.get("data").getAsJsonArray();
                    for (int i = 0; i < data.size(); i++) {
                        JsonElement jsonElement = data.get(i).getAsJsonObject();
                        AssignWorkOrderModel assignWorkOrderModel = new Gson().fromJson(jsonElement, AssignWorkOrderModel.class);
                        workOrderItemModelList.add(assignWorkOrderModel);
                    }
                    callBackParenTaskListener.onSuccessAssignWorkOrder(workOrderItemModelList);
                } else {
                    callBackParenTaskListener.onFailed(resObj.get("msg").getAsString());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                callBackParenTaskListener.onFailed(error);
            }
        }));
    }

    public void createWorkOrder(Context context, HashMap<String, Object> hashMap, CallBackWorkOrderListener callBackListener){
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = serviceApi.createWorkOrder(hashMap);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("success") && resObj.get("success").getAsString().equals("true")){
                    if (resObj.has("msg")) {
                        callBackListener.onSuccessDeleteImageWorkOrder(resObj.get("msg").getAsString());
                    }
                } else {
                    if (resObj.has("msg")) {
                        callBackListener.onFailed(resObj.get("msg").getAsString());
                    }
                }
            }

            @Override
            public void onError(String error, int resCode) {
                callBackListener.onFailed(error);
            }
        }));
    }


    public void selectEmployeeDepartment(Context context, int page, int limit, String property_id, String departmentId, CallBackDepartmentListener callBackListener){
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = serviceApi.getEmployeeByDepartmentId(page, limit, property_id, departmentId);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data")){
                    List<Author_ClerkUserInfo> workOrderItemModelList = new ArrayList<>();
                    JsonArray data = resObj.get("data").getAsJsonArray();
                    for (int i = 0; i < data.size(); i++) {
                        JsonElement jsonElement = data.get(i).getAsJsonObject();
                        Author_ClerkUserInfo assignWorkOrderModel = new Gson().fromJson(jsonElement, Author_ClerkUserInfo.class);
                        workOrderItemModelList.add(assignWorkOrderModel);
                    }
                    callBackListener.onSuccessAssignee(workOrderItemModelList);
                } else {
                    callBackListener.onFailed(resObj.get("msg").getAsString());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                callBackListener.onFailed(error);
            }
        }));
    }

    public void selectDepartment(Context context, String property_id, CallBackDepartmentListener callBackListener){
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = serviceApi.getDepartmentName(property_id);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data")){
                    List<SelectPropertyModel> workOrderItemModelList = new ArrayList<>();
                    JsonArray data = resObj.get("data").getAsJsonArray();
                    for (int i = 0; i < data.size(); i++) {
                        JsonElement jsonElement = data.get(i).getAsJsonObject();
                        SelectPropertyModel assignWorkOrderModel = new Gson().fromJson(jsonElement, SelectPropertyModel.class);
                        workOrderItemModelList.add(assignWorkOrderModel);
                    }
                    callBackListener.onSuccessDepartment(workOrderItemModelList);
                } else {
                    callBackListener.onFailed(resObj.get("msg").getAsString());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                callBackListener.onFailed(error);
            }
        }));
    }

    public interface CallBackListener{
        void onSuccessWorkOrderList(List<WorkOrderItemModel> workOrderItemModelList);
        void onSuccessWorkOrderDetail(WorkOrderDetailModel workOrderDetailModel);
        void onFailed(String msg);
    }

    public interface CallBackWorkProgressListener{
        void onSuccessWorkOrderDetail(WorkOrderProgressModel workOrderDetailModel);
        void onFailed(String msg);
    }

    public interface CallBackWorkOrderListener{
        void onSuccessDeleteImageWorkOrder(String msg);
        void onFailed(String msg);
    }

    public interface CallBackParenTaskListener{
        void onSuccessParentTask(ParentTaskWorkOrderModel itemTemplateAPIModel);
        void onFailed(String msg);
    }

    public interface CallBackAssignWorkOrderListener{
        void onSuccessAssignWorkOrder(List<AssignWorkOrderModel> itemTemplateAPIModel);
        void onFailed(String msg);
    }

    public interface CallBackDepartmentListener{
        void onSuccessAssignee(List<Author_ClerkUserInfo> assigneeEmployee);
        void onSuccessDepartment(List<SelectPropertyModel> assigneeEmployee);
        void onFailed(String msg);
    }
}
