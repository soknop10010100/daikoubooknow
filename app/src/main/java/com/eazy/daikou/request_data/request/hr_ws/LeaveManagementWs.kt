package com.eazy.daikou.request_data.request.hr_ws

import android.app.Activity
import com.eazy.daikou.repository_ws.*
import com.eazy.daikou.model.hr.*
import com.google.gson.Gson
import com.google.gson.JsonObject

class LeaveManagementWs {

    fun getLeaveManagement(context: Activity, userBusinessKey: String, page: Int, limit: Int, listType: String, accountBusinessKey: String, leaveCallBack: OnCallBackLeaveManagementListener) {
        val listLeave = ArrayList<LeaveManagement>()
        val api = RetrofitGenerator.createServiceWithHR(context)
        val res = api.getLeaveManagement(userBusinessKey ,page, limit, listType, accountBusinessKey)
        res.enqueue(CustomCallback(context, object : CustomResponseListener {
            override fun onSuccess(resObj: JsonObject) {
                if (resObj.has("data")) {
                        val jsonArray = resObj.getAsJsonArray("data")
                        for (i in 0 until jsonArray.size()) {
                            val item = jsonArray[i].asJsonObject
                            val leaveManagement =
                                Gson().fromJson(item.toString(), LeaveManagement::class.java)
                            listLeave.add(leaveManagement)
                        }
                    leaveCallBack.onLoadListSuccessFull(listLeave)

                } else {
                    if (resObj["msg"].asString != "Un") {
                        leaveCallBack.onLoadFail(resObj["msg"].asString)
                    } else {
                        CustomCallback.popupSessionExpired(context)
                    }

                }
            }

            override fun onError(error: String, resCode: Int) {
                leaveCallBack.onLoadFail(error)
            }
        }))
    }


    fun getLeaveCategoryManagement(
        context: Activity,
        userId: String,
        leaveCallBack: OnCallBackLeaveManagementListener) {
        val listLeaveBalance = ArrayList<LeaveBalance>()

        val api = RetrofitGenerator.createServiceWithHR(context)
        val res = api.getLeaveCategoryManagement(userId)
        res.enqueue(CustomCallback(context, object : CustomResponseListener {
            override fun onSuccess(resObj: JsonObject) {
                if (resObj.has("data")) {
                    val jsonArray = resObj.getAsJsonArray("data")
                    for (i in 0 until jsonArray.size()) {
                        val item = jsonArray[i].asJsonObject
                        val leaveBalance =
                            Gson().fromJson(item.toString(), LeaveBalance::class.java)
                        listLeaveBalance.add(leaveBalance)
                    }
                    leaveCallBack.onLoadListCategorySuccessFull(listLeaveBalance)

                } else {
                    if (resObj["msg"].asString != "Un") {
                        leaveCallBack.onLoadFail(resObj["msg"].asString)
                    } else {
                        CustomCallback.popupSessionExpired(context)
                    }

                }
            }

            override fun onError(error: String, resCode: Int) {
                leaveCallBack.onLoadFail(error)
            }
        }))
    }


    fun getLeaveCategory(context: Activity, userId: String, searchApprover: String, callBack: OnCallBackLeaveTypeCategoryListener) {
        val listLeaveCategory = LeaveTypeCategory()
        val api = RetrofitGenerator.createServiceWithHR(context)
        val res = api.getLeaveTypeCategory(userId, searchApprover)
        res.enqueue(CustomCallback(context, object : CustomResponseListener {
            override fun onSuccess(resObj: JsonObject) {
                if (resObj.has("data")) {
                    val obj = resObj.getAsJsonObject("data")
                    val listLeave = ArrayList<LeaveCategory>()
                    val listEmployee = ArrayList<AssignEmployee>()
                    if (obj.has("leave_categories")) {
                        val jsonArray = obj.getAsJsonArray("leave_categories")
                        for (i in 0 until jsonArray.size()) {
                            val item = jsonArray[i].asJsonObject
                            val leaveCategory =
                                Gson().fromJson(item.toString(), LeaveCategory::class.java)
                            listLeave.add(leaveCategory)
                            listLeaveCategory.leave_categories = listLeave
                        }
                    }

                    if (obj.has("approved_by")) {
                        val jsonArray = obj.getAsJsonArray("approved_by")
                        for (i in 0 until jsonArray.size()) {
                            val item = jsonArray[i].asJsonObject
                            val employee =
                                Gson().fromJson(item.toString(), AssignEmployee::class.java)
                            listEmployee.add(employee)
                            listLeaveCategory.approved_by = listEmployee
                        }

                    }

                    callBack.onLoadListSuccessFull(listLeaveCategory)
                } else if (resObj.has("msg")) {
                    callBack.onLoadFail(resObj["msg"].asString)
                }
            }

            override fun onError(error: String, resCode: Int) {
                callBack.onLoadFail(error)
            }
        }))
    }

    fun createLeaveManagement(
        context: Activity,
        hashMap: HashMap<String, Any>,
        callBack: OnCallBackCreateListener
    ) {
        val api = RetrofitGenerator.createServiceWithHR(context)
        val res = api.createLeaveManagement(hashMap)
        res.enqueue(CustomCallback(context, object : CustomResponseListener {
            override fun onSuccess(resObj: JsonObject) {
                if (resObj.has("success") && resObj["success"].asBoolean) {
                    if (resObj.has("msg")) {
                        val message = resObj.get("msg").asString
                        callBack.onLoadListSuccessFull(message)
                    }
                } else {
                    callBack.onLoadFail(resObj["msg"].asString)
                }
            }

            override fun onError(error: String, resCode: Int) {
                callBack.onLoadFail(error)
            }
        }))
    }

    fun leaveManagementDetail(context: Activity, id: String, callBack: OnCallBackLeaveTypeDetailListener) {
        val api = RetrofitGenerator.createServiceWithHR(context)
        val res = api.getLeaveDetail(id)
        res.enqueue(CustomCallback(context, object : CustomResponseListener {
            override fun onSuccess(resObj: JsonObject) {
                if (resObj.has("data")) {
                    val data = resObj["data"]
                    val leaveManagement = Gson().fromJson(data, LeaveRequestDetail::class.java)
                    callBack.onLoadListSuccessFull(leaveManagement)
                } else {
                    callBack.onLoadFail(resObj["msg"].asString)
                }
            }

            override fun onError(error: String, resCode: Int) {
                callBack.onLoadFail(error)
            }
        }))
    }

    fun getActionApproveAndRejectWs( context : Activity, hashMap : HashMap<String, Any>, callBackApproveAndReject : OnCallBackCreateListener){
        val api = RetrofitGenerator.createServiceWithHR(context)
        val res = api.getActionApproveAndReject(hashMap)
        res.enqueue(CustomCallback(context, object : CustomResponseListener{
            override fun onSuccess(resObj: JsonObject?) {
                if (resObj != null) {
                    if (resObj.has("success") && resObj["success"].asBoolean){
                        if (resObj.has("msg")){
                            val message = resObj.get("msg").asString
                            callBackApproveAndReject.onLoadListSuccessFull(message)
                        }
                    } else {
                        callBackApproveAndReject.onLoadFail(resObj["msg"].asString)
                    }
                }
            }

            override fun onError(error: String, resCode: Int) {
                    callBackApproveAndReject.onLoadFail(error)
            }

        }))

    }

    interface OnCallBackLeaveManagementListener {
        fun onLoadListSuccessFull(listLeaveManagement: ArrayList<LeaveManagement>)
        fun onLoadListCategorySuccessFull(leaveBalance: ArrayList<LeaveBalance>)
        fun onLoadFail(message: String)

    }

    interface OnCallBackLeaveTypeDetailListener {
        fun onLoadListSuccessFull(leaveDetail: LeaveRequestDetail)
        fun onLoadFail(message: String)

    }

    interface OnCallBackLeaveTypeCategoryListener {
        fun onLoadListSuccessFull(listLeaveCategory: LeaveTypeCategory)
        fun onLoadFail(message: String)

    }

    interface OnCallBackCreateListener {
        fun onLoadListSuccessFull(message: String)
        fun onLoadFail(message: String)

    }
}