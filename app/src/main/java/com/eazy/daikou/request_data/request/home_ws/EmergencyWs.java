package com.eazy.daikou.request_data.request.home_ws;

import android.content.Context;

import com.eazy.daikou.repository_ws.RetrofitConnector;
import com.eazy.daikou.repository_ws.CustomCallback;
import com.eazy.daikou.repository_ws.CustomResponseListener;
import com.eazy.daikou.repository_ws.ServiceApi;
import com.eazy.daikou.model.home.HisEmergencyModel;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;

public class EmergencyWs {

    public void sendEmergency(Context context, HashMap<String, String> hashMap, CallBackEmergency callBackEmergency){
        Call<JsonElement> res = RetrofitConnector.createService(context).getSendEmergency(hashMap);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("success") && resObj.get("success").getAsString().equals("true")){
                    if (resObj.has("msg")) {
                        callBackEmergency.onSuccess(resObj.get("msg").getAsString());
                    }
                } else {
                    if (resObj.has("msg")) {
                        callBackEmergency.onSuccessFalse(resObj.get("msg").getAsString());
                    }
                }

            }

            @Override
            public void onError(String error, int resCode) {
                callBackEmergency.onFailed(error);
            }
        }));
    }

    public void getHistoryEmergency(Context context, Map<String, Object> hashMap, CallBackHistoryEmergency callBackHistoryEmergency){
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = serviceApi.getHistoryEmergency(hashMap);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data")){
                    List<HisEmergencyModel> list = new ArrayList<>();
                    JsonArray data = resObj.getAsJsonArray("data");
                    for (int i=0; i<data.size();i++) {
                        JsonElement item = data.get(i).getAsJsonObject();
                        HisEmergencyModel hisEmergencyModel = new Gson().fromJson(item, HisEmergencyModel.class);
                        list.add(hisEmergencyModel);
                    }
                    callBackHistoryEmergency.hisEmergencyModel(list);
                } else {
                    callBackHistoryEmergency.onFailed(resObj.get("msg").getAsString());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                callBackHistoryEmergency.onFailed(error);
            }
        }));
    }

    public void getHistoryEmergencyDetail(Context context, String emergency_id, CallBackHistoryEmergencyDetail callBackHistoryEmergency){
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = serviceApi.getHistoryEmergencyDetail(emergency_id);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data")){
                    if (!resObj.get("data").isJsonNull()){
                        JsonObject data = resObj.getAsJsonObject("data");
                        HisEmergencyModel hisEmergencyModel = new Gson().fromJson(data, HisEmergencyModel.class);
                        callBackHistoryEmergency.hisEmergencyModel(hisEmergencyModel);
                    } else {
                        callBackHistoryEmergency.onFailed("No Data");
                    }
                } else {
                    callBackHistoryEmergency.onFailed(resObj.get("msg").getAsString());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                callBackHistoryEmergency.onFailed(error);
            }
        }));
    }

    public void statusEmergency(Context context, HashMap<String, Object> hashMap, CallBackEmergency callBackEmergency){
        Call<JsonElement> res = RetrofitConnector.createService(context).statusEmergency(hashMap);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("success") && resObj.get("success").getAsString().equals("true")){
                    if (resObj.has("msg")) {
                        callBackEmergency.onSuccess(resObj.get("msg").getAsString());
                    }
                } else {
                    if (resObj.has("msg")) {
                        callBackEmergency.onSuccessFalse(resObj.get("msg").getAsString());
                    }
                }
            }

            @Override
            public void onError(String error, int resCode) {
                callBackEmergency.onFailed(error);
            }
        }));
    }

    public interface CallBackEmergency{
        void onSuccess(String msgSuccess);
        void onSuccessFalse(String status);
        void onFailed(String mess);
    }

    public interface CallBackHistoryEmergency{
        void hisEmergencyModel(List<HisEmergencyModel> hisEmergencyModels);
        void onFailed(String msg);
    }

    public interface CallBackHistoryEmergencyDetail{
        void hisEmergencyModel(HisEmergencyModel hisEmergencyModels);
        void onFailed(String msg);
    }

}
