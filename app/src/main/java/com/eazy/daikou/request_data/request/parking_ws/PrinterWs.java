package com.eazy.daikou.request_data.request.parking_ws;

import static com.eazy.daikou.helper.Utils.logDebug;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;

import com.eazy.daikou.repository_ws.RetrofitConnector;
import com.eazy.daikou.repository_ws.ServiceApi;
import com.eazy.daikou.model.parking.CheckInOutPrinter;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PrinterWs {

    public void printerCheckIn(Context context, HashMap<String,Object> hashMap, OnRequestPrinterCallBack callBack){
        ServiceApi api = RetrofitConnector.createService(context);
        Call<JsonElement> res = api.pintCheckInt(hashMap);
        res.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(@NonNull Call<JsonElement> call, @NonNull Response<JsonElement> response) {
                if(response.isSuccessful()){
                    assert response.body() != null;
                    JsonObject jsonObject = response.body().getAsJsonObject();
                    if(jsonObject.has("success")){
                        boolean isSuccess = jsonObject.get("success").getAsBoolean();
                        if(isSuccess){
                            if(jsonObject.has("data")){
                                JsonElement data = jsonObject.get("data");
                                CheckInOutPrinter parking = new Gson().fromJson(data, CheckInOutPrinter.class);
                                callBack.onLoadSuccess(parking);
                            }
                        }else {
                            callBack.onLoadFail(jsonObject.get("msg").toString());
                        }
                    }
                }else {
                    if (response.errorBody() != null) {
                        ResponseBody errorString = response.errorBody();
                        logDebug("erorr",response.errorBody().toString());
                        String message = "";
                        try {
                            String json = errorString.string();
                            logDebug("dfkj",errorString.toString());
                            logDebug("error", json);
                            JSONObject jsonOb = new JSONObject(json);
                            if (response.code() == 401) {
                                if(jsonOb.has("message")){
                                    message = jsonOb.getString("message");
                                   callBack.onLoadFail(message);
                                } else if (jsonOb.has("error") && !jsonOb.getString("error").equalsIgnoreCase("Unauthenticated access")) {
                                    message = jsonOb.getString("error");
                                    callBack.onLoadFail(message);
                                } else if (jsonOb.has("msg")){
                                    message = jsonOb.getString("msg");
                                    callBack.onLoadFail(message);
                                }
                            } else if (jsonOb.has("msg")) {
                                message = jsonOb.getString("msg");
                                callBack.onLoadFail(message);
                            } else if (jsonOb.has("error")) {
                                message = jsonOb.getString("error");
                                callBack.onLoadFail(message);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("jfgdf"+e.getClass().getName(), e.getMessage() + "");
                            message = e.getMessage();
                            callBack.onLoadFail(message);
                        } catch (IOException e2) {
                            Log.e(e2.getClass().getName(), e2.getMessage() + "");
                            message = e2.getMessage();
                            callBack.onLoadFail(message);
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<JsonElement> call, @NonNull Throwable t) {
                callBack.onLoadFail(t.getMessage());
            }
        });
    }

    public interface OnRequestPrinterCallBack{
        void onLoadSuccess(CheckInOutPrinter listParking);
        void onLoadFail(String message);
    }
}
