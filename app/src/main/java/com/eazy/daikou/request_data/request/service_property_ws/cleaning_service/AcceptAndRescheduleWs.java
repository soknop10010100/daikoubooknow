package com.eazy.daikou.request_data.request.service_property_ws.cleaning_service;

import android.content.Context;

import com.eazy.daikou.repository_ws.RetrofitConnector;
import com.eazy.daikou.repository_ws.CustomCallback;
import com.eazy.daikou.repository_ws.CustomResponseListener;
import com.eazy.daikou.repository_ws.ServiceApi;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.HashMap;

import retrofit2.Call;

public class AcceptAndRescheduleWs {

    public void acceptAndReschedule(Context context, HashMap<String,Object> hashMap,OnLoadRequestAcceptCallBack onLOadCallBack){
        ServiceApi api = RetrofitConnector.createService(context);
        Call<JsonElement> res = api.acceptAndReschedule(hashMap);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if(resObj.has("success")){
                    boolean isSuccess = resObj.get("success").getAsBoolean();
                    if(isSuccess){
                        if(resObj.has("msg")){
                            onLOadCallBack.onLoadSuccessFull(resObj.get("msg").toString());
                        }
                    }else {
                        if(resObj.has("msg")){
                            onLOadCallBack.onLoadFail(resObj.get("msg").toString());
                        }
                    }
                }

            }

            @Override
            public void onError(String error, int resCode) {
                onLOadCallBack.onLoadFail(error);
            }
        }));
    }

    public interface OnLoadRequestAcceptCallBack {
        void onLoadSuccessFull(String message);
        void onLoadFail(String message);
    }
}
