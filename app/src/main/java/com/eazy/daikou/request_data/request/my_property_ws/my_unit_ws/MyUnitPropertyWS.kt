package com.eazy.daikou.request_data.request.my_property_ws.my_unit_ws

import android.content.Context
import com.eazy.daikou.repository_ws.RetrofitConnector
import com.eazy.daikou.repository_ws.CustomCallback
import com.eazy.daikou.repository_ws.CustomResponseListener
import com.eazy.daikou.request_data.request.book_now_ws.GenerateRespondWs
import com.eazy.daikou.helper.Utils
import com.eazy.daikou.helper.Utils.logDebug
import com.eazy.daikou.model.my_property.my_property.PropertyMyUnitModel
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import id.zelory.compressor.Compressor
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import java.io.File
import java.io.IOException

class MyUnitPropertyWS {

    fun getListPropertyUnitWS(
        context: Context,
        limit: Int,
        page: Int,
        typeAccount: String,
        callBackPropertyMyUnit: CallBackPropertyMyUnit
    ) {
        val serviceApi = RetrofitConnector.createService(context)
        val res = serviceApi.getListMyUnit(page, limit, typeAccount)
        res.enqueue(CustomCallback(context, object : CustomResponseListener {
            override fun onSuccess(resObj: JsonObject?) {
                if (resObj != null) {
                    if (resObj.has("data")) {
                        val listMyUnit: ArrayList<PropertyMyUnitModel> = ArrayList()
                        val data = resObj["data"].asJsonArray
                        for (i in 0 until data.size()) {
                            val item: JsonElement = data[i].asJsonObject
                            val propertyMyUnitModel: PropertyMyUnitModel =
                                Gson().fromJson(item, PropertyMyUnitModel::class.java)
                            listMyUnit.add(propertyMyUnitModel)
                        }
                        callBackPropertyMyUnit.onSuccessFull(listMyUnit)
                    } else {
                        callBackPropertyMyUnit.onFailed(resObj["msg"].asString)
                    }
                }
            }

            override fun onError(error: String, resCode: Int) {
                callBackPropertyMyUnit.onFailed(error)
            }
        }))
    }

    fun getDetailPropertyMyUnitWS(
        context: Context,
        id: String,
        callBackDetail: CallBackDetailPropertyMyUnit
    ) {
        val serviceApi = RetrofitConnector.createService(context)
        val res = serviceApi.getDetailMyUnit(id)
        res.enqueue(CustomCallback(context, object : CustomResponseListener {
            override fun onSuccess(resObj: JsonObject) {
                if (resObj.has("data")) {
                    val data = resObj["data"]
                    val detailPropertyModel = Gson().fromJson(data, PropertyMyUnitModel::class.java)
                    callBackDetail.onSuccessFull(detailPropertyModel)
                } else {
                    callBackDetail.onFailed(resObj["msg"].asString)
                }
            }

            override fun onError(error: String, resCode: Int) {
                callBackDetail.onFailed(error)
            }

        }))
    }

    fun uploadImgVideoUnitWS(
        context: Context,
        action: String,
        hashMap: HashMap<String, Any>,
        callBackDetail: CallBackDetailPropertyMyUnit
    ) {
        val serviceApi = RetrofitConnector.createService(context)
        val res =
            if (action == "upload") serviceApi.uploadImgVideoMyUnit(hashMap) else serviceApi.deleteImgVideoMyUnit(
                hashMap
            )
        res.enqueue(CustomCallback(context, object : CustomResponseListener {
            override fun onSuccess(resObj: JsonObject) {
                if (GenerateRespondWs.onGenerateRespondWs(resObj)) {
                    callBackDetail.onSuccessUploadImgVdo(resObj["msg"].asString)
                } else {
                    callBackDetail.onFailed(resObj["msg"].asString)
                }
            }

            override fun onError(error: String, resCode: Int) {
                callBackDetail.onFailed(error)
            }

        }))
    }

    fun uploadVideoUnitWS(
        context: Context,
        signUpParam: HashMap<String, Any>,
        callBackDetail: CallBackDetailPropertyMyUnit
    ) {
        val serviceApi = RetrofitConnector.createService(context)

        val requestBody = MultipartBody.Builder()
        requestBody.setType(MultipartBody.FORM)
        for (key in signUpParam.keys) {
            if ("file" != key) {
                requestBody.addFormDataPart(key, signUpParam[key].toString())
            } else {
                val path: String = signUpParam[key].toString()
                // Compress file before upload
                var fNew: File? = null
                try {
                    if (path != "") fNew = Compressor(context).compressToFile(File(path))
                    logDebug("filesize", fNew!!.length().toString() + " " + fNew.totalSpace + "s")
                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                    logDebug(ex.javaClass.name, ex.message + "")
                } catch (e: IOException) {
                    e.printStackTrace()
                    logDebug(e.javaClass.name, e.message + "")
                }
                if (fNew == null)
                    fNew = File(path)
                val file = File(path)
                val bodyBye = file.asRequestBody("multipart/form-data".toMediaType())
                requestBody.addFormDataPart(key, Utils.getFileName(fNew.path), bodyBye)
            }
        }

        val res = serviceApi.uploadVideoMyUnit(requestBody.build())
        res.enqueue(CustomCallback(context, object : CustomResponseListener {
            override fun onSuccess(resObj: JsonObject) {
                if (GenerateRespondWs.onGenerateRespondWs(resObj)) {
                    callBackDetail.onSuccessUploadImgVdo(resObj["msg"].asString)
                } else {
                    callBackDetail.onFailed(resObj["msg"].asString)
                }
            }

            override fun onError(error: String, resCode: Int) {
                callBackDetail.onFailed(error)
            }

        }))
    }

    interface CallBackPropertyMyUnit {
        fun onSuccessFull(listMyUnit: ArrayList<PropertyMyUnitModel>)
        fun onFailed(error: String)
    }

    interface CallBackDetailPropertyMyUnit {
        fun onSuccessFull(detailPropertyModel: PropertyMyUnitModel)
        fun onSuccessUploadImgVdo(msg: String)
        fun onFailed(error: String)
    }
}