package com.eazy.daikou.request_data.request.profile_ws;


import android.content.Context;
import com.eazy.daikou.R;
import com.eazy.daikou.repository_ws.RetrofitConnector;
import com.eazy.daikou.repository_ws.CustomCallback;
import com.eazy.daikou.repository_ws.CustomResponseListener;
import com.eazy.daikou.repository_ws.RetrofitGenerator;
import com.eazy.daikou.repository_ws.ServiceApi;
import com.eazy.daikou.helper.InternetConnection;
import com.eazy.daikou.model.profile.User;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class LoginWs {
    public void loginWs(Context context, HashMap<String, String> user, OnRecives recives){
        ServiceApi serviceApi = RetrofitGenerator.createCategoryService(false);
        Call<JsonObject> res = serviceApi.login(user);
        res.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NotNull Call<JsonObject> call, @NotNull Response<JsonObject> response) {
                if (response.isSuccessful() && response.body() != null){
                    String msg = response.body().get("msg").getAsString();
                    String status = response.body().get("success").getAsString();
                    if (status.equals("true")){
                        JsonObject jsonObject = response.body().getAsJsonObject("data");
                        recives.onSuccess(new Gson().fromJson(jsonObject,User.class));
                    } else {
                        recives.onWrong(msg);
                    }
                } else {
                    if (response.errorBody() != null) {
                        ResponseBody errorString = response.errorBody();
                        try {
                            String json = errorString.string();
                            JSONObject jsonObject1 = new JSONObject(json);
                            if (jsonObject1.has("msg")){
                                String message = jsonObject1.getString("msg");
                                recives.onWrong(message);
                            } else  {
                                recives.onFailed(context.getString(R.string.something_went_wrong));
                            }
                        } catch (IOException | JSONException e) {
                            e.printStackTrace();
                            recives.onFailed(context.getString(R.string.something_went_wrong));
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<JsonObject> call, @NotNull Throwable t) {
                if (!InternetConnection.checkInternetConnectionActivity(context)) {
                    recives.onFailed(context.getString(R.string.please_check_your_internet_connection));
                } else  {
                    recives.onFailed(context.getString(R.string.something_went_wrong));
                }
            }
        });
    }

    public void logout(Context context, OnLogoutListener onBackListener){
        Call<JsonElement> res = RetrofitConnector.createService(context).logout();
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("success") && resObj.get("success").getAsString().equals("true")){
                    if (resObj.has("msg")) {
                        onBackListener.onSuccess(resObj.get("msg").getAsString(), new User());
                    }
                } else {
                    if (resObj.has("msg")) {
                        onBackListener.onSuccessFalse(resObj.get("msg").getAsString());
                    }
                }
            }

            @Override
            public void onError(String error, int resCode) {
                onBackListener.onSuccessFalse(error);
            }
        }));
    }

    public void sendVerifyCode(Context context, String action, HashMap<String, Object> hashMap, OnDeleteAccListener callBackSendQrCodeListener) {
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = action.equalsIgnoreCase("get_pin_code") || action.equalsIgnoreCase("verify_pin")? serviceApi.confirmSMSCodeDeleteAcc(hashMap) : serviceApi.deleteUserAccount(hashMap);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("success") && resObj.get("success").getAsString().equals("true")) {
                    if (resObj.has("msg")) {
                        callBackSendQrCodeListener.onSuccess(action, resObj.get("msg").getAsString());
                    }
                } else {
                    if (resObj.has("msg")) {
                        callBackSendQrCodeListener.onFailed(resObj.get("msg").getAsString());
                    }
                }
            }

            @Override
            public void onError(String error, int resCode) {
                callBackSendQrCodeListener.onFailed(error);
            }
        }));
    }

    public interface OnRecives{
        void onSuccess(User data);
        void onWrong(String msg);
        void onFailed(String mess);
    }

    public interface OnLogoutListener{
        void onSuccess(String msg, User data);
        void onSuccessFalse(String status);
        void onWrong(String msg);
        void onFailed(String mess);
    }

    public interface OnDeleteAccListener{
        void onSuccess(String action, String msg);
        void onFailed(String mess);
    }

}
