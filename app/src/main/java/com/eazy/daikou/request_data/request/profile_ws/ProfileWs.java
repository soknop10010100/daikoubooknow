package com.eazy.daikou.request_data.request.profile_ws;

import android.content.Context;

import com.eazy.daikou.repository_ws.CustomCallback;
import com.eazy.daikou.repository_ws.CustomResponseListener;
import com.eazy.daikou.request_data.request.book_now_ws.GenerateRespondWs;
import com.eazy.daikou.helper.Utils;
import com.eazy.daikou.model.profile.UserTypeProfileModel;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.eazy.daikou.repository_ws.RetrofitConnector;
import com.eazy.daikou.repository_ws.ServiceApi;
import com.eazy.daikou.model.profile.User;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;

public class ProfileWs {
    public void viewProfile(Context context,String deviceToken , String appName ,  CallBackListener callBackListener) {
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = serviceApi.getViewProfile(deviceToken, appName);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data")) {
                    JsonObject data = resObj.get("data").getAsJsonObject();
                    User viewUser = new Gson().fromJson(data, User.class);
                    callBackListener.onSuccessViewProfile(viewUser);
                } else {
                    callBackListener.onMessageFalse(resObj.get("msg").getAsString());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                callBackListener.onFailed(error);
            }
        }));
    }

    public void changeProfile(Context context, HashMap<String, String> switch_image, CallBackListener callBackListener) {
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = serviceApi.getUpdateCoverProfileImage(switch_image);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("success") && resObj.get("success").getAsString().equals("true")) {
                    if (resObj.has("msg")) {
                        String image = resObj.get("msg").getAsString();
                        callBackListener.updateSuccessLoadImage(image);
                    }
                } else {
                    if (resObj.has("msg")) {
                        callBackListener.onMessageFalse(resObj.get("msg").getAsString());
                    }
                }
            }

            @Override
            public void onError(String error, int resCode) {
                callBackListener.onFailed(error);
            }
        }));
    }

    public void putQrCodeToServer(Context context, HashMap<String, Object> hashMap, CallBackSendQrCodeListener callBackSendQrCodeListener) {
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = serviceApi.setQrCodeProfileParking(hashMap);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("success") && resObj.get("success").getAsString().equals("true")) {
                    if (resObj.has("msg")) {
                        callBackSendQrCodeListener.onSuccess(resObj.get("msg").getAsString());
                    }
                } else {
                    if (resObj.has("msg")) {
                        callBackSendQrCodeListener.onFailed(resObj.get("msg").getAsString());
                    }
                }
            }

            @Override
            public void onError(String error, int resCode) {
                callBackSendQrCodeListener.onFailed(error);
            }
        }));
    }

    public void sendAlertVerifyPinCodeEmailPhone(Context context, HashMap<String, Object> hashMap, CallBackUpdateEmailPhoneListener callBackListener) {
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = serviceApi.sendVerifyCodeEmailPhone(hashMap);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("success") && resObj.get("success").getAsString().equals("true")) {
                    if (resObj.has("data") && !resObj.get("data").isJsonNull()) {
                        JsonObject data = resObj.get("data").getAsJsonObject();
                        if (data.has("verify_code")) {
                            String dataString = data.get("verify_code").getAsString();
                            callBackListener.onSuccessVerifyCode(dataString);
                        }
                    }
                } else {
                        callBackListener.onFailed(resObj.get("msg").getAsString());
                }
            }
            @Override
            public void onError(String error, int resCode) {
                callBackListener.onFailed(error);
            }
        }));
    }

    public void changeEmailAndPhone(Context context, HashMap<String, Object> hashMap, CallBackEditFirstLogin callBackChangeEmailPhone){
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = serviceApi.editEmailOrPhone(hashMap);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("success") && resObj.get("success").getAsString().equals("true")){
                    if (resObj.has("msg")){
                        callBackChangeEmailPhone.onSuccess(resObj.get("msg").getAsString());
                    }
                } else {
                        callBackChangeEmailPhone.onFailed(resObj.get("msg").getAsString());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                callBackChangeEmailPhone.onFailed(error);
            }
        }));
    }

    public void editFirstLoginStatus(Context context, HashMap<String, Object> hashMap, CallBackEditFirstLogin callBackEditFirstLogin) {
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = serviceApi.editFirstLoginStatus(hashMap);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("success") && resObj.get("success").getAsBoolean()) {
                    callBackEditFirstLogin.onSuccess(resObj.get("msg").getAsString());
                } else {
                    callBackEditFirstLogin.onFailed(resObj.get("msg").getAsString());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                callBackEditFirstLogin.onFailed(error);
            }
        }));
    }

    public void getUserAccountList(Context context, CallBackSendQrCodeListener callBackSendQrCodeListener){
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = serviceApi.getUserAccount();
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data")) {
                    callBackSendQrCodeListener.onSuccess(GenerateRespondWs.Companion.onGenerateRespondDataListWs(resObj, UserTypeProfileModel.class));
                } else {
                    callBackSendQrCodeListener.onFailed(resObj.get("msg").getAsString());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                callBackSendQrCodeListener.onFailed(error);
            }
        }));
    }

    public void switchProfileUser(Context context, HashMap<String, Object> hashMap, CallBackSendQrCodeListener callBackSendQrCodeListener){
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Utils.logDebug("Jeheejejeejee", Utils.getStringGson(hashMap));
        Call<JsonElement> res = serviceApi.switchProfileUser(hashMap);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("success") && resObj.get("success").getAsBoolean()) {
                    callBackSendQrCodeListener.onSuccess(resObj.get("msg").getAsString());
                } else {
                    callBackSendQrCodeListener.onFailed(resObj.get("msg").getAsString());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                callBackSendQrCodeListener.onFailed(error);
            }
        }));
    }

    private void  deleteAccount(Context context , HashMap<String, String> hashMap, CallBackEditFirstLogin callBackDeleteAccount){
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = serviceApi.deleteAccountUser(hashMap);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("success") && resObj.get("success").getAsString().equals("true")){
                    if (resObj.has("msg")){
                        callBackDeleteAccount.onSuccess(resObj.get("msg").getAsString());
                    }
                } else {
                    callBackDeleteAccount.onFailed(resObj.get("msg").getAsString());
                }
            }
            @Override
            public void onError(String error, int resCode) {
                callBackDeleteAccount.onFailed(error);
            }
        }));
    }

    public interface CallBackListener {
        void onSuccessViewProfile(User user);
        void updateSuccessLoadImage(String message);
        void onMessageFalse(String msg);
        void onFailed(String error);
    }

    public interface CallBackUpdateEmailPhoneListener {
        void onSuccessVerifyCode(String pinCode);
        void onFailed(String error);
    }

    public interface CallBackSendQrCodeListener {
        void onSuccess(String msg);
        void onSuccess(List<UserTypeProfileModel> list);
        void onFailed(String error);
    }

    public interface CallBackEditFirstLogin {
        void onSuccess(String msg);
        void onFailed(String error);
    }
}


