package com.eazy.daikou.request_data.request.hr_ws

import android.content.Context
import com.eazy.daikou.repository_ws.CustomCallback
import com.eazy.daikou.repository_ws.CustomResponseListener
import com.eazy.daikou.repository_ws.RetrofitGenerator
import com.eazy.daikou.model.hr.*
import com.google.gson.Gson
import com.google.gson.JsonObject

class PayrollManagementWs {

    fun getListPayRollList(context: Context,page: Int,userId : String,callBackListener: OnCallBackListener){
        val payrollManagement = PayrollManagement()
        val api = RetrofitGenerator.createServiceWithHR(context)
        val res = api.getPayrollManagement(page, 10, userId)
        res.enqueue(CustomCallback(context, object : CustomResponseListener {
            override fun onSuccess(resObj: JsonObject) {
                if (!resObj.isJsonNull && resObj.has("data")) {
                    val ob = resObj.getAsJsonObject("data")
                    if(ob.has("basic_employee_info")){
                        val data = ob["basic_employee_info"].asJsonObject
                        val basicEmployeeInfo = Gson().fromJson(data, BasicEmployeeInfo::class.java)
                        payrollManagement.basic_employee_info = basicEmployeeInfo
                    }

                    if(ob.has("payroll_list")){
                        val listPayRoll = ArrayList<PayrollList>()
                        val jsonArray = ob.getAsJsonArray("payroll_list")
                        for (i in 0 until jsonArray.size()) {
                            val item = jsonArray[i].asJsonObject
                            val payrollList = Gson().fromJson(item.toString(), PayrollList::class.java)
                            listPayRoll.add(payrollList)
                            payrollManagement.payroll_list = listPayRoll
                        }
                    }
                    callBackListener.onLoadListSuccessFull(payrollManagement)

                } else {
                    callBackListener.onLoadFail(resObj["msg"].asString)
                }
            }

            override fun onError(error: String, resCode: Int) {
                callBackListener.onLoadFail(error)
            }
        }))

    }

    fun getListPayRollDetail(context: Context, payRoleId : String, userId : String,callBackListener: OnCallBackListener){
        val api = RetrofitGenerator.createServiceWithHR(context)
        val res = api.getPayrollManagementDetailById(payRoleId, userId)
        res.enqueue(CustomCallback(context, object : CustomResponseListener {
            override fun onSuccess(resObj: JsonObject) {
                if (resObj.has("data")) {
                    if (!resObj.get("data").isJsonNull){
                        val data = resObj.getAsJsonObject("data")
                        val payrollDetailModel = Gson().fromJson(data.asJsonObject, PayrollManagementDetailModel::class.java)
                        callBackListener.onSuccessDataPayrollDetail(payrollDetailModel)
                    }

                } else {
                    callBackListener.onLoadFail(resObj["msg"].asString)
                }
            }

            override fun onError(error: String, resCode: Int) {
                callBackListener.onLoadFail(error)
            }
        }))

    }

    fun confirmSalaryPayrollWS(context: Context, hashMap: HashMap<String, Any>, callBackConfirm : OnConfirmSalaryCallBackListener){
        val serviceApi = RetrofitGenerator.createServiceWithHR(context)
        val res = serviceApi.getConfirmReceiveSalary(hashMap)
        res.enqueue(CustomCallback(context, object : CustomResponseListener{
            override fun onSuccess(resObj: JsonObject?) {
                if (resObj != null) {
                    if (resObj.has("success")) {
                        if (resObj.has("msg")) {
                            callBackConfirm.onLoadListSuccess(resObj["msg"].asString)
                        }
                    } else {
                        callBackConfirm.onLoadFail(resObj["msg"].asString)
                    }
                }
            }

            override fun onError(error: String, resCode: Int) {
                callBackConfirm.onLoadFail(error)
            }

        }))
    }

    interface OnCallBackListener {
        fun onLoadListSuccessFull(listPayRoll: PayrollManagement)
        fun onSuccessDataPayrollDetail(listPayRoll: PayrollManagementDetailModel)
        fun onLoadFail(message: String)
    }

    interface OnConfirmSalaryCallBackListener {
        fun onLoadListSuccess(msg : String)
        fun onLoadFail(message: String)
    }

}