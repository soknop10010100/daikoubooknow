package com.eazy.daikou.request_data.request.facility_booking_ws;

import android.content.Context;
import android.widget.ImageView;

import com.eazy.daikou.repository_ws.RetrofitConnector;
import com.eazy.daikou.repository_ws.CustomCallback;
import com.eazy.daikou.repository_ws.CustomResponseListener;
import com.eazy.daikou.repository_ws.ServiceApi;
import com.eazy.daikou.model.facility_booking.CardItemByProperty;
import com.eazy.daikou.model.facility_booking.FacilityBookingModel;
import com.eazy.daikou.model.facility_booking.FacilitySpaceDetailModel;
import com.eazy.daikou.model.facility_booking.AlertPropertyBookingModel;
import com.eazy.daikou.model.facility_booking.ItemCardBookingModel;
import com.eazy.daikou.model.facility_booking.my_booking.CouponBookingModel;
import com.eazy.daikou.model.facility_booking.my_booking.MyBookingDetailByItemModel;
import com.eazy.daikou.model.facility_booking.my_booking.MyBookingModel;
import com.eazy.daikou.model.facility_booking.VenuePropertyBookingModel;
import com.eazy.daikou.model.facility_booking.my_booking.MyCategoryBookingModel;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;

public class FacilityBookingWs {

    public void getSpaceFacilityList(Context context, int page, int limit, String property_id, String facilityBasicId, String date_time, CallBackListFacilityListener callBackListFacilityListener){
        ServiceApi serviceApi = RetrofitConnector.createService( context);
        Call<JsonElement> res = serviceApi.getSpaceFacilityBooking(page, limit, property_id, facilityBasicId, date_time);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data")){
                    List<FacilityBookingModel> list = new ArrayList<>();
                    JsonArray data = resObj.get("data").getAsJsonArray();
                    for (int i=0; i<data.size();i++) {
                        JsonElement item = data.get(i).getAsJsonObject();
                        FacilityBookingModel complaintSolutionModel = new Gson().fromJson(item, FacilityBookingModel.class);
                        list.add(complaintSolutionModel);
                    }
                    callBackListFacilityListener.onLoadSuccessFull(list);
                } else {
                    callBackListFacilityListener.onLoadFailed(resObj.get("msg").getAsString());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                callBackListFacilityListener.onLoadFailed(error);
            }
        }));
    }

    public void getSpaceFacilityDetail(Context context, String userId, String spaceId, CallBackListFacilityDetailListener callBackListFacility){
        ServiceApi serviceApi = RetrofitConnector.createService( context);
        Call<JsonElement> res = serviceApi.getSpaceFacilityBookingDetail(spaceId,userId);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data")){
                    if (!resObj.get("data").isJsonNull()) {
                        JsonElement item = resObj.get("data").getAsJsonObject();
                        FacilitySpaceDetailModel complaintSolutionModel = new Gson().fromJson(item, FacilitySpaceDetailModel.class);
                        callBackListFacility.onLoadSuccessFull(complaintSolutionModel);
                    } else {
                        callBackListFacility.onLoadSuccessFull(new FacilitySpaceDetailModel());
                    }
                } else {
                    callBackListFacility.onLoadFailed(resObj.get("msg").getAsString());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                callBackListFacility.onLoadFailed(error);
            }
        }));
    }

    public void getVenueFacilityBooking(Context context, String list_by, String property_id, VenuePropertyBookingCallBackListener venuePropertyBookingCallBackListener){
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = serviceApi.getVenueFacilityBooking(list_by,property_id);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data")){
                    JsonArray jsonArray = resObj.get("data").getAsJsonArray();
                    List<VenuePropertyBookingModel> venuePropertyBookingModelList = new ArrayList<>();
                    for (int i = 0; i < jsonArray.size(); i++) {
                        VenuePropertyBookingModel venuePropertyBookingModel = new Gson().fromJson(jsonArray.get(i).getAsJsonObject(),VenuePropertyBookingModel.class);
                        venuePropertyBookingModelList.add(venuePropertyBookingModel);
                    }
                    venuePropertyBookingCallBackListener.onLoadSuccessFull(venuePropertyBookingModelList);
                }else {
                    venuePropertyBookingCallBackListener.onLoadFailed(resObj.get("msg").getAsString());
                }
            }
            @Override
            public void onError(String error, int resCode) {
                venuePropertyBookingCallBackListener.onLoadFailed(error);
            }
        }));
    }

    public void getPropertyBooking(Context context, String keySearch, BookingPropertyCallBackListener bookingPropertyCallBackListener){
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = keySearch.equalsIgnoreCase("") ? serviceApi.getPropertyFacilityBooking() : serviceApi.getListSearchProperty(keySearch);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data")){
                    JsonArray jsonArray = resObj.get("data").getAsJsonArray();
                    List<AlertPropertyBookingModel> alertPropertyBookingModelList = new ArrayList<>();
                    for (int i = 0; i <jsonArray.size() ; i++) {
                        AlertPropertyBookingModel alertPropertyBookingModel = new  Gson().fromJson(jsonArray.get(i).getAsJsonObject(), AlertPropertyBookingModel.class);
                        alertPropertyBookingModelList.add(alertPropertyBookingModel);
                    }
                    bookingPropertyCallBackListener.onLoadSuccessFull(alertPropertyBookingModelList);

                }else {
                    bookingPropertyCallBackListener.onLoadFailed(resObj.get("msg").getAsString());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                   bookingPropertyCallBackListener.onLoadFailed(error);
            }
        }));
    }

    public void getSearchFacility(Context context,int page,int limit,String key_search,String property_id, String facility_basic_id, CallBackListFacilityListener searchBookingCallback){
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = serviceApi.getSearchFacilityBooking(page,limit,key_search,property_id,facility_basic_id);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data")){
                    List<FacilityBookingModel> searchList = new ArrayList<>();
                    JsonArray jsonArray = resObj.get("data").getAsJsonArray();
                    for (int i=0; i<jsonArray.size();i++){
                        JsonElement itemSearch = jsonArray.get(i).getAsJsonObject();
                        FacilityBookingModel searchBookingModel = new Gson().fromJson(itemSearch,FacilityBookingModel.class);
                        searchList.add(searchBookingModel);
                    }
                    searchBookingCallback.onLoadSuccessFull(searchList);
                }else {
                    searchBookingCallback.onLoadFailed(resObj.get("msg").getAsString());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                searchBookingCallback.onLoadFailed(error);
            }
        }));
    }

    public void getMyBookingDetailList(Context context,int page,int limit, String user_id, String currentDateTime, String bookingStatus, MyBookingCallbackListener myBookingCallbackListener){
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = serviceApi.getListMyBookingDetail(page, limit, user_id, currentDateTime, bookingStatus);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data")){
                    List<MyCategoryBookingModel> myBookingModelList = new ArrayList<>();
                    JsonArray jsonArray = resObj.get("data").getAsJsonArray();
                    for (int i=0; i<jsonArray.size();i++){
                        JsonElement itemSearch = jsonArray.get(i).getAsJsonObject();
                        MyCategoryBookingModel searchBookingModel = new Gson().fromJson(itemSearch, MyCategoryBookingModel.class);
                        myBookingModelList.add(searchBookingModel);
                    }
                    myBookingCallbackListener.onLoadBookingDetailSuccessFull(myBookingModelList);
                }else {
                    myBookingCallbackListener.onLoadFailed(resObj.get("msg").getAsString());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                myBookingCallbackListener.onLoadFailed(error);
            }
        }));
    }

    public void getMyBookingDetailByDetail(Context context, String booking_item_id, CallBackBookingItemDetailListener myBookingCallbackListener){
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = serviceApi.getListMyBookingDetailByItem(booking_item_id);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data")){
                    if (!resObj.get("data").isJsonNull()) {
                        JsonElement jsonElement = resObj.get("data").getAsJsonObject();
                        MyBookingDetailByItemModel searchBookingModel = new Gson().fromJson(jsonElement.getAsJsonObject(), MyBookingDetailByItemModel.class);
                        myBookingCallbackListener.onLoadSuccessFull(searchBookingModel);
                    } else {
                        myBookingCallbackListener.onLoadSuccessFull(new MyBookingDetailByItemModel());
                    }
                } else {
                    myBookingCallbackListener.onLoadFailed(resObj.get("msg").getAsString());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                myBookingCallbackListener.onLoadFailed(error);
            }
        }));
    }

    public void applyCouponBooking(Context context, String spaceId, String code, String date, String dayName, String totalAmount, BookingCallBackListener bookingCallBackListener){
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = serviceApi.applyCouponBooking(spaceId, code, date, dayName, totalAmount);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data") && !resObj.get("data").isJsonNull()){
                    JsonElement jsonElement = resObj.get("data").getAsJsonObject();
                    CouponBookingModel searchBookingModel = new Gson().fromJson(jsonElement.getAsJsonObject(), CouponBookingModel.class);
                    bookingCallBackListener.onLoadCouponSuccessFull(searchBookingModel);
                }else {
                    bookingCallBackListener.onLoadFailed(resObj.get("msg").getAsString());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                bookingCallBackListener.onLoadFailed(error);
            }
        }));
    }

    public void addToCardBooking(Context context, HashMap<String, Object> hashMap, String action, BookingCallBackListener bookingCallBackListener){
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        // Add to card if action == "add_to_card" else delete item card booking
        Call<JsonElement> res = action.equalsIgnoreCase("add_to_card") ? serviceApi.addToCardBooking(hashMap) : serviceApi.deleteCardBookingItem(hashMap);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("success") && resObj.get("success").getAsString().equals("true")){
                    if (resObj.has("msg")) {
                        bookingCallBackListener.onLoadSuccessFull(resObj.get("msg").getAsString());
                    }
                } else {
                    if (resObj.has("msg")) {
                        bookingCallBackListener.onLoadFailed(resObj.get("msg").getAsString());
                    }
                }
            }

            @Override
            public void onError(String error, int resCode) {
                bookingCallBackListener.onLoadFailed(error);
            }
        }));
    }

    public void updateItemCard(Context context, HashMap<String, Object> hashMap, int numberCount, Double priceItem, CardItemByProperty itemBooking, String action, ImageView iconAction, BookingCallBackListener bookingCallBackListener){
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = serviceApi.updateBookingItem(hashMap);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("success") && resObj.get("success").getAsString().equals("true")){
                    if (resObj.has("msg")) {
                        bookingCallBackListener.onSuccessUpdateItemCard(numberCount, priceItem,itemBooking, action, iconAction);
                    }
                } else {
                    if (resObj.has("msg")) {
                        bookingCallBackListener.onLoadFailed(resObj.get("msg").getAsString());
                    }
                }
            }

            @Override
            public void onError(String error, int resCode) {
                bookingCallBackListener.onLoadFailed(error);
            }
        }));
    }

    public void getMyCardBookingList(Context context, String userId, CardBookingCallBackListener cardBookingCallBackListener){
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = serviceApi.getMyCardBooking(userId);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data")){
                    List<ItemCardBookingModel> myBookingModelList = new ArrayList<>();
                    JsonArray jsonArray = resObj.get("data").getAsJsonArray();
                    for (int i=0; i< jsonArray.size(); i++){
                        JsonElement itemSearch = jsonArray.get(i).getAsJsonObject();
                        ItemCardBookingModel searchBookingModel = new Gson().fromJson(itemSearch, ItemCardBookingModel.class);
                        myBookingModelList.add(searchBookingModel);
                    }
                    cardBookingCallBackListener.onLoadSuccessFull(myBookingModelList);
                }else {
                    cardBookingCallBackListener.onLoadFailed(resObj.get("msg").getAsString());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                cardBookingCallBackListener.onLoadFailed(error);
            }
        }));
    }

    public void checkOutPayBooking(Context context, HashMap<String, Object> hashMap, BookingCallBackListener bookingCallBackListener){
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = serviceApi.checkOutPayBooking(hashMap);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data") && !resObj.get("data").isJsonNull()){
                    JsonElement jsonElement = resObj.get("data").getAsJsonObject();
                    if (!jsonElement.isJsonNull() && !jsonElement.getAsJsonObject().get("kess_payment_link").isJsonNull()){
                        String kessPaymentLink = jsonElement.getAsJsonObject().get("kess_payment_link").getAsString();
                        bookingCallBackListener.onLoadCheckOutBookingSuccessFull(kessPaymentLink);
                    }
                }else {
                    bookingCallBackListener.onLoadFailed(resObj.get("msg").getAsString());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                bookingCallBackListener.onLoadFailed(error);
            }
        }));
    }

    public void cancelBookingItem(Context context, HashMap<String, Object> hashMap, MyCategoryBookingModel itemMainCategory,MyBookingCallbackListener bookingCallBackListener){
        ServiceApi serviceApi = RetrofitConnector.createService(context);
        Call<JsonElement> res = serviceApi.cancelBookingItem(hashMap);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("success") && resObj.get("success").getAsString().equals("true")){
                    if (resObj.has("msg")) {
                        bookingCallBackListener.onLoadMessageSuccessFull(itemMainCategory, resObj.get("msg").getAsString());
                    }
                } else {
                    if (resObj.has("msg")) {
                        bookingCallBackListener.onLoadFailed(resObj.get("msg").getAsString());
                    }
                }
            }

            @Override
            public void onError(String error, int resCode) {
                bookingCallBackListener.onLoadFailed(error);
            }
        }));
    }

    public interface CallBackListFacilityListener {
        void onLoadSuccessFull(List<FacilityBookingModel> selectPropertyGuests);
        void onLoadFailed(String message);
    }

    public interface CallBackListFacilityDetailListener {
        void onLoadSuccessFull(FacilitySpaceDetailModel selectPropertyGuests);
        void onLoadFailed(String message);
    }

    public interface BookingPropertyCallBackListener {
        void onLoadSuccessFull(List<AlertPropertyBookingModel> alertPropertyBookingModelList);
        void onLoadFailed(String error);
    }

    public interface VenuePropertyBookingCallBackListener {
        void onLoadSuccessFull(List<VenuePropertyBookingModel> listVenue);
        void onLoadFailed(String error);
    }

    public interface BookingCallBackListener{
        void onLoadSuccessFull(String successMsg);
        void onLoadCheckOutBookingSuccessFull(String successMsg);
        void onLoadCouponSuccessFull(CouponBookingModel couponBookingModel);
        void onSuccessUpdateItemCard(int numberCount, Double priceItem, CardItemByProperty itemBooking, String action, ImageView iconAction);
        void onLoadFailed(String error);
    }

    public interface MyBookingCallbackListener{
        void onLoadSuccessFull(List<MyBookingModel> myBookingModelList);
        void onLoadBookingDetailSuccessFull(List<MyCategoryBookingModel> myBookingModelList);
        void onLoadMessageSuccessFull(MyCategoryBookingModel itemMainCategory, String msg);
        void onLoadFailed(String error);
    }

    public interface CallBackBookingItemDetailListener{
        void onLoadSuccessFull(MyBookingDetailByItemModel listVenue);
        void onLoadFailed(String error);
    }

    public interface CardBookingCallBackListener{
        void onLoadSuccessFull(List<ItemCardBookingModel> itemCardBookingModelList);
        void onLoadFailed(String error);
    }

}
