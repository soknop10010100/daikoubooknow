package com.eazy.daikou.request_data.request.home_ws;

import android.app.Activity;
import android.content.Context;

import com.eazy.daikou.repository_ws.CustomCallback;
import com.eazy.daikou.repository_ws.CustomResponseListener;
import com.eazy.daikou.model.contact.ContactProperty;
import com.eazy.daikou.model.contact.FriendContactModel;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.eazy.daikou.repository_ws.RetrofitConnector;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import retrofit2.Call;

public class ContactWs {

    public void getContactDataPropertyAndDaiKou(Activity activity,String propertyId , int page,int limit, ContactCallBackListener contactCallBackListener){
        Call<JsonElement> res = RetrofitConnector.createService(activity).getPropertyContact(propertyId,page,limit);
        res.enqueue(new CustomCallback(activity, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if(resObj.has("data")){
                    JsonArray list = resObj.get("data").getAsJsonArray();
                    List<ContactProperty> stringList = new ArrayList<>();
                    for (int i = 0 ; i < list.size() ; i++){
                        JsonElement item = list.get(i).getAsJsonObject();
                        ContactProperty frontDesk = new Gson().fromJson(item, ContactProperty.class);
                        stringList.add(frontDesk);
                    }
                    contactCallBackListener.getContactModel(stringList);
                } else {
                    contactCallBackListener.onFailed(resObj.get("msg").getAsString());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                contactCallBackListener.onFailed(error);
            }
        }));

    }

    public void getListContactFriend(Context activity,String userId , int page, int limit, CallBackListener contactCallBackListener){
        Call<JsonElement> res = RetrofitConnector.createService(activity).getListAddFriendContact(userId, page, limit);
        res.enqueue(new CustomCallback(activity, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if(resObj.has("data")){
                    JsonArray list = resObj.get("data").getAsJsonArray();
                    List<FriendContactModel> stringList = new ArrayList<>();
                    for (int i = 0 ; i < list.size() ; i++){
                        JsonElement item = list.get(i).getAsJsonObject();
                        FriendContactModel frontDesk = new Gson().fromJson(item, FriendContactModel.class);
                        stringList.add(frontDesk);
                    }
                    contactCallBackListener.onListFriendsSuccess(stringList);
                } else {
                    contactCallBackListener.onFailed(resObj.get("msg").getAsString());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                contactCallBackListener.onFailed(error);
            }
        }));

    }

    public void addFriendContact(Context context, HashMap<String, String> hashMap, CallBackListener contactCallBackListener){
        Call<JsonElement> res = RetrofitConnector.createService(context).addFriendContact(hashMap);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("success") && resObj.get("success").getAsString().equals("true")){
                    contactCallBackListener.onSuccess(resObj.get("msg").getAsString());
                } else {
                    contactCallBackListener.onFailed(resObj.get("msg").getAsString());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                contactCallBackListener.onFailed(error);
            }
        }));

    }

    public interface ContactCallBackListener{
        void getContactModel(List<ContactProperty> contactModelList);
        void onFailed(String error);
    }

    public interface CallBackListener{
        void onListFriendsSuccess(List<FriendContactModel> friendContactModels);
        void onSuccess(String msgSuccess);
        void onFailed(String error);
    }

}
