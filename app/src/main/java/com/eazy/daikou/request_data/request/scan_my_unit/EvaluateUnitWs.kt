package com.eazy.daikou.request_data.request.scan_my_unit

import android.content.Context
import com.eazy.daikou.repository_ws.RetrofitConnector
import com.eazy.daikou.repository_ws.CustomCallback
import com.eazy.daikou.repository_ws.CustomResponseListener
import com.eazy.daikou.model.evaluate.EvaluateIItemModel
import com.eazy.daikou.model.evaluate.EvaluateModel
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.google.gson.JsonObject

class EvaluateUnitWs {
    fun saveEvaluate(context: Context, hashMap: HashMap<String, Any?>, callBackListener: OnCallBackListener){
        val api = RetrofitConnector.createService(context)
        val res = api.getEvaluate(hashMap)
        res.enqueue(CustomCallback(context, object : CustomResponseListener {
            override fun onSuccess(resObj: JsonObject) {
                if (resObj.has("success") && resObj["success"].asString == "true") {
                    if (resObj.has("msg")) {
                        callBackListener.onSuccess(resObj["msg"].asString)
                    }
                } else {
                    if (resObj.has("msg")) {
                        callBackListener.onLoadFail(resObj["msg"].asString)
                    }
                }
            }

            override fun onError(error: String, resCode: Int) {
                callBackListener.onLoadFail(error)
            }
        }))
    }

    fun listEvaluateWS(context: Context, page: Int , limit: Int, listBy : String, accountId : String, callBackList : OnCallBackListEvaluateListener){
        val serviceApi = RetrofitConnector.createService(context)
        val res = serviceApi.getListEvaluate(page, limit, listBy, accountId)
        res.enqueue(CustomCallback(context, object : CustomResponseListener{
            override fun onSuccess(resObj: JsonObject?) {
                if (resObj != null){
                    if (resObj.has("data")){
                        val listEvaluate: ArrayList<EvaluateIItemModel> = ArrayList()
                        val data = resObj["data"].asJsonArray
                        for (i in 0 until data.size()){
                            val item: JsonElement = data[i].asJsonObject
                            val evaluateModel: EvaluateIItemModel = Gson().fromJson(item, EvaluateIItemModel::class.java)
                            listEvaluate.add(evaluateModel)
                        }
                        callBackList.onSuccess(listEvaluate)
                    } else {
                        callBackList.onLoadFail(resObj["msg"].asString)
                    }
                }
            }

            override fun onError(error: String, resCode: Int) {
                callBackList.onLoadFail(error)
            }
        }))
    }

    fun detailEvaluationWS(context: Context, id : String, callBackDetail : OnCallBackDetailListener){
        val serviceApi = RetrofitConnector.createService(context)
        val res = serviceApi.getDetailEvaluate(id)
        res.enqueue(CustomCallback(context, object : CustomResponseListener{
            override fun onSuccess(resObj: JsonObject?) {
               if (resObj != null){
                   if (resObj.has("data")){
                       val data = resObj["data"].asJsonObject
                       val evaluateModel : EvaluateModel = Gson().fromJson(data.asJsonObject, EvaluateModel::class.java)
                       callBackDetail.onSuccess(evaluateModel)
                   } else {
                       callBackDetail.onLoadFail(resObj["msg"].asString)
                   }
               }
            }

            override fun onError(error: String, resCode: Int) {
                callBackDetail.onLoadFail(error)
            }

        }))
    }

    interface OnCallBackListener {
        fun onSuccess(myUnitInfo : String)
        fun onLoadFail(message: String)
    }

    interface OnCallBackListEvaluateListener{
        fun onSuccess(evaluateList : ArrayList<EvaluateIItemModel>)
        fun onLoadFail(message: String)
    }

    interface OnCallBackDetailListener{
        fun onSuccess(evaluate : EvaluateModel)
        fun onLoadFail(message: String)
    }
}