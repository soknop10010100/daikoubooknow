package com.eazy.daikou.request_data.request.parking_ws;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;

import com.eazy.daikou.repository_ws.RetrofitConnector;
import com.eazy.daikou.repository_ws.CustomCallback;
import com.eazy.daikou.repository_ws.CustomResponseListener;
import com.eazy.daikou.repository_ws.ServiceApi;
import com.eazy.daikou.model.parking.Parking;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ParkingWs {

    //getListParking
    public void getListParking(Context context,int page,int limit,String userId,String propertyId,String userType, OnRequestCallBackListener callBack){
        ArrayList<Parking> parkingArrayList = new ArrayList<>();
        ServiceApi api = RetrofitConnector.createService(context);
        Call<JsonElement> res = api.getListParking(page,limit,propertyId,userType,userId);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if(resObj.has("data")){
                    JsonArray jsonArray = resObj.get("data").getAsJsonArray();
                    for (int i = 0; i <jsonArray.size() ; i++) {
                        JsonObject item = jsonArray.get(i).getAsJsonObject();
                        Parking parking = new Gson().fromJson(item.toString(), Parking.class);
                        parkingArrayList.add(parking);
                    }
                    callBack.onLoadSuccess(parkingArrayList);
                }else {
                    callBack.onLoadFail(resObj.get("msg").getAsString());
                }

            }

            @Override
            public void onError(String error, int resCode) {
                callBack.onLoadFail(error);
            }
        }));
    }

    //getDetailParking
    public void getDetailParking(Context context, String parkingId, OnRequestDetailCallBackListener callBack){
        ServiceApi api = RetrofitConnector.createService(context);
        Call<JsonElement> res = api.getParkingDetail(parkingId);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if(resObj.has("success") && resObj.get("success").getAsBoolean()){
                    if(resObj.has("data")){
                        JsonElement data = resObj.get("data");
                        Parking parking = new Gson().fromJson(data, Parking.class);
                        callBack.onLoadSuccess(parking);
                    }else {
                        callBack.onLoadFail(resObj.get("msg").getAsString());
                    }
                }

            }

            @Override
            public void onError(String error, int resCode) {
                callBack.onLoadFail(error);
            }
        }));
    }

    public void createServiceParkingV2(Context context, HashMap<String,Object> hashMap, OnRequestCreateCallBackListener createCallBack){
        ServiceApi api = RetrofitConnector.createService(context);
        Call<JsonElement> res = api.createParkingV2(hashMap);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if(resObj.has("data")){
                    JsonObject object = resObj.get("data").getAsJsonObject();
                    String action = "",parkingId = "";
                    if(object.has("parking_action")){
                        action  = object.get("parking_action").getAsString();
                    }
                    if(object.has("parking_list_id")){
                        parkingId = object.get("parking_list_id").getAsString();
                    }
                    if(object.has("message")){
                        action = object.get("message").getAsString();

                    }
                    createCallBack.onLoadSuccessListener(action,parkingId);
                }
            }

            @Override
            public void onError(String error, int resCode) {
                createCallBack.onLoadFail(error);
            }
        }));

    }

    //CreateParking
    public void createServiceParking(Context context, HashMap<String,Object> hashMap, OnRequestCreateCallBackListener createCallBack){
        ServiceApi api = RetrofitConnector.createService(context);
        Call<JsonElement> res = api.createParking(hashMap);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if(resObj.has("msg")){
                    String message = resObj.get("msg").getAsString();
                    createCallBack.onLoadSuccess(message);
                }
            }

            @Override
            public void onError(String error, int resCode) {
                createCallBack.onLoadFail(error);
            }
        }));

    }

    public void acceptNotification(Context context, HashMap<String,Object> hashMap,String type, OnRequestCreateCallBackListener callBack){
        ServiceApi api = RetrofitConnector.createService(context);
        Call<JsonElement> res;
        if(type.equals("report")){
             res = api.acceptNotificationParking(hashMap);
        }else {
             res = api.canCelDetailParking(hashMap);
        }

        res.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(@NonNull Call<JsonElement> call, @NonNull Response<JsonElement> response) {
                if(response.isSuccessful()){
                    assert response.body() != null;
                    JsonObject jsonObject = response.body().getAsJsonObject();
                    if(jsonObject.has("success")){
                        boolean isSuccess = jsonObject.get("success").getAsBoolean();
                        if(isSuccess){
                            if(jsonObject.has("msg")){
                                String message = jsonObject.get("msg").getAsString();
                                callBack.onLoadSuccess(message);
                            }
                        }else {
                            callBack.onLoadFail(jsonObject.get("msg").getAsString());
                        }
                    }
                }else {
                   callBack.onLoadFail("error");
                }
            }

            @Override
            public void onFailure(@NonNull Call<JsonElement> call, @NonNull Throwable t) {
                callBack.onLoadFail("error");
            }
        });

    }

    //Security report
    public void updateServiceParking(Context context, HashMap<String,Object> hashMap, OnRequestCreateCallBackListener createCallBack){
        ServiceApi api = RetrofitConnector.createService(context);
        Call<JsonElement> res = api.staffUpdate(hashMap);
        String map = new Gson().toJson(hashMap);
        Log.d("maqwep", map);
        res.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(@NonNull Call<JsonElement> call, @NonNull Response<JsonElement> response) {
                Log.d("fkj",response.body() +"");
                if(response.isSuccessful()){
                    assert response.body() != null;
                    JsonObject jsonObject = response.body().getAsJsonObject();
                    if(jsonObject.has("success")){
                        boolean isSuccess = jsonObject.get("success").getAsBoolean();
                        if(isSuccess){
                            createCallBack.onLoadSuccess(jsonObject.get("msg").getAsString());
                        }else {
                            createCallBack.onLoadFail(jsonObject.get("msg").getAsString());
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<JsonElement> call, @NonNull Throwable t) {
                createCallBack.onLoadFail(t.getMessage());
            }
        });

    }


    public void checkOutServiceParking(Context context, HashMap<String,Object> hashMap, OnRequestCreateCallBackListener createCallBack){
        ServiceApi api = RetrofitConnector.createService(context);
        Call<JsonElement> res = api.checkOut(hashMap);
        res.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(@NonNull Call<JsonElement> call, @NonNull Response<JsonElement> response) {
                Log.d("fkj",response.body() +"");
                if(response.isSuccessful()){
                    assert response.body() != null;
                    JsonObject jsonObject = response.body().getAsJsonObject();
                    if(jsonObject.has("success")){
                        boolean isSuccess = jsonObject.get("success").getAsBoolean();
                        if(isSuccess){
                            createCallBack.onLoadSuccess(jsonObject.get("msg").getAsString());
                        }else {
                            createCallBack.onLoadFail(jsonObject.get("msg").getAsString());
                        }
                    }
                }else {
                    createCallBack.onLoadFail("error");
                }
            }

            @Override
            public void onFailure(@NonNull Call<JsonElement> call, @NonNull Throwable t) {
                createCallBack.onLoadFail("error");
            }
        });

    }


    public interface OnRequestCallBackListener {
        void onLoadSuccess(ArrayList<Parking> listParking);
        void onLoadFail(String message);
    }

    public interface OnRequestDetailCallBackListener {
        void onLoadSuccess(Parking parking);
        void onLoadFail(String message);
    }

    public interface OnRequestCreateCallBackListener {
        void onLoadSuccess(String listParking);
        void onLoadSuccessListener(String action,String parkingId);
        void onLoadFail(String message);
    }

}
