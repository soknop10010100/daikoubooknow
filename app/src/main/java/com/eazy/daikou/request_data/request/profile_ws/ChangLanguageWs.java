package com.eazy.daikou.request_data.request.profile_ws;

import androidx.annotation.NonNull;

import com.eazy.daikou.repository_ws.RetrofitGenerator;
import com.eazy.daikou.repository_ws.ServiceApi;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangLanguageWs {

    public void  getLanguage(HashMap<String, Object> hashMap, OnCallBackLanguages onCallBackLanguages){
        ServiceApi api = RetrofitGenerator.createCategoryService(false);
        Call<JsonElement> res = api.changLanguage(hashMap);
        res.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(@NonNull Call<JsonElement> call, @NonNull Response<JsonElement> response) {
                assert response.body() != null;
                JsonObject jsonObject = response.body().getAsJsonObject();
                if(jsonObject.has("success")){
                    boolean isSuccess = jsonObject.get("success").getAsBoolean();
                    if(isSuccess){
                        if (jsonObject.has("msg")){
                            String message = jsonObject.get("msg").toString();
                            onCallBackLanguages.onLoadChangSuccessFully(message);
                        }
                    }else {
                        if (jsonObject.has("msg")){
                            String message = jsonObject.get("msg").toString();
                            onCallBackLanguages.onLoadChangFail(message);
                        }

                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<JsonElement> call, @NonNull Throwable t) {

            }
        });

    }
    public interface OnCallBackLanguages{
        void onLoadChangSuccessFully(String message);
        void onLoadChangFail(String message);
    }
}
