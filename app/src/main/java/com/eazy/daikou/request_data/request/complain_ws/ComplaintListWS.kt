package com.eazy.daikou.request_data.request.complain_ws

import android.content.Context
import com.eazy.daikou.repository_ws.CustomCallback
import com.eazy.daikou.repository_ws.CustomResponseListener
import com.eazy.daikou.repository_ws.RetrofitConnector
import com.eazy.daikou.request_data.request.book_now_ws.GenerateRespondWs
import com.eazy.daikou.model.complaint.ComplaintListModel
import com.eazy.daikou.model.profile.PropertyItemModel
import com.eazy.daikou.model.profile.UnitNoModel
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.google.gson.JsonObject

class ComplaintListWS {

    companion object {

        fun getComplaintSolutionWs(
            context: Context,
            page: Int,
            limit: Int,
            listType: String,
            postType: String,
            accountId: String,
            callBack: OnResponseList
        ) {
            val res = RetrofitConnector.createService(context)
                .getResComplaintSolution(page, limit, listType, postType, accountId)
            val listComplaint = ArrayList<ComplaintListModel>()
            res.enqueue(CustomCallback(context, object : CustomResponseListener {
                override fun onSuccess(resObj: JsonObject?) {
                    if (resObj!!.has("data") && resObj.isJsonObject) {
                        val jsonObject = resObj.asJsonObject
                        val jsonArrayData = jsonObject.getAsJsonArray("data")
                        for (index in 0 until jsonArrayData.size()) {
                            listComplaint.add(
                                Gson().fromJson(
                                    jsonArrayData[index],
                                    ComplaintListModel::class.java
                                )
                            )
                        }
                        callBack.onSuccess(listComplaint)
                    } else {
                        callBack.onError(resObj["msg"].toString())
                    }
                }

                override fun onError(error: String, resCode: Int) {
                    callBack.onError(error)
                }
            }))
        }

        fun getDetailComplaint(context: Context, id: String, callBack: OnResponseDetail) {
            val api = RetrofitConnector.createService(context).getDetailCompliant(id)
            api.enqueue(CustomCallback(context, object : CustomResponseListener {
                override fun onSuccess(resObj: JsonObject) {
                    if (resObj.has("data")) {
                        if (!resObj["data"].isJsonNull) {
                            val jsonObject = resObj.getAsJsonObject("data")
                            val dataComplaintDetail =
                                Gson().fromJson(jsonObject, ComplaintListModel::class.java)
                            callBack.onSuccess(dataComplaintDetail)
                        } else {
                            callBack.onError(resObj["msg"].toString())
                        }
                    } else {
                        callBack.onError(resObj["msg"].toString())
                    }
                }

                override fun onError(error: String, resCode: Int) {
                    callBack.onError(error)
                }
            }))
        }

        fun createComplaintWS(
            context: Context,
            hashMap: HashMap<String, Any>,
            callBackCreateComplaint: CallBackCreateComplaint
        ) {
            val serviceApi = RetrofitConnector.createService(context)
            val res = serviceApi.postComplaint(hashMap)
            res.enqueue(CustomCallback(context, object : CustomResponseListener {
                override fun onSuccess(resObj: JsonObject) {
                    if (GenerateRespondWs.onGenerateRespondWs(resObj)) {
                        callBackCreateComplaint.onSuccessFull(resObj["msg"].asString)
                    } else {
                        callBackCreateComplaint.onFailed(resObj["msg"].asString)
                    }
                }

                override fun onError(error: String, resCode: Int) {
                    callBackCreateComplaint.onFailed(error)
                }
            }))
        }

        fun getAccountPropertyWs(
            context: Context,
            page: Int,
            limit: Int,
            search: String,
            callBackListProperty: CallBackListPropertyComplaint
        ) {
            val serviceApi = RetrofitConnector.createService(context)
            val res = serviceApi.getListItemPropertyComplaint(page, limit, search)
            res.enqueue(CustomCallback(context, object : CustomResponseListener {
                override fun onSuccess(resObj: JsonObject) {
                    if (resObj.has("data")) {
                        val listItem = ArrayList<PropertyItemModel>()
                        val data = resObj["data"].asJsonArray
                        for (i in 0 until data.size()) {
                            val item: JsonElement = data[i].asJsonObject
                            val itemListComplaintModel =
                                Gson().fromJson(item, PropertyItemModel::class.java)
                            listItem.add(itemListComplaintModel)
                        }
                        callBackListProperty.onSuccessFull(listItem)
                    } else {
                        callBackListProperty.onFailed(resObj["msg"].asString)
                    }
                }

                override fun onError(error: String, resCode: Int) {
                    callBackListProperty.onFailed(error)
                }

            }))
        }

        fun getUnitNoByAccountWs(
            context: Context,
            page: Int,
            limit: Int,
            accountId: String,
            search: String,
            callBackListProperty: CallBackListPropertyComplaint
        ) {
            val serviceApi = RetrofitConnector.createService(context)
            val res = serviceApi.getListUnitNoByAccount(page, limit, accountId, search)
            res.enqueue(CustomCallback(context, object : CustomResponseListener {
                override fun onSuccess(resObj: JsonObject) {
                    if (resObj.has("data")) {
                        val listItem = ArrayList<UnitNoModel>()
                        val data = resObj["data"].asJsonArray
                        for (i in 0 until data.size()) {
                            val item: JsonElement = data[i].asJsonObject
                            val itemListComplaintModel =
                                Gson().fromJson(item, UnitNoModel::class.java)
                            listItem.add(itemListComplaintModel)
                        }
                        callBackListProperty.onSuccessUnitNo(listItem)
                    } else {
                        callBackListProperty.onFailed(resObj["msg"].asString)
                    }
                }

                override fun onError(error: String, resCode: Int) {
                    callBackListProperty.onFailed(error)
                }

            }))
        }

        fun doReplyCmtComplaint(
            context: Context,
            hashMap: HashMap<String, Any>,
            callBackCreateComplaint: CallBackCreateComplaint
        ) {
            val serviceApi = RetrofitConnector.createService(context)
            val res = serviceApi.replyCommentComplaint(hashMap)
            res.enqueue(CustomCallback(context, object : CustomResponseListener {
                override fun onSuccess(resObj: JsonObject) {
                    if (GenerateRespondWs.onGenerateRespondWs(resObj)) {
                        callBackCreateComplaint.onSuccessFull((resObj["msg"].asString))
                    } else {
                        callBackCreateComplaint.onFailed(resObj["msg"].asString)
                    }
                }

                override fun onError(error: String, resCode: Int) {
                    callBackCreateComplaint.onFailed(error)
                }
            }))
        }

    }

    interface OnResponseList {
        fun onSuccess(list: List<ComplaintListModel>)
        fun onError(msg: String)
    }

    interface OnResponseDetail {
        fun onSuccess(dataComplaintDetail: ComplaintListModel)
        fun onError(msg: String)
    }

    interface CallBackListPropertyComplaint {
        fun onSuccessFull(listItemProperty: ArrayList<PropertyItemModel>)
        fun onSuccessUnitNo(listItemProperty: ArrayList<UnitNoModel>)
        fun onFailed(message: String)
    }

    interface CallBackCreateComplaint {
        fun onSuccessFull(message: String)
        fun onFailed(mes: String)
    }
}