package com.eazy.daikou.fcm;

import static com.eazy.daikou.helper.Utils.logDebug;

import android.annotation.SuppressLint;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.util.Log;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.eazy.daikou.R;
import com.eazy.daikou.repository_ws.RetrofitConnector;
import com.eazy.daikou.repository_ws.Constant;
import com.eazy.daikou.repository_ws.CustomCallback;
import com.eazy.daikou.repository_ws.CustomResponseListener;
import com.eazy.daikou.repository_ws.RetrofitGenerator;
import com.eazy.daikou.base.BaseActivity;
import com.eazy.daikou.base.MockUpData;
import com.eazy.daikou.request_data.static_data.StaticUtilsKey;
import com.eazy.daikou.helper.BroadcastTypes;
import com.eazy.daikou.helper.CheckIsAppActive;
import com.eazy.daikou.helper.UserSessionManagement;
import com.eazy.daikou.helper.Utils;
import com.eazy.daikou.ui.SplashScreenActivity;
import com.eazy.daikou.ui.home.book_now.booking_hotel.HotelBookingHistoryActivity;
import com.eazy.daikou.ui.home.complaint_solution.complaint.ComplaintsSolutionActivity;
import com.eazy.daikou.ui.home.emergency.fragmentEmergency.EmergencyHistoryActivity;
import com.eazy.daikou.ui.home.hrm.ListHRMActivity;
import com.eazy.daikou.ui.home.inspection_work_order.InspectionListActivity;
import com.eazy.daikou.ui.home.parking.ParkingListActivity;
import com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.activity.ListHistoryCleaningServiceActivity;
import com.eazy.daikou.ui.home.my_property_v1.property_service_provider_part.cleaning_v2.ServiceCleaningPaymentAndBookingActivity;
import com.eazy.daikou.ui.home.my_property_v1.property_service_employee.work_service_employee.ServicePropertyMainEmployeeListActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import retrofit2.Call;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    private String notification_type = "";
    private String scope = "";
    private String messageReport = "";
    private String orderId = "";
    private String userType = "";
    private String work_order_type = "";
    private String id = "";
    private String action_alert = "";
    private String itemType = "";
    private String messageBody = "";
    private String title = "";
    private String outInApp = "";
    private String operationPart = "";
    private String listTypeEmergency = "";

    // Book Now
    private String itemId = "";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        logDebug(TAG, "From: " + remoteMessage.getFrom());
        Utils.logDebug("Jehejejejehe", remoteMessage.toString());
        outInApp = UserSessionManagement.getString(Constant.OUT_IN_APP, this);

        if (remoteMessage.getData() != null){
            messageBody = remoteMessage.getData().get("body");
            title =  remoteMessage.getData().get("title");
            if (remoteMessage.getData().containsKey("additional_data")) {
                String data = remoteMessage.getData().get("additional_data");
                try {
                    if (data != null) {
                        JSONObject jsonObject = new JSONObject(data);

                        if(jsonObject.has("notification_type")) notification_type = jsonObject.get("notification_type").toString();

                        if(jsonObject.has("item_type")) itemType = jsonObject.get("item_type").toString();

                        if(jsonObject.has("order_id")) orderId = jsonObject.get("order_id").toString();

                        // Book Now
                        if(jsonObject.has("item_id")) orderId = jsonObject.get("item_id").toString();

                        // Check action only alert emergency
                        if(jsonObject.has("notification_action")) action_alert = jsonObject.get("notification_action").toString();
                        if(jsonObject.has("alert_to")) listTypeEmergency = jsonObject.get("alert_to").toString();

                        if(jsonObject.has("user_type")) userType = jsonObject.get("user_type").toString();

                        if(jsonObject.has("notification_report_type")) messageReport = jsonObject.get("notification_report_type").toString();

                        // Invoice
                        if (jsonObject.has("category")) {     //if no key "notification_type" it get key category
                            if (notification_type.equalsIgnoreCase(""))     notification_type = jsonObject.get("category").toString();
                        }
                        if (jsonObject.has("work_order_type")) {
                            work_order_type = jsonObject.get("work_order_type").toString();
                        }
                        if (jsonObject.has("operation_part")) {
                            operationPart = jsonObject.get("operation_part").toString();
                        }
                        if (jsonObject.has("active_user_type")) {
                            userType = jsonObject.get("active_user_type").toString();
                        }
                        if (jsonObject.has("id")) {
                            id = jsonObject.get("id").toString();
                        }
                        if (jsonObject.has("scope")) {
                            scope = jsonObject.get("scope").toString();
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d("erkf",e.getMessage());
                }
            }

            //Click on Notification
            actionOnNotification(notification_type);

            // Send to main action, request service notification
            Intent intent = new Intent(BroadcastTypes.UPDATE_IMAGE.name());
            intent.putExtra("notification", "notification");
            LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
        }
    }

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID tokenss
     * is initially generated so this is where you would retrieve the token.
     */
    @Override
    public void onNewToken(@NotNull String token) {
        logDebug(TAG, "Refreshed_token: " + token);
        Utils.saveString(Constant.FIREBASE_TOKEN, token, getApplicationContext());
        String tToken = Utils.getString(Constant.FIREBASE_TOKEN, getApplicationContext());
        logDebug(TAG, tToken);

        UserSessionManagement.saveString(Constant.OUT_IN_APP, "true", getApplicationContext());
        // sendRegistrationToServer(token,this);
    }

    public static void sendRegistrationToServer(final String token, Context ctx, String action) {
        String appName = CheckIsAppActive.Companion.is_daikou_active() ? "daikou" : "book_now";
        requestRegisterNotification(ctx, token, action, appName, !CheckIsAppActive.Companion.is_daikou_active());

    }

    private static void requestRegisterNotification(Context ctx, String token, String action, String appName, boolean isBookNow){
        HashMap<String, String> body = new HashMap<>();
        body.put("user_id", new UserSessionManagement(ctx).getUserId());
        body.put("fcm_token", token);
        body.put("device_token", BaseActivity.DEVICE_ID);
        body.put("device_type", "android");
        body.put("app_name",  appName);
        body.put("action", action);     // action : (add, remove) => add for register / remove for remove
        Call<JsonElement> res = isBookNow ?
                RetrofitGenerator.createServiceWithHotelBooking(ctx).registerNotificationBookNow(body) :
                RetrofitConnector.createService(ctx).registerNotification(body);
        res.enqueue(new CustomCallback(ctx, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                logDebug("uploadToken", "Success  " + resObj);
            }

            @Override
            public void onError(String error, int resCode) {
                logDebug("uploadToken", "error  " + error);
            }
        }));
    }

    // get action from notification
    private void actionOnNotification(String notification_type){
        sendNotification(getNotificationType(notification_type));
    }

    private String getNotificationType(String notification_type){
        if (notification_type.equalsIgnoreCase("emergency")){
            return "emergency";
        } else if (notification_type.equalsIgnoreCase("payment_receipt")
                || notification_type.equalsIgnoreCase("invoice_create")
                || notification_type.equalsIgnoreCase("invoice_overdue")
                || notification_type.equalsIgnoreCase("invoice_notify")
                || notification_type.equalsIgnoreCase("invoice_payment")) {

            return "invoice";
        } else if (notification_type.equalsIgnoreCase("frontdesk")) {
            return "frontdesk";
        } else if (notification_type.equalsIgnoreCase("complain_and_solution")) {
            return  "complain_and_solution";
        } else if(notification_type.equalsIgnoreCase("work_order_create")){
            return  "work_order_create";
        } else if(notification_type.equalsIgnoreCase("parking")) {
            return "parking";
        } else if(notification_type.equalsIgnoreCase("payroll")){
            return "payroll";
        } else if(notification_type.equalsIgnoreCase("leave_application")){
            return "leave_application";
        } else if(notification_type.equalsIgnoreCase("overtime_application")){
            return "overtime_application";
        }  else if(notification_type.equalsIgnoreCase("resignation")){
            return "resignation";
        } else if(notification_type.equalsIgnoreCase("noticeboard")){
            return "noticeboard";
        } else if(notification_type.equalsIgnoreCase("booking")){
            return "booking";
        } else if(notification_type.equalsIgnoreCase("work_schedule")){
            return "work_schedule_employee";    // can cleaner, security, spiderman ....
        } else if (notification_type.equalsIgnoreCase("property_operation_service_invoice")){
            return "invoice_service_operation"; // invoice service operation
        } else if (notification_type.equalsIgnoreCase("service_registration")){
            return "notification_registration"; // Notification Reschedule
        } else {
            return ""; // if no any action key type is equals ""
        }
    }

    @SuppressLint("UnspecifiedImmutableFlag")
    public void sendNotification(String key_type) {
        Intent intent = null;
        PendingIntent pendingIntent;
        if (!key_type.equalsIgnoreCase("")) { // if have any action
            if (outInApp.equalsIgnoreCase("true")) {         // Opening App
                switch (key_type){
                    case "complain_and_solution":{
                        if (orderId != null) {
                            intent = new Intent(this, ComplaintsSolutionActivity.class);
                            intent.putExtra("complaint_id", orderId);
                            intent.putExtra("list_type", itemType);
                            intent.putExtra("action", "complaint_notification");
                        } else {
                            intent = new Intent(this, SplashScreenActivity.class);
                        }
                        break;
                    }
                    case "work_order_create":{
                        intent = new Intent(this, InspectionListActivity.class);
                        intent.putExtra("work_order_id", id);
                        intent.putExtra("work_order_type", work_order_type);
                        intent.putExtra("action", StaticUtilsKey.work_order_action);
                        break;
                    }
                    case "parking":{
                        if(orderId != null){
                            intent = new Intent(this, ParkingListActivity.class);
                            intent.putExtra("order_id",orderId);
                            intent.putExtra("type",userType);
                            intent.putExtra("body",messageBody);
                            if(messageReport!=null){
                                intent.putExtra("parking_report",messageReport);
                            }
                            intent.putExtra("notification","notification_parking");
                        } else {
                            intent = new Intent(this, SplashScreenActivity.class);
                        }
                        break;
                    }
                    case "emergency":{
                        if(orderId != null) {
                            intent = new Intent(this, EmergencyHistoryActivity.class);
                            intent.putExtra("emergency_id", orderId);
                            intent.putExtra("action", "emergency");
                            intent.putExtra("list_type", listTypeEmergency.equalsIgnoreCase("employee") ? "all" : "sending");
                        } else {
                            intent = new Intent(this, SplashScreenActivity.class);
                        }
                        break;
                    }

                    // Part HRM
                    case "payroll":
                    case "leave_application":
                    case "overtime_application":
                    case "noticeboard":
                    case "resignation": {
                        if(orderId != null && Utils.isEmployeeRole(this)){
                            intent = new Intent(this, ListHRMActivity.class);
                            intent.putExtra("id", orderId);
                            intent.putExtra("action", key_type);
                        }else {
                            intent = new Intent(this, SplashScreenActivity.class);
                        }
                        break;
                    }
                    // Part Hotel
                    case "booking": {
                        if (itemId != null) {
                            intent = new Intent(this, HotelBookingHistoryActivity.class);
                            intent.putExtra("id", itemId);
                            intent.putExtra("action", "hotel_booking_notification");
                        } else {
                            intent = new Intent(this, SplashScreenActivity.class);
                        }
                        break;
                    }

                    //Part Service Property Employee
                    case "work_schedule_employee": {
                        if (orderId != null){
                            intent = new Intent(this, ServicePropertyMainEmployeeListActivity.class);
                            intent.putExtra("id", orderId);
                            intent.putExtra("action_notification", "notification_work_schedule_employee");
                            intent.putExtra("action", operationPart);
                        } else {
                            intent = new Intent(this, SplashScreenActivity.class);
                        }
                        break;
                    }

                    //Part Service Property Owner
                    case "invoice_service_operation" : {
                        if (orderId != null ){
                            intent = new Intent(this, ServiceCleaningPaymentAndBookingActivity.class);
                            intent.putExtra("action_notification", "notification_invoice");
                            intent.putExtra("action", operationPart);
                            intent.putExtra("id", orderId);
                        } else  {
                            intent = new Intent(this, SplashScreenActivity.class);
                        }
                        break;
                    }

                    case "notification_registration" : {
                        if (orderId != null ){
                            intent = new Intent(this, ListHistoryCleaningServiceActivity.class);
                            intent.putExtra("action_notification", "notification_reschedule");
                            intent.putExtra("operation_part", operationPart);
                            intent.putExtra("order_id", orderId);
                        } else  {
                            intent = new Intent(this, SplashScreenActivity.class);
                        }
                        break;
                    }
                }
            }

            else {                                                 // Closed App
                intent = new Intent(this, SplashScreenActivity.class);
                switch (key_type){
                    case "frontdesk":{
                        intent.putExtra("action", "front_desk");
                        break;
                    }
                    case "invoice":{
                        intent.putExtra("action", "payment");
                        break;
                    }
                    case "complain_and_solution":{
                        if (MockUpData.UPDATE_COMPLAINT.equals(true)) {
                            if (orderId != null) {
                                intent.putExtra("action", "complaint_notification");
                                intent.putExtra("list_type", itemType);
                                intent.putExtra("complaint_id", orderId);
                            }
                        }
                        break;
                    }
                    case "emergency":{
                        intent.putExtra("emergency_id", orderId);
                        intent.putExtra("action", "emergency");
                        break;
                    }
                    case "work_order_create":{
                        intent.putExtra("id", id);
                        intent.putExtra("work_order_type", work_order_type);
                        intent.putExtra("action", StaticUtilsKey.work_order_action);
                        break;
                    }
                    case "payroll":
                    case "leave_application":
                    case "noticeboard":
                    case "resignation":
                    case "overtime_application":{
                        if(orderId != null){
                            intent.putExtra("id", orderId);
                            intent.putExtra("action", key_type);
                        }
                        break;
                    }
                }
            }
        }
        else {
            intent = new Intent(this, SplashScreenActivity.class);
        }

        assert intent != null;
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT | PendingIntent.FLAG_CANCEL_CURRENT | PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);

        if (messageBody == null) {
            messageBody = this.getString(R.string.description);
        }
        if (title == null) {
            if (notification_type.equalsIgnoreCase("invoice")) {
                title = this.getString(R.string.invoice);
            } else {
                title = this.getString(R.string.notification);
            }
        }

        // String channelId = "channel_id";
        String channelId = this.getPackageName();
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, channelId)
                .setContentTitle(title)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody))
                .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL);
        notificationBuilder.setSmallIcon(R.drawable.ic_logo_notification);
        notificationBuilder.setColor(ContextCompat.getColor(this, R.color.colorPrimary));

        if (key_type.equalsIgnoreCase("emergency") && action_alert.equalsIgnoreCase("alert")){
            playNotificationSound(this, notificationBuilder);
        } else {
            notificationBuilder.setSound(defaultSoundUri);
        }

        //  Notification On Vibrate
        setVibrate();

        notificationBuilder.setContentIntent(pendingIntent);

        // Since android Oreo notification channel is needed.
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = this.getString(R.string.app_name);
            String description = this.getString(R.string.app_name);
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel channel = new NotificationChannel(channelId, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            notificationManager.createNotificationChannel(channel);
        }
        int num = (int) System.currentTimeMillis();
        notificationManager.notify(num /* ID of notification */, notificationBuilder.build());
    }

    public void playNotificationSound(Context context, NotificationCompat.Builder notificationBuilder) {
        try {
            Uri alarmSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + context.getPackageName() + "/" + R.raw.emergency);
            Ringtone r = RingtoneManager.getRingtone(context, alarmSound);
            r.play();
            notificationBuilder.setSound(alarmSound);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("MissingPermission")
    private void setVibrate() {
        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        // Vibrate for 30 seconds
        assert v != null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            v.vibrate(VibrationEffect.createOneShot(1800, VibrationEffect.DEFAULT_AMPLITUDE));
        } else {
            //deprecated in API 26
            v.vibrate(1800);
        }
    }
}
